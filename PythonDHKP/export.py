import cx_Oracle
import datetime
import xlsxwriter
import os

today = f"{datetime.datetime.now():%Y%m%d}"
today2 = f"{datetime.datetime.now():%d-%m-%Y}"
tahun = f"{datetime.datetime.now():%Y}"
if not os.path.exists(today):
    os.makedirs(today)
#start koneksi
con = cx_Oracle.connect('LAYANAN/layanan123@192.168.1.200:1521/sukses')
#start query
cur = con.cursor()
cur.execute("select * from REF_KECAMATAN WHERE KD_KECAMATAN IN('090','040','130','270','070','010','050','140','230','020','260')")
# cur.execute("select * from REF_KECAMATAN WHERE KD_KECAMATAN IN('140')")
res = cur.fetchall()
try:
    for r in res:
        kode=r[2]
        if os.path.isfile(today+'/DHKP-'+today+'-'+kode+'.xlsx'):
            os.remove(today+'/DHKP-'+today+'-'+kode+'.xlsx')
        workbook = xlsxwriter.Workbook(today+'/DHKP-'+today+'-'+kode+'.xlsx')
        print(today+'/DHKP-'+today+'-'+kode+'.xlsx')
        nm_file='PythonDHKP/'+today+'/DHKP-'+today+'-'+kode+'.xlsx'
        qry = "insert into P_DHKP_KEC_FILE(KD_KECAMATAN, FILE_DHKP,TANGGAL) values ('"+str(kode)+"','"+nm_file+"',sysdate )"
        # print(qry)
        cur.execute("select count(ID_INC) ID from p_dhkp_kec_file where kd_kecamatan="+kode+" and TO_DATE(TO_CHAR(TANGGAL,'dd/mm/yyyy'),'dd/mm/yyyy') = TO_DATE('"+today2+"','DD-MM-YYYY')")
        cek = cur.fetchone()
        if cek[0]==0:
            cur.execute(qry)
            con.commit()
        else:
            statement = "delete from P_DHKP_KEC_FILE WHERE kd_kecamatan="+kode+" and TO_DATE(TO_CHAR(TANGGAL,'dd/mm/yyyy'),'dd/mm/yyyy') = TO_DATE('"+today2+"','DD-MM-YYYY')"
            cur.execute(statement)
            cur.execute(qry)
            con.commit()
        desa="select nm_kelurahan nama_kel,kd_kelurahan,kd_kecamatan from ref_kelurahan where kd_kecamatan="+kode+" and kd_kelurahan in (select kd_kelurahan from mv_dhkp_desa_tahun where thn_pajak_sppt="+tahun+" and kd_kecamatan='"+kode+"' ) order by to_number(kd_kelurahan) asc"
        # desa="select nm_kelurahan nama_kel,kd_kelurahan,kd_kecamatan from ref_kelurahan where kd_kecamatan="+kode+" and kd_kelurahan ='012' order by to_number(kd_kelurahan) asc"
        # print(desa)
        sql = con.cursor()
        sql.execute(desa)
        sql2 = sql.fetchall()
        for s in sql2:
            # print(s[1]+' '+s[0])
            worksheet = workbook.add_worksheet(s[1]+' '+s[0])
            # style
            bold = workbook.add_format({'bold': True,'border': True})
            border = workbook.add_format({'border':True})
            format6 = workbook.add_format({'num_format': 'd-mm-yyyy','border': True})
            money = workbook.add_format({'num_format': '#,###','border': True})
            # Start from the first cell below the headers.
            row_num = 0
            columns = ['NOP', 'Alamat OP', 'RW / RT OP', 'Luas Bumi', 'Luas Bangunan', 'Nama', 'Alamat WP', 'RW / RT WP', 'Kelurahan WP', 'Kota WP', 'PBB Awal', 'PBB Akhir','Tahun', 'Terbit SPPT', 'Lunas','Bayar SPPT','Keterangan']
            for col_num in range(len(columns)):
                worksheet.write(row_num, col_num, columns[col_num],bold)
            q = "select NOP,ALAMAT_OP,RWRT_OP,LUAS_BUMI_SPPT,LUAS_BNG_SPPT,NM_WP_SPPT,ALAMAT_WP,RWRT_WP,KELURAHAN_WP_SPPT,KOTA_WP_SPPT,null,PBB,THN_PAJAK_SPPT,to_char(TGL_TERBIT_SPPT,'dd/mm/yyyy') TGL_TERBIT_SPPT,null,to_char(TGL_PEMBAYARAN_SPPT,'dd/mm/yyyy') TGL_PEMBAYARAN_SPPT ,KET,case when TGL_PEMBAYARAN_SPPT is  null then 'Belum Bayar' else 'Lunas' end status_bayar from mv_dhkp_desa_tahun where thn_pajak_sppt="+tahun+" and KD_KECAMATAN='"+s[2]+"' and kd_kelurahan='"+s[1]+"'"
            # print(q)
            data = con.cursor()
            data.execute(q)
            data2 = data.fetchall()
            # print(data2[0])
            total=0;
            for d in data2:
                row_num += 1
                if d[11] is None:
                    total +=0
                else:
                    total +=d[11]
                if d[15] is None:
                    lunas="Belum Bayar"
                else:
                    lunas="Lunas"
                worksheet.write(row_num, 0, d[0],border)
                worksheet.write(row_num, 1, d[1],border)
                worksheet.write(row_num, 2, d[2],border)
                worksheet.write(row_num, 3, d[3],border)
                worksheet.write(row_num, 4, d[4],border)
                worksheet.write(row_num, 5, d[5],border)
                worksheet.write(row_num, 6, d[6],border)
                worksheet.write(row_num, 7, d[7],border)
                worksheet.write(row_num, 8, d[8],border)
                worksheet.write(row_num, 9, d[9],border)
                worksheet.write(row_num, 10, d[10],border)
                worksheet.write(row_num, 11, d[11],border)
                worksheet.write(row_num, 12, d[12],border)
                worksheet.write(row_num, 13, d[13],format6)
                worksheet.write(row_num, 14, lunas,border)
                worksheet.write(row_num, 15, d[15],border)
                worksheet.write(row_num, 16, d[16],border)
            worksheet.write(row_num+1, 11, total,money)
            data.close()
        print ("one record added successfully")
        sql.close()
        workbook.close()
    print('1')
except AssertionError as error:
    # print(error)
    # print ("error in operation")
    print('0')
    con.rollback()
#tutup query
cur.close()
#tutup koneksi
con.close()