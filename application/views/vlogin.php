
<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title> Log in</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- Bootstrap 3.3.2 -->
    <link href="<?php echo base_url().'adminlte/'?>bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <!-- Font Awesome Icons -->
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- Theme style -->
    <link href="<?php echo base_url().'adminlte/'?>dist/css/AdminLTE.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url().'adminlte/'?>plugins/chosen/chosen.css" rel="stylesheet" type="text/css" />
  </head>
  <body class="login-page">
    <div class="login-box">
      <div class="login-logo">
        <a href="#"><b>Layanan</b>PBB</a>
      </div><!-- /.login-logo -->
      <div class="login-box-body">
        <p class="login-box-msg"><?php echo $this->session->flashdata('notif')?></p>
        <form action="<?php echo base_url().'auth/proses'?>" method="post" autocomplete="off">
          <div class="form-group has-feedback">
            <select class="form-control" id="unit_kantor" name="unit_kantor" required >
                <option value="">Pilih Tempat Layanan</option>
                <option value="kota">Kantor Pendopo</option>
                <option value="kabupaten">Kantor Kepanjen</option>
            </select>
          </div>
        <?php if(empty($list)){?>
          <div class="form-group has-feedback">
            <input type="text" class="form-control" id="username" autocomplete="off" placeholder="Username" name="username" value="" >
            <span class="glyphicon glyphicon-user form-control-feedback"></span>
          </div>
          <?php }else{?>
          <div class="form-group has-feedback">
            <select class="chosen form-control" id="username" name="username"  >
                <option value=""></option>
                <?php foreach($list as $rk){?>
                <option value="<?= $rk->USERNAME?>"><?= $rk->LIST?></option>
                <?php } ?>
            </select>
          </div>

          <?php } ?>
          <div class="form-group has-feedback">
            <input type="password" class="form-control"  autocomplete="off"  placeholder="Password" name="password" value="" />
            <span class="glyphicon glyphicon-lock form-control-feedback"></span>
          </div>
          <div class="row">
            <div class="col-xs-8">    
              <div class="checkbox icheck">
                <label>
                  <!-- <a href="#">I forgot my password</a><br> -->
                </label>
              </div>                        
            </div><!-- /.col -->
            <div class="col-xs-4">
              <button type="submit" class="btn btn-primary btn-block btn-flat"><i class="fa fa-sign-in"></i> Masuk</button>
            </div><!-- /.col -->
          </div>
        </form>
        
      </div><!-- /.login-box-body -->
    </div><!-- /.login-box -->

    <!-- jQuery 2.1.3 -->
    <script src="<?php echo base_url().'adminlte/'?>plugins/jQuery/jQuery-2.1.3.min.js"></script>
    <!-- Bootstrap 3.3.2 JS -->
    <script src="<?php echo base_url().'adminlte/'?>bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    
    <script src="<?php echo base_url().'adminlte/'?>plugins/chosen/chosen.jquery.js" type="text/javascript"></script>
    <!-- iCheck 
    <script src="<?php echo base_url().'adminlte/'?>plugins/iCheck/icheck.min.js" type="text/javascript"></script>-->
    <script>

         $(function() {
        
        $('.chosen').chosen({ allow_single_deselect: true });
         // $( ".tanggal" ).datepicker();
           
      });

      $(function () {
        $('input').iCheck({
          checkboxClass: 'icheckbox_square-blue',
          radioClass: 'iradio_square-blue',
          increaseArea: '20%' // optional
        });
      });


      jQuery.extend(jQuery.expr[':'], {
    focusable: function (el, index, selector) {
        return $(el).is('a, button, :input, [tabindex]');
    }
});

$(document).on('keypress', '#username', function (e) {
    if (e.which == 13) {
        e.preventDefault();
        // Get all focusable elements on the page
        var $canfocus = $(':focusable');
        var index = $canfocus.index(this) + 1;
        if (index >= $canfocus.length) index = 0;
        $canfocus.eq(index).focus();
    }
});
    </script>
  </body>
</html>