 <section class="content-header">
  <h1>
    <?php echo $button ?>
  </h1>
</section>
<section class="content">
  <div class="nav-tabs-custom">
     <ul class="nav nav-tabs">
        <li class="<?php if(isset($active2)){ echo $active2; }?>"><a href="#tab_1" data-toggle="tab">NOP Belum Mapping Peta</a></li>
        <li class="<?php if(isset($active)){ echo $active; }?>"><a href="#tab_2" data-toggle="tab">NOP Sudah Mapping Peta</a></li>
      </ul>
      <div class="tab-content">
          <div class="tab-pane <?php if(isset($active2)){ echo $active2; }?>" id="tab_1">
            
            <form class="form-inline" role="form" method="post" action="<?= base_url().'gis/prosesimport' ?>" enctype="multipart/form-data">
                    <div class="form-group">
                      File
                    </div>
                    <div class="form-group">
                      <input type="file" name="data_excel" class="form-control" required="">
                    </div>
                                    
                    <button type="submit" class="btn btn-success"><i class="fa fa-upload"></i> Upload</button>
                    <a href="<?php echo base_url('assets/mapping/MappingNop.xls')?>">Download File</a>
                    <div class="form-group">
                      <?php echo $this->session->flashdata('notif'); ?>
                    </div>
                  </form>
                  <br>
                  <hr>
            <form class="form-inline" method="post" action="<?php echo base_url().'gis/mappingNop.html'?>">
                      <input type="hidden" name="cari2" value="1">
                    <!-- <div class="form-group">
                      Tanggal Layanan
                    </div>
                    <div class="form-group">
                      <input style="width: 90px;" type="text" class="form-control tanggal" name="tgl" value="<?php echo $tgl?>" reuired placeholder="Tanggal Layanan">
                    </div> -->
                    <div class="form-group">
                      Kecamatan
                    </div>
                    <div class="form-group">
                      <select class="form-control" name="KD_KECAMATAN2" id="KD_KECAMATAN2">
                                    <option value="">Pilih</option>
                                    <?php foreach($kec2 as $kec2){?>
                                      <option <?php if($kec2->KD_KECAMATAN==$KD_KECAMATAN2){echo "selected";}?> value="<?= $kec2->KD_KECAMATAN ?>"><?= $kec2->KD_KECAMATAN.' '.$kec2->NM_KECAMATAN ?></option>
                                    <?php } ?>
                      </select>
                    </div>
                    <div class="form-group">
                      Kelurahan
                    </div>
                    <div class="form-group">
                      <select class="form-control"  name="KD_KELURAHAN2" id="KD_KELURAHAN2">
                                    <option value="">Pilih</option>
                                    <?php if(isset($kel2)){?><option value="">Semua Kelurahan</option><?php }?>
                                    <?php 
                                      foreach($kel2 as $kela2){?>
                                        <option  <?php if($kela2->KD_KELURAHAN==$KD_KELURAHAN2){echo "selected";}?> value="<?= $kela2->KD_KELURAHAN?>"><?= $kela2->KD_KELURAHAN.' '.$kela2->NM_KELURAHAN?></option>
                                    <?php  }?>
                                  </select>
                    </div>
<!-- 
                    <div class="form-group">
                      Blok
                    </div>
                    <div class="form-group">
                      <select class="form-control"  name="KD_BLOK" id="KD_BLOK">
                                    <option value="">Pilih</option>
                                    <?php if(isset($blok2)){?><option value="">000 Semua Blok</option><?php }?>
                                    <?php 
                                      foreach($blok as $bloka){?>
                                        <option  <?php if($bloka->KD_BLOK==$KD_BLOK){echo "selected";}?> value="<?= $bloka->KD_BLOK?>"><?= $bloka->KD_BLOK?></option>
                                    <?php  }?>
                                  </select>
                    </div>
                    -->
                    <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i> Cari</button>
                    
                  </form>
                  <div class="row">
                    <div class="col-md-9" id="map" style="width: 83%; height: 400px;"></div>
                    <div class="col-md-1" id="batas" style="width: 1%; height: 400px;"></div>
                    <div class="col-md-2"  id="ket" style="width: 16%; height: 400px;">
                      <p>
                      <span>Langkah - Langkah Mapping NOP : </span>
                      <hr>
                      </p>
                      <p>
                      <span>1. Cari posisi lokasi NOP </span>
                      </p>
                      <p>
                      <span>2. Klik pada lokasi terpilih </span>
                      </p>
                      <p>
                      <span>3. Tampil form input </span>
                      </p>
                      <p>
                      <span>4. Isikan NOP yang valid </span>
                      </p>
                      <p>
                      <span>5. Klik button "Simpan" </span>
                      </p>
                    </div>
                  </div>
              
              
              </div>
              <!-- /.tab-pane -->
              <div class="tab-pane <?php if(isset($active)){ echo $active; }?>" id="tab_2">
                  <!-- form tab 2 -->
                  </form>
                  <br>
            <form class="form-inline" method="post" action="<?php echo base_url().'gis/mappingNop.html'?>">
                      <input type="hidden" name="cari" value="1">
                    <!-- <div class="form-group">
                      Tanggal Layanan
                    </div>
                    <div class="form-group">
                      <input style="width: 90px;" type="text" class="form-control tanggal" name="tgl" value="<?php echo $tgl?>" reuired placeholder="Tanggal Layanan">
                    </div> -->
                    <div class="form-group">
                      Kecamatan
                    </div>
                    <div class="form-group">
                      <select class="form-control" name="KD_KECAMATAN" id="KD_KECAMATAN">
                                    <option value="">Pilih</option>
                                    <?php foreach($kec as $kec){?>
                                      <option <?php if($kec->KD_KECAMATAN==$KD_KECAMATAN){echo "selected";}?> value="<?= $kec->KD_KECAMATAN ?>"><?= $kec->KD_KECAMATAN.' '.$kec->NM_KECAMATAN ?></option>
                                    <?php } ?>
                      </select>
                    </div>
                    <div class="form-group">
                      Kelurahan
                    </div>
                    <div class="form-group">
                      <select class="form-control"  name="KD_KELURAHAN" id="KD_KELURAHAN">
                                    <option value="">Pilih</option>
                                    <?php if(isset($kel)){?><option value="">Semua Kelurahan</option><?php }?>
                                    <?php 
                                      foreach($kel as $kela){?>
                                        <option  <?php if($kela->KD_KELURAHAN==$KD_KELURAHAN){echo "selected";}?> value="<?= $kela->KD_KELURAHAN?>"><?= $kela->KD_KELURAHAN.' '.$kela->NM_KELURAHAN?></option>
                                    <?php  }?>
                                  </select>
                    </div>

                    <div class="form-group">
                      Blok
                    </div>
                    <div class="form-group">
                      <select class="form-control"  name="KD_BLOK" id="KD_BLOK">
                                    <option value="">Pilih</option>
                                    <?php if(isset($blok)){?><option value="">000 Semua Blok</option><?php }?>
                                    <?php 
                                      foreach($blok as $bloka){?>
                                        <option  <?php if($bloka->KD_BLOK==$KD_BLOK){echo "selected";}?> value="<?= $bloka->KD_BLOK?>"><?= $bloka->KD_BLOK?></option>
                                    <?php  }?>
                                  </select>
                    </div>
                   
                    <button type="submit" class="btn btn-primary"><i class="fa fa-search"></i> Cari</button>
                    
                  </form>
                  <br>
              <div id="map2" style="width: 100%; height: 400px;"></div>

              </div>
              <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
          </div>

          <div class="modal fade" id="modal-lokasi">
          <div class="modal-dialog">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Tambah Lokasi NOP</h4>
              </div>
              <div class="modal-body">
                <!-- <p>One fine body&hellip;</p> -->
                  <form action="<?= base_url().'gis/tambahlokasiNop'?>" method="post">
                      <div class="form-group">
                        <label>NOP</label>
                        <input type="text" onkeyup="formatnop(this)"  autofocus="true" class="form-control" id="NOP"  name="NOP" placeholder="" autofocus="true" required>
                      </div>
                      <div class="form-group">
                        <label>Latitude</label>
                        <input type="text" id="lati" name="lati" class="form-control" required="" readonly="" >
                      </div>
                      <div class="form-group">
                        <label>Longitude</label>
                        <input type="text" id="longi" name="longi" class="form-control" required="" readonly="" >
                      </div>
                      <!-- <div class="form-group"> -->
                          <!-- <div class="pull-right"> -->
                            <button type="submit" class="btn btn-success" >Simpan</button>
                          <!-- </div> -->
                      <!-- </div> -->
                  </form>

              </div>
              <!-- <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
              </div> -->
            </div>
            <!-- /.modal-content -->
          </div>
          <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->

<div class="visible-print" id="div1"></div>
          
        </section>

<script type="text/javascript"> 
function stopRKey(evt) { 
  var evt = (evt) ? evt : ((event) ? event : null); 
  var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null); 
  if ((evt.keyCode == 13) && (node.type=="text"))  {return false;} 
} 
document.onkeypress = stopRKey; 
 


 
</script>
<script>
function myMap() {
var mapProp= {
  //center:new google.maps.LatLng(-33.9,151.2),
   center:new google.maps.LatLng(-7.9977877,112.7341118),
  zoom:10,
};

var mapProp2= {
  //center:new google.maps.LatLng(-33.9,151.2),
  <?php if(isset($data2)){?>
   center:new google.maps.LatLng(<?php echo $data2->LATI; ?>,<?php echo $data2->LONGI; ?>),
   zoom:14,
  <?php }else{ ?>
    center:new google.maps.LatLng(-7.9977877,112.7341118),
    zoom:10,
  <?php
  } 
  ?>
    
};



var customLabel = {
        restaurant: {
          label: 'R'
        },
        bar: {
          label: 'B'
        }
      };

var map = new google.maps.Map(document.getElementById("map"),mapProp2);
var map2 = new google.maps.Map(document.getElementById("map2"),mapProp);

google.maps.event.addListener(map, 'click', function(event) {
$('#modal-lokasi').modal('show');
document.getElementById("lati").value = event.latLng.lat();
document.getElementById("longi").value = event.latLng.lng();
//alert(event.latLng.lat() + ", " + event.latLng.lng());
});

setMarkers(map2);
setMarkers2(map);
}


var nops = [
<?php
if(isset($data)){
  $no;
  foreach ($data as $row) {
?>
[<?php echo $row->NOP; ?>, <?php echo $row->LAT; ?> ,  <?php echo $row->LNG; ?>, <?php echo $no++; ?>],
<?php
  }
}
?>
];


    function setMarkers(map) {
      for (var i = 0; i < nops.length; i++) {
          var nop = nops[i];
          var marker = new google.maps.Marker({
            position: {lat: nop[1], lng: nop[2]},
            map: map,
            title: 'a',
            zIndex: nop[3]
          });
        }
      }

      
</script>

<script type="text/javascript">
  function formatnop(objek){
    
    a = objek.value;
    b = a.replace(/[^\d]/g,"");
    c = "";
       panjang = b.length;

       if (panjang <=2){
        // 35 -> 0,2
        c=b;
       }else if (panjang >2 && panjang <=4) {
          // 07. -> 2,2
        c=b.substr(0, 2)+'.'+b.substr(2, 2);
       }else if(panjang>4 && panjang <=7){
        // 123 -> 4,3
        c=b.substr(0, 2)+'.'+b.substr(2, 2)+'.'+b.substr(4, 3);
       }else if(panjang>7 && panjang <= 10){
        // .123. ->
        c=b.substr(0, 2)+'.'+b.substr(2, 2)+'.'+b.substr(4, 3)+'.'+b.substr(7,3);
       }else if(panjang>10 && panjang<=13 ){
        // 123.
        c=b.substr(0, 2)+'.'+b.substr(2, 2)+'.'+b.substr(4, 3)+'.'+b.substr(7, 3)+'.'+b.substr(10, 3);
       }else if(panjang> 13 && panjang<=17){
        // 1234
        c=b.substr(0, 2)+'.'+b.substr(2, 2)+'.'+b.substr(4, 3)+'.'+b.substr(7, 3)+'.'+b.substr(10, 3)+'.'+b.substr(13, 4);
       }else{
        // .0
        c=b.substr(0, 2)+'.'+b.substr(2, 2)+'.'+b.substr(4, 3)+'.'+b.substr(7, 3)+'.'+b.substr(10, 3)+'.'+b.substr(13, 4)+'.'+b.substr(17, 1);
        // alert(panjang);
       }
       objek.value=c;
  }

</script>
<script type="text/javascript">

          $(document).ready(function(){ 
              
              $("#KD_KECAMATAN").change(function(){ 
                
                
              
                $.ajax({
                  type: "POST",
                  url: "<?= base_url().'gis/getkelurahan'?>", 
                  data: {kd_kec : $("#KD_KECAMATAN").val()}, 
                  success: function(response){ 
                    $("#KD_KELURAHAN").html(response);
                  }

                });
              });
            });
        </script>

        <script type="text/javascript">

          $(document).ready(function(){ 
              
              $("#KD_KECAMATAN2").change(function(){ 
                
                
              
                $.ajax({
                  type: "POST",
                  url: "<?= base_url().'gis/getkelurahan'?>", 
                  data: {kd_kec : $("#KD_KECAMATAN2").val()}, 
                  success: function(response){ 
                    $("#KD_KELURAHAN2").html(response);
                  }

                });
              });
            });
        </script>

        <script type="text/javascript">

          $(document).ready(function(){ 
              
              $("#KD_KELURAHAN").change(function(){ 
                
                
              
                $.ajax({
                  type: "POST",
                  url: "<?= base_url().'gis/getblok'?>", 
                  data: {kd_kec : $("#KD_KECAMATAN").val(), kd_kel : $("#KD_KELURAHAN").val()}, 
                  success: function(response){ 
                    $("#KD_BLOK").html(response);
                  }

                });
              });
            });
        </script>



<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBlGqv80eU1XCSUnx_6s4Tvv3Nq0VLFFbI&callback=myMap"></script>

<!-- <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>
<script type="text/javascript">
//              menentukan koordinat titik tengah peta
              var myLatlng = new google.maps.LatLng(-6.176587,106.827115);
//              pengaturan zoom dan titik tengah peta
              var myOptions = {
                  zoom: 13,
                  center: myLatlng
              };
//              menampilkan output pada element
              var map = new google.maps.Map(document.getElementById("map"), myOptions);
//              menambahkan marker
              var marker = new google.maps.Marker({
                   position: myLatlng,
                   map: map,
                   title:"Monas"
              });
</script>

<script type="text/javascript">
//              menentukan koordinat titik tengah peta
              var myLatlng2 = new google.maps.LatLng(-6.176587,106.827115);
//              pengaturan zoom dan titik tengah peta
              var myOptions2 = {
                  zoom: 13,
                  center: myLatlng2
              };
//              menampilkan output pada element
              var map2 = new google.maps.Map(document.getElementById("map2"), myOptions2);
//              menambahkan marker
              var marker2 = new google.maps.Marker({
                   position: myLatlng2,
                   map: map2,
                   title:"Monas"
              });
</script> -->
  
<style type="text/css">
  .dua{
    width: 7%;
  }
  .tiga{
    width: 10%;
  }
  .empat{
    width: 12%;
  }
  .satu{
    width: 5%;
  }
</style>


