<section class="content-header">
          <h1>
          Monitoring Aktivitas Verlap
            <div class="pull-right">
      
            </div>
          </h1>
        </section>
        <section class="content">
          <div class="">
              <div class="col-md-3">
                   <div class="box box-solid">
                    <div class="box-body">
                  <table class="table table-bordered table-striped" id="example2">
                    <thead>
                        <tr>
                          <th width="20%">Username</th>
                          <th width="5%">Status</th>
                          <th width="5%">Lokasi</th>
                          <!-- <th width="15%">Nama</th>
                          <th width="10%">Tahun Pajak</th>
                          <th>Pokok</th>
                          <th>Denda</th>
                          <th>Jumlah</th>
                          <th width="15%">Tanggal Pembayaran</th> -->

                        </tr>
                    </thead>
                 <tbody>
                  <tr>
                      
                          
                       </tr>
                      <?php  $no=1; foreach($data as $a){?>
                       <tr>
                           <td align="left"><?php echo $a->USERNAME ?></td>
                           <td align="center"><?php if($a->STATUS=="1"){?> <font color="green">Online</font> <?php }else{ ?> <font color="grey"><i>Offline</i></font> <?php }?></td>
                           <td align="center"> <?php if($a->STATUS=="1"){?> <a href="<?php echo base_url('gis/monitoringVerlap?username=').$a->USERNAME?>"> <button type="button" class="btn btn-success" ><i class="fa fa-street-view"></i></button> </a>  <?php }else{ ?> <button type="button" class="btn btn-default" disabled=""><i class="fa fa-street-view"></i></button> <?php }?></td>
                          
                       </tr> 
                       <?php $no++; }?>
                   </tbody>
                </table> 
                    </div>
                    <!-- /.box-body -->
                  </div>
              </div>
              <div class="col-md-9">
                  <div class="box">
                       <div class="box-header with-border">
                          <h3 class="box-title">Lokasi Petugas <?php if(isset($nama_petugas)){ echo '"'.$nama_petugas.'"'; }?></h3>
                        
                           
                             <div class="box-tools">
                            <div class="mailbox-controls">
                        </div>
                          </div>
                          
                          <!-- /.box-tools -->
                        </div>
                      <div class="box-body">

                        <div class="mailbox-messages">
<div id="map" style="width: 100%; height: 400px;"></div>
                          
                        
            </div>
          </div>
              </div>
          </div>


        
        </section>
        <script>
function myMap() {
var mapProp= {
  //center:new google.maps.LatLng(-33.9,151.2),
   center:new google.maps.LatLng(-7.9977877,112.7341118),
  zoom:9,
};



var customLabel = {
        restaurant: {
          label: 'R'
        },
        bar: {
          label: 'B'
        }
      };

var map = new google.maps.Map(document.getElementById("map"),mapProp);

setMarkers(map);
}


var nops = [
<?php
if(isset($rk)){
  $no;
  foreach ($rk as $row) {
?>
['<?php echo $row->USERNAME; ?>', <?php echo $row->LATI; ?> ,  <?php echo $row->LONGI; ?>, <?php echo $no++; ?>],
<?php
  }
}
?>

      ];

    function setMarkers(map) {
      for (var i = 0; i < nops.length; i++) {
          var nop = nops[i];
          var marker = new google.maps.Marker({
            position: {lat: nop[1], lng: nop[2]},
            map: map,
            title: 'a',
            zIndex: nop[3]
          });
        }
      }
</script>

        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBlGqv80eU1XCSUnx_6s4Tvv3Nq0VLFFbI&callback=myMap"></script>




