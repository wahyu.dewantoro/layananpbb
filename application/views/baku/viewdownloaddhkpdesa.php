        <section class="content-header">
          <h1>
            <?= $judul ?>
            <div class="pull-right">

            </div>
          </h1>

        </section>
        <!-- Main content -->
        <section class="content">
          <!-- Default box -->
          <div class="row">
            <div class="col-md-12">
              <div class="box box-success">
                <div class="box-header with-border">
                  <h3 class="box-title">Form pencarian</h3>
                </div>
                <div class="box-body">
                  <form class="form-inline" action="<?= base_url() . 'baku/DownloadDhkpDesa' ?>" method="GET">
                    <div class="form-group">
                      Tanggal
                    </div>
                    <div class="form-group">
                      <input type="text" class="form-control tanggal" name="tgl1" value="<?php echo $tgl1 ?>" reuired placeholder="Tanggal mulai">
                    </div>
                    <button class="btn btn-sm btn-primary"><i class="fa fa-search"></i> Cari</button>
                    <!-- <button type="button" id="cetak" class="btn btn-sm btn-success"><i class="fa fa-print"></i> Cetak</button> -->
                  </form>
                </div><!-- /.box-body -->
              </div>
            </div>

            <?php if (!empty($data)) { ?>
              <div class="col-md-12">
                <div class="box box-primary">
                  <div class="box-body">
                    <!--
                       -->
                    <table class="table table-bordered table-striped" id="example3">
                      <thead>
                        <tr>
                          <th width="3%">No</th>
                          <th>Kecamatan</th>
                          <th>DHKP File</th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php $cc = 0;
                        $no = 1;
                        foreach ($data as $rk) { ?>
                          <?php if (file_exists(dirname(dirname(dirname(dirname(__FILE__)))) . "/" . $rk->FILE_DHKP)) { ?>
                            <tr>
                              <td align="center"><?= $no ?></td>
                              <td><?= $rk->NM_KECAMATAN ?></td>
                              <td align="center"><a target="popup" href="<?= base_url() ?><?= $rk->FILE_DHKP ?>" class="btn btn-xs btn-success"><i class="fa fa-download"></i></a></td>
                            </tr>
                        <?php $no++;
                          }
                        } ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            <?php } ?>
          </div>

          <!-- /.box -->
        </section><!-- /.content -->
        <script type="text/javascript">
          $(document).ready(function() {



            $("#KD_KECAMATAN").change(function() {
              $.ajax({
                type: "POST",
                url: "<?= base_url() . 'baku/getkelurahan' ?>",
                data: {
                  kd_kec: $("#KD_KECAMATAN").val()
                },
                success: function(response) {
                  $("#KD_KELURAHAN").html(response);
                }

              });
            });

            $("#cetak").click(function() {

              if ($("#KD_KECAMATAN").val() != '') {



                var base = "<?= base_url() ?>";

                var url = base + 'baku/cetakBakuDHKPdesa?TAHUN=' + $("#TAHUN").val() + '&KD_KECAMATAN=' + $("#KD_KECAMATAN").val() + '&KD_KELURAHAN=' + $("#KD_KELURAHAN").val() + '&BUKU=' + $("#BUKU").val();

                window.location = url;

              }
            });

          });
        </script>
        <script type="text/javascript">
          // $(document).ready(function() {
          //     $('#example-getting-started').multiselect();
          // });
          $(document).ready(function() {
            $('#BUKU').multiselect({
              includeSelectAllOption: true,
            });

          });
        </script>
        <script type="text/javascript" src="<?php echo base_url() . 'adminlte/' ?>bootstrap/js/bootstrap-multiselect.js"></script>