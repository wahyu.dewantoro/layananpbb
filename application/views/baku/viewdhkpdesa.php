        <section class="content-header">
          <h1>
          <?= $judul ?>
            <div class="pull-right">
             
          </div>
          </h1>
          
        </section>
        <!-- Main content -->
        <section class="content">
                    <!-- Default box -->
          <div class="row">
            <div class="col-md-12">
                  <div class="box box-success">
                     <div class="box-header with-border">
                        <h3 class="box-title">Form pencarian</h3>
                      </div>
                    <div class="box-body">
                      <form class="form-inline" action="<?= base_url().'baku/dhkpDesa'?>" method="GET" >
                          <div class="form-group">
                                  <label>Tahun Pajak <sup>*</sup></label>
                          </div>
                          <div class="form-group">
                                  <select class="form-control" required="" name="TAHUN" id="TAHUN">
                                    <?php for($q=date('Y');$q>2009;$q--){?>
                                      <option <?php if($q==$tahun){echo "selected";}?> value="<?= $q ?>"><?= $q ?></option>
                                    <?php } ?>
                                  </select>
                          </div>
                          <div class="form-group">
                            <label>Kecamatan <sup>*</sup></label>
                          </div>
                          <div class="form-group">
                            <select class="form-control" required="" name="KD_KECAMATAN" id="KD_KECAMATAN">
                              <option value="">Pilih</option>
                              <?php foreach($kec as $kec){?>

                                <option <?php if($kec->KD_KECAMATAN==$KD_KECAMATAN){echo "selected";}?> value="<?= $kec->KD_KECAMATAN ?>"><?=$kec->KD_KECAMATAN.' '. $kec->NM_KECAMATAN ?></option>

                                <!-- <option <?php if($kec->KD_KECAMATAN==$KD_KECAMATAN){echo "selected";}?> value="<?= $kec->KD_KECAMATAN ?>"><?= $kec->KD_KECAMATAN.' '.$kec->NM_KECAMATAN ?></option> -->

                              <?php } ?>
                            </select>
                          </div>
                          
                          <div class="form-group">
                            <label>Kelurahan / Desa <sup>*</sup></label>
                          </div>
                          <div class="form-group">
                            <select class="form-control" name="KD_KELURAHAN" id="KD_KELURAHAN">
                              <option value="">Pilih</option>
                              <?php 
                                foreach($kel as $kela){?>
                                  <option  <?php if($kela->KD_KELURAHAN==$KD_KELURAHAN){echo "selected";}?> value="<?= $kela->KD_KELURAHAN?>"><?= $kela->KD_KELURAHAN.' '. $kela->NM_KELURAHAN?></option>
                              <?php  }?>
                            </select>
                          </div>
                          <div class="form-group">
                            <label>Buku <sup>*</sup></label>
                          </div>
                          <div class="form-group">
                            <select class="form-control" name="BUKU[]" id="BUKU" multiple="multiple">
                            <option value="Buku I" <?php if($b1=='Buku I'){ ?> selected="selected" <?php };?> >Buku I</option>
                            <option value="Buku II" <?php if($b2=='Buku II'){ ?> selected="selected" <?php };?> >Buku II</option>
                            <option value="Buku III" <?php if($b3=='Buku III'){ ?> selected="selected" <?php };?> >Buku III</option>
                            <option value="Buku IV" <?php if($b4=='Buku IV'){ ?> selected="selected" <?php };?> >Buku IV</option>
                            <option value="Buku V" <?php if($b5=='Buku V'){ ?> selected="selected" <?php };?> >Buku V</option>
                            </select>
                          </div>
                          <button class="btn btn-sm btn-primary"><i class="fa fa-search"></i> Preview</button>
                          <button type="button" id="cetak" class="btn btn-sm btn-success"><i class="fa fa-print"></i> Cetak</button>
                      </form>
                    </div><!-- /.box-body -->
                  </div>        
                </div>
                
                <?php if(!empty($data)){?>
                <div class="col-md-12">
                  <div class="box box-primary">
                     <div class="box-header with-border">
                        <h3 class="box-title">Data SPPT</h3>
                        <div class="pull-right">
                            <a class="btn btn-sm btn-success" href="<?= base_url().'baku/cetakBakuDHKPdesa?CARI=1&TAHUN='.$tahun.'&KD_KECAMATAN='.$KD_KECAMATAN.'&KD_KELURAHAN='.$KD_KELURAHAN.'&BUKU='.$filter_buku?>"><i class="fa fa-file"></i> Excel</a>
                        </div>
                      </div>
                    <div class="box-body">
                      <!-- 
                       -->
                        <table class="table table-bordered table-striped" id="example3" >
                          <thead>
                            <tr>
                                <th rowspan="2">No</th>
                                
                                <th colspan="5">NOP</th>
                                <th colspan="5">Wajib Pajak</th>
                                <th rowspan="2">PBB Awal</th>
                                <th rowspan="2">PBB Akhir</th>
                                <th rowspan="2">Terbit SPPT</th>
                                <th rowspan="2">Lunas</th>
                                <th rowspan="2">Bayar SPPT</th>
                            </tr>
                            <tr>
                                
                                <th width="12%">NOP</th>
                                <th>Alamat</th>
                                <th>Rw/RT</th>
                                <th>Luas Bumi</th>
                                <th>Luas Bangunan</th>
                                <th>Nama</th>
                                <th>Alamat</th>
                                <th>RW / RT</th>
                                <th>Kelurahan</th>
                                <th>Kota</th>
                                
                            </tr>
                          </thead>
                          <tbody>
                            <?php $cc=0; $no=1; foreach($data as $rk){?>
                              <tr>
                                <td align="center"><?= $no ?></td>
                                <td><?= $rk->NOP ?></td>
                                <td><?= $rk->ALAMAT_OP ?></td>
                                <td><?= $rk->RWRT_OP ?></td>
                                <td align="center"><?= number_format($rk->LUAS_BUMI_SPPT,0,'','.' )?></td>
                                <td align="center"><?= number_format($rk->LUAS_BNG_SPPT,0,'','.' )?></td>
                                <td><?= $rk->NM_WP_SPPT ?></td>
                                <td><?= $rk->ALAMAT_WP ?></td>
                                <td><?= $rk->RWRT_WP ?></td>
                                <td><?= $rk->KELURAHAN_WP_SPPT ?></td>
                                <td><?= $rk->KOTA_WP_SPPT ?></td>
                                <td></td>
                                <td align="right"><?php $cc+=$rk->PBB; echo number_format($rk->PBB,0,'','.'); ?></td>
                                <td><?= $rk->TGL_TERBIT_SPPT ?></td>
                                <td><?php  if($rk->TGL_PEMBAYARAN_SPPT==''){echo "belum";}else{echo "Lunas";} ?></td>
                                <td><?= $rk->TGL_PEMBAYARAN_SPPT ?></td>
                              </tr>
                            <?php $no++;} ?>
                          </tbody>
                          <tfoot>
                            <tr>
                                
                                <td colspan="12"><b>Jumlah</b></td>
                                <td align="right"><b><?php echo number_format($cc,0,'','.'); ?></b></td>
                                <td colspan="3"></td>
                            </tr>
                          </tfoot>
                        </table>
                    </div>
                  </div>
                </div>
              <?php } ?>
          </div>

          <!-- /.box -->
        </section><!-- /.content -->
        <script type="text/javascript">
          $(document).ready(function(){ 
              
            

              $("#KD_KECAMATAN").change(function(){ 
                $.ajax({
                  type: "POST",
                  url: "<?= base_url().'baku/getkelurahan'?>", 
                  data: {kd_kec : $("#KD_KECAMATAN").val()}, 
                  success: function(response){ 
                    $("#KD_KELURAHAN").html(response);
                  }

                });
              });

              $( "#cetak" ).click(function() {

              if($("#KD_KECAMATAN").val()!=''){

                

                var base="<?= base_url()?>";

                var url=base+'baku/cetakBakuDHKPdesa?TAHUN='+$("#TAHUN").val()+'&KD_KECAMATAN='+$("#KD_KECAMATAN").val()+'&KD_KELURAHAN='+$("#KD_KELURAHAN").val()+'&BUKU='+$("#BUKU").val();

                window.location = url;

              }
            });

            });
        </script>
        <script type="text/javascript">
    // $(document).ready(function() {
    //     $('#example-getting-started').multiselect();
    // });
    $(document).ready(function() {
        $('#BUKU').multiselect({
            includeSelectAllOption: true,
        });
        
    });
</script>
<script type="text/javascript" src="<?php echo base_url().'adminlte/'?>bootstrap/js/bootstrap-multiselect.js"></script>