        <section class="content-header">
          <h1>
            <?= $judul ?>
            <div class="pull-right">
              <a class="btn btn-sm btn-success" href="<?= base_url() . 'baku/cetakRingkasanbaku?TAHUN=' . $tahun . '&KD_KECAMATAN=' . $KD_KECAMATAN . '&KD_KELURAHAN=' . $KD_KELURAHAN ?>"><i class="fa fa-file"></i> Excel</a>
            </div>
          </h1>

        </section>
        <!-- Main content -->
        <section class="content">
          <!-- Default box -->
          <div class="row">
            <div class="col-md-12">
              <div class="box box-success">
                <div class="box-header with-border">
                  <h3 class="box-title">Form pencarian</h3>
                </div>
                <div class="box-body">
                  <form action="<?= base_url() . 'baku/ringkasan' ?>" method="GET" class="form-inline">
                    <div class="form-group">
                      <!-- <label>Tahun Pajak <sup>*</sup></label> -->
                      <select class="form-control" required="" name="TAHUN" id="TAHUN">
                        <?php for ($q = date('Y'); $q > 2009; $q--) { ?>
                          <option <?php if ($q == $tahun) {
                                    echo "selected";
                                  } ?> value="<?= $q ?>"><?= $q ?></option>
                        <?php } ?>
                      </select>
                    </div>
                    <div class="form-group">
                      <!-- <label>Kecamatan <sup>*</sup></label> -->
                      <select class="form-control" required="" name="KD_KECAMATAN" id="KD_KECAMATAN">
                        <option value="">Pilih</option>
                        <?php foreach ($kec as $kec) { ?>
                          <option <?php if ($kec->KD_KECAMATAN == $KD_KECAMATAN) {
                                    echo "selected";
                                  } ?> value="<?= $kec->KD_KECAMATAN ?>"><?= $kec->KD_KECAMATAN . ' ' . $kec->NM_KECAMATAN ?></option>
                        <?php } ?>
                      </select>
                    </div>

                    <div class="form-group">
                      <!-- <label>Kelurahan / Desa <sup>*</sup></label> -->
                      <select class="form-control" name="KD_KELURAHAN" id="KD_KELURAHAN">
                        <option value="">Pilih</option>
                        <?php
                        foreach ($kel as $kela) { ?>
                          <option <?php if ($kela->KD_KELURAHAN == $KD_KELURAHAN) {
                                    echo "selected";
                                  } ?> value="<?= $kela->KD_KELURAHAN . ' ' . $kela->KD_KELURAHAN ?>"><?= $kela->NM_KELURAHAN ?></option>
                        <?php  } ?>
                      </select>
                    </div>
                    <button class="btn btn-sm btn-success"><i class="fa fa-search"></i> Cari</button>
                  </form>
                </div><!-- /.box-body -->
              </div>
            </div>
            <div class="col-md-12">

              <div class="box box-success">
                <div class="box-body">
                  <table class="table table-bordered table-striped" id="example3">
                    <thead>
                      <tr>
                        <th>No</th>
                        <th>Kecamatan</th>
                        <th>kelurahan</th>
                        <th>Buku</th>
                        <th>Jumlah Baku</th>
                        <th>Total Baku</th>
                        <th>Jumlah Realisasi</th>
                        <th>Total Realisasi</th>
                        <th>Jumlah Piutang</th>
                        <th>Total Piutang</th>

                      </tr>
                    </thead>
                    <tbody>
                      <?php
                      $a = 0;
                      $b = 0;
                      $c = 0;
                      $d = 0;
                      $e = 0;
                      $f = 0;
                      $no = 1;
                      foreach ($data as $rk) { ?>
                        <tr>
                          <td align="center"><?= $no ?></td>
                          <td> <?= $rk->NAMA_KEC ?></td>
                          <td> <?= $rk->NM_KEL ?></td>
                          <td> <?= $rk->BUKU ?></td>
                          <td align="center"> <?php if (!empty($rk->SPPT_BAKU)) {
                                                echo number_format($rk->SPPT_BAKU, '0', '', '.');
                                              } else {
                                                echo "-";
                                              }
                                              $a += $rk->SPPT_BAKU;  ?></td>
                          <td align="right"> <?php if (!empty($rk->JUMLAH_BAKU)) {
                                                echo number_format($rk->JUMLAH_BAKU, '0', '', '.');
                                              } else {
                                                echo "-";
                                              }
                                              $b += $rk->JUMLAH_BAKU;  ?></td>
                          <td align="center"> <?php if (!empty($rk->SPPT_REALISASI)) {
                                                echo number_format($rk->SPPT_REALISASI, '0', '', '.');
                                              } else {
                                                echo "-";
                                              }
                                              $c += $rk->SPPT_REALISASI;  ?></td>
                          <td align="right"> <?php if (!empty($rk->JUMLAH_REALISASI)) {
                                                echo number_format($rk->JUMLAH_REALISASI, '0', '', '.');
                                              } else {
                                                echo "-";
                                              }
                                              $d += $rk->JUMLAH_REALISASI;  ?></td>
                          <td align="center"> <?php if (!empty($rk->SPPT_PIUTANG)) {
                                                echo number_format($rk->SPPT_PIUTANG, '0', '', '.');
                                              } else {
                                                echo "-";
                                              }
                                              $e += $rk->SPPT_PIUTANG;  ?></td>
                          <td align="right"> <?php if (!empty($rk->JUMLAH_PIUTANG)) {
                                                echo number_format($rk->JUMLAH_PIUTANG, '0', '', '.');
                                              } else {
                                                echo "-";
                                              }
                                              $f += $rk->JUMLAH_PIUTANG;  ?></td>
                        </tr>
                      <?php $no++;
                      } ?>
                    </tbody>

                    <tfoot>
                      <tr>
                        <th colspan="4">Jumlah</th>
                        <td align="right"><b><?= number_format($a, 2, ',', '.'); ?></b></td>
                        <td align="right"><b><?= number_format($b, 2, ',', '.'); ?></b></td>
                        <td align="right"><b><?= number_format($c, 2, ',', '.'); ?></b></td>
                        <td align="right"><b><?= number_format($d, 2, ',', '.'); ?></b></td>
                        <td align="right"><b><?= number_format($e, 2, ',', '.'); ?></b></td>
                        <td align="right"><b><?= number_format($f, 2, ',', '.'); ?></b></td>
                      </tr>
                    </tfoot>
                  </table>
                </div><!-- /.box-body -->
              </div><!-- /.box -->

            </div>
          </div>

        </section><!-- /.content -->
        <script type="text/javascript">
          $(document).ready(function() {

            $("#KD_KECAMATAN").change(function() {



              $.ajax({
                type: "POST",
                url: "<?= base_url() . 'baku/getkelurahan' ?>",
                data: {
                  kd_kec: $("#KD_KECAMATAN").val()
                },
                success: function(response) {
                  $("#KD_KELURAHAN").html(response);
                }

              });
            });
          });
        </script>