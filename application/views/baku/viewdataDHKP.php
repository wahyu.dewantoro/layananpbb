        <section class="content-header">
          <h1>
          <?= $judul ?>
            <div class="pull-right">
             <a class="btn btn-sm btn-success" href="<?= base_url().'baku/cetakBakuDHKP?TAHUN='.$tahun.'&KD_KECAMATAN='.$KD_KECAMATAN.'&KD_KELURAHAN='.$KD_KELURAHAN ?>"><i class="fa fa-file"></i> Excel</a>
          </div>
          </h1>
          
        </section>
        <!-- Main content -->
        <section class="content">
            <div class="row">
                <div class="col-md-12">
                        <div class="box box-success">
                           <div class="box-header with-border">
                              <h3 class="box-title">Form pencarian</h3>
                            </div>
                          <div class="box-body">
                            <form action="<?= base_url().'baku/dataDHKP'?>" method="GET" class="form-inline">
                                 <div class="form-group">
                                  <!-- <label>Tahun Pajak <sup>*</sup></label> -->
                                  <select class="form-control" required="" name="TAHUN" id="TAHUN">
                                    <?php for($q=date('Y');$q>2009;$q--){?>
                                      <option <?php if($q==$tahun){echo "selected";}?> value="<?= $q ?>"><?= $q ?></option>
                                    <?php } ?>
                                  </select>
                                </div>
                                <div class="form-group">
                                  <!-- <label>Kecamatan <sup>*</sup></label> -->
                                  <select class="form-control" required="" name="KD_KECAMATAN" id="KD_KECAMATAN">
                                    <option value="">Pilih</option>
                                    <?php foreach($kec as $kec){?>
                                      <option <?php if($kec->KD_KECAMATAN==$KD_KECAMATAN){echo "selected";}?> value="<?= $kec->KD_KECAMATAN ?>"><?= $kec->KD_KECAMATAN.' '.$kec->NM_KECAMATAN ?></option>
                                    <?php } ?>
                                  </select>
                                </div>
                                
                                <div class="form-group">
                                  <!-- <label>Kelurahan / Desa <sup>*</sup></label> -->
                                  <select class="form-control"  name="KD_KELURAHAN" id="KD_KELURAHAN">
                                    <option value="">Pilih</option>
                                    <?php 
                                      foreach($kel as $kela){?>
                                        <option  <?php if($kela->KD_KELURAHAN==$KD_KELURAHAN){echo "selected";}?> value="<?= $kela->KD_KELURAHAN?>"><?=$kela->KD_KELURAHAN.' '. $kela->NM_KELURAHAN?></option>
                                    <?php  }?>
                                  </select>
                                </div>
                                <button class="btn btn-sm btn-success"><i class="fa fa-search"></i> Cari</button>
                            </form>
                          </div><!-- /.box-body -->
                        </div>        
                      </div>
                      <div class="col-md-12">
          <div class="box box-success">
            <div class="box-body">
                <table class="table table-bordered table-striped" id="example3" >
                      <thead>
                          <tr>
                             <th>No</th>
                             <th>NOP</th>
                             <th>Alamat OP</th>
                             <th>RW/RT OP</th>
                             <th>Luas Bumi</th>
                             <th>Luas Bangunan</th>
                             <th>Nama wp</th>
                             <th>Alamat wp</th>
                             <th>RW/RT WP</th>
                             <th>Kelurahan WP</th>
                             <th>Kota WP</th>
                             <th>PBB</th>
                             <th>Terbit SPPT</th>
                             <th>Pembayaran SPPT</th>

                          </tr>
                      </thead>
                      <tbody>
                        <?php $cv=0; $no=1; foreach($data as $rk){?>
                          <tr>
                            <td align="center"><?= $no ?></td>
                            <td><?= $rk->NOP ?></td>
                            <td><?= $rk->ALAMAT_OP ?></td>
                            <td><?= $rk->RWRT_OP ?></td>
                            <td align="center"><?= number_format($rk->LUAS_BUMI_SPPT,0,'','.') ?> M<sup>2</sup></td>
                            <td align="center"><?= number_format($rk->LUAS_BNG_SPPT,0,'','.') ?> M<sup>2</sup></td>
                            <td><?= $rk->NM_WP_SPPT ?></td>
                            <td><?= $rk->ALAMAT_WP ?></td>
                            <td><?= $rk->RWRT_WP ?></td>
                            <td><?= $rk->KELURAHAN_WP_SPPT ?></td>
                            <td><?= $rk->KOTA_WP_SPPT ?></td>
                            <td align="right"><?php $cv+=$rk->PBB; echo number_format($rk->PBB,0,'','.') ?></td>
                            <td align="center"><?= $rk->TGL_TERBIT_SPPT ?></td>
                            <td align="center"><?= $rk->TGL_PEMBAYARAN_SPPT ?></td>
                          </tr>
                        <?php  $no++; } ?>
                      </tbody>
                      <thead>
                        <tr>
                            
                            <td colspan="11"><b>Jumlah</b></td>
                            <td align="right"><b><?php echo number_format($cv,0,'','.') ?></b></td>
                            <td colspan="2"></td>
                            
                          </tr>
                      </thead>
                  </table>
            </div><!-- /.box-body -->
          </div><!-- /.box -->
        </div>
        </section><!-- /.content -->
            <script type="text/javascript">
          $(document).ready(function(){ 
              
              $("#KD_KECAMATAN").change(function(){ 
                
                
              
                $.ajax({
                  type: "POST",
                  url: "<?= base_url().'baku/getkelurahan'?>", 
                  data: {kd_kec : $("#KD_KECAMATAN").val()}, 
                  success: function(response){ 
                    $("#KD_KELURAHAN").html(response);
                  }

                });
              });
            });
        </script>