<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>Layanan PBB</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- Bootstrap 3.3.2 -->
    <link href="<?php echo base_url().'adminlte/'?>bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />

    <link href="<?php echo base_url().'adminlte/'?>fontawesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <!-- <link href="<?php echo base_url().'adminlte/'?>ionicons.min.css" rel="stylesheet" type="text/css" /> -->
    <link href="<?php echo base_url().'adminlte/'?>plugins/datatables/dataTables.bootstrap.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url().'adminlte/'?>plugins/datatables/buttons.dataTables.min.css" rel="stylesheet" type="text/css" />


    <link href="<?php echo base_url().'adminlte/'?>plugins/chosen/chosen.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url().'adminlte/'?>dist/css/AdminLTE.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url().'adminlte/'?>dist/css/skins/_all-skins.min.css" rel="stylesheet" type="text/css" />
      <link href="<?php echo base_url().'adminlte/'?>plugins/iCheck/all.css" rel="stylesheet" type="text/css" />
      <link href="<?php echo base_url().'adminlte/'?>plugins/datepicker/datepicker3.css" rel="stylesheet" type="text/css" />
      <link href="<?php echo base_url('assets/jquery-ui.css')?>" rel="stylesheet" type="text/css" />
       <!-- Morris charts -->
    <!-- <link href="<?php echo base_url().'adminlte/'?>plugins/morris/morris.css" rel="stylesheet" type="text/css" /> -->
    <!-- jQuery 2.1.3 -->
    <!--<script src="<?php echo base_url().'adminlte/'?>plugins/jQuery/jQuery-2.1.3.min.js"></script>-->
  
    <!--<script src="<?php echo base_url('assets/js/jquery-1.11.2.min.js') ?>"></script>-->
        <script src="<?php echo base_url('assets/js/jquery-1.12.4.js')?>"></script>
        <script src="<?php echo base_url('assets/js/jquery-ui.js')?>"></script>


    <!-- Bootstrap 3.3.2 JS -->
    <script src="<?php echo base_url().'adminlte/'?>bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <!-- SlimScroll -->
    <script src="<?php echo base_url().'adminlte/'?>plugins/slimScroll/jquery.slimScroll.min.js" type="text/javascript"></script>
    
    <!--<script src="<?php echo base_url('assets/datatables/jquery.dataTables.js') ?>"></script>-->
    <script src="<?php echo base_url('assets/datatables/jquery.dataTables.min.js')?>"></script>
    
     
    <script src="<?php echo base_url().'adminlte/'?>plugins/datatables/dataTables.bootstrap.js" type="text/javascript"></script>
    
    
    <!--<script src="<?php echo base_url().'adminlte/'?>plugins/datatables/dataTables.buttons.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url().'adminlte/'?>plugins/datatables/buttons.flash.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url().'adminlte/'?>plugins/datatables/jszip.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url().'adminlte/'?>plugins/datatables/pdfmake.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url().'adminlte/'?>plugins/datatables/vfs_fonts.js" type="text/javascript"></script>
    <script src="<?php echo base_url().'adminlte/'?>plugins/datatables/buttons.html5.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url().'adminlte/'?>plugins/datatables/buttons.print.min.js" type="text/javascript"></script>-->


    <script src="<?php echo base_url().'adminlte/'?>plugins/chosen/chosen.jquery.js" type="text/javascript"></script>
    <script src="<?php echo base_url().'adminlte/'?>plugins/datepicker/bootstrap-datepicker.js" type="text/javascript"></script>


    <!-- Morris.js charts -->
    <script src="http://cdnjs.cloudflare.com/ajax/libs/raphael/2.1.0/raphael-min.js"></script>
    <!--<script src="<?php echo base_url().'adminlte/'?>plugins/morris/morris.min.js" type="text/javascript"></script>-->

    
    <!-- FastClick 

    <script src='<?php echo base_url().'adminlte/'?>plugins/fastclick/fastclick.min.js'></script>-->
    <!-- AdminLTE App -->
    <script src="<?php echo base_url().'adminlte/'?>dist/js/app.min.js" type="text/javascript"></script>
    <script src="<?php echo base_url().'adminlte/'?>jquery.inputmask/dist/min/jquery.inputmask.bundle.min.js" type="text/javascript"></script>
     <script type="text/javascript">
      $(function() {
        
        $('.chosen').chosen({ allow_single_deselect: true });
         // $( ".tanggal" ).datepicker();
           $('.tanggal').datepicker().on('change', function(){
                $('.datepicker').hide();

            });
      });

      $(function () {
        $("#example1").dataTable();
        $('#example3').dataTable({
          "bPaginate": true,
          "pageLength": 20,
          "searching": false,
          "bLengthChange": false,
          "bFilter": false,
          "bSort": true,
          "bInfo": true,
          "bAutoWidth": false,
            'oLanguage':
                    {
                      "sProcessing":   "Sedang memproses...",
                      "sLengthMenu":   "Tampilkan _MENU_ entri",
                      "sZeroRecords":  "Tidak ditemukan data yang sesuai",
                      "sInfo":         "Menampilkan _START_ sampai _END_ dari _TOTAL_ entri",
                      "sInfoEmpty":    "Menampilkan 0 sampai 0 dari 0 entri",
                      "sInfoFiltered": "(disaring dari _MAX_ entri keseluruhan)",
                      "sInfoPostFix":  "",
                      "sSearch":       "Cari:",
                      "sUrl":          "",
                      "oPaginate": {
                        "sFirst":    "Pertama",
                        "sPrevious": "Sebelumnya",
                        "sNext":     "Selanjutnya",
                        "sLast":     "Terakhir"
                      }
                    }
        });
        
         $('#example4').DataTable( {
           "pageLength": 20,
           "searching": false,
           "scrollX": true,
           "bPaginate": true,
           "bLengthChange": false,
           "bFilter": true,
           "bSort": true,
           "bInfo": true,
           "bAutoWidth": false,
            'oLanguage':
                    {
                      "sProcessing":   "Sedang memproses...",
                      "sLengthMenu":   "Tampilkan _MENU_ entri",
                      "sZeroRecords":  "Tidak ditemukan data yang sesuai",
                      "sInfo":         "Menampilkan _START_ sampai _END_ dari _TOTAL_ entri",
                      "sInfoEmpty":    "Menampilkan 0 sampai 0 dari 0 entri",
                      "sInfoFiltered": "(disaring dari _MAX_ entri keseluruhan)",
                      "sInfoPostFix":  "",
                      "sSearch":       "Cari:",
                      "sUrl":          "",
                      "oPaginate": {
                        "sFirst":    "Pertama",
                        "sPrevious": "Sebelumnya",
                        "sNext":     "Selanjutnya",
                        "sLast":     "Terakhir"
                      }
                    },
                dom: 'Bftip',
              buttons: [
                  'copy', 'csv', 'excel',
              ]
          } );

      });

      

$(function() {
  $('.sidebar-menu a[href~="' + location.href + '"]').parents('li').addClass('active');
});

 
    </script>
    <style type="text/css">
    
.dataTables_processing {
                position: absolute;
                top: 50%;
                left: 50%;
                width: 100%;
                margin-left: -50%;
                margin-top: -25px;
                padding-top: 20px;
                text-align: center;
                font-size: 1.2em;
                color:grey;
            }
    </style>
  </head>
  <body class="hold-transition skin-blue layout-top-nav fixed">
    <!-- Site wrapper -->
    <div class="wrapper">
      
      <header class="main-header">
    <nav class="navbar navbar-static-top">
      <div class="container">
        <div class="navbar-header">
          <a href="#" class="logo"><img   height="80%" src="<?php echo base_url().'adminlte/logo.png'?>"><b>Layanan</b> PBB</a>
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar-collapse">
            <i class="fa fa-bars"></i>
          </button>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse pull-left" id="navbar-collapse">
          <ul class="nav navbar-nav">
            <?php 
                      $kode_group=$this->session->userdata('pbb_kg');
                      $qmenu0  =$this->db->query("SELECT a.*
                                                  FROM p_menu a
                                                  JOIN p_role b ON a.KODE_MENU=b.KODE_MENU
                                                  WHERE kode_group='$kode_group' AND status_role='1'  AND active='1' and parent_menu='0' order by sort_menu asc")->result_array();
                      foreach ($qmenu0 as $row0) {
                        $parent  =$row0['KODE_MENU'];
                        $qmenu1  =$this->db->query("SELECT a.*
                                                  FROM p_menu a
                                                  JOIN p_role b ON a.KODE_MENU=b.KODE_MENU
                                                  WHERE kode_group='$kode_group' AND status_role='1'  AND active='1' and parent_menu='$parent' order by sort_menu asc");
                        $cekmenu =$qmenu1->num_rows();
                        $dmenu1  =$qmenu1->result_array();
                          if($cekmenu>0){
                            echo "<li class='dropdown'>
                              <a href='".$row0['LINK_MENU']."' > <i class='".$row0['ICON_MENU']."'></i>  <span>".ucwords($row0['NAMA_MENU'])."</span><i class='caret'></i> </a>";
                                echo "<ul class='dropdown-menu' role='menu'>";
                                  foreach($dmenu1 as $row1){
                                      echo "<li>".anchor($row1['LINK_MENU'],''.ucwords($row1['NAMA_MENU']))."</li>";
                                  }
                              echo "</ul>
                              </li>";
                              }else{
                               echo "<li>".anchor($row0['LINK_MENU']," <i class='".$row0['ICON_MENU']."'></i> <span>".ucwords($row0['NAMA_MENU']))."</span></li>";
                            }
                          }
                        ?>
          </ul>
        </div>
        <!-- /.navbar-collapse -->
        <!-- Navbar Right Menu -->
        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
              <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  <!-- <img src="<?php echo base_url().'adminlte/'?>dist/img/user2-160x160.jpg" class="user-image" alt="User Image"/> -->
                  <span class="hidden-xs">
                      <?php echo $this->session->userdata('pbb_nama')?>
                      <small><?php echo '[ '.$this->session->userdata('seksi').' - '.$this->session->userdata('pbb_ng').' ]'?></small>
                    </span>
                  <i class="fa fa-caret-down"></i>
                </a>
                <ul class="dropdown-menu">
                  <!-- User image -->
                  
                  <!-- Menu Body -->
              
                  <!-- Menu Footer-->
                  <li class="user-footer">
                    
                    <div class="pull-right">
                      <a href="<?php echo base_url().'auth/logout'?>" class="btn btn-danger btn-flat">Keluar</a>
                    </div>
                  </li>
                </ul>
              </li>
            </ul>
          </div>
        <!-- /.navbar-custom-menu -->
      </div>
      <!-- /.container-fluid -->
    </nav>
  </header>

      <!-- =============================================== -->

      <!-- Left side column. contains the sidebar -->

      <!-- =============================================== -->

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <div class="container">
          <?php echo $contents; ?>      
        </div>
        <!-- Content Header (Page header) -->
        

      </div><!-- /.content-wrapper -->

      <footer class="main-footer">
        <div class="pull-right hidden-xs">
          <b>Version</b> 1.0
        </div>
        <strong>Copyright &copy; 2017 DNH  Solution.</strong> All rights reserved.
      </footer>
    </div><!-- ./wrapper -->

    
  </body>
</html>
<script type="text/javascript">
$('ul.nav li.dropdown').hover(function() {
  $(this).find('.dropdown-menu').stop(true, true).delay(100).fadeIn(0);
}, function() {
  $(this).find('.dropdown-menu').stop(false, true).delay(100).fadeOut(500);
});
</script>

<script type="text/javascript">
  $(document).ready(function() {
        $(":input").inputmask();
    });
</script>
