<form method="post" action="<?php echo base_url() . 'sistem/do_role' ?>">
  <section class="content-header">
    <h1>
      Privilege
      <div class="pull-right">
        <div class="btn-group">
          <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Simpan</button> <?php echo anchor('sistem/privilege', '<i class="fa fa-list"></i> List', ' class="btn btn-success"'); ?>
        </div>

      </div>
    </h1>

  </section>
  <!-- Main content -->
  <section class="content">
    <!-- Default box -->
    <div class="box">

      <div class="box-body">
        <input type="hidden" name="kode_group" value="<?php echo $kode_groupq; ?>">
        <table class='table  table-bordered table-hover'>
          <thead>
            <tr>
              <th width="4%">No</th>

              <th colspan="2">Menu</th>
              <th>Parent</th>
            </tr>
          </thead>
          <tbody>
            <?php $no = 1;
            foreach ($role as $row) {
              $st = $no % 2;
            ?>
              <tr>
                <td align="center"><?php echo $no; ?></td>
                <td width="3%" align="center"><input <?php if ($row['STATUS_ROLE'] == 1) {
                                                        echo "checked";
                                                      } ?> type="checkbox" name="role[]" value="<?php echo $row['KODE_ROLE']; ?>"></td>
                <td><?php echo ucwords($row['NAMA_MENU']); ?></td>
                <td><?php echo ucwords($row['PARENT']); ?></td>
              </tr>
            <?php $no++;
            } ?>
          </tbody>
        </table>
      </div><!-- /.box-body -->
    </div><!-- /.box -->
  </section><!-- /.content -->



</form>