<section class="content-header">
  <h1>
    Data SPOP / LSPOP
  </h1>
</section>
<section class="content">
  <div class="box box-primary">
    <div class="box-body">
      <div class="row">
        <div class="col-md-8">
          <form class="form-inline" method="GET" role="form" action="<?= base_url() . 'spop/dataSpop' ?>">
            <div class="form-group">
              <input type="text" value="<?= $nop ?>" class="form-control" id="NOP" placeholder="Masukan NOP" onkeyup="formatnop(this)" autofocus="true" name="NOP">
            </div>
            <button type="submit" class="btn btn-default">Cari</button>
          </form>
        </div>
      </div>
      <div class="row">



        <div class="col-md-12">
          <!-- Nav tabs -->
          <ul class="nav nav-tabs">
            <li class="active"><a href="#home" data-toggle="tab">SPOP</a></li>
            <li><a href="#profile" data-toggle="tab">LSPOP</a></li>

          </ul>

          <!-- Tab panes -->
          <div class="tab-content">
            <div class="tab-pane active" id="home">



              <?php if (!empty($res)) { ?>
                <div class="row">
                  <div class="col-md-6">
                    <table class="table">
                      <tr>
                        <td width="20%">No KTP</td>
                        <td width="1%">:</td>
                        <td><?= $res->SUBJEK_PAJAK_ID ?></td>
                      </tr>
                      <tr>
                        <td width="20%">Pekerjaan</td>
                        <td width="1%">:</td>
                        <td><?= $res->STATUS_PEKERJAAN_WP ?></td>
                      </tr>
                      <tr>
                        <td width="20%">NPWP</td>
                        <td width="1%">:</td>
                        <td><?= $res->NPWP ?></td>
                      </tr>
                      <tr>
                        <td width="20%">Jalan</td>
                        <td width="1%">:</td>
                        <td><?= $res->JALAN_WP ?></td>
                      </tr>
                      <tr>
                        <td width="20%">RW/RT</td>
                        <td width="1%">:</td>
                        <td><?= $res->RW_WP . ' / ' . $res->RT_WP ?> </td>
                      </tr>
                      <tr>
                        <td width="20%">Kota / Dati II </td>
                        <td width="1%">:</td>
                        <td><?= $res->KOTA_WP ?></td>
                      </tr>
                    </table>
                  </div>
                  <div class="col-md-6">
                    <table class="table">
                      <tr>
                        <td width="20%">Status WP</td>
                        <td width="1%">:</td>
                        <td><?= $res->KD_STATUS_WP ?></td>
                      </tr>
                      <tr>
                        <td width="20%">Nama</td>
                        <td width="1%">:</td>
                        <td><?= $res->NM_WP ?></td>
                      </tr>
                      <tr>
                        <td width="20%">No Telp</td>
                        <td width="1%">:</td>
                        <td><?= $res->TELP_WP ?></td>
                      </tr>
                      <tr>
                        <td width="20%">Blok / Kav / No</td>
                        <td width="1%">:</td>
                        <td><?= $res->BLOK_KAV_NO_WP ?></td>
                      </tr>
                      <tr>
                        <td width="20%">Kelurahan</td>
                        <td width="1%">:</td>
                        <td><?= $res->KELURAHAN_WP ?></td>
                      </tr>
                      <tr>
                        <td width="20%">kode pos</td>
                        <td width="1%">:</td>
                        <td><?= $res->KD_POS_WP ?></td>
                      </tr>
                    </table>
                  </div>

                  <div class="col-md-6">
                    <table class="table">
                      <tr>
                        <td width="30%">No Persil</td>
                        <td width="1%">:</td>
                        <td><?= $res->NO_PERSIL ?></td>
                      </tr>
                      <tr>
                        <td width="30%">Jalan. Blok / Kav / No</td>
                        <td width="1%">:</td>
                        <td><?= $res->JALAN_OP . ' ' . $res->BLOK_KAV_NO_OP ?></td>
                      </tr>
                      <tr>
                        <td width="30%">RW / RT</td>
                        <td width="1%">:</td>
                        <td><?= $res->RW_WP . ' / ' . $res->RT_WP ?></td>
                      </tr>
                      <tr>
                        <td width="30%">Luas Tanah</td>
                        <td width="1%">:</td>
                        <td><?= $res->LUAS_BUMI ?> M<sup>2</sup></td>
                      </tr>
                      <tr>
                        <td width="30%">ZNT</td>
                        <td width="1%">:</td>
                        <td><?= $res->KD_ZNT ?></td>
                      </tr>
                      <tr>
                        <td width="30%">Jenis Tanah</td>
                        <td width="1%">:</td>
                        <td><?= $res->JNS_BUMI ?></td>
                      </tr>
                    </table>
                  </div>
                  <div class="col-md-6">
                    <table class="table">
                      <tr>
                        <td width="30%">Tanggal Pendataan</td>
                        <td width="1%">:</td>
                        <td><?= $res->TGL_PENDATAAN_OP ?></td>
                      <tr>
                        <td width="30%">NIP Pendata</td>
                        <td width="1%">:</td>
                        <td><?= $res->NIP_PENDATA ?></td>
                      <tr>
                        <td width="30%">Tanggal Penelitian</td>
                        <td width="1%">:</td>
                        <td><?= $res->TGL_PEMERIKSAAN_OP ?></td>
                      <tr>
                        <td width="30%">NIP peneliti</td>
                        <td width="1%">:</td>
                        <td><?= $res->NIP_PEMERIKSA_OP ?></td>
                    </table>
                  </div>
                </div>
              <?php } ?>
            </div>
            <div class="tab-pane" id="profile">
              <div class="row">
                <div class="col-md-6">
                  <table class="table">
                    <tr>
                      <td width="30%">Jenis Bangunan</td>
                      <td width="1%">:</td>
                      <td><?= $ls->KD_JPB ?></td>
                    </tr>
                    <tr>
                      <td width="30%">Luas Bangunan</td>
                      <td width="1%">:</td>
                      <td><?= $ls->LUAS_BNG ?></td>
                    </tr>
                    <tr>
                      <td width="30%">Tahun dibangun</td>
                      <td width="1%">:</td>
                      <td><?= $ls->THN_DIBANGUN_BNG ?></td>
                    </tr>
                    <tr>
                      <td width="30%">Tahun Renovasi</td>
                      <td width="1%">:</td>
                      <td><?= $ls->THN_RENOVASI_BNG ?></td>
                    </tr>
                    <tr>
                      <td width="30%">Kodnisi Bangunan</td>
                      <td width="1%">:</td>
                      <td><?= $ls->KONDISI_BNG ?></td>
                    </tr>
                  </table>
                </div>
                <div class="col-md-6">
                  <table class="table">
                    <tr>
                      <td width="30%">kontruksi</td>
                      <td width="1%">:</td>
                      <td><?= $ls->JNS_KONSTRUKSI_BNG ?></td>
                    </tr>
                    <tr>
                      <td width="30%">atap</td>
                      <td width="1%">:</td>
                      <td><?= $ls->JNS_ATAP_BNG ?></td>
                    </tr>
                    <tr>
                      <td width="30%">dinding</td>
                      <td width="1%">:</td>
                      <td><?= $ls->KD_DINDING ?></td>
                    </tr>
                    <tr>
                      <td width="30%">lantai</td>
                      <td width="1%">:</td>
                      <td><?= $ls->KD_LANTAI ?></td>
                    </tr>
                    <tr>
                      <td width="30%">langit - langit </td>
                      <td width="1%">:</td>
                      <td><?= $ls->KD_LANGIT_LANGIT ?></td>
                    </tr>
                  </table>

                </div>
                <div class="col-md-6">
                  <table class="table">
                    <tr>
                      <td width="20%">Daya listrik (watt)</td>
                      <td width="1%">: <?= $ls->DAYA_LISTRIK ?></td>
                      <td></td>
                    </tr>
                  </table>
                  <table class="table table-bordered">
                    <tr>
                      <th colspan="3">AC</th>
                    </tr>
                    <tr>
                      <td>Split : <?= $ls->ACSPLIT ?> </td>
                      <td>Window : <?= $ls->ACWINDOW ?></td>
                      <td>Sentral : <?= $ls->ACSENTRAL ?></td>
                    </tr>
                  </table>
                  <table class="table table-bordered">
                    <tr>
                      <td colspan="2">Kolam Renang</td>
                    </tr>
                    <tr>
                      <td>Luas M<sup>2</sup> :<?= $ls->LUAS_KOLAM ?></td>
                      <td>Finishing : <?= $ls->FINISHING_KOLAM ?></td>
                    </tr>
                  </table>
                  <table class="table table-bordered">
                    <tr>
                      <th colspan="2">Perkerasan Halaman</th>
                    </tr>
                    <tr>
                      <td>Ringan : <?= $ls->LUAS_PERKERASAN_RINGAN ?></td>
                      <td>Berat : <?= $ls->LUAS_PERKERASAN_BERAT ?></td>
                    </tr>
                    <tr>
                      <td>Sedang : <?= $ls->LUAS_PERKERASAN_SEDANG ?></td>
                      <td>Dgn Penutup Lantai : <?= $ls->LUAS_PERKERASAN_DG_TUTUP ?></td>
                    </tr>
                  </table>
                  <table class="table table-bordered">
                    <tr>
                      <th colspan="2">Lapangan Tenis</th>
                    </tr>
                    <tr>
                      <th>Dengan Lampu</th>
                      <th>Tanpa Lampu</th>
                    </tr>
                    <tr>
                      <td>Beton : <?= $ls->LAP_TENIS_LAMPU_BETON ?></td>
                      <td>Beton : <?= $ls->LAP_TENIS_BETON ?></td>
                    </tr>
                    <tr>
                      <td>Aspal : <?= $ls->LAP_TENIS_LAMPU_ASPAL ?></td>
                      <td>Aspal : <?= $ls->LAP_TENIS_ASPAL ?></td>
                    </tr>
                    <tr>
                      <td>Tanah liat / Rumput : <?= $ls->LAP_TENIS_LAMPU_RUMPUT ?></td>
                      <td>Tanah liat / Rumput : <?= $ls->LAP_TENIS_RUMPUT ?></td>
                    </tr>
                  </table>
                </div>
                <div class="col-md-6">
                  <table class="table table-bordered">
                    <tr>
                      <th colspan="3">Jumlah Lift</th>
                    </tr>
                    <tr>
                      <td>Penumpang : <?= $ls->LIFT_PENUMPANG ?></td>
                      <td>Kapsul : <?= $ls->LIFT_KAPSUL ?></td>
                      <td>Barang : <?= $ls->LIFT_BARANG ?></td>
                    </tr>
                  </table>
                  <table class="table table-bordered">
                    <tr>
                      <th colspan="2">Jumlah Tangga Berjalan dengan Lebar</th>
                    </tr>
                    <tr>
                      <td>
                        <= 0.8 M : <?= $ls->TGG_BERJALAN_A ?></td> <td> > 0.8 M : <?= $ls->TGG_BERJALAN_B ?>
                      </td>
                    </tr>
                    <tr>
                      <td>Panjang Pagar (m) : <?= $ls->PJG_PAGAR ?></td>
                      <td>Bahan Pagar : <?= $ls->BHN_PAGAR ?></td>
                    </tr>
                  </table>
                  <table class="table table-bordered">
                    <tr>
                      <th colspan="3">Pemadam Kebakaran</th>
                    </tr>
                    <tr>
                      <td>Hydrant : <?= $ls->HYDRANT ?></td>
                      <td>Sprinkler : <?= $ls->SPRINKLER ?></td>
                      <td>Fire Alarm : <?= $ls->FIRE_ALARM ?> </td>
                    </tr>
                    <tr>
                      <td>PABX</td>
                      <td colspan="2">: <?= $ls->SUMUR_ARTESIS ?></td>
                    </tr>
                    <tr>
                      <td>Kedalaman Sumur</td>
                      <td colspan="2">: <?= $ls->SUMUR_ARTESIS ?></td>
                    </tr>
                  </table>

                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- <table class="table table-bordered table-striped" id="example3" >
        <thead>
          <tr>
            <th>No Formulir</th>
            <th>NOP Proses</th>
            <th>NIK</th>
            <th>Nama WP</th>
            <th>Tanggal Pendataan</th>
          </tr>
        </thead>
        <tbody>
          <?php //foreach($data as $data){ 
          ?>
            <tr>
              <td><?php // $data->NO_FORMULIR 
                  ?></td>
              <td><?php // $data->NOP_PROSES 
                  ?></td>
              <td><?php // $data->SUBJEK_PAJAK_ID 
                  ?></td>
              <td><?php // $data->NM_WP 
                  ?></td>
              <td><?php // $data->TGL_PENDATAAN_OP 
                  ?></td>
            </tr>
          <?php //} 
          ?>
        </tbody>
    </table> -->
  </div>
</section>
<script type="text/javascript">
  function formatnop(objek) {

    a = objek.value;
    b = a.replace(/[^\d]/g, "");
    c = "";
    panjang = b.length;

    if (panjang <= 2) {
      // 35 -> 0,2
      c = b;
    } else if (panjang > 2 && panjang <= 4) {
      // 07. -> 2,2
      c = b.substr(0, 2) + '.' + b.substr(2, 2);
    } else if (panjang > 4 && panjang <= 7) {
      // 123 -> 4,3
      c = b.substr(0, 2) + '.' + b.substr(2, 2) + '.' + b.substr(4, 3);
    } else if (panjang > 7 && panjang <= 10) {
      // .123. ->
      c = b.substr(0, 2) + '.' + b.substr(2, 2) + '.' + b.substr(4, 3) + '.' + b.substr(7, 3);
    } else if (panjang > 10 && panjang <= 13) {
      // 123.
      c = b.substr(0, 2) + '.' + b.substr(2, 2) + '.' + b.substr(4, 3) + '.' + b.substr(7, 3) + '.' + b.substr(10, 3);
    } else if (panjang > 13 && panjang <= 17) {
      // 1234
      c = b.substr(0, 2) + '.' + b.substr(2, 2) + '.' + b.substr(4, 3) + '.' + b.substr(7, 3) + '.' + b.substr(10, 3) + '.' + b.substr(13, 4);
    } else {
      // .0
      c = b.substr(0, 2) + '.' + b.substr(2, 2) + '.' + b.substr(4, 3) + '.' + b.substr(7, 3) + '.' + b.substr(10, 3) + '.' + b.substr(13, 4) + '.' + b.substr(17, 1);
      // alert(panjang);
    }
    objek.value = c;
  }
</script>