<form class='form-horizontal' action="<?php echo base_url('spop/form_spop'); ?>" method="post" enctype="multipart/form-data">
    <div class="col-md-4">
        <div class="form-group">
            <label class="col-md-3  control-label">Nomor KTP/ID </label>
            <div class="col-md-9">
                <input type="text" class="form-control" required name="KTP_ID" id="" placeholder="Nomor KTP/ID" value="" />
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-3 control-label">Pekerjaan </label>
            <div class="col-md-9">
                <select class="form-control chosen" requiredd name="PEKERJAAN">
                    <option value=""></option>
                    <option value="0">0 - LAINNYA</option>
                    <option value="1">1 - PNS</option>
                    <option value="2">2 - ABRI</option>
                    <option value="3">3 - PENSIUNAN</option>
                    <option value="4">4 - BADAN</option>
                    <option value="5">5 - LAINNYA</option>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-3 control-label">NPWP </label>
            <div class="col-md-9">
                <input type="text" class="form-control" requiredd name="NPWP" id="" placeholder="NPWP" value="" />
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-3 control-label">Jalan </label>
            <div class="col-md-9">
                <input type="text" class="form-control" requiredd name="JALAN" id="" placeholder="Jalan" value="" />
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-3 control-label">RW /RT </label>
            <div class="col-md-3">
                <input type="text" class="form-control" onkeypress="return isNumberKey(event)" requiredd name="RW_WP" id="" placeholder="RW" value="" />
            </div>
            <div class="col-md-3">
                <input type="text" class="form-control" onkeypress="return isNumberKey(event)" requiredd name="RT_WP" id="" placeholder="RT" value="" />
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-3 control-label">Kota / DATI II </label>
            <div class="col-md-9">
                <input type="text" class="form-control" requiredd name="DATI" id="" placeholder="Kota / DATI II" value="" />
            </div>
        </div>
        <br>
        <div class="form-group">
            <label class="col-md-3  control-label">No. Persil </label>
            <div class="col-md-9">
                <input type="text" class="form-control" requiredd name="PERSIL" id="" placeholder="No. Persil" value="" />
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-3 control-label">Jalan Blok/Kav/No </label>
            <div class="col-md-9">
                <input type="text" class="form-control" requiredd name="BLOK_KAV_OP" maxlength="15" placeholder="Jalan Blok/Kav/No" value="" />
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-3 control-label">ZNT </label>
            <div class="col-md-2">
                <input type="text" class="form-control" requiredd name="ZNT" id="" placeholder="ZNT" value="" />
            </div>
            <label class="col-md-3  control-label">Luas Tanah </label>
            <div class="col-md-4">
                <input type="number" class="form-control" onkeypress="return isNumberKey(event)" requiredd name="LUAS_TANAH" id="" placeholder="" value="" />
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-3  control-label">Tanggal Pendapatan </label>
            <div class="col-md-9">
                <input type="text" class="form-control tanggal" requiredd name="TGL_PENDAPATAN" id="" value="<?php echo date('d-m-Y'); ?>" />
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-3 control-label">Tanggal Penelitian </label>
            <div class="col-md-9">
                <input type="text" class="form-control tanggal" requiredd name="TGL_PENELITIAN" value="<?php echo date('d-m-Y'); ?>" />
            </div>
        </div>
    </div>
    <div class="col-md-4">
        <div class="form-group">
            <label class="col-md-3  control-label">Status WP </label>
            <div class="col-md-9">
                <select class="form-control chosen" requiredd name="STATUS_WP">
                    <option value=""></option>
                    <option value="1">1 - PEMILIK</option>
                    <option value="2">2 - PENYEWA</option>
                    <option value="3">3 - PENGELOLA</option>
                    <option value="4">4 - PEMAKAI</option>
                    <option value="5">5 - SENGKETA</option>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-3 control-label">Nama </label>
            <div class="col-md-9">
                <input type="text" class="form-control" requiredd name="NAMA" id="" placeholder="Nama" value="" />
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-3 control-label">No. telp </label>
            <div class="col-md-9">
                <input type="text" onkeypress="return isNumberKey(event)" class="form-control" requiredd name="TELP" id="" placeholder="No. telp" value="" />
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-3 control-label">Blok / Kav /No </label>
            <div class="col-md-9">
                <input type="text" class="form-control" requiredd name="BLOK_KAV_WP" maxlength="15" placeholder="Blok / Kav /No" value="" />
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-3 control-label">Kelurahan </label>
            <div class="col-md-9">
                <input type="text" class="form-control" requiredd name="KELURAHAN" id="" placeholder="Kelurahan" value="" />
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-3 control-label">Kode Pos </label>
            <div class="col-md-9">
                <input type="text" onkeypress="return isNumberKey(event)" class="form-control" requiredd name="KODE_POS" maxlength="5" placeholder="Kode Pos" value="" />
            </div>
        </div><br>
        <div class="form-group">
            <label class="col-md-3  control-label">RW/RT </label>
            <div class="col-md-3">
                <input type="text" onkeypress="return isNumberKey(event)" class="form-control" requiredd name="RW_OP" id="" placeholder="" value="" />
            </div>
            <div class="col-md-3">
                <input type="text" onkeypress="return isNumberKey(event)" class="form-control" requiredd name="RT_OP" id="" placeholder="" value="" />
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-3 control-label">Cabang </label>
            <div class="col-md-9">
                <input type="checkbox" name="CABANG" value='0' class="minimal">
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-3 control-label">Jenis Tanah </label>
            <div class="col-md-9">
                <select class="form-control chosen" requiredd name="JENIS_TANAH">
                    <option value=""></option>
                    <option value="1">1 - TANAH + BANGUNAN</option>
                    <option value="2">2 - KAVLING SIAP BANGUN</option>
                    <option value="3">3 - TANAH KOSONG</option>
                    <option value="4">4 - FASILITAS UMUM</option>
                    <option value="5">5 - LAIN-LAIN</option>
                </select>
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-3  control-label">NIP Pendata </label>
            <div class="col-md-9">
                <input type="text" onkeypress="return isNumberKey(event)" class="form-control" requiredd name="NIP_PENDATA" id="" placeholder="NIP Pendata" value="" />
            </div>
        </div>
        <div class="form-group">
            <label class="col-md-3 control-label">NIP Peneliti </label>
            <div class="col-md-9">
                <input type="text" onkeypress="return isNumberKey(event)" class="form-control" requiredd name="NIP_PENELITI" id="" placeholder="NIP Peneliti" value="" />
            </div>
        </div><br>
        <div class="form-group">
            <div class="btn-group">
                <button type="submit" disabled id="mySubmit" class="btn btn-sm btn-success"><i class="fa fa-save"></i> Simpan</button>
                <button type="reset" class="btn btn-sm btn-warning"><i class="fa fa-refresh"></i> Batal</button>
            </div>
        </div>
    </div>
    </div>
    </div>
    <!-- body -->
    </div>
</form>