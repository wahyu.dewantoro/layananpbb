<section class="content-header">
  <!--     <h1>
            <?php echo $button ?>
            
          </h1> -->

</section>
<section class="content">
  <div class="nav-tabs-custom">
    <ul class="nav nav-tabs">
      <li class="active"><a href="#tab_1" data-toggle="tab">FORM Generate Nomor</a></li>
      <!--  <li ><a href="#tab_2" data-toggle="tab">Form SK NJOP</a></li> -->
    </ul>
    <div class="tab-content">
      <div class="tab-pane active" id="tab_1">
        <form class='form-horizontal' action="<?php echo base_url('spop/proses_generate_nomor'); ?>" method="post" enctype="multipart/form-data">
          <div class="box box-success">
            <div class="box-header with-border">
            </div>
            <div class="box-body">
              <div class="row">
                <div class="col-md-4" style='border-right:1px solid red; border-bottom:1px solid red;'>
                  <div class="form-group">
                    <label class="col-md-3 control-label">Jenis Formulir</label>
                    <div class="col-md-9">
                      <select class="form-control chosen" required name="KD_JNS_FORMULIR" id="KD_JNS_PELAYANAN" onchange="jenis(this.value)">
                        <option value=""></option>
                        <option value="0001">0001-Hasil Verifikasi Lapangan</option>
                        <option value="0501">0501-Hasil Verifikasi Kantor</option>
                        <option value="1001">1001-Pembatalan</option>
                        <option value="2001">2001-Pemuktahiran</option>
                        <option value="3001">3001-SISMIOP</option>
                        <option value="5001">5001-Individu & Semi Individu</option>
                        <option value="6001">6001-Keberatan & Banding</option>
                        <option value="7001">7001-Fasum</option>
                        <option value="8001">8001-Penghapusan Bangunan</option>
                        <option value="9001">9001-ZNT</option>
                      </select>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-3  control-label">NOP <br>
                      <font color="red" style='font-size: 8px;'>*wajib diisi</font>
                    </label>
                    <div class="col-md-9" id='test'>
                      <table width='100%'>
                        <tr>
                          <td width='12%'><input type="text" required onkeypress="return isNumberKey(event)" maxlength="2" class="form-control" value="35" name="KD_PROPINSI" id="KD_PROPINSI_PEMOHON"></td>
                          <td width='12%'><input type="text" required onkeypress="return isNumberKey(event)" maxlength="2" class="form-control" value="" name="KD_DATI2" id="KD_DATI2_PEMOHON"></td>
                          <td width='15%'><input type="text" required onkeypress="return isNumberKey(event)" maxlength="3" class="form-control" value="" name="KD_KECAMATAN" id="KD_KECAMATAN_PEMOHON"></td>
                          <td width='15%'><input type="text" required onkeypress="return isNumberKey(event)" maxlength="3" class="form-control" value="" name="KD_KELURAHAN" id="KD_KELURAHAN_PEMOHON"></td>
                          <td width='15%'><input type="text" required onkeypress="return isNumberKey(event)" maxlength="3" class="form-control" value="" name="KD_BLOK" id="KD_BLOK_PEMOHON"></td>
                          <td><input type="text" required onkeypress="return isNumberKey(event)" maxlength="4" class="form-control" value="" name="NO_URUT_NOP" id="NO_URUT_PEMOHON"></td>
                          <td width='9%'><input type="text" required onkeypress="return isNumberKey(event)" maxlength="1" class="form-control" value="" name="KD_JNS_OP" id="KD_JNS_OP_PEMOHON"></td>
                        </tr>
                      </table>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-3 control-label">Jumlah Bangunan</label>
                    <div class="col-md-9">
                      <input type="text" tabIndex="24" required class="form-control " name="NO_BNG" value="1" placeholder="Jumlah Bangunan" />
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-md-3 control-label"></label>
                    <div class="col-md-9">
                      <button type="" id="mySubmit" class="btn btn-sm btn-success"><i class="fa fa-search"></i> Proses</button>
                    </div>
                  </div>
                </div>
        </form>
      </div>
      <!-- /.tab-pane 2--><br>
      <h3>List Data Formulir</h3><br>
      <table id="example2" class="table table-striped table-bordered">
        <thead>
          <tr>
            <th width="3%">No</th>
            <th>Nomor Formulir</th>
            <th>NOP</th>
            <th>Keterangan</th>
            <th>Tanggal</th>
            <th>STATUS</th>
          </tr>
        </thead>
        <tbody>
          <?php $no = 1;
          foreach ($nomor as $rk) { ?>
            <tr>
              <td align="center"><?php echo $no ?></td>
              <td><?php echo $rk->NO_FORMULIR ?></td>
              <td><?php echo $rk->NOP ?></td>
              <td>NOMOR <?php echo $rk->KETERANGAN_NOMOR ?></td>
              <td><?php echo $rk->TGL_PEREKAMAN ?></td>
              <td><?php if ($rk->STATUS == 1) {
                    echo "Terpakai";
                  } else {
                    echo "Belum Terpakai";
                  } ?></td>
            </tr>
          <?php $no++;
          } ?>
        </tbody>
      </table>
      <!-- /.tab-pane -->
    </div>
    <!-- /.tab-content -->
  </div>



</section>

<script type="text/javascript">
  $(document).ready(function() {
    $('#KD_JNS_PELAYANAN').val("");

    $('#KD_JNS_PELAYANAN').change(function() {
      selectVal = $('#KD_JNS_PELAYANAN').val();

      if (selectVal == "") {
        $('#mySubmit').prop("disabled", true);
      } else {
        $('#mySubmit').prop("disabled", false);
      }
    })

  });

  function formulir(kategori) {
    //alert(id);
    $.ajax({
      type: "POST",
      url: "<?php echo base_url('Spop/getformulir'); ?>",
      data: "kategori=" + kategori,
      cache: false,
      success: function(data) {
        var z = data.split('|');
        $('#THN_PELAYANAN').attr('value', z[0]);
        $('#KATEGORI_PELAYANAN').attr('value', z[1]);
        $('#NO_URUT_DALAM').attr('value', z[2]);
        $('#NO_URUT').attr('value', z[3]);
        //alert(z[3]);
      }
    });
  }

  function jenis(id) {
    //alert(id);
    if (id != '') {
      document.getElementById("test").innerHTML = '' +
        '<table width="100%">' +
        '<tr>' +
        '<td width="12%"><input type="text" required onkeypress="return isNumberKey(event)" maxlength="2" class="form-control" name="KD_PROPINSI" id="KD_PROPINSI_PEMOHON" value="35"></td>' +
        '<td width="12%"><input type="text" required onkeypress="return isNumberKey(event)" maxlength="2" class="form-control" name="KD_DATI2" id="KD_DATI2_PEMOHON"></td>' +
        '<td width="15%"><input type="text" required onkeypress="return isNumberKey(event)" maxlength="3" class="form-control" name="KD_KECAMATAN" id="KD_KECAMATAN_PEMOHON"></td>' +
        '<td width="15%"><input type="text" required onkeypress="return isNumberKey(event)" maxlength="3" class="form-control" name="KD_KELURAHAN" id="KD_KELURAHAN_PEMOHON"></td>' +
        '<td width="15%"><input type="text" required onkeypress="return isNumberKey(event)" maxlength="3" class="form-control" name="KD_BLOK" id="KD_BLOK_PEMOHON"></td>' +
        '<td><input type="text" required onkeypress="return isNumberKey(event)" maxlength="4" class="form-control" name="NO_URUT_NOP" id="NO_URUT_PEMOHON"></td>' +
        '<td width="9%"><input type="text" required onkeypress="return isNumberKey(event)" maxlength="1" class="form-control" name="KD_JNS_OP" id="KD_JNS_OP_PEMOHON"></td>' +
        '</tr>' +
        '</table>';
      $(document).ready(function() {
        $(".form-control").keyup(function() {
          if (this.value.length == this.maxLength) {
            var nextIndex = $('input:text').index(this) + 1;
            $('input:text')[nextIndex].focus();
          }
        });
        /*document.getElementById('KD_PROPINSI_PEMOHON').value= "" ;*/
        $('#KD_JNS_OP_PEMOHON,#NO_URUT_PEMOHON,#KD_BLOK_PEMOHON,#KD_KELURAHAN_PEMOHON,#KD_KECAMATAN_PEMOHON,#KD_DATI2_PEMOHON,#KD_PROPINSI_PEMOHON').on('change', function() {
          var a = $('#KD_PROPINSI_PEMOHON').val();
          var b = $('#KD_DATI2_PEMOHON').val();
          var c = $('#KD_KECAMATAN_PEMOHON').val();
          var d = $('#KD_KELURAHAN_PEMOHON').val();
          var e = $('#KD_BLOK_PEMOHON').val();
          var f = $('#NO_URUT_PEMOHON').val();
          var g = $('#KD_JNS_OP_PEMOHON').val();
          /*var h   =$('#KD_JNS_PELAYANAN').val();*/
          if (g == '') {
            var nop = '';
          } else {
            var nop = a + "|" + b + "|" + c + "|" + d + "|" + e + "|" + f + "|" + g + "=" + id;;
            //alert(nop);
            $.ajax({
              type: "POST",
              url: "<?php echo base_url('Spop/getNop'); ?>",
              data: "nop=" + nop,
              cache: false,
              success: function(data) {
                var sp = data.split('|');
                //alert(data);                              
                if (sp[0] == 1 && sp[1] == 1) {
                  alert("NOP SUDAH TERDAFTAR");
                  $('#KD_KELURAHAN_PEMOHON').val("");
                  $('#KD_BLOK_PEMOHON').val("");
                  $('#NO_URUT_PEMOHON').val("");
                  $('#KD_JNS_OP_PEMOHON').val("");
                  $('#mySubmit').prop("disabled", true);
                } else if (sp[0] == 0 && sp[1] == 2) {
                  alert("NOP BELUM TERDAFTAR");
                  $('#KD_KELURAHAN_PEMOHON').val("");
                  $('#KD_BLOK_PEMOHON').val("");
                  $('#NO_URUT_PEMOHON').val("");
                  $('#KD_JNS_OP_PEMOHON').val("");
                  $('#mySubmit').prop("disabled", true);
                  //document.getElementById('KD_JNS_OP_PEMOHON').value= "" ;
                } else {
                  $('#mySubmit').prop("disabled", false);
                };
              }
            });
          }
          $.ajax({
            type: "POST",
            url: "<?php echo base_url('Spop/getform'); ?>",
            data: "nop=" + nop,
            cache: false,
            success: function(data) {
              //alert(data);
              $('#detail_from').html(data);
            }
          });
        });
      });


    } else {

    };


  }
</script>
<script type="text/javascript">
  function isNumberKey(evt) {
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode != 46 && charCode > 31 &&
      (charCode < 48 || charCode > 57))
      return false;
    return true;
  }
</script>
<script type="text/javascript">
  function stopRKey(evt) {
    var evt = (evt) ? evt : ((event) ? event : null);
    var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);
    if ((evt.keyCode == 13) && (node.type == "text")) {
      return false;
    }
  }
  document.onkeypress = stopRKey;
</script>
<script type="text/javascript">
  $('body').on('keypress', 'input, select, textarea', function(e) {
    var self = $(this),
      form = self.parents('form:eq(0)'),
      focusable, next;
    if (e.keyCode == 13) {
      focusable = form.find('input,select,button,textarea').filter(':visible');
      next = focusable.eq(focusable.index(this) + 1);
      if (next.length) {
        next.focus();
      } else {
        form.submit();
      }
      return false;

    }
  });
</script>