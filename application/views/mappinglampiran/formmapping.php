  <form class='form-horizontal' action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
    <section class="content-header">
      <h1>
        Mapping Lampiran [<?php echo $layanan ?>]
        <div class="pull-right btn-group">
          <button class="btn btn-sm btn-primary"><i class="fa fa-save"></i> Simpan</button>
          <button type="button" class="btn btn-sm btn-info" onclick="cek(this.form.cekbox)"><i class="fa fa-check"></i> Selcet all</button>
          <button type="button" class="btn btn-sm btn-warning" onclick="uncek(this.form.cekbox)"><i class="fa fa-close"></i> Clear all</button>

          <?php echo anchor('mappinglampiran', '<i class="fa fa-list"></i> List', 'class="btn btn-sm btn-success"'); ?>
        </div>
      </h1>

    </section>
    <section class="content">
      <div class="box">

        <div class="box-body">

          <input type="hidden" name="KD_JNS_PELAYANAN" value="<?php echo $KD_JNS_PELAYANAN ?>">
          <table class="table table-bordered table-hover">
            <thead>
              <tr>
                <th width="5%">No</th>
                <th>Lampiran</th>
                <th width="5%">Aktif</th>
              </tr>
            </thead>
            <tbody>
              <?php $no = 1;
              foreach ($res as $rk) { ?>
                <tr>
                  <td align="center"><?php echo $no ?></td>
                  <td><?php echo $rk->NAMA_LAMPIRAN ?></td>
                  <td align="center"><input type="checkbox" name="KODE[]" id="cekbox" <?php if ($rk->ST == '1') {
                                                                                        echo "checked";
                                                                                      } ?> value="<?php echo $rk->KODE ?>"></td>
                </tr>
              <?php $no++;
              } ?>
            </tbody>
          </table>
        </div>
      </div>
    </section>
  </form>
  <script>
    function cek(cekbox) {
      for (i = 0; i < cekbox.length; i++) {
        cekbox[i].checked = true;
      }
    }

    function uncek(cekbox) {
      for (i = 0; i < cekbox.length; i++) {
        cekbox[i].checked = false;
      }
    }
  </script>