<section class="content-header">
  <h1>
    Layanan
    <div class="pull-right">

    </div>
  </h1>
</section>
<section class="content">
  <div class="box">
    <div class="box-header with-border">


    </div>
    <div class="box-body">
      <?php echo $this->session->flashdata('notif'); ?>
      <table class="table table-bordered table-striped" id="example1">
        <thead>
          <tr>
            <th width="5%">No</th>
            <th>Layanan</th>
            <th width="10%">Aksi</th>
          </tr>
        </thead>
        <tbody>
          <?php $no = 1;
          foreach ($data as $rk) { ?>
            <tr>
              <td align="center"><?php echo $no ?></td>
              <td><?php echo $rk->NM_JENIS_PELAYANAN ?></td>
              <td align="center"><?php echo anchor('mappinglampiran/setting/' . $rk->KD_JNS_PELAYANAN, '<i class="fa fa-cog"></i>', 'class="btn btn-sm btn-success"'); ?></td>
            </tr>
          <?php $no++;
          } ?>
        </tbody>
      </table>
    </div>
  </div>
</section>