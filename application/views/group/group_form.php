<section class="content-header">
  <h1>
    <?php echo $button ?>
    <div class="pull-right">

    </div>
  </h1>

</section>
<section class="content">
  <div class="box">
    <div class="box-header with-border">
      <div class="box-tools pull-right">
        <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
      </div>
    </div>
    <div class="box-body">
      <form class='form-horizontal' action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
        <div class="form-group">
          <label class="col-md-2 control-label">Nama Group </label>
          <div class="col-md-8">
            <input type="text" class="form-control" name="nama_group" id="nama_group" placeholder="Nama Group" value="<?php echo $nama_group; ?>" />
            <?php echo form_error('nama_group') ?>
          </div>
        </div>
        <input type="hidden" name="kode_group" value="<?php echo $kode_group; ?>" />

        <div class="form-group">
          <div class="col-md-8 col-md-offset-2">
            <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Simpan</button>
            <a href="<?php echo site_url('group') ?>" class="btn btn-warning"><i class="fa fa-close"></i> Cancel</a>
          </div>
        </div>
      </form>
    </div>
  </div>
</section>