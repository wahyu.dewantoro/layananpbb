        <section class="content-header">
          <h1>
            Data Hasil Rekaman Lengkap
            <div class="pull-right">
             
          </div>
          </h1>
          
        </section>
        <!-- Main content -->
        <section class="content">
          <!-- Default box -->
          <div class="box">
            <div class="box-header with-border">
              
            </div>
            <div class="box-body">
                  <form class="form-inline" method="get" action="<?php echo base_url().'data/dhrLengkapexcel.html'?>">
                     <div class="form-group">
                      Tahun Pajak
                    </div>
                    <div class="form-group">
                      <select class="form-control" name="tahun">
                        <?php for($q=date('Y');$q>2009;$q--){?>
                                  <option <?php if($q==$tahun){echo "selected";}?> value="<?= $q ?>"><?= $q ?></option>
                                <?php } ?>
                      </select>
                      <!-- <input type="text" name="tahun" value="<?= $tahun ?>" class="form-control"> -->
                      <!-- <input type="hidden" name="cari" value="1"> -->
                    </div>
                     <div class="form-group">
                      Kecamatan
                    </div>
                    <div class="form-group">
                      <select class="form-control" name="KD_KECAMATAN" id="KD_KECAMATAN">
                                    <option value="">Pilih</option>
                                    <?php foreach($kec as $kec){?>
                                      <option <?php if($kec->KD_KECAMATAN==$KD_KECAMATAN){echo "selected";}?> value="<?= $kec->KD_KECAMATAN ?>"><?= $kec->KD_KECAMATAN.' '.$kec->NM_KECAMATAN ?></option>
                                    <?php } ?>
                      </select>
                    </div>
                     <div class="form-group">
                      Kelurahan
                    </div>
                    <div class="form-group">
                      <select class="form-control"  name="KD_KELURAHAN" id="KD_KELURAHAN">
                                    <option value="">Pilih</option>
                                     <?php 
                                      foreach($kel as $kela){?>
                                        <option  <?php if($kela->KD_KELURAHAN==$KD_KELURAHAN){echo "selected";}?> value="<?= $kela->KD_KELURAHAN?>"><?= $kela->KD_KELURAHAN.' '.$kela->NM_KELURAHAN?></option>
                                    <?php  }?>
                                  </select>
                    </div>

                    <div class="form-group">
                      Jenis Bumi
                    </div>
                    <div class="form-group">
                      <select class="form-control" name="jns_bumi[]" id="example-getting-started" multiple="multiple">
                      <option value="1">1 - TANAH + BANGUNAN</option>
                      <option value="2">2 - KAVLING SIAP BANGUN</option>
                      <option value="3">3 - TANAH KOSONG</option>
                      <option value="4">4 - FASILITAS UMUM</option>
                      <option value="5">5 - LAIN-LAIN</option>
                      </select>
                    </div>
                    
                    <button type="submit" class="btn btn-success">Excel</button>
                     
                  </form>

                  
            </div><!-- /.box-body -->
          </div><!-- /.box -->
        </section><!-- /.content -->
        <!-- Initialize the plugin: -->
<script type="text/javascript">
    // $(document).ready(function() {
    //     $('#example-getting-started').multiselect();
    // });
    $(document).ready(function() {
        $('#example-getting-started').multiselect({
            includeSelectAllOption: true,
        });
    });
</script>
</script>
<script type="text/javascript" src="<?php echo base_url().'adminlte/'?>bootstrap/js/bootstrap-multiselect.js"></script>

<script type="text/javascript">
          $(document).ready(function(){ 
              
              $("#KD_KECAMATAN").change(function(){ 
                
                
              
                $.ajax({
                  type: "POST",
                  url: "<?= base_url().'data/getkelurahan'?>", 
                  data: {kd_kec : $("#KD_KECAMATAN").val()}, 
                  success: function(response){ 
                    $("#KD_KELURAHAN").html(response);
                  }

                });
              });
            });
        </script>
  
 