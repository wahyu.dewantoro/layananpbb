        <section class="content-header">
          <h1>
            Data Belum Verlap
            <div class="pull-right">

            </div>
          </h1>

        </section>
        <!-- Main content -->
        <section class="content">
          <!-- Default box -->
          <div class="box">
            <!-- <div class="box-header with-border">
              
            </div> -->
            <div class="box-body">
              <!-- <form class="form-inline" method="post" action="<?php echo base_url() . 'data/dhr.html' ?>">
                     <div class="form-group">
                      Tahun Pajak
                    </div>
                    <div class="form-group">
                      <select class="form-control" name="tahun">
                        <?php for ($q = date('Y'); $q > 2009; $q--) { ?>
                                  <option <?php if ($q == $tahun) {
                                            echo "selected";
                                          } ?> value="<?= $q ?>"><?= $q ?></option>
                                <?php } ?>
                      </select>
                      <input type="hidden" name="cari" value="1">
                    </div>
                     <div class="form-group">
                      Kecamatan
                    </div>
                    <div class="form-group">
                      <select class="form-control" name="KD_KECAMATAN" id="KD_KECAMATAN">
                                    <option value="">Pilih</option>
                                    <?php foreach ($kec as $kec) { ?>
                                      <option <?php if ($kec->KD_KECAMATAN == $KD_KECAMATAN) {
                                                echo "selected";
                                              } ?> value="<?= $kec->KD_KECAMATAN ?>"><?= $kec->KD_KECAMATAN . ' ' . $kec->NM_KECAMATAN ?></option>
                                    <?php } ?>
                      </select>
                    </div>
                     <div class="form-group">
                      Kelurahan
                    </div>
                    <div class="form-group">
                      <select class="form-control"  name="KD_KELURAHAN" id="KD_KELURAHAN">
                                    <option value="">Pilih</option>
                                    <?php
                                    foreach ($kel as $kela) { ?>
                                        <option  <?php if ($kela->KD_KELURAHAN == $KD_KELURAHAN) {
                                                    echo "selected";
                                                  } ?> value="<?= $kela->KD_KELURAHAN ?>"><?= $kela->KD_KELURAHAN . ' ' . $kela->NM_KELURAHAN ?></option>
                                    <?php  } ?>
                                  </select>
                    </div>
                    <div class="form-group">
                      Jenis Bumi
                    </div>
                    <div class="form-group">
                      <select class="form-control" name="jns_bumi[]" id="example-getting-started" multiple="multiple">
                      <option value="1">1 - TANAH + BANGUNAN</option>
                      <option value="2">2 - KAVLING SIAP BANGUN</option>
                      <option value="3">3 - TANAH KOSONG</option>
                      <option value="4">4 - FASILITAS UMUM</option>
                      <option value="5">5 - LAIN-LAIN</option>
                      </select>
                    </div>
                    
                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i> Cari</button>
                     <?php
                      $where = "";
                      if (isset($KD_KECAMATAN)) {
                        $where = "&kec=" . urlencode($KD_KECAMATAN);
                      }
                      if (isset($KD_KELURAHAN)) {
                        $where = "&kec=" . urlencode($KD_KECAMATAN) . "&kel=" . urlencode($KD_KELURAHAN);
                      }
                      //echo $where;

                      if (isset($rk)) {
                        echo anchor("data/dhrexcel?tahun=" . urlencode($tahun) . $where, 'Excel', 'class="btn btn-success"');
                      }
                      ?>
                  </form> -->


              <hr>
              <table class="table table-bordered table-striped" id="example3">
                <thead>
                  <tr>
                    <th width="5%">No</th>
                    <th width="15%">NOP</th>
                    <!-- <th width="15%">NO Layanan</th> -->
                    <th>Nama WP</th>
                    <th>Alamat WP</th>
                    <th width="5%">Aksi</th>

                  </tr>
                </thead>
                <?php if (isset($rk)) { ?>
                  <tbody>
                    <?php $no = 1;
                    foreach ($rk as $rk) {

                    ?>
                      <tr>
                        <td align="center"><?php echo $no ?></td>
                        <td><?= $rk->KD_PROPINSI_RIWAYAT . '.' . $rk->KD_DATI2_RIWAYAT . '.' . $rk->KD_KECAMATAN_RIWAYAT . '.' . $rk->KD_KELURAHAN_RIWAYAT . '.' . $rk->KD_BLOK_RIWAYAT . '.' . $rk->NO_URUT_RIWAYAT . '.' . $rk->KD_JNS_OP_RIWAYAT ?></td>
                        <td><?= $rk->NM_WP ?></td>
                        <td><?= 'DS. ' . $rk->NM_KELURAHAN ?></td>
                        <td align="center"><a href="<?= site_url('replikasi/verlap') . '?id=' . $rk->KD_PROPINSI_RIWAYAT . '.' . $rk->KD_DATI2_RIWAYAT . '.' . $rk->KD_KECAMATAN_RIWAYAT . '.' . $rk->KD_KELURAHAN_RIWAYAT . '.' . $rk->KD_BLOK_RIWAYAT . '.' . $rk->NO_URUT_RIWAYAT . '.' . $rk->KD_JNS_OP_RIWAYAT ?>"><button class="btn btn-xs btn-primary" data-toggle="tooltip" title="Verlap"><i class="fa fa-edit"></i></button></a></td>
                      </tr>
                  <?php
                    }
                  }
                  ?>
                  </tbody>
              </table>

            </div><!-- /.box-body -->
          </div><!-- /.box -->
        </section><!-- /.content -->


        <!-- Initialize the plugin: -->
        <script type="text/javascript">
          // $(document).ready(function() {
          //     $('#example-getting-started').multiselect();
          // });
          $(document).ready(function() {
            $('#example-getting-started').multiselect({
              includeSelectAllOption: true,
            });
          });
        </script>
        </script>
        <script type="text/javascript" src="<?php echo base_url() . 'adminlte/' ?>bootstrap/js/bootstrap-multiselect.js"></script>
        <!-- <script type="text/javascript">

          $(document).ready(function(){ 
              
              $("#KD_KECAMATAN").change(function(){ 
                
                
              
                $.ajax({
                  type: "POST",
                  url: "<?= base_url() . 'data/getkelurahan' ?>", 
                  data: {kd_kec : $("#KD_KECAMATAN").val()}, 
                  success: function(response){ 
                    $("#KD_KELURAHAN").html(response);
                  }

                });
              });
            });
        </script> -->