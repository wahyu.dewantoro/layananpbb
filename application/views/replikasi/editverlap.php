<section class="content-header">
    <h1>
        Edit Verifikasi Lapangan
        <div class="pull-right">

        </div>
    </h1>

</section>
<section class="content">
    <div class="row">
        <div class="col-md-6">


            <div class="box">

                <div class="box-body">
                    <form class='form-horizontal' action="<?php echo base_url('replikasi/updateverlap') ?>" method="post" enctype="multipart/form-data">
                        <div class="form-group">
                            <label class="col-md-2 control-label">NOP </label>
                            <div class="col-md-8">
                                <input type="text" class="form-control" name="nop" id="nop" placeholder="NOP" value="<?php echo $rk->KD_PROPINSI . '.' . $rk->KD_DATI2 . '.' . $rk->KD_KECAMATAN . '.' . $rk->KD_KELURAHAN . '.' . $rk->KD_BLOK . '.' . $rk->NO_URUT . '.' . $rk->KD_JNS_OP ?>" readonly="" />
                            </div>
                        </div>
                        <!-- <div class="form-group">
                                    <label class="col-md-2 control-label">NO Layanan </label>
                                    <div class="col-md-8">
                                        <input type="text" class="form-control" name="nopel" id="nopel" placeholder="NO Layanan" value="<?php echo $rk->THN_PELAYANAN . '.' . $rk->BUNDEL_PELAYANAN . '.' . $rk->NO_URUT_PELAYANAN ?>" readonly=""/>
                                    </div>
                                </div> -->
                        <div class="form-group">
                            <label class="col-md-2 control-label">Nama WP </label>
                            <div class="col-md-8">
                                <input type="text" class="form-control" name="nama_wp" id="nama_wp" placeholder="Nama WP" value="<?php echo $rk->NM_WP_SPPT; ?>" readonly="" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">Alamat WP </label>
                            <div class="col-md-8">
                                <input type="text" class="form-control" name="alamat_wp" id="alamat_wp" placeholder="Alamat WP" value="<?php echo 'DS. ' . $rk->NM_KELURAHAN; ?>" readonly="" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">Luas Bumi </label>
                            <div class="col-md-8">
                                <input type="number" class="form-control" name="luas_bumi" id="luas_bumi" placeholder="Luas Bumi" value="<?php echo $rk->LUAS_TANAH ?>" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">Luas Bangunan </label>
                            <div class="col-md-8">
                                <input type="number" class="form-control" name="luas_bng" id="luas_bng" placeholder="Luas Bumi" value="<?php echo $rk->LUAS_BANGUNAN ?>" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-2 control-label">Catatan </label>
                            <div class="col-md-8">
                                <textarea class="form-control" name="catatan" placeholder="Isi Catatan"><?php echo $rk->CATATAN ?></textarea>
                            </div>
                        </div>
                        <input type="hidden" name="ID" value="<?php echo $rk->ID ?>">

                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-2">
                                <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Simpan</button>
                                <a href="<?php echo site_url('replikasi/sudahverlap') ?>" class="btn btn-warning"><i class="fa fa-close"></i> Cancel</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>