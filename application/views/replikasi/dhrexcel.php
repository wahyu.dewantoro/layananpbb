 <?php
 header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=datahasilrekaman".date('YmdHis').".xls");
  // echo $judul;

  ?>   
              LAPORAN DATA HASIL REKAMAN
             
                  <table  style="font-size:12px; padding:2px">
                      <thead>
                          <tr>
                            <th>No</th>
                            <th>NOP</th>
                            <th>FORMULIR SPOP</th>
                            <th>JUMLAH BNG</th>
                            <th>ALAMAT OP</th>
                            <th>RT/RW</th>
                            <th>NAMA WP</th>
                            <th>LUAS BUMI</th>
                            <th>ZNT</th>
                            <th>NILAI BUMI</th>
                            <th>KTP</th>
                            <th>NPWP</th>
                            <th>STATUS WP</th>
                            <th>PEKERJAAN WP</th>
                            <th>PERSIL</th>

                          </tr>
                      </thead>
                      <tbody>
                        <?php $no=1; foreach($rk as $rk){?>
                        <tr>
                          <td align="center"><?php echo $no?></td>
                          <td><?= $rk->KD_PROPINSI.'.'.$rk->KD_DATI2.'.'.$rk->KD_KECAMATAN.'.'.$rk->KD_KELURAHAN.'.'.$rk->KD_BLOK.'-'.$rk->NO_URUT.'.'.$rk->KD_JNS_OP ?></td>
                          <td><?= $rk->NO_FORMULIR_SPOP ?></td>
                          <td align="center"></td>
                          <td><?= $rk->ALAMAT_OP ?></td>
                          <td><?= $rk->RT_OP.'/'.$rk->RW_OP ?></td>
                          <td><?= $rk->NM_WP_SPPT ?></td>
                          <td><?= $rk->LUAS_BUMI ?></td>
                          <td><?= $rk->KD_ZNT ?></td>
                          <td><?= $rk->NILAI_SISTEM_BUMI ?></td>
                          <td><?= "'".$rk->KD_PROPINSI.$rk->KD_DATI2.$rk->KD_KECAMATAN.$rk->KD_KELURAHAN.$rk->KD_BLOK.$rk->NO_URUT.$rk->KD_JNS_OP ?></td>
                          <td></td>
                          <td><?= $rk->KD_STATUS_WP.' - PEMILIK' ?></td>
                          <td></td>
                          <td><?= $rk->NO_PERSIL ?></td>
                        </tr>
                        <?php $no++;}?>
                      </tbody>
                  </table>
 