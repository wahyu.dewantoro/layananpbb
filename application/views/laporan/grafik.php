<section class="content-header">
  <h1>
    Grafik
    <div class="pull-right">

    </div>
  </h1>

</section>
<!-- Main content -->
<section class="content">
  <!-- Default box -->

  <div class="row">
    <div class="col-md-12">
      <!-- AREA CHART -->
      <div class="box box-primary">

        <div class="box-body chart-responsive">
          <form class="form-inline" method="post" action="<?php echo base_url() . 'laporan/grafik.html' ?>">
            <div class="form-group">
              Tanggal permohonan
            </div>
            <div class="form-group">
              <input type="text" class="form-control tanggal" name="tgl1" value="<?php echo $tgl1 ?>" reuired placeholder="Tanggal mulai">
            </div>
            <div class="form-group">
              <input type="text" class="form-control tanggal" name="tgl2" value="<?php echo $tgl2 ?>" reuired placeholder="Tanggal akhir">
            </div>

            <button type="submit" class="btn btn-default"><i class="fa fa-search"></i> Cari</button>
          </form>
          <div class="chart" id="bar-chart" style="height: auto;"></div>
        </div><!-- /.box-body -->
      </div><!-- /.box -->

      <!-- AREA CHART -->
      <div class="box box-primary">
        <div class="box-header">
          <h3 class="box-title">Rekap <?php echo date('Y') ?></h3>
        </div>
        <div class="box-body chart-responsive">
          <div class="chart" id="asdf" style="height: 300px;"></div>
        </div><!-- /.box-body -->
      </div><!-- /.box -->

    </div>

  </div><!-- /.row -->

</section><!-- /.content -->

<!-- page script -->
<script type="text/javascript">
  $(function() {
    "use strict";

    //BAR CHART
    var bar = new Morris.Bar({
      element: 'bar-chart',
      resize: true,
      data: [
        <?php foreach ($rk as $rk) { ?> {
            layanan: '<?php echo $rk->LAYANAN ?>',
            jumlah: <?php echo $rk->JUM ?>
          },
        <?php } ?>

      ],

      xkey: 'layanan',
      ykeys: ['jumlah'],
      labels: ['jumlah'],
      barRatio: 0.4,
      xLabelAngle: 25,
      hideHover: 'auto'
    });



    //BAR CHART
    var bar = new Morris.Bar({
      element: 'asdf',
      resize: true,
      data: [
        <?php foreach ($rg as $rg) { ?> {
            y: '<?php echo $rg->LAYANAN ?>',
            a: <?php echo $rg->JAN ?>,
            b: <?php echo $rg->FEB ?>,
            c: <?php echo $rg->MAR ?>,
            d: <?php echo $rg->APR ?>,
            e: <?php echo $rg->MEI ?>,
            f: <?php echo $rg->JUN ?>,
            g: <?php echo $rg->JUL ?>,
            h: <?php echo $rg->AGU ?>,
            i: <?php echo $rg->SEP ?>,
            j: <?php echo $rg->OKT ?>,
            k: <?php echo $rg->NOV ?>,
            l: <?php echo $rg->DES ?>
          },
        <?php } ?>

      ],
      // barColors: ['#00a65a', '#f56954'],
      xkey: 'y',
      ykeys: ['a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l'],
      labels: ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'],
      barRatio: 0.4,
      xLabelAngle: 25,
      hideHover: 'auto'
    });


  });
</script>