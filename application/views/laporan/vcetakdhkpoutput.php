<style type="text/css">
    .center {
        margin-left: auto;
        margin-right: auto;
    }

    body {
      font-family: "Courier New";
      font-size: 12.5pt;
      line-height: 1.42857143;
      color: #333;
      background-color: #fff;
    }

    th{
    font-weight: normal;
  }
</style>


<?php 
function   romawibuku($buku){
  switch ($buku) {
    case '1':
      # code...
      $b='I';
      break;
    case '2':
      # code...
    $b='II';
      break;
    case '3':
      # code...
    $b='III';
      break;
    case '4':
      # code...
      $b='IV';
      break;
    default:
      # code...
      $b='V';
      break;
  }
  return  $b;
}


$bb=explode(',',$BUKU);
$no=1;
foreach($bb as $bb){?>
<?php if($no>1){?> <pagebreak ></pagebreak><?php } ?>
<p style="font-size:12.5pt; font-family: 'Courier New'; text-align:center">DAFTAR HIMPUNAN KETETAPAN PAJAK & PEMBAYARAN BUKU <?php echo romawibuku($bb); ?></p>
<p style="font-size:12.5pt; font-family: 'Courier New'; text-align:center">TAHUN : <?php echo $THN_PAJAK_SPPT?> </p>
<table border="0" width="100%" style="font-size:12.5pt; font-family: 'Courier New';">
   
</table>

<table style="font-size:12.5pt; font-family: 'Courier New';" width='100%' border="0" cellspacing="0">
                      <thead>
                    
                              <tr>
                                  <th align='left' colspan='3' style="margin-left:10%">PROPINSI : 35 - Jawa Timur</th>
                                  <th align='left'>&nbsp;</th>
                                  <th align="right">KECAMATAN</th>
                                  <th align='left' colspan='3'>: <?php $this->Mmaster->getkecamatan($KD_PROPINSI,$KD_DATI2,$KD_KECAMATAN) ?></th>
                                </tr>
                                <tr>
                                  <th align='left' colspan='3'>DATI II&nbsp; : 07 - Kabupaten Malang</th>
                                  <th align='left'>&nbsp;</th>
                                  <th align="right">KELURAHAN</th>
                                  <th align='left' colspan="3">: <?php $this->Mmaster->getkelurahan($KD_PROPINSI,$KD_DATI2,$KD_KECAMATAN,$KD_KELURAHAN) ?></th>
                                </tr>
                          <tr >
                            <th style="border-bottom: solid 1px #000; border-top: solid 1px #000;" width="5%">No</th>
                            <th style="border-bottom: solid 1px #000; border-top: solid 1px #000;" align="left" width="10%">NOP</th>
                            <th style="border-bottom: solid 1px #000; border-top: solid 1px #000;" align="left" width="20%">Nama Wajib Pajak</th>
                            <th style="border-bottom: solid 1px #000; border-top: solid 1px #000;" align="left">Alamat Objek Pajak</th>
                            <th style="border-bottom: solid 1px #000; border-top: solid 1px #000;" align="left">Alamat Wajib Pajak</th>
                            <th style="border-bottom: solid 1px #000; border-top: solid 1px #000;" width="5%">L Bumi</th>
                            <th style="border-bottom: solid 1px #000; border-top: solid 1px #000;" width="5%">L BNG</th>
                            <th style="border-bottom: solid 1px #000; border-top: solid 1px #000;" align="right" width="10%">PBB</th>
                          </tr>
                      </thead>
                      <tbody>
                        <?php 
                        $rk=$this->db->query("SELECT a.KD_BLOK||'.'||a.no_urut nop,nm_wp_sppt nama_wp,jln_wp_sppt||', '|| rt_wp_sppt||' / '||rw_wp_sppt alamat_wp,jalan_op||', '|| rt_op||' / '||rw_op alamat_op, luas_bumi_sppt luas_bumi,luas_bng_sppt luas_bangunan,pbb_yg_harus_dibayar_sppt pbb,buku
                                      FROM (select a.*,decode(greatest(pbb_yg_harus_dibayar_sppt,0),least(pbb_yg_harus_dibayar_sppt,100000),1,null) ||
                                                                          decode(greatest(pbb_yg_harus_dibayar_sppt,100001),least(pbb_yg_harus_dibayar_sppt,500000),2,null) ||
                                                                          decode(greatest(pbb_yg_harus_dibayar_sppt,500001),least(pbb_yg_harus_dibayar_sppt,2000000),3,null) ||
                                                                          decode(greatest(pbb_yg_harus_dibayar_sppt,200001),least(pbb_yg_harus_dibayar_sppt,5000000),4,null) || 
                                                                          decode(greatest(pbb_yg_harus_dibayar_sppt,500001),least(pbb_yg_harus_dibayar_sppt,9999999999999),5,null) buku 
                                                                          from SPPT a 
                                                                          WHERE THN_PAJAK_SPPT='$THN_PAJAK_SPPT'
                                                                          and kd_propinsi='$KD_PROPINSI'
                                                                          and kd_dati2='$KD_DATI2'
                                                                          and kd_kecamatan='$KD_KECAMATAN'
                                                                          and kd_kelurahan='$KD_KELURAHAN'
                                                                          ) a
                                      join dat_objek_pajak b on a.KD_PROPINSI=b.KD_PROPINSI and
                                                                          a.KD_DATI2=b.KD_DATI2 and
                                                                          a.KD_KECAMATAN=b.KD_KECAMATAN and
                                                                          a.KD_KELURAHAN=b.KD_KELURAHAN and
                                                                          a.KD_BLOK=b.KD_BLOK and
                                                                          a.NO_URUT=b.NO_URUT and
                                                                          a.KD_JNS_OP=b.KD_JNS_OP
                                                                          where buku in ($bb)")->result();

                        $no=1; foreach($rk as $rk){?>
                        <tr >
                          <td style="border-bottom: solid 1px #000;"  align="center"><?php echo $no;?></td>
                          <td style="border-bottom: solid 1px #000;" ><?= $rk->NOP ?></td>
                          <td style="border-bottom: solid 1px #000;" ><?= $rk->NAMA_WP ?></td>
                          <td style="border-bottom: solid 1px #000;" ><?= $rk->ALAMAT_OP ?></td>
                          <td style="border-bottom: solid 1px #000;" ><?= $rk->ALAMAT_WP ?></td>
                          <td style="border-bottom: solid 1px #000;"  align="center"><?= $rk->LUAS_BUMI ?></td>
                          <td style="border-bottom: solid 1px #000;"  align="center"><?= $rk->LUAS_BANGUNAN ?></td>
                          <td style="border-bottom: solid 1px #000;"  align="right"><?= number_format($rk->PBB,'0','','.') ?></td>
                        </tr>
                        <?php $no++; } ?>
                      </tbody>
                  </table>

<?php $no++; } ?>
                       
                       
                  
                  
                      
               <!--    
                   -->