 <?php
 header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=detailpermohonan".date('YmdHis').".xls");
  // echo $judul;

  ?>   
              LAPORAN DETAIL PERMOHONAN
             
                  <table  style="font-size:12px; padding:2px">
                      <thead>
                          <tr>
                            <th>No</th>
                            <th>No Layanan</th>
                            <th>NOP</th>
                            <th>Pemohon</th>
                            <th>Jenis Pelayanan</th>
                            <th>Tanggal Terima</th>
                            <th>Tanggal Selesai</th>
                            <th>Unit</th>
                            <th>Keterangan</th>

                          </tr>
                      </thead>
                      <tbody>
                        <?php $no=1; foreach($rk as $rk){?>
                        <tr>
                          <td align="center"><?php echo $no?></td>
                          <td><?php echo $rk->NO_LAYANAN?></td>
                          <td><?php echo $rk->NOP?></td>
                          <td><?php echo $rk->NAMA_PEMOHON?></td>
                          <td><?php echo $rk->NM_JENIS_PELAYANAN?></td>
                          <td align="center"><?php echo $rk->TGL_TERIMA?></td>
                          <td align="center"><?php echo $rk->TGL_SELESAI?></td>
                          <td align="center"><?php echo $rk->UNIT_KANTOR?></td>
                          <td><?php echo $rk->KETERANGAN_PST?></td>
                        </tr>
                        <?php $no++;}?>
                      </tbody>
                  </table>
 