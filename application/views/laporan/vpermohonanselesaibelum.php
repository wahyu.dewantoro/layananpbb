        <section class="content-header">
          <h1>
            Laporan Status Permohonan 
            <div class="pull-right">
             
          </div>
          </h1>
          
        </section>
        <!-- Main content -->
        <section class="content">
          <!-- Default box -->
          <div class="box">
            <div class="box-header with-border">
              
            </div>
            <div class="box-body">
                  <form class="form-inline" method="post" action="<?php echo base_url().'laporan/permohonanbelumSelesai.html'?>">
                     <div class="form-group">
                      Tanggal permohonan
                    </div>
                    <div class="form-group">
                      <input type="text" class="form-control tanggal" name="tgl1" value="<?php echo $tgl1?>" reuired placeholder="Tanggal mulai">
                    </div>
                    <div class="form-group">
                      <input type="text" class="form-control tanggal" name="tgl2" value="<?php echo $tgl2?>" reuired placeholder="Tanggal akhir">
                    </div>
                    
                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i> Cari</button>
                    <?php echo anchor("laporan/permohonanselesaibelumexcel?t1=".urlencode($tgl1)."&t2=".urlencode($tgl2),'Excel','class="btn btn-success"');?>
                  </form>

                  <?php if(isset($rk)){?>
                  <hr>
                   <table class="table table-bordered table-striped" id="example3">
                      <thead>
                          <tr>
                            <th width="3%">No</th>
                            <th>Tahun</th>
                            <th>Jenis Pelayanan</th>
                            <th>Jumlah</th>
                            <th></th>
                           
                          </tr>
                      </thead>
                      <tbody>
                        <?php $no=1; foreach($rk as $rk){?>
                        <tr>
                          <td align="center"><?php echo $no?></td>
                          <td align="center"><?= $rk->THN_PELAYANAN ?></td>
                          <td><?= $rk->NM_JENIS_PELAYANAN ?></td>
                          <td align="center"><?= $rk->JUMLAH ?></td>
                          <td align="center"><a target="popup" onclick="window.open('<?= base_url().'laporan/detailbelumselesai?t1='.urlencode($tgl1).'&t2='.urlencode($tgl2).'&thn='.$rk->THN_PELAYANAN.'&jns='.urlencode($rk->NM_JENIS_PELAYANAN)?>','name','width=1200,height=400')" href="<?= base_url().'laporan/detailbelumselesai?t1='.urlencode($tgl1).'&t2='.urlencode($tgl2).'&thn='.$rk->THN_PELAYANAN.'&jns='.urlencode($rk->NM_JENIS_PELAYANAN)?>" class="btn btn-xs btn-success"><i class="fa fa-binoculars"></i></a></td>
                        </tr>
                        <?php $no++;}?>
                      </tbody>
                  </table>

                  <?php } ?>
            </div><!-- /.box-body -->
          </div><!-- /.box -->
        </section><!-- /.content -->

  
 