        <section class="content-header">
          <h1>
            <?php echo $judul ?>
            <div class="pull-right">
              <?php echo anchor('laporan/permohonanrekap', '<i class="fa fa-list"></i> Kembali', 'class="btn btn-sm btn-success"'); ?>
            </div>
          </h1>

        </section>
        <!-- Main content -->
        <section class="content">
          <!-- Default box -->
          <div class="box">
            <div class="box-header with-border">

            </div>
            <div class="box-body">
              <table class="table table-bordered table-striped" id="example3" style="font-size:12px; padding:2px">
                <thead>
                  <tr>
                    <th align="center">No</th>
                    <th align="center">No Layanan</th>
                    <th>Nama Pemohon</th>
                    <th>Status</th>
                    <th>Tanggal Terima</th>
                    <th>Tanggal Selesai</th>

                  </tr>
                </thead>
                <tbody>
                  <?php $no = 1;
                  foreach ($rk as $rk) { ?>
                    <tr>
                      <td align="center"><?php echo $no ?></td>
                      <td><?php echo $rk->NO_LAYANAN ?></td>
                      <td><?php echo $rk->NAMA_PEMOHON ?></td>
                      <td><?php echo $rk->STATUS_KOLEKTIF; ?></td>
                      <td><?php echo $rk->TGL_TERIMA ?></td>
                      <td><?php echo $rk->TGL_SELESAI ?></td>
                      <!-- <td align="center"><?php echo $rk->TGL_TERIMA ?></td>
                          <td align="center"><?php echo $rk->TGL_SELESAI ?></td> -->
                    </tr>
                  <?php $no++;
                  } ?>
                </tbody>
              </table>
            </div><!-- /.box-body -->
          </div><!-- /.box -->
        </section><!-- /.content -->