        <section class="content-header">
          <h1>
            Rekap Pelayanan
            <div class="pull-right">

            </div>
          </h1>

        </section>
        <!-- Main content -->
        <section class="content">
          <!-- Default box -->
          <div class="box">
            <div class="box-header with-border">

            </div>
            <div class="box-body">
              <form class="form-inline" method="post" action="<?php echo base_url() . 'laporan/permohonanrekap.html' ?>">
                <div class="form-group">
                  Tanggal permohonan
                </div>
                <div class="form-group">
                  <input type="text" class="form-control tanggal" name="tgl1" value="<?php echo $tgl1 ?>" reuired placeholder="Tanggal mulai">
                </div>
                <div class="form-group">
                  <input type="text" class="form-control tanggal" name="tgl2" value="<?php echo $tgl2 ?>" reuired placeholder="Tanggal akhir">
                </div>
                <button type="submit" class="btn btn-default"><i class="fa fa-search"></i> Cari</button>
                <?php if (isset($rk)) {
                  echo anchor('laporan/execelJumlahPelayanan/' . $tgl1 . '/' . $tgl2, '<i class="fa fa-print"></i> Excel', 'class="btn btn-sm btn-success"');
                } ?>
              </form>

              <?php if (isset($rk)) { ?>
                <hr>
                <table class="table table-bordered table-striped" id="example3">
                  <thead>
                    <tr>
                      <th width="3%">No</th>
                      <th align="center">Jenis Pelayanan</th>
                      <th align="center">Jumlah Pelayanan</th>
                      <!-- <th>Jumlah</th> -->
                      <th align="center">Detail</th>
                    </tr>
                  </thead>
                  <tbody>
                    <?php $no = 1;
                    foreach ($rk as $rk) { ?>
                      <tr>
                        <td align="center"><?php echo $no ?></td>
                        <td><?php echo $rk->NM_JENIS_PELAYANAN ?></td>
                        <td align="center"><?php echo $rk->JUM ?></td>
                        <td align="center">
                          <!-- <a href="#" class="edit-record" data-id="<?php echo $rk->KD_JNS_PELAYANAN ?>">Show</a> | -->
                          <?php echo anchor('laporan/permohonanDetailNopel/' . $tgl1 . '/' . $tgl2 . '/' . $rk->KD_JNS_PELAYANAN, '<i class="fa fa-binoculars"></i> ', 'class="badge"'); ?>
                        </td>
                      </tr>
                    <?php $no++;
                    } ?>
                  </tbody>
                </table>

              <?php } ?>
            </div><!-- /.box-body -->
          </div><!-- /.box -->
        </section><!-- /.content -->

        <!-- Modal -->
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
          <div class="modal-dialog modal-lg">
            <div class="modal-content">

            </div>
          </div>
        </div>

        <script>
          $(function() {
            $(document).on('click', '.edit-record', function(e) {
              e.preventDefault();
              $("#myModal").modal('show');
              $.post('<?php echo base_url() . "laporan/detailmodalrekappermohonan" ?>', {
                  id: $(this).attr('data-id'),
                  tgl1: '<?php echo $tgl1 ?>',
                  tgl2: '<?php echo $tgl2 ?>'
                },
                function(html) {
                  $(".modal-content").html(html);
                }
              );
            });
          });
        </script>