 <?php
  header("Content-type: application/vnd-ms-excel");
  header("Content-Disposition: attachment; filename=laporanJumlahSpptdanLayanan" . date('YmdHis') . ".xls");
  echo $judul;

  ?>
 <br><br>
 <table border="1" cellspacing="0">
   <thead>
     <tr>
       <th width="3%">No</th>
       <th align="center">Jenis Pelayanan</th>
       <th align="center">Jumlah Pelayanan</th>
       <th align="center">SPPT</th>
     </tr>
   </thead>
   <tbody>
     <?php $no = 1;
      foreach ($rk as $rk) { ?>
       <tr>
         <td align="center"><?php echo $no ?></td>
         <td><?php echo $rk->NM_JENIS_PELAYANAN ?></td>
         <td align="center"><?php echo $rk->NOPEL ?></td>
         <td align="center"><?php echo $rk->JSPPT ?></td>
       </tr>
     <?php $no++;
      } ?>
   </tbody>
 </table>