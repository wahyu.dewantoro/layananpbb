        <section class="content-header">
          <h1>
            Detail Penerimaan NIP : <?php echo $nip?>
            <?php //echo $judul?>
            <div class="pull-right">
              <?php echo anchor('laporan/penerimaPermohon','<i class="fa fa-list"></i> Kembali','class="btn btn-sm btn-success"');?>
          </div>
          </h1>
          
        </section>
        <!-- Main content -->
        <section class="content">
          <!-- Default box -->
          <div class="box">
            <div class="box-header with-border">
              
            </div>
            <div class="box-body">
                  
                  <table class="table table-bordered table-striped" id="example4">
                      <thead>
                          <tr>
                            <th width="3%">No</th>
                            <th>No Layanan</th>
                            <th>Pemohon</th>
                            <th>Tanggal terima</th>
                            <th>Status Kolektif</th>
                            <th>Jumlah</th>
                          </tr>
                      </thead>
                      <tbody>
                        <?php $no=1; foreach($rk as $rk){?>
                        <tr>
                          <td><?php echo $no?></td>
                          <td><?= $rk->NO_LAYANAN?></td>
                          <td><?= $rk->NAMA_PEMOHON?></td>
                          <td><?= $rk->TGL_TERIMA?></td>
                          <td><?= $rk->STATUS_KOLEKTIF?></td>
                          <td><?= $rk->JUMLAH?></td>
                        </tr>
                        <?php $no++; }?>
                      </tbody>
                  </table>

            </div><!-- /.box-body -->
          </div><!-- /.box -->
        </section><!-- /.content -->