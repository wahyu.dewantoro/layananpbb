 <?php
 header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=execellappermohonanbankjatim".date('YmdHis').".xls");
  echo $judul;

  ?>
  <br><br>
                  <table  border="1" cellspacing="0">
                      <thead>
                          <tr>
                            <th width="3%">No</th>
                            <th align="center">NOP</th>
                            <th align="center">Nama WP</th>
                            <th align="center">Alamat WP</th>
                            <th align="center">Pokok</th>
                            <th align="center">Denda</th>
                            <th align="center">Total</th>
                            <th align="center">Tanggal Bayar</th>
                            <th align="center">Tanggal Rekam</th>
                            <th align="center">Kode Pengesahan</th>
                            <th align="center">Bank</th>
                          </tr>
                      </thead>
                      <tbody>
                        <?php $no=1; foreach($rk as $rk){?>
                        <tr>
                          <td align="center"><?php echo $no?></td>
                          <td><?php echo $rk->KD_PROPINSI.'.'.$rk->KD_DATI2.'.'.$rk->KD_KECAMATAN.'.'.$rk->KD_KELURAHAN.'.'.$rk->KD_BLOK.'.'.$rk->NO_URUT.'.'.$rk->KD_JNS_OP?></td>
                          <td><?php echo $rk->NM_WP_SPPT?></td>
                          <td><?php echo 'DS. '.$rk->NM_KELURAHAN.', KEC.'.$rk->NM_KECAMATAN;?></td>
                          <td align="right"><?php echo number_format($rk->POKOK,0,'','.');?></td>
                          <td align="right"><?php echo number_format($rk->DENDA,0,'','.');?></td>
                          <td align="right"><?php echo number_format($rk->TOTAL,0,'','.');?></td>
                          <td align="center"><?php echo $rk->TANGGAL_BAYAR?></td>
                          <td align="center"><?php echo $rk->TANGGAL_REKAM?></td>
                          <td align="center"><?php echo "'".$rk->PENGESAHAN?></td>
                          <td align="center"><?php $nm_bank=""; if($rk->KODE_BANK_BAYAR==''){ $nm_bank="Bank Jatim"; }else{ $cb = $this->db->query("SELECT NAMA_BANK FROM REF_BANK WHERE KODE_BANK=$rk->KODE_BANK_BAYAR")->row(); $nm_bank=$cb->NAMA_BANK; }
                          echo $nm_bank?></td>                       
                        </tr>
                        <?php $no++;}?>
                      </tbody>
                  </table>
 