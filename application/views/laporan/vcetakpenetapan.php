<!DOCTYPE html>
<html>
<head>
	<title>Penetapan pajak</title>
	<style type="text/css">
		body {
		  font-family: "Courier New";
		  font-size: 13pt;
		  line-height: 1.42857143;
		  color: #333;
		  background-color: #fff;
		}

		.center {
		    margin-left: auto;
		    margin-right: auto;
		}



	.table { border-collapse: collapse; }
    .table tbody tr { border: none; }
    .table thead  th {
    	 border: none;
        
    }

    .table tfoot  td {
    	 border: none;
    }
		 
	</style>
</head>
<body>
<?php   $no=0; foreach($rk as $rk){ if($no!=0){?>
		<pagebreak></pagebreak>
		<?php } ?>
	<table  class="center"  >
		<tbody>
			<tr>
				<td align="center">PEMERINTAH KABUPATEN MALANG</td>
			</tr>
			<tr>
				<td align="center">DINAS PENDAPATAN DAERAH</td>
			</tr>
		</tbody>
	</table>
	<br><br>
	<table class="center">
		<tr>
			<td>PROPINSI</td>
			<td>:</td>
			<td>35 - Jawa Timur</td>
		</tr>
		<tr>
			<td>DATI II</td>
			<td>:</td>
			<td>07 - Kabupaten Malang</td>
		</tr>
		<tr>
			<td>KECAMATAN</td>
			<td>:</td>
			<td><?php $this->Mmaster->getkecamatan($rk->KD_PROPINSI,$rk->KD_DATI2,$rk->KD_KECAMATAN) ?></td>
		</tr>
		<tr>
			<td>KELURAHAN / DESA</td>
			<td>:</td>
			<td><?php $this->Mmaster->getkelurahan($rk->KD_PROPINSI,$rk->KD_DATI2,$rk->KD_KECAMATAN,$rk->KD_KELURAHAN) ?></td>
		</tr>
	</table>
	<br> 
	<table class="center">
		<tr>
			<td align="center">DAFTAR HIMPUNAN KETETAPAN PAJAK & PEMBAYARAN BUKU I, II</td>
		</tr>
		<tr>
			<td align="center">(DHKP)</td>
		</tr>
		<tr>
			<td align="center">PAJAK BUMI DAN BANGUNAN</td>
		</tr>
		<tr>
			<td align="center">TAHUN : <?php  echo $rk->THN_PAJAK_SPPT?></td>
		</tr>
	</table>
	<br>
	<table class="center">
		<tr>
			<td>DAFTAR INI TERDIRI ATAS</td>
			<td>:</td>
			<td> &nbsp;&nbsp;&nbsp; Halaman</td>
		</tr>
		<tr>
			<td>JUMLAH STTS SEBANYAK</td>
			<td>:</td>
			<td><?php  echo $rk->JSPPT?> Lembar</td>
		</tr>
		<tr>
			<td>JUMLAH SPPT SEBANYAK</td>
			<td>:</td>
			<td> <?php  echo $rk->JSPPT?> Lembar</td>
		</tr>
	</table>
	<br> 
	<table style="border:solid 1px #000;" cellspacing="0" class="center" cellpadding="0" >
		<tr>
			<td>
	<table class="center table" >
		<thead>
		<tr>
			<th style="border-bottom: solid 1px #000; border-right: solid 1px #000;" width="15%">Buku</th>
			<th style="border-bottom: solid 1px #000; border-right: solid 1px #000;" width="20%">Jumlah Objek</th>
			<th style="border-bottom: solid 1px #000; border-right: solid 1px #000;" width="20%">Luas Tanah</th>
			<th style="border-bottom: solid 1px #000; border-right: solid 1px #000;" width="20%">Luas Bangunan</th>
			<th style="border-bottom: solid 1px #000;" width="25%">Pokok Ketetapan</th>
		</tr>
		</thead>
		<tbody>
		<tr>
			<td style="border-right: solid 1px #000; border-left: solid 1px #000; " align="right">I </td>
			<td style="border-right: solid 1px #000; " align="right"><?php  echo number_format($rk->OBJ1,'0','','.') ?></td>
			<td style="border-right: solid 1px #000; " align="right"><?php  echo number_format($rk->LT1,'0','','.') ?></td>
			<td style="border-right: solid 1px #000; " align="right"><?php  echo number_format($rk->LB1,'0','','.') ?></td>
			<td style="border-right: solid 1px #000; " align="right"><?php  echo number_format($rk->PBB1,'0','','.') ?></td>
		</tr>
		<tr>
			<td style="border-right: solid 1px #000; border-left: solid 1px #000; " align="right">II </td>
			<td style="border-right: solid 1px #000; " align="right"><?php  echo number_format($rk->OBJ2,'0','','.') ?></td>
			<td style="border-right: solid 1px #000; " align="right"><?php  echo number_format($rk->LT2,'0','','.') ?></td>
			<td style="border-right: solid 1px #000; " align="right"><?php  echo number_format($rk->LB2,'0','','.') ?></td>
			<td style="border-right: solid 1px #000; " align="right"><?php  echo number_format($rk->PBB2,'0','','.') ?></td>
		</tr>
		<tr>
			<td style="border-right: solid 1px #000;  border-left: solid 1px #000;" align="right">III </td>
			<td style="border-right: solid 1px #000; " align="right"><?php echo '-'; ?></td>
			<td style="border-right: solid 1px #000; " align="right"><?php echo '-'; ?></td>
			<td style="border-right: solid 1px #000; " align="right"><?php echo '-'; ?></td>
			<td style="border-right: solid 1px #000; " align="right"><?php echo '-'; ?></td>
		</tr>
		<tr>
			<td style="border-right: solid 1px #000;  border-left: solid 1px #000;" align="right">IV </td>
			<td style="border-right: solid 1px #000; " align="right"><?php echo '-'; ?></td>
			<td style="border-right: solid 1px #000; " align="right"><?php echo '-'; ?></td>
			<td style="border-right: solid 1px #000; " align="right"><?php echo '-'; ?></td>
			<td style="border-right: solid 1px #000; " align="right"><?php echo '-'; ?></td>
		</tr>
		<tr>
			<td style="border-right: solid 1px #000; border-left: solid 1px #000; " align="right">V</td>
			<td style="border-right: solid 1px #000; " align="right"><?php echo '-'; ?></td>
			<td style="border-right: solid 1px #000; " align="right"><?php echo '-'; ?></td>
			<td style="border-right: solid 1px #000; " align="right"><?php echo '-'; ?></td>
			<td style="border-right: solid 1px #000; " align="right"><?php echo '-'; ?></td>
		</tr>
		</tbody>
		<tfoot>
		<tr>
			<td  align="center" style="border-top: solid 1px #000; border-right: solid 1px #000;">Jumlah</td>
			<td style="border-top: solid 1px #000; border-right: solid 1px #000;" align="right"><?php  echo number_format($rk->OBJ1+$rk->OBJ2,'0','','.')?></td>
			<td style="border-top: solid 1px #000; border-right: solid 1px #000;" align="right"><?php  echo number_format($rk->LT1+$rk->LT2,'0','','.')?></td>
			<td style="border-top: solid 1px #000; border-right: solid 1px #000;" align="right"><?php  echo number_format($rk->LB1+$rk->LB2,'0','','.')?></td>
			<td style="border-top: solid 1px #000;  " align="right"><?php  echo number_format($rk->PBB1+$rk->PBB2,'0','','.')?></td>
		</tr>
		</tfoot>
	</table>
			</td>
		</tr>
	</table>
	<p style="text-align:center"><?php  $res=$this->Mmaster->terbilang($rk->PBB1+$rk->PBB2); echo strtoupper($res.' RUPIAH');?></p>
	<br>
	<table class="center">
			<tr>
				<td align="center">MALANG, 02 JANUARI 2018</td>
			</tr>
			<tr>
				<td align="center">KEPALA BADAN PENDAPATAN DAERAH</td>
			</tr>
	</table>
	<br><br><br><br>
	<table class="center">
		<tr>
			<td align="center"> DRS. WILLEM P. SALAMENA, MM</td>
		</tr>
		<tr>
			<td align="center">196001081986081002</td>
		</tr>
	</table>
	<br>
Perhatian : <br>
- Halaman pertama dan terakhir ditanda tangani, halaman lainya diparaf<br>
- Pajak terhuhtang harus lunas selambat - lambatnya 6 (enam) bulan sejak diterima SPPT<br>
<?php $no++; } ?>

</body>
</html>