<!DOCTYPE html>
<html>
<head>
	<title>Penetapan pajak</title>
	<style type="text/css">
		.center {
		    margin-left: auto;
		    margin-right: auto;
		}

		body {
		  font-family: "Courier New";
		  font-size: 12pt;
		 
		  background-color: #fff;
		}



	.table { border-collapse: collapse; }
    .table tbody tr { border: none; }
    .table thead  th {
    	 border: none;
        
    }

    .table tfoot  td {
    	 border: none;
    }
		 
	</style>
</head>
<body>

<?php 
	$ee=explode(',',$BUKU);

	// print_r($ee);


	$vb='';
	foreach($ee as $ece){
		$vb.=romawibuku($ece).',';
	}
	$bbb=substr($vb,0,-1);
?>
<!-- <pagebreak></pagebreak> -->
 
	<table  class="center"  >
		<tbody>
			<tr>
				<td align="center">PEMERINTAH KABUPATEN MALANG</td>
			</tr>
			<tr>
				<td align="center">DINAS PENDAPATAN DAERAH</td>
			</tr>
		</tbody>
	</table>
	<br><br>
	<table class="center">
		<tr>
			<td>PROPINSI</td>
			<td>:</td>
			<td>35 - Jawa Timur</td>
		</tr>
		<tr>
			<td>DATI II</td>
			<td>:</td>
			<td>07 - Kabupaten Malang</td>
		</tr>
		<tr>
			<td>KECAMATAN</td>
			<td>:</td>
			<td><?php $this->Mmaster->getkecamatan($rgs->KD_PROPINSI,$rgs->KD_DATI2,$rgs->KD_KECAMATAN) ?></td>
		</tr>
		<tr>
			<td>KELURAHAN / DESA</td>
			<td>:</td>
			<td><?php $this->Mmaster->getkelurahan($rgs->KD_PROPINSI,$rgs->KD_DATI2,$rgs->KD_KECAMATAN,$rgs->KD_KELURAHAN); ?></td>
		</tr>
	</table>
	<br> 
	<table class="center">
		<tr>
			<td align="center">DAFTAR HIMPUNAN KETETAPAN PAJAK & PEMBAYARAN BUKU <?php echo $bbb?></td>
		</tr>
		<tr>
			<td align="center">(DHKP)</td>
		</tr>
		<tr>
			<td align="center">PAJAK BUMI DAN BANGUNAN</td>
		</tr>
		<tr>
			<td align="center">TAHUN : <?php  echo $rgs->THN_PAJAK_SPPT?></td>
		</tr>
	</table>
	<br>
	<table class="center">
		<tr>
			<td>DAFTAR INI TERDIRI ATAS</td>
			<td>:</td>
			<td><?php echo $hal; ?> Halaman</td>
		</tr>
		<tr>
			<td>JUMLAH STTS SEBANYAK</td>
			<td>:</td>
			<td><?php  echo $rgs->JSPPT?> Lembar</td>
		</tr>
		<tr>
			<td>JUMLAH SPPT SEBANYAK</td>
			<td>:</td>
			<td> <?php  echo $rgs->JSPPT?> Lembar</td>
		</tr>
	</table>
	<br> 
	<table style="border:solid 1px #000;" cellspacing="0" class="center" cellpadding="0" >
		<tr>
			<td>
	<table class="center table" >
		<thead>
		<tr>
			<th style="border-bottom: solid 1px #000; border-right: solid 1px #000;" width="15%">Buku</th>
			<th style="border-bottom: solid 1px #000; border-right: solid 1px #000;" width="20%">Jumlah Objek</th>
			<th style="border-bottom: solid 1px #000; border-right: solid 1px #000;" width="20%">Luas Tanah</th>
			<th style="border-bottom: solid 1px #000; border-right: solid 1px #000;" width="20%">Luas Bangunan</th>
			<th style="border-bottom: solid 1px #000;" width="25%">Pokok Ketetapan</th>
		</tr>
		</thead>
		<tbody>
		<?php 
			$to =0;
			$lt =0;
			$lb =0;
			$tp =0;
			?>
		
		<?php if(in_array(1, $ee)){ 
				$to +=$rgs->OBJ1;
				$lt +=$rgs->LT1;
				$lb +=$rgs->LB1;
				$tp +=$rgs->PBB1;

				?>
			<tr>
				<td style="border-right: solid 1px #000; border-left: solid 1px #000; " align="right">I </td>
				<td style="border-right: solid 1px #000; " align="right"><?php  echo number_format($rgs->OBJ1,'0','','.') ?></td>
				<td style="border-right: solid 1px #000; " align="right"><?php  echo number_format($rgs->LT1,'0','','.') ?></td>
				<td style="border-right: solid 1px #000; " align="right"><?php  echo number_format($rgs->LB1,'0','','.') ?></td>
				<td style="border-right: solid 1px #000; " align="right"><?php  echo number_format($rgs->PBB1,'0','','.') ?></td>
			</tr>
			<?php  }else{ 
				$to +=0;
				$lt +=0;
				$lb +=0;
				$tp +=0;

				?>
			<tr>
				<td style="border-right: solid 1px #000; border-left: solid 1px #000; " align="right">I </td>
				<td style="border-right: solid 1px #000; " align="right"> -</td>
				<td style="border-right: solid 1px #000; " align="right"> -</td>
				<td style="border-right: solid 1px #000; " align="right"> -</td>
				<td style="border-right: solid 1px #000; " align="right"> -</td>
			</tr>
			<?php } ?>

			<?php if(in_array(2, $ee)){ 
				$to +=$rgs->OBJ2;
				$lt +=$rgs->LT2;
				$lb +=$rgs->LB2;
				$tp +=$rgs->PBB2;

				?>
			<tr>
				<td style="border-right: solid 1px #000; border-left: solid 1px #000; " align="right">II </td>
				<td style="border-right: solid 1px #000; " align="right"><?php  echo number_format($rgs->OBJ2,'0','','.') ?></td>
				<td style="border-right: solid 1px #000; " align="right"><?php  echo number_format($rgs->LT2,'0','','.') ?></td>
				<td style="border-right: solid 1px #000; " align="right"><?php  echo number_format($rgs->LB2,'0','','.') ?></td>
				<td style="border-right: solid 1px #000; " align="right"><?php  echo number_format($rgs->PBB2,'0','','.') ?></td>
			</tr>
			<?php  }else{ 
				$to +=0;
				$lt +=0;
				$lb +=0;
				$tp +=0;

				?>
			<tr>
				<td style="border-right: solid 1px #000; border-left: solid 1px #000; " align="right">II </td>
				<td style="border-right: solid 1px #000; " align="right"> -</td>
				<td style="border-right: solid 1px #000; " align="right"> -</td>
				<td style="border-right: solid 1px #000; " align="right"> -</td>
				<td style="border-right: solid 1px #000; " align="right"> -</td>
			</tr>
			<?php } ?>


			<?php if(in_array(3, $ee)){ 
				$to +=$rgs->OBJ3;
				$lt +=$rgs->LT3;
				$lb +=$rgs->LB3;
				$tp +=$rgs->PBB3;

				?>
			<tr>
				<td style="border-right: solid 1px #000; border-left: solid 1px #000; " align="right">III </td>
				<td style="border-right: solid 1px #000; " align="right"><?php  echo number_format($rgs->OBJ3,'0','','.') ?></td>
				<td style="border-right: solid 1px #000; " align="right"><?php  echo number_format($rgs->LT3,'0','','.') ?></td>
				<td style="border-right: solid 1px #000; " align="right"><?php  echo number_format($rgs->LB3,'0','','.') ?></td>
				<td style="border-right: solid 1px #000; " align="right"><?php  echo number_format($rgs->PBB3,'0','','.') ?></td>
			</tr>
			<?php  }else{ 
				$to +=0;
				$lt +=0;
				$lb +=0;
				$tp +=0;

				?>
			<tr>
				<td style="border-right: solid 1px #000; border-left: solid 1px #000; " align="right">III </td>
				<td style="border-right: solid 1px #000; " align="right"> -</td>
				<td style="border-right: solid 1px #000; " align="right"> -</td>
				<td style="border-right: solid 1px #000; " align="right"> -</td>
				<td style="border-right: solid 1px #000; " align="right"> -</td>
			</tr>
			<?php } ?>

		<?php if(in_array(4, $ee)){ 
				$to +=$rgs->OBJ4;
				$lt +=$rgs->LT4;
				$lb +=$rgs->LB4;
				$tp +=$rgs->PBB4;

				?>
			<tr>
				<td style="border-right: solid 1px #000; border-left: solid 1px #000; " align="right">IV </td>
				<td style="border-right: solid 1px #000; " align="right"><?php  echo number_format($rgs->OBJ4,'0','','.') ?></td>
				<td style="border-right: solid 1px #000; " align="right"><?php  echo number_format($rgs->LT4,'0','','.') ?></td>
				<td style="border-right: solid 1px #000; " align="right"><?php  echo number_format($rgs->LB4,'0','','.') ?></td>
				<td style="border-right: solid 1px #000; " align="right"><?php  echo number_format($rgs->PBB4,'0','','.') ?></td>
			</tr>
			<?php  }else{ 
				$to +=0;
				$lt +=0;
				$lb +=0;
				$tp +=0;

				?>
			<tr>
				<td style="border-right: solid 1px #000; border-left: solid 1px #000; " align="right">IV </td>
				<td style="border-right: solid 1px #000; " align="right"> -</td>
				<td style="border-right: solid 1px #000; " align="right"> -</td>
				<td style="border-right: solid 1px #000; " align="right"> -</td>
				<td style="border-right: solid 1px #000; " align="right"> -</td>
			</tr>
			<?php } ?>


		<?php if(in_array(5, $ee)){ 
				$to +=$rgs->OBJ5;
				$lt +=$rgs->LT5;
				$lb +=$rgs->LB5;
				$tp +=$rgs->PBB5;

				?>
			<tr>
				<td style="border-right: solid 1px #000; border-left: solid 1px #000; " align="right">V </td>
				<td style="border-right: solid 1px #000; " align="right"><?php  echo number_format($rgs->OBJ5,'0','','.') ?></td>
				<td style="border-right: solid 1px #000; " align="right"><?php  echo number_format($rgs->LT5,'0','','.') ?></td>
				<td style="border-right: solid 1px #000; " align="right"><?php  echo number_format($rgs->LB5,'0','','.') ?></td>
				<td style="border-right: solid 1px #000; " align="right"><?php  echo number_format($rgs->PBB5,'0','','.') ?></td>
			</tr>
			<?php  }else{ 
				$to +=0;
				$lt +=0;
				$lb +=0;
				$tp +=0;

				?>
			<tr>
				<td style="border-right: solid 1px #000; border-left: solid 1px #000; " align="right">V </td>
				<td style="border-right: solid 1px #000; " align="right"> -</td>
				<td style="border-right: solid 1px #000; " align="right"> -</td>
				<td style="border-right: solid 1px #000; " align="right"> -</td>
				<td style="border-right: solid 1px #000; " align="right"> -</td>
			</tr>
			<?php } ?>

		
		<!-- <tr>
			<td style="border-right: solid 1px #000; border-left: solid 1px #000; " align="right">II </td>
			<td style="border-right: solid 1px #000; " align="right"><?php  echo number_format($rgs->OBJ2,'0','','.') ?></td>
			<td style="border-right: solid 1px #000; " align="right"><?php  echo number_format($rgs->LT2,'0','','.') ?></td>
			<td style="border-right: solid 1px #000; " align="right"><?php  echo number_format($rgs->LB2,'0','','.') ?></td>
			<td style="border-right: solid 1px #000; " align="right"><?php  echo number_format($rgs->PBB2,'0','','.') ?></td>
		</tr>
		<tr>
			<td style="border-right: solid 1px #000;  border-left: solid 1px #000;" align="right">III </td>
			<td style="border-right: solid 1px #000; " align="right"><?php echo '-'; ?></td>
			<td style="border-right: solid 1px #000; " align="right"><?php echo '-'; ?></td>
			<td style="border-right: solid 1px #000; " align="right"><?php echo '-'; ?></td>
			<td style="border-right: solid 1px #000; " align="right"><?php echo '-'; ?></td>
		</tr>
		<tr>
			<td style="border-right: solid 1px #000;  border-left: solid 1px #000;" align="right">IV </td>
			<td style="border-right: solid 1px #000; " align="right"><?php echo '-'; ?></td>
			<td style="border-right: solid 1px #000; " align="right"><?php echo '-'; ?></td>
			<td style="border-right: solid 1px #000; " align="right"><?php echo '-'; ?></td>
			<td style="border-right: solid 1px #000; " align="right"><?php echo '-'; ?></td>
		</tr>
		<tr>
			<td style="border-right: solid 1px #000; border-left: solid 1px #000; " align="right">V</td>
			<td style="border-right: solid 1px #000; " align="right"><?php echo '-'; ?></td>
			<td style="border-right: solid 1px #000; " align="right"><?php echo '-'; ?></td>
			<td style="border-right: solid 1px #000; " align="right"><?php echo '-'; ?></td>
			<td style="border-right: solid 1px #000; " align="right"><?php echo '-'; ?></td>
		</tr> -->
		</tbody>
		<tfoot>
		<tr>
			<td  align="center" style="border-top: solid 1px #000; border-right: solid 1px #000;">Jumlah</td>
			<td style="border-top: solid 1px #000; border-right: solid 1px #000;" align="right"><?php  echo number_format($to,'0','','.')?></td>
			<td style="border-top: solid 1px #000; border-right: solid 1px #000;" align="right"><?php  echo number_format($lt,'0','','.')?></td>
			<td style="border-top: solid 1px #000; border-right: solid 1px #000;" align="right"><?php  echo number_format($lb,'0','','.')?></td>
			<td style="border-top: solid 1px #000;  " align="right"><?php  echo number_format($tp,'0','','.')?></td>
		</tr>
		</tfoot>
	</table>
			</td>
		</tr>
	</table>
	<p style="text-align:center"><?php  $res=$this->Mmaster->terbilang($rgs->PBB1+$rgs->PBB2); echo strtoupper($res.' RUPIAH');?></p>
	<br>
	<table class="center">
			<tr>
				<td align="center">MALANG, 02 JANUARI <?php  echo $rgs->THN_PAJAK_SPPT?></td>
			</tr>
			<tr>
				<td align="center">KEPALA BADAN PENDAPATAN DAERAH</td>
			</tr>
	</table>
	<br><br><br><br>
	<table class="center">
		<tr>
			<td align="center"> DRS. WILLEM P. SALAMENA, MM</td>
		</tr>
		<tr>
			<td align="center">196001081986081002</td>
		</tr>
	</table>
	<br>
Perhatian : <br>
- Halaman pertama dan terakhir ditanda tangani, halaman lainya diparaf<br>
- Pajak terhuhtang harus lunas selambat - lambatnya 6 (enam) bulan sejak diterima SPPT<br>
 

</body>
</html>