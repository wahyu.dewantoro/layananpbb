<section class="content-header">
  <h1>
    Form Cetak
    <div class="pull-right">
  </div>
  </h1>
  
</section>
  <!-- Main content -->
  <section class="content">
    <div class="row">
        <div class="col-md-6">
            <div class="box box-primary">
                <div class="box-header with-border">
                    <div class="box-title"> 
                      DHKP
                    </div>
                </div>
                <div class="box-body">
                    <form class="form-horizontal" method="post" action="<?php echo base_url().'laporan/cetakdhkp.html'?>">
                        <div class="form-group ">
                            <label class="control-label col-md-3">Tahun <small>*</small></label>
                            <div class="col-md-9">
                                 <select   class="form-control chosen" required name="THN_PAJAK_SPPT">
                                    <option value=""></option>
                                    <?php for($i=date('Y');$i>=2010;$i--){?>
                                    <option value="<?php echo $i?>"><?php echo $i ?></option>
                                    <?php }?>
                                 </select>
                                 <!-- <input type="text"  value="<?php echo date('Y')?>" > -->
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Propinsi <small>*</small></label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" disabled="" value="35 - Jawa Timur">
                                <input type="hidden" id="KD_PROPINSI" name="KD_PROPINSI" value="35">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Kabupaten / Kota <small>*</small></label>
                            <div class="col-md-9">
                                <input type="text"  class="form-control" disabled="" value="07 - Kabupaten Malang">
                                <input type="hidden" id="KD_DATI2" name="KD_DATI2" value="07">
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Kecamatan <small>*</small></label>
                            <div class="col-md-9">
                                 <select class="form-control chosen" required id="KD_KECAMATAN" name="KD_KECAMATAN">
                                    <option value=""> </option>
                                    <?php foreach($kec as $kec){?>
                                    <option value="<?php echo $kec->KD_KECAMATAN?>"><?php echo $kec->KD_KECAMATAN.' - '.$kec->NM_KECAMATAN?></option>
                                    <?php } ?>
                                 </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Kelurahan <small>*</small></label>
                            <div class="col-md-9">
                                 <select class="form-control chosen" required id="KD_KELURAHAN" name="KD_KELURAHAN">
                                    <option value=""></option>
                                 </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Buku <small>*</small></label>
                            <div class="col-md-9">
                                 <select class="form-control chosen" multiple data-placeholder="Silahkan Pilih" required id="BUKU" name="BUKU[]">
                                    <option value=""></option>
                                    <option value="1">Buku I</option>
                                    <option value="2">Buku II</option>
                                    <option value="3">Buku III</option>
                                    <option value="4">Buku IV</option>
                                    <option value="5">Buku V</option>
                                 </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-9 col-md-offset-3">
                                <button type="submit" name="sub" class="btn btn-sm btn-success"><i class="fa fa-binoculars"></i> Preview</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- <div class="col-md-6">
              <div class="box">
                <div class="box-header with-border">
                    <div class="box-title">
                     Cover DHKP  Cetak Semua  
                    </div>
                    
                </div>
                <div class="box-body">
                      
                              <a target="_blank" href="<?php echo base_url().'laporan/cetakpenetapanpajak'?>">
                              <div class="panel panel-success text-center">
                                  <div class="panel-body" style="background-color:green ">
                                    <i class="fa fa-print fa-2x"></i> cetak cover DHKP
                                  </div>
                              </div>
                              </a>
                      
                </div> 
              </div> 
        </div> -->
    </div>
  
  </section><!-- /.content -->

<script type="text/javascript">
$("#KD_KECAMATAN").change(function(){
    var KD_PROPINSI  = $("#KD_PROPINSI").val();
    var KD_DATI2     = $("#KD_DATI2").val();
    var KD_KECAMATAN = $("#KD_KECAMATAN").val();
    // alert(KD_KECAMATAN);

    $.ajax({
         type: "POST",
        url: "<?php echo base_url().'laporan/ambilkelurahan'?>",
        data: "KD_KECAMATAN="+KD_KECAMATAN+"&KD_PROPINSI="+KD_PROPINSI+"&KD_DATI2="+KD_DATI2,
        cache: false,
        success: function(msga){
            // alert(msga);
            $("#KD_KELURAHAN").html(msga);
        }
    });
  });
</script>