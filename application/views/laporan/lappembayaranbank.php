        <section class="content-header">
          <h1>
            Laporan Pembayaran Bank
            <div class="pull-right">
             
          </div>
          </h1>
          
        </section>
        <!-- Main content -->
        <section class="content">
          <!-- Default box -->
          <div class="box">
            <div class="box-header with-border">
              
            </div>
            <div class="box-body">
                  <form class="form-inline" method="post" action="<?php echo base_url().'laporan/lapPembayaranBank.html'?>">
                     <div class="form-group">
                      Tahun Pajak
                    </div>
                    <div class="form-group">
                      <select class="form-control" name="tahun">
                        <option value="all">Semua</option>
                        <?php for($q=date('Y');$q>2009;$q--){?>
                                  <option <?php if($q==$tahun){echo "selected";}?> value="<?= $q ?>"><?= $q ?></option>
                                <?php } ?>
                      </select>
                    </div>
                    <div class="form-group">
                      Tanggal
                    </div>
                    <div class="form-group">
                      <input style="width: 90px;" type="text" class="form-control tanggal" name="tgl1" value="<?php echo $tgl1?>" reuired placeholder="Tanggal mulai">
                    </div>
                    <div class="form-group">
                      s/d
                    </div>
                    <div class="form-group">
                      <input style="width: 90px;" type="text" class="form-control tanggal" name="tgl2" value="<?php echo $tgl2?>" reuired placeholder="Tanggal akhir">
                    </div>
                    <div class="form-group">
                      Kecamatan
                    </div>
                    <div class="form-group">
                      <select class="form-control" name="KD_KECAMATAN" id="KD_KECAMATAN">
                                    <option value="">Pilih</option>
                                    <?php foreach($kec as $kec){?>
                                      <option <?php if($kec->KD_KECAMATAN==$KD_KECAMATAN){echo "selected";}?> value="<?= $kec->KD_KECAMATAN ?>"><?= $kec->KD_KECAMATAN.' '.$kec->NM_KECAMATAN ?></option>
                                    <?php } ?>
                      </select>
                    </div>
                     <div class="form-group">
                      Kelurahan
                    </div>
                    <div class="form-group">
                      <select class="form-control"  name="KD_KELURAHAN" id="KD_KELURAHAN">
                                    <option value="">Pilih</option>
                                    <?php if(isset($kel)){?><option value="">000 Semua Kelurahan</option><?php }?>
                                    <?php 
                                      foreach($kel as $kela){?>
                                        <option  <?php if($kela->KD_KELURAHAN==$KD_KELURAHAN){echo "selected";}?> value="<?= $kela->KD_KELURAHAN?>"><?= $kela->KD_KELURAHAN.' '.$kela->NM_KELURAHAN?></option>
                                    <?php  }?>
                                  </select>
                    </div>
                    <div class="form-group">
                      Bank
                    </div>
                    <div class="form-group">
                      <select class="form-control" name="bank">
                        <option value="">Semua</option>
                        <?php foreach($bank as $bank){?>
                                      <option <?php if($bank->KODE_BANK==$KD_BANK){echo "selected";}?> value="<?= $bank->KODE_BANK ?>"><?= $bank->KODE_BANK.' '.$bank->NAMA_BANK ?></option>
                                    <?php } ?>
                      </select>
                    </div>
                    <input type="hidden" name="cari" value="1">
                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i> Cari</button>
                    <?php if(isset($rk)){ echo anchor('laporan/excellappermohonanbank?'.$where,'<i class="fa fa-print"></i> Excel','class="btn btn-sm btn-success"');}?>
                    
                  </form>

                  <?php if(isset($rk)){?>
                  <hr>
                  <table class="table table-bordered table-striped" id="example3">
                      <thead>
                          <tr>
                            <th width="3%">No</th>
                            <th align="center" width="12%">NOP</th>
                            <th align="center" width="10%">Nama WP</th>
                            <th align="center" width="10%">Alamat WP</th>
                            <th align="center">Pokok</th>
                            <th align="center">Denda</th>
                            <th align="center">Total</th>
                            <th align="center" width="7%">Tanggal Bayar</th>
                            <th align="center" width="10%">Tanggal Rekam</th>
                            <th align="center" width="10%">Kode Pengesahan</th>
                            <th align="center" width="8%">Bank</th>
                          </tr>
                      </thead>
                      <tbody>
                        <?php $no=1; foreach($rk as $rk){?>
                        <tr>
                          <td align="center"><?php echo $no?></td>
                          <td><?php echo $rk->KD_PROPINSI.'.'.$rk->KD_DATI2.'.'.$rk->KD_KECAMATAN.'.'.$rk->KD_KELURAHAN.'.'.$rk->KD_BLOK.'.'.$rk->NO_URUT.'.'.$rk->KD_JNS_OP?></td>
                          <td><?php echo $rk->NM_WP_SPPT?></td>
                          <td><?php echo 'DS. '.$rk->NM_KELURAHAN.', KEC.'.$rk->NM_KECAMATAN;?></td>
                          <td align="right"><?php echo number_format($rk->POKOK,0,'','.');?></td>
                          <td align="right"><?php echo number_format($rk->DENDA,0,'','.');?></td>
                          <td align="right"><?php echo number_format($rk->TOTAL,0,'','.');?></td>
                          <td align="center"><?php echo $rk->TANGGAL_BAYAR?></td>
                          <td align="center"><?php echo $rk->TANGGAL_REKAM?></td>
                          <td align="center"><?php echo $rk->PENGESAHAN?></td>
                          <td align="center"><?php $nm_bank=""; if($rk->KODE_BANK_BAYAR==''){ $nm_bank="Bank Jatim"; }else{ $cb = $this->db->query("SELECT NAMA_BANK FROM REF_BANK WHERE KODE_BANK=$rk->KODE_BANK_BAYAR")->row(); $nm_bank=$cb->NAMA_BANK; }
                          echo $nm_bank?></td>
                        </tr>
                        <?php $no++;}?>
                      </tbody>
                  </table>

                  <?php }?>
            </div><!-- /.box-body -->
          </div><!-- /.box -->
        </section><!-- /.content -->

 <!-- Modal -->
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    
                </div>
            </div>
        </div>

        <script type="text/javascript">

          $(document).ready(function(){ 
              
              $("#KD_KECAMATAN").change(function(){ 
                
                
              
                $.ajax({
                  type: "POST",
                  url: "<?= base_url().'laporan/getkelurahan'?>", 
                  data: {kd_kec : $("#KD_KECAMATAN").val()}, 
                  success: function(response){ 
                    $("#KD_KELURAHAN").html(response);
                  }

                });
              });
            });
        </script>

<!--  <script>
        $(function(){
            $(document).on('click','.edit-record',function(e){
                e.preventDefault();
                $("#myModal").modal('show');
                $.post('<?php echo base_url()."laporan/detailmodalrekappermohonan" ?>',
                    {id:$(this).attr('data-id'),tgl1:'<?php echo $tgl1?>',tgl2:'<?php echo $tgl2?>'},
                    function(html){
                        $(".modal-content").html(html);
                    }   
                );
            });
        });
    </script> -->