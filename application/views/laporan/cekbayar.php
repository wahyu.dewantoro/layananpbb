        <section class="content-header">
          <h1>
          Cek Pembayaran SPPT
            <div class="pull-right">
             
           </div>
          </h1>
          
        </section>
        <!-- Main content -->
        <section class="content">
          <div class="row">
            <div class="col-md-12">
              <div class="box box-primary">
                <div class="box-body">
                   <form class="form-inline" method="post" action="<?php //echo base_url?>"> 
                      <div class="form-group">
                          <label class="col-md-2 control-label">NOP </label>
                            <div class="input-group">
                          <div class="col-md-1">
                               <input type="text"  maxLength="2" class="form-control" value="<?php //echo $KD_PROPINSI?>" name="KD_PROPINSI" id="KD_PROPINSI">
                          </div>
                          <div class="col-md-1">
                               <input type="text"  maxLength="2" class="form-control" value="<?php //echo $KD_DATI2 ?>" name="KD_DATI2" id="KD_DATI2">
                          </div>
                          <div class="col-md-1">
                               <input type="text"  maxLength="3" class="form-control" value="<?php //echo $KD_KECAMATAN ?>" name="KD_KECAMATAN" id="KD_KECAMATAN">
                          </div>
                          <div class="col-md-1">
                               <input type="text"  maxLength="3" class="form-control" value="<?php //echo $KD_KELURAHAN ?>" name="KD_KELURAHAN" id="KD_KELURAHAN">
                          </div>
                          <div class="col-md-1">
                               <input type="text"   maxLength="3" class="form-control" value="<?php //echo $KD_BLOK ?>" name="KD_BLOK" id="KD_BLOK">
                          </div>
                          <div class="col-md-1">
                               <input type="text"  maxLength="4" class="form-control" value="<?php //echo $NO_URUT ?>" name="NO_URUT" id="NO_URUT">
                          </div>
                          <div class="col-md-1">
                               <input type="text"  maxLength="1" class="form-control" value="<?php //echo $KD_JNS_OP ?>" name="KD_JNS_OP" id="KD_JNS_OP">
                          </div>
                          <div class="col-md-1">
                                  <button class="btn btn-success btn-sm" type="submit" id="btnSubmit"><i class="fa fa-search"></i> Cari</button>  
                          </div>
                          </div>
                      </div>
                    
                   </form>
                
                </div><!-- /.box-body -->
              </div><!-- /.box -->
            </div> 
          </div><!-- /.row -->
        </section><!-- /.content -->

      <script type="text/javascript">
          $(".form-control").keyup(function () {
              if (this.value.length == this.maxLength) {
                var nextIndex = $('input:text').index(this) + 1;
                $('input:text')[nextIndex].focus();
              }
          });


          $('input:text').bind("keydown", function(e) {
             var n = $("input:text").length;
             if (e.which == 13) 
             { //Enter key
               e.preventDefault(); //to skip default behavior of the enter key
               var nextIndex = $('input:text').index(this) + 1;
               if(nextIndex < n)
                 $('input:text')[nextIndex].focus();
               else
               {
                 $('input:text')[nextIndex-1].blur();
                 $('#btnSubmit').click();
               }
             }
          });
       </script>