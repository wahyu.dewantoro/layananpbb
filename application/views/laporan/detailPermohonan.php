        <section class="content-header">
          <h1>
            Laporan detail permohonan
            <div class="pull-right">

            </div>
          </h1>

        </section>
        <!-- Main content -->
        <section class="content">
          <!-- Default box -->
          <div class="box box-success">

            <div class="box-body">
              <form class="form-inline" method="post" action="<?php echo base_url() . 'laporan/permohonanDetail.html' ?>">
                <div class="form-group">
                  Tanggal permohonan
                </div>
                <div class="form-group">
                  <input type="text" class="form-control tanggal" name="tgl1" value="<?php echo $tgl1 ?>" reuired placeholder="Tanggal mulai">
                </div>
                <div class="form-group">
                  <input type="text" class="form-control tanggal" name="tgl2" value="<?php echo $tgl2 ?>" reuired placeholder="Tanggal akhir">
                </div>

                <button type="submit" class="btn btn-default"><i class="fa fa-search"></i> Cari</button>
                <?php echo anchor("laporan/detailpermohonanexcel?t1=" . urlencode($tgl1) . "&t2=" . urlencode($tgl2), 'Excel', 'class="btn btn-success"'); ?>
              </form>

              <?php if (isset($rk)) { ?>
                <hr>

                <table class="table table-bordered table-striped" id="example3">
                  <thead>
                    <tr>
                      <th>No</th>
                      <th>No Layanan</th>
                      <th>NOP</th>
                      <th>Pemohon</th>
                      <th>Jenis Pelayanan</th>
                      <th>Tanggal Terima</th>
                      <th>Tanggal Selesai</th>
                      <th>Unit</th>
                      <th>Keterangan</th>

                    </tr>
                  </thead>
                  <tbody>
                    <?php $no = 1;
                    foreach ($rk as $rk) { ?>
                      <tr>
                        <td align="center"><?php echo $no ?></td>
                        <td><?php echo $rk->NO_LAYANAN ?></td>
                        <td><?php echo $rk->NOP ?></td>
                        <td><?php echo $rk->NAMA_PEMOHON ?></td>
                        <td><?php echo $rk->NM_JENIS_PELAYANAN ?></td>
                        <td align="center"><?php echo $rk->TGL_TERIMA ?></td>
                        <td align="center"><?php echo $rk->TGL_SELESAI ?></td>
                        <td align="center"><?php echo $rk->UNIT_KANTOR ?></td>
                        <td><?php echo $rk->KETERANGAN_PST ?></td>
                      </tr>
                    <?php $no++;
                    } ?>
                  </tbody>
                </table>

              <?php } ?>
            </div><!-- /.box-body -->
          </div><!-- /.box -->
        </section><!-- /.content -->