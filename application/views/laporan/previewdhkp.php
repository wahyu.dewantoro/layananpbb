<section class="content-header">
  <h1>
    Preview Buku <?php echo $BUKU?> Tahun <?= $THN_PAJAK_SPPT ?>
    <div class="pull-right">
      <div class="btn-group">
          <?php echo anchor('laporan/cetakdhkpoutput/'.$THN_PAJAK_SPPT.'/'.$KD_PROPINSI.'/'.$KD_DATI2.'/'.$KD_KECAMATAN.'/'.$KD_KELURAHAN.'/'.str_replace(',', '_', $BUKU),'<i class="fa fa-print"></i> Cetak','class="btn btn-sm btn-primary" target="_blank"');?>
          <?php echo anchor('laporan/formcetakdhkp','<i class="fa fa-list"></i> Kembali','class="btn btn-sm btn-success"');?>
      </div>
  </div>
  </h1>
  
</section>
  <!-- Main content -->
  <section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="box box-success">
                <div class="box-body">
                    <div class="row">
                        <div class="col-xs-6">
                            <table>
                              <tr>
                                <td>PROPINSI</td>
                                <td>:</td>
                                <td>35 - Jawa Timur</td>
                              </tr>
                              <tr>
                                <td>DATI II</td>
                                <td>:</td>
                                <td>07 - Kabupaten Malang</td>
                              </tr>
                            </table>
                        </div>
                        <div class="col-xs-6">
                            <table>
                              <tr>
                                <td>KECAMATAN</td>
                                <td>:</td>
                                <td><?php $this->Mmaster->getkecamatan($KD_PROPINSI,$KD_DATI2,$KD_KECAMATAN) ?></td>
                              </tr>
                              <tr>
                                <td>KELURAHAN / DESA</td>
                                <td>:</td>
                                <td><?php $this->Mmaster->getkelurahan($KD_PROPINSI,$KD_DATI2,$KD_KECAMATAN,$KD_KELURAHAN) ?></td>
                              </tr>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12">
            <div class="box">
                <div class="box-body">
                  
                      
                  <table class="table table-bordered" id="example3">
                      <thead>
                          <tr>
                            <th>No</th>
                            <th>NOP</th>
                            <th>Nama Wajib Pajak</th>
                            <th>Alamat Objek Pajak</th>
                            <th>Alamat Wajib Pajak</th>
                            <th>L Bumi</th>
                            <th>L BNG</th>
                            <th>PBB</th>
                          </tr>
                      </thead>
                      <tbody>
                        <?php $no=1; foreach($rk as $rk){?>
                        <tr>
                          <td align="center"><?php echo $no;?></td>
                          <td><?= $rk->NOP ?></td>
                          <td><?= $rk->NAMA_WP ?></td>
                          <td><?= $rk->ALAMAT_OP ?></td>
                          <td><?= $rk->ALAMAT_WP ?></td>
                          <td align="center"><?= number_format($rk->LUAS_BUMI,'0','','.') ?></td>
                          <td align="center"><?= number_format($rk->LUAS_BANGUNAN,'0','','.') ?></td>
                          <td align="right"><?= number_format($rk->PBB,'0','','.') ?></td>
                        </tr>
                        <?php $no++; } ?>
                      </tbody>
                  </table>
                  </div>
            </div>
        </div>
    </div>
  
  </section><!-- /.content -->

 