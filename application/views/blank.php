<section class="content-header">
  <h1>
   Dasboard
  </h1>
</section>
<section class="content">

      <div class="row">
        <div class="col-md-3">
            <div class="row">
                <div class="col-md-12">
                     <!-- Profile Image -->
                        <div class="box box-primary">
                          <div class="box-body box-profile">
                            <img style="margin-right: auto;margin-left: auto;" class="profile-user-img img-responsive img-circle" src="<?= base_url().'noimage.png'?>" alt="User profile picture">
                            <h3 class="profile-username text-center"><?= $this->session->userdata('pbb_nama') ?></h3>
                            <p class="text-muted text-center"><?php echo $nama_role ?></p>

                            <ul class="list-group list-group-unbordered">
                              <li class="list-group-item">
                                <b>Tempat Pelayanan</b> <a class="pull-right"><?php $tt=$this->session->userdata('unit_kantor'); if($tt=='kota'){echo  "Pendopo";}else{echo "Kepanjen";}?></a>
                              </li>
                              <li class="list-group-item">
                                <b>Login pada </b> <a class="pull-right"><?= $this->session->userdata('jam_login')?></a>
                              </li>
                              
                            </ul>

                            <!-- <a href="#" class="btn btn-primary btn-block"><b>Follow</b></a> -->
                          </div>
                          <!-- /.box-body -->
                        </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
          <div class="row">
              
                  <!-- /.col -->
                <div class="col-md-12">
                     <!-- TABLE: LATEST ORDERS -->
          <div class="box box-info">
            <div class="box-header">
              <h3 class="box-title">Dokumen pada divisi
                  <div class="pull-right">
                    <select class="form-control" id="tahun" class="form-control">
                      <!-- <option ></option> -->
                      <?php for($da=date('Y');$da>=2018;$da--){?>
                        <option value="<?= $da ?>"><?= $da ?></option>
                      <?php } ?>
                    </select>
                  </div>
              </h3>               
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <div class="table-responsive">


                <table class="table no-margin">
                  
                  <tbody id="ambilrekap">
                    <?php foreach($rekap as $rekap){?>
                      <tr>
                        <td><a href="#"><?= $rekap->NAMA_GROUP?></a></td>
                        <td align="center"><div class="label  <?php 
                            if($rekap->DOK<=50){

                              echo "label-success";
                            }else if($rekap->DOK>50 && $rekap->DOK<=100){
                              echo "label-warning";
                              }else{
                                echo "label-danger";
                              }

                        ?> "><?= $rekap->DOK?> Berkas</div></td>
                      </tr>
                    <?php } ?>
                  </tbody>
                </table>
              </div>
              <!-- /.table-responsive -->
            </div>
            <!-- /.box-body -->
            
            <!-- /.box-footer -->
          </div>
          <!-- /.box -->
                </div>
                
          </div>
        </div>
           <div class="col-md-3">
            <div class="row">
                <div class="col-md-12">
                    <a href="<?= base_url().'permohonan/listdokumen.html'?>">
                    <div class="info-box">
                      <span class="info-box-icon bg-aqua"><i class="ion ion-ios-paper"></i></span>

                      <div class="info-box-content">
                        <span class="info-box-text">Todo list</span>
                        <br><br>
                        <span class="info-box-number" id="todo"><?= $jumDok ?> Berkas</span>
                      </div>
                      <!-- /.info-box-content -->
                    </div>
                    <!-- /.info-box -->
                  </a>
                  
                </div>
            </div>
              
           </div>

      </div>
</section>
<script type="text/javascript">
  $( document ).ready(function() {
    // The Calender
    $('#calendar').datepicker();  

    $('select').on('change', function() {
      // alert( this.value );
      datarekap(this.value);
      todo(this.value);
    });

    function datarekap(thn){
      $.ajax({
            url: "<?= base_url().'welcome/ambilrekap'?>",
            type: 'GET',
            data: {tahun : thn},
            success: function(data) {
              $('#ambilrekap').html(data);
            },
        });
    }

    function todo(thn){
      $.ajax({
            url: "<?= base_url().'welcome/ambiltodo'?>",
            type: 'GET',
            data: {tahun : thn},
            success: function(data) {
              $('#todo').html(data);
            },
        }); 
    }

  });
</script>