<section class="content-header">
  <h1>
   Data Potongan Denda
   	<div class="pull-right">
   		<a href="<?= base_url().'potongan'?>" class="btn btn-sm btn-info"><i class="fa fa-plus"></i> Tambah</a>
   	</div>
  </h1>
</section>
<section class="content">
	<div class="row">
		<div class="col-md-12">
			<div class="box box-default color-palette-box">
		        <!-- <div class="box-header with-border">
		          <h3 class="box-title"><i class="fa fa-tag"></i>Riwayat  Potongan</h3>
		        </div> -->
		        <div class="box-body">
		        	 <table class="table table-bordered" id="example1">
		        	 	<thead>
		        	 		<tr>
		        	 			<th>No</th>
		        	 			<th>NOP</th>
		        	 			<th>Tgl SK</th>
		        	 			<th>Potongan</th>
		        	 		</tr>
		        	 	</thead>
		        	 	<?php 
		        	 	$no=1;
		        	 	foreach($data as $rd){?>
		        	 		<tr>
		        	 			<td align="center"><?= $no ?></td>
		        	 			<td>
											<?= $rd->KD_PROPINSI.'.'.
											$rd->KD_DATI2.'.'.
											$rd->KD_KECAMATAN.'.'.
											$rd->KD_KELURAHAN.'.'.
											$rd->KD_BLOK.'.'.
											$rd->NO_URUT.'.'.
											$rd->KD_JNS_OP
											?>
		        	 			</td>
		        	 			<td><?= $rd->TGL_SK?></td>
		        	 			<td align="right"><?= number_format($rd->JUMLAH_POTONGAN,0,'','.') ?></td>
		        	 		</tr>
		        	 	<?php $no++; } ?>	
		        	 </table>
		        </div>
		    </div>

			
		</div>
	 
	</div>
</section>
 