<section class="content-header">
  <h1>
   Data Potongan Denda
  </h1>
</section>
<section class="content">
	<div class="row">
		<div class="col-md-6">
			<div class="box box-default color-palette-box">
		        <div class="box-header with-border">
		          <h3 class="box-title"><i class="fa fa-tag"></i> Form Potongan</h3>
		        </div>
		        <div class="box-body">
		        	<form method="post" action="<?= base_url().'potongan/submit' ?>"> 
						<div class="form-group">
							<label>NOP</label>
							<input type="text" name="NOP" id="NOP" class="form-control" required onkeyup="formatnop(this)"  autofocus="true">
						</div>
						<div class="form-group">
							<label>Tahun Pajak</label>
							<input type="text" onkeyup="hanyaangka(this)" value="<?= date('Y')?>" name="TAHUN_PAJAK" id="TAHUN_PAJAK" class="form-control" required>
						</div>
						
						<div class="form-group">
							<label>Tanggal SK</label>
							<input type="text" name="TGL_SK" id="TGL_SK" class="form-control tanggal" required>
						</div>
						<div class="form-group">
							<label>Jumlah potongan</label>
							<input type="text" name="JUMLAH_POTONGAN" onkeyup="hanyaangka(this)" id="JUMLAH_POTONGAN" class="form-control" required>
						</div>
						<button class="btn btn-sm btn-success"><i class="fa fa-save"></i> Save</button>
						<?php echo anchor('potongan/listdata','<i class="fa fa-close"></i> Batal','class="btn btn-sm btn-warning"')?>
					</form>
		        </div>
		    </div>

			
		</div>
		<div class="col-md-6">
			<div class="box box-default color-palette-box">
		        <div class="box-header with-border">
		          <h3 class="box-title"><i class="fa fa-tag"></i> Data NOP</h3>
		        </div>
		        <div class="box-body">
		        	<table class="table table-striped">
		        		<tr>
		        			<td>Alamat OP</td>
		        			<td width="1%">:</td>
		        			<td>
		        				<span id='ALAMAT_OP'></span>
								RT. <span id='RT_OP'></span>
								RW. <span id='RW_OP'></span>
		        			</td>
		        		</tr>
		        		<tr>
		        			<td>Nama WP</td>
		        			<td width="1%">:</td>
		        			<td>
		        				<span id='NM_WP_SPPT'></span>
		        			</td>
		        		</tr>
		        		<tr>
		        			<td>Alamat</td>
		        			<td width="1%">:</td>
		        			<td>
								<span id='ALAMAT_WP'></span>
								<span id='RW_WP_SPPT'></span>
								<span id='RT_WP_SPPT'></span>
								<span id='KELURAHAN_WP_SPPT'></span>
								<span id='KOTA_WP_SPPT'></span>
		        			</td>
		        		</tr>
		        		<tr>
		        			<td>Pokok</td>
		        			<td width="1%">:</td>
		        			<td>
		        				<div id='PBB_YG_HARUS_DIBAYAR_SPPT'></div>			
		        			</td>
		        		</tr>
		        		<tr>
		        			<td>Denda</td>
		        			<td width="1%">:</td>
		        			<td>
		        				<div id='DENDA'></div>			
		        			</td>
		        		</tr>
		        		<tr>
		        			<td>Status Pembayaran</td>
		        			<td width="1%">:</td>
		        			<td>
		        				<div id='STATUS_PEMBAYARAN_SPPT'></div>			
		        			</td>
		        		</tr>

		        	</table>	
		        	<div class="riwayat_potongan"></div>
		        </div>
		    </div>
				
			
				
				
							
		</div>
	</div>
</section>
<script type="text/javascript">
	function hanyaangka(thisa){
		    a = thisa.value;
    		b = a.replace(/[^\d]/g,"");
    		thisa.value=b;

	}

	function formatnop(objek){
    
    a = objek.value;
    b = a.replace(/[^\d]/g,"");
    c = "";
       panjang = b.length;

       if (panjang <=2){
        // 35 -> 0,2
        c=b;
       }else if (panjang >2 && panjang <=4) {
          // 07. -> 2,2
        c=b.substr(0, 2)+'.'+b.substr(2, 2);
       }else if(panjang>4 && panjang <=7){
        // 123 -> 4,3
        c=b.substr(0, 2)+'.'+b.substr(2, 2)+'.'+b.substr(4, 3);
       }else if(panjang>7 && panjang <= 10){
        // .123. ->
        c=b.substr(0, 2)+'.'+b.substr(2, 2)+'.'+b.substr(4, 3)+'.'+b.substr(7,3);
       }else if(panjang>10 && panjang<=13 ){
        // 123.
        c=b.substr(0, 2)+'.'+b.substr(2, 2)+'.'+b.substr(4, 3)+'.'+b.substr(7, 3)+'.'+b.substr(10, 3);
       }else if(panjang> 13 && panjang<=17){
        // 1234
        c=b.substr(0, 2)+'.'+b.substr(2, 2)+'.'+b.substr(4, 3)+'.'+b.substr(7, 3)+'.'+b.substr(10, 3)+'.'+b.substr(13, 4);
       }else{
        // .0
        c=b.substr(0, 2)+'.'+b.substr(2, 2)+'.'+b.substr(4, 3)+'.'+b.substr(7, 3)+'.'+b.substr(10, 3)+'.'+b.substr(13, 4)+'.'+b.substr(17, 1);
        // alert(panjang);
       }
       objek.value=c;
  }

	$(".form-control").keyup(function (event) {
    if (event.keyCode == 13) {
        textboxes = $("input.form-control");
        currentBoxNumber = textboxes.index(this);
        if (textboxes[currentBoxNumber + 1] != null) {
            nextBox = textboxes[currentBoxNumber + 1];
            nextBox.focus();
            nextBox.select();
        }
        event.preventDefault();
        return false;
    }
});

	$(document).ready(function(){
		



	  $("#NOP,#TAHUN_PAJAK").keyup(function(){
	    // $("span").text(i += 1);
	    // alert('oke');
			var vnop =$('#NOP').val();
			var nopv =vnop.replace(/[^\d]/g,"");
			var vth  =$('#TAHUN_PAJAK').val();
			var thv  =vth.replace(/[^\d]/g,"");

            // alert(nopv);
              $.ajax(
                  {
                     url: "<?= base_url().'potongan/getDataNop'?>", 
                     type: "GET",
                      data: 'nop='+nopv+thv,
                      dataType:"html",
                     success: function(result){
                     	var datashow = JSON.parse(result);
                     		$('#ALAMAT_OP').html(datashow[0].ALAMAT_OP);
							$('#RT_OP').html(datashow[0].RT_OP);
							$('#RW_OP').html(datashow[0].RW_OP);
							$('#NM_WP_SPPT').html(datashow[0].NM_WP_SPPT);
							$('#ALAMAT_WP').html(datashow[0].JLN_WP_SPPT+' '+datashow[0].BLOK_KAV_NO_WP_SPPT);
							$('#RW_WP_SPPT').html(datashow[0].RW_WP_SPPT);
							$('#RT_WP_SPPT').html(datashow[0].RT_WP_SPPT);
							$('#KELURAHAN_WP_SPPT').html(datashow[0].KELURAHAN_WP_SPPT);
							$('#KOTA_WP_SPPT').html(datashow[0].KOTA_WP_SPPT);
							$('#PBB_YG_HARUS_DIBAYAR_SPPT').html(datashow[0].PBB_YG_HARUS_DIBAYAR_SPPT);
							$('#STATUS_PEMBAYARAN_SPPT').html(datashow[0].STATUS_PEMBAYARAN_SPPT);
							$('#DENDA').html(datashow[0].DENDA);
                       },
                       error : function(){

                       }
                    },
                );	

	  });

	  $("#NOP").change(function(){
	    
			var vnop =$('#NOP').val();
			var nopv =vnop.replace(/[^\d]/g,"");
			

            // alert(nopv);
              $.ajax(
                  {
                     url: "<?= base_url().'potongan/getRiwayatPotongan'?>", 
                     type: "GET",
                      data: 'nop='+nopv,
                      dataType:"json",
                     success: function(result){
                     	$('#riwayat_potongan').html(result);
                       },
                       error : function(){

                       }
                    },
                );	

	  });

	});
</script>