<section class="content-header">
          <h1>
            <!-- List Dokumen -->
            <?= $title ?>
            <div class="pull-right">
                
            </div>
          </h1>
        </section>
        <section class="content">
          <div class="box">
            
            <div class="box-body">
              <form method="get" action="<?= $action ?>" class="form-inline">
                  <div class="form-group">
                    <select class="form-control" name="tahun">
                        <?php for($q=date('Y');$q>2009;$q--){?>
                                  <option <?php if($q==$tahun){echo "selected";}?> value="<?= $q ?>"><?= $q ?></option>
                                <?php } ?>
                      </select>
                      <!-- <input type="text" name="tahun" value="<?= $tahun ?>" class="form-control" required > -->
                  </div>
                  <div class="form-group">
                    <button class="btn btn-success"><i class="fa fa-search"></i> Cari</button>
                  </div>
              </form>

            <?= $this->session->flashdata('notif'); ?>
                <table class="table table-bordered table-striped" id="example2">
            <thead>
                <tr>
                      <th width ="5%">No</th>
                      <th>No Layanan</th>
                      <th>Pemohon</th>
                      <th>NOP</th>
                      <th>Layanan</th>
                      <th>Tahun</th>
                      <th>No SK</th>
                      <th>Aksi</th>
                </tr>
            </thead>
      
        </table>
            </div>
          </div>
        </section>


 <!-- Modal -->
<!-- <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <form method="post" action="<?php echo base_url()."permohonan/validasiCheckAll"?>">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Pelimpahan Dokumen</h4>
      </div>
      <div class="modal-body">
          
          <input type='hidden' name='KD_KANWIL' value='01'>
          <input type='hidden' name='KD_KANTOR' value='01'>
          
          <input type="hidden" name="berkas_id" id="berkas_id">
          <input type='hidden' name='NIP' value='<?php echo $this->session->userdata('nip')?>'>
          <input type='hidden' name='TANGGAL_AWAL' value='<?php echo date('Y-m-d H:i:s');?>'>
          <input type='hidden' name='TANGGAL_AKHIR' value='<?php echo date('Y-m-d H:i:s');?>'>
          <div class="form-group">
              <label>Unit Tujuan</label>
              <select class="form-control" required name="KODE_GROUP" >
                  <option value="">Pilih</option>
                  <?php foreach($unit as $unit){?>
                  <option value="<?php echo $unit->KODE_GROUP?>"><?php echo $unit->NAMA_GROUP?></option>
                  <?php }?>
              </select>
          </div>
          <div class="form-group">
            <label>Status</label>
            <select class="form-control" required name="STATUS_BERKAS">
                <?php $hdd=array('Proses','Disetujui','Ditolak','Selesai','Diambil'); 
                   foreach($hdd as $hdd){?>
                   <option value="<?php echo $hdd?>"><?php echo $hdd?></option>
                   <?php } ?>
            </select>
          </div>
          <div class="form-group">
            <label>Catatan</label>
            <textarea name="KETERANGAN" class="form-control"  ></textarea>
          </div>
      </div>
      <div class="modal-footer">
        <div class="btn-group">
            <button type="Submit" class="btn btn-success"><i class="fa fa-send"></i> Kirim</button>
            <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-close"></i> Cancel</button>
        </div>
      </div>
      </form>
    </div>
  </div>
</div> -->

    <script type="text/javascript">
            $(document).ready(function() {
                $.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings)
                {
                    return {
                        "iStart": oSettings._iDisplayStart,
                        "iEnd": oSettings.fnDisplayEnd(),
                        "iLength": oSettings._iDisplayLength,
                        "iTotal": oSettings.fnRecordsTotal(),
                        "iFilteredTotal": oSettings.fnRecordsDisplay(),
                        "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                        "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
                    };
                };

                var t = $("#example2").dataTable({
                    initComplete: function() {
                        var api = this.api();
                        $('#mytable_filter input')
                                .off('.DT')
                                .on('keyup.DT', function(e) {
                                    if (e.keyCode == 13) {
                                        api.search(this.value).draw();
                            }
                        });
                    },
                    
                    "searching": false,
                    "bLengthChange": false,
                    
                    'oLanguage':
                    {
                      "sProcessing":   "Sedang memproses...",
                      "sLengthMenu":   "Tampilkan _MENU_ entri",
                      "sZeroRecords":  "Tidak ditemukan data yang sesuai",
                      "sInfo":         "Menampilkan _START_ sampai _END_ dari _TOTAL_ entri",
                      "sInfoEmpty":    "Menampilkan 0 sampai 0 dari 0 entri",
                      "sInfoFiltered": "(disaring dari _MAX_ entri keseluruhan)",
                      "sInfoPostFix":  "",
                      "sSearch":       "Cari:",
                      "sUrl":          "",
                      "oPaginate": {
                        "sFirst":    "Pertama",
                        "sPrevious": "Sebelumnya",
                        "sNext":     "Selanjutnya",
                        "sLast":     "Terakhir"
                      }
                    },
                    processing: true,
                    serverSide: true,
                    ajax: {"url": "<?php echo base_url()?>surat/jsondatask", "type": "POST","data":{'var':'<?= $var ?>','kode':'<?= $kode ?>','tahun':'<?= $tahun ?>'}},
                    columns: [
                        {
                            "data": "ID",
                            "orderable": false,
                            "className" : "text-center",
                        },
                        {"data": "NO_LAYANAN"},
                        {"data": "NAMA_PEMOHON"},
                        {"data": "NOP"},
                        {"data": "LAYANAN"},
                        {"data": "TAHUN"},
                        {"data": "NOMOR_SK"                          
                        },
                        {
                            "data" : "ID",
                            "orderable": false,
                            "className" : "text-center",
                            render: function ( data, type, row ) {
                              // return '';
                              if(row.NOMOR_SK==null || row.NOMOR_SK==''){
                                urr='<?= base_url() ?>surat/generatenomor?id='+row.ID+'&kode='+row.KD_PENGAJUAN+'&var='+encodeURI(row.NM_PENGAJUAN);
                                aksi="<a onclick='return confirm(\"Apakah anda yakin?\")' class='btn btn-xs btn-success' href='"+urr+"'><i class='fa fa-refresh'></i> Proses</a>";
                              }else{

                                urr='<?= base_url() ?>surat/cetaksk?id='+row.ID+'&kode='+row.KD_PENGAJUAN+'&var='+encodeURI(row.NM_PENGAJUAN);
                                aksi="<a  class='btn btn-xs btn-warning' href='"+urr+"'><i class='fa fa-print'></i> Cetak</a>";
                                // aksi='cetak';
                              }

                              return aksi;
                            }
                        },
                    ],
                    order: [[0, 'desc']],
                    rowCallback: function(row, data, iDisplayIndex) {
                        var info = this.fnPagingInfo();
                        var page = info.iPage;
                        var length = info.iLength;
                        var index = page * length + (iDisplayIndex + 1);
                        $('td:eq(0)', row).html(index);
                    }
                });
            });
        </script>

        <script type="text/javascript"> 
    /*$(document).ready(function() { 
       $('#checkAll').click(function(){
          chk = $(this).is(':checked');
          if(chk){
              $('.chek_all').prop('checked', true);
          }else{
              $('.chek_all').prop('checked', false);
          }
      });

       $("#proses").click(function(){
        var data = $('.chek_all:checked').serialize();

        var berkas_id=$(".chek_all:checked").map(function(){
          return $(this).val();
        }).get();

        $('#berkas_id').val(berkas_id);
        
        if(berkas_id!=''){
          $('#myModal').modal('show');  
        }else{
          alert('belum ada item yang di pilih.');
        }
        
 
        
      });
    
    });   */
  </script>