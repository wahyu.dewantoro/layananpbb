<section class="content-header">
  <h1>
    Realisasi Nominatif
  </h1>
</section>
<section class="content">
  <div class="box">
    <form class="form-inline" role="form" method="post" action="<?= base_url() . 'realisasi/normatif' ?>">
      <div class="form-group">
        <select class="form-control" name="tahun">
          <?php for ($q = date('Y'); $q > 2009; $q--) { ?>
            <option <?php if ($q == $tahun) {
                      echo "selected";
                    } ?> value="<?= $q ?>"><?= $q ?></option>
          <?php } ?>
        </select>
      </div>
      <div class="form-group">
        <select id="kd_kecamatan" name="kd_kecamatan" class="form-control" required="">
          <option value="">Kecamatan</option>
          <?php foreach ($ref_kec as $rk) { ?>
            <option <?php if ($rk->KD_KECAMATAN == $kd_kecamatan) {
                      echo "selected";
                    } ?> value="<?= $rk->KD_KECAMATAN ?>"><?= $rk->KD_KECAMATAN . ' ' . $rk->NM_KECAMATAN ?></option>
          <?php } ?>
        </select>
      </div>
      <div class="form-group">
        <select class="form-control" required="" id="kd_kelurahan" name="kd_kelurahan">
          <option value="">Pilih Kelurahan</option>
          <?php if (isset($ref_kel)) {
            foreach ($ref_kel as $kel) { ?>
              <option <?php if ($kel->KD_KELURAHAN == $kd_kelurahan) {
                        echo "selected";
                      } ?> value="<?= $kel->KD_KELURAHAN ?>"><?= $kel->KD_KELURAHAN . ' ' . $kel->NM_KELURAHAN ?></option>
          <?php }
          } ?>
        </select>
      </div>
      <div class="form-group">
        <select class="form-control" name="buku">
          <option value="">Buku</option>
          <?php foreach ($buku as $buku) { ?>
            <option <?php if ($jns_buku == $buku) {
                      echo "selected";
                    } ?> value="<?= $buku ?>"><?= $buku ?></option>
          <?php } ?>
        </select>
      </div>
      <button type="submit" class="btn btn-success"><i class="fa fa-search"></i> Cari</button>
      <?php echo anchor('realisasi/cetakNominatif?tahun=' . urlencode($tahun) . '&kd_kecamatan=' . urlencode($kd_kecamatan) . '&kd_kelurahan=' . urlencode($kd_kelurahan) . '&buku=' . urlencode($jns_buku), '<i class="fa fa-file"></i> Excel', 'class="btn btn-primary" target="_blank"'); ?>
    </form>


    <table class="table table-bordered table-hover" id="example2">
      <thead>
        <tr>
          <th width="5%">No</th>
          <th>NOP</th>
          <th>Alamat OP</th>
          <th>RT/RW OP</th>
          <th>Luas Tanah</th>
          <th>Luas Bangunan</th>
          <th>Nama WP</th>
          <th>Alamat WP</th>
          <th>RT/RW WP</th>
          <th>Kelurahan WP</th>
          <th>Kota WP</th>
          <th>PBB</th>
          <th>BUKU</th>
        </tr>
      </thead>
    </table>

  </div>
</section>
<script type="text/javascript">
  $(document).ready(function() {
    $.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings) {
      return {
        "iStart": oSettings._iDisplayStart,
        "iEnd": oSettings.fnDisplayEnd(),
        "iLength": oSettings._iDisplayLength,
        "iTotal": oSettings.fnRecordsTotal(),
        "iFilteredTotal": oSettings.fnRecordsDisplay(),
        "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
        "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
      };
    };

    var t = $("#example2").dataTable({
      initComplete: function() {
        var api = this.api();
        $('#mytable_filter input')
          .off('.DT')
          .on('keyup.DT', function(e) {
            if (e.keyCode == 13) {
              api.search(this.value).draw();
            }
          });
      },


      "pageLength": 10,
      "searching": false,
      "bLengthChange": false,
      'oLanguage': {
        "sProcessing": "Sedang memproses...",
        "sLengthMenu": "Tampilkan _MENU_ entri",
        "sZeroRecords": "Tidak ditemukan data yang sesuai",
        "sInfo": "Menampilkan _START_ sampai _END_ dari _TOTAL_ entri",
        "sInfoEmpty": "Menampilkan 0 sampai 0 dari 0 entri",
        "sInfoFiltered": "(disaring dari _MAX_ entri keseluruhan)",
        "sInfoPostFix": "",
        "sSearch": "Cari:",
        "sUrl": "",
        "oPaginate": {
          "sFirst": "Pertama",
          "sPrevious": "Sebelumnya",
          "sNext": "Selanjutnya",
          "sLast": "Terakhir"
        }
      },
      processing: true,
      serverSide: true,
      ajax: {
        "url": "<?= base_url() ?>realisasi/jsonnominatif",
        "type": "POST",
        "data": {
          'tahun': '<?= $tahun ?>',
          'kd_kecamatan': '<?= $kd_kecamatan ?>',
          'kd_kelurahan': '<?= $kd_kelurahan ?>',
          'buku': '<?= $jns_buku ?>'
        }
      },
      columns: [{
          "data": "NOP",
          "orderable": false,
          "className": "text-center"
        },
        {
          "data": "NOP"
        },
        {
          "data": "ALAMAT_OP"
        },
        {
          "data": "RTRW_OP"
        },
        {
          "data": "LUAS_BUMI_SPPT",
          "className": "text-center",
          render: function(data, type, row) {
            return ribuan(data) + ' M<sup>2</sup>';
          }
        },
        {
          "data": "LUAS_BNG_SPPT",
          "className": "text-center",
          render: function(data, type, row) {
            return ribuan(data) + ' M<sup>2</sup>';
          }
        },
        {
          "data": "NM_WP_SPPT"
        },
        {
          "data": "ALAMAT_WP"
        },
        {
          "data": "RTRW_WP"
        },
        {
          "data": "KELURAHAN_WP_SPPT"
        },
        {
          "data": "KOTA_WP_SPPT"
        },
        {
          "data": "PBB",
          "className": "text-center",
          render: function(data, type, row) {
            return ribuan(data);
          }

        },
        {
          "data": "BUKU",
          "className": "text-center"
        },

      ],
      order: [
        [1, 'Asc']
      ],
      rowCallback: function(row, data, iDisplayIndex) {
        var info = this.fnPagingInfo();
        var page = info.iPage;
        var length = info.iLength;
        var index = page * length + (iDisplayIndex + 1);
        $('td:eq(0)', row).html(index);
      }
    });
  });


  $('#kd_kecamatan').change(function() {
    // alert($(this).val());
    var kd_kec = $(this).val();
    $.ajax({
      type: "POST",
      url: "<?= base_url() ?>realisasi/getkelurahan",
      data: {
        'kd_kecamatan': kd_kec
      },
      success: function(response) {
        $("#kd_kelurahan").html(response);
      }

    });
  });
</script>