<section class="content-header">
          <h1>
            Target APBD Buku III, IV dan V
            <div class="pull-right">
                <?php echo anchor('realisasi/targetapbdtigaup_create','<i class="fa fa-plus"></i> Tambah','class="btn btn-sm btn-success"')?>
            </div>
          </h1>
        </section>
        <section class="content">
          <div class="box">
                <table class="table table-bordered table-hover" id="example3">
                    <thead>
                       <tr>
                           <th width="20px">No</th>
                           <th>Tahun</th>
                           <th>Target</th>
                           <th>Target PAK</th>
                           <th width="50px"></th>
                       </tr>
                    </thead>
                    <tbody>
                        <?php $no=1; foreach($rk as $rk){?>
                            <tr>
                                <td align="center"><?= $no ?></td>
                                <td align="center"><?= $rk->THN_PAJAK?></td>
                                <td align="right"><?= number_format($rk->TARGET,'0','','.') ?></td>
                                <td align="right"><?= number_format($rk->TARGET_PAK,'0','','.') ?></td>
                                <td align="center"><?= anchor('realisasi/editapbdtigaup/'.$rk->THN_PAJAK,'<i class="fa fa-edit"></i>','class="btn btn-xs btn-success"')?> <?= anchor('realisasi/hapusapbdtigaup/'.$rk->THN_PAJAK,'<i class="fa fa-trash"></i>','class="btn btn-xs btn-danger"')?></td>
                            </tr>
                        <?php $no++; }?>
                    </tbody>
                </table>
            
          </div>
        </section>
    