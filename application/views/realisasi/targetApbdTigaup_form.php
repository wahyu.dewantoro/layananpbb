<section class="content-header">
          <h1>
            <?php echo $button ?>
            <div class="pull-right">
            <?= anchor('realisasi/apbdtigaup','Target APBD','class="btn btn-sm btn-success"');?>
          </div>
          </h1>
          
        </section>
        <section class="content">
          <div class="box">
            <?= $this->session->flashdata('notif_desa')?>
            <div class="box-body">
                  <form class='form-horizontal' action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
                    <div class="form-group">
                      <label class="col-md-2 control-label">Tahun Pajak <sup>*</sup></label>
                      <div class="col-md-6">
                        <input type="text" name="THN_PAJAK" value="<?= $THN_PAJAK ?>" class="form-control" required="" <?php if($edit==true){ echo "readonly";}?>>
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-md-2 control-label">Target <sup>*</sup></label>
                      <div class="col-md-6">
                        <input type="text" name="TARGET" value="<?= $TARGET ?>" class="form-control" required="" onkeyup="formatangka(this)">
                      </div>
                    </div>
                    <div class="form-group">
                      <label class="col-md-2 control-label">Target PAK <sup>*</sup></label>
                      <div class="col-md-6">
                        <input type="text" name="TARGET_PAK" value="<?= $TARGET_PAK ?>" class="form-control" required="" onkeyup="formatangka(this)">
                      </div>
                    </div>
              	    
                    <div class="form-group">   
                        <div class="col-md-8 col-md-offset-2">
                            <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Simpan</button> 
    	                       <a href="<?php echo site_url('realisasi/apbdtigaup') ?>" class="btn btn-warning"><i class="fa fa-close"></i> Kembali</a>
                        </div>
                    </div>        
              	</form>
            </div>
          </div>
        </section>
        <script type="text/javascript">
            function formatangka(objek) {
     a = objek.value;
     b = a.replace(/[^\d]/g,"");
     c = "";
     panjang = b.length;
     j = 0;
     for (i = panjang; i > 0; i--) {
       j = j + 1;
       if (((j % 3) == 1) && (j != 1)) {
         c = b.substr(i-1,1) + "." + c;
       } else {
         c = b.substr(i-1,1) + c;
       }
     }
     objek.value = c;
  };

        </script>
 