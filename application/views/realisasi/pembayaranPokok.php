<section class="content-header">
  <h1>
    Capaian Realisasi Pembayaran PBB-P2
    <div class="pull-right">
      <?= anchor('realisasi/cetakpembayaranPokok', '<i class="fa fa-file"></i> Excel', 'class="btn btn-sm btn-success"'); ?>
    </div>
  </h1>
</section>
<section class="content">
  <div class="box">
    <div class="table-responsive">
      <table width="100%" class="table table-bordered table-hover" id="">
        <thead>
          <tr>
            <th rowspan="3">Uraian</th>
            <th rowspan="3">Baku</th>

            <th colspan="9">Realisasi</th>
            <th rowspan="3">(%)</th>
            <th rowspan="3">Sisa</th>
          </tr>
          <tr>
            <th colspan="3">Minggu Lalu</th>
            <th colspan="3">Minggu Ini</th>
            <th colspan="3">S/D Minggu Ini</th>
          </tr>
          <tr>
            <th>Pokok</th>
            <th>Denda</th>
            <th>Jumlah</th>
            <th>Pokok</th>
            <th>Denda</th>
            <th>Jumlah</th>
            <th>Pokok</th>
            <th>Denda</th>
            <th>Jumlah</th>

          </tr>
        </thead>
        <tbody>
          <?php
          $b = 0;
          $c = 0;
          $d = 0;
          $e = 0;
          $f = 0;
          $g = 0;
          $h = 0;
          $i = 0;
          $j = 0;
          $k = 0;
          $l = 0;
          $m = 0;
          foreach ($data as $rk) { ?>
            <tr>
              <td><?= $rk->BUKU ?></td>
              <td align="right"><?php $b += $rk->BAKU;
                                echo number_format($rk->BAKU, 2, ',', '.'); ?></td>
              <td align="right"><?php $c += $rk->POKOK_ML;
                                echo number_format($rk->POKOK_ML, 2, ',', '.'); ?></td>
              <td align="right"><?php $d += $rk->DENDA_ML;
                                echo number_format($rk->DENDA_ML, 2, ',', '.'); ?></td>
              <td align="right"><?php $e += $rk->JUMLAH_ML;
                                echo number_format($rk->JUMLAH_ML, 2, ',', '.'); ?></td>
              <td align="right"><?php $f += $rk->POKOK_MI;
                                echo number_format($rk->POKOK_MI, 2, ',', '.'); ?></td>
              <td align="right"><?php $g += $rk->DENDA_MI;
                                echo number_format($rk->DENDA_MI, 2, ',', '.'); ?></td>
              <td align="right"><?php $h += $rk->JUMLAH_MI;
                                echo number_format($rk->JUMLAH_MI, 2, ',', '.'); ?></td>
              <td align="right"><?php $i += $rk->POKOK_SMI;
                                echo number_format($rk->POKOK_SMI, 2, ',', '.'); ?></td>
              <td align="right"><?php $j += $rk->DENDA_SMI;
                                echo number_format($rk->DENDA_SMI, 2, ',', '.'); ?></td>
              <td align="right"><?php $k += $rk->JUMLAH_SMI;
                                echo number_format($rk->JUMLAH_SMI, 2, ',', '.'); ?></td>
              <td align="right"><?php $l += $rk->PERSEN;
                                echo number_format($rk->PERSEN, 2, ',', '.'); ?></td>
              <td align="right"><?php $m += $rk->SISA;
                                echo number_format($rk->SISA, 2, ',', '.'); ?></td>
            </tr>
          <?php  } ?>
        </tbody>
        <tfoot>
          <tr>
            <th>Jumlah</th>
            <th align="right"><?= number_format($b, 2, ',', '.'); ?></th>
            <th align="right"><?= number_format($c, 2, ',', '.'); ?></th>
            <th align="right"><?= number_format($d, 2, ',', '.'); ?></th>
            <th align="right"><?= number_format($e, 2, ',', '.'); ?></th>
            <th align="right"><?= number_format($f, 2, ',', '.'); ?></th>
            <th align="right"><?= number_format($g, 2, ',', '.'); ?></th>
            <th align="right"><?= number_format($h, 2, ',', '.'); ?></th>
            <th align="right"><?= number_format($i, 2, ',', '.'); ?></th>
            <th align="right"><?= number_format($j, 2, ',', '.'); ?></th>
            <th align="right"><?= number_format($k, 2, ',', '.'); ?></th>
            <th align="right"><?= number_format($l, 2, ',', '.'); ?></th>
            <th align="right"><?= number_format($m, 2, ',', '.'); ?></th>
          </tr>
        </tfoot>
      </table>
    </div>
  </div>
</section>

</script>