<section class="content-header">
  <h1>
    Realisasi Target APBD
    <div class="pull-right">
      <?= anchor('realisasi/cetaksatuUp?tahun=' . $tahun, '<i class="fa fa-file"></i> Excel', 'class="btn btn-sm btn-success"') ?>
    </div>
  </h1>
</section>
<section class="content">
  <div class="box">
    <?= $this->session->flashdata('notif_desa') ?>
    <form class="form-inline" role="form" method="post" action="<?= base_url() . 'realisasi/realisasiAPBD' ?>">
      <div class="form-group">
        <select class="form-control" name="TAHUN">
          <?php for ($q = date('Y'); $q > 2009; $q--) { ?>
            <option <?php if ($q == $tahun) {
                      echo "selected";
                    } ?> value="<?= $q ?>"><?= $q ?></option>
          <?php } ?>
        </select>
      </div>

      <button type="submit" class="btn btn-success"><i class="fa fa-search"></i> Cari</button>
    </form>
    <br><br>
    <table class="table table-bordered table-hover">
      <thead>
        <tr>
          <th width="5%">No</th>
          <!-- <th>Tahun</th> -->
          <th>Buku</th>
          <th>Target</th>
          <th>Pokok</th>
          <th>Denda</th>
          <th>Jumlah</th>
          <th>%</th>
          <th>Kurang / Lebih</th>
        </tr>
      </thead>
      <tbody>
        <?php

        $a = 0;
        $b = 0;
        $c = 0;
        $d = 0;
        $e = 0;
        $f = 0;

        $no = 1;
        foreach ($rk as $rk) { ?>
          <tr>
            <td align="center"><?= $no ?></td>
            <td><?= $rk->BUKU ?></td>
            <td align="right"><?php $a += $rk->TARGET;
                              echo number_format($rk->TARGET, 0, '', '.'); ?></td>
            <td align="right"><?php $b += $rk->POKOK;
                              echo number_format($rk->POKOK, 0, '', '.'); ?></td>
            <td align="right"><?php $c += $rk->DENDA;
                              echo number_format($rk->DENDA, 0, '', '.'); ?></td>
            <td align="right"><?php $d += $rk->JUMLAH;
                              echo number_format($rk->JUMLAH, 0, '', '.'); ?></td>
            <td align="center"><?php $e += $rk->PERSEN;
                                echo number_format($rk->PERSEN, 0, ',', '.'); ?></td>
            <td align="right"><?php $f += $rk->KURANG_ATAU_LEBIH;
                              echo number_format($rk->KURANG_ATAU_LEBIH, 0, '', '.'); ?></td>
          </tr>
        <?php $no++;
        } ?>
      </tbody>
      <tfoot>
        <tr>
          <td colspan="2">Jumlah</td>
          <td align="right"><b><?= number_format($a, 0, ',', '.'); ?></b></td>
          <td align="right"><b><?= number_format($b, 0, ',', '.'); ?></b></td>
          <td align="right"><b><?= number_format($c, 0, ',', '.'); ?></b></td>
          <td align="right"><b><?= number_format($d, 0, ',', '.'); ?></b></td>
          <td align="center"><b><?= number_format($e, 0, ',', '.'); ?></b></td>
          <td align="right"><b><?= number_format($f, 0, ',', '.'); ?></b></td>
        </tr>
      </tfoot>
    </table>

  </div>
</section>