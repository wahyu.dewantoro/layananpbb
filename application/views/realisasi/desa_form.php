<section class="content-header">
          <h1>
            <?php echo $button ?>
            <div class="pull-right">
            <?= anchor('realisasi/desaLunas','Data Desa','class="btn btn-sm btn-success"');?>
          </div>
          </h1>
          
        </section>
        <section class="content">
          <div class="box">
            <?= $this->session->flashdata('notif_desa')?>
            <div class="box-body">
                  <form class='form-horizontal' action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
                    <div class="form-group">
                        <label class="col-md-2 control-label">Kecamatan <sup>*</sup></label>
                        <div class="col-md-4">
                          <!-- <input type="text" name="KD_KECAMATAN" required="" class="form-control" id="KD_KECAMATAN" value="<?= $KD_KECAMATAN ?>"> -->
                          <select id="KD_KECAMATAN" name="KD_KECAMATAN"  class="form-control" required="">
                              <option value="">Kecamatan</option>
                              <?php foreach($ref_kec as $rk){?>
                                <option <?php if($rk->KD_KECAMATAN==$KD_KECAMATAN){echo "selected";} ?> value="<?= $rk->KD_KECAMATAN?>"><?= $rk->NM_KECAMATAN ?></option>
                              <?php } ?>
                          </select>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Kelurahan <sup>*</sup></label>
                        <div class="col-md-4">
                          <!-- <input type="text" name="KD_KELURAHAN" required="" class="form-control" id="KD_KELURAHAN" value="<?= $KD_KELURAHAN ?>"> -->
                          <select class="form-control" required="" id="KD_KELURAHAN" name="KD_KELURAHAN">
                          <option value="">Pilih Kelurahan</option>
                          <?php if(isset($ref_kel)){ foreach($ref_kel as $kel){?>
                            <option <?php if($kel->KD_KELURAHAN==$KD_KELURAHAN){echo "selected";}?> value="<?= $kel->KD_KELURAHAN ?>"><?= $kel->NM_KELURAHAN ?></option>
                          <?php } } ?>
                      </select>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="col-md-2 control-label">Tanggal Lunas <sup>*</sup></label>
                        <div class="col-md-4">
                          <input type="text" name="TGL_LUNAS"   required="" class="form-control tanggal" id="TGL_LUNAS" value="<?= $TGL_LUNAS ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Baku <sup>*</sup></label>
                        <div class="col-md-4">
                          <input type="text" name="BAKU" required="" onkeyup="formatangka(this)" class="form-control" id="BAKU" value="<?= $BAKU ?>">
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Tahun Pajak <sup>*</sup></label>
                        <div class="col-md-4">
                          <input type="text" name="TAHUN_PAJAK" required="" class="form-control" id="TAHUN_PAJAK" value="<?= $TAHUN_PAJAK ?>">
                        </div>
                    </div>
<!--               	    <div class="form-group">
                          <label class="col-md-2 control-label">Nama Bank </label>
                          <div class="col-md-8">
                              <input type="text" class="form-control" name="NAMA_BANK" id="NAMA_BANK" placeholder="Nama Group" value="<?php echo $NAMA_BANK; ?>" />
                              <?php echo form_error('NAMA_BANK') ?>
                          </div>
                      </div>
                      <div class="form-group">
                          <label class="col-md-2 control-label">Kode Bank </label>
                          <div class="col-md-8">
                              <input type="text" class="form-control" name="KODE_BANK" id="KODE_BANK" placeholder="Nama Group" value="<?php echo $KODE_BANK; ?>" />
                              <?php echo form_error('KODE_BANK') ?>
                          </div>
                      </div> -->
              	    <input type="hidden" name="ID" value="<?php echo $ID; ?>" /> 
                    <div class="form-group">   
                        <div class="col-md-8 col-md-offset-2">
                            <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Simpan</button> 
    	                       <a href="<?php echo site_url('realisasi/desaLunas') ?>" class="btn btn-warning"><i class="fa fa-close"></i> Kembali</a>
                        </div>
                    </div>        
              	</form>
            </div>
          </div>
        </section>
<script type="text/javascript">

  function formatangka(objek) {
     a = objek.value;
     b = a.replace(/[^\d]/g,"");
     c = "";
     panjang = b.length;
     j = 0;
     for (i = panjang; i > 0; i--) {
       j = j + 1;
       if (((j % 3) == 1) && (j != 1)) {
         c = b.substr(i-1,1) + "." + c;
       } else {
         c = b.substr(i-1,1) + c;
       }
     }
     objek.value = c;
  };



  $(document).ready(function() {
      $('#KD_KECAMATAN').change(function () {
                var kd_kec = $(this).val();
                $.ajax({
                    type: "POST",
                    url: "<?= base_url() ?>realisasi/getkelurahan",
                    data: {'kd_kecamatan': kd_kec},
                     success: function(response){
                        $("#KD_KELURAHAN").html(response); 
                      }

                });
            });
  });
     
</script>