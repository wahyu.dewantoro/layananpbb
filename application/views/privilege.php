<section class="content-header">
  <h1>
    Privilege
  </h1>
</section>
<!-- Main content -->
<section class="content">
  <!-- Default box -->
  <div class="box">
    <div class="box-body">
      <table class="table table-bordered table-hover" id="example3">
        <thead>
          <tr>
            <th width="5%">No</th>
            <th>Group</th>
            <th width="10%">Aksi</th>
          </tr>
        </thead>
        <tbody>
          <?php $no = 1;
          foreach ($data as $row) { ?>
            <tr>
              <td align="center"><?php echo $no; ?></td>
              <td><?php echo ucwords($row['NAMA_GROUP']); ?></td>
              <td align="center"> <?php echo anchor('sistem/settingPrivilege/' . $row['KODE_GROUP'], '<i class="btn btn-primary btn-xs fa fa-cog"></i>'); ?></td>
            </tr>
          <?php $no++;
          } ?>
        </tbody>
      </table>
    </div><!-- /.box-body -->
  </div><!-- /.box -->
</section><!-- /.content -->