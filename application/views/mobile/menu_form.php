<section class="content-header">
  <h1>
    <?php echo $button ?>
    <div class="pull-right">

    </div>
  </h1>

</section>
<section class="content">
  <div class="row">
    <div class="col-md-6">


      <div class="box">

        <div class="box-body">
          <form class='form-horizontal' action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
            <div class="form-group">
              <label class="col-md-2 control-label">Keterangan </label>
              <div class="col-md-8">
                <textarea type="text" class="form-control" name="keterangan" id="keterangan" placeholder="Keterangan" /><?php echo $keterangan; ?> </textarea>
                <?php echo form_error('keterangan') ?>
              </div>
            </div>
            <div class="form-group">
              <label class="col-md-2 control-label">Active </label>
              <div class="col-md-8">

                <input type="radio" name="active" class="flat-red" value="1" <?php if ($status == '1') {
                                                                                echo "checked";
                                                                              } ?> /> Aktif<br>
                <input type="radio" name="active" class="flat-red" value="0" <?php if ($status == '0') {
                                                                                echo "checked";
                                                                              } ?> /> Non Aktif



                <?php echo form_error('active') ?>
              </div>
            </div>
            <input type="hidden" name="kode_config" value="<?php echo $kode_config; ?>" />

            <div class="form-group">
              <div class="col-md-8 col-md-offset-2">
                <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Simpan</button>
                <a href="<?php echo site_url('menu') ?>" class="btn btn-warning"><i class="fa fa-close"></i> Cancel</a>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</section>