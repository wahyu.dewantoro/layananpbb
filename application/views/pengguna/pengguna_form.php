<section class="content-header">
    <h1>
        <?php echo $button ?>
        <div class="pull-right">

        </div>
    </h1>

</section>
<section class="content">
    <div class="box">
        <div class="box-header with-border">
            <div class="box-tools pull-right">
                <button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>
            </div>
        </div>
        <div class="box-body">
            <form class='form-horizontal' action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
                <div class="form-group">
                    <label class="col-md-2 control-label">Username </label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" name="USERNAME" id="USERNAME" placeholder="USERNAME" value="<?php echo $USERNAME; ?>" />
                        <?php echo form_error('USERNAME') ?>
                    </div>
                </div>
                <?php if ($button != 'Update Pengguna') { ?>
                    <div class="form-group">
                        <label class="col-md-2 control-label">Password </label>
                        <div class="col-md-8">
                            <input type="password" class="form-control" name="PASSWORD" id="PASSWORD" placeholder="PASSWORD" value="<?php echo $PASSWORD; ?>" />
                            <?php echo form_error('PASSWORD') ?>
                        </div>
                    </div>
                <?php } ?>
                <div class="form-group">
                    <label class="col-md-2 control-label">Nama </label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" name="NAMA_PENGGUNA" id="NAMA_PENGGUNA" placeholder="NAMA PENGGUNA" value="<?php echo $NAMA_PENGGUNA; ?>" />
                        <?php echo form_error('NAMA_PENGGUNA') ?>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-2 control-label">NIP </label>
                    <div class="col-md-8">
                        <input type="text" class="form-control" name="NIP" id="NIP" placeholder="NIP" value="<?php echo $NIP; ?>" />
                        <?php echo form_error('NIP') ?>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-2 control-label">Seksi </label>
                    <div class="col-md-8">
                        <select class="form-control chosen" name="KD_SEKSI" id="KD_SEKSI">
                            <option value=""></option>
                            <?php foreach ($seksi as $seksi) { ?>
                                <option <?php if ($seksi->KD_SEKSI == $KD_SEKSI) {
                                            echo "selected";
                                        } ?> value="<?php echo $seksi->KD_SEKSI ?>"><?php echo $seksi->NM_SEKSI ?></option>
                            <?php } ?>
                        </select>
                        <?php echo form_error('KD_SEKSI') ?>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-2 control-label"> Group </label>
                    <div class="col-md-8">
                        <!-- <input type="text"  placeholder="KODE GROUP" value="<?php echo $KODE_GROUP; ?>" /> -->
                        <select class="form-control chosen" name="KODE_GROUP" id="KODE_GROUP">
                            <option value=""></option>
                            <?php foreach ($group as $group) { ?>
                                <option <?php if ($KODE_GROUP == $group->KODE_GROUP) {
                                            echo "selected";
                                        } ?> value="<?php echo $group->KODE_GROUP ?>"><?php echo $group->NAMA_GROUP ?></option>
                            <?php } ?>
                        </select>
                        <?php echo form_error('KODE_GROUP') ?>
                    </div>
                </div>

                <input type="hidden" name="KODE_PENGGUNA" value="<?php echo $KODE_PENGGUNA; ?>" />

                <div class="form-group">
                    <div class="col-md-8 col-md-offset-2">
                        <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Simpan</button>
                        <a href="<?php echo site_url('pengguna') ?>" class="btn btn-warning"><i class="fa fa-close"></i> Cancel</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
</section>