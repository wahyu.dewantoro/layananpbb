<section class="content-header">
  <h1>
    Nomor SK
    <div class="pull-right">

    </div>
  </h1>
</section>
<section class="content">
  <div class="box">

    <div class="box-body">
      <?php echo $this->session->flashdata('notif'); ?>
      <table class="table table-bordered table-striped" id="example1">
        <thead>
          <tr>
            <th width="5%">No</th>
            <th>Jenis SK</th>
            <th>Nomor</th>
            <th>Pejabat</th>
            <th>Jabatan</th>
            <th>NIP</th>
            <th width="5%%"></th>
          </tr>
        </thead>
        <tbody>
          <?php $no = 1;
          foreach ($data as $rk) { ?>
            <tr>
              <td align="center"><?php echo $no ?></td>
              <td> <?= $rk->JENIS ?> </td>
              <td> <?= $rk->NOMOR ?> </td>
              <td> <?= $rk->NAMA_PEGAWAI ?> </td>
              <td> <?= $rk->JABATAN ?> </td>
              <td> <?= $rk->NIP ?> </td>
              <td><?= anchor('master/updatenomorsk?p=' . urlencode(urlencode(urlencode($rk->JENIS))), '<i class="fa fa-edit"></i> Edit', 'class="btn btn-sm btn-success"'); ?></td>
            </tr>
          <?php $no++;
          } ?>
        </tbody>
      </table>
    </div>
  </div>
</section>