<section class="content-header">
  <h1>
    Blokir NOP
    <div class="pull-right">

    </div>
  </h1>
</section>
<section class="content">
  <div class="box">

    <div class="box-body">
      <?php echo $this->session->flashdata('notif'); ?>


      <form class="form-inline" role="form" method="post" action="<?= base_url() . 'master/blokirnopsppt' ?>">
        <div class="form-group">
          NOP
        </div>
        <div class="form-group">
          <input type="text" onkeyup="formatnop(this)" autofocus="true" name="NOP" class="form-control" id="NOP" placeholder="" autofocus="true" required>
        </div>
        <div class="form-group">
          <input type="text" name="TAHUN" class="form-control" placeholder="Tahun Pajak" required>
        </div>

        <button type="submit" class="btn btn-danger">Blokir</button>
        <button type="button" class="btn btn-primary" data-toggle="modal" data-target=".bs-example-modal-sm"><i class="fa fa-upload"></i> Uplaod</button>
        <a href="<?= base_url() . 'tmp_blok.xls' ?>">Template</a>
      </form>

      <form method="post" action="<?= base_url() . 'master/bukablokirmasal' ?>">
        <div class="mailbox-messages">

          <table class="table table-bordered table-striped" id="example1">
            <thead>
              <tr>
                <th width="10%">
                  <div class="btn-group">
                    <button type="button" class="btn btn-default btn-xs checkbox-toggle"><i class="fa fa-square-o"></i></button> <button type="submit" class="btn btn-xs btn-success" id="buka">Buka</button>
                  </div>
                </th>
                <th>NOP</th>
                <th>Tahun</th>
                <th>Nama WP</th>
                <th>Tanggal Blokir</th>
                <th width="10%"></th>
              </tr>
            </thead>
            <tbody>
              <?php $no = 1;
              foreach ($data as $rk) { ?>
                <tr>
                  <td align="center">
                    <input type="checkbox" class="chek_all" name="nop[]" value="<?= $rk->KD_PROPINSI . '.' . $rk->KD_DATI2 . '.' . $rk->KD_KECAMATAN . '.' . $rk->KD_KELURAHAN . '.' . $rk->KD_BLOK . '.' . $rk->NO_URUT . '.' . $rk->KD_JNS_OP . '.' . $rk->THN_PAJAK_SPPT ?>">
                  </td>
                  <td> <?=

                          $rk->KD_PROPINSI . '.' .
                            $rk->KD_DATI2 . '.' .
                            $rk->KD_KECAMATAN . '.' .
                            $rk->KD_KELURAHAN . '.' .
                            $rk->KD_BLOK . '.' .
                            $rk->NO_URUT . '.' .
                            $rk->KD_JNS_OP
                        ?> </td>
                  <td> <?= $rk->THN_PAJAK_SPPT ?> </td>
                  <td> <?= $rk->NM_WP_SPPT ?> </td>
                  <td> <?= $rk->TGL_MASUK ?> </td>


                  <td><?= anchor('master/bukablokir?THN_PAJAK_SPPT=' . $rk->THN_PAJAK_SPPT . '&KD_PROPINSI=' . urlencode($rk->KD_PROPINSI) . '& KD_DATI2=' . urlencode($rk->KD_DATI2) . '& KD_KECAMATAN=' . urlencode($rk->KD_KECAMATAN) . '& KD_KELURAHAN=' . urlencode($rk->KD_KELURAHAN) . '& KD_BLOK=' . urlencode($rk->KD_BLOK) . '& NO_URUT=' . urlencode($rk->NO_URUT) . '& KD_JNS_OP=' . urlencode($rk->KD_JNS_OP), '<i class="fa fa-key"></i> Buka', 'class="btn btn-sm btn-success"'); ?>
                    <?= anchor('master/delblokir?THN_PAJAK_SPPT=' . $rk->THN_PAJAK_SPPT . '&KD_PROPINSI=' . urlencode($rk->KD_PROPINSI) . '& KD_DATI2=' . urlencode($rk->KD_DATI2) . '& KD_KECAMATAN=' . urlencode($rk->KD_KECAMATAN) . '& KD_KELURAHAN=' . urlencode($rk->KD_KELURAHAN) . '& KD_BLOK=' . urlencode($rk->KD_BLOK) . '& NO_URUT=' . urlencode($rk->NO_URUT) . '& KD_JNS_OP=' . urlencode($rk->KD_JNS_OP), '<i class="fa fa-trash"></i>', 'class="btn btn-sm btn-danger" onclick="return confirm(\'Apakah anda yakin, data yang sudah di hapus tidak bisa di kembalikan?\')"'); ?>
                  </td>
                </tr>
              <?php $no++;
              } ?>
            </tbody>
          </table>
        </div>
      </form>
    </div>
  </div>
</section>



<!-- Small modal -->


<div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Uppload NOP Blokir</h4>
      </div>
      <div class="modal-body">
        <form method="post" action="<?= base_url() . 'master/prosesimportNopblokir' ?>" enctype="multipart/form-data">
          <div class="form-group">
            <label>File</label>
            <input type="file" name="nop_blokir" class="form-control" required="">
          </div>
          <button class="btn btn-sm btn-success"><i class="fa fa-upload"></i> Upload</button>
        </form>
      </div>

    </div>
  </div>
</div>
<script type="text/javascript">
  function formatnop(objek) {

    a = objek.value;
    b = a.replace(/[^\d]/g, "");
    c = "";
    panjang = b.length;

    if (panjang <= 2) {
      // 35 -> 0,2
      c = b;
    } else if (panjang > 2 && panjang <= 4) {
      // 07. -> 2,2
      c = b.substr(0, 2) + '.' + b.substr(2, 2);
    } else if (panjang > 4 && panjang <= 7) {
      // 123 -> 4,3
      c = b.substr(0, 2) + '.' + b.substr(2, 2) + '.' + b.substr(4, 3);
    } else if (panjang > 7 && panjang <= 10) {
      // .123. ->
      c = b.substr(0, 2) + '.' + b.substr(2, 2) + '.' + b.substr(4, 3) + '.' + b.substr(7, 3);
    } else if (panjang > 10 && panjang <= 13) {
      // 123.
      c = b.substr(0, 2) + '.' + b.substr(2, 2) + '.' + b.substr(4, 3) + '.' + b.substr(7, 3) + '.' + b.substr(10, 3);
    } else if (panjang > 13 && panjang <= 17) {
      // 1234
      c = b.substr(0, 2) + '.' + b.substr(2, 2) + '.' + b.substr(4, 3) + '.' + b.substr(7, 3) + '.' + b.substr(10, 3) + '.' + b.substr(13, 4);
    } else {
      // .0
      c = b.substr(0, 2) + '.' + b.substr(2, 2) + '.' + b.substr(4, 3) + '.' + b.substr(7, 3) + '.' + b.substr(10, 3) + '.' + b.substr(13, 4) + '.' + b.substr(17, 1);
      // alert(panjang);
    }
    objek.value = c;
  }
</script>
<script type="text/javascript">
  $(document).ready(function() {

    //Enable iCheck plugin for checkboxes
    //iCheck for checkbox and radio inputs
    $('.mailbox-messages input[type="checkbox"]').iCheck({
      checkboxClass: 'icheckbox_flat-blue',
      radioClass: 'iradio_flat-blue'
    });

    //Enable check and uncheck all functionality
    $(".checkbox-toggle").click(function() {
      var clicks = $(this).data('clicks');
      if (clicks) {
        // alert('a');

        //Uncheck all checkboxes
        $(".mailbox-messages input[type='checkbox']").iCheck("uncheck");
        $(".fa", this).removeClass("fa-check-square-o").addClass('fa-square-o');
      } else {
        // alert('b');
        //Check all checkboxes
        $(".mailbox-messages input[type='checkbox']").iCheck("check");
        $(".fa", this).removeClass("fa-square-o").addClass('fa-check-square-o');
      }
      $(this).data("clicks", !clicks);
    });

    //Enable iCheck plugin for checkboxes
    //iCheck for checkbox and radio inputs
    $('.mailbox-messages input[type="checkbox"]').iCheck({
      checkboxClass: 'icheckbox_flat-blue',
      radioClass: 'iradio_flat-blue'
    });
    /*
           $("#proses").click(function(){
            var data = $('.chek_all:checked').serialize();

            var berkas_id=$(".chek_all:checked").map(function(){
              return $(this).val();
            }).get();

            $('#berkas_id').val(berkas_id);
            
            if(berkas_id!=''){
              $('#myModal').modal('show');  
            }else{
              alert('belum ada item yang di pilih.');
            }
            
     
            
          });
        */


  });
</script>