<section class="content-header">
  <h1>
    Form Nomor SK
    <div class="pull-right">
      <?= anchor('master/nomorsk', '<i class="fa fa-list"></i> Data', 'class="btn btn-sm btn-success"') ?>
    </div>
  </h1>
</section>
<section class="content">
  <div class="box">
    <div class="box-body">
      <form class="form-horizontal" method="post" action="<?= base_url() . 'master/prosesupdatesk' ?>">
        <div class="row">
          <div class="col-md-4">
            <div class="form-group">
              <label class="control-label col-md-4">Jenis SK <sup>*</sup></label>
              <div class="col-md-8">
                <input type="text" name="JENIS" id="JENIS" readonly class="form-control form-control-sm" value="<?= $JENIS ?>" required>
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-4">Format Nomor <sup>*</sup></label>
              <div class="col-md-8">
                <input type="text" name="NOMOR" id="NOMOR" class="form-control form-control-sm" value="<?= $NOMOR ?>" required>
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-4">Nama Pejabat <sup>*</sup></label>
              <div class="col-md-8">
                <input type="text" name="NAMA_PEGAWAI" id="NAMA_PEGAWAI" class="form-control form-control-sm" value="<?= $NAMA_PEGAWAI ?>" required>
              </div>
            </div>
          </div>
          <div class="col-md-4">
            <div class="form-group">
              <label class="control-label col-md-4">Jabatan <sup>*</sup></label>
              <div class="col-md-8">
                <input type="text" name="JABATAN" id="JABATAN" class="form-control form-control-sm" value="<?= $JABATAN ?>" required>
              </div>
            </div>
            <div class="form-group">
              <label class="control-label col-md-4">NIP <sup>*</sup></label>
              <div class="col-md-8">
                <input type="text" name="NIP" id="NIP" class="form-control form-control-sm" value="<?= $NIP ?>" required>
              </div>
            </div>
            <p class="text-right">
              <button class="btn btn-sm btn-primary"><i class="fa fa-save"></i> Submit</button>
            </p>
          </div>
        </div>



      </form>


    </div>
  </div>
</section>