<section class="content-header">
  <h1>
    Jenis Pelayanan
    <div class="pull-right">

    </div>
  </h1>
</section>
<section class="content">
  <div class="box">
    <div class="box-header with-border">


    </div>
    <div class="box-body">
      <?php echo $this->session->flashdata('notif'); ?>
      <table class="table table-bordered table-striped" id="example1">
        <thead>
          <tr>
            <th width="5%">No</th>
            <th>Layanan</th>

          </tr>
        </thead>
        <tbody>
          <?php $no = 1;
          foreach ($data as $rk) { ?>
            <tr>
              <td align="center"><?php echo $no ?></td>
              <td><?php echo $rk->NM_JENIS_PELAYANAN ?></td>

            </tr>
          <?php $no++;
          } ?>
        </tbody>
      </table>
    </div>
  </div>
</section>