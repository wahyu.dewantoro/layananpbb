<section class="content-header">
    <h1>
        Kode Bank
        <div class="pull-right">
            <?php echo anchor(site_url('bank/create'), '<i class="fa fa-plus"></i> Tambah', 'class="btn btn-success"'); ?>
        </div>
    </h1>
</section>
<section class="content">
    <div class="box">
        <div class="box-header with-border">

        </div>
        <div class="box-body">
            <table class="table table-bordered table-hover" id="example2">
                <thead>
                    <tr>
                        <th width="5%">No</th>
                        <th> Bank</th>
                        <th> Kode Bank</th>
                        <th width="10%">Aksi</th>
                    </tr>
                </thead>

            </table>
        </div>
    </div>
</section>
<script type="text/javascript">
    $(document).ready(function() {
        $.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings) {
            return {
                "iStart": oSettings._iDisplayStart,
                "iEnd": oSettings.fnDisplayEnd(),
                "iLength": oSettings._iDisplayLength,
                "iTotal": oSettings.fnRecordsTotal(),
                "iFilteredTotal": oSettings.fnRecordsDisplay(),
                "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
            };
        };

        var t = $("#example2").dataTable({
            initComplete: function() {
                var api = this.api();
                $('#mytable_filter input')
                    .off('.DT')
                    .on('keyup.DT', function(e) {
                        if (e.keyCode == 13) {
                            api.search(this.value).draw();
                        }
                    });
            },



            'oLanguage': {
                "sProcessing": "Sedang memproses...",
                "sLengthMenu": "Tampilkan _MENU_ entri",
                "sZeroRecords": "Tidak ditemukan data yang sesuai",
                "sInfo": "Menampilkan _START_ sampai _END_ dari _TOTAL_ entri",
                "sInfoEmpty": "Menampilkan 0 sampai 0 dari 0 entri",
                "sInfoFiltered": "(disaring dari _MAX_ entri keseluruhan)",
                "sInfoPostFix": "",
                "sSearch": "Cari:",
                "sUrl": "",
                "oPaginate": {
                    "sFirst": "Pertama",
                    "sPrevious": "Sebelumnya",
                    "sNext": "Selanjutnya",
                    "sLast": "Terakhir"
                }
            },
            processing: true,
            serverSide: true,
            ajax: {
                "url": "bank/json",
                "type": "POST"
            },
            columns: [{
                    "data": "ID",
                    "orderable": false,
                    "className": "text-center"
                },
                {
                    "data": "NAMA_BANK"
                },
                {
                    "data": "KODE_BANK"
                },
                {
                    "data": "action",
                    "orderable": false,
                    "className": "text-center"
                }
            ],
            order: [
                [1, 'Asc']
            ],
            rowCallback: function(row, data, iDisplayIndex) {
                var info = this.fnPagingInfo();
                var page = info.iPage;
                var length = info.iLength;
                var index = page * length + (iDisplayIndex + 1);
                $('td:eq(0)', row).html(index);
            }
        });
    });
</script>