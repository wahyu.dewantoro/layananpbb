<section class="content-header">
  <h1>
    Rekap Pembayaran BNI
    <div class="pull-right">

    </div>
  </h1>
</section>
<section class="content">
  <div class="">
    <div class="col-md-3">
      <div class="box box-solid">
        <div class="box-header with-border">
          <form class="form-inline" method="post" action="<?php echo base_url() . 'rekon/laprekonpembayaranbni.html' ?>">
            <div class="form-group">Bulan</div>
            <div class="form-group">
              <select class="form-control" name="bulan">
                <option <?php if ($bln == '01') {
                          echo "selected";
                        } ?> value="01">Januari</option>
                <option <?php if ($bln == '02') {
                          echo "selected";
                        } ?> value="02">Februari</option>
                <option <?php if ($bln == '03') {
                          echo "selected";
                        } ?> value="03">Maret</option>
                <option <?php if ($bln == '04') {
                          echo "selected";
                        } ?> value="04">April</option>
                <option <?php if ($bln == '05') {
                          echo "selected";
                        } ?> value="05">Mei</option>
                <option <?php if ($bln == '06') {
                          echo "selected";
                        } ?> value="06">Juni</option>
                <option <?php if ($bln == '07') {
                          echo "selected";
                        } ?> value="07">Juli</option>
                <option <?php if ($bln == '08') {
                          echo "selected";
                        } ?> value="08">Agustus</option>
                <option <?php if ($bln == '09') {
                          echo "selected";
                        } ?> value="09">September</option>
                <option <?php if ($bln == '10') {
                          echo "selected";
                        } ?> value="10">Oktober</option>
                <option <?php if ($bln == '11') {
                          echo "selected";
                        } ?> value="11">November</option>
                <option <?php if ($bln == '12') {
                          echo "selected";
                        } ?> value="12">Desember</option>
              </select>
            </div>
            <div class="form-group">Tahun</div>
            <div class="form-group">
              <select class="form-control" name="tahun">
                <?php for ($q = date('Y'); $q > 2009; $q--) { ?>
                  <option <?php if ($q == $thn) {
                            echo "selected";
                          } ?> value="<?= $q ?>"><?= $q ?></option>
                <?php } ?>
              </select>
            </div>
            <input type="hidden" name="cari" value="1">
            <button type="submit" class="btn btn-default"><i class="fa fa-search"></i></button>
          </form>

        </div>
        <div class="box-body">
          <table class="table table-bordered table-striped" id="example2">
            <thead>
              <tr>
                <th width="10%">Tanggal</th>
                <th width="15%">Jumlah NOP</th>
                <!-- <th width="15%">Nama</th>
                          <th width="10%">Tahun Pajak</th>
                          <th>Pokok</th>
                          <th>Denda</th>
                          <th>Jumlah</th>
                          <th width="15%">Tanggal Pembayaran</th> -->

              </tr>
            </thead>
            <tbody>
              <?php $no = 1;
              foreach ($rk as $rk) { ?>
                <tr>
                  <td align="center"><?php echo $rk->TGL_PEMBAYARAN ?></td>
                  <td align="center"> <a href="<?php echo base_url('rekon/laprekonpembayaranbni?t=') . $rk->TGL_PEMBAYARAN ?>"> <?= $rk->JML_NOP ?> </a> </td>
                  <!-- <td> <?= $rk->NAMA ?> </td>
                           <td align="center"> <?= $rk->THN_PAJAK ?> </td>
                           <td align="right"> <?= number_format($rk->POKOK, 0, '', '.') ?> </td>
                           <td align="right"> <?= number_format($rk->DENDA, 0, '', '.') ?> </td>
                           <td align="right"> <?= number_format($rk->JUMLAH, 0, '', '.') ?> </td>
                           <td align="center"> <?= $rk->TGL_PEMBAYARAN ?> </td> -->
                </tr>
              <?php $no++;
              } ?>
            </tbody>
          </table>
        </div>
        <!-- /.box-body -->
      </div>
    </div>
    <?php if (isset($drk)) { ?>
      <div class="col-md-9">
        <div class="box">
          <div class="box-header with-border">
            <h3 class="box-title">Detail Rekap Rekon Pembayaran BNI</h3>


            <div class="box-tools">
              <div class="mailbox-controls">
              </div>
            </div>

            <!-- /.box-tools -->
          </div>
          <div class="box-body">

            <div class="mailbox-messages">

              <table class="table table-bordered table-striped" id="example1">
                <thead>
                  <tr>
                    <th width="5%">No</th>
                    <th width="15%">NOP</th>
                    <th width="15%">Nama</th>
                    <th width="8%">Tahun Pajak</th>
                    <th>Pokok</th>
                    <th>Denda</th>
                    <th>Jumlah</th>
                    <th width="15%">Tanggal Pembayaran</th>

                  </tr>
                </thead>
                <tbody>
                  <?php $no = 1;
                  foreach ($drk as $drk) { ?>
                    <tr>
                      <td align="center"><?php echo $no ?></td>
                      <td> <?= $drk->NOP ?> </td>
                      <td> <?= $drk->NAMA ?> </td>
                      <td align="center"> <?= $drk->THN_PAJAK ?> </td>
                      <td align="right"> <?= number_format($drk->POKOK, 0, '', '.') ?> </td>
                      <td align="right"> <?= number_format($drk->DENDA, 0, '', '.') ?> </td>
                      <td align="right"> <?= number_format($drk->JUMLAH, 0, '', '.') ?> </td>
                      <td align="center"> <?= $drk->TGL_PEMBAYARAN ?> </td>
                    </tr>
                  <?php $no++;
                  } ?>
                </tbody>
              </table>

            </div>
          </div>
        </div>
      </div>
    <?php } ?>



</section>
<!-- <section class="content" >
          <div class="col-md-4">
              <div class="box">
                <div class="box-header with-border">
                  <h3 class="box-title">Bulan</h3>
                </div>
                <div class="box-body">
                  <table class="table table-bordered table-striped" id="example1">
                    <thead>
                        <tr>
                            <th width="10%">Tanggal</th>
                          <th width="15%">Jumlah NOP</th>
                          <th width="15%">Nama</th>
                          <th width="10%">Tahun Pajak</th>
                          <th>Pokok</th>
                          <th>Denda</th>
                          <th>Jumlah</th>
                          <th width="15%">Tanggal Pembayaran</th>

                        </tr>
                    </thead>
                 <tbody>
                       <?php $no = 1;
                        foreach ($rk as $rk) { ?>
                       <tr>
                           <td align="center"><?php echo $rk->TGL_PEMBAYARAN ?></td>
                           <td> <?= $rk->JML_NOP ?> </td>
                           <td> <?= $rk->NAMA ?> </td>
                           <td align="center"> <?= $rk->THN_PAJAK ?> </td>
                           <td align="right"> <?= number_format($rk->POKOK, 0, '', '.') ?> </td>
                           <td align="right"> <?= number_format($rk->DENDA, 0, '', '.') ?> </td>
                           <td align="right"> <?= number_format($rk->JUMLAH, 0, '', '.') ?> </td>
                           <td align="center"> <?= $rk->TGL_PEMBAYARAN ?> </td>
                       </tr>
                       <?php $no++;
                        } ?>
                   </tbody>
                </table> 
                </div>
              </div>
          </div>
          <div class="col-md-8">
              <div class="box box-solid">
                <div class="box-header with-border">
                  <h3 class="box-title">Bulan</h3>
                </div>
                <div class="box-body">
                  
                </div>
              </div>
          </div>
          
          <div class="box">
            
            <div class="box-body">
                
                
                <form class="form-inline" role="form" method="post" action="<?= base_url() . 'rekon/prosesimport' ?>" enctype="multipart/form-data">
                    <div class="form-group">
                      File
                    </div>
                    <div class="form-group">
                      <input type="file" name="data_excel" class="form-control" required="">
                    </div>
                                    
                    <button type="submit" class="btn btn-success"><i class="fa fa-upload"></i> Upload</button>
                    <div class="form-group">
                      <?php echo $this->session->flashdata('notif'); ?>
                    </div>
                  </form>
            </div>
            <div class="box-body"></div>
          </div>

          <div class="box">
            
            <div class="box-body">
                
                
                <form class="form-inline" role="form" method="post" action="<?= base_url() . 'rekon/laprekonpembayaran' ?>">
                    <div class="form-group">
                      Tanggal Pembayaran
                    </div>
                    <div class="form-group">
                      <input type="text" class="form-control tanggal" name="tgl1" value="<?php echo $tgl1 ?>" reuired placeholder="Tanggal mulai">
                    </div>
                    <input type="hidden" name="cari" value="1">
                                    
                    <button type="submit" class="btn btn-default"><i class="fa fa-search"></i> Cari</button>
                  </form>

               
<?php if (isset($rk)) { ?>
                <table class="table table-bordered table-striped" id="example1">
                    <thead>
                        <tr>
                            <th width="10%">Tanggal</th>
                          <th width="15%">Jumlah NOP</th>
                          <th width="15%">Nama</th>
                          <th width="10%">Tahun Pajak</th>
                          <th>Pokok</th>
                          <th>Denda</th>
                          <th>Jumlah</th>
                          <th width="15%">Tanggal Pembayaran</th>

                        </tr>
                    </thead>
                 <tbody>
                       <?php $no = 1;
                        foreach ($rk as $rk) { ?>
                       <tr>
                           <td align="center"><?php echo $rk->TGL_PEMBAYARAN ?></td>
                           <td> <?= $rk->JML_NOP ?> </td>
                           <td> <?= $rk->NAMA ?> </td>
                           <td align="center"> <?= $rk->THN_PAJAK ?> </td>
                           <td align="right"> <?= number_format($rk->POKOK, 0, '', '.') ?> </td>
                           <td align="right"> <?= number_format($rk->DENDA, 0, '', '.') ?> </td>
                           <td align="right"> <?= number_format($rk->JUMLAH, 0, '', '.') ?> </td>
                           <td align="center"> <?= $rk->TGL_PEMBAYARAN ?> </td>
                       </tr>
                       <?php $no++;
                        } ?>
                   </tbody>
                </table> 
              <?php } ?>
            </div>
            <div class="box-body"></div>
          </div>
        </section> -->



<!-- Small modal -->


<div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" id="modal-cek">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Konfirmasi !</h4>
      </div>
      <div class="modal-body">
        <form method="post">
          <div class="form-group">
            <label>File</label>
            <span><?php echo "Data yang akan diupload ada " . $this->session->flashdata('jml_data') . " record"; ?></span>
          </div>

          <button type="button" class="btn btn-sm btn-success" data-dismiss="modal">OK</button>
        </form>
      </div>

    </div>
  </div>
</div>
<?php
$modal = $this->session->flashdata('modal');
if ($modal == '1') {
?>
  <script type="text/javascript">
    $("#modal-cek").modal("show");
  </script>
<?php
}
?>

<script type="text/javascript">
  function formatnop(objek) {

    a = objek.value;
    b = a.replace(/[^\d]/g, "");
    c = "";
    panjang = b.length;

    if (panjang <= 2) {
      // 35 -> 0,2
      c = b;
    } else if (panjang > 2 && panjang <= 4) {
      // 07. -> 2,2
      c = b.substr(0, 2) + '.' + b.substr(2, 2);
    } else if (panjang > 4 && panjang <= 7) {
      // 123 -> 4,3
      c = b.substr(0, 2) + '.' + b.substr(2, 2) + '.' + b.substr(4, 3);
    } else if (panjang > 7 && panjang <= 10) {
      // .123. ->
      c = b.substr(0, 2) + '.' + b.substr(2, 2) + '.' + b.substr(4, 3) + '.' + b.substr(7, 3);
    } else if (panjang > 10 && panjang <= 13) {
      // 123.
      c = b.substr(0, 2) + '.' + b.substr(2, 2) + '.' + b.substr(4, 3) + '.' + b.substr(7, 3) + '.' + b.substr(10, 3);
    } else if (panjang > 13 && panjang <= 17) {
      // 1234
      c = b.substr(0, 2) + '.' + b.substr(2, 2) + '.' + b.substr(4, 3) + '.' + b.substr(7, 3) + '.' + b.substr(10, 3) + '.' + b.substr(13, 4);
    } else {
      // .0
      c = b.substr(0, 2) + '.' + b.substr(2, 2) + '.' + b.substr(4, 3) + '.' + b.substr(7, 3) + '.' + b.substr(10, 3) + '.' + b.substr(13, 4) + '.' + b.substr(17, 1);
      // alert(panjang);
    }
    objek.value = c;
  }
</script>