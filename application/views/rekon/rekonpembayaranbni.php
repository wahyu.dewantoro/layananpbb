<section class="content-header">
  <h1>
    Rekon Pembayaran BNI
    <div class="pull-right">

    </div>
  </h1>
</section>
<section class="content">
  <div class="box">

    <div class="box-body">


      <form class="form-inline" role="form" method="post" action="<?= base_url() . 'rekon/prosesimport' ?>" enctype="multipart/form-data">
        <div class="form-group">
          File
        </div>
        <div class="form-group">
          <input type="file" name="data_excel" class="form-control" required="">
        </div>

        <button type="submit" class="btn btn-success"><i class="fa fa-upload"></i> Upload</button>
        <div class="form-group">
          <?php echo $this->session->flashdata('notif'); ?>
        </div>
      </form>
    </div>
    <div class="box-body"></div>
  </div>

  <div class="box">

    <div class="box-body">


      <form class="form-inline" role="form" method="post" action="<?= base_url() . 'rekon/rekonpembayaranbni' ?>">
        <div class="form-group">
          Tanggal Pembayaran
        </div>
        <div class="form-group">
          <input type="text" class="form-control tanggal" name="tgl1" value="<?php echo $tgl1 ?>" reuired placeholder="Tanggal mulai">
        </div>

        <input type="hidden" name="cari" value="1">

        <button type="submit" class="btn btn-default"><i class="fa fa-search"></i> Cari</button>
      </form>
      <!-- <table class="table table-bordered table-striped" id="example1">
                    <thead>
                        <tr>
                          <th width="5%" rowspan="2">No</th>
                          <th colspan="4">Bapenda</th>
                          <th colspan="4">BNI</th>

                        </tr>
                        <tr>
                          <th width="15%">NOP</th>
                          <th width="10%">Tahun Pajak</th>
                          <th>Jumlah Bayar</th>
                          <th>Tanggal Bayar</th>
                          <th width="15%">NOP</th>
                          <th width="10%">Tahun Pajak</th>
                          <th>Jumlah Bayar</th>
                          <th>Tanggal Bayar</th>

                        </tr>
                    </thead>
                 <tbody>
                       
                   </tbody>
                </table>  -->
      <br>


      <?php if (isset($rk)) { ?>

        <center><span class="badge badge-primary">
            <h4>Perbedaan data sebanyak <b><?php echo $hitung; ?></b> record</h4>
          </span></center>

        <table class="table table-bordered table-striped" id="example1">
          <thead>
            <tr>
              <th width="5%" rowspan="2">No</th>
              <th colspan="4">Bapenda</th>
              <th colspan="4">BNI</th>

            </tr>
            <tr>
              <th width="11%">NOP</th>
              <th width="10%">Tahun Pajak</th>
              <th>Jumlah Bayar</th>
              <th>Tanggal Bayar</th>
              <th width="11%">NOP</th>
              <th width="10%">Tahun Pajak</th>
              <th>Jumlah Bayar</th>
              <th>Tanggal Bayar</th>

            </tr>
          </thead>
          <tbody>
            <?php $no = 1;
            foreach ($rk as $rk) { ?>
              <tr <?php if (($rk->JUMLAH_BPD != $rk->JUMLAH_BNI) && $rk->JUMLAH_BPD != '' && $rk->JUMLAH_BNI != '') {
                    echo "style='background-color: #ffe4a5'";
                  } else if ($rk->NOP_BPD == '' || $rk->THN_PAJAK_BPD == '' || $rk->TGL_BPD == '' || $rk->JUMLAH_BPD == '' || $rk->NOP_BNI == '' || $rk->THN_PAJAK_BNI == '' || $rk->TANGGAL_BNI == '' || $rk->JUMLAH_BNI == '') {
                    echo "style='background-color: #ffcfbf'";
                  } ?>>
                <td align="center"><?php echo $no ?></td>
                <td> <?= $rk->NOP_BPD ?> </td>
                <td align="center"> <?= $rk->THN_PAJAK_BPD ?> </td>
                <td align="right"><?php if ($rk->JUMLAH_BPD == '') {
                                  } else {
                                    echo number_format($rk->JUMLAH_BPD, 0, '', '.');
                                  } ?> </td>
                <td align="center"> <?= $rk->TGL_BPD ?> </td>
                <td> <?= $rk->NOP_BNI ?> </td>
                <td align="center"> <?= $rk->THN_PAJAK_BNI ?> </td>
                <td align="right"> <?php if ($rk->JUMLAH_BNI == '') {
                                    } else {
                                      echo number_format($rk->JUMLAH_BNI, 0, '', '.');
                                    } ?> </td>
                <td align="center"> <?php echo date('d-m-Y', strtotime($rk->TANGGAL_BNI)) ?> </td>
              </tr>
            <?php $no++;
            } ?>
          </tbody>
        </table>
      <?php } ?>

      <!--#f2b7a2  #f9d581-->

    </div>
    <div class="box-body"></div>
  </div>
</section>



<!-- Small modal -->


<div class="modal fade bs-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" id="modal-cek">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">Konfirmasi !</h4>
      </div>
      <div class="modal-body">
        <form method="post">
          <div class="form-group">
            <label>File</label>
            <span><?php echo "Data yang akan diupload ada " . $this->session->flashdata('jml_data') . " record"; ?></span>
          </div>

          <button type="button" class="btn btn-sm btn-success" data-dismiss="modal">OK</button>
        </form>
      </div>

    </div>
  </div>
</div>
<?php
$modal = $this->session->flashdata('modal');
if ($modal == '1') {
?>
  <script type="text/javascript">
    $("#modal-cek").modal("show");
  </script>
<?php
}
?>

<script type="text/javascript">
  function formatnop(objek) {

    a = objek.value;
    b = a.replace(/[^\d]/g, "");
    c = "";
    panjang = b.length;

    if (panjang <= 2) {
      // 35 -> 0,2
      c = b;
    } else if (panjang > 2 && panjang <= 4) {
      // 07. -> 2,2
      c = b.substr(0, 2) + '.' + b.substr(2, 2);
    } else if (panjang > 4 && panjang <= 7) {
      // 123 -> 4,3
      c = b.substr(0, 2) + '.' + b.substr(2, 2) + '.' + b.substr(4, 3);
    } else if (panjang > 7 && panjang <= 10) {
      // .123. ->
      c = b.substr(0, 2) + '.' + b.substr(2, 2) + '.' + b.substr(4, 3) + '.' + b.substr(7, 3);
    } else if (panjang > 10 && panjang <= 13) {
      // 123.
      c = b.substr(0, 2) + '.' + b.substr(2, 2) + '.' + b.substr(4, 3) + '.' + b.substr(7, 3) + '.' + b.substr(10, 3);
    } else if (panjang > 13 && panjang <= 17) {
      // 1234
      c = b.substr(0, 2) + '.' + b.substr(2, 2) + '.' + b.substr(4, 3) + '.' + b.substr(7, 3) + '.' + b.substr(10, 3) + '.' + b.substr(13, 4);
    } else {
      // .0
      c = b.substr(0, 2) + '.' + b.substr(2, 2) + '.' + b.substr(4, 3) + '.' + b.substr(7, 3) + '.' + b.substr(10, 3) + '.' + b.substr(13, 4) + '.' + b.substr(17, 1);
      // alert(panjang);
    }
    objek.value = c;
  }
</script>