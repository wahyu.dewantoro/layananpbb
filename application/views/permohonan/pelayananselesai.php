<section class="content-header">
  <h1>
    Pelayanan Selesai
    <div class="pull-right">
        
    </div>
  </h1>
</section>
<section class="content">
  <div class="row">
      
        <div class="col-md-12">
          <!-- Custom Tabs -->
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#tab_1" data-toggle="tab">Belum di ambil</a></li>
              <li><a href="#tab_2" data-toggle="tab">Sudah di ambil</a></li>
            </ul>
            <div class="tab-content">
              <div class="tab-pane active" id="tab_1">
                <div class="table-responsive">
                    <table class="table table-bordered table-striped suka_suka" width="100%"> 
                        <thead>
                          <tr>
                            <th>No Layanan</th>
                            <th>NOP</th>
                            <th>pelayanan</th>
                            <th>Selesai  </th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php foreach($selesai as $rs){?>
                            <tr>
                                <td><?= $rs->THN_PELAYANAN.'.'.$rs->BUNDEL_PELAYANAN.'.'.$rs->NO_URUT_PELAYANAN?></td>
                                <td>
                                    <?= 
                                    $rs->KD_PROPINSI_PEMOHON.'.'.
                                    $rs->KD_DATI2_PEMOHON.'.'.
                                    $rs->KD_KECAMATAN_PEMOHON.'.'.
                                    $rs->KD_KELURAHAN_PEMOHON.'.'.
                                    $rs->KD_BLOK_PEMOHON.'.'.
                                    $rs->NO_URUT_PEMOHON.'.'.
                                    $rs->KD_JNS_OP_PEMOHON
                                    ?>
                                </td>
                                <td><?= $rs->NM_PELAYANAN?></td>
                                <td><?= $rs->TGL_SELESAI?></td>
                            </tr>
                          <?php } ?>
                        </tbody>
                    </table>
                </div>
              </div>
              <!-- /.tab-pane -->
              <div class="tab-pane" id="tab_2">
                <div class="table-responsive">
                 <table class="table table-striped suka_suka" width="100%"> 
                        <thead  width="100%">
                          <tr>
                            <th>No Layanan</th>
                            <th>NOP</th>
                            <th>pelayanan</th>
                            <th>Selesai  </th>
                          </tr>
                        </thead>
                        <tbody>
                          <?php foreach($diambil as $rdi){?>
                            <tr>
                                <td><?= $rdi->THN_PELAYANAN.'.'.$rdi->BUNDEL_PELAYANAN.'.'.$rdi->NO_URUT_PELAYANAN?></td>
                                <td>
                                    <?= 
                                    $rdi->KD_PROPINSI_PEMOHON.'.'.
                                    $rdi->KD_DATI2_PEMOHON.'.'.
                                    $rdi->KD_KECAMATAN_PEMOHON.'.'.
                                    $rdi->KD_KELURAHAN_PEMOHON.'.'.
                                    $rdi->KD_BLOK_PEMOHON.'.'.
                                    $rdi->NO_URUT_PEMOHON.'.'.
                                    $rdi->KD_JNS_OP_PEMOHON
                                    ?>
                                </td>
                                <td><?= $rdi->NM_PELAYANAN?></td>
                                <td><?= $rdi->TGL_SELESAI?></td>
                            </tr>
                          <?php } ?>
                        </tbody>
                    </table>
                  </div>
              </div>
              <!-- /.tab-pane -->
              
              <!-- /.tab-pane -->
            </div>
            <!-- /.tab-content -->
          </div>
          <!-- nav-tabs-custom -->
        </div>
        <!-- /.col -->
  </div>  
</section>

