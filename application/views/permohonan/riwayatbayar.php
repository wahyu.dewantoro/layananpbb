 <span class="badge">Riwayat Pembayran SPPT</span>
 <button class="bnn btn-success btn-xs" id="lanjut"> memenuhi persyaratan </button>
 <table style='font-size:12px' class='table table-hover table-bordered'>
 	<thead>
 		<tr>
 			<th>Tahun</th>
 			<th>PBB</th>
 			<th>Tanggal Bayar</th>
 		</tr>
 	</thead>
 	<tbody>
 		<?php foreach ($rk as $rk) { ?>

 			<tr>
 				<td align="center"><?php echo  $rk->TAHUN ?></td>
 				<td align="right"><?php echo  number_format($rk->BAYAR, '0', '', '.') ?></td>
 				<td align="center"><?php echo $rk->TANGGAL ?></td>
 			</tr>

 		<?php 		} ?>

 	</tbody>
 </table>
 <script type="text/javascript">
 	$(document).ready(function() {

 		$("#lanjut").click(function(e) {
 			e.preventDefault();
 			$('#myBtn').prop('disabled', false);
 			$('#tambahdiag').prop('disabled', false);

 		});
 	});
 </script>