
<section class="content-header">
          <h1>
            <?php echo $button ?>
            
          </h1>
          
        </section>
        <section class="content">
          <form class='form-horizontal' action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
          <div class="box">
            <div class="box-header with-border">
                <?php echo $this->session->flashdata('notif');?>  
              
            </div>
            <div class="box-body">
                        <div class="row">
                            <div class="col-md-10">
                                <div class="form-group">
                                    <label class="col-md-4 control-label">No Pelayanan </label>
                                    <div class="col-md-3">
                                        <input type="text" readonly class="form-control inputs"  maxlength="4" name="THN_PELAYANAN" id="THN_PELAYANAN" placeholder="Tahun" value="<?php echo $THN_PELAYANAN; ?>" required />
                                        
                                    </div>
                                    <div class="col-md-3">
                                        <input type="text" readonly class="form-control inputs"  maxlength="4"   name="BUNDEL_PELAYANAN" id="BUNDEL_PELAYANAN" placeholder="Bundel" required value="<?php echo $BUNDEL_PELAYANAN; ?>" />
                                        
                                    </div>
                                    <div class="col-md-2">
                                        <input type="text" readonly class="form-control inputs"  maxlength="3"  name="NO_URUT_PELAYANAN" id="NO_URUT_PELAYANAN" placeholder="No urut"  required value="<?php echo $NO_URUT_PELAYANAN; ?>" />
                                         
                                    </div>
                                </div>
                                  <div class="form-group">
                                    <label class="col-md-4 control-label">No Surat Permohonan </label>
                                    <div class="col-md-8">
                                        <input type="text" class="form-control inputs"  required name="NO_SRT_PERMOHONAN" id="NO_SRT_PERMOHONAN" placeholder="No surat permohonan" value="<?php echo $NO_SRT_PERMOHONAN; ?>" required/>
                                         
                                    </div>
                                </div>
                                 <div class="form-group">
                                    <label class="col-md-4 control-label">Tanggal Terima</label>
                                    <div class="col-md-8">
                                        <input type="text"  class="form-control tanggal11" data-inputmask="'mask': '99-99-9999'" required name="TGL_TERIMA_DOKUMEN_WP" id="TGL_TERIMA_DOKUMEN_WP" placeholder="TGL TERIMA DOKUMEN WP" value="<?php echo date('d-m-Y',strtotime($TGL_TERIMA_DOKUMEN_WP));?>" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Tanggal Perkiraan Selesai </label>
                                    <div class="col-md-8">
                                        <input type="text" class="form-control tanggal11" data-inputmask="'mask': '99-99-9999'" required name="TGL_PERKIRAAN_SELESAI" id="TGL_PERKIRAAN_SELESAI" placeholder="TGL PERKIRAAN SELESAI" value="<?php echo date('d-m-Y',strtotime($TGL_PERKIRAAN_SELESAI));?>" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Tanggal Surat Permohonan </label>
                                    <div class="col-md-8">
                                        <input type="text" required class="form-control tanggal11" data-inputmask="'mask': '99-99-9999'" name="TGL_SURAT_PERMOHONAN" id="TGL_SURAT_PERMOHONAN" placeholder="TGL SURAT PERMOHONAN" value="<?php echo date('d-m-Y',strtotime($TGL_SURAT_PERMOHONAN));?>" />
                                        
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Catatan </label>
                                    <div class="col-md-8">
                                        <input type="text" class="form-control" required name="CATATAN_PST" id="CATATAN_PST" placeholder="catatan" value="<?php echo $CATATAN_PST; ?>" />
                                        <input type="hidden" name="ID" value="<?php echo $id?>" >
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">Email</label>
                                    <div class="col-md-8">
                                        <input type="text" class="form-control" required name="IP_EMAIL" id="JUMLAH_BERKAS" value="<?php echo $IP_EMAIL; ?>" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-4 control-label">No. HP</label>
                                    <div class="col-md-8">
                                        <input  maxlength="14" type="number" class="form-control" required name="IP_HP" id="JUMLAH_NOP"  value="<?php echo $IP_HP; ?>" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-8 col-md-offset-4">
                                        <div class="btn-group">
                                            <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Update</button>
                                            <?php echo anchor('permohonan/detail/'.$id,'<i class="fa fa-list"></i> Kembali','class="btn btn-success"') ?>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                
                
            </div>
          </div>
          </form>
        </section>
<script type="text/javascript">
    
    


      var htmlobjek;
    $(document).ready(function(){ 
        

        
          
  $('body').on('keypress', 'input, select, textarea', function(e) {
    var self = $(this)
      , form = self.parents('form:eq(0)')
      , focusable
      , next
      ;
    if (e.keyCode == 13) {
        focusable = form.find('input,select,button,textarea').filter(':visible');
        next = focusable.eq(focusable.index(this)+1);
        if (next.length) {
            next.focus();
        } else {
            form.submit();
        }
        return false;

    }
});


      $(".form-control").keyup(function () {

          if (this.value.length == this.maxLength) {
            var nextIndex = $('input:text').index(this) + 1;
            $('input:text')[nextIndex].focus();
          }


      });
     
    });
    
</script>
    <!-- jquery.inputmask -->
    <script>
      $(document).ready(function() {
        $(":input").inputmask();
      });
      $(function() {
               // $( ".tanggal" ).datepicker();
           $('.tanggal1').datepicker().on('change', function(){
                $('.datepicker').hide(this.focus());
            });
      });
    </script>
    <!-- /jquery.inputmask -->


