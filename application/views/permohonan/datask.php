<section class="content-header">
    <h1>
        Data SK
        <div class="pull-right">

        </div>
    </h1>
</section>
<section class="content">
    <div class="box">
        <div class="box-body">

            <table class="table table-bordered table-striped" id="example2">
                <thead>
                    <tr>
                        <th width="5%">No</th>
                        <th>JENIS_SK</th>
                        <th>TANGGALSK</th>
                        <th>NOMOR_SK</th>
                        <th>NOP</th>
                        <th>TANGGALPERMOHONAN</th>
                        <th>NOPEL</th>
                        <th></th>
                    </tr>
                </thead>

            </table>
        </div>
    </div>
</section>



<script type="text/javascript">
    $(document).ready(function() {
        $.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings) {
            return {
                "iStart": oSettings._iDisplayStart,
                "iEnd": oSettings.fnDisplayEnd(),
                "iLength": oSettings._iDisplayLength,
                "iTotal": oSettings.fnRecordsTotal(),
                "iFilteredTotal": oSettings.fnRecordsDisplay(),
                "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
            };
        };

        var t = $("#example2").dataTable({
            initComplete: function() {
                var api = this.api();
                $('#mytable_filter input')
                    .off('.DT')
                    .on('keyup.DT', function(e) {
                        if (e.keyCode == 13) {
                            api.search(this.value).draw();
                        }
                    });
            },



            'oLanguage': {
                "sProcessing": "Sedang memproses...",
                "sLengthMenu": "Tampilkan _MENU_ entri",
                "sZeroRecords": "Tidak ditemukan data yang sesuai",
                "sInfo": "Menampilkan _START_ sampai _END_ dari _TOTAL_ entri",
                "sInfoEmpty": "Menampilkan 0 sampai 0 dari 0 entri",
                "sInfoFiltered": "(disaring dari _MAX_ entri keseluruhan)",
                "sInfoPostFix": "",
                "sSearch": "Cari:",
                "sUrl": "",
                "oPaginate": {
                    "sFirst": "Pertama",
                    "sPrevious": "Sebelumnya",
                    "sNext": "Selanjutnya",
                    "sLast": "Terakhir"
                }
            },
            processing: true,
            serverSide: true,
            ajax: {
                "url": "<?php echo base_url() ?>permohonan/jsondatask",
                "type": "POST"
            },
            columns: [{
                    "data": "ID",
                    "orderable": false,
                    "className": "text-center",
                },
                {
                    "data": "JENIS_SK"
                },
                {
                    "data": "TANGGALSK"
                },
                {
                    "data": "NOMOR_SK"
                },
                {
                    "data": "NOP"
                },
                {
                    "data": "TANGGALPERMOHONAN"
                },
                {
                    "data": "NOPEL"
                },
                {

                    "data": "JENIS_SK",
                    "orderable": false,
                    "className": "text-center",
                    render: function(data, type, row) {
                        var url = "<?= base_url() ?>";

                        var check = '<div class="btn btn-group">';
                        // check +='<a class="btn btn-xs btn-success" target="_blank" href="'+url+'permohonan/cetaksk/'+encodeURI(encodeURI(encodeURI(data)))+'/'+row.ID+'"><i class="fa fa-print"></i></a>';
                        var check = '<a class="btn btn-xs btn-info" target="_blank" href="' + url + 'permohonan/cetaksk?var=' + encodeURI(data) + '&id=' + row.ID + '"><i class="fa fa-print"></i></a>';
                        check += '</div>';
                        return check;
                    }
                },
            ],
            order: [
                [0, 'desc']
            ],
            rowCallback: function(row, data, iDisplayIndex) {
                var info = this.fnPagingInfo();
                var page = info.iPage;
                var length = info.iLength;
                var index = page * length + (iDisplayIndex + 1);
                $('td:eq(0)', row).html(index);
            }
        });
    });
</script>

<script type="text/javascript">
    $(document).ready(function() {
        $('#checkAll').click(function() {
            chk = $(this).is(':checked');
            if (chk) {
                $('.chek_all').prop('checked', true);
            } else {
                $('.chek_all').prop('checked', false);
            }
        });

        $("#proses").click(function() {
            var data = $('.chek_all:checked').serialize();

            // if(data!=null){

            var berkas_id = $(".chek_all:checked").map(function() {
                return $(this).val();
            }).get();

            $('#berkas_id').val(berkas_id);
            $('#myModal').modal('show');



            /*$.ajax({
                type: 'POST',
                url: "<?php echo base_url() . 'permohonan/validasiCheckAll' ?>",
                data: data,
                success: function(msg) {
                        // window.location='<?= base_url("permohonan/listdokumen") ?>';
                }
            });*/

        });
        // }
    });
</script>