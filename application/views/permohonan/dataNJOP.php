<section class="content-header">
  <h1>
    SK NJOP
    <div class="pull-right">

    </div>
  </h1>
</section>
<section class="content">
  <div class="box">
    <div class="box-body">
      <form class="form-inline" method="post" action="<?php echo base_url() . 'permohonan/dataNJOP.html' ?>" autocomplete="off">
        <div class="form-group">
          <div class="input-group">
            <input type="text" class="form-control tanggal" name="p_tanggal1" required value="<?php echo $p_tanggal1 ?>">
          </div>
        </div>
        <div class="form-group">
          <div class="input-group">
            <input type="text" class="form-control tanggal" name="p_tanggal2" required value="<?php echo $p_tanggal2 ?>">

          </div>
        </div>
        <div class="form-group">
          <button type="submit" class="btn btn-xs btn-success"><i class="fa fa-search"></i> Cari</button>
          <a class="btn btn-xs btn-primary" href="<?= base_url().'permohonan/dataNJOPexcel?t1='.urlencode($p_tanggal1).'&t2='.urlencode($p_tanggal2) ?>"><i class="fa fa-file"></i> Excell</a>
        </div>



      </form>
      <table class="table table-bordered table-hover" id="example2">
        <thead>
          <tr>
            <th width="5%">No</th>
            <th>No Layanan</th>
            <th>Tanggal</th>
            <th>Pemohon</th>
            <th>Kolektif NOP</th>
            <th>Aksi</th>
          </tr>
        </thead>

      </table>
    </div>
  </div>
  </div>
</section>
<div class="visible-print" id="div1"></div>
<script type="text/javascript">
  $(document).ready(function() {
    $.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings) {
      return {
        "iStart": oSettings._iDisplayStart,
        "iEnd": oSettings.fnDisplayEnd(),
        "iLength": oSettings._iDisplayLength,
        "iTotal": oSettings.fnRecordsTotal(),
        "iFilteredTotal": oSettings.fnRecordsDisplay(),
        "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
        "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
      };
    };

    var t = $("#example2").dataTable({
      initComplete: function() {
        var api = this.api();
        $('#mytable_filter input')
          .off('.DT')
          .on('keyup.DT', function(e) {
            if (e.keyCode == 13) {
              api.search(this.value).draw();
            }
          });
      },



      'oLanguage': {
        "sProcessing": "Sedang memproses...",
        "sLengthMenu": "Tampilkan _MENU_ entri",
        "sZeroRecords": "Tidak ditemukan data yang sesuai",
        "sInfo": "Menampilkan _START_ sampai _END_ dari _TOTAL_ entri",
        "sInfoEmpty": "Menampilkan 0 sampai 0 dari 0 entri",
        "sInfoFiltered": "(disaring dari _MAX_ entri keseluruhan)",
        "sInfoPostFix": "",
        "sSearch": "Cari:",
        "sUrl": "",
        "oPaginate": {
          "sFirst": "Pertama",
          "sPrevious": "Sebelumnya",
          "sNext": "Selanjutnya",
          "sLast": "Terakhir"
        }
      },
      processing: true,
      serverSide: true,
      ajax: {
        "url": "<?php echo base_url() ?>permohonan/jsonNjop",
        "type": "POST",
        "data": {
          "p_tanggal1": "<?= $p_tanggal1 ?>",
          "p_tanggal2": "<?= $p_tanggal2 ?>"
        }
      },
      columns: [{
          "data": "NOPEL",
          "className": "text-center",
          "orderable": false
        },
        {
          "data": "NOPEL"
        },
        {
          "data": "TGL_PERMOHONAN"
        },
        {
          "data": "NAMA_PEMOHON"
        },
        {
          "className": "text-center",
          "data": "JUMLAH"
        },
         {
           "data": "NOPEL",
           render: function(data, type, row) {
             var url = "<?= base_url() . 'permohonan/cetaksknjop/' ?>";
             var check = '<div class="btn btn-group">';
             if (row.NOMOR_SK == null) {
               check += '<a class="btn btn-xs btn-primary"  target="_blank" href="' + url + data + '"><i class="fa fa-refresh"></i> Proses</a>';
             } else {
               check += '<a class="btn btn-xs btn-success cetak" target="_blank" data-nopel="' + data + '"  href="' + url + data + '"><i class="fa fa-print"></i> Cetak</a>';
               check += '<a class="btn btn-xs btn-warning" target="_blank"  href="' + url + data + '/pdf"><i class="fa fa-pdf"></i> PDF</a>';
             }


             check += '</div>';
             return check;
           },
           "orderable": false,
           "className": "text-center",
         },

      ],
      order: [],
      rowCallback: function(row, data, iDisplayIndex) {
        var info = this.fnPagingInfo();
        var page = info.iPage;
        var length = info.iLength;
        var index = page * length + (iDisplayIndex + 1);
        $('td:eq(0)', row).html(index);
      }
    });
  });





  /*
              $(document).on('click','.cetak',function(e){
                  e.preventDefault();
                  $('html, body').css("cursor", "wait");

                  var nopel=$(this).attr('data-nopel');
                  $("#div1").html('');
                      $.ajax({url:"<?php echo base_url() ?>permohonan/cetaksknjop/"+nopel, success: function(result){
                         $('html, body').css("cursor", "auto");
                        $("#div1").html(result);
                        // window.print();
                        printDiv('div1');
                    }});
                  
              });*/

  /*
               function printDiv(divName){
        var printContents = document.getElementById(divName).innerHTML;
        var originalContents = document.body.innerHTML;
        document.body.innerHTML = printContents;
        window.print();
        document.body.innerHTML = originalContents;
      }*/
</script>