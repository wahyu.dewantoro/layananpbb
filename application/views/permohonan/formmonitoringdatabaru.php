<section class="content-header">
  <h1>
    Monitoring
    <div class="pull-right">

    </div>
  </h1>

</section>
<!-- Main content -->
<section class="content">
  <!-- Default box -->
  <div class="box">
    <div class="box-body">
      <form method="post" class="form-horizontal" action="<?php echo base_url() . 'permohonan/monitorBaru' ?>">
        <div class="form-group">
          <label class="control-label col-md-2">Filter</label>
          <div class="col-md-8">
            <select class="form-control" name="filter" id="filter" onchange="myFunction()">
              <?php $arr = array('No Layanan');
              foreach ($arr as $arr) { ?>
                <option <?php if ($filter == $arr) {
                          echo "selected";
                        } ?> value="<?php echo $arr ?>"><?php echo $arr ?></option>
              <?php } ?>
            </select>
          </div>
        </div>
        <div id="demo">
          <?php if ($filter != 'NOP') { ?>

            <div class="form-group">
              <label class="control-label col-md-2">No Layanan</label>
              <div class="col-md-8">
                <input type="text" name="no_layanan" class="form-control" value="<?php echo $no_layanan ?>" required placeholder="Isikan no layanan di sini">
              </div>

            </div>
          <?php } ?>


        </div>
        <div class="form-group">
          <div class="col-md-8 col-md-offset-2">
            <button class="btn btn-primary btn-sm" type="submit"><i class="fa fa-search"></i> Cari</button>
          </div>
        </div>
      </form>
      <?php if (isset($res)) { ?>
        <hr>
        <table class="table table-hover table-bordered ">
          <thead>
            <tr>
              <th>No</th>
              <th>NOP</th>
              <th>Status</th>
              <th>perekaman OP</th>
              <th>NIP</th>
            </tr>
          </thead>
          <tbody>
            <?php if (count($res) > 0) {
              $no = 1;
              foreach ($res as $res) { ?>
                <tr>
                  <td align="center"><?php echo $no++; ?></td>

                  <td><?php echo $res->NOP ?></td>
                  <td><?php if ($res->STATUS == 1) {
                        echo "Selesai";
                      } else {
                        echo "Belum";
                      } ?></td>
                  <td align="center"><?= $res->TGL_REKAM ?></td>
                  <td align="center"><?= $res->NIP_PEREKAM ?></td>
                </tr>
              <?php }
            } else { ?>
              <tr>
                <td colspan="6">Data tidak ada</td>
              </tr>
            <?php } ?>
          </tbody>
        </table>
      <?php } ?>
    </div><!-- /.box-body -->
  </div><!-- /.box -->
</section><!-- /.content -->

<!-- Modal -->
<div class="modal fade bs-example-modal-lg" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content  modal-lg">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
        <h4 class="modal-title" id="myModalLabel">Monitor Permohonan</h4>
      </div>
      <div class="modal-body">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">
  $(function() {
    $(document).on('click', '.edit-record', function(e) {
      e.preventDefault();
      $("#myModal").modal('show');
      $.post('<?php echo base_url() . "permohonan/viewmonitoring" ?>', {
          id: $(this).attr('data-id')
        },
        function(html) {
          $(".modal-body").html(html);
        }
      );
    });
  });


  $(".form-control").keyup(function() {

    if (this.value.length == this.maxLength) {
      var nextIndex = $('input:text').index(this) + 1;
      $('input:text')[nextIndex].focus();
    }


  });

  function myFunction() {
    var x = document.getElementById("filter").value;
    // 
    if (x == 'No Layanan') {
      document.getElementById("demo").innerHTML = '<div class="form-group">' +
        '<label class="control-label col-md-2">No Layanan</label>' +
        '<div class="col-md-8">' +
        '<input type="text" name="no_layanan" class="form-control" required placeholder="Isikan no layanan di sini">' +
        '</div>';
    } else if (x == 'NOP') {
      document.getElementById("demo").innerHTML = '<div class="form-group">' +
        '<label class="col-md-2 control-label">NOP </label>' +
        '<div class="col-md-1 dua">' +
        '<input type="text"  maxLength="2" class="form-control" name="KD_PROPINSI_PEMOHON" id="KD_PROPINSI_PEMOHON" value="35">' +
        '</div>' +
        '<div class="col-md-1 dua">' +
        '<input type="text"  maxLength="2" class="form-control" name="KD_DATI2_PEMOHON" id="KD_DATI2_PEMOHON" value="07">' +
        '</div>' +
        '<div class="col-md-1 tiga">' +
        '<input type="text"  maxLength="3" class="form-control" name="KD_KECAMATAN_PEMOHON" id="KD_KECAMATAN_PEMOHON" onfocus="this.value=(null)">' +
        '</div>' +
        '<div class="col-md-1 tiga">' +
        '<input type="text"  maxLength="3" class="form-control" name="KD_KELURAHAN_PEMOHON" id="KD_KELURAHAN_PEMOHON" onfocus="this.value=(null)">' +
        '</div>' +
        '<div class="col-md-1 tiga">' +
        '<input type="text"   maxLength="3" class="form-control" name="KD_BLOK_PEMOHON" id="KD_BLOK_PEMOHON" onfocus="this.value=(null)">' +
        '</div>' +
        '<div class="col-md-1 empat">' +
        '<input type="text"  maxLength="4" class="form-control" name="NO_URUT_PEMOHON" id="NO_URUT_PEMOHON" onfocus="this.value=(null)">' +
        '</div>' +
        '<div class="col-md-1 satu">' +
        '<input type="text"  maxLength="1" class="form-control" name="KD_JNS_OP_PEMOHON" id="KD_JNS_OP_PEMOHON" onfocus="this.value=(null)">' +
        '</div>' +
        '</div>';


      $(".form-control").keyup(function() {

        if (this.value.length == this.maxLength) {
          var nextIndex = $('input:text').index(this) + 1;
          $('input:text')[nextIndex].focus();
        }

        $('body').on('keypress', 'input, select, textarea', function(e) {
          var self = $(this),
            form = self.parents('form:eq(0)'),
            focusable, next;
          if (e.keyCode == 13) {
            focusable = form.find('input,select,button,textarea').filter(':visible');
            next = focusable.eq(focusable.index(this) + 1);
            if (next.length) {
              next.focus();
            } else {
              form.submit();
            }
            return false;

          }
        });
      });
    }
  }
</script>
<script type="text/javascript">
  function stopRKey(evt) {
    var evt = (evt) ? evt : ((event) ? event : null);
    var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);
    if ((evt.keyCode == 13) && (node.type == "text")) {
      return false;
    }
  }
  document.onkeypress = stopRKey;
</script>

<style type="text/css">
  .dua {
    width: 4%;
  }

  .tiga {
    width: 5%;
  }

  .empat {
    width: 6%;
  }

  .satu {
    width: 3%;
  }
</style>