
 
      <section class="content-header">
        <h1>
          Detail Lampiran
        </h1>
      </section>
      <section class="content">
          <div class="box">
            <div class="box-body">
              <div class="row" style='margin-left:0px;'>
                <div class="col-md-6" >
                  <table class="table">
                      <tbody>
                          <tr>
                            <td width="20%">No Layanan</td>
                            <td width="1%">:</td>
                            <td ><?php echo $rk->NO_LAYANAN?></td>
                          </tr>
                          <tr>
                            <td>Tanggal Pelayanan</td>
                            <td>:</td>
                            <td><?php echo date('d/m/Y',strtotime($rk->TGL_TERIMA_DOKUMEN_WP))?></td>
                          </tr>
                          <tr>
                            <td>Tanggal Selesai (<small>Perkiraan</small>)</td>
                            <td>:</td>
                            <td><?php echo date('d/m/Y',strtotime($rk->TGL_PERKIRAAN_SELESAI))?></td>
                          </tr>
                          <tr>
                            <td>Jenis Pelayanan </td>
                            <td>:</td>
                            <td><?php if($rk->STATUS_KOLEKTIF=='0'){ echo $rk->NM_JENIS_PELAYANAN.' / Perseorangan'; }else{ echo $rk->NM_JENIS_PELAYANAN." / Kolektif";}   ?></td>
                          </tr>
                          <tr>
                            <td>Nama Pemohon</td>
                            <td>:</td>
                            <td><?php echo $rk->NAMA_PEMOHON?></td>
                          </tr>
                          <tr>
                            <td>Alamat Pemohon</td>
                            <td>:</td>
                            <td><?php echo $rk->ALAMAT_PEMOHON?></td>
                          </tr>
                          <tr>
                            <td>Keterangan</td>
                            <td>:</td>
                            <td><?php echo $rk->KETERANGAN_PST?></td>
                          </tr>
                      </tbody>
                  </table>
                </div>
                <div class="col-md-6" >
                  Dokumen Terlampir :
                              <ol>
                                  <li><input class="searchType" type="checkbox" <?php  if($dok->L_PERMOHONAN =='1'){echo "checked";}?> id="<?php echo "L_PERMOHONAN".'|'.$id.'|PST';?>">Pengajuan Permohonan </li>
                                  <li><input class="searchType" type="checkbox" <?php  if($dok->L_BAHASA =='1'){echo "checked";}?> id="<?php echo "L_BAHASA".'|'.$id.'|P';?>">Diajukan secara tertulis </li>
                                  <li><input class="searchType" type="checkbox" <?php  if($dok->L_BPD =='1'){echo "checked";}?> id="<?php echo "L_BPD".'|'.$id.'|P';?>">Diajukan kepada Kepala BPD</li>
                                  <li><input class="searchType" type="checkbox" <?php  if($dok->L_SKET_LURAH =='1'){echo "checked";}?> id="<?php echo "L_SKET_LURAH".'|'.$id.'|PST';?>">Sket Lurah </li>
                                  <li><input class="searchType" type="checkbox" <?php  if($dok->L_SPOP_LSPOP =='1'){echo "checked";}?> id="<?php echo "L_SPOP_LSPOP".'|'.$id.'|P';?>">SPOP dan LSPOP</li>
                                  <li><input class="searchType" type="checkbox" <?php  if($dok->L_KTP_WP     =='1'){echo "checked";}?> id="<?php echo "L_KTP_WP".'|'.$id.'|PST';?>">FC KTP/SIM dan KK </li>
                                  <li><input class="searchType" type="checkbox" <?php  if($dok->L_SERTIFIKAT_TANAH =='1'){echo "checked";}?> id="<?php echo "L_SERTIFIKAT_TANAH".'|'.$id.'|PST';?>">FC Sertifikat /  Kepemilikan </li>
                                  <li><input class="searchType" type="checkbox" <?php  if($dok->L_SPPT_STTS =='1'){echo "checked";}?> id="<?php echo "L_SPPT_STTS".'|'.$id.'|PST';?>">FC SPPT / SSPD / Bukti lunas</li>
                                  <li><input class="searchType" type="checkbox" <?php  if($dok->L_SPPT_T=='1'){echo "checked";}?> id="<?php echo "L_SPPT_T".'|'.$id.'|P';?>">SPPT Tetangga </li>
                                  <li><input class="searchType" type="checkbox" <?php  if($dok->L_SURAT_KUASA=='1'){echo "checked";}?> id="<?php echo "L_SURAT_KUASA".'|'.$id.'|PST';?>">Surat Kuasa </li>
                                  <li><input class="searchType" type="checkbox" <?php  if($dok->L_WAKTU =='1'){echo "checked";}?> id="<?php echo "L_WAKTU".'|'.$id.'|P';?>">Jangka waktu Terpenuhi</li>
                                  <li><input class="searchType" type="checkbox" <?php  if($dok->L_TUNGGAKAN =='1'){echo "checked";}?> id="<?php echo "L_TUNGGAKAN".'|'.$id.'|P';?>">Tidak Memliki Tunggakan</li>
                                  <li><input class="searchType" type="checkbox" <?php  if($dok->L_SKD_PO =='1'){echo "checked";}?> id="<?php echo "L_SKD_PO".'|'.$id.'|P';?>">SK Desa alasan pernyataan penghapusan</li>
                                  <li><input class="searchType" type="checkbox" <?php  if($dok->L_WP =='1'){echo "checked";}?> id="<?php echo "L_WP".'|'.$id.'|P';?>">FC WP</li>
                                  <li><input class="searchType" type="checkbox" <?php  if($dok->L_SPPT_STTS =='1'){echo "checked";}?> id="<?php echo "L_SPPT_STTS".'|'.$id.'|PST';?>">FC SPPT/SKP PBB</li>
                                  <li><input class="searchType" type="checkbox" <?php  if($dok->L_SK_PENDUKUNG =='1'){echo "checked";}?> id="<?php echo "L_SK_PENDUKUNG".'|'.$id.'|P';?>">SK pendukung alasan penghapusan</li>
                                  <li><input class="searchType" type="checkbox" <?php  if($dok->L_PERSENTASE =='1'){echo "checked";}?> id="<?php echo "L_PERSENTASE".'|'.$id.'|P';?>">Persentase Pengurangan dicantumkan dan disertai alasan yang jelas</li>
                                  <li><input class="searchType" type="checkbox" <?php  if($dok->L_BANDING =='1'){echo "checked";}?> id="<?php echo "L_BANDING".'|'.$id.'|P';?>">SK Keberatan/Banding</li>
                                  <br>
                                  <?php if ($rk->KD_PENGAJUAN==null) {
                                     echo "BELUM ADA KODE PENGAJUAN";
                                  } else {?>
                                      <?php if ($rk->NOMOR_URUT_LAMPIRAN==null) {?>
                                        <a href="#" id="<?php echo $id.'/'.$rk->KD_PENGAJUAN?>" class='class="btn btn-warning btn-sm' ><i class="fa fa-edit"> verifikasi</i></a>
                                      <?php  
                                      } else {?>
                                        <a href="<?php echo base_url().'permohonan/cetak_lembar_penelitian/'.$id.'/'.$rk->STATUS_KOLEKTIF;?>" class='class="btn btn-success btn-sm' target="_blank"><i class="fa fa-print"> Cetak</i></a>
                                      <?php }?>
                                  <?php
                                  }?>
                                  <a href="javascript:history.back()" class='class="btn btn-primary btn-sm'><i class="fa fa-back"> Kembali</i></a>
                              </ol>
                </div>
              </dv>  
            </div>
          </div>     
      </section> 

<script type="text/javascript">
$('.searchType').click(function() {
    //alert($(this).attr('id'));  //-->this will alert id of checked checkbox.
    var isi=$(this).attr('id');
       if(this.checked){
            $.ajax({
                type: "POST",
                url: "<?php echo base_url().'permohonan/update_berkas1'?>",
                data: {id:isi}, //--> send id of checked checkbox on other page
                success: function(data) {
                    // alert(data);
                    //$('#container').html(data);
                },
                 /*error: function() {
                    alert('it broke');
                },
                complete: function() {
                    alert('it completed');
                }*/
            });
          }else{
            $.ajax({
                type: "POST",
                url: "<?php echo base_url().'permohonan/update_berkas0'?>",
                data: {id:isi}, //--> send id of checked checkbox on other page
                success: function(data) {
                    // alert(data);
                    //$('#container').html(data);
                },
            });
          }
      });
</script>
<script type="text/javascript">
        $(document).ready(function(){
    $(document).delegate('.btn-warning', 'click', function() { 
            var id = $(this).attr('id');
            var data = 'recordToDelete='+ id;
            var parent = $(this).parent().parent();
            //alert(id);
            $.ajax({
                type: "POST",
                url: "<?php echo base_url('permohonan/proses_penomoran'); ?>",
                data:{id:id},
                cache: false,
                success: function(data)
                {
                   //$('#'+id).hide();
                   //alert(data);
                    setTimeout(function(){// wait for 5 secs(2)
                           location.reload(); // then reload the page.(3)
                      }, 1);
                    //alert(data);//parent.fadeOut('slow', function() {$(this).remove();});
                }
            });                
    });
    $('table#userTabla tr:odd').css('background',' #FFFFFF');
});
        </script>



  