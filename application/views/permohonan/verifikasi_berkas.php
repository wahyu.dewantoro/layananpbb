<section class="content-header">
  <h1>
    Verifikasi Berkas
    <div class="pull-right">

    </div>
  </h1>
</section>
<section class="content">
  <div class="box">
    <div class="box-body">
      <form class="form-inline" method="get" action="<?php echo base_url() . 'permohonan/verifikasi_berkas.html' ?>">
        <div class="form-group">
          <input type="text" class="form-control tanggal" name="p_tanggal1" required value="<?php echo $this->session->userdata('p_tanggal1') ?>">
        </div>
        <div class="form-group">
          <input type="text" class="form-control tanggal" name="p_tanggal2" required value="<?php echo $this->session->userdata('p_tanggal2') ?>">
        </div>
        <div class="form-group">
          <select class="form-control chosen" name="KD_JNS_PELAYANAN" id="KD_JNS_PELAYANAN">
            <option value=""></option>
            <?php foreach ($pelayanan as $rp) { ?>
              <option value="<?php echo $rp->KD_JNS_PELAYANAN ?>" <?php if ($this->session->userdata('P_KD_JNS_PELAYANAN') == $rp->KD_JNS_PELAYANAN) {
                                                                    echo "selected";
                                                                  } ?>><?php echo $rp->NM_JENIS_PELAYANAN ?></option>
            <?php } ?>
          </select>
        </div>
        <button type="submit" class="btn btn-success"><i class="fa fa-search"></i> Cari</button>
      </form>
      <table class="table table-bordered table-striped" id="example2">
        <thead>
          <tr>
            <th width="5%">No</th>
            <th>No Layanan</th>
            <th>Pemohon</th>
            <th>Jenis Pelayanan</th>
            <th>KD Pengajuan</th>
            <th>Status</th>
            <th>Terima</th>
            <th>Selesai (Perkiraan)</th>
            <th width="8%">Aksi</th>

          </tr>
        </thead>

      </table>
    </div>
  </div>
</section>
<script type="text/javascript">
  $(document).ready(function() {
    $.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings) {
      return {
        "iStart": oSettings._iDisplayStart,
        "iEnd": oSettings.fnDisplayEnd(),
        "iLength": oSettings._iDisplayLength,
        "iTotal": oSettings.fnRecordsTotal(),
        "iFilteredTotal": oSettings.fnRecordsDisplay(),
        "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
        "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
      };
    };

    var t = $("#example2").dataTable({
      initComplete: function() {
        var api = this.api();
        $('#mytable_filter input')
          .off('.DT')
          .on('keyup.DT', function(e) {
            if (e.keyCode == 13) {
              api.search(this.value).draw();
            }
          });
      },



      'oLanguage': {
        "sProcessing": "Sedang memproses...",
        "sLengthMenu": "Tampilkan _MENU_ entri",
        "sZeroRecords": "Tidak ditemukan data yang sesuai",
        "sInfo": "Menampilkan _START_ sampai _END_ dari _TOTAL_ entri",
        "sInfoEmpty": "Menampilkan 0 sampai 0 dari 0 entri",
        "sInfoFiltered": "(disaring dari _MAX_ entri keseluruhan)",
        "sInfoPostFix": "",
        "sSearch": "Cari:",
        "sUrl": "",
        "oPaginate": {
          "sFirst": "Pertama",
          "sPrevious": "Sebelumnya",
          "sNext": "Selanjutnya",
          "sLast": "Terakhir"
        }
      },
      processing: true,
      serverSide: true,
      ajax: {
        "url": "<?php echo base_url() ?>permohonan/json_ferifikasi_berkas",
        "type": "post"
      },
      columns: [{
          "data": "NO_LAYANAN",
          "orderable": false
        },
        {
          "data": "NO_LAYANAN"
        },
        {
          "data": "NAMA_PEMOHON"
        },
        {
          "data": "NM_JENIS_PELAYANAN"
        },
        {
          "data": "CONCAT_NOMOR"
        },
        {
          "data": "KET_KOLEKTIF"
        },
        {
          "data": "TGL_TERIMA"
        },
        {
          "data": "TGL_SELESAI"
        },
        {
          "data": "KD_STATUS_KOLEKTIF",
          render: function(data, type, row) {
            this.url = '<?php echo base_url() ?>';
            if (row.KD_PENGAJUAN == null) {
              var d = '<a href="' + this.url + 'permohonan/verifikasi_detail/' + row.NO_LAYANAN + '" class="btn btn-xs btn-info " ><i class="fa fa-search"></i></a>';
            } else {
              if (row.NOMOR_URUT_LAMPIRAN == null) {
                var d = '<a href="' + this.url + 'permohonan/verifikasi_detail/' + row.NO_LAYANAN + '" class="btn btn-xs btn-info " ><i class="fa fa-search"></i></a><a id="' + row.NO_LAYANAN + '/' + row.KD_PENGAJUAN + '" class="btn btn-xs btn-warning pp" href="#"><i class="fa fa-edit"></i> verifikasi</a>';
              } else {
                var d = '<a href="' + this.url + 'permohonan/verifikasi_detail/' + row.NO_LAYANAN + '" class="btn btn-xs btn-info " ><i class="fa fa-search"></i></a><a href="' + this.url + 'permohonan/cetak_lembar_penelitian/' + row.NO_LAYANAN + '/' + row.KD_STATUS_KOLEKTIF + '" class="btn btn-xs btn-success"><i class="fa fa-print"></i></a>';
              };
            };

            return d;
          }
        },
      ],
      order: [],
      rowCallback: function(row, data, iDisplayIndex) {
        var info = this.fnPagingInfo();
        var page = info.iPage;
        var length = info.iLength;
        var index = page * length + (iDisplayIndex + 1);
        $('td:eq(0)', row).html(index);
      }
    });
  });
</script>
<script type="text/javascript">
  /*$(function(){
                  $('.trash').click(function(){
                      var del_id= $(this).attr('id');
                      var $ele = $(this).parent().parent();
                      alert(del_id);
                      $.ajax({
                            type:'POST',
                            url:'<?php echo base_url('permohonan/hapus_data_img'); ?>',
                            data:{del_id:del_id},
                            success: function(data){
                               
                            }

                             })
                        })
              });*/
</script>
<script type="text/javascript">
  $(document).ready(function() {
    $(document).delegate('.btn-warning', 'click', function() {
      /*if (confirm("Do you really want to delete User's record?"))
      {*/
      var id = $(this).attr('id');
      var data = 'recordToDelete=' + id;
      var parent = $(this).parent().parent();
      //alert(id);
      $.ajax({
        type: "POST",
        url: "<?php echo base_url('permohonan/proses_penomoran'); ?>",
        data: {
          id: id
        },
        cache: false,
        success: function(data) {
          //$('#'+id).hide();
          setTimeout(function() { // wait for 5 secs(2)
            location.reload(); // then reload the page.(3)
          }, 1);
          //alert(data);
          //alert(data);//parent.fadeOut('slow', function() {$(this).remove();});
        }
      });
      // }
    });
    $('table#userTabla tr:odd').css('background', ' #FFFFFF');
  });
</script>