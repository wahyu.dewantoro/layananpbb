        <section class="content-header">
          <h1>
            Cek Pendataan
            <div class="pull-right">

            </div>
          </h1>

        </section>
        <!-- Main content -->
        <section class="content">
          <!-- Default box -->
          <div class="box">
            <div class="box-body">
              <form method="get" class="form-horizontal" action="<?php echo base_url() . 'permohonan/cekpendataan' ?>">
                <div class="form-group">
                  <label class="control-label col-md-2">Filter</label>
                  <div class="col-md-4">
                    <select class="form-control" name="filter" id="filter" onchange="myFunction()">
                      <?php $arr = array('No Layanan', 'NOP');
                      foreach ($arr as $arr) { ?>
                        <option <?php if ($filter == $arr) {
                                  echo "selected";
                                } ?> value="<?php echo $arr ?>"><?php echo $arr ?></option>
                      <?php } ?>
                    </select>
                  </div>
                </div>
                <div id="demo">
                  <?php if ($filter != 'NOP') { ?>
                    <div class="form-group">
                      <label class="control-label col-md-2">No Layanan</label>
                      <div class="col-md-4">
                        <input type="text" name="no_layanan" class="form-control" value="<?php echo $no_layanan ?>" required placeholder="Isikan no layanan di sini">
                      </div>
                    </div>
                  <?php } else { ?>
                    <div class="form-group">
                      <label class="col-md-2 control-label">NOP </label>
                      <div class="col-md-1 dua">
                        <input type="text" maxLength="2" class="form-control" value="<?php echo $KD_PROPINSI_PEMOHON ?>" name="KD_PROPINSI_PEMOHON" id="KD_PROPINSI_PEMOHON" onfocus="this.value=''">
                      </div>
                      <div class="col-md-1 dua">
                        <input type="text" maxLength="2" class="form-control" value="<?php echo $KD_DATI2_PEMOHON ?>" name="KD_DATI2_PEMOHON" id="KD_DATI2_PEMOHON" onfocus="this.value=''">
                      </div>
                      <div class="col-md-1 tiga">
                        <input type="text" maxLength="3" class="form-control" value="<?php echo $KD_KECAMATAN_PEMOHON ?>" name="KD_KECAMATAN_PEMOHON" id="KD_KECAMATAN_PEMOHON" onfocus="this.value=''">
                      </div>
                      <div class="col-md-1 tiga">
                        <input type="text" maxLength="3" class="form-control" value="<?php echo $KD_KELURAHAN_PEMOHON ?>" name="KD_KELURAHAN_PEMOHON" id="KD_KELURAHAN_PEMOHON" onfocus="this.value=''">
                      </div>
                      <div class="col-md-1 tiga">
                        <input type="text" maxLength="3" class="form-control" value="<?php echo $KD_BLOK_PEMOHON ?>" name="KD_BLOK_PEMOHON" id="KD_BLOK_PEMOHON" onfocus="this.value=''">
                      </div>
                      <div class="col-md-1 empat">
                        <input type="text" maxLength="4" class="form-control" value="<?php echo $NO_URUT_PEMOHON ?>" name="NO_URUT_PEMOHON" id="NO_URUT_PEMOHON" onfocus="this.value=''">
                      </div>
                      <div class="col-md-1 satu">
                        <input type="text" maxLength="1" class="form-control" value="<?php echo $KD_JNS_OP_PEMOHON ?>" name="KD_JNS_OP_PEMOHON" id="KD_JNS_OP_PEMOHON" onfocus="this.value=''">
                      </div>
                    </div>
                  <?php } ?>

                </div>


                <div class="form-group">
                  <div class="col-md-8 col-md-offset-2">
                    <button class="btn btn-primary btn-sm" type="submit"><i class="fa fa-search"></i> Cari</button>
                  </div>
                </div>
              </form>

            </div><!-- /.box-body -->
          </div><!-- /.box -->
          <div class="row">
            <?php foreach ($data as $rk) { ?>
              <div class="col-md-3">
                <div class="box box-primary">
                  <div class="box-header with-border">
                    <h3 class="box-title">NOP : <?= $rk->KD_PROPINSI_PEMOHON . '.' . $rk->KD_DATI2_PEMOHON . '.' . $rk->KD_KECAMATAN_PEMOHON . '.' . $rk->KD_KELURAHAN_PEMOHON . '.' . $rk->KD_BLOK_PEMOHON . '.' . $rk->NO_URUT_PEMOHON . '.' . $rk->KD_JNS_OP_PEMOHON ?></h3>
                  </div>
                  <!-- /.box-header -->
                  <div class="box-body">
                    <strong><i class="fa fa-book margin-r-5"></i> Permohonan</strong>
                    <p class="text-muted">
                      <p class="text-muted"><?= $rk->PERMOHONAN <> '' ? $rk->PERMOHONAN : '-'; ?></p>
                    </p>
                    <hr>
                    <strong><i class="fa fa-map-marker margin-r-5"></i> Pendataan</strong>
                    <p class="text-muted">
                      <p class="text-muted"><?= $rk->PENDATAAN <> '' ? $rk->PENDATAAN : '-'; ?></p>
                    </p>
                    <hr>
                    <strong><i class="fa fa-file-text-o margin-r-5"></i> Penetapan</strong>
                    <p class="text-muted">
                      <p class="text-muted"><?= $rk->PENETAPAN <> '' ? $rk->PENETAPAN : '-'; ?></p>
                    </p>
                  </div>
                  <!-- /.box-body -->
                </div>
                <!-- /.box -->
              </div>
            <?php } ?>

          </div>
        </section><!-- /.content -->



        <script type="text/javascript">
          function myFunction() {
            var x = document.getElementById("filter").value;
            // 
            if (x == 'No Layanan') {
              document.getElementById("demo").innerHTML = '<div class="form-group">' +
                '<label class="control-label col-md-2">No Layanan</label>' +
                '<div class="col-md-4">' +
                '<input type="text" name="no_layanan" class="form-control" required placeholder="Isikan no layanan di sini">' +
                '</div>';
            } else if (x == 'NOP') {
              document.getElementById("demo").innerHTML = '<div class="form-group">' +
                '<label class="col-md-2 control-label">NOP </label>' +
                '<div class="col-md-1 dua">' +
                '<input type="text"  maxLength="2" class="form-control" name="KD_PROPINSI_PEMOHON" id="KD_PROPINSI_PEMOHON" value="35">' +
                '</div>' +
                '<div class="col-md-1 dua">' +
                '<input type="text"  maxLength="2" class="form-control" name="KD_DATI2_PEMOHON" id="KD_DATI2_PEMOHON" value="07">' +
                '</div>' +
                '<div class="col-md-1 tiga">' +
                '<input type="text"  maxLength="3" class="form-control" name="KD_KECAMATAN_PEMOHON" id="KD_KECAMATAN_PEMOHON" onfocus="this.value=(null)">' +
                '</div>' +
                '<div class="col-md-1 tiga">' +
                '<input type="text"  maxLength="3" class="form-control" name="KD_KELURAHAN_PEMOHON" id="KD_KELURAHAN_PEMOHON" onfocus="this.value=(null)">' +
                '</div>' +
                '<div class="col-md-1 tiga">' +
                '<input type="text"   maxLength="3" class="form-control" name="KD_BLOK_PEMOHON" id="KD_BLOK_PEMOHON" onfocus="this.value=(null)">' +
                '</div>' +
                '<div class="col-md-1 empat">' +
                '<input type="text"  maxLength="4" class="form-control" name="NO_URUT_PEMOHON" id="NO_URUT_PEMOHON" onfocus="this.value=(null)">' +
                '</div>' +
                '<div class="col-md-1 satu">' +
                '<input type="text"  maxLength="1" class="form-control" name="KD_JNS_OP_PEMOHON" id="KD_JNS_OP_PEMOHON" onfocus="this.value=(null)">' +
                '</div>' +
                '</div>';


              $(".form-control").keyup(function() {

                if (this.value.length == this.maxLength) {
                  var nextIndex = $('input:text').index(this) + 1;
                  $('input:text')[nextIndex].focus();
                }

                $('body').on('keypress', 'input, select, textarea', function(e) {
                  var self = $(this),
                    form = self.parents('form:eq(0)'),
                    focusable, next;
                  if (e.keyCode == 13) {
                    focusable = form.find('input,select,button,textarea').filter(':visible');
                    next = focusable.eq(focusable.index(this) + 1);
                    if (next.length) {
                      next.focus();
                    } else {
                      form.submit();
                    }
                    return false;

                  }
                });
              });
            }
          }



          function stopRKey(evt) {
            var evt = (evt) ? evt : ((event) ? event : null);
            var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);
            if ((evt.keyCode == 13) && (node.type == "text")) {
              return false;
            }
          }
          document.onkeypress = stopRKey;
        </script>

        <style type="text/css">
          .dua {
            width: 4%;
          }

          .tiga {
            width: 5%;
          }

          .empat {
            width: 6%;
          }

          .satu {
            width: 3%;
          }
        </style>