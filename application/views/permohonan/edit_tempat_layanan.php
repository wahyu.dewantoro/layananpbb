<section class="content-header">
  <h1>
    <?php echo $button ?>
    <div class="pull-right">

    </div>
  </h1>

</section>
<section class="content">
  <div class="row">
    <div class="col-md-6">
      <div class="box">
        <div class="box-body">
          <form class='form-horizontal' action="<?php echo $action; ?>" method="post" enctype="multipart/form-data">
            <div class="form-group">
              <label class="col-md-2 control-label">NO Layanan </label>
              <div class="col-md-8">
                <input type="text" class="form-control" name="nopel" id="nopel" placeholder="NO Layanan" value="<?php echo $nopel; ?>" readonly="" />
              </div>
            </div>
            <div class="form-group">
              <label class="col-md-2 control-label">Tempat Layanan </label>
              <div class="col-md-8">
                <select class="form-control" id="unit_kantor" name="unit_kantor" required>
                  <option value="">Pilih Tempat Layanan</option>
                  <option <?php if ($unit_kantor == 'kota') {
                            echo "selected";
                          } ?> value="kota">Kantor Pendopo</option>
                  <option <?php if ($unit_kantor == 'kabupaten') {
                            echo "selected";
                          } ?> value="kabupaten">Kantor Kepanjen</option>
                </select>
              </div>
            </div>

            <div class="form-group">
              <div class="col-md-8 col-md-offset-2">
                <button type="submit" class="btn btn-success"><i class="fa fa-save"></i> Simpan</button>
                <a href="<?php echo site_url('permohonan/listdokumen') ?>" class="btn btn-warning"><i class="fa fa-close"></i> Cancel</a>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</section>