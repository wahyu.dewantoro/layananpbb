<section class="content-header">
  <h1>
    Todo List dokumen
    <div class="pull-right btn-group">

    </div>
  </h1>
</section>
<section class="content">
  <div class="col-md-2">
    <div class="box box-solid">
      <div class="box-header with-border">
        <h3 class="box-title">Folders</h3>

      </div>
      <div class="box-body no-padding">
        <ul class="nav nav-pills nav-stacked">
          <li class="active"><a href="<?= base_url() . 'permohonan/listdokumen.html' ?>"><i class="fa fa-inbox"></i> Dokumen Masuk</a></li>
          <li><a href="#"><i class="fa fa-envelope-o"></i> Sent</a></li>
        </ul>
      </div>
      <!-- /.box-body -->
    </div>
  </div>
  <div class="col-md-10">
    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title">Detail Dokumen</h3>
      </div>
      <div class="box-body">
        <?php echo $this->session->flashdata('notif'); ?>
        <div class="row">
          <div class="col-md-4">
            <table class="table">
              <tbody>
                <tr>
                  <td>No Layanan</td>
                  <td width="1%">:</td>
                  <td><?php echo $rk->NO_LAYANAN ?></td>
                </tr>
                <tr>
                  <td>Tanggal Pelayanan</td>
                  <td>:</td>
                  <td><?php echo date_indo($rk->TGL_TERIMA_DOKUMEN_WP) ?></td>
                </tr>
                <tr>
                  <td>Tanggal Selesai (<small>Perkiraan</small>)</td>
                  <td>:</td>
                  <td><?php echo date_indo($rk->TGL_PERKIRAAN_SELESAI) ?></td>
                </tr>
                <tr>
                  <td>Jenis Pelayanan </td>
                  <td>:</td>
                  <td><?php if ($rk->STATUS_KOLEKTIF == '0') {
                        echo $rg[0]['LAYANAN'] . ' / Individual';
                      } else {
                        echo " / Kolektif";
                      }   ?></td>
                </tr>
                <tr>
                  <td>N.O.P</td>
                  <td>:</td>
                  <td><?php if ($rk->STATUS_KOLEKTIF == '0') {
                        echo $rg[0]['NOP'];
                      } else {
                        echo "-";
                      }   ?></td>
                </tr>


              </tbody>
            </table>
          </div>
          <div class="col-md-4">
            <table class="table">
              <tr>
                <td>Nama Pemohon</td>
                <td width="1%">:</td>
                <td><?php echo $rk->NAMA_PEMOHON ?></td>
              </tr>
              <tr>
                <td>Alamat Pemohon</td>
                <td>:</td>
                <td><?php echo $rk->ALAMAT_PEMOHON ?></td>
              </tr>
              <tr>
                <td>Letak Objek Pajak</td>
                <td>:</td>
                <td><?php if ($rk->STATUS_KOLEKTIF == '0') {
                      echo $rg[0]['JALAN_OP'];
                    } else {
                      echo "-";
                    }   ?></td>
              </tr>
              <tr>
                <td>Kelurahan</td>
                <td>:</td>
                <td><?php if ($rk->STATUS_KOLEKTIF == '0') {
                      echo $rg[0]['NM_KELURAHAN'];
                    } else {
                      echo "-";
                    }   ?></td>
              </tr>
              <tr>
                <td>Kecamatan</td>
                <td>:</td>
                <td><?php if ($rk->STATUS_KOLEKTIF == '0') {
                      echo $rg[0]['NM_KECAMATAN'];
                    } else {
                      echo "-";
                    }   ?></td>
              </tr>
              <tr>
                <td>Keterangan</td>
                <td>:</td>
                <td><?php echo $rk->KETERANGAN_PST ?></td>
              </tr>
            </table>
          </div>

          <div class="col-md-12">
            <p><b>Dokumen Terlampir :</b> </p>

            <div class="col-md-3">
              <ul>
                <li>Pengajuan Permohonan <?php if ($dok->L_PERMOHONAN                == '1') {
                                            echo "( <i class='fa fa-check'></i> )";
                                          } ?></li>
                <li>Surat Kuasa <?php if ($dok->L_SURAT_KUASA                        == '1') {
                                  echo "( <i class='fa fa-check'></i> )";
                                } ?></li>
                <li>FC KTP/SIM dan KK <?php if ($dok->L_KTP_WP                       == '1') {
                                        echo "( <i class='fa fa-check'></i> )";
                                      } ?></li>
                <li>FC Sertifikat / Kepemilikan <?php if ($dok->L_SERTIFIKAT_TANAH == '1') {
                                                  echo "( <i class='fa fa-check'></i> )";
                                                } ?></li>
                <li>Asli SPPT <?php if ($dok->L_SPPT                                 == '1') {
                                echo "( <i class='fa fa-check'></i> )";
                              } ?></li>
              </ul>
            </div>
            <div class="col-md-3">
              <ul>
                <li>FC IMB <?php if ($dok->L_IMB                             == '1') {
                              echo "( <i class='fa fa-check'></i> )";
                            } ?></li>
                <li>FC akte jual beli/hibah <?php if ($dok->L_AKTE_JUAL_BELI == '1') {
                                              echo "( <i class='fa fa-check'></i> )";
                                            } ?></li>
                <li>FC SK Pensiun / Veteran <?php if ($dok->L_SK_PENSIUN     == '1') {
                                              echo "( <i class='fa fa-check'></i> )";
                                            } ?></li>
                <li>FC SPPT / SSPD / Bukti lunas<?php if ($dok->L_SPPT_STTS  == '1') {
                                                  echo "( <i class='fa fa-check'></i> )";
                                                } ?></li>
                <li>Asli BBPD / Bukti Lunas<?php if ($dok->L_STTS            == '1') {
                                              echo "( <i class='fa fa-check'></i> )";
                                            } ?></li>
              </ul>
            </div>
            <div class="col-md-3">
              <ul>
                <li>FC SK Pengurangan<?php if ($dok->L_SK_PENGURANGAN == '1') {
                                        echo "( <i class='fa fa-check'></i> )";
                                      } ?></li>
                <li>FC SK Keberatan<?php if ($dok->L_SK_KEBERATAN     == '1') {
                                      echo "( <i class='fa fa-check'></i> )";
                                    } ?></li>
                <li>FC SSPD BPHTB<?php if ($dok->L_SKKP_PBB           == '1') {
                                    echo "( <i class='fa fa-check'></i> )";
                                  } ?></li>
                <li>SKTM <?php if ($dok->L_SPMKP_PBB                  == '1') {
                            echo "( <i class='fa fa-check'></i> )";
                          } ?></li>
                <li>Sket tanah<?php if ($dok->L_SKET_TANAH            == '1') {
                                echo "( <i class='fa fa-check'></i> )";
                              } ?></li>
              </ul>
            </div>
            <div class="col-md-3">
              <ul>
                <li>Sket Lurah <?php if ($dok->L_SKET_LURAH         == '1') {
                                  echo "( <i class='fa fa-check'></i> )";
                                } ?></li>
                <li>NPWP/NPWPD<?php if ($dok->L_NPWPD               == '1') {
                                echo "( <i class='fa fa-check'></i> )";
                              } ?></li>
                <li>Rincian Penghasil <?php if ($dok->L_PENGHASILAN == '1') {
                                        echo "( <i class='fa fa-check'></i> )";
                                      } ?></li>
                <li>SK Cagar budaya <?php if ($dok->L_CAGAR         == '1') {
                                      echo "( <i class='fa fa-check'></i> )";
                                    } ?></li>
                <li>Lain - lain <?php if ($dok->L_LAIN_LAIN         == '1') {
                                  echo "( <i class='fa fa-check'></i> )";
                                } ?></li>
              </ul>
            </div>
          </div>
          <div class="col-md-12">
            <?php if ($rk->STATUS_KOLEKTIF == '1') { ?>
              <P><b>Lampiran tanda terima pelayanan kolektif</b></P>
              <table class="table table-bordered">
                <thead>
                  <tr>
                    <th>No</th>
                    <th>NO Layanan</th>
                    <th>NOP</th>
                    <th>Nama WP</th>
                    <th>Tahun</th>
                    <th>Jenis Pelayanan</th>

                  </tr>
                </thead>
                <tbody>
                  <?php $urut = 0;
                    $no = 1;
                    foreach ($rg as $rg) { ?>
                    <tr>
                      <td align="center"><?php echo $no ?></td>
                      <td><?php echo $rg['NO_LAYANAN'] . '.' . sprintf('%03s', $urut) ?></td>
                      <td><?php echo $rg['NOP'] ?></td>
                      <td><?php echo $rg['NM_WP'] ?></td>
                      <td><?php echo $rg['THN_PELAYANAN'] ?></td>
                      <td><?php echo $rg['LAYANAN'] ?></td>

                    </tr>
                  <?php $no++;
                      $urut++;
                    } ?>
                </tbody>
              </table>

            <?php } ?>
          </div>



        </div>


        <?php if ($rk->STATUS_KOLEKTIF == '1') { ?>

          <!-- <P><b>Lampiran tanda terima pelayanan kolektif</b></P> -->
          <table class="table tble-bordered">
            <thead>

            </thead>

          </table>
        <?php } ?>
      </div>
    </div>
  </div>
</section>