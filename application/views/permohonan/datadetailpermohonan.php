<section class="content-header">
  <h1>
    Data Detail Permohonan

  </h1>
</section>
<section class="content">
  <div class="box">
    <div class="box-body">
      <form class="form-inline" method="post" action="<?php echo base_url() . 'permohonan/datadetailpermohonan.html' ?>">
        <div class="form-group">
          <input type="text" class="form-control tanggal" name="p_tanggal1" required value="<?php echo $this->session->userdata('p_tanggal1') ?>">
        </div>
        <div class="form-group">
          <input type="text" class="form-control tanggal" name="p_tanggal2" required value="<?php echo $this->session->userdata('p_tanggal2') ?>">
        </div>
        <button type="submit" class="btn btn-success"><i class="fa fa-search"></i> Cari</button>
      </form>
      <table class="table table-bordered table-striped" id="example2">
        <thead>
          <tr>
            <th width="5%">No</th>
            <th>No Layanan</th>
            <th>NOP</th>
            <th>Nama</th>
            <th>Layanan</th>
            <th>Terima</th>
            <th>Selesai</th>

          </tr>
        </thead>

      </table>
    </div>
  </div>
</section>
<script type="text/javascript">
  $(document).ready(function() {
    $.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings) {
      return {
        "iStart": oSettings._iDisplayStart,
        "iEnd": oSettings.fnDisplayEnd(),
        "iLength": oSettings._iDisplayLength,
        "iTotal": oSettings.fnRecordsTotal(),
        "iFilteredTotal": oSettings.fnRecordsDisplay(),
        "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
        "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
      };
    };

    var t = $("#example2").dataTable({
      initComplete: function() {
        var api = this.api();
        $('#mytable_filter input')
          .off('.DT')
          .on('keyup.DT', function(e) {
            if (e.keyCode == 13) {
              api.search(this.value).draw();
            }
          });
      },



      'oLanguage': {
        "sProcessing": "Sedang memproses...",
        "sLengthMenu": "Tampilkan _MENU_ entri",
        "sZeroRecords": "Tidak ditemukan data yang sesuai",
        "sInfo": "Menampilkan _START_ sampai _END_ dari _TOTAL_ entri",
        "sInfoEmpty": "Menampilkan 0 sampai 0 dari 0 entri",
        "sInfoFiltered": "(disaring dari _MAX_ entri keseluruhan)",
        "sInfoPostFix": "",
        "sSearch": "Cari:",
        "sUrl": "",
        "oPaginate": {
          "sFirst": "Pertama",
          "sPrevious": "Sebelumnya",
          "sNext": "Selanjutnya",
          "sLast": "Terakhir"
        }
      },
      processing: true,
      serverSide: true,
      ajax: {
        "url": "<?php echo base_url() ?>permohonan/jsondatadetailpermohonan",
        "type": "POST"
      },
      columns: [{
          "data": "ID",
          "orderable": false
        }, {
          "data": "NO_LAYANAN"
        }, {
          "data": "NOP"
        }, {
          "data": "NAMA"
        }, {
          "data": "LAYANAN"
        }, {
          "data": "TGL_TERIMA"
        }, {
          "data": "TGLSELESAI"
        },

      ],
      order: [],
      rowCallback: function(row, data, iDisplayIndex) {
        var info = this.fnPagingInfo();
        var page = info.iPage;
        var length = info.iLength;
        var index = page * length + (iDisplayIndex + 1);
        $('td:eq(0)', row).html(index);
      }
    });
  });
</script>