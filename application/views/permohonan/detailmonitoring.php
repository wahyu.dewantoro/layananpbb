<div class="row">
  <div class="col-md-4">

    <section class="content">
      <div class="box">
        <div class='box-header with-border'>
          <h3 class='box-title'> <i class="fa fa-globe"></i> Ojek Pajak</h3>
          <div class="pull-right">
            <div class="btn-group">
              <?php
              if ($rk->NM_PENGAJUAN != null && $rk->NM_PENGAJUAN != 'MUTASI' && $this->session->userdata('pbb_kg') == '34') {

                // echo anchor('permohonan/cetaksk?var='.urlencode($rk->NM_PENGAJUAN).'&id='.$ID,'<i class="fa fa-file-text"></i> File SK','class="btn btn-xs btn-primary" target="_blank"');
              }

              if ($button == '1') { ?>
                <a href="#" data-toggle="modal" data-target="#myModal" class="btn btn-success btn-xs"><i class="fa fa-slideshare"></i> Disposisi</a>
              <?php } ?>
            </div>
          </div>
        </div>
        <div class="box-body">
          <table class="table" style="font-size:12px">
            <tbody>
              <tr>
                <td>No Layanan</td>
                <td>:</td>
                <td><?php echo $rk->NO_LAYANAN ?></td>
              </tr>
              <tr>
                <td>NOP</td>
                <td>:</td>
                <td><?php echo $rk->NOP ?></td>
              </tr>
              <tr>
                <td>Nama WP</td>
                <td>:</td>
                <td><?php echo $this->Mpermohonan->getNamaWP($rk->NOP) ?></td>
              </tr>

              <tr>
                <td>Layanan</td>
                <td>:</td>
                <td><?php echo $rk->LAYANAN ?></td>
              </tr>
              <tr>
                <td>Tahun Pajak </td>
                <td>:</td>
                <td><?php echo $rk->TAHUN ?></td>
              </tr>
              <tr>
                <td>Catatan </td>
                <td>:</td>
                <td><?php echo $rk->CATATAN_PST ?></td>
              </tr>
            </tbody>
          </table>
        </div>
      </div>
    </section>
  </div>
  <div class="col-md-8">
    <section class="content">
      <div class="box">
        <div class='box-header with-border'>
          <h3 class='box-title'><i class="fa fa-history"></i> Riwayat Dokumen</h3>
          <div class="pull-right">
            <?php echo anchor('permohonan/listdokumen', '<i class="fa fa-reply"></i> kembali', 'class="btn btn-primary btn-xs"'); ?>
          </div>
        </div>
        <div class="box-body">
          <table class="table table-bordered table-striped">
            <thead>
              <tr>
                <th>No</th>
                <th>Tanggal</th>
                <th>Posisi</th>
                <th>Pegawai</th>
                <th>Status</th>
                <th>Keterangan</th>
                <!-- <th>Aksi</th> -->
              </tr>
            </thead>
            <tbody>
              <?php if (count($rn) > 0) {
                $no = 1;
                foreach ($rn as $rn) {   ?>
                  <tr>
                    <td align="center"><?php echo $no++; ?></td>
                    <td><?php if ($rn->AWAL == $rn->AKHIR) {
                              echo $rn->AWAL;
                            } else {
                              if ($rn->AKHIR != '') {
                                echo $rn->AWAL . ' - ' . $rn->AKHIR;
                              } else {
                                echo $rn->AWAL;
                              }
                            } ?></td>
                    <td><?php echo $rn->POSISI ?></td>
                    <td><?php echo $rn->NM_PEGAWAI ?></td>
                    <td><?php echo $rn->STATUS_BERKAS ?></td>
                    <td><?php echo $rn->KETERANGAN ?></td>
                    <!-- <td></td> -->
                  </tr>
                <?php  }
                } else { ?>
                <tr>
                  <td colspan="5">Tidak ada data</td>
                </tr>

              <?php  } ?>
            </tbody>
          </table>
        </div>
      </div>
    </section>
  </div>
</div>


<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <form method="post" action="<?php echo base_url() . "permohonan/proseskirimdokumen" ?>">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
          <h4 class="modal-title" id="myModalLabel">Disposisi Berkas</h4>
        </div>
        <div class="modal-body">
          <?php $exp = explode('.', $rk->NO_LAYANAN);
          $asp = explode('.', $rk->NOP);
          ?>
          <input type='hidden' name='KD_KANWIL' value='01'>
          <input type='hidden' name='KD_KANTOR' value='01'>
          <input type='hidden' name='THN_PELAYANAN' value='<?php echo $exp[0] ?>'>
          <input type='hidden' name='BUNDEL_PELAYANAN' value='<?php echo $exp[1] ?>'>
          <input type='hidden' name='NO_URUT_PELAYANAN' value='<?php echo $exp[2] ?>'>
          <input type='hidden' name='KD_PROPINSI_RIWAYAT' value='<?php echo $asp[0] ?>'>
          <input type='hidden' name='KD_DATI2_RIWAYAT' value='<?php echo $asp[1] ?>'>
          <input type='hidden' name='KD_KECAMATAN_RIWAYAT' value='<?php echo $asp[2] ?>'>
          <input type='hidden' name='KD_KELURAHAN_RIWAYAT' value='<?php echo $asp[3] ?>'>
          <input type='hidden' name='KD_BLOK_RIWAYAT' value='<?php echo $asp[4] ?>'>
          <input type='hidden' name='NO_URUT_RIWAYAT' value='<?php echo $asp[5] ?>'>
          <input type='hidden' name='KD_JNS_OP_RIWAYAT' value='<?php echo $asp[6] ?>'>
          <input type='hidden' name='NIP' value='<?php echo $this->session->userdata('nip') ?>'>
          <input type='hidden' name='TANGGAL_AWAL' value='<?php echo date('Y-m-d H:i:s'); ?>'>
          <input type='hidden' name='TANGGAL_AKHIR' value='<?php echo date('Y-m-d H:i:s'); ?>'>
          <div class="form-group">
            <label>Unit Tujuan</label>
            <select class="form-control" required name="KODE_GROUP">
              <option value="">Pilih</option>
              <?php foreach ($unit as $unit) { ?>
                <option value="<?php echo $unit->KODE_GROUP ?>"><?php echo $unit->NAMA_GROUP ?></option>
              <?php } ?>
            </select>
          </div>
          <div class="form-group">
            <label>Status</label>
            <select class="form-control" required name="STATUS_BERKAS">
              <!-- <option value="">Pilih</option> -->
              <?php $hdd = array('Proses', 'Disetujui', 'Ditolak', 'Selesai', 'Diambil');
              foreach ($hdd as $hdd) { ?>
                <option value="<?php echo $hdd ?>"><?php echo $hdd ?></option>
              <?php } ?>
            </select>
          </div>
          <div class="form-group">
            <label>Catatan</label>
            <textarea name="KETERANGAN" class="form-control"></textarea>
          </div>
        </div>
        <div class="modal-footer">
          <div class="btn-group">
            <button type="Submit" class="btn btn-success"><i class="fa fa-send"></i> Kirim</button>
            <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-close"></i> Cancel</button>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>

<script type="text/javascript">
  // register jQuery extension
  jQuery.extend(jQuery.expr[':'], {
    focusable: function(el, index, selector) {
      return $(el).is('a, button, :input, [tabindex]');
    }
  });

  $(document).on('keypress', 'input,select', function(e) {
    if (e.which == 13) {
      e.preventDefault();
      // Get all focusable elements on the page
      var $canfocus = $(':focusable');
      var index = $canfocus.index(this) + 1;
      if (index >= $canfocus.length) index = 0;
      $canfocus.eq(index).focus();
    }
  });
</script>
<style>
  .row {
    margin-top: 30px;
  }
</style>