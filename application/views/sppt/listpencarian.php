<div class="box">
    <div class="box-header with-border">
        <h3 class="box-title">Data SPPT</h3>
        <div class="pull-right">


        </div>
    </div>
    <div class="box-body">

        <table class="table table-bordered table-striped table-responsive" id="example2">
            <thead>
                <tr>
                    <th>NO</th>
                    <th>Tahun</th>
                    <th>NOP</th>
                    <th>nama</th>
                    <th>Alama</th>
                    <th>Luas Bumi</th>
                    <th>Luas Bangunan</th>
                    <th>NJOP Bumi</th>
                    <th>NJOP Bangunan</th>
                    <th>PBB</th>
                </tr>
            </thead>

        </table>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function() {
        $.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings) {
            return {
                "iStart": oSettings._iDisplayStart,
                "iEnd": oSettings.fnDisplayEnd(),
                "iLength": oSettings._iDisplayLength,
                "iTotal": oSettings.fnRecordsTotal(),
                "iFilteredTotal": oSettings.fnRecordsDisplay(),
                "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
            };
        };

        var t = $("#example2").dataTable({
            initComplete: function() {
                var api = this.api();
                $('#mytable_filter input')
                    .off('.DT')
                    .on('keyup.DT', function(e) {
                        if (e.keyCode == 13) {
                            api.search(this.value).draw();
                        }
                    });
            },



            'oLanguage': {
                "sProcessing": "Sedang memproses...",
                "sLengthMenu": "Tampilkan _MENU_ entri",
                "sZeroRecords": "Tidak ditemukan data yang sesuai",
                "sInfo": "Menampilkan _START_ sampai _END_ dari _TOTAL_ entri",
                "sInfoEmpty": "Menampilkan 0 sampai 0 dari 0 entri",
                "sInfoFiltered": "(disaring dari _MAX_ entri keseluruhan)",
                "sInfoPostFix": "",
                "sSearch": "Cari:",
                "sUrl": "",
                "oPaginate": {
                    "sFirst": "Pertama",
                    "sPrevious": "Sebelumnya",
                    "sNext": "Selanjutnya",
                    "sLast": "Terakhir"
                }
            },
            processing: true,
            serverSide: true,
            ajax: {
                "url": "<?php echo base_url() ?>sppt/jsonspptpencarian",
                "type": "POST",
                "data": {
                    'KODE_KEL': '<?= $KODE_KEL ?>',
                    'KODE_KEC': '<?= $KODE_KEC ?>',
                    'NAMA': '<?= $nama ?>'
                }
            },
            columns: [

                // NOP,NM_WP_SPPT,ALAMAT,NJOP_BUMI_SPPT,NJOP_BNG_SPPT,PPB_YG_HARUS_DIBAYAR_SPPT,STATUS_PEMBAYARAN,_SPPT,TGL_BAYAR
                {
                    "data": "NOP",
                    "orderable": false,
                    "className": "text-center",
                },
                {
                    "data": "THN_PAJAK_SPPT"
                },
                {
                    "data": "NOP"
                },
                {
                    "data": "NM_WP_SPPT"
                },
                {
                    "data": "ALAMAT"
                },
                {
                    "data": "LUAS_BUMI_SPPT"
                },
                {
                    "data": "LUAS_BNG_SPPT"
                },
                {
                    "data": "NJOP_BUMI_SPPT"
                },
                {
                    "data": "NJOP_BNG_SPPT"
                },
                {
                    "data": "PBB_YG_HARUS_DIBAYAR_SPPT"
                },
                /*{"data": "NOP"},
                {"data": "NM_WP_SPPT"},
                {"data": "ALAMAT"},
                {"data": "NJOP_BUMI_SPPT"},
                {"data": "NJOP_BNG_SPPT"},*/
                /*{"data": "PPB_YG_HARUS_DIBAYAR_SPPT"},
                {"data": "STATUS_PEMBAYARAN_SPPT"},
                {"data": "TGL_BAYAR"},*/
            ],
            order: [
                [0, 'desc']
            ],
            rowCallback: function(row, data, iDisplayIndex) {
                var info = this.fnPagingInfo();
                var page = info.iPage;
                var length = info.iLength;
                var index = page * length + (iDisplayIndex + 1);
                $('td:eq(0)', row).html(index);
            }
        });
    });
</script>