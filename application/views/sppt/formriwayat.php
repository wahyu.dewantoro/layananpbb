<section class="content-header">
  <h1>
    Riwayat Pembayaran SPPT
  </h1>
</section>
<section class="content">
  <div class="row">
    <div class="col-md-4">
      <div class="box">
        <div class="box-body">
          <div class="row">
            <div class="col-md-12">
              <form class="form-inline" role="form">
                <div class="form-group">
                  <select class="form-control" id="var">
                    <option value="nop">NOP</option>
                    <option value="scan">Scan</option>

                  </select>
                </div>
                <div class="form-group">
                  <input type="text" onkeyup="formatnop(this)" autofocus="true" class="form-control" id="NOP" placeholder="" autofocus="true" required>
                </div>

              </form>

              <div class="input-group">
                <div class="input-group-btn">

                </div>

              </div>
            </div>

            <div class="col-md-2">
              <!-- <p class="pull-right"><strong>NOP</strong></p> -->
            </div>
            <div class="col-md-8">

            </div>
            <div class="col-md-2">
              <!-- <button id="cari" type="button" class='btn btn-sm btn-primary'><i class="fa fa-search"></i> Cari</button> -->
            </div>
          </div>

        </div>
      </div>
    </div>
    <div class="col-md-8">
      <div id="result"></div>
    </div>
  </div>

</section>
<script type="text/javascript">
  $(document).ready(function() {

    var input = document.getElementById("NOP");
    input.addEventListener("keyup", function(event) {
      event.preventDefault();
      if (event.keyCode === 13) {
        document.getElementById("cari").click();
      }
    });



    $("#NOP").keyup(function() {

      var sc = $("#var").val();
      var nop = $("#NOP").val();

      cekscan(sc, nop);
    });

    $("#NOP").keydown(function() {

      var sc = $("#var").val();
      var nop = $("#NOP").val();

      cekscan(sc, nop);
    });

    function cekscan(sc, nop) {
      $.ajax({
        type: "POST",
        url: "<?= base_url() . 'sppt/ceknop' ?>",
        data: {
          'nop': nop,
          'var': sc
        },
        success: function(data) {
          $('#result').html(data);
        }
      });
    }




  });

  function checkLengthnop(el) {
    b = el.value.replace(/[^\d]/g, "");

    if (b.length != 18) {
      // alert("Panjang nik 16 digit.")
      $("#VAL_NOP").html("<span style='color:red'>Panjang NOP harus  18 angka.</span>");
    } else {
      $("#VAL_NOP").html("");
    }
  }

  function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57)) {
      return false;
    }
    return true;
  }

  function formatnop(objek) {

    a = objek.value;
    b = a.replace(/[^\d]/g, "");
    c = "";
    panjang = b.length;

    if (panjang <= 2) {
      // 35 -> 0,2
      c = b;
    } else if (panjang > 2 && panjang <= 4) {
      // 07. -> 2,2
      c = b.substr(0, 2) + '.' + b.substr(2, 2);
    } else if (panjang > 4 && panjang <= 7) {
      // 123 -> 4,3
      c = b.substr(0, 2) + '.' + b.substr(2, 2) + '.' + b.substr(4, 3);
    } else if (panjang > 7 && panjang <= 10) {
      // .123. ->
      c = b.substr(0, 2) + '.' + b.substr(2, 2) + '.' + b.substr(4, 3) + '.' + b.substr(7, 3);
    } else if (panjang > 10 && panjang <= 13) {
      // 123.
      c = b.substr(0, 2) + '.' + b.substr(2, 2) + '.' + b.substr(4, 3) + '.' + b.substr(7, 3) + '.' + b.substr(10, 3);
    } else if (panjang > 13 && panjang <= 17) {
      // 1234
      c = b.substr(0, 2) + '.' + b.substr(2, 2) + '.' + b.substr(4, 3) + '.' + b.substr(7, 3) + '.' + b.substr(10, 3) + '.' + b.substr(13, 4);
    } else {
      // .0
      c = b.substr(0, 2) + '.' + b.substr(2, 2) + '.' + b.substr(4, 3) + '.' + b.substr(7, 3) + '.' + b.substr(10, 3) + '.' + b.substr(13, 4) + '.' + b.substr(17, 1);
      // alert(panjang);
    }
    objek.value = c;
  }
</script>