<section class="content-header">
	<h1>
		Pencarian SPPT
	</h1>
</section>
<section class="content">
	<div class="row">
		<div class="col-md-3">
			<div class="box">
				<div class="box-header with-border">
					<h3 class="box-title">Form Pencarian</h3>
				</div>
				<div class="box-body">

					<div class="form-group">
						<label for="exampleInputEmail1"><strong>Kecamatan</strong> <sup>*</sup></label>
						<select name="KODE_KEC" id="KODE_KEC" class="form-control" required>
							<option value="">pilih</option>
							<?php foreach ($kec as $kec) { ?>
								<option value="<?= $kec->KD_KECAMATAN ?>"><?= $kec->KD_KECAMATAN . ' ' . $kec->NM_KECAMATAN ?></option>
							<?php } ?>
						</select>
					</div>
					<div class="form-group">
						<label><strong>Kelurahan/Desa</strong> <sup>*</sup></label>
						<select class="form-control" name="KODE_KEL" id="KODE_KEL" required>
							<option value="">Pilih</option>
						</select>
					</div>
					<div class="form-group">
						<label><strong>Nama</strong> <sup>*</sup></label>
						<input type="text" class="form-control" name="NAMA_WP" id="NAMA_WP" required>
					</div>
					<div class="form-group">
						<div class="text-right">
							<button id="cari" type="button" class='btn btn-sm btn-primary'><i class="fa fa-search"></i> Cari</button>
						</div>
					</div>

				</div>
			</div>
		</div>
		<div class="col-md-9">
			<div id="result"></div>
		</div>
	</div>

</section>
<script type="text/javascript">
	$('#KODE_KEC').change(function() {
		kode_kec = $('#KODE_KEC').val();

		$.ajax({
			url: "<?= base_url() . 'sppt/getKelurahan' ?>",

			type: 'POST',
			data: {
				kode_kec: kode_kec
			},
			success: function(hasil) {
				$('#KODE_KEL').html(hasil);
			}
		});
	});

	$("#cari").click(function() {

		var kode_kec = $("#KODE_KEC").val();
		var kode_kel = $("#KODE_KEL").val();
		var nama = $("#NAMA_WP").val();
		// alert(nama);
		if (kode_kec == '' || kode_kel == '' || nama == null) {

			alert('kecamatan, kelurahan dan nama harus di isi.');
		} else {
			$.ajax({
				type: "POST",
				url: "<?= base_url() . 'sppt/listpencarian' ?>",
				data: {
					'KODE_KEC': kode_kec,
					'KODE_KEL': kode_kel,
					'nama': nama
				},
				success: function(data) {
					$('#result').html(data);
				}
			});
		}
	});


	var input = document.getElementById("NAMA_WP");
	input.addEventListener("keyup", function(event) {
		event.preventDefault();
		if (event.keyCode === 13) {
			document.getElementById("cari").click();
		}
	});
</script>