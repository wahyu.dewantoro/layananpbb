<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>Layanan PBB</title>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <!-- Bootstrap 3.3.2 -->
    <link href="<?php echo base_url().'adminlte/'?>bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
     <link href="<?php echo base_url().'adminlte/'?>bootstrap/css/bootstrap-multiselect.css" rel="stylesheet" type="text/css" />



    <link href="<?php echo base_url().'adminlte/'?>fontawesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="<?php echo base_url().'adminlte/'?>bower_components/Ionicons/css/ionicons.min.css">
     

     <link rel="stylesheet" href="<?php echo base_url().'adminlte/'?>bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">

    <link href="<?php echo base_url().'adminlte/'?>plugins/chosen/chosen.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url().'adminlte/'?>dist/css/AdminLTE.min.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo base_url().'adminlte/'?>dist/css/skins/_all-skins.min.css" rel="stylesheet" type="text/css" />
      <link href="<?php echo base_url().'adminlte/'?>plugins/iCheck/all.css" rel="stylesheet" type="text/css" />
      <link href="<?php echo base_url().'adminlte/'?>plugins/datepicker/datepicker3.css" rel="stylesheet" type="text/css" />
      <link href="<?php echo base_url('assets/jquery-ui.css')?>" rel="stylesheet" type="text/css" />
      
        <script src="<?php echo base_url('assets/js/jquery-1.12.4.js')?>"></script>
        <script src="<?php echo base_url('assets/js/jquery-ui.js')?>"></script>


    <!-- Bootstrap 3.3.2 JS -->
    <script src="<?php echo base_url().'adminlte/'?>bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    


    <style type="text/css">
             .row{margin-top: 30px; } 
  </style>

  </head>
  <body class="hold-transition skin-blue layout-top-nav fixed">
    <!-- Site wrapper -->
    <div class="wrapper">
      
     

      <!-- =============================================== -->

      <!-- Left side column. contains the sidebar -->

      <!-- =============================================== -->

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <div class="container">
        <?php echo $contents; ?>      
        </div>
        <!-- Content Header (Page header) -->
        

      </div><!-- /.content-wrapper -->
 
    </div><!-- ./wrapper -->

     

  </body>
</html>

 <script type="text/javascript">
    function autoRefreshPage()
    {
        window.location = window.location.href;
    }
    setInterval('autoRefreshPage()', 10000);
</script>