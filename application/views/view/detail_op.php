
<section class="content-header">
          <h1>
            <?php /*echo $button*/ ?>
            
          </h1>
          
        </section>
        <section class="content">
          <div class="row">
            <form class='form-horizontal' action="<?php echo base_url('view/detail_op');?>" method="post" enctype="multipart/form-data">
              <div class="col-md-8">
                  <div class="form-group">
                      <label class="col-md-3 control-label">Kode SPPT / Barcode </label>
                          <div class="col-md-4"> 
                              <input type="text"   class="form-control" name="" id="" placeholder="Kode SPPT / Barcode" value=""/>                                     
                          </div>    
                  </div>
                  <div class="form-group">
                      <label class="col-md-3  control-label">NOP </label>
                          <div class="col-md-4">
                              <table width='100%'>
                                  <tr>
                                      <td width='11%'><input type="text" maxlength="2" class="form-control" name="KD_PROPINSI" value="<?php echo $_SESSION['KD_PROPINSI']?>" onkeyup="myFunction()"></td>
                                      <td width='11%'><input type="text" maxlength="2" class="form-control" name="KD_DATI2" value="<?php echo $_SESSION['KD_DATI2']?>"></td>
                                      <td width='14%'><input type="text" maxlength="3" class="form-control" name="KD_KECAMATAN" value="<?php echo $_SESSION['KD_KECAMATAN']?>"></td>
                                      <td width='14%'><input type="text" maxlength="3" class="form-control" name="KD_KELURAHAN" value="<?php echo $_SESSION['KD_KELURAHAN']?>"></td>
                                      <td width='14%'><input type="text" maxlength="3" class="form-control" name="KD_BLOK" value="<?php echo $_SESSION['KD_BLOK']?>"></td>
                                      <td width='15%'><input type="text" maxlength="4" class="form-control" name="NO_URUT" value="<?php echo $_SESSION['NO_URUT']?>"></td>
                                      <td width='10%'><input type="text" maxlength="1" class="form-control" name="KD_JNS_OP" value="<?php echo $_SESSION['KD_JNS_OP']?>"></td>
                                  </tr>
                              </table>
                          </div>
                          <div class="col-md-1">
                            <button type="submit" class="btn btn-success" value='cari'><i class="fa fa-search"></i> Lihat</button>
                          </div>
                  </div>
              </div>
            </form>
          </div><br>
          <div class="nav-tabs-custom">
            <ul class="nav nav-tabs">
              <li class="active"><a href="#tab_1" data-toggle="tab">Objek Pajak</a></li>
              <li ><a href="#tab_2" data-toggle="tab">Subjek Pajak</a></li>
              <li ><a href="#tab_3" data-toggle="tab">Data Bangunan</a></li>
              <li ><a href="#tab_4" data-toggle="tab">Gambar Objek</a></li>
              <li ><a href="#tab_5" data-toggle="tab">SPPT</a></li>
              <li ><a href="#tab_6" data-toggle="tab">Detail SPPT</a></li>
            </ul>
            <div class="tab-content">
              <div class="tab-pane active" id="tab_1">
                  <form class='form-horizontal' action="" method="post" enctype="multipart/form-data">
                      <div class="box box-success">
                        <h4 class='box-title'> <i class="fa fa-globe"></i> Data Objek Pajak</h4>
                        <div class="box-body">
                         <div id="idn">
                           <div class="row"><?php error_reporting(E_ALL^(E_NOTICE|E_WARNING));?>
                              <div class="col-md-6">                                 
                                  <table class="table"  style="font-size:13.4px" width='100%'>  
                                      <tbody>
                                        <tr>
                                          <td width='35%'>NOP</td>
                                          <td width='1%'>:</td>
                                          <td><?php echo $objek_pajak->NOP?></td>
                                        </tr>
                                        <tr>
                                          <td>Kecamatan</td>
                                          <td>:</td>
                                          <td><?php echo $objek_pajak->KECAMATAN?></td>
                                        </tr>
                                        <tr>
                                          <td>Kelurahan</td>
                                          <td>:</td>
                                          <td><?php echo $objek_pajak->KELURAHAN?></td>
                                        </tr>
                                        <tr>
                                          <td>Alamat</td>
                                          <td>:</td>
                                          <td><?php echo $objek_pajak->JALAN_OP?></td>
                                        </tr>
                                        <tr>
                                          <td>No. Persil  </td>
                                          <td>:</td>
                                          <td><?php echo $objek_pajak->NO_PERSIL?></td>
                                        </tr>
                                        <tr>
                                          <td>RW / RT</td>
                                          <td>:</td>
                                          <td><?php echo $objek_pajak->RW_OP.' / '.$objek_pajak->RT_OP ?> </td>
                                        </tr>
                                        <tr>
                                          <td>Cabang</td>
                                          <td>:</td>
                                          <td><?php if ($objek_pajak->KD_STATUS_CABANG=='1'){echo "IYA";}
                                                    else if ($objek_pajak->KD_STATUS_CABANG=='0') {echo "TIDAK";}  else { echo "";}?></td>
                                        </tr>
                                      </tbody>
                                  </table> 
                              </div>
                              <div class="col-md-6"> 
                                  <table class="table"  style="font-size:13.4px" width='100%'>  
                                      <tbody>
                                        <tr>
                                          <td width='35%'>Status WP</td>
                                          <td width='1%'>:</td>
                                          <td><?php 
                                              if ($objek_pajak->KD_STATUS_WP=='1') {
                                                  echo $objek_pajak->KD_STATUS_WP.' - PEMILIK';}
                                              else if ($objek_pajak->KD_STATUS_WP=='2') { 
                                                  echo $objek_pajak->KD_STATUS_WP.' - PENYEWA';} 
                                              else if ($objek_pajak->KD_STATUS_WP=='3') {
                                                  echo $objek_pajak->KD_STATUS_WP.' - PENGELOLA';}
                                              else if ($objek_pajak->KD_STATUS_WP=='4') {
                                                  echo $objek_pajak->KD_STATUS_WP.' - PEMAKAI';}
                                              else if ($objek_pajak->KD_STATUS_WP=='5') {
                                                  echo $objek_pajak->KD_STATUS_WP.' - SENGKETA';}
                                              else { }?></td>
                                        </tr>
                                        <tr>
                                          <td>Luas Bumi</td>
                                          <td>:</td>
                                          <td align='right'><?php echo $objek_pajak->TOTAL_LUAS_BUMI?> M<sup>2</sup></td>
                                        </tr>
                                        <tr>
                                          <td>Luas Bangunan</td>
                                          <td>:</td>
                                          <td align='right'><?php echo $objek_pajak->TOTAL_LUAS_BNG?> M<sup>2</sup></td>
                                        </tr>
                                        
                                        <tr>
                                          <td>NJOP Bumi</td>
                                          <td>:</td>
                                          <td align='right'>Rp. <?php echo number_format($objek_pajak->NJOP_BUMI,'0',',','.')?></td>
                                        </tr>
                                        <tr>
                                          <td>NJOP Bangunan</td>
                                          <td>:</td>
                                          <td  align='right'>Rp. <?php echo number_format($objek_pajak->NJOP_BNG,'0',',','.')?></td>
                                        </tr>
                                        <tr>
                                          <td>ZNT</td>
                                          <td>:</td>
                                          <td><?php?></td>
                                        </tr>
                                        <tr>
                                          <td>Jenis Penggunaan Tanah</td>
                                          <td>:</td>
                                          <td><?php if ($objek_pajak->STATUS_PETA_OP=='1') {echo $objek_pajak->STATUS_PETA_OP.' - TANAH + BANGUNAN';} 
                                                    else if ($objek_pajak->STATUS_PETA_OP=='0') {echo $objek_pajak->STATUS_PETA_OP.' - TANAH';} else {echo "";}?></td>
                                        </tr>
                                      </tbody>
                                  </table>                                  
                              </div>
                           </div>
                           <div class="col-md-12" style="background: #9fc5e8">
                              <b>Informasi Perekaman Data</b>
                           </div>
                           <div class="row">
                              <div class="col-md-6">                                 
                                  <table class="table"  style="font-size:13.4px" width='100%'>  
                                      <tbody>
                                        <tr>
                                          <td width='35%'>Jenis Transaksi</td>
                                          <td width='1%'>:</td>
                                          <td><?php if ($objek_pajak->JNS_TRANSAKSI_OP==1) {echo $objek_pajak->JNS_TRANSAKSI_OP.' - Perekaman Data';} else { $objek_pajak->JNS_TRANSAKSI_OP.' - Pemut'; }?></td>
                                        </tr>
                                        <tr>
                                          <td>Tanggal Pendataan</td>
                                          <td>:</td>
                                          <td><?php echo $objek_pajak->TGL_PEREKAMAN?></td>
                                        </tr>
                                        <tr>
                                          <td>Tanggal Penelitian</td>
                                          <td>:</td>
                                          <td><?php echo $objek_pajak->TGL_PEMERIKSAAN?></td>
                                        </tr>
                                        <tr>
                                          <td>Nomor Formulir</td>
                                          <td>:</td>
                                          <td><?php echo $objek_pajak->NO_FORMULIR_SPOP?></td>
                                        </tr>
                                      </tbody>
                                  </table> 
                              </div>
                              <div class="col-md-6"> 
                                  <table class="table"  style="font-size:13.4px" width='100%'>  
                                      <tbody>
                                        <tr>
                                          <td width='35%'>NIP Pendata</td>
                                          <td width='1%'>:</td>
                                          <td><?php echo $objek_pajak->NIP_PEREKAM_OP;?></td>
                                        </tr>
                                        <tr>
                                          <td>NIP Peneliti</td>
                                          <td>:</td>
                                          <td><?php echo $objek_pajak->NIP_PEMERIKSA_OP;?></td>
                                        </tr>
                                      </tbody>
                                  </table>                                  
                              </div>
                           </div>
                         </div>                                                                      
                        </div>  <!-- body -->
                      </div>
                  </form>
              </div>
              <!-- /.tab-pane -->
              <div class="tab-pane" id="tab_2">
                  <!-- form tab 2 -->
                  <form class='form-horizontal' action="" method="post" enctype="multipart/form-data">
                      <div class="box box-success">
                        <h4 class='box-title'> <i class="fa fa-globe"></i> Data Subjek Pajak</h4>
                        <div class="box-body">
                         <div id="idn">
                           <div class="row">
                              <div class="col-md-6">                                 
                                  <table class="table"  style="font-size:13.4px" width='100%'>  
                                      <tbody>
                                        <tr>
                                          <td width='35%'>Nama </td>
                                          <td width='1%'>:</td>
                                          <td><?php echo $subjek_pajak->NM_WP;?></td>
                                        </tr>
                                        <tr>
                                          <td>Alamat</td>
                                          <td>:</td>
                                          <td><?php echo $subjek_pajak->JALAN_WP;?></td>
                                        </tr>
                                        <tr>
                                          <td>RW / RT</td>
                                          <td>:</td>
                                          <td><?php echo $subjek_pajak->RW_WP.' / '.$subjek_pajak->RT_WP;?></td>
                                        </tr>
                                        <tr>
                                          <td>Kelurahan</td>
                                          <td>:</td>
                                          <td><?php echo $subjek_pajak->KELURAHAN_WP;?></td>
                                        </tr>
                                        <tr>
                                          <td>Kota</td>
                                          <td>:</td>
                                          <td><?php echo $subjek_pajak->KOTA_WP;?></td>
                                        </tr>
                                      </tbody>
                                  </table> 
                              </div>
                              <div class="col-md-6"> 
                                  <table class="table"  style="font-size:13.4px" width='100%'>  
                                      <tbody>
                                        <tr>
                                          <td width='35%'>Kode Pos</td>
                                          <td width='1%'>:</td>
                                          <td><?php echo $subjek_pajak->KD_POS_WP;?></td>
                                        </tr>
                                        <tr>
                                          <td>No. Telp</td>
                                          <td>:</td>
                                          <td><?php echo $subjek_pajak->TELP_WP;?></td>
                                        </tr>
                                        <tr>
                                          <td>NPWP</td>
                                          <td>:</td>
                                          <td><?php echo $subjek_pajak->NPWP;?></td>
                                        </tr>                                        
                                        <tr>
                                          <td>Pekerjaan</td>
                                          <td>:</td>
                                          <td><?php if ($subjek_pajak->STATUS_PEKERJAAN_WP=='1') {
                                                        echo $subjek_pajak->STATUS_PEKERJAAN_WP.' - PNS';
                                                    } else if ($subjek_pajak->STATUS_PEKERJAAN_WP=='2') {
                                                        echo $subjek_pajak->STATUS_PEKERJAAN_WP.' - ABRI';
                                                    } else if ($subjek_pajak->STATUS_PEKERJAAN_WP=='3') {
                                                        echo $subjek_pajak->STATUS_PEKERJAAN_WP.' - PENSIUNAN';
                                                    } else if ($subjek_pajak->STATUS_PEKERJAAN_WP=='4') {
                                                        echo $subjek_pajak->STATUS_PEKERJAAN_WP.' - BADAN';
                                                    } else if ($subjek_pajak->STATUS_PEKERJAAN_WP=='5') {
                                                        echo $subjek_pajak->STATUS_PEKERJAAN_WP.' - LAINNYA';
                                                    } else if ($subjek_pajak->STATUS_PEKERJAAN_WP=='0') {
                                                        echo $subjek_pajak->STATUS_PEKERJAAN_WP.' - LAINNYA';
                                                    } else { }?></td>
                                        </tr>
                                      </tbody>
                                  </table>                                  
                              </div>
                           </div>
                         </div>                                               
                        </div>  <!-- body -->
                      </div>
                  </form>
              </div>
              <!-- /.tab-pane -->
              <div class="tab-pane" id="tab_3">
                  <form class='form-horizontal' action="" method="post" enctype="multipart/form-data">
                      <div class="box box-success">
                        <h4 class='box-title'> <i class="fa fa-globe"></i> Data Bangunan</h4>
                        <div class="box-body">
                         <div id="idn">
                           <div class="row">
                              <div class="col-md-12" style="background: #9fc5e8">
                                  <b>Rincian Data Bangunan</b>
                              </div>
                              <div class="col-md-6">                                 
                                  <table class="table"  style="font-size:13.4px" width='100%'>  
                                      <tbody>
                                        <tr>
                                          <td width='35%'>Jenis Bangunan</td>
                                          <td width='1%'>:</td>
                                          <td></td>
                                        </tr>
                                        <tr>
                                          <td>Luas Bangunan</td>
                                          <td>:</td>
                                          <td><?php echo $DATA_BANGUNAN->LUAS_BNG;?></td>
                                        </tr>
                                        <tr>
                                          <td>Jumlah Lantai</td>
                                          <td>:</td>
                                          <td><?php echo $DATA_BANGUNAN->JML_LANTAI_BNG;?></td>
                                        </tr>
                                        <tr>
                                          <td>Thn Dibangun</td>
                                          <td>:</td>
                                          <td><?php echo $DATA_BANGUNAN->THN_DIBANGUN_BNG;?></td>
                                        </tr>
                                        <tr>
                                          <td>Thn Renovasi</td>
                                          <td>:</td>
                                          <td><?php echo $DATA_BANGUNAN->THN_RENOVASI_BNG;?></td>
                                        </tr>
                                        <tr>
                                          <td>Kondisi Bangunan </td>
                                          <td>:</td>
                                          <td><?php if ($DATA_BANGUNAN->KONDISI_BNG==1) {
                                                       echo $DATA_BANGUNAN->KONDISI_BNG.' - SANGAT BAIK';
                                                    } else if ($DATA_BANGUNAN->KONDISI_BNG==2) {
                                                       echo $DATA_BANGUNAN->KONDISI_BNG.' - BAIK';
                                                    } else if ($DATA_BANGUNAN->KONDISI_BNG==3) {
                                                       echo $DATA_BANGUNAN->KONDISI_BNG.' - SEDANG';
                                                    } else if ($DATA_BANGUNAN->KONDISI_BNG==4) {
                                                       echo $DATA_BANGUNAN->KONDISI_BNG.' - JELEK';
                                                    } else { }?></td>
                                        </tr>
                                      </tbody>
                                  </table> 
                              </div>
                              <div class="col-md-6"> 
                                  <table class="table"  style="font-size:13.4px" width='100%'>  
                                      <tbody>
                                        <tr>
                                          <td width='35%'>Konstruksi</td>
                                          <td width='1%'>:</td>
                                          <td><?php if     ($DATA_BANGUNAN->JNS_KONSTRUKSI_BNG==1) {echo $DATA_BANGUNAN->JNS_KONSTRUKSI_BNG.' - BAJA';}
                                                    elseif ($DATA_BANGUNAN->JNS_KONSTRUKSI_BNG==2) {echo $DATA_BANGUNAN->JNS_KONSTRUKSI_BNG.' - BETON';}
                                                    elseif ($DATA_BANGUNAN->JNS_KONSTRUKSI_BNG==3) {echo $DATA_BANGUNAN->JNS_KONSTRUKSI_BNG.' - BATU BATA';}
                                                    elseif ($DATA_BANGUNAN->JNS_KONSTRUKSI_BNG==4) {echo $DATA_BANGUNAN->JNS_KONSTRUKSI_BNG.' - KAYU';}
                                                    else { }?></td>
                                        </tr>
                                        <tr>
                                          <td>Atap</td>
                                          <td>:</td>
                                          <td><?php if     ($DATA_BANGUNAN->JNS_ATAP_BNG==1) {echo $DATA_BANGUNAN->JNS_ATAP_BNG.' - jenis 1';}
                                                    elseif ($DATA_BANGUNAN->JNS_ATAP_BNG==2) {echo $DATA_BANGUNAN->JNS_ATAP_BNG.' - jenis 2';}
                                                    elseif ($DATA_BANGUNAN->JNS_ATAP_BNG==3) {echo $DATA_BANGUNAN->JNS_ATAP_BNG.' - GTG';}
                                                    elseif ($DATA_BANGUNAN->JNS_ATAP_BNG==4) {echo $DATA_BANGUNAN->JNS_ATAP_BNG.' - ASBES';}
                                                    elseif ($DATA_BANGUNAN->JNS_ATAP_BNG==5) {echo $DATA_BANGUNAN->JNS_ATAP_BNG.' - SENG';}
                                                    else   { }?></td>
                                        </tr>
                                        <tr>
                                          <td>Dinding</td>
                                          <td>:</td>
                                          <td><?php if ($DATA_BANGUNAN->KD_DINDING==0) { echo $DATA_BANGUNAN->KD_DINDING.' - jenis 0';}
                                                    elseif ($DATA_BANGUNAN->KD_DINDING==1) {echo $DATA_BANGUNAN->KD_DINDING.' - jenis 1';}
                                                    elseif ($DATA_BANGUNAN->KD_DINDING==2) {echo $DATA_BANGUNAN->KD_DINDING.' - jenis 2';}
                                                    elseif ($DATA_BANGUNAN->KD_DINDING==3) {echo $DATA_BANGUNAN->KD_DINDING.' - BATU BATA/CONBLOK';}
                                                    elseif ($DATA_BANGUNAN->KD_DINDING==4) {echo $DATA_BANGUNAN->KD_DINDING.' - jenis 4';}
                                                    elseif ($DATA_BANGUNAN->KD_DINDING==5) {echo $DATA_BANGUNAN->KD_DINDING.' - jenis 5';}
                                                    elseif ($DATA_BANGUNAN->KD_DINDING==6) {echo $DATA_BANGUNAN->KD_DINDING.' - jenis 6';}
                                                    else {echo $DATA_BANGUNAN->KD_DINDING.' - LAINNYA';}?></td>
                                        </tr>
                                        
                                        <tr>
                                          <td>Lantai</td>
                                          <td>:</td>
                                          <td><?php if ($DATA_BANGUNAN->KD_LANTAI==0) { echo $DATA_BANGUNAN->KD_LANTAI.' - jenis 0';}
                                                    elseif ($DATA_BANGUNAN->KD_LANTAI==1) {echo $DATA_BANGUNAN->KD_LANTAI.' - jenis 1';}
                                                    elseif ($DATA_BANGUNAN->KD_LANTAI==2) {echo $DATA_BANGUNAN->KD_LANTAI.' - KERAMIK';}
                                                    elseif ($DATA_BANGUNAN->KD_LANTAI==3) {echo $DATA_BANGUNAN->KD_LANTAI.' - JENIS 3';}
                                                    elseif ($DATA_BANGUNAN->KD_LANTAI==4) {echo $DATA_BANGUNAN->KD_LANTAI.' - jenis 4';}
                                                    elseif ($DATA_BANGUNAN->KD_LANTAI==5) {echo $DATA_BANGUNAN->KD_LANTAI.' - jenis 5';}
                                                    else {echo $DATA_BANGUNAN->KD_LANTAI.' - LAINNYA';}?></td>
                                        </tr>
                                        <tr>
                                          <td>Langit - langit</td>
                                          <td>:</td>
                                          <td><?php if ($DATA_BANGUNAN->KD_LANGIT_LANGIT==0) { echo $DATA_BANGUNAN->KD_LANGIT_LANGIT.' - jenis 0';}
                                                    elseif ($DATA_BANGUNAN->KD_LANGIT_LANGIT==1) {echo $DATA_BANGUNAN->KD_LANGIT_LANGIT.' - jenis 1';}
                                                    elseif ($DATA_BANGUNAN->KD_LANGIT_LANGIT==2) {echo $DATA_BANGUNAN->KD_LANGIT_LANGIT.' - TRIPLEK/ASBES BAMBU';}
                                                    elseif ($DATA_BANGUNAN->KD_LANGIT_LANGIT==3) {echo $DATA_BANGUNAN->KD_LANGIT_LANGIT.' - JENIS 3';}
                                                    elseif ($DATA_BANGUNAN->KD_LANGIT_LANGIT==4) {echo $DATA_BANGUNAN->KD_LANGIT_LANGIT.' - jenis 4';}
                                                    elseif ($DATA_BANGUNAN->KD_LANGIT_LANGIT==5) {echo $DATA_BANGUNAN->KD_LANGIT_LANGIT.' - jenis 5';}
                                                    elseif ($DATA_BANGUNAN->KD_LANGIT_LANGIT==6) {echo $DATA_BANGUNAN->KD_LANGIT_LANGIT.' - jenis 6';}
                                                    else {echo $DATA_BANGUNAN->KD_LANGIT_LANGIT.' - LAINNYA';}?></td>
                                        </tr>
                                      </tbody>
                                  </table>                                  
                              </div>
                           </div>
                           <div class="row">
                              <div class="col-md-12" style="background: #9fc5e8">
                                  <b>Fasilitas Bangunan</b>
                              </div>
                              <div class="col-md-6">                                 
                                  <table class="table"  style="font-size:13.4px" width='100%'>  
                                      <tbody>
                                        <tr>
                                          <td width='35%'>Daya Listrik</td>
                                          <td width='1%'>:</td>
                                          <td></td>
                                        </tr>
                                        <tr>
                                          <td colspan="3" style="background: #9fc5e8"><b>AC</b></td>
                                        </tr>
                                        <tr>
                                          <td>Split</td>
                                          <td>:</td>
                                          <td></td>
                                        </tr>
                                        <tr>
                                          <td>AC Central</td>
                                          <td>:</td>
                                          <td></td>
                                        </tr>
                                        <tr>
                                          <td>Window</td>
                                          <td>:</td>
                                          <td></td>
                                        </tr>
                                        <tr>
                                          <td colspan="3" style="background: #9fc5e8"><b>Kolam Renang</b></td>
                                        </tr>
                                        <tr>
                                          <td>Luas (M<sup>2</sup>)</td>
                                          <td>:</td>
                                          <td></td>
                                        </tr>
                                        <tr>
                                          <td>Finishing</td>
                                          <td>:</td>
                                          <td></td>
                                        </tr>
                                        <tr>
                                          <td colspan="3" style="background: #9fc5e8"><b>Luas Perkerasan Halaman (M<sup>2</sup>)</b></td>
                                        </tr>
                                        <tr>
                                          <td>Ringan</td>
                                          <td>:</td>
                                          <td></td>
                                        </tr>
                                        <tr>
                                          <td>Sedang</td>
                                          <td>:</td>
                                          <td></td>
                                        </tr>
                                        <tr>
                                          <td>Berat</td>
                                          <td>:</td>
                                          <td></td>
                                        </tr>
                                        <tr>
                                          <td>Dg Penutup</td>
                                          <td>:</td>
                                          <td></td>
                                        </tr>
                                        <tr>
                                          <td colspan="3" style="background: #9fc5e8"><b>Jumlah Lapangan Tenis</b></td>
                                        </tr>
                                        <tr>
                                          <td>Beton</td>
                                          <td>:</td>
                                          <td></td>
                                        </tr>
                                        <tr>
                                          <td>Aspal</td>
                                          <td></td>
                                          <td></td>
                                        </tr>
                                        <tr>
                                          <td>Tanah Liat/Rumput</td>
                                          <td></td>
                                          <td></td>
                                        </tr>
                                      </tbody>
                                  </table> 
                              </div>
                              <div class="col-md-6"> 
                                  <table class="table"  style="font-size:13.4px" width='100%'>  
                                      <tbody>
                                        <tr>
                                          <td colspan="3" style="background: #9fc5e8"><b>Jumlah Lift</b></td>
                                        </tr>
                                        <tr>
                                          <td width='35%'>Penumpang</td>
                                          <td width='1%'>:</td>
                                          <td></td>
                                        </tr>
                                        <tr>
                                          <td>Barang</td>
                                          <td>:</td>
                                          <td></td>
                                        </tr>
                                        <tr>
                                          <td colspan="3" style="background: #9fc5e8"><b>Tangga Berjalan dg lebar</b></td>
                                        </tr>
                                        <tr>
                                          <td width='35%'></td>
                                          <td width='1%'>:</td>
                                          <td></td>
                                        </tr>
                                        <tr>
                                          <td>Panjang Pagar</td>
                                          <td>:</td>
                                          <td></td>
                                        </tr>
                                        <tr>
                                          <td width='35%'>Bahan Pagar</td>
                                          <td width='1%'>:</td>
                                          <td></td>
                                        </tr>
                                        <tr>
                                          <td colspan="3" style="background: #9fc5e8"><b>Pemadam Kebakaran</b></td>
                                        </tr>
                                        <tr>
                                          <td width='35%'></td>
                                          <td width='1%'>:</td>
                                          <td></td>
                                        </tr>
                                        <tr>
                                          <td>Jumlah PABX</td>
                                          <td>:</td>
                                          <td></td>
                                        </tr>
                                        <tr>
                                          <td width='35%'>Kedalaman Artesis</td>
                                          <td width='1%'>:</td>
                                          <td></td>
                                        </tr>
                                      </tbody>
                                  </table>                                  
                              </div>                           
                           </div>
                           <div class="row">
                              <div class="col-md-12" style="background: #9fc5e8">
                                  <b>Informasi Perekaman Data</b>
                              </div>
                              <div class="col-md-6">                                 
                                   <table class="table"  style="font-size:13.4px" width='100%'>  
                                      <tbody>
                                        <tr>
                                          <td width='35%'>Jenis Transaksi</td>
                                          <td width='1%'>:</td>
                                          <td><?php if ($objek_pajak->JNS_TRANSAKSI_OP==1) {echo $objek_pajak->JNS_TRANSAKSI_OP.' - Perekaman Data';} else { $objek_pajak->JNS_TRANSAKSI_OP.' - Pemut'; }?></td>
                                        </tr>
                                        <tr>
                                          <td>Tanggal Pendataan</td>
                                          <td>:</td>
                                          <td><?php echo $objek_pajak->TGL_PEREKAMAN?></td>
                                        </tr>
                                        <tr>
                                          <td>Tanggal Penelitian</td>
                                          <td>:</td>
                                          <td><?php echo $objek_pajak->TGL_PEMERIKSAAN?></td>
                                        </tr>
                                        <tr>
                                          <td>Nomor Formulir</td>
                                          <td>:</td>
                                          <td><?php echo $objek_pajak->NO_FORMULIR_SPOP?></td>
                                        </tr>
                                      </tbody>
                                  </table>  
                              </div>
                              <div class="col-md-6"> 
                                  <table class="table"  style="font-size:13.4px" width='100%'>  
                                      <tbody>
                                        <tr>
                                          <td width='35%'>NIP Pendata</td>
                                          <td width='1%'>:</td>
                                          <td><?php echo $objek_pajak->NIP_PEREKAM_OP;?></td>
                                        </tr>
                                        <tr>
                                          <td>NIP Peneliti</td>
                                          <td>:</td>
                                          <td><?php echo $objek_pajak->NIP_PEMERIKSA_OP;?></td>
                                        </tr>
                                      </tbody>
                                  </table>                               
                              </div>                           
                           </div>
                         </div>                                                                      
                        </div>  <!-- body -->
                      </div>
                  </form>
              </div>
              <!-- tab 4 !-->
              <div class="tab-pane" id="tab_4">
                  <form class='form-horizontal' action="" method="post" enctype="multipart/form-data">
                      <div class="box box-success">
                        <h4 class='box-title'> <i class="fa fa-galeri"></i> Gambar Objek</h4>
                        <div class="box-body">
                         <div id="idn">
                           <div class="row">
                              <div class="col-md-6">                                 
                                 
                              </div>
                              <div class="col-md-6"> 
                                                     
                              </div>
                           </div>
                         </div>                                               
                        </div>  <!-- body -->
                      </div>
                  </form>
              </div>
              <!-- tab 5 !-->
              <div class="tab-pane" id="tab_5">
                  <form class='form-horizontal' action="" method="post" enctype="multipart/form-data">
                      <div class="box box-success">
                        <div class="box-body">
                         <div id="idn">
                           <div class="row">
                              <div class="col-md-8">                                 
                                    <table class="table table-bordered table-hover table-striped">
                                        <thead>
                                          <tr>
                                            <th>Tahun</th>
                                            <th>Nama WP</th>
                                            <th>Alamat WP</th>
                                            <th>NJOP Bumi</th>
                                            <th>NJOP Bng</th>
                                            <th>PBB</th>
                                            <th>Lunas ?</th>
                                            <th>TglPembayaran</th>
                                            <th>Flag/Unflag</th>
                                          </tr>
                                        </thead>
                                        <tbody>
                                          <?php if(count($SPPT)>0){ 
                                           $no=1; foreach($SPPT as $rn){   ?>
                                           <tr>
                                             <td><?php echo $rn->THN_PAJAK_SPPT?></td>
                                             <td><?php echo $rn->NM_WP_SPPT?></td>
                                             <td><?php echo $rn->JLN_WP_SPPT?></td>
                                             <td align='right'><?php echo number_format($rn->NJOP_BUMI_SPPT,'0',',','.')?></td>
                                             <td align='right'><?php echo number_format($rn->NJOP_BNG_SPPT,'0',',','.')?></td>
                                             <td align='right'><?php echo number_format($rn->PBB_TERHUTANG_SPPT,'0',',','.')?></td>
                                             <td><?php if ($rn->STATUS_PEMBAYARAN_SPPT=='1') {echo "Iya";} else {echo "Tidak";}?></td>
                                             <td><?php if ($rn->STATUS_PEMBAYARAN_SPPT=='1') {echo " ";} else {echo " ";}?></td>
                                             <td></td>
                                           </tr>
                                          <?php  } }else{?>
                                            <tr>
                                              <td colspan="5">Tidak ada data</td>
                                            </tr>

                                           <?php  } ?>
                                        </tbody>
                                    </table>
                              </div>
                              <div class="col-md-4"> 
                                                     
                              </div>
                           </div>
                         </div>                                               
                        </div>  <!-- body -->
                      </div>
                  </form>
              </div>
               <!-- tab 6 !-->
              <div class="tab-pane" id="tab_6">
                  <form class='form-horizontal' action="" method="post" enctype="multipart/form-data">
                      <div class="box box-success">
                        <h4 class='box-title'> <i class="fa fa-globe"></i> Detail SPPT</h4>
                        <div class="box-body">
                         <div id="idn">
                           <div class="row">
                              <div class="col-md-6">                                 
                                  <table class="table"  style="font-size:13.4px" width='100%'>  
                                      <tbody>
                                        <tr>
                                          <td width='35%'>Letak Objek Pajak</td>
                                          <td width='1%'>:</td>
                                          <td></td>
                                        </tr>
                                        <tr>
                                          <td>RW / RT</td>
                                          <td>:</td>
                                          <td></td>
                                        </tr>
                                        <tr>
                                          <td>Persil</td>
                                          <td>:</td>
                                          <td></td>
                                        </tr>
                                      </tbody>
                                  </table> 
                              </div>
                              <div class="col-md-6"> 
                                  <table class="table"  style="font-size:13.4px" width='100%'>  
                                      <tbody>
                                        <tr>
                                          <td width='35%'>Nama Subjek Pajak</td>
                                          <td width='1%'>:</td>
                                          <td></td>
                                        </tr>
                                        <tr>
                                          <td>Alamat Subjek Pajak</td>
                                          <td>:</td>
                                          <td align='right'></td>
                                        </tr>
                                      </tbody>
                                  </table>                                  
                              </div>
                           </div>
                           <div class="col-md-12" >                              
                           </div>
                           <div class="row">
                              <div class="col-md-9">
                                  <table class="table"  style="font-size:13.4px" width='100%'>
                                        <thead>
                                          <tr>
                                            <td></td>
                                            <td></td>
                                            <td align='center'>Luas(M<sup>2</sup>)</td>
                                            <td align='center'>Kelas</td>
                                            <td align='center'>NJOP Per M<sup>2</sup></td>
                                            <td align='center'>Total NJOP</td>
                                          </tr>
                                        </thead>
                                        <tbody>
                                          <tr>
                                          <td width='23%'>Bumi</td>
                                          <td width='1%'></td>
                                          <td><input type='text'></td>
                                          <td><input type='text'></td>
                                          <td><input type='text'></td>
                                          <td><input type='text'></td>
                                        </tr>
                                        <tr>
                                          <td>Bangunan</td>
                                          <td></td>
                                          <td><input type='text'></td>
                                          <td><input type='text'></td>
                                          <td><input type='text'></td>
                                          <td><input type='text'></td>
                                        </tr>
                                        <tr>
                                          <td>Bumi*</td>
                                          <td></td>
                                          <td><input type='text'></td>
                                          <td><input type='text'></td>
                                          <td><input type='text'></td>
                                          <td><input type='text'></td>
                                        </tr>
                                        <tr>
                                          <td>Bangunan</td>
                                          <td></td>
                                          <td><input type='text'></td>
                                          <td><input type='text'></td>
                                          <td><input type='text'></td>
                                          <td><input type='text'></td>
                                        </tr>
                                        </tbody>
                                  </table>                                 
                              </div>
                              <div class="col-md-4">                                                                  
                              </div>
                           </div>
                           <div class="col-md-12" >                              
                           </div><br>
                           <div class="row">
                              <div class="col-md-6">
                                 <table class="table"  style="font-size:13.4px" width='100%'>  
                                      <tbody>
                                        <tr>
                                          <td width='35%'>Jumlah NJOP Bumi</td>
                                          <td width='1%'>:</td>
                                          <td></td>
                                        </tr>
                                        <tr>
                                          <td>Jumlah NJOP Bangunan</td>
                                          <td>:</td>
                                          <td></td>
                                        </tr>
                                        <tr>
                                          <td>NJOP PBB</td>
                                          <td>:</td>
                                          <td></td>
                                        </tr>
                                        <tr>
                                          <td>NJOPTKP</td>
                                          <td>:</td>
                                          <td></td>
                                        </tr>
                                      </tbody>
                                  </table>                                   
                              </div>
                              <div class="col-md-6"> 
                                   <table class="table"  style="font-size:13.4px" width='100%'>  
                                      <tbody>
                                        <tr>
                                          <td width='35%'>PBB Terhutang</td>
                                          <td width='1%'>:</td>
                                          <td></td>
                                        </tr>
                                        <tr>
                                          <td>Faktor Pengurang</td>
                                          <td>:</td>
                                          <td></td>
                                        </tr>
                                        <tr>
                                          <td>PBB yang harus dibayar</td>
                                          <td>:</td>
                                          <td></td>
                                        </tr>
                                        <tr>
                                          <td>Status Pembayaran</td>
                                          <td>:</td>
                                          <td></td>
                                        </tr>
                                      </tbody>
                                  </table>                               
                              </div>
                           </div>
                           <div class="col-md-12" >                              
                           </div><br>
                           <div class="row">
                              <div class="col-md-6">
                                 <table class="table"  style="font-size:13.4px" width='100%'>  
                                      <tbody>
                                        <tr>
                                          <td width='35%'>Denda SPPT</td>
                                          <td width='1%'>:</td>
                                          <td></td>
                                        </tr>
                                        <tr>
                                          <td>Jumlah Pembayaran</td>
                                          <td>:</td>
                                          <td></td>
                                        </tr>
                                        <tr>
                                          <td>Selisih Pembayaran</td>
                                          <td>:</td>
                                          <td></td>
                                        </tr>
                                      </tbody>
                                  </table>                                   
                              </div>
                              <div class="col-md-6"> 
                                   <table class="table"  style="font-size:13.4px" width='100%'>  
                                      <tbody>
                                        <tr>
                                          <td width='35%'>Tanggal Jatuh Tempo</td>
                                          <td width='1%'>:</td>
                                          <td></td>
                                        </tr>
                                        <tr>
                                          <td>Tanggal Pembayaran</td>
                                          <td>:</td>
                                          <td></td>
                                        </tr>
                                        <tr>
                                          <td>Tempat Pembayaran</td>
                                          <td>:</td>
                                          <td></td>
                                        </tr>
                                      </tbody>
                                  </table>                               
                              </div>
                           </div>
                         </div>
                         <div class="col-md-12" >                              
                           </div><br>
                           <div class="row">
                              <div class="col-md-6">
                                 <table class="table"  style="font-size:13.4px" width='100%'>  
                                      <tbody>
                                        <tr>
                                          <td width='35%'>Tanggal Terbit</td>
                                          <td width='1%'>:</td>
                                          <td></td>
                                        </tr>
                                        <tr>
                                          <td>Tanggal cetak</td>
                                          <td>:</td>
                                          <td></td>
                                        </tr>
                                        <tr>
                                          <td>NIP Pencetak</td>
                                          <td>:</td>
                                          <td></td>
                                        </tr>
                                      </tbody>
                                  </table>                                   
                              </div>
                              <div class="col-md-6">                            
                              </div>
                           </div>                                                                      
                        </div>  <!-- body -->
                      </div>
                  </form>
              </div>
            </div>
            <!-- /.tab-content -->
          </div>        
        </section>


<style type="text/css">
 .control-label{
    text-align: left;
}
.table > tbody > tr > td, .table > tbody > tr > th, .table > tfoot > tr > td, .table > tfoot > tr > th, .table > thead > tr > td, .table > thead > tr > th {
    padding: 3px;
    line-height: 1.42857143;
    vertical-align: top;
    border-top: 1px solid #f9f9f900;
}
</style>
<script type="text/javascript">
function myFunction() {
  if (true) {
            $(".form-control").keyup(function () {
                if (this.value.length == this.maxLength) {
                  var nextIndex = $('input:text').index(this) + 1;
                  $('input:text')[nextIndex].focus();
                }
        });

};
};
</script>
