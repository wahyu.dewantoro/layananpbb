<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Penetapan extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('Mview');
		$this->load->model('Mspop');
		$this->load->model('Mpermohonan');
		if ($this->session->userdata('pbb') <> 1) {
			$this->session->set_flashdata('notif', '<div class="badge">
                    Silahkan login dengan username dan password anda.</p>
                    </div>');
			redirect('auth');
		}
	}
	function penetapan_terseleksi()
	{
		$data['kecamatan']				 = $this->db->query("select kd_kecamatan,kd_propinsi||'.'||kd_dati2||'.'||kd_kecamatan kode,kd_propinsi||'.'||kd_dati2||'.'||kd_kecamatan||' - '||nm_kecamatan nama
 															from ref_kecamatan where kd_dati2='07'")->result_array();
		$this->template->load('template', 'penetapan/penetapan_terseleksi', $data);
	}
	function data_penetapan_terseleksi()
	{
		$tahun     = $this->input->post('tahun');
		$propinsi  = $this->input->post('propinsi');
		$dati2 	   = $this->input->post('dati2');
		$kecamatan = $this->input->post('kecamatan');
		$kelurahan = $this->input->post('kelurahan');
		$blok 	   = $this->input->post('blok');
		$nourut    = $this->input->post('nourut');


		echo $nourut;
	}
}
