<?php

if (!defined('BASEPATH'))
  exit('No direct script access allowed');

require_once APPPATH . '/third_party/spout/src/Spout/Autoloader/autoload.php';

use Box\Spout\Writer\WriterFactory;
use Box\Spout\Common\Type;
use Box\Spout\Writer\Style\StyleBuilder;

class Laporan extends CI_Controller
{
  function __construct()
  {
    parent::__construct();
    if ($this->session->userdata('pbb') <> 1) {
      $this->session->set_flashdata('notif', '<div class="badge">
                    Silahkan login dengan username dan password anda.</p>
                    </div>');
      redirect('auth');
    }
    $this->load->model('Mmaster');
    $this->load->library('form_validation');
    $this->load->library('datatables');
  }

  function getkelurahan()
  {
    $KD_KECAMATAN = $this->input->post('kd_kec');
    $res = $this->db->query("select kd_kelurahan,nm_kelurahan 
                                from ref_kelurahan
                                where kd_kecamatan='$KD_KECAMATAN' order by kd_kelurahan asc")->result();
    $option = "<option value=''>Pilih</option>";
    $option .= "<option value=''>000 Semua Kelurahan</option>";
    foreach ($res as $res) {
      $option .= "<option value='" . $res->KD_KELURAHAN . "'>" . $res->KD_KELURAHAN . ' ' . $res->NM_KELURAHAN . "</option>";
    }

    echo $option;
  }


  function permohonanDetail()
  {
    if (isset($_POST['tgl1']) && isset($_POST['tgl2'])) {
      $tgl1       = $_POST['tgl1'];
      $tgl2       = $_POST['tgl2'];


      // echo $this->db->last_query();
    } else {
      $tgl1 = DATE('01-m-Y');
      $tgl2 = DATE('d-m-Y');
    }

    $kg = $this->session->userdata('pbb_kg');
    switch ($kg) {
      case '3':
        # pel 1
        $and = "";
        break;
      case '21':
        # pel 2
        $and = "";
        break;

      case '1':
        # admin
        $and = "";
        break;

      default:
        # code...
        $and = "and  no_layanan||'.'||nop in (
                select distinct  THN_PELAYANAN||'.'||BUNDEL_PELAYANAN||'.'||NO_URUT_PELAYANAN||'.'||KD_PROPINSI_RIWAYAT||'.'||KD_DATI2_RIWAYAT||'.'||KD_KECAMATAN_RIWAYAT||'.'||KD_KELURAHAN_RIWAYAT||'.'||KD_BLOK_RIWAYAT||'.'||NO_URUT_RIWAYAT||'.'||KD_JNS_OP_RIWAYAT
                from p_riwayat 
                where kode_group='$kg') ";
        // $and="";
        break;
    }
    $data['rk'] = $this->db->query("select no_layanan,nop,nama_pemohon,to_char(tanggal_terima,'dd/mm/YYYY') tgl_terima , to_char(tanggal_selesai,'dd/mm/YYYY') tgl_selesai,nm_jenis_pelayanan,keterangan_pst,unit_kantor from pv_rekappelayanan where tanggal_terima between  to_date('$tgl1','dd-mm-yyyy') and to_date('$tgl2','dd-mm-yyyy') $and  order by tanggal_terima desc")->result();
    $data['tgl1'] = $tgl1;
    $data['tgl2'] = $tgl2;
    // echo $this->db->last_query();
    $this->template->load('template', 'laporan/detailPermohonan', $data);
  }

  function detailpermohonanexcel()
  {
    $tgl1 = urldecode($this->input->get('t1'));
    $tgl2 = urldecode($this->input->get('t2'));

    $kg = $this->session->userdata('pbb_kg');
    switch ($kg) {
      case '3':
        # pel 1
        $and = "";
        break;
      case '21':
        # pel 2
        $and = "";
        break;

      case '1':
        # admin
        $and = "";
        break;

      default:
        # code...
        $and = "and  no_layanan||'.'||nop in (
                select distinct  THN_PELAYANAN||'.'||BUNDEL_PELAYANAN||'.'||NO_URUT_PELAYANAN||'.'||KD_PROPINSI_RIWAYAT||'.'||KD_DATI2_RIWAYAT||'.'||KD_KECAMATAN_RIWAYAT||'.'||KD_KELURAHAN_RIWAYAT||'.'||KD_BLOK_RIWAYAT||'.'||NO_URUT_RIWAYAT||'.'||KD_JNS_OP_RIWAYAT
                from p_riwayat 
                where kode_group='$kg') ";
        break;
    }
    $data['rk'] = $this->db->query("select no_layanan,nop,nama_pemohon,to_char(tanggal_terima,'dd/mm/YYYY') tgl_terima , to_char(tanggal_selesai,'dd/mm/YYYY') tgl_selesai,nm_jenis_pelayanan,keterangan_pst,unit_kantor from pv_rekappelayanan where tanggal_terima between  to_date('$tgl1','dd-mm-yyyy') and to_date('$tgl2','dd-mm-yyyy') $and  order by tanggal_terima desc")->result();

    // print_r($data);
    $this->load->view('laporan/exceldetailpermohonan', $data);
  }

  function permohonanrekap_sppt()
  {
    if (isset($_POST['tgl1']) && isset($_POST['tgl2'])) {
      $tgl1       = $_POST['tgl1'];
      $tgl2       = $_POST['tgl2'];
    } else {
      $tgl1 = date('01-m-Y');
      $tgl2 = date('d-m-Y');
    }
    $kg = $this->session->userdata('pbb_kg');
    switch ($kg) {
      case '3':
        # pel 1
        $and = "";
        break;
      case '21':
        # pel 2
        $and = "";
        break;

      case '1':
        # admin
        $and = "";
        break;

      default:
        # code...
        $and = ""/*"and  no_layanan||'.'||nop in (
                select distinct  THN_PELAYANAN||'.'||BUNDEL_PELAYANAN||'.'||NO_URUT_PELAYANAN||'.'||KD_PROPINSI_RIWAYAT||'.'||KD_DATI2_RIWAYAT||'.'||KD_KECAMATAN_RIWAYAT||'.'||KD_KELURAHAN_RIWAYAT||'.'||KD_BLOK_RIWAYAT||'.'||NO_URUT_RIWAYAT||'.'||KD_JNS_OP_RIWAYAT
                from p_riwayat 
                where kode_group='$kg') "*/;
        break;
    }
    $data['rk'] = $this->db->query("select KD_JNS_PELAYANAN,nm_jenis_pelayanan,count(*) jum 
                                      from pv_rekappelayanan where tanggal_terima between  to_date('$tgl1','dd-mm-yyyy') and to_date('$tgl2','dd-mm-yyyy') $and group by KD_JNS_PELAYANAN, nm_jenis_pelayanan order by jum desc")->result();
    /* $data['rk'] =$this->db->query("select KD_JNS_PELAYANAN,nm_jenis_pelayanan,CASE WHEN  sum(jumlah_berkas) is null THEN   0  ELSE sum(jumlah_berkas) END jml_berkas,CASE WHEN  sum(jumlah_nop) is null THEN   0  ELSE sum(jumlah_nop) END jml_nop,count(*) jum 
                                      from pv_rekappelayanan where tanggal_terima between  to_date('$tgl1','dd-mm-yyyy') and to_date('$tgl2','dd-mm-yyyy') $and group by KD_JNS_PELAYANAN, nm_jenis_pelayanan order by jum desc")->result();*/

    $data['tgl1'] = $tgl1;
    $data['tgl2'] = $tgl2;
    $this->template->load('template', 'laporan/rekapPermohonanSppt', $data);
  }
  function permohonanrekap()
  {
    if (isset($_POST['tgl1']) && isset($_POST['tgl2'])) {
      $tgl1       = $_POST['tgl1'];
      $tgl2       = $_POST['tgl2'];
    } else {
      $tgl1 = date('01-m-Y');
      $tgl2 = date('d-m-Y');
    }
    $kg = $this->session->userdata('pbb_kg');
    switch ($kg) {
      case '3':
        # pel 1
        $and = "";
        break;
      case '21':
        # pel 2
        $and = "";
        break;

      case '1':
        # admin
        $and = "";
        break;

      default:
        # code...
        $and = ""/*"and  no_layanan||'.'||nop in (
                select distinct  THN_PELAYANAN||'.'||BUNDEL_PELAYANAN||'.'||NO_URUT_PELAYANAN||'.'||KD_PROPINSI_RIWAYAT||'.'||KD_DATI2_RIWAYAT||'.'||KD_KECAMATAN_RIWAYAT||'.'||KD_KELURAHAN_RIWAYAT||'.'||KD_BLOK_RIWAYAT||'.'||NO_URUT_RIWAYAT||'.'||KD_JNS_OP_RIWAYAT
                from p_riwayat 
                where kode_group='$kg') "*/;
        break;
    }
    $data['rk'] = $this->db->query("select nm_jenis_pelayanan,kd_jns_pelayanan,count(kd_jns_pelayanan) jum
                                      from pv_rekap_nopel
                                      where tanggal_terima between  to_date('$tgl1','dd-mm-yyyy') and to_date('$tgl2','dd-mm-yyyy') $and group by KD_JNS_PELAYANAN, nm_jenis_pelayanan order by jum desc")->result();
    /* $data['rk'] =$this->db->query("select KD_JNS_PELAYANAN,nm_jenis_pelayanan,CASE WHEN  sum(jumlah_berkas) is null THEN   0  ELSE sum(jumlah_berkas) END jml_berkas,CASE WHEN  sum(jumlah_nop) is null THEN   0  ELSE sum(jumlah_nop) END jml_nop,count(*) jum 
                                      from pv_rekappelayanan where tanggal_terima between  to_date('$tgl1','dd-mm-yyyy') and to_date('$tgl2','dd-mm-yyyy') $and group by KD_JNS_PELAYANAN, nm_jenis_pelayanan order by jum desc")->result();*/

    $data['tgl1'] = $tgl1;
    $data['tgl2'] = $tgl2;
    $this->template->load('template', 'laporan/rekapPermohonan', $data);
  }
  function permohonanDetailNopel($tgl1, $tgl2, $var)
  {

    $data['rk'] = $this->db->query("select  THN_PELAYANAN||'.'||BUNDEL_PELAYANAN||'.'||NO_URUT_PELAYANAN no_layanan,kd_jns_pelayanan,nama_pemohon,TO_CHAR (tanggal_terima, 'dd/mm/yyyy') TGL_TERIMA,
            TO_CHAR (tanggal_selesai, 'dd/mm/yyyy') TGL_SELESAI,STATUS_KOLEKTIF
                                      from pv_rekap_nopel
                                      where tanggal_terima between  to_date('$tgl1','dd-mm-yyyy') and to_date('$tgl2','dd-mm-yyyy') and kd_jns_pelayanan='$var'
                                      group by THN_PELAYANAN||'.'||BUNDEL_PELAYANAN||'.'||NO_URUT_PELAYANAN,kd_jns_pelayanan,nama_pemohon,tanggal_terima,tanggal_selesai,STATUS_KOLEKTIF")->result();
    /*echo  $this->db->last_query();
      exit();*/
    $dd = $this->db->query("select * from ref_jns_pelayanan where kd_jns_pelayanan='$var'")->row();

    $data['judul'] = "Detail Pelayanan " . $dd->NM_JENIS_PELAYANAN . ' Periode ' . date('d-m-Y', strtotime($tgl1)) . ' s/d ' . date('d-m-Y', strtotime($tgl2));
    $this->template->load('template', 'laporan/detailNopel', $data);
  }
  function detailRekappermohonan($tgl1, $tgl2, $var)
  {
    $kg = $this->session->userdata('pbb_kg');
    /*switch ($kg) {
        case '3':
          # pel 1
        $and="";
          break;
        case '21':
          # pel 2
          $and="";
          break;

        case '1':
          # admin
          $and="";
          break;
        
        default:
          # code...*/
    /*$and="and  no_layanan||'.'||nop in (
                select distinct  THN_PELAYANAN||'.'||BUNDEL_PELAYANAN||'.'||NO_URUT_PELAYANAN||'.'||KD_PROPINSI_RIWAYAT||'.'||KD_DATI2_RIWAYAT||'.'||KD_KECAMATAN_RIWAYAT||'.'||KD_KELURAHAN_RIWAYAT||'.'||KD_BLOK_RIWAYAT||'.'||NO_URUT_RIWAYAT||'.'||KD_JNS_OP_RIWAYAT
                from p_riwayat 
                where kode_group='$kg') ";*/
    /*    break;
      }*/
    $and = "and  no_layanan||'.'||nop in (
                select distinct  THN_PELAYANAN||'.'||BUNDEL_PELAYANAN||'.'||NO_URUT_PELAYANAN||'.'||KD_PROPINSI_RIWAYAT||'.'||KD_DATI2_RIWAYAT||'.'||KD_KECAMATAN_RIWAYAT||'.'||KD_KELURAHAN_RIWAYAT||'.'||KD_BLOK_RIWAYAT||'.'||NO_URUT_RIWAYAT||'.'||KD_JNS_OP_RIWAYAT
                from p_riwayat 
               ) ";
    // echo $and;

    $data['rk'] = $this->db->query("select no_layanan,nop,nama_pemohon,to_char( tanggal_terima,'dd/mm/yyyy') tanggal_terima from pv_rekappelayanan  where tanggal_terima between  to_date('$tgl1','dd-mm-yyyy') and to_date('$tgl2','dd-mm-yyyy') and kd_jns_pelayanan = '$var' ")->result();
    /*echo  $this->db->last_query();
			exit();*/
    $dd = $this->db->query("select * from ref_jns_pelayanan where kd_jns_pelayanan='$var'")->row();

    $data['judul'] = "Detail " . $dd->NM_JENIS_PELAYANAN . ' Periode ' . date('d-m-Y', strtotime($tgl1)) . ' s/d ' . date('d-m-Y', strtotime($tgl2));
    $this->template->load('template', 'laporan/detailRekappermohonan', $data);
  }


  function detailmodalrekappermohonan()
  {
    $var = $_POST['id'];
    $tgl1 = $_POST['tgl1'];
    $tgl2 = $_POST['tgl2'];
    $kg = $this->session->userdata('pbb_kg');
    switch ($kg) {
      case '3':
        # pel 1
        $and = "";
        break;
      case '21':
        # pel 2
        $and = "";
        break;

      case '1':
        # admin
        $and = "";
        break;

      default:
        # code...
        $and = "and  no_layanan||'.'||nop in (
                select distinct  THN_PELAYANAN||'.'||BUNDEL_PELAYANAN||'.'||NO_URUT_PELAYANAN||'.'||KD_PROPINSI_RIWAYAT||'.'||KD_DATI2_RIWAYAT||'.'||KD_KECAMATAN_RIWAYAT||'.'||KD_KELURAHAN_RIWAYAT||'.'||KD_BLOK_RIWAYAT||'.'||NO_URUT_RIWAYAT||'.'||KD_JNS_OP_RIWAYAT
                from p_riwayat 
                where kode_group='$kg') ";
        break;
    }

    // echo $and;

    $data['rk'] = $this->db->query("select no_layanan,nop,nama_pemohon,to_char( tanggal_terima,'dd/mm/yyyy') tanggal_terima from pv_rekappelayanan  where tanggal_terima between  to_date('$tgl1','yyyy-mm-dd') and to_date('$tgl2','yyyy-mm-dd') and kd_jns_pelayanan = '$var' $and")->result();
    /*echo  $this->db->last_query();
      exit();*/
    $dd = $this->db->query("select * from ref_jns_pelayanan where kd_jns_pelayanan='$var'")->row();

    $data['judul'] = "Detail " . $dd->NM_JENIS_PELAYANAN . ' Periode ' . date('d-m-Y', strtotime($tgl1)) . ' s/d ' . date('d-m-Y', strtotime($tgl2));
    // $this->template->load('template','laporan/detailRekappermohonan',$data);
    $this->load->view('laporan/detailRekappermohonanmodal', $data);
  }
  function permohonanrekap_sppt_layanan()
  {
    if (isset($_POST['tgl1']) && isset($_POST['tgl2'])) {
      $tgl1       = $_POST['tgl1'];
      $tgl2       = $_POST['tgl2'];
    } else {
      $tgl1 = date('01-m-Y');
      $tgl2 = date('d-m-Y');
    }
    $data['rk'] = $this->db->query(" select aa.kd_jns_pelayanan, aa.nm_jenis_pelayanan,bb.jum as nopel, aa.jum as jsppt from (
                                      select KD_JNS_PELAYANAN,nm_jenis_pelayanan,count(*) jum 
                                      from pv_rekappelayanan where 
                                      tanggal_terima between to_date('$tgl1','dd-mm-yyyy') and to_date('$tgl2','dd-mm-yyyy') 
                                      group by KD_JNS_PELAYANAN, nm_jenis_pelayanan 
                                      )  aa, 
                                      (
                                      select nm_jenis_pelayanan,kd_jns_pelayanan,count(kd_jns_pelayanan) jum 
                                      from pv_rekap_nopel where tanggal_terima between to_date('$tgl1','dd-mm-yyyy') and to_date('$tgl2','dd-mm-yyyy')  
                                      group by KD_JNS_PELAYANAN, nm_jenis_pelayanan 
                                      )  bb
                                      where aa.kd_jns_pelayanan = bb.kd_jns_pelayanan ")->result();

    $data['tgl1'] = $tgl1;
    $data['tgl2'] = $tgl2;
    $this->template->load('template', 'laporan/rekapPermohonan_sppt_Nolayanan', $data);
  }
  function detailRekappermohonan_SpptLayanan($tgl1, $tgl2, $var)
  {
    $data['sppt']  = $this->db->query("select no_layanan,nop,nama_pemohon,to_char( tanggal_terima,'dd/mm/yyyy') tanggal_terima from pv_rekappelayanan  where tanggal_terima between  to_date('$tgl1','dd-mm-yyyy') and to_date('$tgl2','dd-mm-yyyy') and kd_jns_pelayanan = '$var' ")->result();
    $data['nopel'] = $this->db->query("select  THN_PELAYANAN||'.'||BUNDEL_PELAYANAN||'.'||NO_URUT_PELAYANAN no_layanan,kd_jns_pelayanan,nama_pemohon,TO_CHAR (tanggal_terima, 'dd/mm/yyyy') TGL_TERIMA,
                                      TO_CHAR (tanggal_selesai, 'dd/mm/yyyy') TGL_SELESAI,STATUS_KOLEKTIF
                                      from pv_rekap_nopel
                                      where tanggal_terima between  to_date('$tgl1','dd-mm-yyyy') and to_date('$tgl2','dd-mm-yyyy') and kd_jns_pelayanan='$var'
                                      group by THN_PELAYANAN||'.'||BUNDEL_PELAYANAN||'.'||NO_URUT_PELAYANAN,kd_jns_pelayanan,nama_pemohon,tanggal_terima,tanggal_selesai,STATUS_KOLEKTIF")->result();
    $dd = $this->db->query("select * from ref_jns_pelayanan where kd_jns_pelayanan='$var'")->row();

    $data['judul'] = "Detail " . $dd->NM_JENIS_PELAYANAN . ' Periode ' . date('d-m-Y', strtotime($tgl1)) . ' s/d ' . date('d-m-Y', strtotime($tgl2));
    $this->template->load('template', 'laporan/detailRekappermohonan_SpptLayanan', $data);
  }
  function grafik()
  {
    if (isset($_POST['tgl1']) && isset($_POST['tgl2'])) {
      $tgl1       = $_POST['tgl1'];
      $tgl2       = $_POST['tgl2'];
    } else {
      $tgl1 = date('Y-m-d');
      $tgl2 = date('Y-m-d');
    }
    $data['tgl1'] = $tgl1;
    $data['tgl2'] = $tgl2;

    $tahun = date('Y');

    $data['rk'] = $this->db->query("select nm_jenis_pelayanan layanan,count(*) jum 
                                  from pv_rekappelayanan 
                                  where tanggal_terima between to_date('$tgl1','yyyy-mm-dd') and to_date('$tgl2','yyyy-mm-dd')    group by nm_jenis_pelayanan order by jum desc")->result();

    $data['rg'] = $this->db->query("SELECT layanan,sum(jan) jan,
                                              sum(feb) feb,
                                              sum(mar) mar,
                                              sum(apr) apr,
                                              sum(mei) mei,
                                              sum(jun) jun,
                                              sum(jul) jul,
                                              sum(agu) agu,
                                              sum(sep) sep,
                                              sum(okt) okt,
                                              sum(nov) nov,
                                              sum(des) des
                                  from (            
                                      select layanan, case when urut='01' then jum else 0 end jan,
                                                      case when urut='02' then jum else 0 end feb,
                                                      case when urut='03' then jum else 0 end mar,
                                                      case when urut='04' then jum else 0 end apr,
                                                      case when urut='05' then jum else 0 end mei,
                                                      case when urut='06' then jum else 0 end jun,
                                                      case when urut='07' then jum else 0 end jul,
                                                      case when urut='08' then jum else 0 end agu,
                                                      case when urut='09' then jum else 0 end sep,
                                                      case when urut='10' then jum else 0 end okt,
                                                      case when urut='11' then jum else 0 end nov,
                                                      case when urut='12' then jum else 0 end des
                                      from (
                                          select nm_jenis_pelayanan layanan, to_char(tanggal_terima,'MM') urut  ,count(*) jum 
                                          from pv_rekappelayanan 
                                          where to_char(tanggal_terima,'yyyy')='$tahun'
                                          group by nm_jenis_pelayanan,to_char(tanggal_terima,'MM') order by urut asc
                                          )   
                                     ) 
                                      group by layanan ")->result();


    $this->template->load('template', 'laporan/grafik', $data);
  }

  function penerimaPermohon()
  {
    $kg = $this->session->userdata('pbb_kg');
    $nip = $this->session->userdata('nip');
    if (isset($_POST['tgl1']) && isset($_POST['tgl2'])) {
      $tgl1       = $_POST['tgl1'];
      $tgl2       = $_POST['tgl2'];
    } else {
      $tgl1 = date('d-m-Y');
      $tgl2 = date('d-m-Y');
    }

    if ($kg == 1) {
      // admin
      $wh = "WHERE TGL_TERIMA_DOKUMEN_WP BETWEEN  TO_DATE('$tgl1','dd-mm-yyyy') AND TO_DATE('$tgl2','dd-mm-yyyy')";
    } else {
      $wh = "WHERE TGL_TERIMA_DOKUMEN_WP BETWEEN  TO_DATE('$tgl1','dd-mm-yyyy') AND TO_DATE('$tgl2','dd-mm-yyyy') and NIP_PENERIMA='$nip'";
    }

    $sql = "SELECT  NIP_PENERIMA,nama_pengguna NAMA_PEGAWAI,TO_CHAR(TGL_TERIMA_DOKUMEN_WP,'mm') ANGKA_BULAN,TO_CHAR(TGL_TERIMA_DOKUMEN_WP,'Month') BULAN,TO_CHAR(TGL_TERIMA_DOKUMEN_WP,'YYYY') tahun,COUNT(*) JUMLAH  
        FROM PST_PERMOHONAN a
        left join p_pengguna b on a.nip_penerima=b.nip
        $wh
        GROUP BY NIP_PENERIMA,NAMA_pengguna,TO_CHAR(TGL_TERIMA_DOKUMEN_WP,'mm'),TO_CHAR(TGL_TERIMA_DOKUMEN_WP,'YYYY'),TO_CHAR(TGL_TERIMA_DOKUMEN_WP,'Month')
        ORDER BY TO_NUMBER(TO_CHAR(TGL_TERIMA_DOKUMEN_WP,'YYYY')) DESC, TO_CHAR(TGL_TERIMA_DOKUMEN_WP,'mm') DESC";
    $rk = $this->db->query($sql)->result();
    // echo $this->db->last_query();
    $data = array(
      'tgl1' => $tgl1,
      'tgl2' => $tgl2,
      'rk' => $rk
    );

    $this->template->load('template', 'laporan/penerimaPermohon', $data);
  }

  function execelPenerimaPermohonan($tgl1, $tgl2)
  {
    $kg = $this->session->userdata('pbb_kg');
    $nip = $this->session->userdata('nip');

    if ($kg == 1) {
      // admin
      $wh = "WHERE TGL_TERIMA_DOKUMEN_WP BETWEEN  TO_DATE('$tgl1','dd-mm-yyyy') AND TO_DATE('$tgl2','dd-mm-yyyy')";
    } else {
      $wh = "WHERE TGL_TERIMA_DOKUMEN_WP BETWEEN  TO_DATE('$tgl1','dd-mm-yyyy') AND TO_DATE('$tgl2','dd-mm-yyyy') and NIP_PENERIMA='$nip'";
    }

    $sql = "SELECT  NIP_PENERIMA ,NAMA_PENGGUNA NAMA_PEGAWAI,TO_CHAR(TGL_TERIMA_DOKUMEN_WP,'mm') ANGKA_BULAN,TO_CHAR(TGL_TERIMA_DOKUMEN_WP,'Month') BULAN,TO_CHAR(TGL_TERIMA_DOKUMEN_WP,'YYYY') TAHUN,COUNT(*) JUMLAH  
          FROM PST_PERMOHONAN A
          JOIN P_PENGGUNA B ON A.NIP_PENERIMA=B.NIP
          $wh
          GROUP BY NIP_PENERIMA,NAMA_PENGGUNA,TO_CHAR(TGL_TERIMA_DOKUMEN_WP,'mm'),TO_CHAR(TGL_TERIMA_DOKUMEN_WP,'YYYY'),TO_CHAR(TGL_TERIMA_DOKUMEN_WP,'Month')
          ORDER BY TO_NUMBER(TO_CHAR(TGL_TERIMA_DOKUMEN_WP,'YYYY')) DESC, TO_CHAR(TGL_TERIMA_DOKUMEN_WP,'mm') DESC";
    $rk = $this->db->query($sql)->result();
    // echo $this->db->last_query();
    $data = array(
      'tgl1' => $tgl1,
      'tgl2' => $tgl2,
      'rk' => $rk,
      'judul' => 'Rekap penerima permohonan ' . date('d M Y', strtotime($tgl1)) . ' sampai ' . date('d M Y', strtotime($tgl2))
    );

    $this->load->view('laporan/execelPenerimaPermohonan', $data);
  }

  function execelJumlahPelayanan($tgl1, $tgl2)
  {
    $kg = $this->session->userdata('pbb_kg');
    $nip = $this->session->userdata('nip');

    /*if($kg==1){
      // admin
      $wh="WHERE TGL_TERIMA_DOKUMEN_WP BETWEEN  TO_DATE('$tgl1','dd-mm-yyyy') AND TO_DATE('$tgl2','dd-mm-yyyy')";
    }else{
      $wh="WHERE TGL_TERIMA_DOKUMEN_WP BETWEEN  TO_DATE('$tgl1','dd-mm-yyyy') AND TO_DATE('$tgl2','dd-mm-yyyy') and NIP_PENERIMA='$nip'";
    }*/
    $wh = "where tanggal_terima between  to_date('$tgl1','dd-mm-yyyy') and to_date('$tgl2','dd-mm-yyyy')";
    $sql = "select nm_jenis_pelayanan,kd_jns_pelayanan,count(kd_jns_pelayanan) jum
                                      from pv_rekap_nopel $wh group by KD_JNS_PELAYANAN, nm_jenis_pelayanan order by jum desc";
    $rk = $this->db->query($sql)->result();
    // echo $this->db->last_query();
    $data = array(
      'tgl1' => $tgl1,
      'tgl2' => $tgl2,
      'rk' => $rk,
      'judul' => 'Rekap Jumlah Pelayanan ' . date('d M Y', strtotime($tgl1)) . ' sampai ' . date('d M Y', strtotime($tgl2))
    );

    $this->load->view('laporan/execelJumlahPelayanan', $data);
  }
  function execelJumlahSppt($tgl1, $tgl2)
  {
    $kg = $this->session->userdata('pbb_kg');
    $nip = $this->session->userdata('nip');

    /*if($kg==1){
      // admin
      $wh="WHERE TGL_TERIMA_DOKUMEN_WP BETWEEN  TO_DATE('$tgl1','dd-mm-yyyy') AND TO_DATE('$tgl2','dd-mm-yyyy')";
    }else{
      $wh="WHERE TGL_TERIMA_DOKUMEN_WP BETWEEN  TO_DATE('$tgl1','dd-mm-yyyy') AND TO_DATE('$tgl2','dd-mm-yyyy') and NIP_PENERIMA='$nip'";
    }*/
    $wh = "where tanggal_terima between  to_date('$tgl1','dd-mm-yyyy') and to_date('$tgl2','dd-mm-yyyy')";
    $sql = "select KD_JNS_PELAYANAN,nm_jenis_pelayanan,count(*) jum 
                                      from pv_rekappelayanan $wh group by KD_JNS_PELAYANAN, nm_jenis_pelayanan order by jum desc";
    $rk = $this->db->query($sql)->result();
    // echo $this->db->last_query();
    $data = array(
      'tgl1' => $tgl1,
      'tgl2' => $tgl2,
      'rk' => $rk,
      'judul' => 'Rekap Jumlah Sppt ' . date('d M Y', strtotime($tgl1)) . ' sampai ' . date('d M Y', strtotime($tgl2))
    );

    $this->load->view('laporan/execelJumlahSppt', $data);
  }
  function execelpermohonanrekap_sppt_layanan($tgl1, $tgl2)
  {
    $wh = "where tanggal_terima between  to_date('$tgl1','dd-mm-yyyy') and to_date('$tgl2','dd-mm-yyyy')";
    $sql = "select aa.kd_jns_pelayanan, aa.nm_jenis_pelayanan,bb.jum as nopel, aa.jum as jsppt from (
          select KD_JNS_PELAYANAN,nm_jenis_pelayanan,count(*) jum 
          from pv_rekappelayanan $wh  
          group by KD_JNS_PELAYANAN, nm_jenis_pelayanan 
          )  aa, 
          (
          select nm_jenis_pelayanan,kd_jns_pelayanan,count(kd_jns_pelayanan) jum 
          from pv_rekap_nopel $wh  
          group by KD_JNS_PELAYANAN, nm_jenis_pelayanan 
          )  bb
          where aa.kd_jns_pelayanan = bb.kd_jns_pelayanan";
    $rk = $this->db->query($sql)->result();
    // echo $this->db->last_query();
    $data = array(
      'tgl1' => $tgl1,
      'tgl2' => $tgl2,
      'rk' => $rk,
      'judul' => 'Rekap Jumlah Pelayanan + SPPT ' . date('d M Y', strtotime($tgl1)) . ' sampai ' . date('d M Y', strtotime($tgl2))
    );

    $this->load->view('laporan/execelpermohonanrekap_sppt_layanan', $data);
  }

  function cekbayar()
  {
    $this->template->load('template', 'laporan/cekbayar');
  }



  function cetakpenetapanpajak()
  {
    ini_set("memory_limit", "2G");
    ini_set('max_execution_time', 2000);

    $this->load->library('M_pdf');
    $mpdf       = new mPDF('utf-8', array(297, 377));

    $filename = 'cetak/hasil' . date('Ymdhis') . '.pdf';
    // $filename = 'hasil_'.date('Ymdhis').'.pdf';
    $data['rk'] = $this->db->query("SELECT * FROM DTDHKP")->result();
    $html = $this->load->view('laporan/vcetakpenetapan', $data, true);


    $mpdf->SetDisplayMode('fullpage');
    $mpdf->WriteHTML($html);
    $mpdf->Output($filename);
  }

  function formcetakdhkp()
  {
    $data['tahun'] = $this->db->query("SELECT distinct thn_pajak_sppt FROM DTDHKP")->result();
    $data['kec']   = $this->db->query("select * from REF_KECAMATAN")->result();
    $this->template->load('template', 'laporan/formcetakdhkp', $data);
  }


  function testterbilang()
  {
    $this->load->view('laporan/testtabel');
  }


  function ambilkelurahan()
  {
    $KD_PROPINSI = $_POST['KD_PROPINSI'];
    $KD_DATI2      = $_POST['KD_DATI2'];
    $KD_KECAMATAN  = $_POST['KD_KECAMATAN'];
    $data['rk'] = $this->db->query("SELECT KD_KELURAHAN,NM_KELURAHAN FROM REF_KELURAHAN WHERE KD_PROPINSI='$KD_PROPINSI' AND KD_DATI2='$KD_DATI2' AND KD_KECAMATAN='$KD_KECAMATAN'")->result();
    $this->load->view('laporan/ajaxambilkel', $data);
    // print_r($data);   
  }


  function cetakdhkp()
  {
    if (isset($_POST['sub'])) {
      $THN_PAJAK_SPPT = $_POST['THN_PAJAK_SPPT'];
      $KD_PROPINSI    = $_POST['KD_PROPINSI'];
      $KD_DATI2       = $_POST['KD_DATI2'];
      $KD_KECAMATAN   = $_POST['KD_KECAMATAN'];
      $KD_KELURAHAN   = $_POST['KD_KELURAHAN'];
      $BUKU           = $_POST['BUKU'];

      $vb = implode(',', $BUKU);
      $data['THN_PAJAK_SPPT'] = $THN_PAJAK_SPPT;
      $data['KD_PROPINSI']    = $KD_PROPINSI;
      $data['KD_DATI2']       = $KD_DATI2;
      $data['KD_KECAMATAN']   = $KD_KECAMATAN;
      $data['KD_KELURAHAN']   = $KD_KELURAHAN;
      $data['BUKU']           = $vb;
      $data['rk'] = $this->db->query("SELECT a.KD_BLOK||'.'||a.no_urut nop,nm_wp_sppt nama_wp,jln_wp_sppt||', '|| rt_wp_sppt||' / '||rw_wp_sppt alamat_wp,jalan_op||', '|| rt_op||' / '||rw_op alamat_op, luas_bumi_sppt luas_bumi,luas_bng_sppt luas_bangunan,pbb_yg_harus_dibayar_sppt pbb,buku                                   
                                      FROM (select a.*,decode(greatest(pbb_yg_harus_dibayar_sppt,0),least(pbb_yg_harus_dibayar_sppt,100000),1,null) ||
                                                                          decode(greatest(pbb_yg_harus_dibayar_sppt,100001),least(pbb_yg_harus_dibayar_sppt,500000),2,null) ||
                                                                          decode(greatest(pbb_yg_harus_dibayar_sppt,500001),least(pbb_yg_harus_dibayar_sppt,2000000),3,null) ||
                                                                          decode(greatest(pbb_yg_harus_dibayar_sppt,200001),least(pbb_yg_harus_dibayar_sppt,5000000),4,null) || 
                                                                          decode(greatest(pbb_yg_harus_dibayar_sppt,500001),least(pbb_yg_harus_dibayar_sppt,9999999999999),5,null) buku 
                                                                          from SPPT a 
                                                                          WHERE THN_PAJAK_SPPT='$THN_PAJAK_SPPT'
                                                                          and kd_propinsi='$KD_PROPINSI'
                                                                          and kd_dati2='$KD_DATI2'
                                                                          and kd_kecamatan='$KD_KECAMATAN'
                                                                          and kd_kelurahan='$KD_KELURAHAN'
                                                                          ) a
                                      join dat_objek_pajak b on a.KD_PROPINSI=b.KD_PROPINSI and
                                                                          a.KD_DATI2=b.KD_DATI2 and
                                                                          a.KD_KECAMATAN=b.KD_KECAMATAN and
                                                                          a.KD_KELURAHAN=b.KD_KELURAHAN and
                                                                          a.KD_BLOK=b.KD_BLOK and
                                                                          a.NO_URUT=b.NO_URUT and
                                                                          a.KD_JNS_OP=b.KD_JNS_OP
                                                                          where buku in ($vb)")->result();



      $this->template->load('template', 'laporan/previewdhkp', $data);
    } else {
      redirect('laporan/formcetakdhkp');
    }
  }

  function cetakdhkpoutput($THN_PAJAK_SPPT, $KD_PROPINSI, $KD_DATI2, $KD_KECAMATAN, $KD_KELURAHAN, $BUKU)
  {
    ini_set("memory_limit", "2G");
    ini_set('max_execution_time', 2000);

    $vb = str_replace('_', ',', $BUKU);
    $data['THN_PAJAK_SPPT'] = $THN_PAJAK_SPPT;
    $data['KD_PROPINSI']    = $KD_PROPINSI;
    $data['KD_DATI2']       = $KD_DATI2;
    $data['KD_KECAMATAN']   = $KD_KECAMATAN;
    $data['KD_KELURAHAN']   = $KD_KELURAHAN;
    $data['BUKU']           = $vb;

    $data['rgs'] = $this->db->query("select kd_propinsi, kd_dati2, a.kd_kecamatan, a.kd_kelurahan ,thn_pajak_sppt,jsppt,obj1,obj2,obj3,obj4,obj5,lt1,lt2,lt3,lt4,lt5,lb1,lb2,lb3,lb4,lb5,pbb1,pbb2,pbb3,pbb4,pbb5
                                        from (
                                        select kd_propinsi, kd_dati2, kd_kecamatan, kd_kelurahan ,thn_pajak_sppt,count(*) as jsppt
                                        from sppt
                                        where thn_pajak_sppt = 2017
                                        group by kd_propinsi, kd_dati2,kd_kecamatan, kd_kelurahan,thn_pajak_sppt
                                        ) a,
                                        (
                                        select kd_kecamatan,kd_kelurahan, sum(decode(greatest(pbb_yg_harus_dibayar_sppt,0),least(pbb_yg_harus_dibayar_sppt,100000),1,0)) obj1 ,
                                        sum(decode(greatest(pbb_yg_harus_dibayar_sppt,100001),least(pbb_yg_harus_dibayar_sppt,500000),1,0)) obj2,
                                        sum(decode(greatest(pbb_yg_harus_dibayar_sppt,500001),least(pbb_yg_harus_dibayar_sppt,2000000),1,0)) obj3,
                                        sum(decode(greatest(pbb_yg_harus_dibayar_sppt,200001),least(pbb_yg_harus_dibayar_sppt,5000000),1,0)) obj4, 
                                        sum(decode(greatest(pbb_yg_harus_dibayar_sppt,500001),least(pbb_yg_harus_dibayar_sppt,9999999999999),1,0)) obj5,  
                                        sum(decode(greatest(pbb_yg_harus_dibayar_sppt,0),least(pbb_yg_harus_dibayar_sppt,100000),luas_bumi_sppt,0)) LT1 ,
                                        sum(decode(greatest(pbb_yg_harus_dibayar_sppt,100001),least(pbb_yg_harus_dibayar_sppt,500000),luas_bumi_sppt,0)) LT2,
                                        sum(decode(greatest(pbb_yg_harus_dibayar_sppt,500001),least(pbb_yg_harus_dibayar_sppt,2000000),luas_bumi_sppt,0)) LT3,
                                        sum(decode(greatest(pbb_yg_harus_dibayar_sppt,200001),least(pbb_yg_harus_dibayar_sppt,5000000),luas_bumi_sppt,0)) LT4, 
                                        sum(decode(greatest(pbb_yg_harus_dibayar_sppt,500001),least(pbb_yg_harus_dibayar_sppt,9999999999999),luas_bumi_sppt,0)) LT5,  
                                        sum(decode(greatest(pbb_yg_harus_dibayar_sppt,0),least(pbb_yg_harus_dibayar_sppt,100000),luas_bng_sppt,0)) LB1 ,
                                        sum(decode(greatest(pbb_yg_harus_dibayar_sppt,100001),least(pbb_yg_harus_dibayar_sppt,500000),luas_bng_sppt,0)) LB2,
                                        sum(decode(greatest(pbb_yg_harus_dibayar_sppt,500001),least(pbb_yg_harus_dibayar_sppt,2000000),luas_bng_sppt,0)) LB3,
                                        sum(decode(greatest(pbb_yg_harus_dibayar_sppt,200001),least(pbb_yg_harus_dibayar_sppt,5000000),luas_bng_sppt,0)) LB4, 
                                        sum(decode(greatest(pbb_yg_harus_dibayar_sppt,500001),least(pbb_yg_harus_dibayar_sppt,9999999999999),luas_bng_sppt,0)) LB5,
                                        sum(decode(greatest(pbb_yg_harus_dibayar_sppt,0),least(pbb_yg_harus_dibayar_sppt,100000),pbb_yg_harus_dibayar_sppt,0)) pbb1 ,
                                        sum(decode(greatest(pbb_yg_harus_dibayar_sppt,100001),least(pbb_yg_harus_dibayar_sppt,500000),pbb_yg_harus_dibayar_sppt,0)) pbb2,
                                        sum(decode(greatest(pbb_yg_harus_dibayar_sppt,500001),least(pbb_yg_harus_dibayar_sppt,2000000),pbb_yg_harus_dibayar_sppt,0)) pbb3,
                                        sum(decode(greatest(pbb_yg_harus_dibayar_sppt,200001),least(pbb_yg_harus_dibayar_sppt,5000000),pbb_yg_harus_dibayar_sppt,0)) pbb4, 
                                        sum(decode(greatest(pbb_yg_harus_dibayar_sppt,500001),least(pbb_yg_harus_dibayar_sppt,9999999999999),pbb_yg_harus_dibayar_sppt,0)) pbb5
                                        from sppt
                                        where thn_pajak_sppt ='$THN_PAJAK_SPPT'
                                        and kd_propinsi='$KD_PROPINSI'
                                        and kd_dati2  ='$KD_DATI2'
                                        and kd_kecamatan='$KD_KECAMATAN'
                                        and kd_kelurahan='$KD_KELURAHAN'
                                        group by kd_kecamatan,kd_kelurahan
                                        ) abc
                                        where a.kd_kecamatan = abc.kd_kecamatan
                                        and a.kd_kelurahan = abc.kd_kelurahan")->row();




    $html  = $this->load->view("laporan/vcetakdhkpoutput", $data, true);
    $html2 = $this->load->view("laporan/vcoverdhkp", $data, true);

    $this->load->library('M_pdf');
    $mpdf       = new mPDF('utf-8', array(377, 297));
    $mpdf->setFooter('{PAGENO}');

    // $mpdf->AddPage('','','','','on');
    $mpdf->WriteHTML($html2);
    // $mpdf->AddPage('','','','','on');
    // $mpdf->setFooter('{PAGENO}');
    $mpdf->AddPage();
    // $mpdf->WriteHTML('<pagebreak type="NEXT-ODD" pagenumstyle="1" />');
    $mpdf->WriteHTML($html);
    $mpdf->AddPage();

    $var['hal'] = $mpdf->getPageCount();




    $html3 = $this->load->view("laporan/vcoverbelakangdhkp", array_merge($data, $var), true);
    $mpdf->WriteHTML($html3);
    // $mpdf->AddPage();


    $mpdf->Output();
  }

  function detailpenerimapermohonan($nip, $t1, $t2)
  {
    $data['rk'] = $this->db->query("select * from PV_PERMOHONAN where TGL_TERIMA_DOKUMEN_WP between  to_date('$t1','dd-mm-yyyy') and to_date('$t2','dd-mm-yyyy') and nip_penerima='$nip'")->result();
    $data['nip'] = $nip;
    $this->template->load('template', 'laporan/vdetailpenerima', $data);
  }

  function permohonanSelesai()
  {
    if (isset($_POST['tgl1']) && isset($_POST['tgl2'])) {
      $tgl1       = $_POST['tgl1'];
      $tgl2       = $_POST['tgl2'];
    } else {
      $tgl1 = date('01-m-Y');
      $tgl2 = date('d-m-Y');
    }

    $data['rk'] = $this->db->query("  SELECT  
                                      A.THN_PELAYANAN,
                                        NM_JENIS_PELAYANAN, COUNT(*) JUMLAH,SUM(SELESAI) SELESAI ,SUM(BELUM) BELUM
                                 FROM    PST_PERMOHONAN A
                                      JOIN  (SELECT PST_DETAIL.*,CASE WHEN  GET_STATUS_SELESAI(THN_PELAYANAN,KD_PROPINSI_PEMOHON||KD_DATI2_PEMOHON|| KD_KECAMATAN_PEMOHON||KD_KELURAHAN_PEMOHON||KD_BLOK_PEMOHON||NO_URUT_PEMOHON||KD_JNS_OP_PEMOHON) = 1 THEN 1 ELSE 0 END  SELESAI, CASE WHEN  GET_STATUS_SELESAI(THN_PELAYANAN,KD_PROPINSI_PEMOHON||KD_DATI2_PEMOHON|| KD_KECAMATAN_PEMOHON||KD_KELURAHAN_PEMOHON||KD_BLOK_PEMOHON||NO_URUT_PEMOHON||KD_JNS_OP_PEMOHON) != 1 THEN 1 ELSE 0 END  BELUM
                                                FROM PST_DETAIL) B
                                      ON     A.KD_KANWIL = B.KD_KANWIL
                                         AND A.KD_KANTOR = B.KD_KANTOR
                                         AND A.THN_PELAYANAN = B.THN_PELAYANAN
                                         AND A.BUNDEL_PELAYANAN = B.BUNDEL_PELAYANAN
                                         AND A.NO_URUT_PELAYANAN = B.NO_URUT_PELAYANAN
                                         JOIN REF_JNS_PELAYANAN C ON C.KD_JNS_PELAYANAN=B.KD_JNS_PELAYANAN
                                         WHERE TGL_SURAT_PERMOHONAN  BETWEEN TO_DATE('$tgl1','dd-mm-yyyy') AND   TO_DATE('$tgl2','dd-mm-yyyy')  
                                            GROUP BY  NM_JENIS_PELAYANAN,A.THN_PELAYANAN
                                            ORDER BY NM_JENIS_PELAYANAN ASC")->result();
    $data['tgl1'] = $tgl1;
    $data['tgl2'] = $tgl2;
    $this->template->load('template', 'laporan/vpermohonanselesai', $data);
  }

  function permohonanselesaiexcel()
  {
    $tgl1 = urldecode($this->input->get('t1'));
    $tgl2 = urldecode($this->input->get('t2'));

    $data['rk'] = $this->db->query("  SELECT  
                                      A.THN_PELAYANAN,
                                        NM_JENIS_PELAYANAN, COUNT(*) JUMLAH,SUM(SELESAI) SELESAI ,SUM(BELUM) BELUM
                                 FROM    PST_PERMOHONAN A
                                      JOIN  (SELECT PST_DETAIL.*,CASE WHEN  GET_STATUS_SELESAI(THN_PELAYANAN,KD_PROPINSI_PEMOHON||KD_DATI2_PEMOHON|| KD_KECAMATAN_PEMOHON||KD_KELURAHAN_PEMOHON||KD_BLOK_PEMOHON||NO_URUT_PEMOHON||KD_JNS_OP_PEMOHON) = 1 THEN 1 ELSE 0 END  SELESAI, CASE WHEN  GET_STATUS_SELESAI(THN_PELAYANAN,KD_PROPINSI_PEMOHON||KD_DATI2_PEMOHON|| KD_KECAMATAN_PEMOHON||KD_KELURAHAN_PEMOHON||KD_BLOK_PEMOHON||NO_URUT_PEMOHON||KD_JNS_OP_PEMOHON) != 1 THEN 1 ELSE 0 END  BELUM
                                                FROM PST_DETAIL) B
                                      ON     A.KD_KANWIL = B.KD_KANWIL
                                         AND A.KD_KANTOR = B.KD_KANTOR
                                         AND A.THN_PELAYANAN = B.THN_PELAYANAN
                                         AND A.BUNDEL_PELAYANAN = B.BUNDEL_PELAYANAN
                                         AND A.NO_URUT_PELAYANAN = B.NO_URUT_PELAYANAN
                                         JOIN REF_JNS_PELAYANAN C ON C.KD_JNS_PELAYANAN=B.KD_JNS_PELAYANAN
                                         WHERE TGL_SURAT_PERMOHONAN  BETWEEN TO_DATE('$tgl1','dd-mm-yyyy') AND   TO_DATE('$tgl2','dd-mm-yyyy')  
                                            GROUP BY  NM_JENIS_PELAYANAN,A.THN_PELAYANAN
                                            ORDER BY NM_JENIS_PELAYANAN ASC")->result();

    // print_r($data);
    $this->load->view('laporan/excelselesaipermohonan', $data);
  }

  function detailselesai()
  {
    ini_set("memory_limit", "2G");
    ini_set('max_execution_time', 90000);

    // $res =$this->input->get();


    $t1  = str_replace("'", '', $this->input->get('t1', true));
    $t2  = str_replace("'", '', $this->input->get('t2', true));
    $th  = $this->input->get('thn', true);
    $jns = $this->input->get('jns', true);

    $this->db->select("A.THN_PELAYANAN || '.'|| A.BUNDEL_PELAYANAN || '.'|| A.NO_URUT_PELAYANAN NO_LAYANAN, KD_PROPINSI_PEMOHON || '.'|| KD_DATI2_PEMOHON || '.'|| KD_KECAMATAN_PEMOHON || '.'|| KD_KELURAHAN_PEMOHON || '.'|| KD_BLOK_PEMOHON || '.'|| NO_URUT_PEMOHON || '.'|| KD_JNS_OP_PEMOHON NOP, TO_CHAR (TGL_SURAT_PERMOHONAN, 'yyyy-mm-dd') TGL_SURAT_PERMOHONAN, TGL_TERIMA_DOKUMEN_WP, TGL_PERKIRAAN_SELESAI, TO_CHAR (TGL_TERIMA_DOKUMEN_WP, 'dd/mm/yyyy') TGL_TERIMA, TO_CHAR (TGL_PERKIRAAN_SELESAI, 'dd/mm/yyyy') TGL_SELESAI, CASE STATUS_KOLEKTIF WHEN '0' THEN 'Individual'ELSE 'Kolektif/Masal'END STATUS_KOLEKTIF, A.THN_PELAYANAN, NIP_PENERIMA, STATUS_SELESAI, NM_JENIS_PELAYANAN, GET_STATUS_SELESAI (B.THN_PELAYANAN, KD_PROPINSI_PEMOHON || KD_DATI2_PEMOHON || KD_KECAMATAN_PEMOHON || KD_KELURAHAN_PEMOHON || KD_BLOK_PEMOHON || NO_URUT_PEMOHON || KD_JNS_OP_PEMOHON) stt");
    $this->db->from('PST_PERMOHONAN A');
    $this->db->join('PST_DETAIL B', "  A.KD_KANWIL = B.KD_KANWIL AND A.KD_KANTOR = B.KD_KANTOR AND A.THN_PELAYANAN = B.THN_PELAYANAN AND A.BUNDEL_PELAYANAN = B.BUNDEL_PELAYANAN AND A.NO_URUT_PELAYANAN = B.NO_URUT_PELAYANAN");
    $this->db->join('REF_JNS_PELAYANAN C', "C.KD_JNS_PELAYANAN = B.KD_JNS_PELAYANAN");
    $this->db->where("KD_PROPINSI_PEMOHON || '.'|| KD_DATI2_PEMOHON || '.'|| KD_KECAMATAN_PEMOHON || '.'|| KD_KELURAHAN_PEMOHON || '.'|| KD_BLOK_PEMOHON || '.'|| NO_URUT_PEMOHON || '.'|| KD_JNS_OP_PEMOHON IN (  SELECT    KD_PROPINSI || '.'|| KD_DATI2 || '.'|| KD_KECAMATAN || '.'|| KD_KELURAHAN || '.'|| KD_BLOK || '.'|| NO_URUT || '.'|| KD_JNS_OP FROM SPPT GROUP BY    KD_PROPINSI || '.'|| KD_DATI2 || '.'|| KD_KECAMATAN || '.'|| KD_KELURAHAN || '.'|| KD_BLOK || '.'|| NO_URUT || '.'|| KD_JNS_OP)", "", false);
    $this->db->where("TGL_SURAT_PERMOHONAN BETWEEN TO_DATE ('$t1', 'dd-mm-yyyy')
                                    AND TO_DATE ('$t2', 'dd-mm-yyyy')", "", false);
    $this->db->where("NM_JENIS_PELAYANAN", $jns);
    $this->db->where("A.THN_PELAYANAN ", $th);
    $resd = $this->db->get()->result();
    $data['rk'] = $resd;
    /*echo $this->db->last_query();
      die();*/
    $data['title'] = "Detail Permohonan " . $jns;
    $this->template->load('template', 'laporan/popdetail', $data);
  }


  function detailbelumselesai()
  {
    ini_set("memory_limit", "2G");
    ini_set('max_execution_time', 90000);

    /*$th   =$res['thn'];
        $t1 =$res['t1'];
        $t2 =$res['t2'];
        $jns  =$res['jns'];*/



    $t1  = str_replace("'", '', $this->input->get('t1', true));
    $t2  = str_replace("'", '', $this->input->get('t2', true));
    $th  = $this->input->get('thn', true);
    $jns = $this->input->get('jns', true);


    $data['rk'] = $this->db->query("SELECT    A.THN_PELAYANAN || '.' || A.BUNDEL_PELAYANAN|| '.'|| A.NO_URUT_PELAYANAN NO_LAYANAN, KD_PROPINSI_PEMOHON||'.'||KD_DATI2_PEMOHON||'.'|| KD_KECAMATAN_PEMOHON||'.'||KD_KELURAHAN_PEMOHON||'.'||KD_BLOK_PEMOHON||'.'||NO_URUT_PEMOHON||'.'||KD_JNS_OP_PEMOHON NOP,to_char(TGL_SURAT_PERMOHONAN,'yyyy-mm-dd') TGL_SURAT_PERMOHONAN,  TGL_TERIMA_DOKUMEN_WP, TGL_PERKIRAAN_SELESAI,
                                      TO_CHAR (TGL_TERIMA_DOKUMEN_WP, 'dd/mm/yyyy') TGL_TERIMA,
                                      TO_CHAR (TGL_PERKIRAAN_SELESAI, 'dd/mm/yyyy') TGL_SELESAI,
                                      CASE STATUS_KOLEKTIF
                                         WHEN '0' THEN 'Individual'
                                         ELSE 'Kolektif/Masal'
                                      END
                                         STATUS_KOLEKTIF,
                                      A.THN_PELAYANAN,
                                      NIP_PENERIMA,STATUS_SELESAI,NM_JENIS_PELAYANAN
                                 FROM    PST_PERMOHONAN A
                                      JOIN                    PST_DETAIL B
                                      ON     A.KD_KANWIL = B.KD_KANWIL
                                         AND A.KD_KANTOR = B.KD_KANTOR
                                         AND A.THN_PELAYANAN = B.THN_PELAYANAN
                                         AND A.BUNDEL_PELAYANAN = B.BUNDEL_PELAYANAN
                                         AND A.NO_URUT_PELAYANAN = B.NO_URUT_PELAYANAN
                                         JOIN REF_JNS_PELAYANAN C ON C.KD_JNS_PELAYANAN=B.KD_JNS_PELAYANAN
                                         WHERE KD_PROPINSI_PEMOHON||'.'||KD_DATI2_PEMOHON||'.'|| KD_KECAMATAN_PEMOHON||'.'||KD_KELURAHAN_PEMOHON||'.'||KD_BLOK_PEMOHON||'.'||NO_URUT_PEMOHON||'.'||KD_JNS_OP_PEMOHON NOT IN (
                                            SELECT   KD_PROPINSI||'.'||KD_DATI2||'.'|| KD_KECAMATAN||'.'||KD_KELURAHAN||'.'||KD_BLOK||'.'||NO_URUT||'.'||KD_JNS_OP FROM SPPT
                                            GROUP BY KD_PROPINSI||'.'||KD_DATI2||'.'|| KD_KECAMATAN||'.'||KD_KELURAHAN||'.'||KD_BLOK||'.'||NO_URUT||'.'||KD_JNS_OP ) AND TGL_SURAT_PERMOHONAN  BETWEEN TO_DATE('$t1','dd-mm-yyyy') and   to_date('$t2','dd-mm-yyyy') and NM_JENIS_PELAYANAN='$jns' and A.THN_PELAYANAN='$th'
                                            ")->result();
    $data['title'] = "Permohonan Belum Selesai";

    $this->load->view('laporan/popdetail', $data);
  }

  function permohonanbelumSelesai()
  {
    if (isset($_POST['tgl1']) && isset($_POST['tgl2'])) {
      $tgl1       = $_POST['tgl1'];
      $tgl2       = $_POST['tgl2'];
    } else {
      $tgl1 = date('01-m-Y');
      $tgl2 = date('d-m-Y');
    }

    $data['rk'] = $this->db->query("SELECT  
                                      A.THN_PELAYANAN,
                                        NM_JENIS_PELAYANAN, COUNT(*) JUMLAH
                                      FROM    PST_PERMOHONAN A
                                      JOIN PST_DETAIL B
                                      ON     A.KD_KANWIL = B.KD_KANWIL
                                         AND A.KD_KANTOR = B.KD_KANTOR
                                         AND A.THN_PELAYANAN = B.THN_PELAYANAN
                                         AND A.BUNDEL_PELAYANAN = B.BUNDEL_PELAYANAN
                                         AND A.NO_URUT_PELAYANAN = B.NO_URUT_PELAYANAN
                                         JOIN REF_JNS_PELAYANAN C ON C.KD_JNS_PELAYANAN=B.KD_JNS_PELAYANAN
                                         WHERE KD_PROPINSI_PEMOHON||'.'||KD_DATI2_PEMOHON||'.'|| KD_KECAMATAN_PEMOHON||'.'||KD_KELURAHAN_PEMOHON||'.'||KD_BLOK_PEMOHON||'.'||NO_URUT_PEMOHON||'.'||KD_JNS_OP_PEMOHON NOT IN (
                                            SELECT   KD_PROPINSI||'.'||KD_DATI2||'.'|| KD_KECAMATAN||'.'||KD_KELURAHAN||'.'||KD_BLOK||'.'||NO_URUT||'.'||KD_JNS_OP FROM SPPT
                                            GROUP BY KD_PROPINSI||'.'||KD_DATI2||'.'|| KD_KECAMATAN||'.'||KD_KELURAHAN||'.'||KD_BLOK||'.'||NO_URUT||'.'||KD_JNS_OP ) AND TGL_SURAT_PERMOHONAN  BETWEEN TO_DATE('$tgl1','dd-mm-yyyy') AND   TO_DATE('$tgl2','dd-mm-yyyy')
                                            GROUP BY  NM_JENIS_PELAYANAN,A.THN_PELAYANAN
                                            ORDER BY NM_JENIS_PELAYANAN ASC")->result();

    $data['tgl1'] = $tgl1;
    $data['tgl2'] = $tgl2;
    $this->template->load('template', 'laporan/vpermohonanselesaibelum', $data);
  }

  function permohonanselesaibelumexcel()
  {
    $tgl1 = urldecode($this->input->get('t1'));
    $tgl2 = urldecode($this->input->get('t2'));

    $data['rk'] = $this->db->query("SELECT  
                                      A.THN_PELAYANAN,
                                        NM_JENIS_PELAYANAN, COUNT(*) JUMLAH
                                      FROM    PST_PERMOHONAN A
                                      JOIN PST_DETAIL B
                                      ON     A.KD_KANWIL = B.KD_KANWIL
                                         AND A.KD_KANTOR = B.KD_KANTOR
                                         AND A.THN_PELAYANAN = B.THN_PELAYANAN
                                         AND A.BUNDEL_PELAYANAN = B.BUNDEL_PELAYANAN
                                         AND A.NO_URUT_PELAYANAN = B.NO_URUT_PELAYANAN
                                         JOIN REF_JNS_PELAYANAN C ON C.KD_JNS_PELAYANAN=B.KD_JNS_PELAYANAN
                                         WHERE KD_PROPINSI_PEMOHON||'.'||KD_DATI2_PEMOHON||'.'|| KD_KECAMATAN_PEMOHON||'.'||KD_KELURAHAN_PEMOHON||'.'||KD_BLOK_PEMOHON||'.'||NO_URUT_PEMOHON||'.'||KD_JNS_OP_PEMOHON NOT IN (
                                            SELECT   KD_PROPINSI||'.'||KD_DATI2||'.'|| KD_KECAMATAN||'.'||KD_KELURAHAN||'.'||KD_BLOK||'.'||NO_URUT||'.'||KD_JNS_OP FROM SPPT
                                            GROUP BY KD_PROPINSI||'.'||KD_DATI2||'.'|| KD_KECAMATAN||'.'||KD_KELURAHAN||'.'||KD_BLOK||'.'||NO_URUT||'.'||KD_JNS_OP ) AND TGL_SURAT_PERMOHONAN  BETWEEN TO_DATE('$tgl1','dd-mm-yyyy') AND   TO_DATE('$tgl2','dd-mm-yyyy')
                                            GROUP BY  NM_JENIS_PELAYANAN,A.THN_PELAYANAN
                                            ORDER BY NM_JENIS_PELAYANAN ASC")->result();

    // print_r($data);
    $this->load->view('laporan/excelselesaibelumpermohonan', $data);
  }

  function rekap_kode_pengajuan()
  {
    if (isset($_POST['tgl1']) && isset($_POST['tgl2'])) {
      $tgl1       = $_POST['tgl1'];
      $tgl2       = $_POST['tgl2'];
    } else {
      $tgl1 = date('01-m-Y');
      $tgl2 = date('d-m-Y');
    }
    $kg = $this->session->userdata('pbb_kg');
    $data['rk'] = $this->db->query("select kd_pengajuan,nm_pengajuan,count(*) jum 
                                      from pv_rekap_kd_pengajuan
                                      where tgl_terima_dokumen_wp between  to_date('$tgl1','dd-mm-yyyy') and to_date('$tgl2','dd-mm-yyyy')  
                                      group by kd_pengajuan,nm_pengajuan order by kd_pengajuan asc")->result();
    $data['tgl1'] = $tgl1;
    $data['tgl2'] = $tgl2;
    $this->template->load('template', 'laporan/rekap_kode_pengajuan', $data);
  }

  function lapPembayaranBank()
  {

    $kec = $this->db->query("SELECT KD_KECAMATAN,NM_KECAMATAN FROM REF_KECAMATAN ORDER BY KD_KECAMATAN ASC")->result();
    $KD_KECAMATAN = $this->input->post('KD_KECAMATAN', true);
    $KD_KELURAHAN = $this->input->post('KD_KELURAHAN', true);
    $KD_BANK = $this->input->post('bank', true);
    $kode_cari = $this->input->post('cari', true);

    $where = "";
    $where_get = "";
    $and = "";

    if (isset($_POST['tgl1']) && isset($_POST['tgl2'])) {
      $tgl1       = $_POST['tgl1'];
      $tgl2       = $_POST['tgl2'];
      $where .= " TGL_PEMBAYARAN_SPPT BETWEEN TO_DATE('$tgl1','dd-mm-yyyy') AND TO_DATE('$tgl2','dd-mm-yyyy')";
      $where_get .= "tgl1=" . $tgl1 . "&tgl2=" . $tgl2;
    } else {
      $tgl1 = date('d-m-Y');
      $tgl2 = date('d-m-Y');
      $where .= " TGL_PEMBAYARAN_SPPT BETWEEN TO_DATE('$tgl1','dd-mm-yyyy') AND TO_DATE('$tgl2','dd-mm-yyyy')";
      $where_get .= "&tgl1=" . $tgl1 . "&tgl2=" . $tgl2;
    }

    if (isset($_POST['tahun'])) {
      $tahun = $_POST['tahun'];
      if ($tahun == 'all') {
        $where .= "";
        $where_get .= "&tahun=" . $tahun;
      } else {
        $where .= " AND THN_PAJAK_SPPT='" . $tahun . "'";
        $where_get .= "&tahun=" . $tahun;
      }
    } else {
      $tahun = date('Y');
      $where .= " AND THN_PAJAK_SPPT='" . $tahun . "'";
      $where_get .= "tahun=" . $tahun;
    }


    if ($KD_KECAMATAN <> '') {
      $where .= " AND KD_KECAMATAN='" . $KD_KECAMATAN . "'";
      $where_get .= "&kec=" . $KD_KECAMATAN;
    }

    if ($KD_KELURAHAN <> '') {
      $where .= " AND KD_KELURAHAN='" . $KD_KELURAHAN . "'";
      $where_get .= "&kel=" . $KD_KELURAHAN;
    }

    if ($KD_BANK <> '') {

      if ($KD_BANK == '114') {
        $where .= " AND KODE_BANK_BAYAR IS NULL";
        $where_get .= "&bank=" . $KD_BANK;
      } else {
        $where .= " AND KODE_BANK_BAYAR='" . $KD_BANK . "'";
        $where_get .= "&bank=" . $KD_BANK;
      }
    }

    if ($KD_KECAMATAN <> '') {
      $kel = $this->db->query("select kd_kelurahan,nm_kelurahan 
                                    from ref_kelurahan
                                    where kd_kecamatan='$KD_KECAMATAN' order by kd_kelurahan asc")->result();
    } else {
      $kel = null;
    }

    //   echo "SELECT * FROM pembayaran_sppt@tospo14 WHERE $where";
    // exit();

    $bank = $this->db->query("SELECT * FROM REF_BANK WHERE STATUS=1 ORDER BY NAMA_BANK ASC")->result();

    if (isset($kode_cari)) {
      $query = $this->db->query("SELECT a.*, to_char(TGL_PEMBAYARAN_SPPT,'DD-MM-YYYY') as TANGGAL_BAYAR, to_char(TGL_REKAM_BYR_SPPT,'DD-MM-YYYY HH:MI:SS') as TANGGAL_REKAM  FROM MV_PEMBAYARAN_BANK@to17 a WHERE $where")->result();

      $data = array(
        'rk'           => $query,
        'tahun'        => $tahun,
        'kec'          => $kec,
        'kel'          => $kel,
        'KD_KELURAHAN' => $KD_KELURAHAN,
        'KD_KECAMATAN' => $KD_KECAMATAN,
        'where'        => $where_get,
        'bank'         => $bank,
        'KD_BANK'      => $KD_BANK,
        'tgl1'         => $tgl1,
        'tgl2'         => $tgl2
      );
    } else {

      $data = array(
        'tahun'        => $tahun,
        'kec'          => $kec,
        'kel'          => $kel,
        'KD_KELURAHAN' => $KD_KELURAHAN,
        'KD_KECAMATAN' => $KD_KECAMATAN,
        'bank'         => $bank,
        'KD_BANK'      => $KD_BANK,
        'tgl1'         => $tgl1,
        'tgl2'         => $tgl2
      );
    }

    $this->template->load('template', 'laporan/lappembayaranbank', $data);
  }

  function excellappermohonanbank()
  {
    $kec = $this->input->get('kec', true);
    $kel = $this->input->get('kel', true);
    $tahun = $this->input->get('tahun', true);
    $tgl1 = $this->input->get('tgl1', true);
    $tgl2 = $this->input->get('tgl2', true);
    $bank = $this->input->get('bank', true);

    $where = "";

    if ($tgl1 <> '' && $tgl2 <> '') {
      $where .= " TGL_PEMBAYARAN_SPPT BETWEEN TO_DATE('$tgl1','dd-mm-yyyy') AND TO_DATE('$tgl2','dd-mm-yyyy')";
    }

    if ($tahun <> '') {
      if ($tahun == 'all') {
        $where .= "";
      } else {
        $where .= " AND THN_PAJAK_SPPT='" . $tahun . "'";
      }
    }

    if ($kec <> '') {
      $where .= " AND KD_KECAMATAN='" . $kec . "'";
    }

    if ($kel <> '') {
      $where .= " AND KD_KELURAHAN='" . $kel . "'";
    }

    $nama_bank = "Semua Bank";
    if ($bank <> '') {

      if ($bank == 114) {
        $where .= " AND KODE_BANK_BAYAR IS NULL";
        $cb = $this->db->query("SELECT NAMA_BANK FROM REF_BANK WHERE KODE_BANK=$bank")->row();
        $nama_bank = $cb->NAMA_BANK;
      } else {
        $where .= " AND KODE_BANK_BAYAR='" . $bank . "'";
        $cb = $this->db->query("SELECT NAMA_BANK FROM REF_BANK WHERE KODE_BANK=$bank")->row();
        $nama_bank = $cb->NAMA_BANK;
      }
    }


    $rk = $this->db->query("SELECT a.*, to_char(TGL_PEMBAYARAN_SPPT,'DD-MM-YYYY') as TANGGAL_BAYAR, to_char(TGL_REKAM_BYR_SPPT,'DD-MM-YYYY HH:MI:SS') as TANGGAL_REKAM  FROM MV_PEMBAYARAN_BANK@to17 a WHERE $where")->result();
    // echo $this->db->last_query();
    $data = array(
      'rk' => $rk,
      'judul' => 'Laporan Pembayaran ' . $nama_bank,
    );

    $this->load->view('laporan/excellappermohonanbank', $data);
  }


  function pendataanpenetapan()
  {
    $data = [];
    $this->template->load('template', 'laporan/vpendataanpenetapan', $data);
  }

  function jsonpendataanpenetapan()
  {
    $sql = "SELECT B.KD_PROPINSI,
B.KD_DATI2,
B.KD_KECAMATAN,
B.KD_KELURAHAN,
B.KD_BLOK,
B.NO_URUT,
B.KD_JNS_OP,GET_KELURAHAN(SUBSTR (A.NOP_PROSES, 8, 3),SUBSTR (A.NOP_PROSES, 5, 3)) KELURAHAN,GET_KECAMATAN(SUBSTR (A.NOP_PROSES, 5, 3)) KECAMATAN, NM_WP,TGL_PENDATAAN_OP PENDATAAN,TGL_TERBIT_SPPT PENETAPAN
FROM TBL_SPOP@TO17 A
LEFT JOIN SPPT B ON B.KD_PROPINSI = SUBSTR (A.NOP_PROSES, 1, 2)
               AND B.KD_DATI2 = SUBSTR (A.NOP_PROSES, 3, 2)
               AND B.KD_KECAMATAN = SUBSTR (A.NOP_PROSES, 5, 3)
               AND B.KD_KELURAHAN = SUBSTR (A.NOP_PROSES, 8, 3)
               AND B.KD_BLOK = SUBSTR (A.NOP_PROSES, 11, 3)
               AND B.NO_URUT = SUBSTR (A.NOP_PROSES, 14, 4)
               AND B.KD_JNS_OP = SUBSTR (A.NOP_PROSES, 18, 1)";

    $this->datatables->select("KD_PROPINSI, KD_DATI2, KD_KECAMATAN, KD_KELURAHAN, KD_BLOK, NO_URUT, KD_JNS_OP, KELURAHAN, KECAMATAN, NM_WP, PENDATAAN, PENETAPAN");
    $this->datatables->from("($sql)");
    $this->datatables->where("to_char(pendataan,'yyyy')=to_char(sysdate,'yyyy')");
    echo $this->datatables->generate();
  }


  function excelpenetapan()
  {
    //ambil data
    $sql = "SELECT B.KD_PROPINSI,
B.KD_DATI2,
B.KD_KECAMATAN,
B.KD_KELURAHAN,
B.KD_BLOK,
B.NO_URUT,
B.KD_JNS_OP,GET_KELURAHAN(SUBSTR (A.NOP_PROSES, 8, 3),SUBSTR (A.NOP_PROSES, 5, 3)) KELURAHAN,GET_KECAMATAN(SUBSTR (A.NOP_PROSES, 5, 3)) KECAMATAN, NM_WP,TGL_PENDATAAN_OP PENDATAAN,TGL_TERBIT_SPPT PENETAPAN
FROM TBL_SPOP@TO17 A
LEFT JOIN SPPT B ON B.KD_PROPINSI = SUBSTR (A.NOP_PROSES, 1, 2)
               AND B.KD_DATI2 = SUBSTR (A.NOP_PROSES, 3, 2)
               AND B.KD_KECAMATAN = SUBSTR (A.NOP_PROSES, 5, 3)
               AND B.KD_KELURAHAN = SUBSTR (A.NOP_PROSES, 8, 3)
               AND B.KD_BLOK = SUBSTR (A.NOP_PROSES, 11, 3)
               AND B.NO_URUT = SUBSTR (A.NOP_PROSES, 14, 4)
               AND B.KD_JNS_OP = SUBSTR (A.NOP_PROSES, 18, 1)";
    $get  = $this->db->query($sql)->result();


    $writer = WriterFactory::create(Type::XLSX);
    // set style untuk header
    $headerStyle = (new StyleBuilder())
      ->setFontBold()
      ->build();
    $namaFile = date('YmdHis') . '_data_penetapan.xlsx'; //nama filenya
    $filePath = 'tmp/' . $namaFile;

    $defaultStyle = (new StyleBuilder())
      ->setFontName('Arial')
      ->setFontSize(12)
      ->setShouldWrapText(false)
      ->build();
    $writer->setDefaultRowStyle($defaultStyle)
      ->openToFile($filePath);

    // $writer->openToBrowser("data_penetapan.xlsx");
    //silahkan sobat sesuaikan dengan data yang ingin sobat tampilkan
    $header = [
      'NOP',
      'Kelurahan',
      'Kecamatan',
      'Nama WP',
      'Pendataan',
      'Penetapan',
    ];

    $writer->addRow($header); //tambah row untuk header data

    $data   = array(); //siapkan variabel array untuk menampung data
    $no     = 1;

    //looping pembacaan data
    foreach ($get as $key) {
      //masukkan data dari database ke variabel array
      //silahkan sobat sesuaikan dengan nama field pada tabel database
      $anggota    = array(
        // $key->KD_JNS_OP,
        $key->KD_PROPINSI . '.' . $key->KD_DATI2 . '.' . $key->KD_KECAMATAN . '.' . $key->KD_KELURAHAN . '.' . $key->KD_BLOK . '.' . $key->NO_URUT . '.' . $key->KD_JNS_OP,
        $key->KELURAHAN,
        $key->KECAMATAN,
        $key->NM_WP,
        $key->PENDATAAN,
        $key->PENETAPAN,
      );

      array_push($data, $anggota); //masukkan variabel array anggota ke variabel array data
    }

    $writer->addRows($data); // tambahkan row untuk data anggota

    $writer->close();
    $this->load->helper('download');
    force_download($filePath, null);
  }
}

/* End of file Group.php */
/* Location: ./application/controllers/Group.php */
/* Generated by Mohamad Wahyu Dewantoro 2017-04-28 01:25:45 */
