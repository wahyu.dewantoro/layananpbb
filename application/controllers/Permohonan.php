<?php

if (!defined('BASEPATH'))
	exit('No direct script access allowed');

class Permohonan extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		if ($this->session->userdata('pbb') <> 1) {
			$this->session->set_flashdata('notif', '<div class="badge">
                    Silahkan login dengan username dan password anda.</p>
                    </div>');
			redirect('auth');
		}
		$this->load->model('Mpermohonan');
		$this->load->library('form_validation');
		$this->load->library('datatables');
		$this->kg = $this->session->userdata('pbb_kg');
		$this->load->model('Mmaster');
	}

	public function index()
	{
		if (isset($_POST['p_tanggal1']) && isset($_POST['p_tanggal2'])) {
			$tanggal1 = $_POST['p_tanggal1'];
			$tanggal2 = $_POST['p_tanggal2'];
		} else {
			$tanggal1 = date('d-m-Y');
			$tanggal2 = date('d-m-Y');
		}
		$sess = array(
			'p_tanggal1' => $tanggal1,
			'p_tanggal2' => $tanggal2
		);
		$this->session->set_userdata($sess);

		$this->template->load('template', 'permohonan/permohonan_list');
	}

	function dataNJOP()
	{
		if (isset($_POST['p_tanggal1']) && isset($_POST['p_tanggal2'])) {
			$tanggal1 = $_POST['p_tanggal1'];
			$tanggal2 = $_POST['p_tanggal2'];
		} else {
			$tanggal1 = date('d-m-Y');
			$tanggal2 = date('d-m-Y');
		}

		$data['p_tanggal1'] = $tanggal1;
		$data['p_tanggal2'] = $tanggal2;


		$this->template->load('template', 'permohonan/dataNJOP', $data);
	}

	public function json()
	{
		header('Content-Type: application/json');
		echo $this->Mpermohonan->json();
	}

	function jsonNjop()
	{
		header('Content-Type: application/json');
		$data = array('p_tanggal1' => $this->input->post('p_tanggal1'), 'p_tanggal2' => $this->input->post('p_tanggal2'));
		echo $this->Mpermohonan->jsonNjop($data);
	}


	function dataNJOPexcel()
	{
		$t1 = $this->input->get('t1');
		$t2 = $this->input->get('t2');

		$this->db->select("NOPEL,NOP,TGL_PERMOHONAN,NAMA_PEMOHON,NM_WP_SPPT");
		$this->db->from("(SELECT THN_PELAYANAN || '.' || BUNDEL_PELAYANAN || '.' || NO_URUT_PELAYANAN
		NOPEL, KD_PROPINSI_PEMOHON       || '.'
	 || KD_DATI2_PEMOHON
	 || '.'
	 || KD_KECAMATAN_PEMOHON
	 || '.'
	 || KD_KELURAHAN_PEMOHON
	 || '.'
	 || KD_BLOK_PEMOHON
	 || '.'
	 || NO_URUT_PEMOHON
	 || '.'
	 || KD_JNS_OP_PEMOHON
		NOP,
	 TGL_SURAT_PERMOHONAN TGL_PERMOHONAN,
	 NAMA_PEMOHON,
	 NM_WP_SPPT
FROM (SELECT *
		FROM VSK_NJOP
	   WHERE TGL_SURAT_PERMOHONAN BETWEEN TO_DATE ('$t1',
												   'dd-mm-yyyy')
									  AND TO_DATE ('$t2',
												   'dd-mm-yyyy')))");
		$sql = $this->db->get()->result();



		$html = "<table>
				<tr>
				<th>Tanggal</th>
					<th>Nopel</th>
					<th>Nama Kuasa</th>
					<th>Nama Subjek Pajak</th>
					<th>Nop</th>
				</tr>";
		$tmp = "";
		$no = 1;
		foreach ($sql as $rk) {
			if ($tmp == $rk->NOPEL) {
				$t = 'sama';
				++$no;
			} else {
				$t = 'tidak';
				$no = 1;
			}
			$html .= "<tr>
					<td>" . $rk->TGL_PERMOHONAN . "</td>
					<td>" . $rk->NOPEL . "(" . $no . ")</td>
					<td>" . $rk->NAMA_PEMOHON . "</td>
					<td>" . $rk->NM_WP_SPPT . "</td>
					<td>" . $rk->NOP . "</td>
				</tr>";

			$tmp = $rk->NOPEL;
		}

		$html .= "</table>";
		header("Content-type: application/vnd-ms-excel");
		header("Content-Disposition: attachment; filename=SK_NJOP.xls");
		echo $html;
	}


	public function create()
	{
		$rk   = $this->Mpermohonan->getNoLayanan();
		$rn   = $this->Mpermohonan->getNopeNJOP();
		$data = array(
			'button'                => 'Form Permohonan',
			'action'                => site_url('permohonan/create_action'),
			'actionc'               => site_url('permohonan/createsknjopaction'),
			'id'                    => set_value('id'),
			'KD_KANWIL'             => set_value('KD_KANWIL'),
			'KD_KANTOR'             => set_value('KD_KANTOR'),
			'THN_PELAYANAN'         => set_value('THN_PELAYANAN', $rk['tahun']),
			'BUNDEL_PELAYANAN'      => set_value('BUNDEL_PELAYANAN', $rk['bundel']),
			'NO_URUT_PELAYANAN'     => set_value('NO_URUT_PELAYANAN', $rk['no_urut']),
			'THN_PELAYANANC'        => set_value('THN_PELAYANANC', $rn['tahun']),
			'BUNDEL_PELAYANANC'     => set_value('BUNDEL_PELAYANANC', $rn['bundel']),
			'NO_URUT_PELAYANANC'    => set_value('NO_URUT_PELAYANANC', $rn['no_urut']),
			'NO_SRT_PERMOHONAN'     => set_value('NO_SRT_PERMOHONAN', '-'),
			'TGL_SURAT_PERMOHONAN'  => set_value('TGL_SURAT_PERMOHONAN', date('d-m-Y')),
			'NAMA_PEMOHON'          => set_value('NAMA_PEMOHON'),
			'ALAMAT_PEMOHON'        => set_value('ALAMAT_PEMOHON'),
			'NAMA_PEMOHONC'         => set_value('NAMA_PEMOHONC'),
			'ALAMAT_PEMOHONC'       => set_value('ALAMAT_PEMOHONC'),
			'KETERANGAN_PST'        => set_value('KETERANGAN_PST'),
			'CATATAN_PST'           => set_value('CATATAN_PST'),
			'STATUS_KOLEKTIF'       => set_value('STATUS_KOLEKTIF'),
			'TGL_TERIMA_DOKUMEN_WP' => set_value('TGL_TERIMA_DOKUMEN_WP'),
			'TGL_PERKIRAAN_SELESAI' => set_value('TGL_PERKIRAAN_SELESAI'),
			'KD_JNS_PELAYANAN'      => set_value('KD_JNS_PELAYANAN'),
			'pelayanan'             => $this->db->query("SELECT * FROM REF_JNS_PELAYANAN ORDER BY TO_NUMBER(KD_JNS_PELAYANAN) ASC")->result(),
			'dl'                    => $this->db->query("SELECT * FROM P_REF_LAMPIRAN")->result()

		);
		$this->template->load('template', 'permohonan/forminputpermohonan', $data);
	}


	function tambahNop($id)
	{
		$rk = $this->Mpermohonan->get_by_id($id);
		if ($rk) {

			$data = array(
				'button'                => 'Tambah NOP',
				'action'                => site_url('permohonan/tambahNOPaction'),
				'id'                    => $id,
				'THN_PELAYANAN'         => set_value('THN_PELAYANAN', $rk->THN_PELAYANAN),
				'BUNDEL_PELAYANAN'      => set_value('BUNDEL_PELAYANAN', $rk->BUNDEL_PELAYANAN),
				'NO_URUT_PELAYANAN'     => set_value('NO_URUT_PELAYANAN', $rk->NO_URUT_PELAYANAN),
				'NO_SRT_PERMOHONAN'     => set_value('NO_SRT_PERMOHONAN', $rk->NO_SRT_PERMOHONAN),
				'TGL_SURAT_PERMOHONAN'  => set_value('TGL_SURAT_PERMOHONAN', $rk->TGL_SURAT_PERMOHONAN),
				'TGL_TERIMA_DOKUMEN_WP' => set_value('TGL_TERIMA_DOKUMEN_WP', $rk->TGL_TERIMA_DOKUMEN_WP),
				'TGL_PERKIRAAN_SELESAI' => set_value('TGL_PERKIRAAN_SELESAI', $rk->TGL_PERKIRAAN_SELESAI),
				'pelayanan'             => $this->db->query("select * from ref_jns_pelayanan order by nm_jenis_pelayanan asc")->result(),

			);
			$this->template->load('template', 'permohonan/tambahNOP', $data);
		}
	}


	function getNopsknjop()
	{
		if (isset($_POST['nop'])) {
			$nop = $_POST['nop'];
			$exp = explode('|', $nop);
			$a = $exp[0];
			$b = $exp[1];
			$c = $exp[2];
			$d = $exp[3];
			$e = $exp[4];
			$f = $exp[5];
			$g = $exp[6];
			/*$rk=$this->db->query("select nm_wp,kelurahan_wp||' '||kota_wp  alamat from dat_objek_pajak a
									join dat_subjek_pajak b on b.SUBJEK_PAJAK_ID=a.SUBJEK_PAJAK_ID
									where kd_propinsi='$a'
									and kd_dati2='$b'
									and kd_kecamatan='$c'
									and kd_kelurahan='$d'
									and kd_blok='$e'
									and no_urut='$f'
									and kd_jns_op='$g'")->row();*/


			$rk = $this->db->query("select thn_pajak_sppt,nm_wp_sppt,kelurahan_wp_sppt||' '||kota_wp_sppt  alamat from sppt where 
                                    kd_propinsi='$a'
									and kd_dati2='$b'
									and kd_kecamatan='$c'
									and kd_kelurahan='$d'
									and kd_blok='$e'
									and no_urut='$f'
									and kd_jns_op='$g'
                                    order by thn_pajak_sppt desc")->row();

			if ($rk) {

				$row['value']          = $nop;
				$row['NAMA_PEMOHON']   = $rk->NM_WP_SPPT;
				$row['ALAMAT_PEMOHON'] = $rk->ALAMAT;

				$row_set[]       = $row;

				echo $return = json_encode($row_set);
				// }
			} else {
				$row['value']          = '';
				$row['NAMA_PEMOHON']   = '';
				$row['ALAMAT_PEMOHON'] = '';

				$row_set[]       = $row;

				echo $return = json_encode($row_set);
			}
		}
	}

	function getNop()
	{
		if (isset($_POST['nop'])) {
			$nop = $_POST['nop'];
			$exp = explode('|', $nop);
			$a = $exp[0];
			$b = $exp[1];
			$c = $exp[2];
			$d = $exp[3];
			$e = $exp[4];
			$f = $exp[5];
			$g = $exp[6];
			$rk = $this->db->query("SELECT NM_WP,
							       JALAN_WP || ' '
							       || CASE
							             WHEN BLOK_KAV_NO_WP IS NOT NULL
							             THEN
							                CASE WHEN BLOK_KAV_NO_WP <> '-' THEN BLOK_KAV_NO_WP END
							          END
							       || CASE WHEN RW_WP IS NOT NULL THEN ' RW ' || RW_WP END
							       || CASE WHEN RT_WP   IS NOT  NULL THEN ' RT '|| RT_WP END  
       || CASE WHEN KELURAHAN_WP  IS NOT NULL THEN ' Desa/Kel. '|| KELURAHAN_WP END  alamat from dat_objek_pajak a
									join dat_subjek_pajak b on b.SUBJEK_PAJAK_ID=a.SUBJEK_PAJAK_ID
									where kd_propinsi='$a'
									and kd_dati2='$b'
									and kd_kecamatan='$c'
									and kd_kelurahan='$d'
									and kd_blok='$e'
									and no_urut='$f'
									and kd_jns_op='$g'")->row();

			if ($rk) {

				$row['value']          = $nop;
				$row['NAMA_PEMOHON']   = $rk->NM_WP;
				$row['ALAMAT_PEMOHON'] = $rk->ALAMAT;

				$row_set[]       = $row;
				$cc = $this->db->query("select count(*) res from PV_PERMOHONANDETAIL where  layanan not in ('SK NJOP','PENDAFTARAN DATA BARU') and nop=replace('$nop','|','.') and tahun=to_char(sysdate, 'YYYY')")->row();
				if ($cc->RES > 0) {
					echo "0";
				} else {
					// cek
					echo $return = json_encode($row_set);
				}
			} else {
				$row['value']          = '';
				$row['NAMA_PEMOHON']   = '';
				$row['ALAMAT_PEMOHON'] = '';

				$row_set[]       = $row;

				// cek
				echo $return = json_encode($row_set);
			}
		}
	}


	function deskripsialert()
	{
		$nop = $_POST['nop'];
		$res = $this->db->query("select 'Sudah mengajukan layanan '||layanan||' dengan nopel '||no_layanan deskripsi  from PV_PERMOHONANDETAIL where  nop=replace('$nop','|','.') and tahun=to_char(sysdate, 'YYYY')")->row();
		echo str_replace('|', '.', $_POST['nop']) . ' ' . $res->DESKRIPSI;
	}

	function updatepermohonanaction()
	{
		$ID                    = $this->input->post('ID', TRUE);
		$NO_SRT_PERMOHONAN     = $this->input->post('NO_SRT_PERMOHONAN', TRUE);
		$TGL_SURAT_PERMOHONAN  = date('Y-m-d', strtotime($this->input->post('TGL_SURAT_PERMOHONAN', TRUE)));

		$CATATAN_PST           = $this->input->post('CATATAN_PST', TRUE);
		$TGL_TERIMA_DOKUMEN_WP = date('Y-m-d', strtotime($this->input->post('TGL_TERIMA_DOKUMEN_WP', TRUE)));
		$TGL_PERKIRAAN_SELESAI = date('Y-m-d', strtotime($this->input->post('TGL_PERKIRAAN_SELESAI', TRUE)));

		$THN_PELAYANAN         = $this->input->post('THN_PELAYANAN', TRUE);
		$BUNDEL_PELAYANAN      = $this->input->post('BUNDEL_PELAYANAN', TRUE);
		$NO_URUT_PELAYANAN     = $this->input->post('NO_URUT_PELAYANAN', TRUE);
		$IP_EMAIL   	  	   = $this->input->post('IP_EMAIL', TRUE);
		$IP_HP 		     	   = $this->input->post('IP_HP', TRUE);

		$sql = "UPDATE PST_PERMOHONAN SET NO_SRT_PERMOHONAN='$NO_SRT_PERMOHONAN' ,  
						TGL_TERIMA_DOKUMEN_WP=TO_DATE('$TGL_TERIMA_DOKUMEN_WP','yyyy-mm-dd'), 
						TGL_PERKIRAAN_SELESAI=TO_DATE('$TGL_PERKIRAAN_SELESAI','yyyy-mm-dd'), 
						TGL_SURAT_PERMOHONAN=TO_DATE('$TGL_SURAT_PERMOHONAN','yyyy-mm-dd'), 
						CATATAN_PST='$CATATAN_PST' 
					WHERE THN_PELAYANAN ='$THN_PELAYANAN' 
					and BUNDEL_PELAYANAN='$BUNDEL_PELAYANAN'
					and NO_URUT_PELAYANAN='$NO_URUT_PELAYANAN'";

		// echo $sql;			
		$this->db->query($sql);
		$this->db->query("UPDATE P_IP SET IP_EMAIL='$IP_EMAIL',IP_HP='$IP_HP' WHERE IP_THN_PELAYANAN ='$THN_PELAYANAN' 
								and IP_BUNDEL_PELAYANAN='$BUNDEL_PELAYANAN'
								and IP_NO_URUT_PELAYANAN='$NO_URUT_PELAYANAN'");
		redirect('permohonan/detail/' . $ID);
	}


	function createsknjopaction()
	{
		$IP       = $this->Mmaster->getIP();
		$IP_EMAIL = $this->input->post('IP_EMAIL', TRUE);
		$IP_HP    = $this->input->post('IP_HP', TRUE);
		$rk       = $this->Mpermohonan->getNopeNJOP();
		$unit_kantor = $this->session->userdata('unit_kantor');
		/*print_r($rk);
			redirect('permohonan/cetaksknjop/'.$rk['tahun'].'.'. $rk['bundel'].'.'. $rk['no_urut']);
			die();*/

		$date     = date('d/m/Y');
		$this->db->set('KD_KANWIL', '01');
		$this->db->set('KD_KANTOR', '01');
		$this->db->set('THN_PELAYANAN', $rk['tahun']);
		$this->db->set('BUNDEL_PELAYANAN', $rk['bundel']);
		$this->db->set('NO_URUT_PELAYANAN', $rk['no_urut']);
		$this->db->set('TANGGAL_PERMOHONAN', "to_date('" . $this->input->post('TANGGAL_PERMOHONANC', TRUE) . "','dd-mm-yyyy')", false);
		$this->db->set('NAMA_PEMOHON', str_replace("'", "", $this->input->post('NAMA_PEMOHONC', TRUE)));
		$this->db->set('ALAMAT_PEMOHON', $this->input->post('ALAMAT_PEMOHONC', TRUE));
		$this->db->set('CATATAN_PEMOHONAN', $this->input->post('CATATANC', TRUE));
		$this->db->set('KD_PROPINSI', $_POST['KD_PROPINSIC']);
		$this->db->set('KD_DATI2', $_POST['KD_DATI2C']);
		$this->db->set('KD_KECAMATAN', $_POST['KD_KECAMATANC']);
		$this->db->set('KD_KELURAHAN', $_POST['KD_KELURAHANC']);
		$this->db->set('KD_BLOK', $_POST['KD_BLOKC']);
		$this->db->set('NO_URUT', $_POST['NO_URUTC']);
		$this->db->set('KD_JNS_OP', $_POST['KD_JNS_OPC']);
		$this->db->set('KD_JNS_PELAYANAN', '17');
		$this->db->set('TGL_INSERT', "to_date('$date','dd-mm-yyyy')", false);
		$this->db->set('NIP_INSERT', $this->session->userdata('nip'));
		$this->db->insert('P_SKNJOP');

		$this->db->query("INSERT INTO P_IP (IP_KD_KANWIL, IP_KD_KANTOR, IP_THN_PELAYANAN, IP_BUNDEL_PELAYANAN, IP_NO_URUT_PELAYANAN, IP_PELAYANAN,IP_EMAIL,IP_HP,UNIT_KANTOR) 
							  VALUES ('01', '01', '" . $rk['tahun'] . "', '" . $rk['bundel'] . "', '" . $rk['no_urut'] . "', '$IP','$IP_EMAIL','$IP_HP','$unit_kantor')");

		$notif = "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button> Input permohnan SK NJOP  berhasil</div>";
		$id = $rk['tahun'] . '.' . $rk['bundel'] . '.' . $rk['no_urut'];
		$this->session->set_flashdata('notif', $notif);

		// $this->session->set_flashdata('nopel',$rk['tahun'].'.'. $rk['bundel'].'.'. $rk['no_urut']);

		// redirect('permohonan/create');	

		redirect('permohonan/cetaksknjop/' . $rk['tahun'] . '.' . $rk['bundel'] . '.' . $rk['no_urut'], 'refresh');
	}


	function ceklunas($nop, $th)
	{

		$sql = "select status_pembayaran_sppt from sppt where  thn_pajak_sppt = $th
               AND kd_propinsi = SUBSTR ('$nop', 1, 2)
               AND kd_dati2 = SUBSTR ('$nop', 3, 2)
               AND kd_kecamatan = SUBSTR ('$nop', 5, 3)
               AND kd_kelurahan = SUBSTR ('$nop', 8, 3)
               AND kd_blok = SUBSTR ('$nop', 11, 3)
               AND no_urut = SUBSTR ('$nop', 14, 4)
               AND kd_jns_op = SUBSTR ('$nop', 18, 1)";
		$cek = $this->db->query($sql)->row();
		if ($cek) {
			return $cek->STATUS_PEMBAYARAN_SPPT;
		} else {
			return 0;
		}
	}

	public function create_action()
	{
		ini_set('max_execution_time', 0);

		/* echo "<pre>";
		print_r($this->input->post());
		die(); */

		$IP                = $this->Mmaster->getIP();
		$rk                = $this->Mpermohonan->getNoLayanan();
		$KD_KANWIL         = '01';
		$KD_KANTOR         = '01';
		$THN_PELAYANAN     = $rk['tahun'];
		$BUNDEL_PELAYANAN  = $rk['bundel'];
		$NO_URUT_PELAYANAN = $rk['no_urut'];

		$blok = array('16', '15', '17');

		$this->db->trans_start();
		// kolektif opo ora 
		$kolektif = $this->input->post('STATUS_KOLEKTIF', true) == '0' ? 'false' : 'true';
		// echo "<hr>";

		// data pst_permohonan
		$this->db->set('KD_KANWIL', $KD_KANWIL);
		$this->db->set('KD_KANTOR', $KD_KANTOR);
		$this->db->set('THN_PELAYANAN', $THN_PELAYANAN);
		$this->db->set('BUNDEL_PELAYANAN', $BUNDEL_PELAYANAN);
		$this->db->set('NO_URUT_PELAYANAN', $NO_URUT_PELAYANAN);
		$this->db->set('NO_SRT_PERMOHONAN', $this->input->post('NO_SRT_PERMOHONAN', TRUE));
		$this->db->set('TGL_SURAT_PERMOHONAN', "to_date('" . $this->input->post('TGL_SURAT_PERMOHONAN', true) . "','dd-mm-yyyy')", false);
		$this->db->set('NAMA_PEMOHON', $this->input->post('NAMA_PEMOHON', true));
		$this->db->set('ALAMAT_PEMOHON', $this->input->post('ALAMAT_PEMOHON', true));
		$this->db->set('KETERANGAN_PST', $this->input->post('KETERANGAN_PST', true));
		$this->db->set('CATATAN_PST', $this->input->post('CATATAN_PST', true));
		$this->db->set('STATUS_KOLEKTIF', $this->input->post('STATUS_KOLEKTIF', true));
		$this->db->set('TGL_TERIMA_DOKUMEN_WP', "to_date('" . $this->input->post('TGL_TERIMA_DOKUMEN_WP', true) . "','dd-mm-yyyy')", false);
		$this->db->set('TGL_PERKIRAAN_SELESAI', "to_date('" . $this->input->post('TGL_PERKIRAAN_SELESAI', true) . "','dd-mm-yyyy')", false);
		$this->db->set('NIP_PENERIMA', $this->session->userdata('nip'));
		$this->db->insert('PST_PERMOHONAN');

		// data p_ip
		$this->db->set('IP_KD_KANWIL', $KD_KANWIL);
		$this->db->set('IP_KD_KANTOR', $KD_KANTOR);
		$this->db->set('IP_THN_PELAYANAN', $THN_PELAYANAN);
		$this->db->set('IP_BUNDEL_PELAYANAN', $BUNDEL_PELAYANAN);
		$this->db->set('IP_NO_URUT_PELAYANAN', $NO_URUT_PELAYANAN);
		$this->db->set('IP_PELAYANAN', $IP);
		$this->db->set('IP_EMAIL', $this->input->post('IP_EMAIL', TRUE));
		$this->db->set('IP_HP', $this->input->post('IP_HP', TRUE));
		$this->db->set('UNIT_KANTOR', $this->session->userdata('unit_kantor'));
		$this->db->set('ID_INC', 'SEQ_P_IP.nextval', false);
		$this->db->insert('P_IP');

		// jika selesai tahun depan maka tidak di blokir
		// dd-mm-yyyy

		$pjka = explode('-', $this->input->post('TGL_PERKIRAAN_SELESAI', true));
		$cth = (int) $pjka[2];
		$cths = (int) date('Y');

		if ($kolektif == 'false') {
			// individu

			// pst detail
			$this->db->set('KD_KANWIL', $KD_KANWIL);
			$this->db->set('KD_KANTOR', $KD_KANTOR);
			$this->db->set('THN_PELAYANAN', $THN_PELAYANAN);
			$this->db->set('BUNDEL_PELAYANAN', $BUNDEL_PELAYANAN);
			$this->db->set('NO_URUT_PELAYANAN', $NO_URUT_PELAYANAN);
			$this->db->set('KD_PROPINSI_PEMOHON', $this->input->post('KD_PROPINSI_PEMOHON', true));
			$this->db->set('KD_DATI2_PEMOHON', $this->input->post('KD_DATI2_PEMOHON', true));
			$this->db->set('KD_KECAMATAN_PEMOHON', $this->input->post('KD_KECAMATAN_PEMOHON', true));
			$this->db->set('KD_KELURAHAN_PEMOHON', $this->input->post('KD_KELURAHAN_PEMOHON', true));
			$this->db->set('KD_BLOK_PEMOHON', $this->input->post('KD_BLOK_PEMOHON', true));
			$this->db->set('NO_URUT_PEMOHON', $this->input->post('NO_URUT_PEMOHON', true));
			$this->db->set('KD_JNS_OP_PEMOHON', $this->input->post('KD_JNS_OP_PEMOHON', true));
			$this->db->set('KD_JNS_PELAYANAN', $this->input->post('KD_JNS_PELAYANAN', true));
			$this->db->set('THN_PAJAK_PERMOHONAN', $THN_PELAYANAN);
			$this->db->insert('PST_DETAIL');


			// jika selesai tahun depan maka tidak di blokir
			// dd-mm-yyyy
			if ($cths < $cth) {
				// blokir sppt
				$vnn = $this->input->post('KD_PROPINSI_PEMOHON', true) . $this->input->post('KD_DATI2_PEMOHON', true) . $this->input->post('KD_KECAMATAN_PEMOHON', true) . $this->input->post('KD_KELURAHAN_PEMOHON', true) . $this->input->post('KD_BLOK_PEMOHON', true) . $this->input->post('NO_URUT_PEMOHON', true) . $this->input->post('KD_JNS_OP_PEMOHON', true);

				$cl = $this->ceklunas($vnn, $THN_PELAYANAN);

				if ($cl == 0) {
					if (!in_array($this->input->post('KD_JNS_PELAYANAN'), $blok)) {
						$this->db->query("call blokir_nop@to17(" . $rk['tahun'] . ",'" . $this->input->post('KD_KECAMATAN_PEMOHON', true) . "','" . $this->input->post('KD_KELURAHAN_PEMOHON', true) . "','" . $this->input->post('KD_BLOK_PEMOHON', true) . "','" . $this->input->post('NO_URUT_PEMOHON', true) . "','" . $this->input->post('KD_JNS_OP_PEMOHON', true) . "')");
					}
				}
			}


			if (!in_array($this->input->post('KD_JNS_PELAYANAN'), $blok)) {
				// rekam riwayat dan pelimpahan otomatis ke pelayanan 2
				$riwayat = array(
					"KD_KANWIL"            => '01',
					"KD_KANTOR"            => '01',
					"THN_PELAYANAN"        => $rk['tahun'],
					"BUNDEL_PELAYANAN"     => $rk['bundel'],
					"NO_URUT_PELAYANAN"    => $rk['no_urut'],
					"KD_PROPINSI_RIWAYAT"  => $this->input->post('KD_PROPINSI_PEMOHON', true),
					"KD_DATI2_RIWAYAT"     => $this->input->post('KD_DATI2_PEMOHON', true),
					"KD_KECAMATAN_RIWAYAT" => $this->input->post('KD_KECAMATAN_PEMOHON', true),
					"KD_KELURAHAN_RIWAYAT" => $this->input->post('KD_KELURAHAN_PEMOHON', true),
					"KD_BLOK_RIWAYAT"      => $this->input->post('KD_BLOK_PEMOHON', true),
					"NO_URUT_RIWAYAT"      => $this->input->post('NO_URUT_PEMOHON', true),
					"KD_JNS_OP_RIWAYAT"    => $this->input->post('KD_JNS_OP_PEMOHON', true),
					"KODE_GROUP"           => $this->session->userdata('pbb_kg'),
					"NIP"                  => $this->session->userdata('nip'),
					"TANGGAL_AWAL"         => date('Y-m-d H:i:s'),
					"TANGGAL_AKHIR"        => date('Y-m-d H:i:s'),
					"KETERANGAN"           => 'Entri data permohonan',
					"STATUS_BERKAS"        => 'Proses',
					"UNIT_SEBEUMNYA"       => $this->session->userdata('pbb_kg')
				);
				$this->Mpermohonan->insertriwayat($riwayat);

				$riwayatb = array(
					"KD_KANWIL"            => '01',
					"KD_KANTOR"            => '01',
					"THN_PELAYANAN"        => $rk['tahun'],
					"BUNDEL_PELAYANAN"     => $rk['bundel'],
					"NO_URUT_PELAYANAN"    => $rk['no_urut'],
					"KD_PROPINSI_RIWAYAT"  => $this->input->post('KD_PROPINSI_PEMOHON', true),
					"KD_DATI2_RIWAYAT"     => $this->input->post('KD_DATI2_PEMOHON', true),
					"KD_KECAMATAN_RIWAYAT" => $this->input->post('KD_KECAMATAN_PEMOHON', true),
					"KD_KELURAHAN_RIWAYAT" => $this->input->post('KD_KELURAHAN_PEMOHON', true),
					"KD_BLOK_RIWAYAT"      => $this->input->post('KD_BLOK_PEMOHON', true),
					"NO_URUT_RIWAYAT"      => $this->input->post('NO_URUT_PEMOHON', true),
					"KD_JNS_OP_RIWAYAT"    => $this->input->post('KD_JNS_OP_PEMOHON', true),
					"KODE_GROUP"           => '21',
					"NIP"                  => null,
					"TANGGAL_AWAL"         => date('Y-m-d H:i:s'),
					"TANGGAL_AKHIR"        => null,
					"KETERANGAN"           => null,
					"STATUS_BERKAS"        => null,
					"UNIT_SEBEUMNYA"       => $this->session->userdata('pbb_kg')
				);
				$this->Mpermohonan->insertriwayat($riwayatb);
			}
		} else {
			// kolektif
			// hitung jumlah nop
			$jn = count($this->input->post('KD_PROPINSI_PEMOHONB', true));
			$pstdetail=array();
			// looping
			for ($i = 0; $i < $jn; $i++) {

				// update ke pst_permohonan
				if ($i == 0) {
					$this->db->set('NAMA_PEMOHON', $this->input->post('NAMA_PEMOHONB')[$i]);
					$this->db->set('ALAMAT_PEMOHON', $this->input->post('ALAMAT_PEMOHONB')[$i]);
					$this->db->where('THN_PELAYANAN', $THN_PELAYANAN);
					$this->db->where('BUNDEL_PELAYANAN', $BUNDEL_PELAYANAN);
					$this->db->where('NO_URUT_PELAYANAN', $NO_URUT_PELAYANAN);
					$this->db->update('PST_PERMOHONAN');
				}

				// pst detail
				// $this->db->set('KD_KANWIL', $KD_KANWIL);
				// $this->db->set('KD_KANTOR', $KD_KANTOR);
				// $this->db->set('THN_PELAYANAN', $THN_PELAYANAN);
				// $this->db->set('BUNDEL_PELAYANAN', $BUNDEL_PELAYANAN);
				// $this->db->set('NO_URUT_PELAYANAN', $NO_URUT_PELAYANAN);
				// $this->db->set('KD_PROPINSI_PEMOHON', $this->input->post('KD_PROPINSI_PEMOHONB', true)[$i]);
				// $this->db->set('KD_DATI2_PEMOHON', $this->input->post('KD_DATI2_PEMOHONB', true)[$i]);
				// $this->db->set('KD_KECAMATAN_PEMOHON', $this->input->post('KD_KECAMATAN_PEMOHONB', true)[$i]);
				// $this->db->set('KD_KELURAHAN_PEMOHON', $this->input->post('KD_KELURAHAN_PEMOHONB', true)[$i]);
				// $this->db->set('KD_BLOK_PEMOHON', $this->input->post('KD_BLOK_PEMOHONB', true)[$i]);
				// $this->db->set('NO_URUT_PEMOHON', $this->input->post('NO_URUT_PEMOHONB', true)[$i]);
				// $this->db->set('KD_JNS_OP_PEMOHON', $this->input->post('KD_JNS_OP_PEMOHONB', true)[$i]);
				// $this->db->set('KD_JNS_PELAYANAN', $this->input->post('KD_JNS_PELAYANANB', true)[$i]);
				// $this->db->set('THN_PAJAK_PERMOHONAN', $THN_PELAYANAN);
				// $this->db->insert('PST_DETAIL');

				// insert batch ke pst detail
				
				$vpst=array(
					'KD_KANWIL' => $KD_KANWIL,
					'KD_KANTOR' => $KD_KANTOR,
					'THN_PELAYANAN' => $THN_PELAYANAN,
					'BUNDEL_PELAYANAN' => $BUNDEL_PELAYANAN,
					'NO_URUT_PELAYANAN' => $NO_URUT_PELAYANAN,
					'KD_PROPINSI_PEMOHON' => $this->input->post('KD_PROPINSI_PEMOHONB', true)[$i],
					'KD_DATI2_PEMOHON' => $this->input->post('KD_DATI2_PEMOHONB', true)[$i],
					'KD_KECAMATAN_PEMOHON' => $this->input->post('KD_KECAMATAN_PEMOHONB', true)[$i],
					'KD_KELURAHAN_PEMOHON' => $this->input->post('KD_KELURAHAN_PEMOHONB', true)[$i],
					'KD_BLOK_PEMOHON' => $this->input->post('KD_BLOK_PEMOHONB', true)[$i],
					'NO_URUT_PEMOHON' => $this->input->post('NO_URUT_PEMOHONB', true)[$i],
					'KD_JNS_OP_PEMOHON' => $this->input->post('KD_JNS_OP_PEMOHONB', true)[$i],
					'KD_JNS_PELAYANAN' => $this->input->post('KD_JNS_PELAYANANB', true)[$i],
					'THN_PAJAK_PERMOHONAN' => $THN_PELAYANAN
				);

				array_push($pstdetail,$vpst);

				// jika selesai tahun depan maka tidak di blokir
				// dd-mm-yyyy
				if ($cths < $cth) {
					// blokir sppt
					$vnn = $this->input->post('KD_PROPINSI_PEMOHONB', true)[$i] . $this->input->post('KD_DATI2_PEMOHONB', true)[$i] . $this->input->post('KD_KECAMATAN_PEMOHONB', true)[$i] . $this->input->post('KD_KELURAHAN_PEMOHONB', true)[$i] . $this->input->post('KD_BLOK_PEMOHONB', true)[$i] . $this->input->post('NO_URUT_PEMOHONB', true)[$i] . $this->input->post('KD_JNS_OP_PEMOHONB', true)[$i];
					$cl = $this->ceklunas($vnn, $THN_PELAYANAN);
					if ($cl == 0) {
						if (!in_array($this->input->post('KD_JNS_PELAYANANB'), $blok)) {
							$this->db->query("CALL BLOKIR_NOP@TO17(" . $rk['tahun'] . ",'" . $this->input->post('KD_KECAMATAN_PEMOHONB', true)[$i] . "','" . $this->input->post('KD_KELURAHAN_PEMOHONB', true)[$i] . "','" . $this->input->post('KD_BLOK_PEMOHONB', true)[$i] . "','" . $this->input->post('NO_URUT_PEMOHONB', true)[$i] . "','" . $this->input->post('KD_JNS_OP_PEMOHONB', true)[$i] . "')");
						}
					}
				}


				if (!in_array($this->input->post('KD_JNS_PELAYANAN'), $blok)) {

					// rekam riwayat dan pelimpahan otomatis ke pelayanan 2
					$riwayat = array(
						"KD_KANWIL"            => '01',
						"KD_KANTOR"            => '01',
						"THN_PELAYANAN"        => $rk['tahun'],
						"BUNDEL_PELAYANAN"     => $rk['bundel'],
						"NO_URUT_PELAYANAN"    => $rk['no_urut'],
						"KD_PROPINSI_RIWAYAT"  => $this->input->post('KD_PROPINSI_PEMOHONB', true)[$i],
						"KD_DATI2_RIWAYAT"     => $this->input->post('KD_DATI2_PEMOHONB', true)[$i],
						"KD_KECAMATAN_RIWAYAT" => $this->input->post('KD_KECAMATAN_PEMOHONB', true)[$i],
						"KD_KELURAHAN_RIWAYAT" => $this->input->post('KD_KELURAHAN_PEMOHONB', true)[$i],
						"KD_BLOK_RIWAYAT"      => $this->input->post('KD_BLOK_PEMOHONB', true)[$i],
						"NO_URUT_RIWAYAT"      => $this->input->post('NO_URUT_PEMOHONB', true)[$i],
						"KD_JNS_OP_RIWAYAT"    => $this->input->post('KD_JNS_OP_PEMOHONB', true)[$i],
						"KODE_GROUP"           => $this->session->userdata('pbb_kg'),
						"NIP"                  => $this->session->userdata('nip'),
						"TANGGAL_AWAL"         => date('Y-m-d H:i:s'),
						"TANGGAL_AKHIR"        => date('Y-m-d H:i:s'),
						"KETERANGAN"           => 'Entri data permohonan',
						"STATUS_BERKAS"        => 'Proses',
						"UNIT_SEBEUMNYA"       => $this->session->userdata('pbb_kg')
					);
					$this->Mpermohonan->insertriwayat($riwayat);

					$riwayatb = array(
						"KD_KANWIL"            => '01',
						"KD_KANTOR"            => '01',
						"THN_PELAYANAN"        => $rk['tahun'],
						"BUNDEL_PELAYANAN"     => $rk['bundel'],
						"NO_URUT_PELAYANAN"    => $rk['no_urut'],
						"KD_PROPINSI_RIWAYAT"  => $this->input->post('KD_PROPINSI_PEMOHONB', true)[$i],
						"KD_DATI2_RIWAYAT"     => $this->input->post('KD_DATI2_PEMOHONB', true)[$i],
						"KD_KECAMATAN_RIWAYAT" => $this->input->post('KD_KECAMATAN_PEMOHONB', true)[$i],
						"KD_KELURAHAN_RIWAYAT" => $this->input->post('KD_KELURAHAN_PEMOHONB', true)[$i],
						"KD_BLOK_RIWAYAT"      => $this->input->post('KD_BLOK_PEMOHONB', true)[$i],
						"NO_URUT_RIWAYAT"      => $this->input->post('NO_URUT_PEMOHONB', true)[$i],
						"KD_JNS_OP_RIWAYAT"    => $this->input->post('KD_JNS_OP_PEMOHONB', true)[$i],
						"KODE_GROUP"           => '21',
						"NIP"                  => null,
						"TANGGAL_AWAL"         => date('Y-m-d H:i:s'),
						"TANGGAL_AKHIR"        => null,
						"KETERANGAN"           => null,
						"STATUS_BERKAS"        => null,
						"UNIT_SEBEUMNYA"       => $this->session->userdata('pbb_kg')
					);
					$this->Mpermohonan->insertriwayat($riwayatb);
				}
			}

			// insert pst detail
			$this->db->insert_batch('PST_DETAIL', $pstdetail); 

		}

		$lampiran = $this->input->post('lampiran', true);

		if (in_array('L_PERMOHONAN', $lampiran)) {
			$L_PERMOHONAN = '1';
		} else {
			$L_PERMOHONAN = '0';
		}
		if (in_array('L_SURAT_KUASA', $lampiran)) {
			$L_SURAT_KUASA = '1';
		} else {
			$L_SURAT_KUASA = '0';
		}
		if (in_array('L_KTP_WP', $lampiran)) {
			$L_KTP_WP = '1';
		} else {
			$L_KTP_WP = '0';
		}
		if (in_array('L_SERTIFIKAT_TANAH', $lampiran)) {
			$L_SERTIFIKAT_TANAH = '1';
		} else {
			$L_SERTIFIKAT_TANAH = '0';
		}
		if (in_array('L_SPPT', $lampiran)) {
			$L_SPPT = '1';
		} else {
			$L_SPPT = '0';
		}
		if (in_array('L_IMB', $lampiran)) {
			$L_IMB = '1';
		} else {
			$L_IMB = '0';
		}
		if (in_array('L_AKTE_JUAL_BELI', $lampiran)) {
			$L_AKTE_JUAL_BELI = '1';
		} else {
			$L_AKTE_JUAL_BELI = '0';
		}
		if (in_array('L_SK_PENSIUN', $lampiran)) {
			$L_SK_PENSIUN = '1';
		} else {
			$L_SK_PENSIUN = '0';
		}
		if (in_array('L_SPPT_STTS', $lampiran)) {
			$L_SPPT_STTS = '1';
		} else {
			$L_SPPT_STTS = '0';
		}
		if (in_array('L_STTS', $lampiran)) {
			$L_STTS = '1';
		} else {
			$L_STTS = '0';
		}
		if (in_array('L_SK_PENGURANGAN', $lampiran)) {
			$L_SK_PENGURANGAN = '1';
		} else {
			$L_SK_PENGURANGAN = '0';
		}
		if (in_array('L_SK_KEBERATAN', $lampiran)) {
			$L_SK_KEBERATAN = '1';
		} else {
			$L_SK_KEBERATAN = '0';
		}
		if (in_array('L_SKKP_PBB', $lampiran)) {
			$L_SKKP_PBB = '1';
		} else {
			$L_SKKP_PBB = '0';
		}
		if (in_array('L_SPMKP_PBB', $lampiran)) {
			$L_SPMKP_PBB = '1';
		} else {
			$L_SPMKP_PBB = '0';
		}
		if (in_array('L_LAIN_LAIN', $lampiran)) {
			$L_LAIN_LAIN = '1';
		} else {
			$L_LAIN_LAIN = '0';
		}
		if (in_array('L_SKET_TANAH', $lampiran)) {
			$L_SKET_TANAH = '1';
		} else {
			$L_SKET_TANAH = '0';
		}
		if (in_array('L_SKET_LURAH', $lampiran)) {
			$L_SKET_LURAH = '1';
		} else {
			$L_SKET_LURAH = '0';
		}
		if (in_array('L_NPWPD', $lampiran)) {
			$L_NPWPD = '1';
		} else {
			$L_NPWPD = '0';
		}
		if (in_array('L_PENGHASILAN', $lampiran)) {
			$L_PENGHASILAN = '1';
		} else {
			$L_PENGHASILAN = '0';
		}
		if (in_array('L_CAGAR', $lampiran)) {
			$L_CAGAR = '1';
		} else {
			$L_CAGAR = '0';
		}


		// lampiran
		$datac = array(
			'KD_KANWIL'          => '01',
			'KD_KANTOR'          => '01',
			'THN_PELAYANAN'      => $rk['tahun'],
			'BUNDEL_PELAYANAN'   => $rk['bundel'],
			'NO_URUT_PELAYANAN'  => $rk['no_urut'],
			"L_PERMOHONAN"       => $L_PERMOHONAN,
			"L_SURAT_KUASA"      => $L_SURAT_KUASA,
			"L_KTP_WP"           => $L_KTP_WP,
			"L_SERTIFIKAT_TANAH" => $L_SERTIFIKAT_TANAH,
			"L_SPPT"             => $L_SPPT,
			"L_IMB"              => $L_IMB,
			"L_AKTE_JUAL_BELI"   => $L_AKTE_JUAL_BELI,
			"L_SK_PENSIUN"       => $L_SK_PENSIUN,
			"L_SPPT_STTS"        => $L_SPPT_STTS,
			"L_STTS"             => $L_STTS,
			"L_SK_PENGURANGAN"   => $L_SK_PENGURANGAN,
			"L_SK_KEBERATAN"     => $L_SK_KEBERATAN,
			"L_SKKP_PBB"         => $L_SKKP_PBB,
			"L_SPMKP_PBB"        => $L_SPMKP_PBB,
			"L_LAIN_LAIN"        => $L_LAIN_LAIN,
			"L_SKET_TANAH"       => $L_SKET_TANAH,
			"L_SKET_LURAH"       => $L_SKET_LURAH,
			"L_NPWPD"            => $L_NPWPD,
			"L_PENGHASILAN"      => $L_PENGHASILAN,
			"L_CAGAR"            => $L_CAGAR,
		);
		$this->db->insert('PST_LAMPIRAN', $datac);


		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE) {
			echo "gagal";
			$notif = "<div class='alert alert-danger'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button> Input data gagal</div>";
			$this->session->set_flashdata('notif', $notif);
			redirect('permohonan/create');
		} else {

			echo "sukses";
			$id = $rk['tahun'] . '.' . $rk['bundel'] . '.' . $rk['no_urut'];

			redirect('permohonan/cetak/' . $id);
		}


		// ====================================================



		die();

		/*
$this->db->trans_start();		
if($STATUS_KOLEKTIF=='0'){
	// entri individu
	$this->db->query("INSERT INTO PST_PERMOHONAN (KD_KANWIL, KD_KANTOR, THN_PELAYANAN, BUNDEL_PELAYANAN, NO_URUT_PELAYANAN, NO_SRT_PERMOHONAN, TGL_SURAT_PERMOHONAN, NAMA_PEMOHON, ALAMAT_PEMOHON, KETERANGAN_PST, CATATAN_PST, STATUS_KOLEKTIF, TGL_TERIMA_DOKUMEN_WP, TGL_PERKIRAAN_SELESAI, NIP_PENERIMA) 
											VALUES ('$KD_KANWIL', '$KD_KANTOR', '$THN_PELAYANAN', '$BUNDEL_PELAYANAN', '$NO_URUT_PELAYANAN', '$NO_SRT_PERMOHONAN', to_date('$TGL_SURAT_PERMOHONAN','dd-mm-yyyy'), '$NAMA_PEMOHON', '$ALAMAT_PEMOHON', '$KETERANGAN_PST', '$CATATAN_PST', '$STATUS_KOLEKTIF', to_date('$TGL_TERIMA_DOKUMEN_WP','dd-mm-yyyy'), to_date('$TGL_PERKIRAAN_SELESAI','dd-mm-yyyy'), '$NIP_PENERIMA')");
	//etry ip
	$this->db->query("INSERT INTO P_IP (IP_KD_KANWIL, IP_KD_KANTOR, IP_THN_PELAYANAN, IP_BUNDEL_PELAYANAN, IP_NO_URUT_PELAYANAN, IP_PELAYANAN,IP_EMAIL,IP_HP,ID_INC) 
								VALUES ('$KD_KANWIL', '$KD_KANTOR', '$THN_PELAYANAN', '$BUNDEL_PELAYANAN', '$NO_URUT_PELAYANAN', '$IP','$IP_EMAIL','$IP_HP',SEQ_P_IP.nextval)");
// data detail ini individual
	$datab=array(
			'KD_KANWIL'            =>'01',
			'KD_KANTOR'            =>'01',
			'THN_PELAYANAN'        =>$rk['tahun'],
			'BUNDEL_PELAYANAN'     =>$rk['bundel'],
			'NO_URUT_PELAYANAN'    =>$rk['no_urut'],
			'KD_PROPINSI_PEMOHON'  =>$_POST['KD_PROPINSI_PEMOHON'],
			'KD_DATI2_PEMOHON'     =>$_POST['KD_DATI2_PEMOHON'],
			'KD_KECAMATAN_PEMOHON' =>$_POST['KD_KECAMATAN_PEMOHON'],
			'KD_KELURAHAN_PEMOHON' =>$_POST['KD_KELURAHAN_PEMOHON'],
			'KD_BLOK_PEMOHON'      =>$_POST['KD_BLOK_PEMOHON'],
			'NO_URUT_PEMOHON'      =>$_POST['NO_URUT_PEMOHON'],
			'KD_JNS_OP_PEMOHON'    =>$_POST['KD_JNS_OP_PEMOHON'],
			'KD_JNS_PELAYANAN'     =>$this->input->post('KD_JNS_PELAYANAN'),
			'THN_PAJAK_PERMOHONAN' =>$rk['tahun'],
			// 'NO_KOLEKTIF'          =>'000'
		);	
 		$this->db->insert('PST_DETAIL',$datab);

 		// blokir sppt
 		if(!in_array($this->input->post('KD_JNS_PELAYANAN'),$blok)){
				// echo "bloken";
 			$this->db->query("call blokir_nop@to17(".$rk['tahun'].",'".$_POST['KD_KECAMATAN_PEMOHON']."','".$_POST['KD_KELURAHAN_PEMOHON']."','".$_POST['KD_BLOK_PEMOHON']."','".$_POST['NO_URUT_PEMOHON']."','".$_POST['KD_JNS_OP_PEMOHON']."')");
		}


 	// data riwayat
 	$riwayat=array(
			"KD_KANWIL"            => '01', 
			"KD_KANTOR"            => '01', 
			"THN_PELAYANAN"        => $rk['tahun'], 
			"BUNDEL_PELAYANAN"     => $rk['bundel'], 
			"NO_URUT_PELAYANAN"    => $rk['no_urut'], 
			"KD_PROPINSI_RIWAYAT"  => $_POST['KD_PROPINSI_PEMOHON'], 
			"KD_DATI2_RIWAYAT"     => $_POST['KD_DATI2_PEMOHON'], 
			"KD_KECAMATAN_RIWAYAT" => $_POST['KD_KECAMATAN_PEMOHON'], 
			"KD_KELURAHAN_RIWAYAT" => $_POST['KD_KELURAHAN_PEMOHON'], 
			"KD_BLOK_RIWAYAT"      => $_POST['KD_BLOK_PEMOHON'], 
			"NO_URUT_RIWAYAT"      => $_POST['NO_URUT_PEMOHON'], 
			"KD_JNS_OP_RIWAYAT"    => $_POST['KD_JNS_OP_PEMOHON'], 
			"KODE_GROUP"           => $this->session->userdata('pbb_kg'),
			"NIP"                  => $this->session->userdata('nip'),
			"TANGGAL_AWAL"         => date('Y-m-d H:i:s'),
			"TANGGAL_AKHIR"        => date('Y-m-d H:i:s'),
			"KETERANGAN"           => 'Entri data permohonan',
			"STATUS_BERKAS"        => 'Proses',	
			"UNIT_SEBEUMNYA"       => $this->session->userdata('pbb_kg')
 		);
 	$this->Mpermohonan->insertriwayat($riwayat);

 	$riwayatb=array(
				"KD_KANWIL"            =>'01', 
				"KD_KANTOR"            =>'01', 
				"THN_PELAYANAN"        =>$rk['tahun'], 
				"BUNDEL_PELAYANAN"     =>$rk['bundel'], 
				"NO_URUT_PELAYANAN"    =>$rk['no_urut'], 
				"KD_PROPINSI_RIWAYAT"  =>$_POST['KD_PROPINSI_PEMOHON'], 
				"KD_DATI2_RIWAYAT"     =>$_POST['KD_DATI2_PEMOHON'], 
				"KD_KECAMATAN_RIWAYAT" =>$_POST['KD_KECAMATAN_PEMOHON'], 
				"KD_KELURAHAN_RIWAYAT" =>$_POST['KD_KELURAHAN_PEMOHON'], 
				"KD_BLOK_RIWAYAT"      =>$_POST['KD_BLOK_PEMOHON'], 
				"NO_URUT_RIWAYAT"      =>$_POST['NO_URUT_PEMOHON'], 
				"KD_JNS_OP_RIWAYAT"    =>$_POST['KD_JNS_OP_PEMOHON'], 
				"KODE_GROUP"           => '21',
				"NIP"                  => null,
				"TANGGAL_AWAL"         => date('Y-m-d H:i:s'),
				"TANGGAL_AKHIR"        => null,
				"KETERANGAN"           => null,
				"STATUS_BERKAS"		   => null,
				"UNIT_SEBEUMNYA"       => $this->session->userdata('pbb_kg')
 		);
 	$this->Mpermohonan->insertriwayat($riwayatb);

 	}else if($STATUS_KOLEKTIF=='1'){
 		// entri kolektif
 		for($i=0;$i<count($_POST['KD_PROPINSI_PEMOHONB']);$i++){
 			if($i==0){

 					$namapemohon=$_POST['NAMA_PEMOHONB'][$i];
 					$alamatpemohon=$_POST['ALAMAT_PEMOHONB'][$i];
 					$this->db->query("INSERT INTO PST_PERMOHONAN (KD_KANWIL, KD_KANTOR, THN_PELAYANAN, BUNDEL_PELAYANAN, NO_URUT_PELAYANAN, NO_SRT_PERMOHONAN, TGL_SURAT_PERMOHONAN, NAMA_PEMOHON, ALAMAT_PEMOHON, KETERANGAN_PST, CATATAN_PST, STATUS_KOLEKTIF, TGL_TERIMA_DOKUMEN_WP, TGL_PERKIRAAN_SELESAI, NIP_PENERIMA) 
											VALUES ('$KD_KANWIL', '$KD_KANTOR', '$THN_PELAYANAN', '$BUNDEL_PELAYANAN', '$NO_URUT_PELAYANAN', '$NO_SRT_PERMOHONAN', to_date('$TGL_SURAT_PERMOHONAN','dd-mm-yyyy'), '$namapemohon', '$alamatpemohon', '$KETERANGAN_PST', '$CATATAN_PST', '$STATUS_KOLEKTIF', to_date('$TGL_TERIMA_DOKUMEN_WP','dd-mm-yyyy'), to_date('$TGL_PERKIRAAN_SELESAI','dd-mm-yyyy'), '$NIP_PENERIMA')");
 					$this->db->query("INSERT INTO P_IP (IP_KD_KANWIL, IP_KD_KANTOR, IP_THN_PELAYANAN, IP_BUNDEL_PELAYANAN, IP_NO_URUT_PELAYANAN, IP_PELAYANAN,IP_EMAIL,IP_HP) 
								VALUES ('$KD_KANWIL', '$KD_KANTOR', '$THN_PELAYANAN', '$BUNDEL_PELAYANAN', '$NO_URUT_PELAYANAN', '$IP','$IP_EMAIL','$IP_HP')");
 			}

			$databas=array(
					'KD_KANWIL'            =>'01',
					'KD_KANTOR'            =>'01',
					'THN_PELAYANAN'        =>$rk['tahun'],
					'BUNDEL_PELAYANAN'     =>$rk['bundel'],
					'NO_URUT_PELAYANAN'    =>$rk['no_urut'],
					'KD_PROPINSI_PEMOHON'  =>$_POST['KD_PROPINSI_PEMOHONB'][$i],
					'KD_DATI2_PEMOHON'     =>$_POST['KD_DATI2_PEMOHONB'][$i],
					'KD_KECAMATAN_PEMOHON' =>$_POST['KD_KECAMATAN_PEMOHONB'][$i],
					'KD_KELURAHAN_PEMOHON' =>$_POST['KD_KELURAHAN_PEMOHONB'][$i],
					'KD_BLOK_PEMOHON'      =>$_POST['KD_BLOK_PEMOHONB'][$i],
					'NO_URUT_PEMOHON'      =>$_POST['NO_URUT_PEMOHONB'][$i],
					'KD_JNS_OP_PEMOHON'    =>$_POST['KD_JNS_OP_PEMOHONB'][$i],
					'KD_JNS_PELAYANAN'     =>$_POST['KD_JNS_PELAYANANB'][$i],
					'THN_PAJAK_PERMOHONAN' =>$rk['tahun'],
					// 'NO_KOLEKTIF'          =>sprintf('%03s', $i)

				);
		 	$this->db->insert('PST_DETAIL',$databas); 	
		 	if(!in_array($_POST['KD_JNS_PELAYANANB'][$i],$blok)){
				// echo "bloken";
 				$this->db->query("call blokir_nop@to17(".$rk['tahun'].",'".$_POST['KD_KECAMATAN_PEMOHONB'][$i]."','".$_POST['KD_KELURAHAN_PEMOHONB'][$i]."','".$_POST['KD_BLOK_PEMOHONB'][$i]."','".$_POST['NO_URUT_PEMOHONB'][$i]."','".$_POST['KD_JNS_OP_PEMOHONB'][$i]."')");
			}

		 	// data riwayat
		 	$riwayat=array(
					"KD_KANWIL"            =>'01',
					"KD_KANTOR"            =>'01',
					"THN_PELAYANAN"        =>$rk['tahun'],
					"BUNDEL_PELAYANAN"     =>$rk['bundel'],
					"NO_URUT_PELAYANAN"    =>$rk['no_urut'],
					"KD_PROPINSI_RIWAYAT"  =>$_POST['KD_PROPINSI_PEMOHONB'][$i],
					"KD_DATI2_RIWAYAT"     =>$_POST['KD_DATI2_PEMOHONB'][$i],
					"KD_KECAMATAN_RIWAYAT" =>$_POST['KD_KECAMATAN_PEMOHONB'][$i],
					"KD_KELURAHAN_RIWAYAT" =>$_POST['KD_KELURAHAN_PEMOHONB'][$i],
					"KD_BLOK_RIWAYAT"      =>$_POST['KD_BLOK_PEMOHONB'][$i],
					"NO_URUT_RIWAYAT"      =>$_POST['NO_URUT_PEMOHONB'][$i],
					"KD_JNS_OP_RIWAYAT"    =>$_POST['KD_JNS_OP_PEMOHONB'][$i],
					"KODE_GROUP"           => $this->session->userdata('pbb_kg'),
					"NIP"                  => $this->session->userdata('nip'),
					"TANGGAL_AWAL"         => date('Y-m-d H:i:s'),
					"TANGGAL_AKHIR"        => date('Y-m-d H:i:s'),
					"KETERANGAN"           => 'Entri data permohonan',
					"STATUS_BERKAS"		   => 'Proses',
					"UNIT_SEBEUMNYA"       => $this->session->userdata('pbb_kg')
		 		);
		 	$this->Mpermohonan->insertriwayat($riwayat);

		 	$riwayatb=array(
				"KD_KANWIL"            =>'01',
					"KD_KANTOR"            =>'01',
					"THN_PELAYANAN"        =>$rk['tahun'],
					"BUNDEL_PELAYANAN"     =>$rk['bundel'],
					"NO_URUT_PELAYANAN"    =>$rk['no_urut'],
					"KD_PROPINSI_RIWAYAT"  =>$_POST['KD_PROPINSI_PEMOHONB'][$i],
					"KD_DATI2_RIWAYAT"     =>$_POST['KD_DATI2_PEMOHONB'][$i],
					"KD_KECAMATAN_RIWAYAT" =>$_POST['KD_KECAMATAN_PEMOHONB'][$i],
					"KD_KELURAHAN_RIWAYAT" =>$_POST['KD_KELURAHAN_PEMOHONB'][$i],
					"KD_BLOK_RIWAYAT"      =>$_POST['KD_BLOK_PEMOHONB'][$i],
					"NO_URUT_RIWAYAT"      =>$_POST['NO_URUT_PEMOHONB'][$i],
					"KD_JNS_OP_RIWAYAT"    =>$_POST['KD_JNS_OP_PEMOHONB'][$i],
					"KODE_GROUP"           => '21',
					"NIP"                  => null,
					"TANGGAL_AWAL"         => date('Y-m-d H:i:s'),
					"TANGGAL_AKHIR"        => null,
					"KETERANGAN"           => null,
					"STATUS_BERKAS"		   => null,
					"UNIT_SEBEUMNYA"       => $this->session->userdata('pbb_kg')
		 		);
		 	$this->Mpermohonan->insertriwayat($riwayatb);
 		}
 	}

 		$lampiran =$this->input->post('lampiran');

	 	if(in_array('L_PERMOHONAN', $lampiran)){$L_PERMOHONAN             ='1';}else{$L_PERMOHONAN ='0';}
		if(in_array('L_SURAT_KUASA',$lampiran)){$L_SURAT_KUASA           ='1';}else{$L_SURAT_KUASA='0';}
		if(in_array('L_KTP_WP',$lampiran)){$L_KTP_WP                     ='1';}else{$L_KTP_WP='0';}
		if(in_array('L_SERTIFIKAT_TANAH',$lampiran)){$L_SERTIFIKAT_TANAH ='1';}else{$L_SERTIFIKAT_TANAH='0';}
		if(in_array('L_SPPT',$lampiran)){$L_SPPT                         ='1';}else{$L_SPPT='0';}
		if(in_array('L_IMB',$lampiran)){$L_IMB                           ='1';}else{$L_IMB='0';}
		if(in_array('L_AKTE_JUAL_BELI',$lampiran)){$L_AKTE_JUAL_BELI     ='1';}else{$L_AKTE_JUAL_BELI='0';}
		if(in_array('L_SK_PENSIUN',$lampiran)){$L_SK_PENSIUN             ='1';}else{$L_SK_PENSIUN='0';}
		if(in_array('L_SPPT_STTS',$lampiran)){$L_SPPT_STTS               ='1';}else{$L_SPPT_STTS='0';}
		if(in_array('L_STTS',$lampiran)){$L_STTS                         ='1';}else{$L_STTS='0';}
		if(in_array('L_SK_PENGURANGAN',$lampiran)){$L_SK_PENGURANGAN     ='1';}else{$L_SK_PENGURANGAN='0';}
		if(in_array('L_SK_KEBERATAN',$lampiran)){$L_SK_KEBERATAN         ='1';}else{$L_SK_KEBERATAN='0';}
		if(in_array('L_SKKP_PBB',$lampiran)){$L_SKKP_PBB                 ='1';}else{$L_SKKP_PBB='0';}
		if(in_array('L_SPMKP_PBB',$lampiran)){$L_SPMKP_PBB               ='1';}else{$L_SPMKP_PBB='0';}
		if(in_array('L_LAIN_LAIN',$lampiran)){$L_LAIN_LAIN               ='1';}else{$L_LAIN_LAIN='0';}
		if(in_array('L_SKET_TANAH',$lampiran)){$L_SKET_TANAH             ='1';}else{$L_SKET_TANAH='0';}
		if(in_array('L_SKET_LURAH',$lampiran)){$L_SKET_LURAH             ='1';}else{$L_SKET_LURAH='0';}
		if(in_array('L_NPWPD',$lampiran)){$L_NPWPD                       ='1';}else{$L_NPWPD='0';}
		if(in_array('L_PENGHASILAN',$lampiran)){$L_PENGHASILAN           ='1';}else{$L_PENGHASILAN='0';}
		if(in_array('L_CAGAR',$lampiran)){$L_CAGAR                       ='1';}else{$L_CAGAR='0';}


	// lampiran
		$datac=array(
				'KD_KANWIL'          =>'01',
				'KD_KANTOR'          =>'01',
				'THN_PELAYANAN'      =>$rk['tahun'],
				'BUNDEL_PELAYANAN'   =>$rk['bundel'],
				'NO_URUT_PELAYANAN'  =>$rk['no_urut'],
				"L_PERMOHONAN"       =>$L_PERMOHONAN,
				"L_SURAT_KUASA"      =>$L_SURAT_KUASA,
				"L_KTP_WP"           =>$L_KTP_WP,
				"L_SERTIFIKAT_TANAH" =>$L_SERTIFIKAT_TANAH,
				"L_SPPT"             =>$L_SPPT,
				"L_IMB"              =>$L_IMB,
				"L_AKTE_JUAL_BELI"   =>$L_AKTE_JUAL_BELI,
				"L_SK_PENSIUN"       =>$L_SK_PENSIUN,
				"L_SPPT_STTS"        =>$L_SPPT_STTS,
				"L_STTS"             =>$L_STTS,
				"L_SK_PENGURANGAN"   =>$L_SK_PENGURANGAN,
				"L_SK_KEBERATAN"     =>$L_SK_KEBERATAN,
				"L_SKKP_PBB"         =>$L_SKKP_PBB,
				"L_SPMKP_PBB"        =>$L_SPMKP_PBB,
				"L_LAIN_LAIN"        =>$L_LAIN_LAIN,
				"L_SKET_TANAH"       =>$L_SKET_TANAH,
				"L_SKET_LURAH"       =>$L_SKET_LURAH,
				"L_NPWPD"            =>$L_NPWPD,
				"L_PENGHASILAN"      =>$L_PENGHASILAN,
				"L_CAGAR"            =>$L_CAGAR,
			);
			$this->db->insert('PST_LAMPIRAN',$datac);


 		// $this->db->query("commit");
		$this->db->trans_complete();

		if ($this->db->trans_status() === FALSE)
		{
			$notif="<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button> Input data gagal</div>";
			$this->session->set_flashdata('notif',$notif);
			redirect('permohonan/create');
		}else{

	 		$id=$rk['tahun'].'.'.$rk['bundel'].'.'.$rk['no_urut'];
			
			redirect('permohonan/cetak/'.$id);	
		}*/
	}

	function tambahNOPaction()
	{
		/*$mm=$this->db->query("select max(no_kolektif) no from pst_detail where thn_pelayanan='".$this->input->post('THN_PELAYANAN')."' and bundel_pelayanan='".a."' and no_urut_pelayanan='".9."'")->row();
    	$urut=$mm->NO;*/

		for ($i = 0; $i < count($_POST['KD_PROPINSI_PEMOHONB']); $i++) {
			// $no_urut=$urut+($i+1);
			$databas = array(
				'KD_KANWIL'            => '01',
				'KD_KANTOR'            => '01',
				'THN_PELAYANAN'        => $this->input->post('THN_PELAYANAN'),
				'BUNDEL_PELAYANAN'     => $this->input->post('BUNDEL_PELAYANAN'),
				'NO_URUT_PELAYANAN'    => $this->input->post('NO_URUT_PELAYANAN'),
				'KD_PROPINSI_PEMOHON'  => $_POST['KD_PROPINSI_PEMOHONB'][$i],
				'KD_DATI2_PEMOHON'     => $_POST['KD_DATI2_PEMOHONB'][$i],
				'KD_KECAMATAN_PEMOHON' => $_POST['KD_KECAMATAN_PEMOHONB'][$i],
				'KD_KELURAHAN_PEMOHON' => $_POST['KD_KELURAHAN_PEMOHONB'][$i],
				'KD_BLOK_PEMOHON'      => $_POST['KD_BLOK_PEMOHONB'][$i],
				'NO_URUT_PEMOHON'      => $_POST['NO_URUT_PEMOHONB'][$i],
				'KD_JNS_OP_PEMOHON'    => $_POST['KD_JNS_OP_PEMOHONB'][$i],
				'KD_JNS_PELAYANAN'     => $_POST['KD_JNS_PELAYANANB'][$i],
				'THN_PAJAK_PERMOHONAN' => $this->input->post('THN_PELAYANAN'),
				// 'NO_KOLEKTIF'          =>sprintf('%03s', $no_urut)
			);
			$this->db->insert('PST_DETAIL', $databas);
			/*	 	echo "nop baru ".$i." <pre>";
		 	print_r($databas);
		 	echo "</pre>";*/
			// data riwayat
			$riwayat = array(
				"KD_KANWIL"            => '01',
				"KD_KANTOR"            => '01',
				"THN_PELAYANAN"        => $this->input->post('THN_PELAYANAN'),
				"BUNDEL_PELAYANAN"     => $this->input->post('BUNDEL_PELAYANAN'),
				"NO_URUT_PELAYANAN"    => $this->input->post('NO_URUT_PELAYANAN'),
				"KD_PROPINSI_RIWAYAT"  => $_POST['KD_PROPINSI_PEMOHONB'][$i],
				"KD_DATI2_RIWAYAT"     => $_POST['KD_DATI2_PEMOHONB'][$i],
				"KD_KECAMATAN_RIWAYAT" => $_POST['KD_KECAMATAN_PEMOHONB'][$i],
				"KD_KELURAHAN_RIWAYAT" => $_POST['KD_KELURAHAN_PEMOHONB'][$i],
				"KD_BLOK_RIWAYAT"      => $_POST['KD_BLOK_PEMOHONB'][$i],
				"NO_URUT_RIWAYAT"      => $_POST['NO_URUT_PEMOHONB'][$i],
				"KD_JNS_OP_RIWAYAT"    => $_POST['KD_JNS_OP_PEMOHONB'][$i],
				"KODE_GROUP"           => $this->session->userdata('pbb_kg'),
				"NIP"                  => $this->session->userdata('nip'),
				"TANGGAL_AWAL"         => date('Y-m-d H:i:s'),
				"TANGGAL_AKHIR"        => date('Y-m-d H:i:s'),
				"KETERANGAN"           => 'Entri data permohonan',
				"STATUS_BERKAS"		   => 'Proses'
			);
			$this->Mpermohonan->insertriwayat($riwayat);

			/*	echo "<hr>";
			echo "riwayat ".$i." <pre>";
				print_r($riwayat);
			echo "<pre>";*/

			$riwayatb = array(
				"KD_KANWIL"            => '01',
				"KD_KANTOR"            => '01',
				"THN_PELAYANAN"        => $this->input->post('THN_PELAYANAN'),
				"BUNDEL_PELAYANAN"     => $this->input->post('BUNDEL_PELAYANAN'),
				"NO_URUT_PELAYANAN"    => $this->input->post('NO_URUT_PELAYANAN'),
				"KD_PROPINSI_RIWAYAT"  => $_POST['KD_PROPINSI_PEMOHONB'][$i],
				"KD_DATI2_RIWAYAT"     => $_POST['KD_DATI2_PEMOHONB'][$i],
				"KD_KECAMATAN_RIWAYAT" => $_POST['KD_KECAMATAN_PEMOHONB'][$i],
				"KD_KELURAHAN_RIWAYAT" => $_POST['KD_KELURAHAN_PEMOHONB'][$i],
				"KD_BLOK_RIWAYAT"      => $_POST['KD_BLOK_PEMOHONB'][$i],
				"NO_URUT_RIWAYAT"      => $_POST['NO_URUT_PEMOHONB'][$i],
				"KD_JNS_OP_RIWAYAT"    => $_POST['KD_JNS_OP_PEMOHONB'][$i],
				"KODE_GROUP"           => '21',
				"NIP"                  => null,
				"TANGGAL_AWAL"         => date('Y-m-d H:i:s'),
				"TANGGAL_AKHIR"        => null,
				"KETERANGAN"           => null,
				"STATUS_BERKAS"		   => null
			);
			$this->Mpermohonan->insertriwayat($riwayatb);
			/*echo "<hr>";
			echo "riwayat B ".$i." <pre>";
				print_r($riwayatb);
			echo "<pre>";*/
		}

		$this->db->query("UPDATE PST_PERMOHONAN SET STATUS_KOLEKTIF='1' WHERE THN_PELAYANAN|| '.'|| BUNDEL_PELAYANAN|| '.'|| NO_URUT_PELAYANAN='" . $_POST['idurl'] . "'");
		$notif = "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button> Tambah NOP Kolektif berhasil</div>";
		$this->session->set_flashdata('notif', $notif);
		redirect('permohonan/detail/' . $_POST['idurl']);
	}

	/* 
	public function update_action()
	{
		$this->_rules();

		if ($this->form_validation->run() == FALSE) {
			$this->update($this->input->post('id', TRUE));
		} else {
			$data = array(
				'KD_KANWIL'             => $this->input->post('KD_KANWIL', TRUE),
				'KD_KANTOR'             => $this->input->post('KD_KANTOR', TRUE),
				'THN_PELAYANAN'         => $this->input->post('THN_PELAYANAN', TRUE),
				'BUNDEL_PELAYANAN'      => $this->input->post('BUNDEL_PELAYANAN', TRUE),
				'NO_URUT_PELAYANAN'     => $this->input->post('NO_URUT_PELAYANAN', TRUE),
				'NO_SRT_PERMOHONAN'     => $this->input->post('NO_SRT_PERMOHONAN', TRUE),
				'TGL_SURAT_PERMOHONAN'  => $this->input->post('TGL_SURAT_PERMOHONAN', TRUE),
				'NAMA_PEMOHON'          => $this->input->post('NAMA_PEMOHON', TRUE),
				'ALAMAT_PEMOHON'        => $this->input->post('ALAMAT_PEMOHON', TRUE),
				'KETERANGAN_PST'        => $this->input->post('KETERANGAN_PST', TRUE),
				'CATATAN_PST'           => $this->input->post('CATATAN_PST', TRUE),
				'STATUS_KOLEKTIF'       => $this->input->post('STATUS_KOLEKTIF', TRUE),
				'TGL_TERIMA_DOKUMEN_WP' => $this->input->post('TGL_TERIMA_DOKUMEN_WP', TRUE),
				'TGL_PERKIRAAN_SELESAI' => $this->input->post('TGL_PERKIRAAN_SELESAI', TRUE),
				'NIP_PENERIMA'          => $this->input->post('NIP_PENERIMA', TRUE),
			);

			$this->Mpermohonan->update($this->input->post('id', TRUE), $data);
			$this->session->set_flashdata('message', 'Update Record Success');
			redirect(site_url('permohonan'));
		}
	}
 */
	public function delete($id)
	{
		$row = $this->Mpermohonan->get_by_id($id);

		if ($row) {
			$this->Mpermohonan->delete($id);
			$this->session->set_flashdata('message', 'Delete Record Success');
			redirect(site_url('permohonan'));
		} else {
			$this->session->set_flashdata('message', 'Record Not Found');
			redirect(site_url('permohonan'));
		}
	}

	function detail($id)
	{
		$data['rk']  = $this->Mpermohonan->getDetailpermohonan($id);
		$data['rg']  = $this->Mpermohonan->getAnggotapermohonan($id);
		$data['lap'] = $this->db->query("select * from p_ref_lampiran")->result();
		$data['dok'] = $this->Mpermohonan->getDOkterlampir($id);
		$data['file']  = $this->db->query("SELECT nama_file from p_berkas  WHERE THN_PELAYANAN|| '.'|| BUNDEL_PELAYANAN|| '.'|| NO_URUT_PELAYANAN='$id'")->result();
		$data['id'] = $id;
		/*print_r($data);
		die();*/
		$this->template->load('template', 'permohonan/detail', $data);
	}

	function detaildua($id)
	{
		$data['rk']  = $this->Mpermohonan->getDetailpermohonan($id);
		$data['rg']  = $this->Mpermohonan->getAnggotapermohonan($id);
		$data['lap'] = $this->db->query("select * from p_ref_lampiran")->result();
		$data['dok'] = $this->Mpermohonan->getDOkterlampir($id);
		$data['id']  = $id;
		$this->template->load('template', 'permohonan/detaildua', $data);
	}

	function updatepermohonan($id)
	{
		$rk = $this->Mpermohonan->getDetailpermohonan($id);
		$exp = explode('.', $id);
		$data = array(
			'button'                => 'Update Permohonan',
			'action'                => site_url('permohonan/updatepermohonanaction'),
			'id'                    => set_value('id', $id),
			'THN_PELAYANAN'         => set_value('THN_PELAYANAN', $exp[0]),
			'BUNDEL_PELAYANAN'      => set_value('BUNDEL_PELAYANAN', $exp[1]),
			'NO_URUT_PELAYANAN'     => set_value('NO_URUT_PELAYANAN', $exp[2]),
			'NO_SRT_PERMOHONAN'     => set_value('NO_SRT_PERMOHONAN', $rk->NO_SRT_PERMOHONAN),
			'TGL_SURAT_PERMOHONAN'  => set_value('TGL_SURAT_PERMOHONAN', $rk->TGL_SURAT_PERMOHONAN),
			'KETERANGAN_PST'        => set_value('KETERANGAN_PST', $rk->KETERANGAN_PST),
			'CATATAN_PST'           => set_value('CATATAN_PST', $rk->CATATAN_PST),
			'TGL_TERIMA_DOKUMEN_WP' => set_value('TGL_TERIMA_DOKUMEN_WP', $rk->TGL_TERIMA_DOKUMEN_WP),
			'TGL_PERKIRAAN_SELESAI' => set_value('TGL_PERKIRAAN_SELESAI', $rk->TGL_PERKIRAAN_SELESAI),
			'IP_EMAIL'		 		=> set_value('IP_EMAIL', $rk->IP_EMAIL),
			'IP_HP' 				=> set_value('IP_HP', $rk->IP_HP),
		);
		$this->template->load('template', 'permohonan/updatepermohonan', $data);
	}

	function cetak($id)
	{
		$rk = $this->Mpermohonan->getDetailpermohonan($id);
		if (count($rk) > 0) {
			$data['rk']  = $rk;
			$data['rg']  = $this->Mpermohonan->getAnggotapermohonan($id);
			$data['lap'] = $this->db->query("select * from p_ref_lampiran")->result();
			$data['dok'] = $this->Mpermohonan->getDOkterlampir($id);
			$data['id']  = $id;


			$html = $this->load->view('permohonan/tandaterimaasdf', $data, true);
			$rgs  = $this->load->view('permohonan/kolektif', $data, true);
			$filename = time() . ".pdf";
			$this->load->library('M_pdf');
			$mpdf       = new mPDF('utf-8', array(216, 270), '', '', '2', '2', '3', '3');

			$mpdf->SetDisplayMode('fullpage');
			$mpdf->WriteHTML($html);
			if ($rk->STATUS_KOLEKTIF == '1') {
				$mpdf->WriteHTML('<pagebreak  />');
				$mpdf->WriteHTML($rgs);
			}
			$mpdf->Output('tandaterimaa.pdf', 'I');
		}
	}

	function ceknomerlayanan()
	{
		if (isset($_POST['no'])) {
			$no = $_POST['no'];
			$res = $this->db->query("select count(*) res from pst_permohonan where THN_PELAYANAN|| '.'|| BUNDEL_PELAYANAN|| '.'|| NO_URUT_PELAYANAN='$no'")->row();

			echo $res->RES . '|' . $no;
		}
	}

	function cekriwayatbayardua()
	{
		// $var=explode('_', $_POST['var']);
		$exp = explode('.', $_POST['var']);
		if (count($exp) == 7) {
			$a = $exp[0];
			$b = $exp[1];
			$c = $exp[2];
			$d = $exp[3];
			$e = $exp[4];
			$f = $exp[5];
			$g = $exp[6];



			$rk = $this->db->query("SELECT  b.KdD_PROPINSI||'.'||b.KD_DATI2||'.'||b.KD_KECAMATAN||'.'||b.KD_KELURAHAN||'.'||b.KD_BLOK||'.'||b.NO_URUT||'.'||b.KD_JNS_OP NOP,b.THN_PAJAK_SPPT TAHUN ,PBB_YG_HARUS_DIBAYAR_SPPT  BAYAR ,TO_CHAR(TGL_PEMBAYARAN_SPPT,'dd/mm/yyyy') TANGGAL
                                     FROM  SPPT B 
                                    left  JOIN PEMBAYARAN_SPPT A ON A.KD_PROPINSI=B.KD_PROPINSI AND
                                    A.KD_DATI2=B.KD_DATI2 AND
                                    A.KD_KECAMATAN=B.KD_KECAMATAN AND
                                    A.KD_KELURAHAN=B.KD_KELURAHAN AND
                                    A.KD_BLOK=B.KD_BLOK AND
                                    A.NO_URUT=B.NO_URUT AND
                                    A.KD_JNS_OP=B.KD_JNS_OP and b.thn_pajak_sppt=a.thn_pajak_sppt
									where b.kd_propinsi='$a' and b.kd_dati2='$b' and b.kd_kecamatan='$c' and b.kd_kelurahan='$d' and b.kd_blok='$e' and b.no_urut='$f' and b.kd_jns_op='$g'
									order by to_number(b.thn_pajak_sppt) desc
									")->result();
			// if($rk){

			$this->load->view('permohonan/riwayatbayar', array('rk' => $rk));

			// }
		}
	}


	function cekriwayatbayar()
	{
		$var = explode('_', $_POST['var']);
		$exp = explode('.', $var[1]);
		if (count($exp) == 7) {
			$a = $exp[0];
			$b = $exp[1];
			$c = $exp[2];
			$d = $exp[3];
			$e = $exp[4];
			$f = $exp[5];
			$g = $exp[6];


			$rk = $this->db->query("SELECT  b.KD_PROPINSI||'.'||b.KD_DATI2||'.'||b.KD_KECAMATAN||'.'||b.KD_KELURAHAN||'.'||b.KD_BLOK||'.'||b.NO_URUT||'.'||b.KD_JNS_OP NOP,b.THN_PAJAK_SPPT TAHUN ,PBB_YG_HARUS_DIBAYAR_SPPT  BAYAR ,TO_CHAR(TGL_PEMBAYARAN_SPPT,'dd/mm/yyyy') TANGGAL
                                     FROM  SPPT B 
                                    left  JOIN PEMBAYARAN_SPPT A ON A.KD_PROPINSI=B.KD_PROPINSI AND
                                    A.KD_DATI2=B.KD_DATI2 AND
                                    A.KD_KECAMATAN=B.KD_KECAMATAN AND
                                    A.KD_KELURAHAN=B.KD_KELURAHAN AND
                                    A.KD_BLOK=B.KD_BLOK AND
                                    A.NO_URUT=B.NO_URUT AND
                                    A.KD_JNS_OP=B.KD_JNS_OP and b.thn_pajak_sppt=a.thn_pajak_sppt
									where b.kd_propinsi='$a' and b.kd_dati2='$b' and b.kd_kecamatan='$c' and b.kd_kelurahan='$d' and b.kd_blok='$e' and b.no_urut='$f' and b.kd_jns_op='$g'
									order by to_number(b.thn_pajak_sppt) desc
									")->result();
			// if($rk){
			$this->load->view('permohonan/riwayatbayar', array('rk' => $rk));
			// }
		}
	}

	function getnopel()
	{
		$kode_group = $this->session->userdata('pbb_kg');
		$tahun = $this->input->post('tahun');
		$res = $this->db->query("SELECT DISTINCT NO_LAYANAN FROM PV_DOKUMEN WHERE TAHUN='$tahun' AND KODE_GROUP='$kode_group' ORDER BY NO_LAYANAN DESC")->result();
		$option = "<option value=''>Pilih</option>";
		foreach ($res as $res) {
			$option .= "<option value='" . $res->NO_LAYANAN . "'>" . $res->NO_LAYANAN . "</option>";
		}

		echo $option;
	}

	function listdokumen()
	{

		$tahun = $this->input->post('tahun', true);

		$where = "";
		if ($tahun <> '') {
			$where .= " and tahun='" . $tahun . "'";
		} else {
			$where .= " and tahun='" . date('Y') . "'";
		}

		if (isset($_POST['cari'])) {

			$data['tahun'] = $tahun;
			$data['where'] = $where;
		} else {
			$data['tahun'] = $tahun;
			$data['where'] = $where;
		}

		$data['rk'] = "rk";
		$data['unit'] = $this->db->query("SELECT * FROM P_GROUP WHERE KODE_GROUP not in ('1','41') ORDER BY NAMA_GROUP ASC")->result();

		$this->template->load('template', 'permohonan/listdokumen', $data);
	}

	function tempatlayanan()
	{

		$tahun = $this->input->post('tahun', true);
		$nopel = $this->input->post('nopel', true);


		$where = "";
		if ($tahun <> '') {
			$where .= " and tahun='" . $tahun . "'";
		}

		if ($nopel <> '') {
			$where .= " and no_layanan='" . $nopel . "'";
		}


		if (isset($_POST['cari'])) {
			$data['rk'] = "rk";
			$data['tahun'] = $tahun;
			$data['nopel'] = $nopel;
			$data['where'] = $where;
		} else {
			$data['tahun'] = $tahun;
			$data['where'] = $where;
		}

		$data['unit'] = $this->db->query("SELECT * FROM P_GROUP WHERE KODE_GROUP not in ('1','41') ORDER BY NAMA_GROUP ASC")->result();

		$this->template->load('template', 'permohonan/tempatlayanan', $data);
	}

	function listDokKaPedanil()
	{
		echo "string";
	}

	public function jsonmonitoring()
	{
		header('Content-Type: application/json');
		$where = $this->input->post('where');
		echo $this->Mpermohonan->jsonmonitoring($where);
	}

	public function jsontempatlayanan()
	{
		header('Content-Type: application/json');
		// $tahun=$this->input->post('tahun');
		// $nopel=$this->input->post('nopel');
		$where = $this->input->post('where');
		echo $this->Mpermohonan->jsontempatlayanan($where);
	}

	public function edittempatlayanan($id)
	{

		$rk = $this->db->query("SELECT UNIT_KANTOR,IP_THN_PELAYANAN||'.'||IP_BUNDEL_PELAYANAN||'.'||IP_NO_URUT_PELAYANAN NO_LAYANAN FROM P_IP WHERE IP_THN_PELAYANAN||'.'||IP_BUNDEL_PELAYANAN||'.'||IP_NO_URUT_PELAYANAN='$id'")->row();

		if ($rk) {
			$data = array(
				'button'      => 'Edit Tempat Layanan',
				'action'      => site_url('permohonan/updatetempatlayanan'),
				'unit_kantor' => $rk->UNIT_KANTOR,
				'nopel'		  => $rk->NO_LAYANAN,
			);
			$this->template->load('template', 'permohonan/edit_tempat_layanan', $data);
		} else {
			$this->session->set_flashdata('message', 'Record Not Found');
			redirect(site_url('permohonan/tempatlayanan'));
		}
	}

	function updatetempatlayanan()
	{

		$unit_kantor = $this->input->post('unit_kantor', TRUE);
		$nopel = $this->input->post('nopel', TRUE);

		$this->db->query("UPDATE P_IP SET UNIT_KANTOR='$unit_kantor' WHERE IP_THN_PELAYANAN||'.'||IP_BUNDEL_PELAYANAN||'.'||IP_NO_URUT_PELAYANAN='$nopel' AND UNIT_KANTOR IS NOT NULL");
		$this->db->query("commit");
		$this->session->set_flashdata('message', 'Update Record Success');
		redirect(site_url('permohonan/tempatlayanan'));
	}

	function jsondatask()
	{
		header('Content-Type: application/json');
		echo $this->Mpermohonan->jsondatask();
	}


	function pengambilan()
	{
		$this->template->load('template', 'permohonan/pengambilan');
	}

	public function jsonpengambilan()
	{
		header('Content-Type: application/json');
		echo $this->Mpermohonan->jsonpengambilan();
	}

	function detailmonitoring($id)
	{
		$data['rk'] = $this->db->query("SELECT A. THN_PELAYANAN||A.BUNDEL_PELAYANAN||A.NO_URUT_PELAYANAN||A.KD_PROPINSI_PEMOHON||A.KD_DATI2_PEMOHON||A.KD_KECAMATAN_PEMOHON||A.KD_KELURAHAN_PEMOHON||A.KD_BLOK_PEMOHON||A.NO_URUT_PEMOHON||A.KD_JNS_OP_PEMOHON ID,
										A.THN_PELAYANAN||'.'|| A.BUNDEL_PELAYANAN||'.'|| A.NO_URUT_PELAYANAN NO_LAYANAN, 
										A.KD_PROPINSI_PEMOHON||'.'||A.KD_DATI2_PEMOHON||'.'||A.KD_KECAMATAN_PEMOHON||'.'||A.KD_KELURAHAN_PEMOHON||'.'||A.KD_BLOK_PEMOHON||'.'||A.NO_URUT_PEMOHON||'.'||A.KD_JNS_OP_PEMOHON NOP,
										B.NM_JENIS_PELAYANAN LAYANAN,A.THN_PELAYANAN TAHUN,CATATAN_PST,nm_pengajuan
										FROM PST_DETAIL A
										JOIN PST_PERMOHONAN D    ON     D.KD_KANWIL = A.KD_KANWIL
										AND D.KD_KANTOR = A.KD_KANTOR
										AND D.THN_PELAYANAN = A.THN_PELAYANAN
										AND D.BUNDEL_PELAYANAN = A.BUNDEL_PELAYANAN
										AND D.NO_URUT_PELAYANAN = A.NO_URUT_PELAYANAN
										JOIN REF_JNS_PELAYANAN B ON A.KD_JNS_PELAYANAN=B.KD_JNS_PELAYANAN
										WHERE A.THN_PELAYANAN||A.BUNDEL_PELAYANAN||A.NO_URUT_PELAYANAN||A.KD_PROPINSI_PEMOHON||A.KD_DATI2_PEMOHON||A.KD_KECAMATAN_PEMOHON||A.KD_KELURAHAN_PEMOHON||A.KD_BLOK_PEMOHON||NO_URUT_PEMOHON||A.KD_JNS_OP_PEMOHON='$id'")->row();
		$data['rn'] = $this->db->query("SELECT  to_char(tanggal_awal,'dd/mm/YYYY' ) awal,to_char(tanggal_akhir,'dd/mm/YYYY' ) akhir,keterangan,a.nip, nama_group posisi,status_berkas,nm_pegawai
									from p_riwayat a
									join p_group b on a.kode_group=b.kode_group
									left join pegawai c on c.nip=a.nip
									where THN_PELAYANAN||BUNDEL_PELAYANAN||NO_URUT_PELAYANAN||KD_PROPINSI_RIWAYAT||KD_DATI2_RIWAYAT||KD_KECAMATAN_RIWAYAT||KD_KELURAHAN_RIWAYAT||KD_BLOK_RIWAYAT||NO_URUT_RIWAYAT||KD_JNS_OP_RIWAYAT='$id' order by masuk desc,a.kode_group desc")->result();

		$kg = $this->session->userdata('pbb_kg');
		$hd = $this->db->query("select count(*) res
							from p_riwayat
							where THN_PELAYANAN||BUNDEL_PELAYANAN||NO_URUT_PELAYANAN||KD_PROPINSI_RIWAYAT||KD_DATI2_RIWAYAT||KD_KECAMATAN_RIWAYAT||KD_KELURAHAN_RIWAYAT||KD_BLOK_RIWAYAT||NO_URUT_RIWAYAT||KD_JNS_OP_RIWAYAT='$id' and tanggal_akhir is null and kode_group='$kg'")->row();
		$shd = $this->db->query("select count(*) res
							from p_riwayat
							where THN_PELAYANAN||BUNDEL_PELAYANAN||NO_URUT_PELAYANAN||KD_PROPINSI_RIWAYAT||KD_DATI2_RIWAYAT||KD_KECAMATAN_RIWAYAT||KD_KELURAHAN_RIWAYAT||KD_BLOK_RIWAYAT||NO_URUT_RIWAYAT||KD_JNS_OP_RIWAYAT='$id'  and kode_group='$kg'")->row();

		$data['ID'] = $id;
		$data['button'] = $hd->RES;
		$data['btn']    = $shd->RES;

		$data['unit'] = $this->db->query("SELECT * FROM P_GROUP WHERE KODE_GROUP not in ('1','41') ORDER BY NAMA_GROUP ASC")->result();
		$this->template->load('template', 'permohonan/detailmonitoring', $data);
	}


	function detailpengambilan($id)
	{
		$data['rk'] = $this->db->query("SELECT  
									        THN_PELAYANAN||BUNDEL_PELAYANAN||NO_URUT_PELAYANAN||KD_PROPINSI_PEMOHON||KD_DATI2_PEMOHON||KD_KECAMATAN_PEMOHON||KD_KELURAHAN_PEMOHON||KD_BLOK_PEMOHON||NO_URUT_PEMOHON||KD_JNS_OP_PEMOHON ID,
									        THN_PELAYANAN||'.'|| BUNDEL_PELAYANAN||'.'|| NO_URUT_PELAYANAN NO_LAYANAN, 
									        KD_PROPINSI_PEMOHON||'.'||KD_DATI2_PEMOHON||'.'||KD_KECAMATAN_PEMOHON||'.'||KD_KELURAHAN_PEMOHON||'.'||KD_BLOK_PEMOHON||'.'||NO_URUT_PEMOHON||'.'||KD_JNS_OP_PEMOHON NOP,
									        NM_JENIS_PELAYANAN LAYANAN,THN_PELAYANAN TAHUN
									FROM PST_DETAIL A
									JOIN REF_JNS_PELAYANAN B ON A.KD_JNS_PELAYANAN=B.KD_JNS_PELAYANAN
									WHERE THN_PELAYANAN||BUNDEL_PELAYANAN||NO_URUT_PELAYANAN||KD_PROPINSI_PEMOHON||KD_DATI2_PEMOHON||KD_KECAMATAN_PEMOHON||KD_KELURAHAN_PEMOHON||KD_BLOK_PEMOHON||NO_URUT_PEMOHON||KD_JNS_OP_PEMOHON='$id'
									")->row();
		$data['rn'] = $this->db->query("select to_char(tanggal_awal,'d Mon YYYY' ) awal,to_char(tanggal_akhir,'d Mon YYYY' ) akhir,keterangan,nip, nama_group posisi,status_berkas
									from p_riwayat a
									join p_group b on a.kode_group=b.kode_group
									where THN_PELAYANAN||BUNDEL_PELAYANAN||NO_URUT_PELAYANAN||KD_PROPINSI_RIWAYAT||KD_DATI2_RIWAYAT||KD_KECAMATAN_RIWAYAT||KD_KELURAHAN_RIWAYAT||KD_BLOK_RIWAYAT||NO_URUT_RIWAYAT||KD_JNS_OP_RIWAYAT='$id' order by masuk desc,a.kode_group desc")->result();

		$kg = $this->session->userdata('pbb_kg');
		$hd = $this->db->query("select count(*) res
							from p_riwayat
							where THN_PELAYANAN||BUNDEL_PELAYANAN||NO_URUT_PELAYANAN||KD_PROPINSI_RIWAYAT||KD_DATI2_RIWAYAT||KD_KECAMATAN_RIWAYAT||KD_KELURAHAN_RIWAYAT||KD_BLOK_RIWAYAT||NO_URUT_RIWAYAT||KD_JNS_OP_RIWAYAT='$id' and tanggal_akhir is null and kode_group='$kg'")->row();
		$shd = $this->db->query("select count(*) res
							from p_riwayat
							where THN_PELAYANAN||BUNDEL_PELAYANAN||NO_URUT_PELAYANAN||KD_PROPINSI_RIWAYAT||KD_DATI2_RIWAYAT||KD_KECAMATAN_RIWAYAT||KD_KELURAHAN_RIWAYAT||KD_BLOK_RIWAYAT||NO_URUT_RIWAYAT||KD_JNS_OP_RIWAYAT='$id'  and kode_group='$kg'")->row();



		$data['button'] = $hd->RES;
		$data['btn']    = $shd->RES;

		$data['unit'] = $this->db->query("SELECT * FROM p_group where kode_group ='36'")->row();
		$this->template->load('template', 'permohonan/detailpengambilan', $data);
	}


	function proseskirimdokumen()
	{
		$KD_KANWIL            = $_POST['KD_KANWIL'];
		$KD_KANTOR            = $_POST['KD_KANTOR'];
		$THN_PELAYANAN        = $_POST['THN_PELAYANAN'];
		$BUNDEL_PELAYANAN     = $_POST['BUNDEL_PELAYANAN'];
		$NO_URUT_PELAYANAN    = $_POST['NO_URUT_PELAYANAN'];
		$KD_PROPINSI_RIWAYAT  = $_POST['KD_PROPINSI_RIWAYAT'];
		$KD_DATI2_RIWAYAT     = $_POST['KD_DATI2_RIWAYAT'];
		$KD_KECAMATAN_RIWAYAT = $_POST['KD_KECAMATAN_RIWAYAT'];
		$KD_KELURAHAN_RIWAYAT = $_POST['KD_KELURAHAN_RIWAYAT'];
		$KD_BLOK_RIWAYAT      = $_POST['KD_BLOK_RIWAYAT'];
		$NO_URUT_RIWAYAT      = $_POST['NO_URUT_RIWAYAT'];
		$KD_JNS_OP_RIWAYAT    = $_POST['KD_JNS_OP_RIWAYAT'];
		$NIP                  = $_POST['NIP'];
		$TANGGAL_AWAL         = $_POST['TANGGAL_AWAL'];
		$KODE_GROUP           = $_POST['KODE_GROUP'];
		$KETERANGAN           = $_POST['KETERANGAN'];
		$TANGGAL_AKHIR        = $_POST['TANGGAL_AKHIR'];
		$STATUS_BERKAS		  = $_POST['STATUS_BERKAS'];

		$dataupdate = array(
			'KD_KANWIL'            => $KD_KANWIL,
			'KD_KANTOR'            => $KD_KANTOR,
			'THN_PELAYANAN'        => $THN_PELAYANAN,
			'BUNDEL_PELAYANAN'     => $BUNDEL_PELAYANAN,
			'NO_URUT_PELAYANAN'    => $NO_URUT_PELAYANAN,
			'KD_PROPINSI_RIWAYAT'  => $KD_PROPINSI_RIWAYAT,
			'KD_DATI2_RIWAYAT'     => $KD_DATI2_RIWAYAT,
			'KD_KECAMATAN_RIWAYAT' => $KD_KECAMATAN_RIWAYAT,
			'KD_KELURAHAN_RIWAYAT' => $KD_KELURAHAN_RIWAYAT,
			'KD_BLOK_RIWAYAT'      => $KD_BLOK_RIWAYAT,
			'NO_URUT_RIWAYAT'      => $NO_URUT_RIWAYAT,
			'KD_JNS_OP_RIWAYAT'    => $KD_JNS_OP_RIWAYAT,
			'KODE_GROUP'           => $this->session->userdata('pbb_kg'),
			'NIP'                  => $NIP,
			'KETERANGAN'           => $KETERANGAN,
			'TANGGAL_AKHIR'        => $TANGGAL_AKHIR,
			'STATUS_BERKAS'        => $STATUS_BERKAS
		);

		$datanext = array(
			"KD_KANWIL"            => $KD_KANWIL,
			"KD_KANTOR"            => $KD_KANTOR,
			"THN_PELAYANAN"        => $THN_PELAYANAN,
			"BUNDEL_PELAYANAN"     => $BUNDEL_PELAYANAN,
			"NO_URUT_PELAYANAN"    => $NO_URUT_PELAYANAN,
			"KD_PROPINSI_RIWAYAT"  => $KD_PROPINSI_RIWAYAT,
			"KD_DATI2_RIWAYAT"     => $KD_DATI2_RIWAYAT,
			"KD_KECAMATAN_RIWAYAT" => $KD_KECAMATAN_RIWAYAT,
			"KD_KELURAHAN_RIWAYAT" => $KD_KELURAHAN_RIWAYAT,
			"KD_BLOK_RIWAYAT"      => $KD_BLOK_RIWAYAT,
			"NO_URUT_RIWAYAT"      => $NO_URUT_RIWAYAT,
			"KD_JNS_OP_RIWAYAT"    => $KD_JNS_OP_RIWAYAT,
			"KODE_GROUP"           => $KODE_GROUP,
			"NIP"                  => null,
			"TANGGAL_AWAL"         => $TANGGAL_AWAL,
			"TANGGAL_AKHIR"        => null,
			"KETERANGAN"           => null,
			"UNIT_SEBEUMNYA"       => $this->session->userdata('pbb_kg')
		);
		$this->Mpermohonan->updateriwayat($dataupdate);
		if ($STATUS_BERKAS != 'Diambil') {
			$this->Mpermohonan->insertriwayat($datanext);
		} else {
			$this->prosesselesai();
		}

		redirect('permohonan/detailmonitoring/' . $THN_PELAYANAN . $BUNDEL_PELAYANAN . $NO_URUT_PELAYANAN . $KD_PROPINSI_RIWAYAT . $KD_DATI2_RIWAYAT . $KD_KECAMATAN_RIWAYAT . $KD_KELURAHAN_RIWAYAT . $KD_BLOK_RIWAYAT . $NO_URUT_RIWAYAT . $KD_JNS_OP_RIWAYAT);
	}

	function proseskirimdokumenambil()
	{
		$KD_KANWIL            = $_POST['KD_KANWIL'];
		$KD_KANTOR            = $_POST['KD_KANTOR'];
		$THN_PELAYANAN        = $_POST['THN_PELAYANAN'];
		$BUNDEL_PELAYANAN     = $_POST['BUNDEL_PELAYANAN'];
		$NO_URUT_PELAYANAN    = $_POST['NO_URUT_PELAYANAN'];
		$KD_PROPINSI_RIWAYAT  = $_POST['KD_PROPINSI_RIWAYAT'];
		$KD_DATI2_RIWAYAT     = $_POST['KD_DATI2_RIWAYAT'];
		$KD_KECAMATAN_RIWAYAT = $_POST['KD_KECAMATAN_RIWAYAT'];
		$KD_KELURAHAN_RIWAYAT = $_POST['KD_KELURAHAN_RIWAYAT'];
		$KD_BLOK_RIWAYAT      = $_POST['KD_BLOK_RIWAYAT'];
		$NO_URUT_RIWAYAT      = $_POST['NO_URUT_RIWAYAT'];
		$KD_JNS_OP_RIWAYAT    = $_POST['KD_JNS_OP_RIWAYAT'];
		$NIP                  = $_POST['NIP'];
		$TANGGAL_AWAL         = $_POST['TANGGAL_AWAL'];
		$KODE_GROUP           = $_POST['KODE_GROUP'];
		$KETERANGAN           = $_POST['KETERANGAN'];
		$TANGGAL_AKHIR        = $_POST['TANGGAL_AKHIR'];
		$STATUS_BERKAS		  = $_POST['STATUS_BERKAS'];

		$dataupdate = array(
			'KD_KANWIL'            => $KD_KANWIL,
			'KD_KANTOR'            => $KD_KANTOR,
			'THN_PELAYANAN'        => $THN_PELAYANAN,
			'BUNDEL_PELAYANAN'     => $BUNDEL_PELAYANAN,
			'NO_URUT_PELAYANAN'    => $NO_URUT_PELAYANAN,
			'KD_PROPINSI_RIWAYAT'  => $KD_PROPINSI_RIWAYAT,
			'KD_DATI2_RIWAYAT'     => $KD_DATI2_RIWAYAT,
			'KD_KECAMATAN_RIWAYAT' => $KD_KECAMATAN_RIWAYAT,
			'KD_KELURAHAN_RIWAYAT' => $KD_KELURAHAN_RIWAYAT,
			'KD_BLOK_RIWAYAT'      => $KD_BLOK_RIWAYAT,
			'NO_URUT_RIWAYAT'      => $NO_URUT_RIWAYAT,
			'KD_JNS_OP_RIWAYAT'    => $KD_JNS_OP_RIWAYAT,
			'KODE_GROUP'           => $this->session->userdata('pbb_kg'),
			'NIP'                  => $NIP,
			'KETERANGAN'           => $KETERANGAN,
			'TANGGAL_AKHIR'        => $TANGGAL_AKHIR,
			'STATUS_BERKAS'		   => $STATUS_BERKAS
		);

		$datanext = array(
			"KD_KANWIL"            => $KD_KANWIL,
			"KD_KANTOR"            => $KD_KANTOR,
			"THN_PELAYANAN"        => $THN_PELAYANAN,
			"BUNDEL_PELAYANAN"     => $BUNDEL_PELAYANAN,
			"NO_URUT_PELAYANAN"    => $NO_URUT_PELAYANAN,
			"KD_PROPINSI_RIWAYAT"  => $KD_PROPINSI_RIWAYAT,
			"KD_DATI2_RIWAYAT"     => $KD_DATI2_RIWAYAT,
			"KD_KECAMATAN_RIWAYAT" => $KD_KECAMATAN_RIWAYAT,
			"KD_KELURAHAN_RIWAYAT" => $KD_KELURAHAN_RIWAYAT,
			"KD_BLOK_RIWAYAT"      => $KD_BLOK_RIWAYAT,
			"NO_URUT_RIWAYAT"      => $NO_URUT_RIWAYAT,
			"KD_JNS_OP_RIWAYAT"    => $KD_JNS_OP_RIWAYAT,
			"KODE_GROUP"           => $KODE_GROUP,
			"NIP"                  => null,
			"TANGGAL_AWAL"         => $TANGGAL_AWAL,
			"TANGGAL_AKHIR"        => null,
			"KETERANGAN"           => null,
			"UNIT_SEBEUMNYA"       => $this->session->userdata('pbb_kg')
		);
		$this->Mpermohonan->updateriwayat($dataupdate);
		if ($STATUS_BERKAS != 'Diambil') {
			$this->Mpermohonan->insertriwayat($datanext);
		} else {
			$this->prosesselesai();
		}

		redirect('permohonan/detailpengambilan/' . $THN_PELAYANAN . $BUNDEL_PELAYANAN . $NO_URUT_PELAYANAN . $KD_PROPINSI_RIWAYAT . $KD_DATI2_RIWAYAT . $KD_KECAMATAN_RIWAYAT . $KD_KELURAHAN_RIWAYAT . $KD_BLOK_RIWAYAT . $NO_URUT_RIWAYAT . $KD_JNS_OP_RIWAYAT);
	}

	function prosesselesai()
	{
		$KD_KANWIL            = $_POST['KD_KANWIL'];
		$KD_KANTOR            = $_POST['KD_KANTOR'];
		$THN_PELAYANAN        = $_POST['THN_PELAYANAN'];
		$BUNDEL_PELAYANAN     = $_POST['BUNDEL_PELAYANAN'];
		$NO_URUT_PELAYANAN    = $_POST['NO_URUT_PELAYANAN'];
		$KD_PROPINSI_RIWAYAT  = $_POST['KD_PROPINSI_RIWAYAT'];
		$KD_DATI2_RIWAYAT     = $_POST['KD_DATI2_RIWAYAT'];
		$KD_KECAMATAN_RIWAYAT = $_POST['KD_KECAMATAN_RIWAYAT'];
		$KD_KELURAHAN_RIWAYAT = $_POST['KD_KELURAHAN_RIWAYAT'];
		$KD_BLOK_RIWAYAT      = $_POST['KD_BLOK_RIWAYAT'];
		$NO_URUT_RIWAYAT      = $_POST['NO_URUT_RIWAYAT'];
		$KD_JNS_OP_RIWAYAT    = $_POST['KD_JNS_OP_RIWAYAT'];
		$NIP                  = $_POST['NIP'];
		$TANGGAL_AWAL         = $_POST['TANGGAL_AWAL'];
		$KODE_GROUP           = $_POST['KODE_GROUP'];
		$KETERANGAN           = $_POST['KETERANGAN'];
		$TANGGAL_AKHIR        = $_POST['TANGGAL_AKHIR'];
		$STATUS_BERKAS		  = $_POST['STATUS_BERKAS'];

		$dataupdate = array(
			'KD_KANWIL'            => $KD_KANWIL,
			'KD_KANTOR'            => $KD_KANTOR,
			'THN_PELAYANAN'        => $THN_PELAYANAN,
			'BUNDEL_PELAYANAN'     => $BUNDEL_PELAYANAN,
			'NO_URUT_PELAYANAN'    => $NO_URUT_PELAYANAN,
			'KD_PROPINSI_RIWAYAT'  => $KD_PROPINSI_RIWAYAT,
			'KD_DATI2_RIWAYAT'     => $KD_DATI2_RIWAYAT,
			'KD_KECAMATAN_RIWAYAT' => $KD_KECAMATAN_RIWAYAT,
			'KD_KELURAHAN_RIWAYAT' => $KD_KELURAHAN_RIWAYAT,
			'KD_BLOK_RIWAYAT'      => $KD_BLOK_RIWAYAT,
			'NO_URUT_RIWAYAT'      => $NO_URUT_RIWAYAT,
			'KD_JNS_OP_RIWAYAT'    => $KD_JNS_OP_RIWAYAT,
			'KODE_GROUP'           => $this->session->userdata('pbb_kg'),
			'NIP'                  => $NIP,
			'KETERANGAN'           => $KETERANGAN,
			'TANGGAL_AKHIR'        => $TANGGAL_AKHIR,
			'STATUS_BERKAS'		   => $STATUS_BERKAS
		);

		$this->Mpermohonan->updateriwayat($dataupdate);
		$this->db->query("UPDATE pst_detail set tgl_selesai=TO_DATE('$TANGGAL_AKHIR', 'yyyy-mm-dd hh24:mi:ss') , status_selesai='1'
							where KD_KANWIL ='$KD_KANWIL'
							and KD_KANTOR ='$KD_KANTOR'
							and THN_PELAYANAN ='$THN_PELAYANAN'
							and BUNDEL_PELAYANAN ='$BUNDEL_PELAYANAN'
							and NO_URUT_PELAYANAN ='$NO_URUT_PELAYANAN'
							and KD_PROPINSI_PEMOHON ='$KD_PROPINSI_RIWAYAT'
							and KD_DATI2_PEMOHON ='$KD_DATI2_RIWAYAT'
							and KD_KECAMATAN_PEMOHON ='$KD_KECAMATAN_RIWAYAT'
							and KD_KELURAHAN_PEMOHON ='$KD_KELURAHAN_RIWAYAT'
							and KD_BLOK_PEMOHON ='$KD_BLOK_RIWAYAT'
							and NO_URUT_PEMOHON ='$NO_URUT_RIWAYAT'
							and KD_JNS_OP_PEMOHON ='$KD_JNS_OP_RIWAYAT'");




		// redirect('permohonan/detailmonitoring/'.$THN_PELAYANAN.$BUNDEL_PELAYANAN.$NO_URUT_PELAYANAN.$KD_PROPINSI_RIWAYAT.$KD_DATI2_RIWAYAT.$KD_KECAMATAN_RIWAYAT.$KD_KELURAHAN_RIWAYAT.$KD_BLOK_RIWAYAT.$NO_URUT_RIWAYAT.$KD_JNS_OP_RIWAYAT);    	
	}


	function monitoring()
	{
		if (isset($_POST['filter'])) {
			$filter               = $_POST['filter'];
			switch ($filter) {
				case 'No Layanan':
					# code...

					$nl = $_POST['no_layanan'];
					$wh = "where no_layanan='$nl'";
					$data['no_layanan'] = $nl;
					break;
				case 'NOP':
					# code...
					$KD_PROPINSI_PEMOHON  = $_POST['KD_PROPINSI_PEMOHON'];
					$KD_DATI2_PEMOHON     = $_POST['KD_DATI2_PEMOHON'];
					$KD_KECAMATAN_PEMOHON = $_POST['KD_KECAMATAN_PEMOHON'];
					$KD_KELURAHAN_PEMOHON = $_POST['KD_KELURAHAN_PEMOHON'];
					$KD_BLOK_PEMOHON      = $_POST['KD_BLOK_PEMOHON'];
					$NO_URUT_PEMOHON      = $_POST['NO_URUT_PEMOHON'];
					$KD_JNS_OP_PEMOHON    = $_POST['KD_JNS_OP_PEMOHON'];
					$nop                          = $KD_PROPINSI_PEMOHON . '.' . $KD_DATI2_PEMOHON . '.' . $KD_KECAMATAN_PEMOHON . '.' . $KD_KELURAHAN_PEMOHON . '.' . $KD_BLOK_PEMOHON . '.' . $NO_URUT_PEMOHON . '.' . $KD_JNS_OP_PEMOHON;
					$wh                           = "where nop='$nop'";
					$data['KD_PROPINSI_PEMOHON']  = $KD_PROPINSI_PEMOHON;
					$data['KD_DATI2_PEMOHON']     = $KD_DATI2_PEMOHON;
					$data['KD_KECAMATAN_PEMOHON'] = $KD_KECAMATAN_PEMOHON;
					$data['KD_KELURAHAN_PEMOHON'] = $KD_KELURAHAN_PEMOHON;
					$data['KD_BLOK_PEMOHON']      = $KD_BLOK_PEMOHON;
					$data['NO_URUT_PEMOHON']      = $NO_URUT_PEMOHON;
					$data['KD_JNS_OP_PEMOHON']    = $KD_JNS_OP_PEMOHON;

					break;
			}
			$data['filter'] = $filter;
			$data['res']    = $this->db->query("select * from PV_MONITOR $wh")->result();
		} else {
			$data['filter'] = 'No Layanan';
			$data['no_layanan'] = null;
		}


		$this->template->load('template', 'permohonan/formmonitoring', $data);
	}

	function viewmonitoring()
	{
		if (isset($_POST['id'])) {
			$id = $_POST['id'];
			$data['rn'] = $this->db->query("select to_char(tanggal_awal,'dd Mon YYYY' ) awal,to_char(tanggal_akhir,'dd Mon YYYY' ) akhir,keterangan,nip, nama_group posisi,status_berkas
									from p_riwayat a
									join p_group b on a.kode_group=b.kode_group
									where THN_PELAYANAN||BUNDEL_PELAYANAN||NO_URUT_PELAYANAN||KD_PROPINSI_RIWAYAT||KD_DATI2_RIWAYAT||KD_KECAMATAN_RIWAYAT||KD_KELURAHAN_RIWAYAT||KD_BLOK_RIWAYAT||NO_URUT_RIWAYAT||KD_JNS_OP_RIWAYAT='$id' order by masuk desc,a.kode_group desc")->result();
			$this->load->view('permohonan/kontenmodalmonitor', $data);
		}
	}



	function datadetailpermohonan()
	{
		if (isset($_POST['p_tanggal1']) && isset($_POST['p_tanggal2'])) {
			$tanggal1 = $_POST['p_tanggal1'];
			$tanggal2 = $_POST['p_tanggal2'];
		} else {
			$tanggal1 = date('d-m-Y');
			$tanggal2 = date('d-m-Y');
		}
		$sess = array(
			'p_tanggal1' => $tanggal1,
			'p_tanggal2' => $tanggal2
		);
		$this->session->set_userdata($sess);
		$this->template->load('template', 'permohonan/datadetailpermohonan');
	}

	public function jsondatadetailpermohonan()
	{
		header('Content-Type: application/json');
		echo $this->Mpermohonan->jsondatadetailpermohonan();
	}

	function cekmapiinglampiran()
	{
		if (isset($_POST['KD_JNS_PELAYANAN'])) {
			$KD_JNS_PELAYANAN = $_POST['KD_JNS_PELAYANAN'];
			$data['rk'] = $this->db->query("SELECT KODE,NAMA_LAMPIRAN,case when  PCEKLAMPIRAN('" . $KD_JNS_PELAYANAN . "',KODE)=1 then 'selected' else null end ST FROM P_REF_LAMPIRAN order by nama_lampiran asc")->result();
			$this->load->view('permohonan/ambillampiran', $data);
		}
	}

	////ADY//
	function json_berkas()
	{
		header('Content-Type: application/json');
		echo $this->Mpermohonan->json_berkas();
	}
	function json_berkas_kurang()
	{
		header('Content-Type: application/json');
		echo $this->Mpermohonan->json_berkas_kurang();
	}
	public function upload_berkas()
	{
		if (isset($_POST['p_tanggal1']) && isset($_POST['p_tanggal2'])) {
			$tanggal1 = $_POST['p_tanggal1'];
			$tanggal2 = $_POST['p_tanggal2'];
		} else {
			$tanggal1 = date('d-m-Y');
			$tanggal2 = date('d-m-Y');
		}
		$sess = array(
			'p_tanggal1' => $tanggal1,
			'p_tanggal2' => $tanggal2
		);
		$this->session->set_userdata($sess);

		$this->template->load('template', 'permohonan/upload_berkas_permohonan_list');
	}
	function form_upload($id)
	{
		$data['rk']  = $this->Mpermohonan->getDetailpermohonan($id);
		$data['file']  = $this->db->query("SELECT nama_file from p_berkas  WHERE THN_PELAYANAN|| '.'|| BUNDEL_PELAYANAN|| '.'|| NO_URUT_PELAYANAN='$id'")->result();
		$data['id'] = $id;
		$this->template->load('template', 'permohonan/form_upload', $data);
	}

	function insert_data_upload()
	{
		$data_upload = array(
			"THN_PELAYANAN"        => $this->input->post('THN_PELAYANAN'),
			"BUNDEL_PELAYANAN"     => $this->input->post('BUNDEL_PELAYANAN'),
			"NO_URUT_PELAYANAN"    => $this->input->post('NO_URUT_PELAYANAN'),
			"NAMA_FILE"  		   => $this->input->post('nama_file')
		);
		$this->db->insert('P_BERKAS', $data_upload);
		redirect('permohonan/upload_berkas');
	}
	function uploadimg()
	{

		$ds          = DIRECTORY_SEPARATOR;  //1
		$storeFolder = 'uploads';   //2
		$th 		 = $_POST['THN_PELAYANAN'];
		$bp 		 = $_POST['BUNDEL_PELAYANAN'];
		$nu 		 = $_POST['NO_URUT_PELAYANAN'];

		if (!empty($_FILES)) {
			$tempFile   = $_FILES['file']['tmp_name'];          //3             

			$nf         = $storeFolder . $ds . $th . $bp . $nu . '_' . $_FILES['file']['name'];
			$nfd		= $th . $bp . $nu . '_' . $_FILES['file']['name'];
			//$cek        =$this->db->query("SELECT * FROM temp_upload WHERE jenis='client' and namagambar='$nf' AND pengguna_id='$iu'")->row();
			//if(count($cek)==0){
			move_uploaded_file($tempFile, $nf); //6
			$data_upload = array(
				"THN_PELAYANAN"        => $this->input->post('THN_PELAYANAN'),
				"BUNDEL_PELAYANAN"     => $this->input->post('BUNDEL_PELAYANAN'),
				"NO_URUT_PELAYANAN"    => $this->input->post('NO_URUT_PELAYANAN'),
				"NAMA_FILE"  		   => $nfd,
				"NIP_UPLOAD"		   => $this->session->userdata('nip')
			);
			$this->db->insert('P_BERKAS', $data_upload);
			// }
		}
	}
	function hapusimg()
	{
		$namagambar = $_POST['namagambar'];
		/*$iu         =$this->session->userdata('dku');*/
		$nf         = "uploads/" . $namagambar;
		@unlink($nf);
		$this->db->query("DELETE FROM P_BERKAS WHERE  NAMA_FILE = '$namagambar'");
	}
	function hapus_data_img()
	{
		$namagambar = $_POST['del_id'];

		$nf         = "uploads/" . $namagambar;
		@unlink($nf);
		$this->db->query("DELETE FROM P_BERKAS WHERE  NAMA_FILE = '$namagambar'");
	}

	function json_ferifikasi_berkas()
	{
		header('Content-Type: application/json');
		echo $this->Mpermohonan->json_ferifikasi_berkas();
	}
	public function verifikasi_berkas()
	{
		if (isset($_GET['p_tanggal1']) && isset($_GET['p_tanggal2']) && isset($_GET['KD_JNS_PELAYANAN'])) {
			$tanggal1 = $_GET['p_tanggal1'];
			$tanggal2 = $_GET['p_tanggal2'];
			$KD_JNS_PELAYANAN = $_GET['KD_JNS_PELAYANAN'];
		} else {
			$tanggal1 = date('d-m-Y');
			$tanggal2 = date('d-m-Y');
			$KD_JNS_PELAYANAN = '';
		}
		$sess = array(
			'p_tanggal1' => $tanggal1,
			'p_tanggal2' => $tanggal2,
			'P_KD_JNS_PELAYANAN' => $KD_JNS_PELAYANAN
		);
		$this->session->set_userdata($sess);
		$data['pelayanan'] = $this->db->query("select a.kd_jns_pelayanan,NM_JENIS_PELAYANAN from pst_detail a
							join ref_jns_pelayanan b on a.kd_jns_pelayanan=b.kd_jns_pelayanan
							group by a.kd_jns_pelayanan,NM_JENIS_PELAYANAN")->result();
		$this->template->load('template', 'permohonan/verifikasi_berkas', $data);
	}
	public function verifikasi_detail($id = "")
	{
		$data['rk']  = $this->db->query("select * from  PV_PERMOHONAN_VER_BERKAS where no_layanan='$id'")->row();
		$data['lap'] = $this->db->query("select * from p_ref_lampiran")->result();
		$data['dok'] = $this->Mpermohonan->getDok_Ketlampiran($id);
		$data['id']  = $id;
		$this->template->load('template', 'permohonan/verifikasi_detail', $data);
	}
	public function update_berkas1()
	{
		$id = $_POST['id'];
		$exp = explode('|', $id);
		$exp1 = explode('.', $exp[1]);
		$rk = $this->db->query("SELECT THN_PELAYANAN FROM P_LAMPIRAN WHERE THN_PELAYANAN|| '.'|| BUNDEL_PELAYANAN|| '.'|| NO_URUT_PELAYANAN='$exp[1]'")->row();
		if ($exp[2] == 'PST') {
			$this->db->query("UPDATE PST_LAMPIRAN SET " . $exp[0] . "='1' WHERE THN_PELAYANAN|| '.'|| BUNDEL_PELAYANAN|| '.'|| NO_URUT_PELAYANAN='$exp[1]'");
		} else {
			if (count($rk) <= 0) {
				$and = "," . $exp[0];
				$val = ",'1'";
				$this->db->query("INSERT INTO P_LAMPIRAN (THN_PELAYANAN, BUNDEL_PELAYANAN, NO_URUT_PELAYANAN " . $and . ") VALUES ('$exp1[0]', '$exp1[1]', '$exp1[2]' " . $val . ")");
			} else {
				$this->db->query("UPDATE P_LAMPIRAN SET " . $exp[0] . "='1' WHERE THN_PELAYANAN|| '.'|| BUNDEL_PELAYANAN|| '.'|| NO_URUT_PELAYANAN='$exp[1]'");
			}
		}
		echo $exp[0];
	}
	public function update_berkas0()
	{
		$id = $_POST['id'];
		$exp = explode('|', $id);
		if ($exp[2] == 'PST') {
			$this->db->query("UPDATE PST_LAMPIRAN SET " . $exp[0] . "='0' WHERE THN_PELAYANAN|| '.'|| BUNDEL_PELAYANAN|| '.'|| NO_URUT_PELAYANAN='$exp[1]'");
		} else {
			$this->db->query("UPDATE P_LAMPIRAN SET " . $exp[0] . "='0' WHERE THN_PELAYANAN|| '.'|| BUNDEL_PELAYANAN|| '.'|| NO_URUT_PELAYANAN='$exp[1]'");
		}
		//echo count($rk);

	}
	function cetak_lembar_penelitian($id = "", $STATUS = "")
	{
		$dok = $this->Mpermohonan->getDOkterlampir($id);
		$detail = $this->db->query("select * from  PV_PERMOHONAN_VER_BERKAS where no_layanan='$id'")->row();
		if ($STATUS == '1') {
			$count = $this->db->query("SELECT count(*)jumlah from pst_detail WHERE THN_PELAYANAN|| '.'|| BUNDEL_PELAYANAN|| '.'|| NO_URUT_PELAYANAN='$id'")->row();
			// membaca data dari form

			// memanggil dan membaca template dokumen yang telah kita buat
			$document = file_get_contents("kolektif.rtf");
			// isi dokumen dinyatakan dalam bentuk string
			$document = str_replace("#JENIS_P", $detail->NM_PENGAJUAN, $document);
			$document = str_replace("#KATEGORI_P", $detail->KET_KOLEKTIF, $document);
			$document = str_replace("#NO_SURAT", $detail->KD_PENGAJUAN, $document);
			$document = str_replace("#NO_SURAT", $detail->KD_PENGAJUAN, $document);
			$document = str_replace("#TGL_SURAT", date('d/m/Y', strtotime($detail->TGL_SURAT_PERMOHONAN)), $document);
			$document = str_replace("#TGL_TERIMA", $detail->TGL_TERIMA, $document);
			$document = str_replace("#NO_LAYANAN", $detail->NO_LAYANAN, $document);
			$document = str_replace("#NAMA", $detail->NAMA_PEMOHON, $document);
			$document = str_replace("#ALMT", $detail->ALAMAT_PEMOHON, $document);
			$document = str_replace("#TAHUN_PAJAK", date('Y'), $document);
			$document = str_replace("#JUMLAH_SPPT", $count->JUMLAH, $document);

			//isi tebel
			/*if () {} else {}
				if () {} else {}	           
				if () {} else {}
				if () {} else {}
				if () {} else {}
				if () {} else {}	*/
			if ($dok->L_PERMOHONAN == '1') {
				$document = str_replace("#SKP", 'V', $document);
				$document = str_replace("NOTMAIL", '', $document);
			} else {
				$document = str_replace("#SKP", '', $document);
				$document = str_replace("NOTMAIL", 'V', $document);
			}

			$document = str_replace("#TULIS", 'V', $document);
			$document = str_replace("NOWRITE", '', $document);
			$document = str_replace("#BPD", '', $document);
			$document = str_replace("DONTBPD", '', $document);
			if ($dok->L_SKET_LURAH == '1') {
				$document = str_replace("#LURAH", 'V', $document);
				$document = str_replace("NOTLURAH", '', $document);
			} else {
				$document = str_replace("#LURAH", '', $document);
				$document = str_replace("NOTLURAH", 'V', $document);
			}

			$document = str_replace("#SPOP", '', $document);
			$document = str_replace("NOTSPOP", '', $document);
			if ($dok->L_KTP_WP == '1') {
				$document = str_replace("#KTP", 'V', $document);
				$document = str_replace("DONTKTP", '', $document);
			} else {
				$document = str_replace("#KTP", '', $document);
				$document = str_replace("DONTKTP", 'V', $document);
			}

			if ($dok->L_SERTIFIKAT_TANAH == '1') {
				$document = str_replace("#KEPEMILIKAN", 'V', $document);
				$document = str_replace("DONTME", '', $document);
			} else {
				$document = str_replace("#KEPEMILIKAN", '', $document);
				$document = str_replace("DONTME", 'V', $document);
			}
			$document = str_replace("#SPPT_TETANGGA", '', $document);
			$document = str_replace("NOTME", '', $document);
			if ($dok->L_SURAT_KUASA == '1') {
				$document = str_replace("#KUASA", 'V', $document);
				$document = str_replace("AGE", '', $document);
			} else {
				$document = str_replace("#KUASA", '', $document);
				$document = str_replace("AGE", 'V', $document);
			}

			$document = str_replace("#WAKTU", '', $document);
			$document = str_replace("TIME", '', $document);
			//$document = str_replace("#TUNGGAKAN", '', $document);
			//$document = str_replace("PAYMEN", '', $document);
			$document = str_replace("THN", '', $document);
			$document = str_replace("NOTYEAR", '', $document);

			$document = str_replace("#TYPE", strtolower($detail->NM_PENGAJUAN), $document);
			$document = str_replace("#TGL_TERBIT", date('d/m/Y'), $document);
			$document = str_replace("#PENELITI", $this->session->userdata('pbb_nama'), $document);
			$document = str_replace("#NIP", $this->session->userdata('nip'), $document);




			// header untuk membuka file output RTF dengan MS. Word
			header("Content-type: application/msword");
			header("Content-disposition: inline; filename=penelitian_kolektif.doc");
			header("Content-length: " . strlen($document));
			echo $document;
		} else {
			$wp = $this->Mpermohonan->getAnggotapermohonan_a($id);
			// membaca data dari form

			// memanggil dan membaca template dokumen yang telah kita buat
			$document = file_get_contents("perseorangan.rtf");
			// isi dokumen dinyatakan dalam bentuk string
			$document = str_replace("#JENIS_P", $detail->NM_PENGAJUAN, $document);
			$document = str_replace("#KATEGORI_P", $detail->KET_KOLEKTIF, $document);
			$document = str_replace("#NO_SURAT", $detail->KD_PENGAJUAN, $document);
			$document = str_replace("#NO_SURAT", $detail->KD_PENGAJUAN, $document);
			$document = str_replace("#TGL_SURAT", date('d/m/Y', strtotime($detail->TGL_SURAT_PERMOHONAN)), $document);
			$document = str_replace("#TGL_TERIMA", $detail->TGL_TERIMA, $document);
			$document = str_replace("#NO_LAYANAN", $detail->NO_LAYANAN, $document);
			$document = str_replace("#NAMA", $wp[0]['NM_WP'], $document);
			$document = str_replace("#ALMT", $wp[0]['JALAN_OP'], $document);
			$document = str_replace("#NPWP", $wp[0]['NPWP'], $document);
			$document = str_replace("NAMA_KUASA", $detail->NAMA_PEMOHON, $document);
			$document = str_replace("ALMT_KUASA", $detail->ALAMAT_PEMOHON, $document);
			$document = str_replace("#NOP", $wp[0]['NOP'], $document);
			$document = str_replace("HUTANG", '', $document);
			$document = str_replace("YEAR", date('Y'), $document);

			//isi tebel
			if ($dok->L_PERMOHONAN == '1') {
				$document = str_replace("#SKP", 'V', $document);
				$document = str_replace("NOTMAIL", '', $document);
			} else {
				$document = str_replace("#SKP", '', $document);
				$document = str_replace("NOTMAIL", 'V', $document);
			}

			$document = str_replace("WRITE", '', $document);
			$document = str_replace("LANGUANGE", '', $document);
			$document = str_replace("#BPD", '', $document);
			$document = str_replace("DONTBPD", '', $document);

			$document = str_replace("#SPOP", '', $document);
			$document = str_replace("NOTSPOP", '', $document);
			if ($dok->L_KTP_WP == '1') {
				$document = str_replace("#KTP", 'V', $document);
				$document = str_replace("DONTKTP", '', $document);
			} else {
				$document = str_replace("#KTP", '', $document);
				$document = str_replace("DONTKTP", 'V', $document);
			}
			if ($dok->L_SERTIFIKAT_TANAH == '1') {
				$document = str_replace("#KEPEMILIKAN", 'V', $document);
				$document = str_replace("DONTME", '', $document);
			} else {
				$document = str_replace("#KEPEMILIKAN", '', $document);
				$document = str_replace("DONTME", 'V', $document);
			}


			$document = str_replace("#SPPT_TETANGGA", '', $document);
			$document = str_replace("NOTME", '', $document);
			if ($dok->L_SURAT_KUASA == '1') {
				$document = str_replace("#KUASA", 'V', $document);
				$document = str_replace("AGE", '', $document);
			} else {
				$document = str_replace("#KUASA", '', $document);
				$document = str_replace("AGE", 'V', $document);
			}
			$document = str_replace("#WAKTU", '', $document);
			$document = str_replace("TIME", '', $document);
			//$document = str_replace("#TUNGGAKAN", '', $document);
			$document = str_replace("THN", '', $document);
			$document = str_replace("NOTMONTH", '', $document);

			$document = str_replace("#TYPE", strtolower($detail->NM_PENGAJUAN), $document);
			$document = str_replace("#TGL_TERBIT", date('d/m/Y'), $document);
			$document = str_replace("#PENELITI", $this->session->userdata('pbb_nama'), $document);
			$document = str_replace("#NIP", $this->session->userdata('nip'), $document);




			// header untuk membuka file output RTF dengan MS. Word
			header("Content-type: application/msword");
			header("Content-disposition: inline; filename=penelitian_perseorangan.doc");
			header("Content-length: " . strlen($document));
			echo $document;
		}


		//$this->load->view('cetak');
	}
	function proses_penomoran()
	{
		$data_m = $_POST['id'];
		$exp = explode('/', $data_m);
		$exp1 = explode('.', $exp[0]);
		$var = $this->db->query("SELECT MAX(TO_NUMBER(NOMOR_URUT_LAMPIRAN))+1 maks from P_NOMOR_LAMPIRAN 
								WHERE THN_PELAYANAN='$exp1[0]' and KODE_ABJAD_LAMPIRAN='$exp[1]'")->row();
		if ($var->MAKS == null) {
			$nomor = sprintf('%04s', '1');
		} else {
			$nomor = sprintf('%04s', $var->MAKS);
		}
		$this->db->query("INSERT INTO P_NOMOR_LAMPIRAN (THN_PELAYANAN, BUNDEL_PELAYANAN, NO_URUT_PELAYANAN, KODE_ABJAD_LAMPIRAN, NOMOR_URUT_LAMPIRAN) VALUES ('$exp1[0]', '$exp1[1]', '$exp1[2]', '$exp[1]', '$nomor')");
		echo $nomor;
		//sprintf('%04s',$rk->BUNDEL_PELAYANAN),
	}

	function validasiCheckAll()
	{
		$data = $this->input->post();
		/*echo "<pre>";
    	print_r($data);
    	echo "</pre>";
    	die();
*/



		if (!empty($data['berkas_id'])) {
			$res = explode(',', $data['berkas_id']);
			foreach ($res as $res) {
				// echo $res.'<br>';

				$THN_PELAYANAN       = substr($res, 0, 4); // $_POST['THN_PELAYANAN'];
				$BUNDEL_PELAYANAN    = substr($res, 4, 4);
				$NO_URUT_PELAYANAN   = substr($res, 8, 3);
				$KD_PROPINSI_RIWAYAT = substr($res, 11, 2);
				$KD_DATI2_RIWAYAT    = substr($res, 13, 2);
				$KD_KECAMATAN_RIWAYAT = substr($res, 15, 3);
				$KD_KELURAHAN_RIWAYAT = substr($res, 18, 3);
				$KD_BLOK_RIWAYAT      = substr($res, 21, 3);
				$NO_URUT_RIWAYAT      = substr($res, 24, 4);
				$KD_JNS_OP_RIWAYAT    = substr($res, 28, 1);
				$NIP                  = $data['NIP'];
				$TANGGAL_AWAL         = $data['TANGGAL_AWAL'];
				$KODE_GROUP           = $data['KODE_GROUP'];
				$KETERANGAN           = $data['KETERANGAN'];
				$TANGGAL_AKHIR        = $data['TANGGAL_AKHIR'];
				$STATUS_BERKAS		  = $data['STATUS_BERKAS'];


				/*echo $data['STATUS_BERKAS'];		
				die();*/

				$dataupdate = array(
					'KD_KANWIL'            => $data['KD_KANWIL'],
					'KD_KANTOR'            => $data['KD_KANTOR'],
					'THN_PELAYANAN'        => $THN_PELAYANAN,
					'BUNDEL_PELAYANAN'     => $BUNDEL_PELAYANAN,
					'NO_URUT_PELAYANAN'    => $NO_URUT_PELAYANAN,
					'KD_PROPINSI_RIWAYAT'  => $KD_PROPINSI_RIWAYAT,
					'KD_DATI2_RIWAYAT'     => $KD_DATI2_RIWAYAT,
					'KD_KECAMATAN_RIWAYAT' => $KD_KECAMATAN_RIWAYAT,
					'KD_KELURAHAN_RIWAYAT' => $KD_KELURAHAN_RIWAYAT,
					'KD_BLOK_RIWAYAT'      => $KD_BLOK_RIWAYAT,
					'NO_URUT_RIWAYAT'      => $NO_URUT_RIWAYAT,
					'KD_JNS_OP_RIWAYAT'    => $KD_JNS_OP_RIWAYAT,
					'KODE_GROUP'           => $this->session->userdata('pbb_kg'),
					'NIP'                  => $NIP,
					'KETERANGAN'           => $KETERANGAN,
					'TANGGAL_AKHIR'        => $TANGGAL_AKHIR,
					'STATUS_BERKAS'        => $STATUS_BERKAS
				);

				$datanext = array(
					'KD_KANWIL'            => $data['KD_KANWIL'],
					'KD_KANTOR'            => $data['KD_KANTOR'],
					"THN_PELAYANAN"        => $THN_PELAYANAN,
					"BUNDEL_PELAYANAN"     => $BUNDEL_PELAYANAN,
					"NO_URUT_PELAYANAN"    => $NO_URUT_PELAYANAN,
					"KD_PROPINSI_RIWAYAT"  => $KD_PROPINSI_RIWAYAT,
					"KD_DATI2_RIWAYAT"     => $KD_DATI2_RIWAYAT,
					"KD_KECAMATAN_RIWAYAT" => $KD_KECAMATAN_RIWAYAT,
					"KD_KELURAHAN_RIWAYAT" => $KD_KELURAHAN_RIWAYAT,
					"KD_BLOK_RIWAYAT"      => $KD_BLOK_RIWAYAT,
					"NO_URUT_RIWAYAT"      => $NO_URUT_RIWAYAT,
					"KD_JNS_OP_RIWAYAT"    => $KD_JNS_OP_RIWAYAT,
					"KODE_GROUP"           => $KODE_GROUP,
					"NIP"                  => null,
					"TANGGAL_AWAL"         => $TANGGAL_AWAL,
					"TANGGAL_AKHIR"        => null,
					"KETERANGAN"           => null,
					"UNIT_SEBEUMNYA"       => $this->session->userdata('pbb_kg'),
					'STATUS_BERKAS'        => $STATUS_BERKAS
				);

				$this->Mpermohonan->updateriwayat($dataupdate);


				if ($STATUS_BERKAS == 'Selesai') {
					$this->db->query("call blokir_buka_nop@to17($THN_PELAYANAN,'$KD_KECAMATAN_RIWAYAT','$KD_KELURAHAN_RIWAYAT','$KD_BLOK_RIWAYAT','$NO_URUT_RIWAYAT','$KD_JNS_OP_RIWAYAT')");
				}


				if ($STATUS_BERKAS != 'Diambil') {
					$this->Mpermohonan->insertriwayat($datanext);
				} else {
					$this->prosesselesai();
				}
			}
			redirect('permohonan/listdokumen');
		}
	}

	///
	function monitoring_dokumen()
	{
		$data['monitoring'] = $this->db->query("SELECT A.KODE_GROUP,sort,NAMA_GROUP,ICON, COUNT(*)JUMLAH FROM PV_DOKUMEN A
											   JOIN P_GROUP B ON A.KODE_GROUP=B.KODE_GROUP
											   WHERE TAHUN=TO_CHAR(SYSDATE,'YYYY') AND TGL_SELESAI IS NULL AND A.KODE_GROUP!=36
											   GROUP BY sort,A.KODE_GROUP,NAMA_GROUP,ICON ORDER BY sort asc")->result();
		$data['masuk'] = $this->db->query("SELECT CASE status_kolektif WHEN '1' THEN 'Kolektif' ELSE 'Individu' END
									            permohonan,count(1) jumlah
									    FROM pst_permohonan
									   WHERE tgl_terima_dokumen_wp =
									            TO_DATE (TO_CHAR (SYSDATE, 'dd/mm/yy'), 'dd/mm/yy')
									GROUP BY CASE status_kolektif WHEN '1' THEN 'Kolektif' ELSE 'Individu' END")->result();
		$data['masuk_bulan_ini'] = $this->db->query("SELECT CASE status_kolektif WHEN '1' THEN 'Kolektif' ELSE 'Individu' END permohonan,count(1) jumlah
													    FROM pst_permohonan
													   WHERE to_date( to_char( tgl_terima_dokumen_wp,'mm/yy'),'mm/yy') =
													            TO_DATE (TO_CHAR (SYSDATE, 'mm/yy'), 'mm/yy')
													GROUP BY CASE status_kolektif WHEN '1' THEN 'Kolektif' ELSE 'Individu' END")->result();
		$data['sampai'] = $this->db->query("SELECT CASE status_kolektif WHEN '1' THEN 'Kolektif' ELSE 'Individu' END permohonan,count(1) jumlah
    FROM pst_permohonan
   WHERE thn_pelayanan=to_char(sysdate,'yyyy') and tgl_terima_dokumen_wp <=
            TO_DATE (TO_CHAR (SYSDATE, 'dd/mm/yy'), 'dd/mm/yy')
GROUP BY CASE status_kolektif WHEN '1' THEN 'Kolektif' ELSE 'Individu' END")->result();
		$this->template->load('templatedua', 'permohonan/monitoring_dokumen', $data);
	}
	function detail_monitoring($id = "")
	{
		$data['kode_group'] = $id;
		$this->template->load('template', 'permohonan/detail_monitoring', $data);
	}
	function jsonmonitoring_dokumen($id = "")
	{
		header('Content-Type: application/json');
		echo $this->Mpermohonan->jsonmonitoring_dokumen($id);
	}


	function cetaksknjop($nopel, $pdf = null)
	{

		// cek di sk njop
		$qa = $this->db->query("select count(1) jum from p_sknjop
		where THN_PELAYANAN||'.'||BUNDEL_PELAYANAN||'.'||NO_URUT_PELAYANAN='$nopel'")->row();

		if ($qa->JUM == 0) {
			// insert nek sk njop
			$this->db->query("INSERT INTO LAYANAN.P_SKNJOP (
				KD_KANWIL, KD_KANTOR, THN_PELAYANAN, 
				BUNDEL_PELAYANAN, NO_URUT_PELAYANAN, KD_PROPINSI, 
				KD_DATI2, KD_KECAMATAN, KD_KELURAHAN, 
				KD_BLOK, NO_URUT, KD_JNS_OP, 
				KD_JNS_PELAYANAN, TANGGAL_PERMOHONAN, NAMA_PEMOHON, 
				ALAMAT_PEMOHON, CATATAN_PEMOHONAN, NIP_INSERT, 
				TGL_INSERT, NAMA_PEGAWAI, JABATAN, 
				NIP, NOMOR_SK, TANGGAL_SK) 
			 select '01','01',THN_PELAYANAN, 
				BUNDEL_PELAYANAN, NO_URUT_PELAYANAN, KD_PROPINSI_pemohon, 
				KD_DATI2_pemohon, KD_KECAMATAN_pemohon, KD_KELURAHAN_pemohon, 
				KD_BLOK_pemohon, NO_URUT_pemohon, KD_JNS_OP_pemohon, 
				17 KD_JNS_PELAYANAN, tgl_surat_permohonan, NAMA_PEMOHON, 
			 null   ALAMAT_PEMOHON,null CATATAN_PEMOHONAN, null NIP_INSERT, 
			   null TGL_INSERT, null NAMA_PEGAWAI, null JABATAN, 
			   null NIP,null NOMOR_SK, null TANGGAL_SK 
			 from VSK_NJOP
			 where THN_PELAYANAN||'.'||BUNDEL_PELAYANAN||'.'||NO_URUT_PELAYANAN='$nopel'");

			// setting nomer sk njop
			$sk = $this->db->query("SELECT A.*
                                FROM P_SKNJOP A                                 
                               WHERE THN_PELAYANAN||'.'||BUNDEL_PELAYANAN||'.'||NO_URUT_PELAYANAN='$nopel'")->result();

			foreach ($sk as $rsk) {
				$th  = date('Y');
				$bln = date('m');
				$nm = $this->db->query("SELECT * FROM P_MASTER_SK WHERE JENIS='NJOP'")->row();
				$nnn = $this->db->query("select INC_NJOP.nextval nomer from dual")->row();
				$rr = $this->db->query("select ip_pelayanan,unit_kantor from P_IP
											where ip_thn_pelayanan||'.'||ip_bundel_pelayanan||'.'||ip_no_urut_pelayanan='$nopel'")->row();

				if ($rr->UNIT_KANTOR == 'kota') {
					$nnn = $this->db->query("select INC_NJOP.nextval nomer from dual")->row();
					$nomer = 'A' . sprintf("%05d", $nnn->NOMER);
				} else {

					$nnn = $this->db->query("select INC_NJOP_B.nextval nomer from dual")->row();
					$nomer = 'B' . sprintf("%05d", $nnn->NOMER);
				}

				$pref = $nm->NOMOR . $th;
				$fixno = str_replace('#', $nomer, $pref);

				$this->db->query("UPDATE P_SKNJOP SET TANGGAL_SK=sysdate ,NAMA_PEGAWAI='" . $nm->NAMA_PEGAWAI . "' ,JABATAN='" . $nm->JABATAN . "', NIP='" . $nm->NIP . "', NOMOR_SK='$fixno' 
					WHERE THN_PELAYANAN='" . $rsk->THN_PELAYANAN . "'  and BUNDEL_PELAYANAN='" . $rsk->BUNDEL_PELAYANAN . "'  and NO_URUT_PELAYANAN='" . $rsk->NO_URUT_PELAYANAN . "'  and KD_PROPINSI='" . $rsk->KD_PROPINSI . "'  and KD_DATI2='" . $rsk->KD_DATI2 . "'  and KD_KECAMATAN='" . $rsk->KD_KECAMATAN . "'  and KD_KELURAHAN='" . $rsk->KD_KELURAHAN . "'  and KD_BLOK='" . $rsk->KD_BLOK . "'  and NO_URUT='" . $rsk->NO_URUT . "'  and KD_JNS_OP='" . $rsk->KD_JNS_OP . "'");
			}
		}





		$sk = $this->db->query("select a.*,KD_PROPINSI || '.'|| KD_DATI2 || '.'|| KD_KECAMATAN|| '.'|| KD_KELURAHAN || '.'|| KD_BLOK || '.'|| NO_URUT || '.'|| KD_JNS_OP NOP,nomor_sk,tanggal_sk,nama_pegawai, nip,jabatan
				from vsk_njop a    
				join p_sknjop b on a.thn_pelayanan=b.thn_pelayanan and a.bundel_pelayanan=b.bundel_pelayanan and a.no_urut_pelayanan=b.no_urut_pelayanan
				and a.kd_propinsi_pemohon=b.kd_propinsi and a.kd_dati2_pemohon=b.kd_dati2 and A.KD_KECAMATAN_PEMOHON=b.kd_kecamatan and a.kd_kelurahan_pemohon=b.kd_kelurahan and a.kd_blok_pemohon=b.kd_blok
				and a.no_urut_pemohon=b.no_urut and a.kd_jns_op_pemohon=b.kd_jns_op
				WHERE a.THN_PELAYANAN||'.'||a.BUNDEL_PELAYANAN||'.'||a.NO_URUT_PELAYANAN='$nopel'")->result();
		$data['rk'] = $sk;

		echo $this->load->view('permohonan/cetaksknjop', $data, true);
		/* 	 }
		} else {
			redirect('permohonan/create');
		} */
	}


	function cetaksk()
	{
		// cek sk
		// $var=urldecode(urldecode(urldecode($jenis)));

		$var = urldecode($this->input->get('var'));
		$id  = $this->input->get('id');

		$rr = $this->db->query("SELECT * FROM PST_DETAIL
								where THN_PELAYANAN||BUNDEL_PELAYANAN||NO_URUT_PELAYANAN||KD_PROPINSI_PEMOHON||KD_DATI2_PEMOHON||KD_KECAMATAN_PEMOHON||KD_KELURAHAN_PEMOHON||KD_BLOK_PEMOHON||NO_URUT_PEMOHON||KD_JNS_OP_PEMOHON='$id'")->row();

		if ($rr) {
			$cek = $this->db->query("SELECT * FROM P_PST_DETAIL_SK 
										where THN_PELAYANAN||BUNDEL_PELAYANAN||NO_URUT_PELAYANAN||KD_PROPINSI_PEMOHON||KD_DATI2_PEMOHON||KD_KECAMATAN_PEMOHON||KD_KELURAHAN_PEMOHON||KD_BLOK_PEMOHON||NO_URUT_PEMOHON||KD_JNS_OP_PEMOHON='$id' and jenis_sk='$var'")->row();
			if (empty($cek)) {
				$this->db->trans_start();
				// insert 
				$this->db->query("INSERT INTO P_PST_DETAIL_SK (
								   THN_PELAYANAN, BUNDEL_PELAYANAN, NO_URUT_PELAYANAN, 
								   KD_PROPINSI_PEMOHON, KD_DATI2_PEMOHON, KD_KECAMATAN_PEMOHON, 
								   KD_KELURAHAN_PEMOHON, KD_BLOK_PEMOHON, NO_URUT_PEMOHON, 
								   KD_JNS_OP_PEMOHON, KD_JNS_PELAYANAN, THN_PAJAK_PERMOHONAN,JENIS_SK) 
								  select THN_PELAYANAN, BUNDEL_PELAYANAN, NO_URUT_PELAYANAN, 
								   KD_PROPINSI_PEMOHON, KD_DATI2_PEMOHON, KD_KECAMATAN_PEMOHON, 
								   KD_KELURAHAN_PEMOHON, KD_BLOK_PEMOHON, NO_URUT_PEMOHON, 
								   KD_JNS_OP_PEMOHON, KD_JNS_PELAYANAN, THN_PAJAK_PERMOHONAN,'$var' from pst_detail 
								   where THN_PELAYANAN||BUNDEL_PELAYANAN||NO_URUT_PELAYANAN||KD_PROPINSI_pemohon||KD_DATI2_pemohon||KD_KECAMATAN_pemohon||KD_KELURAHAN_pemohon||KD_BLOK_pemohon||NO_URUT_pemohon||KD_JNS_OP_pemohon='$id'");


				// increment surat
				$tahun = date('Y');
				$ic = $this->db->query("SELECT MAX(TO_NUMBER(REGEXP_SUBSTR(NOMOR_SK, '[^/]+', 1, 2))) ID_INC
		                        FROM P_PST_DETAIL_SK
		                        WHERE TO_CHAR(TANGGAL_SK,'yyyy')='$tahun' AND JENIS_SK='$var'")->row();
				// nomer sk
				$sk = $this->db->query("select * from p_master_sk where jenis='$var'")->row();

				if ($sk) {

					$urut = ($ic->ID_INC) + 1;
					$nomorsk = str_replace('#', $urut, $sk->NOMOR) . $tahun;
					$this->db->query("UPDATE P_PST_DETAIL_SK
			    					SET NOMOR_SK='" . $nomorsk . "',TANGGAL_SK=SYSDATE,NAMA_PEGAWAI='" . $sk->NAMA_PEGAWAI . "',NIP='" . $sk->NIP . "', JABATAN='" . $sk->JABATAN . "'
			    					WHERE JENIS_SK='" . $var . "' AND THN_PELAYANAN||BUNDEL_PELAYANAN||NO_URUT_PELAYANAN||KD_PROPINSI_PEMOHON||KD_DATI2_PEMOHON||KD_KECAMATAN_PEMOHON||KD_KELURAHAN_PEMOHON||KD_BLOK_PEMOHON||NO_URUT_PEMOHON||KD_JNS_OP_PEMOHON='$id'");
				} else {
					echo '- belum ada template penomoran SK<br>';
				}

				$this->db->trans_complete();
			}

			$result['rk'] = $this->db->query("SELECT A.*,B.*,C.* ,get_kecamatan(KD_KECAMATAN_PEMOHON) kecop, get_kelurahan(kd_kelurahan_pemohon,kd_kecamatan_pemohon) desaop ,TO_CHAR(TANGGAL_SK,'yyyy-mm-dd') TANGGALSK ,TO_CHAR(TGL_SURAT_PERMOHONAN,'yyyy-mm-dd') TANGGALPERMOHONAN,PBB_YG_HARUS_DIBAYAR_SPPT FROM P_PST_DETAIL_SK A JOIN DAT_OBJEK_PAJAK B ON A.KD_PROPINSI_PEMOHON=B.KD_PROPINSI AND A.KD_DATI2_PEMOHON=B.KD_DATI2 AND A.KD_KECAMATAN_PEMOHON=B.KD_KECAMATAN AND A.KD_KELURAHAN_PEMOHON=B.KD_KELURAHAN AND A.KD_BLOK_PEMOHON=B.KD_BLOK AND A.NO_URUT_PEMOHON=B.NO_URUT AND A.KD_JNS_OP_PEMOHON=B.KD_JNS_OP left  JOIN DAT_SUBJEK_PAJAK C ON C.SUBJEK_PAJAK_ID=B.SUBJEK_PAJAK_ID left JOIN PST_PERMOHONAN D ON A.THN_PELAYANAN = D.THN_PELAYANAN AND A.BUNDEL_PELAYANAN = D.BUNDEL_PELAYANAN AND A.NO_URUT_PELAYANAN = D.NO_URUT_PELAYANAN  left JOIN SPPT E ON A.KD_PROPINSI_PEMOHON=E.KD_PROPINSI  AND A.KD_DATI2_PEMOHON=E.KD_DATI2  AND A.KD_KECAMATAN_PEMOHON=E.KD_KECAMATAN  AND A.KD_KELURAHAN_PEMOHON=E.KD_KELURAHAN  AND A.KD_BLOK_PEMOHON=E.KD_BLOK  AND A.NO_URUT_PEMOHON=E.NO_URUT  AND A.KD_JNS_OP_PEMOHON=E.KD_JNS_OP AND E.THN_PAJAK_SPPT=A.THN_PAJAK_PERMOHONAN  WHERE A.THN_PELAYANAN||A.BUNDEL_PELAYANAN||A.NO_URUT_PELAYANAN||KD_PROPINSI_PEMOHON||KD_DATI2_PEMOHON||KD_KECAMATAN_PEMOHON||KD_KELURAHAN_PEMOHON||KD_BLOK_PEMOHON||NO_URUT_PEMOHON||KD_JNS_OP_PEMOHON='$id'")->row();

			switch ($var) {
				case 'PEMBATALAN SPPT/SKP':
					# code...
					$this->load->view('permohonan/sk/pembatalan', $result);

					break;

				case 'PENDAFTARAN DATA BARU':
					$this->load->view('permohonan/sk/databaru', $result);
					break;
				case 'PENGURANGAN ATAS BESARNYA PAJAK TERHUTAN':
					$this->load->view('permohonan/sk/pengurangan', $result);
					break;

				case 'PENGURANGAN ATAS BESARNYA PAJAK TERHUTANG':
					$this->load->view('permohonan/sk/pengurangan', $result);
					break;

				case 'PENGURANGAN DENDA ADMINISTRASI':
					$nn = substr($id, -18);

					$result['tagihan'] = $this->db->query("select thn_pajak_sppt,PBB_YG_HARUS_DIBAYAR_SPPT from sppt 
						where KD_PROPINSI||KD_DATI2||KD_KECAMATAN||KD_KELURAHAN||KD_BLOK||NO_URUT||KD_JNS_OP ='$nn' and  status_pembayaran_sppt=0")->result();
					$this->load->view('permohonan/sk/pengurangandenda', $result);
					break;

				case 'PEMBETULAN SPPT/SKP/STP':
					# code...
					$this->load->view('permohonan/sk/pembetulan', $result);
					break;

				case 'KEBERATAN PENUNJUKAN WAJIB PAJAK':
					$this->load->view('permohonan/sk/keberatan', $result);
					break;
				default:
					# code...
					echo '- Jenis SK: ' . $var . ', belum ada template cetak.';
					break;
			}
		} else {
			redirect($_SERVER['HTTP_REFERER']);
		}
	}


	function datask()
	{

		$this->template->load('template', 'permohonan/datask');
	}

	function decodebarcode()
	{
		$barcode = $this->input->post('barcode');
		// $cc=$this->db->query("select substr( dec_sppt('$barcode'),1,18) nop from dual")->row();
		$cc = $this->db->query("select replace(substr( dec_sppt('$barcode'),1,18),' ','') nop from dual")->row();


		if ($cc->NOP != 0) {
			$res = $cc->NOP;
			$row['KD_PROPINSI_PEMOHON'] = substr($res, 0, 2);
			$row['KD_DATI2_PEMOHON'] = substr($res, 2, 2);
			$row['KD_KECAMATAN_PEMOHON'] = substr($res, 4, 3);
			$row['KD_KELURAHAN_PEMOHON'] = substr($res, 7, 3);
			$row['KD_BLOK_PEMOHON'] = substr($res, 10, 3);
			$row['NO_URUT_PEMOHON'] = substr($res, 13, 4);
			$row['KD_JNS_OP_PEMOHON'] = substr($res, 17, 1);
		} else {
			$row['KD_PROPINSI_PEMOHON'] = null;
			$row['KD_DATI2_PEMOHON'] = null;
			$row['KD_KECAMATAN_PEMOHON'] = null;
			$row['KD_KELURAHAN_PEMOHON'] = null;
			$row['KD_BLOK_PEMOHON'] = null;
			$row['NO_URUT_PEMOHON'] = null;
			$row['KD_JNS_OP_PEMOHON'] = null;
		}
		$row_set[]       = $row;

		echo $return = json_encode($row_set);
	}

	function hapusDetail()
	{
		/*echo "<pre>";
		print_r($this->input->get());
		echo "</pre>";*/
		$nopel = $this->input->get('nopel');
		$nop   = $this->input->get('nop');

		if ($nopel <> '' && $nop <> '') {
			$xx = explode('.', $nopel);
			$cc = explode('.', $nop);

			// print_r($xx);
			$thn          = $xx[0];
			$bundel       = $xx[1];
			$urut         = $xx[2];

			$kd_propinsi  = $cc[0];
			$kd_dati2     = $cc[1];
			$kd_kecamatan = $cc[2];
			$kd_kelurahan = $cc[3];
			$kd_blok      = $cc[4];
			$no_urut      = $cc[5];

			$this->db->where('THN_PELAYANAN', $xx[0]);
			$this->db->where('BUNDEL_PELAYANAN', $xx[1]);
			$this->db->where('NO_URUT_PELAYANAN', $xx[2]);
			$this->db->where('KD_PROPINSI_PEMOHON', $cc[0]);
			$this->db->where('KD_DATI2_PEMOHON', $cc[1]);
			$this->db->where('KD_KECAMATAN_PEMOHON', $cc[2]);
			$this->db->where('KD_KELURAHAN_PEMOHON', $cc[3]);
			$this->db->where('KD_BLOK_PEMOHON', $cc[4]);
			$this->db->where('NO_URUT_PEMOHON', $cc[5]);
			$this->db->where('KD_JNS_OP_PEMOHON', $cc[6]);
			$this->db->delete('PST_DETAIL');
			$this->db->query("call blokir_buka_nop@to17('" . $xx[0] . "','" . $cc[2] . "','" . $cc[3] . "','" . $cc[4] . "','" . $cc[5] . "','" . $cc[6] . "')");
		}

		header('Location: ' . $_SERVER['HTTP_REFERER']);
	}


	function selesai()
	{
		$th = date('Y');
		$selesai = $this->db->query("select * from permohonan_selesai 
          where thn_pelayanan=$th and  diambil=0")->result();
		$diambil = $this->db->query("select * from permohonan_selesai 
          where thn_pelayanan=$th and  diambil=1")->result();
		$data = array(
			'selesai' => $selesai,
			'diambil' => $diambil
		);

		$this->template->load('template', 'permohonan/pelayananselesai', $data);
	}

	function monitorBaru()
	{
		if (isset($_POST['filter'])) {
			$filter               = $_POST['filter'];
			switch ($filter) {
				case 'No Layanan':
					# code...

					$nl = $_POST['no_layanan'];
					$wh = "where no_layanan='$nl'";
					$data['no_layanan'] = $nl;
					break;
				case 'NOP':
					# code...
					$KD_PROPINSI_PEMOHON  = $_POST['KD_PROPINSI_PEMOHON'];
					$KD_DATI2_PEMOHON     = $_POST['KD_DATI2_PEMOHON'];
					$KD_KECAMATAN_PEMOHON = $_POST['KD_KECAMATAN_PEMOHON'];
					$KD_KELURAHAN_PEMOHON = $_POST['KD_KELURAHAN_PEMOHON'];
					$KD_BLOK_PEMOHON      = $_POST['KD_BLOK_PEMOHON'];
					$NO_URUT_PEMOHON      = $_POST['NO_URUT_PEMOHON'];
					$KD_JNS_OP_PEMOHON    = $_POST['KD_JNS_OP_PEMOHON'];
					$nop                          = $KD_PROPINSI_PEMOHON . '.' . $KD_DATI2_PEMOHON . '.' . $KD_KECAMATAN_PEMOHON . '.' . $KD_KELURAHAN_PEMOHON . '.' . $KD_BLOK_PEMOHON . '.' . $NO_URUT_PEMOHON . '.' . $KD_JNS_OP_PEMOHON;
					$wh                           = "where nop='$nop'";
					$data['KD_PROPINSI_PEMOHON']  = $KD_PROPINSI_PEMOHON;
					$data['KD_DATI2_PEMOHON']     = $KD_DATI2_PEMOHON;
					$data['KD_KECAMATAN_PEMOHON'] = $KD_KECAMATAN_PEMOHON;
					$data['KD_KELURAHAN_PEMOHON'] = $KD_KELURAHAN_PEMOHON;
					$data['KD_BLOK_PEMOHON']      = $KD_BLOK_PEMOHON;
					$data['NO_URUT_PEMOHON']      = $NO_URUT_PEMOHON;
					$data['KD_JNS_OP_PEMOHON']    = $KD_JNS_OP_PEMOHON;

					break;
			}
			$data['filter'] = $filter;
			$data['res']    = $this->db->query("select nop,get_status_selesai(tahun,replace(nop,'.','') ) status,get_tanggal_perekaman(replace(nop,'.','')) tgl_rekam,get_nip_perekaman(replace(nop,'.','')) nip_perekam from PV_MONITOR $wh")->result();
		} else {
			$data['filter'] = 'No Layanan';
			$data['no_layanan'] = null;
		}


		$this->template->load('template', 'permohonan/formmonitoringdatabaru', $data);
	}


	function cekpendataan()
	{
		$no_layanan = urldecode(trim($this->input->get('no_layanan', true)));
		$filter     = $this->input->get('filter');
		$var        = str_replace('.', '', $no_layanan);
		$KD_PROPINSI_PEMOHON  = $this->input->get('KD_PROPINSI_PEMOHON', true);
		$KD_DATI2_PEMOHON     = $this->input->get('KD_DATI2_PEMOHON', true);
		$KD_KECAMATAN_PEMOHON = $this->input->get('KD_KECAMATAN_PEMOHON', true);
		$KD_KELURAHAN_PEMOHON = $this->input->get('KD_KELURAHAN_PEMOHON', true);
		$KD_BLOK_PEMOHON      = $this->input->get('KD_BLOK_PEMOHON', true);
		$NO_URUT_PEMOHON      = $this->input->get('NO_URUT_PEMOHON', true);
		$KD_JNS_OP_PEMOHON    = $this->input->get('KD_JNS_OP_PEMOHON', true);

		if ($filter == 'No Layanan') {
			$where = "where thn_pelayanan=substr('$var',1,4) and bundel_pelayanan=substr('$var',5,4) and no_urut_pelayanan=substr('$var',9,3)";
		} else {
			$where = "where KD_PROPINSI_PEMOHON='$KD_PROPINSI_PEMOHON' and KD_DATI2_PEMOHON='$KD_DATI2_PEMOHON' and KD_KECAMATAN_PEMOHON='$KD_KECAMATAN_PEMOHON' and KD_KELURAHAN_PEMOHON='$KD_KELURAHAN_PEMOHON' and KD_BLOK_PEMOHON='$KD_BLOK_PEMOHON' and NO_URUT_PEMOHON='$NO_URUT_PEMOHON' and KD_JNS_OP_PEMOHON='$KD_JNS_OP_PEMOHON'";
		}


		$sql = $this->db->query("select thn_pelayanan,bundel_pelayanan,no_urut_pelayanan,kd_propinsi_pemohon,kd_dati2_pemohon,kd_kecamatan_pemohon,kd_kelurahan_pemohon,kd_blok_pemohon,no_urut_pemohon, kd_jns_op_pemohon,
								get_tgl_permohonan(thn_pelayanan,bundel_pelayanan,no_urut_pelayanan) permohonan,
								case when  get_tgl_pendataan(kd_propinsi_pemohon|| kd_dati2_pemohon|| kd_kecamatan_pemohon|| kd_kelurahan_pemohon|| kd_blok_pemohon|| no_urut_pemohon|| kd_jns_op_pemohon) <= get_tgl_permohonan(thn_pelayanan,bundel_pelayanan,no_urut_pelayanan) then null else get_tgl_pendataan(kd_propinsi_pemohon|| kd_dati2_pemohon|| kd_kecamatan_pemohon|| kd_kelurahan_pemohon|| kd_blok_pemohon|| no_urut_pemohon|| kd_jns_op_pemohon)  end  pendataan,
								case when get_tgl_penetapan(kd_propinsi_pemohon,kd_dati2_pemohon,kd_kecamatan_pemohon,kd_kelurahan_pemohon,kd_blok_pemohon,no_urut_pemohon, kd_jns_op_pemohon,thn_pelayanan) <=    get_tgl_permohonan(thn_pelayanan,bundel_pelayanan,no_urut_pelayanan) then null else get_tgl_penetapan(kd_propinsi_pemohon,kd_dati2_pemohon,kd_kecamatan_pemohon,kd_kelurahan_pemohon,kd_blok_pemohon,no_urut_pemohon, kd_jns_op_pemohon,thn_pelayanan) end   penetapan
								from pst_detail
								" . $where)->result();

		$data = array(
			'no_layanan'           => $no_layanan,
			'data'                 => $sql,
			'filter'               => $filter,
			'KD_PROPINSI_PEMOHON'  => $KD_PROPINSI_PEMOHON,
			'KD_DATI2_PEMOHON'     => $KD_DATI2_PEMOHON,
			'KD_KECAMATAN_PEMOHON' => $KD_KECAMATAN_PEMOHON,
			'KD_KELURAHAN_PEMOHON' => $KD_KELURAHAN_PEMOHON,
			'KD_BLOK_PEMOHON'      => $KD_BLOK_PEMOHON,
			'NO_URUT_PEMOHON'      => $NO_URUT_PEMOHON,
			'KD_JNS_OP_PEMOHON'    => $KD_JNS_OP_PEMOHON,
		);
		$this->template->load('template', 'permohonan/cek_pendataan', $data);
	}
}

/* End of file Permohonan.php */
/* Location: ./application/controllers/Permohonan.php */
/* Generated by Mohamad Wahyu Dewantoro 2017-04-29 08:12:44 */
