<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Spop extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('Mspop');
		$this->load->model('Mpermohonan');
		if ($this->session->userdata('pbb') <> 1) {
			$this->session->set_flashdata('notif', '<div class="badge">
                    Silahkan login dengan username dan password anda.</p>
                    </div>');
			redirect('auth');
		}
	}


	function form_spop($var = "")
	{
		$jns = isset($_GET['JENIS']) ? $_GET['JENIS'] : null;

		/*echo $jns;
			die();*/
		if ($jns == 'SPOP') {
			$JNS_PELAYANAN     = $this->input->get('KD_JNS_PELAYANAN');
			$THN_PELAYANAN     = $this->input->get('THN_PELAYANAN');
			$BUNDEL_PELAYANAN  = $this->input->get('BUNDEL_PELAYANAN');
			$NO_URUT_DALAM     = $this->input->get('NO_URUT_DALAM');
			$NO_URUT  		   = $this->input->get('NO_URUT');

			$KD_PROPINSI  = $this->input->get('KD_PROPINSI');
			$KD_DATI2     = $this->input->get('KD_DATI2');
			$KD_KECAMATAN = $this->input->get('KD_KECAMATAN');
			$KD_KELURAHAN = $this->input->get('KD_KELURAHAN');
			$KD_BLOK      = $this->input->get('KD_BLOK');
			$NO_URUT_NOP  = $this->input->get('NO_URUT_NOP');
			$KD_JNS_OP    = $this->input->get('KD_JNS_OP');
			$NOP 		  = $KD_PROPINSI . $KD_DATI2 . $KD_KECAMATAN . $KD_KELURAHAN . $KD_BLOK . $NO_URUT_NOP . $KD_JNS_OP;

			$KD_PROPINSI_ASAL  = $this->input->get('KD_PROPINSI_ASAL');
			$KD_DATI2_ASAL     = $this->input->get('KD_DATI2_ASAL');
			$KD_KECAMATAN_ASAL = $this->input->get('KD_KECAMATAN_ASAL');
			$KD_KELURAHAN_ASAL = $this->input->get('KD_KELURAHAN_ASAL');
			$KD_BLOK_ASAL      = $this->input->get('KD_BLOK_ASAL');
			$NO_URUT_ASAL      = $this->input->get('NO_URUT_ASAL');
			$KD_JNS_OP_ASAL    = $this->input->get('KD_JNS_OP_ASAL');
			$NOP_ASAL		   = $KD_PROPINSI_ASAL . $KD_DATI2_ASAL . $KD_KECAMATAN_ASAL . $KD_KELURAHAN_ASAL . $KD_BLOK_ASAL . $NO_URUT_ASAL . $KD_JNS_OP_ASAL;


			// data spop

			$_SESSION['JNS_PELAYANAN_SPOP']     = $JNS_PELAYANAN;
			$_SESSION['THN_PELAYANAN_SPOP']     = $THN_PELAYANAN;
			$_SESSION['BUNDEL_PELAYANAN_SPOP']  = $BUNDEL_PELAYANAN;
			$_SESSION['NO_URUT_DALAM_SPOP']     = $NO_URUT_DALAM;
			$_SESSION['NO_URUT_SPOP']           = $NO_URUT;
			$_SESSION['KD_PROPINSI_SPOP']       = $KD_PROPINSI;
			$_SESSION['KD_DATI2_SPOP']          = $KD_DATI2;
			$_SESSION['KD_KECAMATAN_SPOP']      = $KD_KECAMATAN;
			$_SESSION['KD_KELURAHAN_SPOP']      = $KD_KELURAHAN;
			$_SESSION['KD_BLOK_SPOP']           = $KD_BLOK;
			$_SESSION['NO_URUT_NOP_SPOP']       = $NO_URUT_NOP;
			$_SESSION['KD_JNS_OP_SPOP']         = $KD_JNS_OP;

			$_SESSION['KD_PROPINSI_ASAL_SPOP']  = $KD_PROPINSI_ASAL;
			$_SESSION['KD_DATI2_ASAL_SPOP']     = $KD_DATI2_ASAL;
			$_SESSION['KD_KECAMATAN_ASAL_SPOP'] = $KD_KECAMATAN_ASAL;
			$_SESSION['KD_KELURAHAN_ASAL_SPOP'] = $KD_KELURAHAN_ASAL;
			$_SESSION['KD_BLOK_ASAL_SPOP']      = $KD_BLOK_ASAL;
			$_SESSION['NO_URUT_ASAL_SPOP']      = $NO_URUT_ASAL;
			$_SESSION['KD_JNS_OP_ASAL_SPOP']    = $KD_JNS_OP_ASAL;
			$_SESSION['t_spop']                 = "active";
			$_SESSION['t_lspop']                = null;
			$_SESSION['t_formulir']             = NULL;

			// datalspop
			$_SESSION['KD_JNS_PELAYANAN_LSPOP'] = null;
			$_SESSION['THN_PELAYANAN']          = null;
			$_SESSION['BUNDEL_PELAYANAN']       = null;
			$_SESSION['NO_URUT_DALAM']          = null;
			$_SESSION['NO_URUT']                = null;
			$_SESSION['KD_PROPINSI']            = null;
			$_SESSION['KD_DATI2']               = null;
			$_SESSION['KD_KECAMATAN']           = null;
			$_SESSION['KD_KELURAHAN']           = null;
			$_SESSION['KD_BLOK']                = null;
			$_SESSION['NO_URUT_NOP']            = null;
			$_SESSION['KD_JNS_OP']              = null;

			$_SESSION['KD_PROPINSI_ASAL']       = null;
			$_SESSION['KD_DATI2_ASAL']          = null;
			$_SESSION['KD_KECAMATAN_ASAL']      = null;
			$_SESSION['KD_KELURAHAN_ASAL']      = null;
			$_SESSION['KD_BLOK_ASAL']           = null;
			$_SESSION['NO_URUT_ASAL']           = null;
			$_SESSION['KD_JNS_OP_ASAL']         = null;
			$_SESSION['NO_BNG']                 = null;


			$data['NO_FORMULIR_SPOP']	 = $THN_PELAYANAN . $BUNDEL_PELAYANAN . $NO_URUT_DALAM . $NO_URUT;
			$data['nop_asal']			 = $NOP_ASAL;
			$data['isi']				 = $this->db->query("select b.*,a.* from dat_subjek_pajak a
												join dat_objek_pajak b on a.subjek_pajak_id=b.subjek_pajak_id
												where a.subjek_pajak_id='$NOP'")->row();
			$data['bumi']				 = $this->db->query("select * from dat_op_bumi
												where kd_propinsi='$KD_PROPINSI'
												and kd_dati2='$KD_DATI2'
												and kd_kecamatan='$KD_KECAMATAN'
												and kd_kelurahan='$KD_KELURAHAN' 
												and kd_blok='$KD_BLOK'
												and no_urut='$NO_URUT_NOP'
												and kd_jns_op='$KD_JNS_OP'")->row();
			$data['znt']				 = $this->db->query("select * from dat_peta_znt
												where kd_propinsi='$KD_PROPINSI'
												and kd_dati2='$KD_DATI2'
												and kd_kecamatan='$KD_KECAMATAN'
												and kd_kelurahan='$KD_KELURAHAN' 
												and kd_blok='$KD_BLOK'")->result();
			$NIP = $this->session->userdata('nip');
			$data['nomor'] = $this->db->query("select * from p_nomor where NIP_PEMBUAT='$NIP'")->result();
			$this->template->load('template', 'spop/form_spop', $data);
		}
		if ($jns == 'LSPOP') {
			$JNS_PELAYANAN     = $_GET['KD_JNS_PELAYANAN'];
			//$JNS_FORMULIR      =$_GET['KD_JNS_FORMULIR'];
			$THN_PELAYANAN     = $_GET['THN_PELAYANAN'];
			$BUNDEL_PELAYANAN  = $_GET['BUNDEL_PELAYANAN'];
			$NO_URUT_DALAM     = $_GET['NO_URUT_DALAM'];
			$NO_URUT  		   = $_GET['NO_URUT'];

			$KD_PROPINSI  = $_GET['KD_PROPINSI'];
			$KD_DATI2     = $_GET['KD_DATI2'];
			$KD_KECAMATAN = $_GET['KD_KECAMATAN'];
			$KD_KELURAHAN = $_GET['KD_KELURAHAN'];
			$KD_BLOK      = $_GET['KD_BLOK'];
			$NO_URUT_NOP  = $_GET['NO_URUT_NOP'];
			$KD_JNS_OP    = $_GET['KD_JNS_OP'];
			$NOP 		  = $KD_PROPINSI . $KD_DATI2 . $KD_KECAMATAN . $KD_KELURAHAN . $KD_BLOK . $NO_URUT_NOP . $KD_JNS_OP;

			$KD_PROPINSI_ASAL  = $_GET['KD_PROPINSI_ASAL'];
			$KD_DATI2_ASAL     = $_GET['KD_DATI2_ASAL'];
			$KD_KECAMATAN_ASAL = $_GET['KD_KECAMATAN_ASAL'];
			$KD_KELURAHAN_ASAL = $_GET['KD_KELURAHAN_ASAL'];
			$KD_BLOK_ASAL      = $_GET['KD_BLOK_ASAL'];
			$NO_URUT_ASAL  	   = $_GET['NO_URUT_ASAL'];
			$KD_JNS_OP_ASAL    = $_GET['KD_JNS_OP_ASAL'];
			$NOP_ASAL		   = $KD_PROPINSI_ASAL . $KD_DATI2_ASAL . $KD_KECAMATAN_ASAL . $KD_KELURAHAN_ASAL . $KD_BLOK_ASAL . $NO_URUT_ASAL . $KD_JNS_OP_ASAL;
			$NO_BNG    		   = $_GET['NO_BNG'];


			// data lspop
			$_SESSION['KD_JNS_PELAYANAN_LSPOP'] = $JNS_PELAYANAN;
			//$_SESSION['JNS_FORMULIR']    =$JNS_FORMULIR;
			$_SESSION['THN_PELAYANAN']     = $THN_PELAYANAN;
			$_SESSION['BUNDEL_PELAYANAN']  = $BUNDEL_PELAYANAN;
			$_SESSION['NO_URUT_DALAM']     = $NO_URUT_DALAM;
			$_SESSION['NO_URUT']           = $NO_URUT;
			$_SESSION['KD_PROPINSI']       = $KD_PROPINSI;
			$_SESSION['KD_DATI2']          = $KD_DATI2;
			$_SESSION['KD_KECAMATAN']      = $KD_KECAMATAN;
			$_SESSION['KD_KELURAHAN']      = $KD_KELURAHAN;
			$_SESSION['KD_BLOK']           = $KD_BLOK;
			$_SESSION['NO_URUT_NOP']       = $NO_URUT_NOP;
			$_SESSION['KD_JNS_OP']         = $KD_JNS_OP;

			$_SESSION['KD_PROPINSI_ASAL']  = $KD_PROPINSI_ASAL;
			$_SESSION['KD_DATI2_ASAL']     = $KD_DATI2_ASAL;
			$_SESSION['KD_KECAMATAN_ASAL'] = $KD_KECAMATAN_ASAL;
			$_SESSION['KD_KELURAHAN_ASAL'] = $KD_KELURAHAN_ASAL;
			$_SESSION['KD_BLOK_ASAL']      = $KD_BLOK_ASAL;
			$_SESSION['NO_URUT_ASAL']      = $NO_URUT_ASAL;
			$_SESSION['KD_JNS_OP_ASAL']    = $KD_JNS_OP_ASAL;
			$_SESSION['NO_BNG']            = $NO_BNG;
			$_SESSION['t_formulir'] = null;
			$_SESSION['t_spop']     = NULL;
			$_SESSION['t_lspop']    = "active";


			// data spop
			$_SESSION['JNS_PELAYANAN_SPOP']     = null;
			$_SESSION['THN_PELAYANAN_SPOP']     = null;
			$_SESSION['BUNDEL_PELAYANAN_SPOP']  = null;
			$_SESSION['NO_URUT_DALAM_SPOP']     = null;
			$_SESSION['NO_URUT_SPOP']           = null;
			$_SESSION['KD_PROPINSI_SPOP']       = null;
			$_SESSION['KD_DATI2_SPOP']          = null;
			$_SESSION['KD_KECAMATAN_SPOP']      = null;
			$_SESSION['KD_KELURAHAN_SPOP']      = null;
			$_SESSION['KD_BLOK_SPOP']           = null;
			$_SESSION['NO_URUT_NOP_SPOP']       = null;
			$_SESSION['KD_JNS_OP_SPOP']         = null;

			$_SESSION['KD_PROPINSI_ASAL_SPOP']  = null;
			$_SESSION['KD_DATI2_ASAL_SPOP']     = null;
			$_SESSION['KD_KECAMATAN_ASAL_SPOP'] = null;
			$_SESSION['KD_KELURAHAN_ASAL_SPOP'] = null;
			$_SESSION['KD_BLOK_ASAL_SPOP']      = null;
			$_SESSION['NO_URUT_ASAL_SPOP']      = null;
			$_SESSION['KD_JNS_OP_ASAL_SPOP']    = null;


			$data['NO_FORMULIR_SPOP']	 = $THN_PELAYANAN . $BUNDEL_PELAYANAN . $NO_URUT_DALAM . $NO_URUT;
			$data['nop_asal']			 = $NOP_ASAL;
			$data['isi'] = $this->db->query("select * from dat_objek_pajak where subjek_pajak_id='$NOP'")->row();
			$this->template->load('template', 'spop/form_spop', $data);
		} else {
			$_SESSION['JNS_PELAYANAN_SPOP']     = NULL;
			$_SESSION['JNS_FORMULIR_SPOP']      = NULL;
			$_SESSION['THN_PELAYANAN_SPOP']     = NULL;
			$_SESSION['BUNDEL_PELAYANAN_SPOP']  = NULL;
			$_SESSION['NO_URUT_DALAM_SPOP']     = NULL;
			$_SESSION['NO_URUT_SPOP']           = NULL;
			$_SESSION['KD_PROPINSI_SPOP']       = NULL;
			$_SESSION['KD_DATI2_SPOP']          = NULL;
			$_SESSION['KD_KECAMATAN_SPOP']      = NULL;
			$_SESSION['KD_KELURAHAN_SPOP']      = NULL;
			$_SESSION['KD_BLOK_SPOP']           = NULL;
			$_SESSION['NO_URUT_NOP_SPOP']       = NULL;
			$_SESSION['KD_JNS_OP_SPOP']         = NULL;

			$_SESSION['KD_PROPINSI_ASAL_SPOP']  = NULL;
			$_SESSION['KD_DATI2_ASAL_SPOP']     = NULL;
			$_SESSION['KD_KECAMATAN_ASAL_SPOP'] = NULL;
			$_SESSION['KD_KELURAHAN_ASAL_SPOP'] = NULL;
			$_SESSION['KD_BLOK_ASAL_SPOP']      = NULL;
			$_SESSION['NO_URUT_ASAL_SPOP']      = NULL;
			$_SESSION['KD_JNS_OP_ASAL_SPOP']    = NULL;


			$_SESSION['KD_JNS_PELAYANAN_LSPOP'] = null;
			$_SESSION['THN_PELAYANAN']          = null;
			$_SESSION['BUNDEL_PELAYANAN']       = null;
			$_SESSION['NO_URUT_DALAM']          = null;
			$_SESSION['NO_URUT']                = null;
			$_SESSION['KD_PROPINSI']            = null;
			$_SESSION['KD_DATI2']               = null;
			$_SESSION['KD_KECAMATAN']           = null;
			$_SESSION['KD_KELURAHAN']           = null;
			$_SESSION['KD_BLOK']                = null;
			$_SESSION['NO_URUT_NOP']            = null;
			$_SESSION['KD_JNS_OP']              = null;

			$_SESSION['KD_PROPINSI_ASAL']       = null;
			$_SESSION['KD_DATI2_ASAL']          = null;
			$_SESSION['KD_KECAMATAN_ASAL']      = null;
			$_SESSION['KD_KELURAHAN_ASAL']      = null;
			$_SESSION['KD_BLOK_ASAL']           = null;
			$_SESSION['NO_URUT_ASAL']           = null;
			$_SESSION['KD_JNS_OP_ASAL']         = null;
			$_SESSION['NO_BNG']                 = null;


			$_SESSION['t_formulir'] = "active";
			$_SESSION['t_spop']     = NULL;
			$_SESSION['t_lspop']    = null;
			$data['nop_asal']                   = '';
			$data['JNS_PELAYANAN']              = '';
			$NIP                                = $this->session->userdata('nip');
			$data['nomor']                      = $this->db->query("select * from p_nomor where NIP_PEMBUAT='$NIP'")->result();
			$this->template->load('template', 'spop/form_spop', $data);
		}
	}
	function create_spop()
	{
		$KD_PROPINSI  	= $_POST['KD_PROPINSI'];
		$KD_DATI2     	= $_POST['KD_DATI2'];
		$KD_KECAMATAN 	= $_POST['KD_KECAMATAN'];
		$KD_KELURAHAN 	= $_POST['KD_KELURAHAN'];
		$KD_BLOK      	= $_POST['KD_BLOK'];
		$NO_URUT_NOP    = $_POST['NO_URUT_NOP'];
		$KD_JNS_OP    	= $_POST['KD_JNS_OP'];
		$NOP 			= $KD_PROPINSI . $KD_DATI2 . $KD_KECAMATAN . $KD_KELURAHAN . $KD_BLOK . $NO_URUT_NOP . $KD_JNS_OP;

		$KD_JNS_PELAYANAN = $_POST['KD_JNS_PELAYANAN'];
		$NO_FORMULIR    = $_POST['NO_FORMULIR_SPOP'];/*$_POST['THN_PELAYANAN'].$_POST['BUNDEL_PELAYANAN'].$_POST['NO_URUT_DALAM'].$_POST['NO_URUT'];*/
		$KTP_ID  		= $_POST['KTP_ID'];
		$PEKERJAAN     	= $_POST['PEKERJAAN'];
		$NOP_ASAL 		= $_POST['NOP_ASAL'];
		$NPWP  			= $_POST['NPWP'];
		$JALAN 			= $_POST['JALAN'];
		$RW_WP 			= $_POST['RW_WP'];
		$RT_WP 			= $_POST['RT_WP'];
		$DATI  			= $_POST['DATI'];
		$PERSIL			= $_POST['PERSIL'];
		$JALAN_OP	    = $_POST['JALAN_OP'];
		$BLOK_KAV_OP    = $_POST['BLOK_KAV_OP'];
		$ZNT    		= $_POST['ZNT'];
		$LUAS_TANAH     = $_POST['LUAS_TANAH'];
		$TGL_PENDATAAN  = date('Y-m-d', strtotime($this->input->post('TGL_PENDATAAN', TRUE)));
		$TGL_PENELITIAN = date('Y-m-d', strtotime($this->input->post('TGL_PENELITIAN', TRUE)));
		$STATUS_WP    	= $_POST['STATUS_WP'];
		$NAMA    		= $_POST['NAMA'];
		$TELP    		= $_POST['TELP'];
		$BLOK_KAV_WP    = $_POST['BLOK_KAV_WP'];
		$KELURAHAN    	= $_POST['KELURAHAN'];
		$KODE_POS    	= $_POST['KODE_POS'];
		$RW_OP    		= $_POST['RW_OP'];
		$RT_OP	    	= $_POST['RT_OP'];
		error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
		$CABANG    		= $_POST['CABANG'];
		$JENIS_TANAH    = $_POST['JENIS_TANAH'];
		$EXP0			= explode('|', $_POST['NIP_PENDATA']);
		$EXP1			= explode('|', $_POST['NIP_PENELITI']);
		$NIP_PENDATA    = $EXP0[0];
		$NIP_PENELITI   = $EXP1[0];

		if ($_POST['KD_JNS_PELAYANAN'] == 1) {
			$datab = array(
				'SUBJEK_PAJAK_ID'	  => $KTP_ID,
				'NM_WP'     	 	  => $NAMA,
				'JALAN_WP'    	 	  => $JALAN,
				'BLOK_KAV_NO_WP' 	  => $BLOK_KAV_WP,
				'RW_WP'     	 	  => $RW_WP,
				'RT_WP' 		 	  => $RT_WP,
				'KELURAHAN_WP' 	 	  => $KELURAHAN,
				'KOTA_WP'      	 	  => $DATI,
				'KD_POS_WP'      	  => $KODE_POS,
				'TELP_WP'    	 	  => $TELP,
				'NPWP'     		 	  => $NPWP,
				'STATUS_PEKERJAAN_WP' => $PEKERJAAN,
			);


			// cek subjek pajak
			$sd = $this->db->query("select * from DAT_SUBJEK_PAJAK_TEMP where subjek_pajak_id='" . $KTP_ID . "'")->row();
			if (empty($sd)) {
				$this->db->insert('DAT_SUBJEK_PAJAK_TEMP', $datab);
			} else {
				$this->db->where('SUBJEK_PAJAK_ID', $KTP_ID);
				$this->db->update('DAT_SUBJEK_PAJAK_TEMP', $datab);
			}

			$this->db->query("INSERT INTO tbl_spop ( NO_FORMULIR,JNS_TRANSAKSI,NOP_PROSES,NOP_ASAL,SUBJEK_PAJAK_ID,NM_WP,
													 JALAN_WP,BLOK_KAV_NO_WP,RW_WP,RT_WP,KELURAHAN_WP,KOTA_WP,
													 KD_POS_WP,NPWP,TELP_WP,STATUS_PEKERJAAN_WP,NO_PERSIL,JALAN_OP,BLOK_KAV_NO_OP,RW_OP,RT_OP,KD_STATUS_CABANG,KD_STATUS_WP,
													 KD_ZNT,LUAS_BUMI,JNS_BUMI,
													 TGL_PENDATAAN_OP,NIP_PENDATA,TGL_PEMERIKSAAN_OP,NIP_PEMERIKSA_OP,TGL_PEREKAMAN_OP, NIP_PEREKAM_OP)  
											VALUES ( '$NO_FORMULIR','$KD_JNS_PELAYANAN','$NOP','$NOP_ASAL','$KTP_ID','$NAMA',
													 '$JALAN','$BLOK_KAV_WP','$RW_WP','$RT_WP','$KELURAHAN','$DATI',
													 '$KODE_POS','$NPWP','$TELP','$PEKERJAAN','$PERSIL','$JALAN_OP','$BLOK_KAV_OP','$RW_OP','$RT_OP','$CABANG','$STATUS_WP',
													 '$ZNT','$LUAS_TANAH','$JENIS_TANAH',
													  TO_DATE('$TGL_PENDATAAN','yyyy-mm-dd'),'$NIP_PENDATA',TO_DATE('$TGL_PENELITIAN','yyyy-mm-dd'),'$NIP_PENELITI',TO_DATE('$TGL_PENDATAAN','yyyy-mm-dd'),'$NIP_PENDATA')");
		} else {
		}
		/*$this->db->query("INSERT INTO DAT_OBJEK_PAJAK (KD_PROPINSI,KD_DATI2,KD_KECAMATAN,KD_KELURAHAN,KD_BLOK,NO_URUT,KD_JNS_OP,
										  SUBJEK_PAJAK_ID,NO_FORMULIR_SPOP,NO_PERSIL,JALAN_OP,BLOK_KAV_NO_OP,RW_OP,RT_OP,
										  KD_STATUS_CABANG,KD_STATUS_WP,TOTAL_LUAS_BUMI,TOTAL_LUAS_BNG,NJOP_BUMI,NJOP_BNG,
										  
										  STATUS_PETA_OP,JNS_TRANSAKSI_OP,
										  TGL_PENDATAAN_OP,NIP_PENDATA,
										  TGL_PEMERIKSAAN_OP,NIP_PEMERIKSA_OP,
										  TGL_PEREKAMAN_OP,NIP_PEREKAM_OP) 
										  VALUES ('$KD_PROPINSI','$KD_DATI2','$KD_KECAMATAN','$KD_KELURAHAN','$KD_BLOK','$NO_URUT_NOP','$KD_JNS_OP',
										  '$KTP_ID','$NO_FORMULIR','$PERSIL','$JALAN','$BLOK_KAV_OP','$RW_OP','$RT_OP',
										  '$CABANG','$STATUS_WP','$LUAS_TANAH','0','0','0',
										  
										  '0','$KD_JNS_PELAYANAN',
										  to_date('20-AUG-09','DD-MON-RR'),'060000000000000000',
										  to_date('20-AUG-09','DD-MON-RR'),'060000000000000000',
										  to_date('13-NOV-09','DD-MON-RR'),'060000000000000000')");*/
		$this->db->query("UPDATE P_NOMOR SET STATUS = '1' WHERE  NO_FORMULIR= '$NO_FORMULIR' AND NOP = '$NOP'");
		redirect('spop/form_spop');
	}
	function generate_nomor()
	{
		$NIP = $this->session->userdata('nip');
		$data['nomor'] = $this->db->query("select * from p_nomor where NIP_PEMBUAT='$NIP'")->result();
		$this->template->load('template', 'spop/form_generate_nomor', $data);
	}
	function proses_generate_nomor()
	{
		$NIP = $this->session->userdata('nip');
		$JNS_FORMULIR      = $_POST['KD_JNS_FORMULIR'];
		$NO_BANGUNAN	   = $_POST['NO_BNG'];

		$KD_PROPINSI  = $_POST['KD_PROPINSI'];
		$KD_DATI2     = $_POST['KD_DATI2'];
		$KD_KECAMATAN = $_POST['KD_KECAMATAN'];
		$KD_KELURAHAN = $_POST['KD_KELURAHAN'];
		$KD_BLOK      = $_POST['KD_BLOK'];
		$NO_URUT_NOP  = $_POST['NO_URUT_NOP'];
		$KD_JNS_OP    = $_POST['KD_JNS_OP'];
		$NOP 		  = $KD_PROPINSI . $KD_DATI2 . $KD_KECAMATAN . $KD_KELURAHAN . $KD_BLOK . $NO_URUT_NOP . $KD_JNS_OP;

		$rk = $this->db->query("SELECT  TO_CHAR(CURRENT_DATE,'YYYY') TAHUN ,  
									 CASE WHEN  MAX( TO_NUMBER(NO_URUT)) < 200 THEN   MAX( TO_NUMBER(BUNDEL))  ELSE MAX( TO_NUMBER(BUNDEL))+1 END   BUNDEL_PELAYANAN ,
									 CASE WHEN   MAX( TO_NUMBER(NO_URUT)) <200 THEN   MAX( TO_NUMBER(NO_URUT)) +1 ELSE 1 END  NO_URUT
                                FROM P_NOMOR 
                                WHERE TAHUN=TO_CHAR(CURRENT_DATE,'YYYY')
                                AND jenis_pelayanan='$JNS_FORMULIR'
                                AND BUNDEL IN (SELECT  MAX( TO_NUMBER(BUNDEL)) BUNDEL_PELAYANAN
                                FROM P_NOMOR WHERE TAHUN=TO_CHAR(CURRENT_DATE,'YYYY') AND jenis_pelayanan='$JNS_FORMULIR')")->row();
		error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
		$TAHUN  = $rk->TAHUN;
		$JENIS_PELAYANAN = $JNS_FORMULIR;
		$BUNDEL = sprintf('%03s', $rk->BUNDEL_PELAYANAN);
		if ($rk->NO_URUT % 2 == 0) {
			$URUT  = sprintf('%03s', $rk->NO_URUT + 1);
		} else {
			$URUT  = sprintf('%03s', $rk->NO_URUT);
		}
		$NO_FORM = $TAHUN . $JENIS_PELAYANAN . $BUNDEL . $URUT;
		$this->db->query("INSERT INTO P_NOMOR ( NO_FORMULIR,TAHUN,JENIS_PELAYANAN,BUNDEL,NO_URUT,NOP,
 														NIP_PEMBUAT,TGL_PEREKAMAN,KETERANGAN_NOMOR,NO_BANGUNAN)  
											VALUES( '$NO_FORM','$TAHUN','$JENIS_PELAYANAN','$BUNDEL','$URUT','$NOP',
													'$NIP',SYSDATE,'SPOP','0')");

		if ($NO_BANGUNAN != '' or $NO_BANGUNAN != 0) {
			$NO_BANGUNAN1 = $NO_BANGUNAN + 1;
			for ($i = 1; $i < $NO_BANGUNAN1; $i++) {
				$rk = $this->db->query("SELECT  TO_CHAR(CURRENT_DATE,'YYYY') TAHUN ,  
									 CASE WHEN  MAX( TO_NUMBER(NO_URUT)) <200 THEN   MAX( TO_NUMBER(BUNDEL))  ELSE MAX( TO_NUMBER(BUNDEL))+1 END   BUNDEL_PELAYANAN ,
									 CASE WHEN  MAX( TO_NUMBER(NO_URUT)) <200 THEN   MAX( TO_NUMBER(NO_URUT)) +1 ELSE 1 END  NO_URUT
                                FROM P_NOMOR 
                                WHERE TAHUN=TO_CHAR(CURRENT_DATE,'YYYY')
                                AND jenis_pelayanan='$JNS_FORMULIR'
                                AND BUNDEL IN (SELECT  MAX( TO_NUMBER(BUNDEL)) BUNDEL_PELAYANAN
                                FROM P_NOMOR WHERE TAHUN=TO_CHAR(CURRENT_DATE,'YYYY') AND jenis_pelayanan='$JNS_FORMULIR')")->row();
				error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
				$TAHUN = $rk->TAHUN;
				$JENIS_PELAYANAN = $JNS_FORMULIR;
				$BUNDEL = sprintf('%03s', $rk->BUNDEL_PELAYANAN);
				if ($rk->NO_URUT % 2 == '0') {
					$URUT  = sprintf('%03s', $rk->NO_URUT);
				} else {
					$URUT  = sprintf('%03s', $rk->NO_URUT + 1);
				}
				$NO_FORM = $TAHUN . $JENIS_PELAYANAN . $BUNDEL . $URUT;
				$this->db->query("INSERT INTO P_NOMOR ( NO_FORMULIR,TAHUN,JENIS_PELAYANAN,BUNDEL,NO_URUT,NOP,
 														NIP_PEMBUAT,TGL_PEREKAMAN,KETERANGAN_NOMOR,NO_BANGUNAN)  
											VALUES( '$NO_FORM','$TAHUN','$JENIS_PELAYANAN','$BUNDEL','$URUT','$NOP',
													'$NIP',SYSDATE,'LSPOP','$i')");
			}
		}
		// $_SESSION['t_formulir']='active';
		redirect('spop/form_spop');
	}

	function getformulir()
	{
		if (isset($_POST['nop'])) {
			$NOP = $_POST['nop'];
			$exp = explode('=', $NOP);
			$id = str_replace('|', '', $exp[0]);
			$rk = $this->db->query("select tahun,jenis_pelayanan,bundel,no_urut from p_nomor where nop='$id' and keterangan_nomor='SPOP' AND STATUS='0'")->row();
			error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
			echo $rk->TAHUN . '|' . $rk->JENIS_PELAYANAN . '|' . $rk->BUNDEL . '|' . $rk->NO_URUT;
		}
	}
	function getNop()
	{
		if (isset($_POST['nop'])) {
			$nop = $_POST['nop'];
			$exp = explode('=', $nop);
			$id = str_replace('|', '', $exp[0]);
			$jns = $exp[1];
			$rk = $this->db->query("select * from dat_objek_pajak
									where subjek_pajak_id='$id'")->row();
			error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
			if (count($rk) >= '1') {
				$cek = '1';
			} else {
				$cek = '0';
			};
			echo $cek . '|' . $jns;
		}
	}

	function lookup_kelurahan()
	{
		$keyword = $this->input->post('term');
		$data['response'] = 'false'; //Set default response        
		$query = $this->db->query("select nm_kelurahan  from ref_kelurahan where nm_kelurahan like UPPER('%$keyword%')")->result(); //Search DB
		if (!empty($query)) {
			$data['response'] = 'true'; //Set response
			$data['message'] = array(); //Create array
			foreach ($query as $row) {
				$data['message'][] = array(
					'value' => $row->NM_KELURAHAN,
					''
				);  //Add a row to array
			}
		}
		if ($data != '') {
			echo json_encode($data); //echo json string if ajax request
		} else {
		}
	}
	function lookup_dati2()
	{
		$keyword = $this->input->post('term');
		$data['response'] = 'false'; //Set default response        
		$query = $this->db->query("select nm_dati2  from ref_dati2 where nm_dati2 like UPPER('%$keyword%')")->result(); //Search DB
		if (!empty($query)) {
			$data['response'] = 'true'; //Set response
			$data['message'] = array(); //Create array
			foreach ($query as $row) {
				$data['message'][] = array(
					'value' => $row->NM_DATI2,
					''
				);  //Add a row to array
			}
		}
		if ($data != '') {
			echo json_encode($data); //echo json string if ajax request
		} else {
		}
	}
	function lookup_karyawan()
	{
		$keyword = $this->input->post('term');
		$data['response'] = 'false'; //Set default response        
		$query = $this->db->query("select *  from pegawai where nm_pegawai like UPPER('%$keyword%') or nip like '%$keyword%'")->result(); //Search DB
		if (!empty($query)) {
			$data['response'] = 'true'; //Set response
			$data['message'] = array(); //Create array
			foreach ($query as $row) {
				$data['message'][] = array(
					'value' => $row->NIP . '|' . $row->NM_PEGAWAI,
					''
				);  //Add a row to array
			}
		}
		if ($data != '') {
			echo json_encode($data); //echo json string if ajax request
		} else {
		}
	}
	function penilaian()
	{
		$data['kecamatan']				 = $this->db->query("select kd_kecamatan,kd_propinsi||'.'||kd_dati2||'.'||kd_kecamatan kode,kd_propinsi||'.'||kd_dati2||'.'||kd_kecamatan||' - '||nm_kecamatan nama
 															from ref_kecamatan where kd_dati2='07'")->result_array();
		$this->template->load('template', 'spop/penilaian', $data);
	}
	function proses_penilaian()
	{
		$param  = $this->input->post('param');
		$tahun  = $this->input->post('tahun');
		$njoptkp = $this->input->post('njoptkp');
		$exp = explode('|', $param);


		$jumlah = count($exp);
		if ($njoptkp == 'true') {
		} else {
			for ($i = 0; $i < $jumlah; $i++) {
				$pecah = explode('.', $exp[$i]);
				error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
				$this->db->query("call penilaian_massal('$tahun','$pecah[0]','$pecah[1]','$pecah[2]','$pecah[3]')");
				//echo '<pre>'; print_r($pecah[0]); echo '</pre>';
			}
		}
		//echo $this->db->last_query();
	}
	function tes_jason()
	{
		$param = $this->input->post('param');
		$exp = explode('|', $param);
		$jumlah = count($exp);
		for ($i = 0; $i < $jumlah; $i++) {
			$pecah = explode('.', $exp[$i]);
			error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
			//echo '<pre>'; print_r($pecah[2].','.$pecah[3]); echo '</pre>';
			echo $pecah[2];
			echo $pecah[3];
			$data['data1']	= $this->db->query("select c.nm_kecamatan,b.nm_kelurahan, a.kd_propinsi||'.'||a.kd_dati2||'.'||a.kd_kecamatan||'.'||a.kd_kelurahan kode,count(*)jumlah from dat_objek_pajak a
											JOIN ref_kelurahan b on a.kd_kelurahan=b.kd_kelurahan and a.kd_kecamatan=b.kd_kecamatan
											JOIN ref_kecamatan c on c.kd_kecamatan=a.kd_kecamatan 
											where a.kd_kecamatan in('$pecah[2]') and a.kd_kelurahan in('$pecah[3]')
											group by c.nm_kecamatan,b.nm_kelurahan, a.kd_kecamatan,a.kd_kelurahan,a.kd_propinsi||'.'||a.kd_dati2||'.'||a.kd_kecamatan")->result();
			$this->load->view('spop/ajax_penilaian', $data);
		}
		//$this->load->view('spop/ajax_penilaian',$data);
		/*$data['data1']	=$this->db->query("select c.nm_kecamatan,b.nm_kelurahan, a.kd_propinsi||'.'||a.kd_dati2||'.'||a.kd_kecamatan||'.'||a.kd_kelurahan kode,count(*)jumlah from dat_objek_pajak a
											JOIN ref_kelurahan b on a.kd_kelurahan=b.kd_kelurahan and a.kd_kecamatan=b.kd_kecamatan
											JOIN ref_kecamatan c on c.kd_kecamatan=a.kd_kecamatan 
											where a.kd_kecamatan in('310') and a.kd_kelurahan in('001','002')
											group by c.nm_kecamatan,b.nm_kelurahan, a.kd_kecamatan,a.kd_kelurahan,a.kd_propinsi||'.'||a.kd_dati2||'.'||a.kd_kecamatan")->result();
    	$this->load->view('spop/ajax_penilaian',$data); */
	}


	function dataSpop()
	{
		// $data['data']=$this->db->query("select * from tbl_spop")->result();
		$nop = urldecode($this->input->get('NOP'));
		if ($nop <> '') {
			$this->db->select('*');
			$this->db->where('NOP_PROSES', str_replace('.', '', $this->input->get('NOP')));
			$data = $this->db->get('TBL_SPOP')->row();
			$this->db->select('*');
			$this->db->where('NOP', str_replace('.', '', $this->input->get('NOP')));
			$dat = $this->db->get('TBL_LSPOP')->row();
		} else {
			$data = array();
			$dat = array();
		}
		// echo $this->db->last_query();
		$this->template->load('template', 'spop/dataSpop', array('res' => $data, 'nop' => $nop, 'ls' => $dat));
	}
}
