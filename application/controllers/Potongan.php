<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Potongan extends CI_Controller
{
  function __construct()
  {
    parent::__construct();
    $this->load->model('Msppt');
    $this->load->library('datatables');
    /*$this->load->model('Mspop');
		$this->load->model('Mpermohonan');*/
    if ($this->session->userdata('pbb') <> 1) {
      $this->session->set_flashdata('notif', '<div class="badge">
                    Silahkan login dengan username dan password anda.</p>
                    </div>');
      redirect('auth');
    }
  }





  function index()
  {
    $data = array();
    $this->template->load('template', 'potongan/list', $data);
  }

  function submit()
  {
    $var = str_replace('.', '', $this->input->post('NOP'));


    $th = $this->input->post('TAHUN_PAJAK');
    $cek = $this->db->query("select count(1) cek from p_pot_denda WHERE thn_pajak = $th
               AND kd_propinsi = SUBSTR ('$var', 1, 2)
               AND kd_dati2 = SUBSTR ('$var', 3, 2)
               AND kd_kecamatan = SUBSTR ('$var', 5, 3)
               AND kd_kelurahan = SUBSTR ('$var', 8, 3)
               AND kd_blok = SUBSTR ('$var', 11, 3)
               AND no_urut = SUBSTR ('$var', 14, 4)
               AND kd_jns_op = SUBSTR ('$var', 18, 1)")->row();

    if ($cek->CEK == 0) {
      $this->db->set('KD_PROPINSI', "SUBSTR ('" . $var . "', 1, 2)", false);
      $this->db->set('KD_DATI2', "SUBSTR ('" . $var . "', 3, 2)", false);
      $this->db->set('KD_KECAMATAN', "SUBSTR ('" . $var . "', 5, 3)", false);
      $this->db->set('KD_KELURAHAN', "SUBSTR ('" . $var . "', 8, 3)", false);
      $this->db->set('KD_BLOK', "SUBSTR ('" . $var . "', 11, 3)", false);
      $this->db->set('NO_URUT', "SUBSTR ('" . $var . "', 14, 4)", false);
      $this->db->set('KD_JNS_OP', "SUBSTR ('" . $var . "', 18, 1)", false);
      $this->db->set("THN_PAJAK", $this->input->post('TAHUN_PAJAK'));
      $this->db->set('TGL_SK', "to_date('" . $this->input->post('TGL_SK') . "','dd-mm-yyyy')", false);
      $this->db->set('JUMLAH_POTONGAN', $this->input->post('JUMLAH_POTONGAN'));
      $this->db->set('USER_ENTRI', $this->session->userdata('pbb_ku'));
      $this->db->insert('P_POT_DENDA');
    }

    redirect('potongan/listdata');
  }


  function listdata()
  {
    $data['data'] = $this->db->query('SELECT * FROM P_POT_DENDA ORDER BY THN_PAJAK DESC')->result();
    $this->template->load('template', 'potongan/listdata', $data);
  }


  function getDataNop()
  {
    $var = $this->input->get('nop');

    $sql = "SELECT a.*,get_alamat_op('ALAMAT',SUBSTR ('$var', 1, 18)) alamat_op, get_alamat_op('RT',SUBSTR ('$var', 1, 18)) RT_op, get_alamat_op('RW',SUBSTR ('$var', 1, 18)) RW_op,get_denda@to17(a.kd_dati2, a.kd_kecamatan, a.kd_kelurahan, a.kd_blok, a.no_urut, a.kd_jns_op,thn_pajak_sppt,pbb_yg_harus_dibayar_sppt,tgl_jatuh_tempo_sppt,sysdate) denda
          FROM sppt a
         WHERE      thn_pajak_sppt = SUBSTR ('$var', 19, 4)
               AND kd_propinsi = SUBSTR ('$var', 1, 2)
               AND kd_dati2 = SUBSTR ('$var', 3, 2)
               AND kd_kecamatan = SUBSTR ('$var', 5, 3)
               AND kd_kelurahan = SUBSTR ('$var', 8, 3)
               AND kd_blok = SUBSTR ('$var', 11, 3)
               AND no_urut = SUBSTR ('$var', 14, 4)
               AND kd_jns_op = SUBSTR ('$var', 18, 1)";
    $res = $this->db->query($sql)->row();
    // echo "<pre>";
    // print_r($this->input->get());
    // echo "</pre>";
    if ($res) {
      $KD_PROPINSI               = $res->KD_PROPINSI <> '' ? $res->KD_PROPINSI : '';
      $KD_DATI2                  = $res->KD_DATI2 <> '' ? $res->KD_DATI2 : '';
      $KD_KECAMATAN              = $res->KD_KECAMATAN <> '' ? $res->KD_KECAMATAN : '';
      $KD_KELURAHAN              = $res->KD_KELURAHAN <> '' ? $res->KD_KELURAHAN : '';
      $KD_BLOK                   = $res->KD_BLOK <> '' ? $res->KD_BLOK : '';
      $NO_URUT                   = $res->NO_URUT <> '' ? $res->NO_URUT : '';
      $KD_JNS_OP                 = $res->KD_JNS_OP <> '' ? $res->KD_JNS_OP : '';
      $THN_PAJAK_SPPT            = $res->THN_PAJAK_SPPT <> '' ? $res->THN_PAJAK_SPPT : '';
      $SIKLUS_SPPT               = $res->SIKLUS_SPPT <> '' ? $res->SIKLUS_SPPT : '';
      $KD_KANWIL                 = $res->KD_KANWIL <> '' ? $res->KD_KANWIL : '';
      $KD_KANTOR                 = $res->KD_KANTOR <> '' ? $res->KD_KANTOR : '';
      $KD_TP                     = $res->KD_TP <> '' ? $res->KD_TP : '';
      $NM_WP_SPPT                = $res->NM_WP_SPPT <> '' ? $res->NM_WP_SPPT : '';
      $JLN_WP_SPPT               = $res->JLN_WP_SPPT <> '' ? $res->JLN_WP_SPPT : '';
      $BLOK_KAV_NO_WP_SPPT       = $res->BLOK_KAV_NO_WP_SPPT <> '' ? $res->BLOK_KAV_NO_WP_SPPT : '';
      $RW_WP_SPPT                = $res->RW_WP_SPPT <> '' ? $res->RW_WP_SPPT : '';
      $RT_WP_SPPT                = $res->RT_WP_SPPT <> '' ? $res->RT_WP_SPPT : '';
      $KELURAHAN_WP_SPPT         = $res->KELURAHAN_WP_SPPT <> '' ? $res->KELURAHAN_WP_SPPT : '';
      $KOTA_WP_SPPT              = $res->KOTA_WP_SPPT <> '' ? $res->KOTA_WP_SPPT : '';
      $KD_POS_WP_SPPT            = $res->KD_POS_WP_SPPT <> '' ? $res->KD_POS_WP_SPPT : '';
      $NPWP_SPPT                 = $res->NPWP_SPPT <> '' ? $res->NPWP_SPPT : '';
      $NO_PERSIL_SPPT            = $res->NO_PERSIL_SPPT <> '' ? $res->NO_PERSIL_SPPT : '';
      $KD_KLS_TANAH              = $res->KD_KLS_TANAH <> '' ? $res->KD_KLS_TANAH : '';
      $THN_AWAL_KLS_TANAH        = $res->THN_AWAL_KLS_TANAH <> '' ? $res->THN_AWAL_KLS_TANAH : '';
      $KD_KLS_BNG                = $res->KD_KLS_BNG <> '' ? $res->KD_KLS_BNG : '';
      $THN_AWAL_KLS_BNG          = $res->THN_AWAL_KLS_BNG <> '' ? $res->THN_AWAL_KLS_BNG : '';
      $TGL_JATUH_TEMPO_SPPT      = $res->TGL_JATUH_TEMPO_SPPT <> '' ? $res->TGL_JATUH_TEMPO_SPPT : '';
      $LUAS_BUMI_SPPT            = $res->LUAS_BUMI_SPPT <> '' ? $res->LUAS_BUMI_SPPT : '';
      $LUAS_BNG_SPPT             = $res->LUAS_BNG_SPPT <> '' ? $res->LUAS_BNG_SPPT : '';
      $NJOP_BUMI_SPPT            = $res->NJOP_BUMI_SPPT <> '' ? $res->NJOP_BUMI_SPPT : '';
      $NJOP_BNG_SPPT             = $res->NJOP_BNG_SPPT <> '' ? $res->NJOP_BNG_SPPT : '';
      $NJOP_SPPT                 = $res->NJOP_SPPT <> '' ? $res->NJOP_SPPT : '';
      $NJOPTKP_SPPT              = $res->NJOPTKP_SPPT <> '' ? $res->NJOPTKP_SPPT : '';
      $PBB_TERHUTANG_SPPT        = $res->PBB_TERHUTANG_SPPT <> '' ? $res->PBB_TERHUTANG_SPPT : '';
      $FAKTOR_PENGURANG_SPPT     = $res->FAKTOR_PENGURANG_SPPT <> '' ? $res->FAKTOR_PENGURANG_SPPT : '';
      $PBB_YG_HARUS_DIBAYAR_SPPT = $res->PBB_YG_HARUS_DIBAYAR_SPPT <> '' ? $res->PBB_YG_HARUS_DIBAYAR_SPPT : '';
      $STATUS_PEMBAYARAN_SPPT    = $res->STATUS_PEMBAYARAN_SPPT <> '1' ? 'Lunas' : 'Belum Lunas';
      $STATUS_TAGIHAN_SPPT       = $res->STATUS_TAGIHAN_SPPT <> '' ? $res->STATUS_TAGIHAN_SPPT : '';
      $STATUS_CETAK_SPPT         = $res->STATUS_CETAK_SPPT <> '' ? $res->STATUS_CETAK_SPPT : '';
      $TGL_TERBIT_SPPT           = $res->TGL_TERBIT_SPPT <> '' ? $res->TGL_TERBIT_SPPT : '';
      $TGL_CETAK_SPPT            = $res->TGL_CETAK_SPPT <> '' ? $res->TGL_CETAK_SPPT : '';
      $NIP_PENCETAK_SPPT         = $res->NIP_PENCETAK_SPPT <> '' ? $res->NIP_PENCETAK_SPPT : '';
      $ALAMAT_OP = $res->ALAMAT_OP <> '' ? $res->ALAMAT_OP : '';
      $RT_OP = $res->RT_OP <> '' ? $res->RT_OP : '';
      $RW_OP = $res->RW_OP <> '' ? $res->RW_OP : '';
      $DENDA = $res->DENDA;
    } else {
      $KD_PROPINSI               = '';
      $KD_DATI2                  = '';
      $KD_KECAMATAN              = '';
      $KD_KELURAHAN              = '';
      $KD_BLOK                   = '';
      $NO_URUT                   = '';
      $KD_JNS_OP                 = '';
      $THN_PAJAK_SPPT            = '';
      $SIKLUS_SPPT               = '';
      $KD_KANWIL                 = '';
      $KD_KANTOR                 = '';
      $KD_TP                     = '';
      $NM_WP_SPPT                = '';
      $JLN_WP_SPPT               = '';
      $BLOK_KAV_NO_WP_SPPT       = '';
      $RW_WP_SPPT                = '';
      $RT_WP_SPPT                = '';
      $KELURAHAN_WP_SPPT         = '';
      $KOTA_WP_SPPT              = '';
      $KD_POS_WP_SPPT            = '';
      $NPWP_SPPT                 = '';
      $NO_PERSIL_SPPT            = '';
      $KD_KLS_TANAH              = '';
      $THN_AWAL_KLS_TANAH        = '';
      $KD_KLS_BNG                = '';
      $THN_AWAL_KLS_BNG          = '';
      $TGL_JATUH_TEMPO_SPPT      = '';
      $LUAS_BUMI_SPPT            = '';
      $LUAS_BNG_SPPT             = '';
      $NJOP_BUMI_SPPT            = '';
      $NJOP_BNG_SPPT             = '';
      $NJOP_SPPT                 = '';
      $NJOPTKP_SPPT              = '';
      $PBB_TERHUTANG_SPPT        = '';
      $FAKTOR_PENGURANG_SPPT     = '';
      $PBB_YG_HARUS_DIBAYAR_SPPT = '';
      $STATUS_PEMBAYARAN_SPPT    = '';
      $STATUS_TAGIHAN_SPPT       = '';
      $STATUS_CETAK_SPPT         = '';
      $TGL_TERBIT_SPPT           = '';
      $TGL_CETAK_SPPT            = '';
      $NIP_PENCETAK_SPPT         = '';
      $ALAMAT_OP = '';
      $RT_OP     = '';
      $RW_OP     = '';
      $DENDA = 0;
    }

    $row['KD_PROPINSI']               = $KD_PROPINSI;
    $row['KD_DATI2']                  = $KD_DATI2;
    $row['KD_KECAMATAN']              = $KD_KECAMATAN;
    $row['KD_KELURAHAN']              = $KD_KELURAHAN;
    $row['KD_BLOK']                   = $KD_BLOK;
    $row['NO_URUT']                   = $NO_URUT;
    $row['KD_JNS_OP']                 = $KD_JNS_OP;
    $row['THN_PAJAK_SPPT']            = $THN_PAJAK_SPPT;
    $row['SIKLUS_SPPT']               = $SIKLUS_SPPT;
    $row['KD_KANWIL']                 = $KD_KANWIL;
    $row['KD_KANTOR']                 = $KD_KANTOR;
    $row['KD_TP']                     = $KD_TP;
    $row['NM_WP_SPPT']                = $NM_WP_SPPT;
    $row['JLN_WP_SPPT']               = $JLN_WP_SPPT;
    $row['BLOK_KAV_NO_WP_SPPT']       = $BLOK_KAV_NO_WP_SPPT;
    $row['RW_WP_SPPT']                = $RW_WP_SPPT;
    $row['RT_WP_SPPT']                = $RT_WP_SPPT;
    $row['KELURAHAN_WP_SPPT']         = $KELURAHAN_WP_SPPT;
    $row['KOTA_WP_SPPT']              = $KOTA_WP_SPPT;
    $row['KD_POS_WP_SPPT']            = $KD_POS_WP_SPPT;
    $row['NPWP_SPPT']                 = $NPWP_SPPT;
    $row['NO_PERSIL_SPPT']            = $NO_PERSIL_SPPT;
    $row['KD_KLS_TANAH']              = $KD_KLS_TANAH;
    $row['THN_AWAL_KLS_TANAH']        = $THN_AWAL_KLS_TANAH;
    $row['KD_KLS_BNG']                = $KD_KLS_BNG;
    $row['THN_AWAL_KLS_BNG']          = $THN_AWAL_KLS_BNG;
    $row['TGL_JATUH_TEMPO_SPPT']      = $TGL_JATUH_TEMPO_SPPT;
    $row['LUAS_BUMI_SPPT']            = $LUAS_BUMI_SPPT;
    $row['LUAS_BNG_SPPT']             = $LUAS_BNG_SPPT;
    $row['NJOP_BUMI_SPPT']            = $NJOP_BUMI_SPPT;
    $row['NJOP_BNG_SPPT']             = $NJOP_BNG_SPPT;
    $row['NJOP_SPPT']                 = $NJOP_SPPT;
    $row['NJOPTKP_SPPT']              = $NJOPTKP_SPPT;
    $row['PBB_TERHUTANG_SPPT']        = $PBB_TERHUTANG_SPPT;
    $row['FAKTOR_PENGURANG_SPPT']     = $FAKTOR_PENGURANG_SPPT;
    $row['PBB_YG_HARUS_DIBAYAR_SPPT'] = number_format($PBB_YG_HARUS_DIBAYAR_SPPT, 0, '', '.');
    $row['STATUS_PEMBAYARAN_SPPT']    = $STATUS_PEMBAYARAN_SPPT;
    $row['STATUS_TAGIHAN_SPPT']       = $STATUS_TAGIHAN_SPPT;
    $row['STATUS_CETAK_SPPT']         = $STATUS_CETAK_SPPT;
    $row['TGL_TERBIT_SPPT']           = $TGL_TERBIT_SPPT;
    $row['TGL_CETAK_SPPT']            = $TGL_CETAK_SPPT;
    $row['NIP_PENCETAK_SPPT']         = $NIP_PENCETAK_SPPT;
    $row['ALAMAT_OP'] = $ALAMAT_OP;
    $row['RT_OP'] = $RT_OP;
    $row['RW_OP'] = $RW_OP;
    $row['DENDA'] = number_format($DENDA, 0, '', '.');
    $row_set[]                 = $row;
    echo $return = json_encode($row_set);
  }
}
