<?php if (!defined('BASEPATH'))   exit('No direct script access allowed');

class Disposisi extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        if ($this->session->userdata('pbb') <> 1) {
            $this->session->set_flashdata('notif', '<div class="badge">
                    Silahkan login dengan username dan password anda.</p>
                    </div>');
            redirect('auth');
        }
        $this->load->model('Mpermohonan');
        $this->load->model('Mdispo');
        $this->load->library('datatables');
        $this->kg = $this->session->userdata('pbb_kg');
    }

    public function index()
    {
        $this->template->load('template', 'disposisi/listdata');
    }

    public function jsonmonitoring()
    {
        header('Content-Type: application/json');
        echo $this->Mdispo->jsonmonitoringdispo();
    }

    function share($id)
    {
        if ($this->kg == '35') {
            $this->shareKaPedanil($id);
        } else {
            $this->pedanilSelesai($id);
        }
    }

    function shareKaPedanil($id)
    {
        $data = $this->Mdispo->getDataShare($id);
        $this->template->load('template', 'disposisi/share', $data);
    }

    function pedanilSelesai($id)
    {
        $data = $this->Mdispo->getDataShareSelesai($id);
        $this->template->load('template', 'disposisi/pedanilSelesai', $data);
    }


    function proseskirimdokumen()
    {
        $KD_KANWIL            = $_POST['KD_KANWIL'];
        $KD_KANTOR            = $_POST['KD_KANTOR'];
        $THN_PELAYANAN        = $_POST['THN_PELAYANAN'];
        $BUNDEL_PELAYANAN     = $_POST['BUNDEL_PELAYANAN'];
        $NO_URUT_PELAYANAN    = $_POST['NO_URUT_PELAYANAN'];
        $KD_PROPINSI_RIWAYAT  = $_POST['KD_PROPINSI_RIWAYAT'];
        $KD_DATI2_RIWAYAT     = $_POST['KD_DATI2_RIWAYAT'];
        $KD_KECAMATAN_RIWAYAT = $_POST['KD_KECAMATAN_RIWAYAT'];
        $KD_KELURAHAN_RIWAYAT = $_POST['KD_KELURAHAN_RIWAYAT'];
        $KD_BLOK_RIWAYAT      = $_POST['KD_BLOK_RIWAYAT'];
        $NO_URUT_RIWAYAT      = $_POST['NO_URUT_RIWAYAT'];
        $KD_JNS_OP_RIWAYAT    = $_POST['KD_JNS_OP_RIWAYAT'];
        $NIP                  = $_POST['NIP'];

        $TANGGAL_AWAL         = $_POST['TANGGAL_AWAL'];
        $KODE_GROUP           = $_POST['KODE_GROUP'];
        $KETERANGAN           = NULL;
        $TANGGAL_AKHIR        = $_POST['TANGGAL_AKHIR'];
        $STATUS_BERKAS        = $_POST['STATUS_BERKAS'];





        $dataupdate = array(
            'KD_KANWIL'            => $KD_KANWIL,
            'KD_KANTOR'            => $KD_KANTOR,
            'THN_PELAYANAN'        => $THN_PELAYANAN,
            'BUNDEL_PELAYANAN'     => $BUNDEL_PELAYANAN,
            'NO_URUT_PELAYANAN'    => $NO_URUT_PELAYANAN,
            'KD_PROPINSI_RIWAYAT'  => $KD_PROPINSI_RIWAYAT,
            'KD_DATI2_RIWAYAT'     => $KD_DATI2_RIWAYAT,
            'KD_KECAMATAN_RIWAYAT' => $KD_KECAMATAN_RIWAYAT,
            'KD_KELURAHAN_RIWAYAT' => $KD_KELURAHAN_RIWAYAT,
            'KD_BLOK_RIWAYAT'      => $KD_BLOK_RIWAYAT,
            'NO_URUT_RIWAYAT'      => $NO_URUT_RIWAYAT,
            'KD_JNS_OP_RIWAYAT'    => $KD_JNS_OP_RIWAYAT,
            'KODE_GROUP'           => $this->session->userdata('pbb_kg'),
            'NIP'                  => $NIP,
            'KETERANGAN'           => $KETERANGAN,
            'TANGGAL_AKHIR'        => $TANGGAL_AKHIR,
            'STATUS_BERKAS'        => $STATUS_BERKAS
        );

        $datanext = array(
            "KD_KANWIL"            => $KD_KANWIL,
            "KD_KANTOR"            => $KD_KANTOR,
            "THN_PELAYANAN"        => $THN_PELAYANAN,
            "BUNDEL_PELAYANAN"     => $BUNDEL_PELAYANAN,
            "NO_URUT_PELAYANAN"    => $NO_URUT_PELAYANAN,
            "KD_PROPINSI_RIWAYAT"  => $KD_PROPINSI_RIWAYAT,
            "KD_DATI2_RIWAYAT"     => $KD_DATI2_RIWAYAT,
            "KD_KECAMATAN_RIWAYAT" => $KD_KECAMATAN_RIWAYAT,
            "KD_KELURAHAN_RIWAYAT" => $KD_KELURAHAN_RIWAYAT,
            "KD_BLOK_RIWAYAT"      => $KD_BLOK_RIWAYAT,
            "NO_URUT_RIWAYAT"      => $NO_URUT_RIWAYAT,
            "KD_JNS_OP_RIWAYAT"    => $KD_JNS_OP_RIWAYAT,
            "KODE_GROUP"           => $KODE_GROUP,
            "NIP"                  => null,
            "TANGGAL_AWAL"         => $TANGGAL_AWAL,
            "TANGGAL_AKHIR"        => null,
            "KETERANGAN"           => null,
            'UNIT_SEBEUMNYA'       => $this->kg
        );


        $disposisi = array(
            'KD_KANWIL'             => $KD_KANWIL,
            'KD_KANTOR'             => $KD_KANTOR,
            'THN_PELAYANAN'         => $THN_PELAYANAN,
            'BUNDEL_PELAYANAN'      => $BUNDEL_PELAYANAN,
            'NO_URUT_PELAYANAN'     => $NO_URUT_PELAYANAN,
            'KD_PROPINSI'           => $KD_PROPINSI_RIWAYAT,
            'KD_DATI2'              => $KD_DATI2_RIWAYAT,
            'KD_KECAMATAN'          => $KD_KECAMATAN_RIWAYAT,
            'KD_KELURAHAN'          => $KD_KELURAHAN_RIWAYAT,
            'KD_BLOK'               => $KD_BLOK_RIWAYAT,
            'NO_URUT'               => $NO_URUT_RIWAYAT,
            'KD_JNS_OP'             => $KD_JNS_OP_RIWAYAT,
            'NIP'                   => $_POST['NIP_DISPOSISI'],
            'TGL_AWAL'              => date('Y-m-d',  strtotime($_POST['TGL_AWAL'])),
            'TGL_PERKIRAAN_SELESAI' => date('Y-m-d', strtotime($_POST['TGL_PERKIRAAN_SELESAI'])),
            'KETERANGAN'            => NULL
        );
        /*echo "<pre>";
        print_r($dataupdate);
        print_r($datanext);
        print_r($disposisi);
        echo "<pre>";*/

        $this->Mdispo->insertDisposisi($disposisi);
        $this->Mpermohonan->updateriwayat($dataupdate);
        $this->Mpermohonan->insertriwayat($datanext);
        // redirect('disposisi/share/'.$THN_PELAYANAN.$BUNDEL_PELAYANAN.$NO_URUT_PELAYANAN.$KD_PROPINSI_RIWAYAT.$KD_DATI2_RIWAYAT.$KD_KECAMATAN_RIWAYAT.$KD_KELURAHAN_RIWAYAT.$KD_BLOK_RIWAYAT.$NO_URUT_RIWAYAT.$KD_JNS_OP_RIWAYAT);
        redirect('disposisi');
    }


    function prosesselsaidisposisi()
    {
        $KD_KANWIL            = $_POST['KD_KANWIL'];
        $KD_KANTOR            = $_POST['KD_KANTOR'];
        $THN_PELAYANAN        = $_POST['THN_PELAYANAN'];
        $BUNDEL_PELAYANAN     = $_POST['BUNDEL_PELAYANAN'];
        $NO_URUT_PELAYANAN    = $_POST['NO_URUT_PELAYANAN'];
        $KD_PROPINSI_RIWAYAT  = $_POST['KD_PROPINSI_RIWAYAT'];
        $KD_DATI2_RIWAYAT     = $_POST['KD_DATI2_RIWAYAT'];
        $KD_KECAMATAN_RIWAYAT = $_POST['KD_KECAMATAN_RIWAYAT'];
        $KD_KELURAHAN_RIWAYAT = $_POST['KD_KELURAHAN_RIWAYAT'];
        $KD_BLOK_RIWAYAT      = $_POST['KD_BLOK_RIWAYAT'];
        $NO_URUT_RIWAYAT      = $_POST['NO_URUT_RIWAYAT'];
        $KD_JNS_OP_RIWAYAT    = $_POST['KD_JNS_OP_RIWAYAT'];
        $NIP                  = $_POST['NIP'];
        $TANGGAL_AWAL         = $_POST['TANGGAL_AWAL'];
        $KODE_GROUP           = $_POST['KODE_GROUP'];
        $KETERANGAN           = NULL;
        $TANGGAL_AKHIR        = $_POST['TANGGAL_AKHIR'];
        $STATUS_BERKAS        = $_POST['STATUS_BERKAS'];

        $dataupdate = array(
            'KD_KANWIL'            => $KD_KANWIL,
            'KD_KANTOR'            => $KD_KANTOR,
            'THN_PELAYANAN'        => $THN_PELAYANAN,
            'BUNDEL_PELAYANAN'     => $BUNDEL_PELAYANAN,
            'NO_URUT_PELAYANAN'    => $NO_URUT_PELAYANAN,
            'KD_PROPINSI_RIWAYAT'  => $KD_PROPINSI_RIWAYAT,
            'KD_DATI2_RIWAYAT'     => $KD_DATI2_RIWAYAT,
            'KD_KECAMATAN_RIWAYAT' => $KD_KECAMATAN_RIWAYAT,
            'KD_KELURAHAN_RIWAYAT' => $KD_KELURAHAN_RIWAYAT,
            'KD_BLOK_RIWAYAT'      => $KD_BLOK_RIWAYAT,
            'NO_URUT_RIWAYAT'      => $NO_URUT_RIWAYAT,
            'KD_JNS_OP_RIWAYAT'    => $KD_JNS_OP_RIWAYAT,
            'KODE_GROUP'           => $this->session->userdata('pbb_kg'),
            'NIP'                  => $NIP,
            'KETERANGAN'           => $KETERANGAN,
            'TANGGAL_AKHIR'        => $TANGGAL_AKHIR,
            'STATUS_BERKAS'        => $STATUS_BERKAS
        );

        $disposisi = array(
            'KD_KANWIL'         => $KD_KANWIL,
            'KD_KANTOR'         => $KD_KANTOR,
            'THN_PELAYANAN'     => $THN_PELAYANAN,
            'BUNDEL_PELAYANAN'  => $BUNDEL_PELAYANAN,
            'NO_URUT_PELAYANAN' => $NO_URUT_PELAYANAN,
            'KD_PROPINSI'       => $KD_PROPINSI_RIWAYAT,
            'KD_DATI2'          => $KD_DATI2_RIWAYAT,
            'KD_KECAMATAN'      => $KD_KECAMATAN_RIWAYAT,
            'KD_KELURAHAN'      => $KD_KELURAHAN_RIWAYAT,
            'KD_BLOK'           => $KD_BLOK_RIWAYAT,
            'NO_URUT'           => $NO_URUT_RIWAYAT,
            'KD_JNS_OP'         => $KD_JNS_OP_RIWAYAT,
            'TGL_SELESAI'       => $_POST['TGL_SELESAI'],
            'STATUS'            => '1',

        );

        $datanext = array(
            "KD_KANWIL"           => $KD_KANWIL,
            "KD_KANTOR"           => $KD_KANTOR,
            "THN_PELAYANAN"       => $THN_PELAYANAN,
            "BUNDEL_PELAYANAN"    => $BUNDEL_PELAYANAN,
            "NO_URUT_PELAYANAN"   => $NO_URUT_PELAYANAN,
            "KD_PROPINSI_RIWAYAT" => $KD_PROPINSI_RIWAYAT,
            "KD_DATI2_RIWAYAT"    => $KD_DATI2_RIWAYAT,
            "KD_KECAMATAN_RIWAYAT" => $KD_KECAMATAN_RIWAYAT,
            "KD_KELURAHAN_RIWAYAT" => $KD_KELURAHAN_RIWAYAT,
            "KD_BLOK_RIWAYAT"     => $KD_BLOK_RIWAYAT,
            "NO_URUT_RIWAYAT"     => $NO_URUT_RIWAYAT,
            "KD_JNS_OP_RIWAYAT"   => $KD_JNS_OP_RIWAYAT,
            "KODE_GROUP"          => $KODE_GROUP,
            "NIP"                 => null,
            "TANGGAL_AWAL"        => $TANGGAL_AWAL,
            "TANGGAL_AKHIR"       => null,
            "KETERANGAN"          => null,
            "UNIT_SEBEUMNYA" => $this->kg
        );



        $this->Mpermohonan->updateriwayat($dataupdate);
        $this->Mpermohonan->insertriwayat($datanext);
        $this->Mdispo->updatedisposisi($disposisi);
        // redirect('disposisi/share/'.$THN_PELAYANAN.$BUNDEL_PELAYANAN.$NO_URUT_PELAYANAN.$KD_PROPINSI_RIWAYAT.$KD_DATI2_RIWAYAT.$KD_KECAMATAN_RIWAYAT.$KD_KELURAHAN_RIWAYAT.$KD_BLOK_RIWAYAT.$NO_URUT_RIWAYAT.$KD_JNS_OP_RIWAYAT);
        redirect('disposisi');
        /*echo "<pre>";
        print_r($dataupdate);
        print_r($disposisi);
        echo "</pre>";*/
    }
}
