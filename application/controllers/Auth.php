<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Auth extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
	}

	function index()
	{

		if (ENVIRONMENT == 'development') {
			$data['list'] = $this->db->query("SELECT USERNAME,NAMA_PENGGUNA||' ('||NAMA_GROUP||')' AS LIST
												FROM P_PENGGUNA A
												JOIN P_GROUP B ON A.KODE_GROUP=B.KODE_GROUP
												ORDER BY NAMA_GROUP ASC")->result();
		} else {
			$data = [];
		}

		$this->load->view('vlogin', $data);
	}

	function proses()
	{
		$username = strtoupper(trim($this->input->post('username')));
		$password = strtoupper(trim($this->input->post('password')));
		$unit_kantor = $this->input->post('unit_kantor');



		if (ENVIRONMENT == 'development') {
			$sql      = "select KODE_PENGGUNA,NAMA_PENGGUNA,a.KODE_GROUP,NAMA_GROUP,a.KD_SEKSI,NM_SEKSI,NIP
						from P_PENGGUNA a
						join P_GROUP B on a.KODE_GROUP=B.KODE_GROUP
						left join REF_SEKSI c on c.KD_SEKSI=a.KD_SEKSI
						where upper(USERNAME)=? ";
			if ($password == 'PASS') {
				$query    = $this->db->query($sql, array($username))->row();
			}
		} else {
			$sql      = "select KODE_PENGGUNA,NAMA_PENGGUNA,a.KODE_GROUP,NAMA_GROUP,a.KD_SEKSI,NM_SEKSI,NIP
						from P_PENGGUNA a
						join P_GROUP B on a.KODE_GROUP=B.KODE_GROUP
						left join REF_SEKSI c on c.KD_SEKSI=a.KD_SEKSI
						where upper(USERNAME)=? and upper(password)=?";
			$query    = $this->db->query($sql, array($username, $password))->row();
		}

		if (isset($query)) {
			if (count($query) > 0) {
				$data['pbb']         = '1';
				$data['pbb_nama']    = $query->NAMA_PENGGUNA;
				$data['pbb_ku']      = $query->KODE_PENGGUNA;
				$data['pbb_kg']      = $query->KODE_GROUP;
				$data['pbb_ng']      = $query->NAMA_GROUP;
				$data['kd_seksi']    = $query->KD_SEKSI;
				$data['nip']         = $query->NIP;
				$data['seksi']       = $query->NM_SEKSI;
				$data['unit_kantor'] = $unit_kantor;
				$data['jam_login']   = date('Y-m-d H:i:s');
				$data['username']    = $username;
				$this->session->set_userdata($data);

				switch ($query->KODE_GROUP) {
						/*case '1':
							# code...
							redirect('welcome');
							break;*/
					case '3':
						# code...
						redirect('permohonan/create');
						break;
					case '41':
						# code...
						// redirect('disposisi');
						redirect('replikasi/belumverlap');
						break;

					default:
						# PDI I, PDI II, PEL II, Pedanil

						// redirect('permohonan/listdokumen');
						redirect('welcome');
						break;
				}


				//	echo "oke";
			} else {
				$this->session->set_flashdata('notif', '<div class="badge">
														         <p> Username dan password anda salah !</p>
														        </div>');
				redirect('Auth');
			}
		} else {
			$this->session->set_flashdata('notif', '<div class="badge">
														         <p>password anda salah !</p>
														        </div>');
			redirect('Auth');
		}
	}

	function logout()
	{
		$data = array('pbb', 'pbb_nama', 'pbb_ku', 'pbb_kg', 'pbb_ng', 'kd_seksi', 'nip', 'seksi');
		$this->session->unset_userdata($data);
		$this->session->set_flashdata('notif', '<div class="badge">
													         <p> Berhasil logout !</p>
													        </div>');
		redirect('auth');
	}

	function test()
	{
		echo "827ccb0eea8a706c4c34a16891f84e7b";
		echo "<hr>";
		echo md5('PEDANIL');
	}
}
