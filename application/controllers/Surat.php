<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Surat extends CI_Controller
{

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	function __construct()
	{
		parent::__construct();
		if ($this->session->userdata('pbb') <> 1) {
			$this->session->set_flashdata('notif', '<div class="badge">
                    Silahkan login dengan username dan password anda.</p>
                    </div>');
			redirect('auth');
		}

		$this->load->library('datatables');
		$this->load->model('Mpermohonan');
	}

	public function index()
	{
		// $this->template->load('template','blank');
	}


	public function databaru()
	{
		// 
		$data['title']  = "Data SK Pengajuan Baru";
		$data['kode']   = "D";
		$data['var']    = "PENDAFTARAN DATA BARU";
		$data['action'] = base_url() . 'surat/databaru';
		$data['tahun']  = $this->input->get('tahun') <> '' ? $this->input->get('tahun') : date('Y');
		$this->template->load('template', 'surat/datask', $data);
	}

	public function mutasi()
	{
		$data['title']  = "Data SK Mutasi";
		$data['kode']   = "A";
		$data['var']    = "MUTASI OBJEK/SUBJEK";
		$data['action'] = base_url() . 'surat/mutasi';
		$data['tahun']  = $this->input->get('tahun') <> '' ? $this->input->get('tahun') : date('Y');
		$this->template->load('template', 'surat/datask', $data);
	}

	public function pembetulansppt()
	{
		$data['title'] = "Pembetulan SPPT/SKP/STP";
		$data['kode'] = "A";
		$data['var'] = "PEMBETULAN SPPT/SKP/STP";
		$data['action'] = base_url() . 'surat/pembetulansppt';
		$data['tahun']  = $this->input->get('tahun') <> '' ? $this->input->get('tahun') : date('Y');
		$this->template->load('template', 'surat/datask', $data);
	}

	public function pembatalansppt()
	{
		$data['title'] = "PEMBATALAN SPPT/SKP";
		$data['kode'] = "F";
		$data['var'] = "PEMBATALAN SPPT/SKP";
		$data['action'] = base_url() . 'surat/pembatalansppt';
		$data['tahun']  = $this->input->get('tahun') <> '' ? $this->input->get('tahun') : date('Y');
		$this->template->load('template', 'surat/datask', $data);
	}

	public function keberatwp()
	{
		$data['title'] = "Keberatan Penunjukan Wajib Pajak";
		$data['kode'] = "G";
		$data['var'] = "KEBERATAN PENUNJUKAN WAJIB PAJAK";
		$data['action'] = base_url() . 'surat/keberatwp';
		$data['tahun']  = $this->input->get('tahun') <> '' ? $this->input->get('tahun') : date('Y');
		$this->template->load('template', 'surat/datask', $data);
	}


	public function keberatanpajakutang()
	{
		$data['title'] = "Keberatan Atas Pajak Terhutang";
		$data['kode'] = "G";
		$data['var'] = "KEBERATAN ATAS PAJAK TERHUTANG";
		$data['action'] = base_url() . 'surat/keberatanpajakutang';
		$data['tahun']  = $this->input->get('tahun') <> '' ? $this->input->get('tahun') : date('Y');
		$this->template->load('template', 'surat/datask', $data);
	}

	public function penguranganPajakTerutang()
	{
		$data['title'] = "Pengurangan Atas Besarnya Pajak Terhutang";
		$data['kode']  = "E";
		$data['var']   = "PENGURANGAN ATAS BESARNYA PAJAK TERHUTANG";
		$data['action'] = base_url() . 'surat/penguranganPajakTerutang';
		$data['tahun']  = $this->input->get('tahun') <> '' ? $this->input->get('tahun') : date('Y');
		$this->template->load('template', 'surat/datask', $data);
	}

	public function penguranDenda()
	{
		$data['title']  = "Pengurangan Denda Administrasi";
		$data['kode']   = "E";
		$data['var']    = "PENGURANGAN DENDA ADMINISTRASI";
		$data['action'] = base_url() . 'surat/penguranDenda';
		$data['tahun']  = $this->input->get('tahun') <> '' ? $this->input->get('tahun') : date('Y');
		$this->template->load('template', 'surat/datask', $data);
	}

	function jsondatask()
	{
		header('Content-Type: application/json');
		$var = array(
			'kode'  => $this->input->post('kode'),
			'var'   => $this->input->post('var'),
			'tahun' => $this->input->post('tahun')
		);
		echo $this->Mpermohonan->jsonuntuksk($var);
	}

	function generatenomor()
	{
		$res = $this->input->get();
		$id  = $res['id'];
		$var = $res['var'];
		$rr = $this->db->query("SELECT * FROM PST_DETAIL
								where THN_PELAYANAN||BUNDEL_PELAYANAN||NO_URUT_PELAYANAN||KD_PROPINSI_PEMOHON||KD_DATI2_PEMOHON||KD_KECAMATAN_PEMOHON||KD_KELURAHAN_PEMOHON||KD_BLOK_PEMOHON||NO_URUT_PEMOHON||KD_JNS_OP_PEMOHON='$id'")->row();





		if ($rr) {
			$cek = $this->db->query("SELECT * FROM P_PST_DETAIL_SK 
										where THN_PELAYANAN||BUNDEL_PELAYANAN||NO_URUT_PELAYANAN||KD_PROPINSI_PEMOHON||KD_DATI2_PEMOHON||KD_KECAMATAN_PEMOHON||KD_KELURAHAN_PEMOHON||KD_BLOK_PEMOHON||NO_URUT_PEMOHON||KD_JNS_OP_PEMOHON='$id' and jenis_sk='$var'")->row();


			if (empty($cek)) {
				$this->db->trans_start();
				// insert 
				$this->db->query("INSERT INTO P_PST_DETAIL_SK (
								   THN_PELAYANAN, BUNDEL_PELAYANAN, NO_URUT_PELAYANAN, 
								   KD_PROPINSI_PEMOHON, KD_DATI2_PEMOHON, KD_KECAMATAN_PEMOHON, 
								   KD_KELURAHAN_PEMOHON, KD_BLOK_PEMOHON, NO_URUT_PEMOHON, 
								   KD_JNS_OP_PEMOHON, KD_JNS_PELAYANAN, THN_PAJAK_PERMOHONAN,JENIS_SK) 
								  select THN_PELAYANAN, BUNDEL_PELAYANAN, NO_URUT_PELAYANAN, 
								   KD_PROPINSI_PEMOHON, KD_DATI2_PEMOHON, KD_KECAMATAN_PEMOHON, 
								   KD_KELURAHAN_PEMOHON, KD_BLOK_PEMOHON, NO_URUT_PEMOHON, 
								   KD_JNS_OP_PEMOHON, KD_JNS_PELAYANAN, THN_PAJAK_PERMOHONAN,'$var' from pst_detail 
								   where THN_PELAYANAN||BUNDEL_PELAYANAN||NO_URUT_PELAYANAN||KD_PROPINSI_pemohon||KD_DATI2_pemohon||KD_KECAMATAN_pemohon||KD_KELURAHAN_pemohon||KD_BLOK_pemohon||NO_URUT_pemohon||KD_JNS_OP_pemohon='$id'");


				// increment surat
				$tahun = date('Y');

				switch (strtolower($var)) {

					case 'pendaftaran data baru':
						$sx = $this->db->query("SELECT SEQ_SK_BARU.NEXTVAL INC FROM DUAL")->row();
						break;

					case 'pembetulan sppt/skp/stp':
						$sx = $this->db->query("SELECT SEQ_SK_PEMBETULAN_SPPT.NEXTVAL INC FROM DUAL")->row();
						break;

					case 'pembatalan sppt/skp':
						$sx = $this->db->query("SELECT SEQ_SK_PEMBATALAN_SPPT.NEXTVAL INC FROM DUAL")->row();
						break;


					case 'pengurangan denda administrasi':
						$sx = $this->db->query("SELECT SEQ_SK_PENGURANGAN_DENDA.NEXTVAL INC FROM DUAL")->row();
						break;


					case 'mutasi objek/subjek':
						$sx = $this->db->query("SELECT SEQ_SK_MUTASI.NEXTVAL INC FROM DUAL")->row();
						break;


					case 'keberatan atas pajak terhutang':
						$sx = $this->db->query("SELECT SEQ_SK_KEBERATAN_PAJAK.NEXTVAL INC FROM DUAL")->row();
						break;



					case 'pengurangan atas besarnya pajak terhutang':
						$sx = $this->db->query("SELECT SEQ_SK_PENGURANGAN_PAJAK.NEXTVAL INC FROM DUAL")->row();
						break;


					case 'keberatan penunjukan wajib pajak':
						$sx = $this->db->query("SELECT SEQ_SK_KEBERATAN_WP.NEXTVAL INC FROM DUAL")->row();
						break;
					default:
						# code...
						$sx = $this->db->query("SELECT MAX(TO_NUMBER(REGEXP_SUBSTR(NOMOR_SK, '[^/]+', 1, 2))) INC
		                        FROM P_PST_DETAIL_SK
		                        WHERE TO_CHAR(TANGGAL_SK,'yyyy')='$tahun' ")->row();
						break;
				}
				// nomer sk
				$sk = $this->db->query("select * from p_master_sk where jenis='$var'")->row();

				if ($sk) {



					$urut = $sx->INC;
					$nomorsk = str_replace('#', $urut, $sk->NOMOR) . $tahun;
					$this->db->query("UPDATE P_PST_DETAIL_SK
			    					SET NOMOR_SK='" . $nomorsk . "',TANGGAL_SK=SYSDATE,NAMA_PEGAWAI='" . $sk->NAMA_PEGAWAI . "',NIP='" . $sk->NIP . "', JABATAN='" . $sk->JABATAN . "'
			    					WHERE JENIS_SK='" . $var . "' AND THN_PELAYANAN||BUNDEL_PELAYANAN||NO_URUT_PELAYANAN||KD_PROPINSI_PEMOHON||KD_DATI2_PEMOHON||KD_KECAMATAN_PEMOHON||KD_KELURAHAN_PEMOHON||KD_BLOK_PEMOHON||NO_URUT_PEMOHON||KD_JNS_OP_PEMOHON='$id'");
					$notif = "Sukses , Nomor SK : " . $nomorsk;
				} else {
					$notif = 'Belum ada template penomoran SK';
				}

				$this->db->trans_complete();
			} else {
				$tahun = date('Y');

				switch (strtolower($var)) {

					case 'pendaftaran data baru':
						$sx = $this->db->query("SELECT SEQ_SK_BARU.NEXTVAL INC FROM DUAL")->row();
						break;

					case 'pembetulan sppt/skp/stp':
						$sx = $this->db->query("SELECT SEQ_SK_PEMBETULAN_SPPT.NEXTVAL INC FROM DUAL")->row();
						break;

					case 'pembatalan sppt/skp':
						$sx = $this->db->query("SELECT SEQ_SK_PEMBATALAN_SPPT.NEXTVAL INC FROM DUAL")->row();
						break;


					case 'pengurangan denda administrasi':
						$sx = $this->db->query("SELECT SEQ_SK_PENGURANGAN_DENDA.NEXTVAL INC FROM DUAL")->row();
						break;


					case 'mutasi objek/subjek':
						$sx = $this->db->query("SELECT SEQ_SK_MUTASI.NEXTVAL INC FROM DUAL")->row();
						break;


					case 'keberatan atas pajak terhutang':
						$sx = $this->db->query("SELECT SEQ_SK_KEBERATAN_PAJAK.NEXTVAL INC FROM DUAL")->row();
						break;



					case 'pengurangan atas besarnya pajak terhutang':
						$sx = $this->db->query("SELECT SEQ_SK_PENGURANGAN_PAJAK.NEXTVAL INC FROM DUAL")->row();
						break;


					case 'keberatan penunjukan wajib pajak':
						$sx = $this->db->query("SELECT SEQ_SK_KEBERATAN_WP.NEXTVAL INC FROM DUAL")->row();
						break;
					default:
						# code...
						$sx = $this->db->query("SELECT MAX(TO_NUMBER(REGEXP_SUBSTR(NOMOR_SK, '[^/]+', 1, 2))) INC
		                        FROM P_PST_DETAIL_SK
		                        WHERE TO_CHAR(TANGGAL_SK,'yyyy')='$tahun' ")->row();
						break;
				}
				// nomer sk
				$sk = $this->db->query("select * from p_master_sk where jenis='$var'")->row();

				if ($sk) {



					$urut = $sx->INC;
					$nomorsk = str_replace('#', $urut, $sk->NOMOR) . $tahun;
					$this->db->query("UPDATE P_PST_DETAIL_SK
			    					SET NOMOR_SK='" . $nomorsk . "',TANGGAL_SK=SYSDATE,NAMA_PEGAWAI='" . $sk->NAMA_PEGAWAI . "',NIP='" . $sk->NIP . "', JABATAN='" . $sk->JABATAN . "'
			    					WHERE JENIS_SK='" . $var . "' AND THN_PELAYANAN||BUNDEL_PELAYANAN||NO_URUT_PELAYANAN||KD_PROPINSI_PEMOHON||KD_DATI2_PEMOHON||KD_KECAMATAN_PEMOHON||KD_KELURAHAN_PEMOHON||KD_BLOK_PEMOHON||NO_URUT_PEMOHON||KD_JNS_OP_PEMOHON='$id'");
					$notif = "Sukses , Nomor SK : " . $nomorsk;
				} else {
					$notif = 'Belum ada template penomoran SK';
				}
			}
		} else {
			$notif = "Data tidak ada.";
		}
		$alert = "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button> " . $notif . "</div>";
		$this->session->set_flashdata('notif', $alert);
		redirect($_SERVER['HTTP_REFERER']);
	}

	function cetaksk()
	{
		$res = $this->input->get();
		$id  = $res['id'];
		$var = $res['var'];


		$result['rk'] = $this->db->query("SELECT A.*,B.*,C.* ,get_kecamatan(KD_KECAMATAN_PEMOHON) kecop, get_kelurahan(kd_kelurahan_pemohon,kd_kecamatan_pemohon) desaop ,TO_CHAR(TANGGAL_SK,'yyyy-mm-dd') TANGGALSK ,TO_CHAR(TGL_SURAT_PERMOHONAN,'yyyy-mm-dd') TANGGALPERMOHONAN,PBB_YG_HARUS_DIBAYAR_SPPT FROM P_PST_DETAIL_SK A JOIN DAT_OBJEK_PAJAK B ON A.KD_PROPINSI_PEMOHON=B.KD_PROPINSI AND A.KD_DATI2_PEMOHON=B.KD_DATI2 AND A.KD_KECAMATAN_PEMOHON=B.KD_KECAMATAN AND A.KD_KELURAHAN_PEMOHON=B.KD_KELURAHAN AND A.KD_BLOK_PEMOHON=B.KD_BLOK AND A.NO_URUT_PEMOHON=B.NO_URUT AND A.KD_JNS_OP_PEMOHON=B.KD_JNS_OP left  JOIN DAT_SUBJEK_PAJAK C ON C.SUBJEK_PAJAK_ID=B.SUBJEK_PAJAK_ID left JOIN PST_PERMOHONAN D ON A.THN_PELAYANAN = D.THN_PELAYANAN AND A.BUNDEL_PELAYANAN = D.BUNDEL_PELAYANAN AND A.NO_URUT_PELAYANAN = D.NO_URUT_PELAYANAN  left JOIN SPPT E ON A.KD_PROPINSI_PEMOHON=E.KD_PROPINSI  AND A.KD_DATI2_PEMOHON=E.KD_DATI2  AND A.KD_KECAMATAN_PEMOHON=E.KD_KECAMATAN  AND A.KD_KELURAHAN_PEMOHON=E.KD_KELURAHAN  AND A.KD_BLOK_PEMOHON=E.KD_BLOK  AND A.NO_URUT_PEMOHON=E.NO_URUT  AND A.KD_JNS_OP_PEMOHON=E.KD_JNS_OP AND E.THN_PAJAK_SPPT=A.THN_PAJAK_PERMOHONAN  WHERE A.THN_PELAYANAN||A.BUNDEL_PELAYANAN||A.NO_URUT_PELAYANAN||KD_PROPINSI_PEMOHON||KD_DATI2_PEMOHON||KD_KECAMATAN_PEMOHON||KD_KELURAHAN_PEMOHON||KD_BLOK_PEMOHON||NO_URUT_PEMOHON||KD_JNS_OP_PEMOHON='$id'")->row();

		switch ($var) {
			case 'PEMBATALAN SPPT/SKP':
				# code...
				$this->load->view('permohonan/sk/pembatalan', $result);

				break;

			case 'PENDAFTARAN DATA BARU':

				/*echo "<pre>";
						print_r($result);
						echo "</pre>";*/

				$this->load->view('permohonan/sk/databaru', $result);
				break;
			case 'PENGURANGAN ATAS BESARNYA PAJAK TERHUTAN':
				$this->load->view('permohonan/sk/pengurangan', $result);
				break;

			case 'PENGURANGAN ATAS BESARNYA PAJAK TERHUTANG':
				$this->load->view('permohonan/sk/pengurangan', $result);
				break;

			case 'PENGURANGAN DENDA ADMINISTRASI':
				$nn = substr($id, -18);

				$result['tagihan'] = $this->db->query("select thn_pajak_sppt,PBB_YG_HARUS_DIBAYAR_SPPT from sppt 
						where KD_PROPINSI||KD_DATI2||KD_KECAMATAN||KD_KELURAHAN||KD_BLOK||NO_URUT||KD_JNS_OP ='$nn' and  status_pembayaran_sppt=0")->result();
				$this->load->view('permohonan/sk/pengurangandenda', $result);
				break;

			case 'PEMBETULAN SPPT/SKP/STP':
				# code...
				$this->load->view('permohonan/sk/pembetulan', $result);
				break;

			case 'KEBERATAN PENUNJUKAN WAJIB PAJAK':
				$this->load->view('permohonan/sk/keberatan', $result);
				break;
			default:
				# code...
				echo '- Jenis SK: ' . $var . ', belum ada template cetak.';
				break;
		}
	}
}
