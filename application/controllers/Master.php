<?php

if (!defined('BASEPATH'))
  exit('No direct script access allowed');

require_once APPPATH . '/third_party/spout/src/Spout/Autoloader/autoload.php';

use Box\Spout\Writer\WriterFactory;
use Box\Spout\Common\Type;
use Box\Spout\Writer\Style\StyleBuilder;


class Master extends CI_Controller
{
  function __construct()
  {
    parent::__construct();
    if ($this->session->userdata('pbb') <> 1) {
      $this->session->set_flashdata('notif', '<div class="badge">
                    Silahkan login dengan username dan password anda.</p>
                    </div>');
      redirect('auth');
    }
    $this->load->model('Mmaster');
    $this->load->library('form_validation');
    $this->load->library('datatables');
  }

  function wajibpajak()
  {
    $this->template->load('template', 'master/wajibpajak');
  }

  public function wpjson()
  {
    memory_get_usage();
    header('Content-Type: application/json');
    echo $this->Mmaster->wpjson();
  }

  function objekpajak()
  {
    $this->template->load('template', 'master/objekpajak');
  }

  function kecamatan()
  {
    $this->template->load('template', 'master/kecamatan');
  }


  function kelurahan()
  {
    $this->template->load('template', 'master/kelurahan');
  }

  function mspelayanan()
  {
    $data['data'] = $this->db->query("SELECT * FROM REF_JNS_PELAYANAN ORDER BY TO_NUMBER(KD_JNS_PELAYANAN) ASC")->result();
    $this->template->load('template', 'master/mspelayanan', $data);
  }

  function blokirNop()
  {
    $data['data'] = $this->db->query("select a.*,to_char(date_insert,'dd-mm-YYYY HH:ii:ss') tgl_masuk from sppt_blokir@to17 a order by date_insert desc")->result();
    $this->template->load('template', 'master/nopBlokir', $data);
  }

  function bukablokir()
  {
    $THN_PAJAK_SPPT = $this->input->get('THN_PAJAK_SPPT');
    $KD_KECAMATAN   = $this->input->get('KD_KECAMATAN');
    $KD_KELURAHAN   = $this->input->get('KD_KELURAHAN');
    $KD_BLOK        = $this->input->get('KD_BLOK');
    $NO_URUT        = $this->input->get('NO_URUT');
    $KD_JNS_OP      = $this->input->get('KD_JNS_OP');


    $this->db->query("call blokir_buka_nop@to17($THN_PAJAK_SPPT,'$KD_KECAMATAN','$KD_KELURAHAN','$KD_BLOK','$NO_URUT','$KD_JNS_OP')");

    redirect('master/blokirNop');
  }

  function bukablokirmasal()
  {
    echo "<pre>";
    // print_r($this->input->post());
    if (count($this->input->post('nop')) > 0) {
      $this->db->trans_start();
      for ($i = 0; $i < count($this->input->post('nop')); $i++) {
        $nop = $this->input->post('nop')[$i];
        $exp = explode('.', $nop);
        $this->db->query("call blokir_buka_nop@to17('" . $exp[7] . "','" . $exp[2] . "','" . $exp[3] . "','" . $exp[4] . "','" . $exp[5] . "','" . $exp[6] . "')");
      }
      $this->db->trans_complete();
    }

    redirect('master/blokirNop');
  }

  function delblokir()
  {

    $THN_PAJAK_SPPT = $this->input->get('THN_PAJAK_SPPT');
    $KD_KECAMATAN   = $this->input->get('KD_KECAMATAN');
    $KD_KELURAHAN   = $this->input->get('KD_KELURAHAN');
    $KD_BLOK        = $this->input->get('KD_BLOK');
    $NO_URUT        = $this->input->get('NO_URUT');
    $KD_JNS_OP      = $this->input->get('KD_JNS_OP');


    $this->db->query("call blokir_nop_delete@to17($THN_PAJAK_SPPT,'$KD_KECAMATAN','$KD_KELURAHAN','$KD_BLOK','$NO_URUT','$KD_JNS_OP')");

    redirect('master/blokirNop');
  }

  function blokirnopsppt()
  {
    $nop = $this->input->post('NOP', true);
    $ss = explode('.', $nop);
    $cc = count($ss);
    $tahun = $this->input->post('TAHUN');

    if ($cc == 7) {

      $this->db->query("call blokir_nop@to17($tahun,'" . $ss[2] . "','" . $ss[3] . "','" . $ss[4] . "','" . $ss[5] . "','" . $ss[6] . "')");
      /*echo "<pre>";
      print_r($ss);*/
    }

    redirect('master/blokirNop');
  }

  function nomorsk()
  {
    $data['data'] = $this->db->query("SELECT * FROM P_MASTER_SK")->result();
    $this->template->load('template', 'master/nomorsk', $data);
  }

  function updatenomorsk()
  {
    $get = $this->input->get('p');
    $vv = urldecode(urldecode(urldecode($get)));
    $res = $this->db->query("SELECT * FROM P_MASTER_SK WHERE JENIS='$vv'")->row();
    if ($res) {
      $data = array(
        'JENIS' => set_value('JENIS', $res->JENIS),
        'NOMOR' => set_value('NOMOR', $res->NOMOR),
        'NAMA_PEGAWAI' => set_value('NAMA_PEGAWAI', $res->NAMA_PEGAWAI),
        'JABATAN' => set_value('JABATAN', $res->JABATAN),
        'NIP' => set_value('NIP', $res->NIP),
      );

      $this->template->load('template', 'master/formsk', $data);
    }
  }




  function prosesupdatesk()
  {
    $JENIS = $this->input->post('JENIS');
    $NOMOR = $this->input->post('NOMOR');
    $NAMA_PEGAWAI = $this->input->post('NAMA_PEGAWAI');
    $JABATAN = $this->input->post('JABATAN');
    $NIP = $this->input->post('NIP');


    $this->db->where('JENIS', $JENIS);
    $this->db->set('NOMOR', $NOMOR);
    $this->db->set('NAMA_PEGAWAI', $NAMA_PEGAWAI);
    $this->db->set('JABATAN', $JABATAN);
    $this->db->set('NIP', $NIP);
    $as = $this->db->update('P_MASTER_SK');
    if ($as) {
      redirect('master/nomorsk');
    } else {
      redirect('master/updatenomorsk?p=' . urlencode(urlencode(urlencode($JENIS))));
    }


    /*$res=$this->input->post();
    echo "<pre>";
    print_r($res);
    echo "</pre>";*/
  }


  function prosesimportNopblokir()
  {
    error_reporting(E_ALL ^ E_NOTICE);

    require_once 'excel_reader2.php';
    $data = new Spreadsheet_Excel_Reader($_FILES['nop_blokir']['tmp_name']);

    $baris = $data->rowcount();


    $win = '';

    for ($i = 2; $i <= $baris; $i++) {
      // $wer="'".$data->val($i,8).$data->val($i,1).$data->val($i,2).$data->val($i,3).$data->val($i,4).$data->val($i,5).$data->val($i,6).$data->val($i,7)."'";
      $win .= " select kd_propinsi, kd_dati2, kd_kecamatan, kd_kelurahan, kd_blok, no_urut, kd_jns_op,kd_propinsi||'.'||kd_dati2||'.'||kd_kecamatan||'.'||kd_kelurahan||'.'||kd_blok||'.'||no_urut||'.'||kd_jns_op nop , nm_wp_sppt,thn_pajak_sppt
                            from sppt 
                            where  thn_pajak_sppt='" . $data->val($i, 8) . "'
                            and kd_propinsi='" . $data->val($i, 1) . "'
                            and kd_dati2='" . $data->val($i, 2) . "'
                            and kd_kecamatan ='" . $data->val($i, 3) . "'
                            and kd_kelurahan='" . $data->val($i, 4) . "'
                            and kd_blok='" . $data->val($i, 5) . "'
                            and no_urut='" . $data->val($i, 6) . "'
                            and kd_jns_op ='" . $data->val($i, 7) . "'
                             union";
    }

    $vwin = substr($win, 0, -5);
    // echo $vwin;

    // die();
    /*$dd=$this->db->query("select kd_propinsi, kd_dati2, kd_kecamatan, kd_kelurahan, kd_blok, no_urut, kd_jns_op,kd_propinsi||'.'||kd_dati2||'.'||kd_kecamatan||'.'||kd_kelurahan||'.'||kd_blok||'.'||no_urut||'.'||kd_jns_op nop , nm_wp_sppt,thn_pajak_sppt
                            from sppt 
                            where  thn_pajak_sppt||kd_propinsi||kd_dati2||kd_kecamatan||kd_kelurahan||kd_blok||no_urut||kd_jns_op in ($vwin)")->result();*/
    $dd = $this->db->query($vwin)->result();

    $array = array(
      'data' => $dd,
      'count' => $baris
    );
    $this->template->load('template', 'master/previewnopBlokir', $array);
  }

  function submitblokir()
  {


    // echo "<pre>";
    // print_r($this->input->post());
    //     die();
    $KD_KECAMATAN   = $this->input->post('KD_KECAMATAN');
    $KD_KELURAHAN   = $this->input->post('KD_KELURAHAN');
    $KD_BLOK        = $this->input->post('KD_BLOK');
    $NO_URUT        = $this->input->post('NO_URUT');
    $KD_JNS_OP      = $this->input->post('KD_JNS_OP');
    $THN_PAJAK_SPPT = $this->input->post('THN_PAJAK_SPPT');

    $baris = $this->input->post('count');
    $this->db->trans_start();
    $jum = 0;
    for ($i = 0; $i < count($KD_KECAMATAN); $i++) {


      $rr = $this->db->query("call blokir_nop@to17(" . $THN_PAJAK_SPPT[$i] . ",'" . $KD_KECAMATAN[$i] . "','" . $KD_KELURAHAN[$i] . "','" . $KD_BLOK[$i] . "','" . $NO_URUT[$i] . "','" . $KD_JNS_OP[$i] . "')");
      if ($rr) {
        $jum += 1;
      }
    }
    $this->db->trans_complete();

    echo "<script>alert('data berhasil di proses :'" . $jum . ");</script>";
    redirect('master/blokirNop');
  }
}

/* End of file Group.php */
/* Location: ./application/controllers/Group.php */
/* Generated by Mohamad Wahyu Dewantoro 2017-04-28 01:25:45 */
