<?php

defined('BASEPATH') or exit('No direct script access allowed');

class Welcome extends CI_Controller
{
 
	function __construct()
	{
		parent::__construct();
		if ($this->session->userdata('pbb') <> 1) {
			$this->session->set_flashdata('notif', '<div class="badge">
                    Silahkan login dengan username dan password anda.</p>
                    </div>');
			redirect('auth');
		}

		$this->group = $this->session->userdata('pbb_kg');
	}

	public function index()
	{
		// $this->load->view('template');
		// $this->template->load('template','blank');

		if ($this->group == 1) {
			$data = array(
				'countPengguna' => $this->countPengguna(),
				'countPermohonan' => $this->countPermohonan()
			);
			$this->template->load('template', 'blankdashboard', $data);
		} else {
			$kg = $this->db->query("select nama_group from p_group where kode_group='" . $this->group . "'")->row();
			$dok = $this->db->query(" select  count(1) jum
                 from PV_DOKUMEN a
                 where  kode_group='" . $this->group . "' and  tahun=to_number(to_char(sysdate,'yyyy'))")->row();
			$rekap = $this->db->query("select nama_group ,count(1) dok
										 from pv_dokumen a
										 join p_group b on a.kode_group=b.kode_group
										 where status!='Selesai' and  tahun=to_number(to_char(sysdate,'yyyy'))
										 group by nama_group
										 order by nama_group desc")->result();
			$data = array(
				'nama_role' => $kg->NAMA_GROUP,
				'jumDok'    => $dok->JUM,
				'rekap'     => $rekap
			);

			$this->template->load('template', 'blank', $data);
		}
	}

	function ambiltodo()
	{
		$thn = $this->input->get('tahun');
		$dok = $this->db->query(" select  count(1) jum
                 from PV_DOKUMEN a
                 where  kode_group='" . $this->group . "' and  tahun=$thn")->row()->JUM;
		echo $dok . " Berkas";
	}

	function ambilrekap()
	{
		$thn = $this->input->get('tahun');
		$rekap = $this->db->query("select nama_group ,count(1) dok
										 from pv_dokumen a
										 join p_group b on a.kode_group=b.kode_group
										 where status!='Selesai' and  tahun='$thn'
										 group by nama_group
										 order by nama_group desc")->result();
		$html = '';

		foreach ($rekap as $rekap) {
			$html .= '<tr>';
			$html .= ' <td><a href="#">' . $rekap->NAMA_GROUP . '</a></td>';
			$html .= '<td align="center"><div class="label ';
			if ($rekap->DOK <= 50) {
				$html .= ' label-success';
			} else if ($rekap->DOK > 50 && $rekap->DOK <= 100) {
				$html .= ' label-warning';
			} else {
				$html .= ' label-danger';
			}

			$html .= ' ">' . $rekap->DOK . ' Berkas</div></td>
                      </tr>';
		}

		echo $html;
	}


	function countPengguna()
	{
		$res = $this->db->query("select count(1) jum from p_pengguna")->row();
		return $res->JUM;
	}

	function countPermohonan()
	{
		$res = $this->db->query("select  count(1) jum from pst_permohonan where to_char(tgl_terima_dokumen_wp,'yyyymm')=to_char(sysdate,'yyyymm')")->row();
		return $res->JUM;
	}

	function resetpass()
	{
		$p_pengguna = $this->session->userdata('pbb_ku');
		$cek = $this->db->query("select * from p_pengguna where kode_pengguna=$p_pengguna")->row();
		if ($cek) {
			// echo "string";
			$password_lama = $this->input->post("password_lama");
			$password_baru = $this->input->post("password_baru");

			if (strtolower($cek->PASSWORD) == strtolower($password_lama)) {

				$this->db->set('PASSWORD', $password_baru);
				$this->db->where('KODE_PENGGUNA', $p_pengguna);
				$this->db->update('P_PENGGUNA');
				$msg = "berhasil ganti password";
			} else {
				// redirect('welcome');	
				$msg = "password lama tidak sesuai";
			}
			// echo $msg;

			// redirect('welcome');
			// echo "<script type='text/javascript'>alert('$msg');</script>";

			$url = base_url();
			// header("Location:".$url);
			echo ("<script LANGUAGE='JavaScript'>
					    window.alert('$msg');
					    window.location.href='$url';
					    </script>");
		} else {
			redirect('welcome');
		}
	}
}
