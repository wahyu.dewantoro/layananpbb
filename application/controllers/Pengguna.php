<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Pengguna extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        if ($this->session->userdata('pbb') <> 1) {
            $this->session->set_flashdata('notif', '<div class="badge">
                    Silahkan login dengan username dan password anda.</p>
                    </div>');
            redirect('auth');
        }
        $this->load->model('Mpengguna');
        $this->load->library('form_validation');
        $this->load->library('datatables');
    }

    public function index()
    {
        $this->template->load('template', 'pengguna/pengguna_list');
    }

    public function json()
    {
        header('Content-Type: application/json');
        echo $this->Mpengguna->json();
    }



    public function create()
    {
        $data = array(
            'button'        => 'Tambah Pengguna',
            'action'        => site_url('pengguna/create_action'),
            'KODE_PENGGUNA' => set_value('KODE_PENGGUNA'),
            'USERNAME'      => set_value('USERNAME'),
            'PASSWORD'      => set_value('PASSWORD'),
            'NAMA_PENGGUNA' => set_value('NAMA_PENGGUNA'),
            'KODE_GROUP'    => set_value('KODE_GROUP'),
            'NIP'           => set_value('NIP'),
            'KD_SEKSI'           => set_value('KD_SEKSI'),
            'group'         => $this->db->query("SELECT KODE_GROUP, NAMA_GROUP FROM P_GROUP")->result(),
            'seksi' => $this->db->query("select KD_SEKSI,NM_SEKSI from REF_SEKSI")->result()
        );
        $this->template->load('template', 'pengguna/pengguna_form', $data);
    }

    public function create_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
                'USERNAME'      => $this->input->post('USERNAME', TRUE),
                'PASSWORD'      => $this->input->post('PASSWORD', TRUE),
                'NAMA_PENGGUNA' => $this->input->post('NAMA_PENGGUNA', TRUE),
                'KODE_GROUP'    => $this->input->post('KODE_GROUP', TRUE),
                'NIP'           => $this->input->post('NIP', TRUE),
                'KD_SEKSI'           => $this->input->post('KD_SEKSI', TRUE),
            );

            $this->Mpengguna->insert($data);
            $this->db->query("commit");
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('pengguna'));
        }
    }

    public function update($id)
    {
        $row = $this->Mpengguna->get_by_id($id);

        if ($row) {
            $data = array(
                'button'        => 'Update Pengguna',
                'action'        => site_url('pengguna/update_action'),
                'KODE_PENGGUNA' => set_value('KODE_PENGGUNA', $row->KODE_PENGGUNA),
                'USERNAME'      => set_value('USERNAME', $row->USERNAME),

                'NAMA_PENGGUNA' => set_value('NAMA_PENGGUNA', $row->NAMA_PENGGUNA),
                'KODE_GROUP'    => set_value('KODE_GROUP', $row->KODE_GROUP),
                'NIP'           => set_value('NIP', $row->NIP),
                'KD_SEKSI'      => set_value('KD_SEKSI', $row->KD_SEKSI),
                'group'         => $this->db->query("SELECT KODE_GROUP, NAMA_GROUP FROM P_GROUP")->result(),
                'seksi'         => $this->db->query("select KD_SEKSI,NM_SEKSI from REF_SEKSI")->result()
            );
            $this->template->load('template', 'pengguna/pengguna_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('pengguna'));
        }
    }

    public function update_action()
    {
        $this->_rulesas();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('KODE_PENGGUNA', TRUE));
        } else {
            $data = array(
                'USERNAME'      => $this->input->post('USERNAME', TRUE),
                'NAMA_PENGGUNA' => $this->input->post('NAMA_PENGGUNA', TRUE),
                'KODE_GROUP'    => $this->input->post('KODE_GROUP', TRUE),
                'NIP'           => $this->input->post('NIP', TRUE),
                'KD_SEKSI'      => $this->input->post('KD_SEKSI', TRUE),
            );

            $this->Mpengguna->update($this->input->post('KODE_PENGGUNA', TRUE), $data);
            $this->db->query("commit");
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('pengguna'));
        }
    }

    public function delete($id)
    {
        $row = $this->Mpengguna->get_by_id($id);

        if ($row) {
            $this->Mpengguna->delete($id);
            $this->db->query("commit");
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('pengguna'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('pengguna'));
        }
    }

    public function _rules()
    {
        $this->form_validation->set_rules('USERNAME', 'username', 'trim|required');
        $this->form_validation->set_rules('PASSWORD', 'password', 'trim|required');
        $this->form_validation->set_rules('NAMA_PENGGUNA', 'nama pengguna', 'trim|required');
        $this->form_validation->set_rules('KODE_GROUP', 'group', 'trim|required');
        $this->form_validation->set_rules('NIP', 'nip', 'trim|required');
        $this->form_validation->set_rules('KD_SEKSI', 'Seksi', 'trim|required');
        $this->form_validation->set_rules('KODE_PENGGUNA', 'KODE_PENGGUNA', 'trim');
        $this->form_validation->set_error_delimiters('<span class="label label-danger ">', '</span>');
    }

    function _rulesas()
    {
        $this->form_validation->set_rules('USERNAME', 'username', 'trim|required');
        $this->form_validation->set_rules('NAMA_PENGGUNA', 'nama pengguna', 'trim|required');
        $this->form_validation->set_rules('KODE_GROUP', 'group', 'trim|required');
        $this->form_validation->set_rules('NIP', 'nip', 'trim|required');
        $this->form_validation->set_rules('KD_SEKSI', 'Seksi', 'trim|required');
        $this->form_validation->set_rules('KODE_PENGGUNA', 'KODE_PENGGUNA', 'trim');
        $this->form_validation->set_error_delimiters('<span class ="label label-danger ">', '</span>');
    }

    function resetPassword($id)
    {

        $this->db->set('PASSWORD', 'pass');
        $this->db->where('KODE_PENGGUNA', $id);
        $this->db->update('P_PENGGUNA');
        $msg="Password telah di update";
        $url = base_url() . 'pengguna';
        // header("Location:".$url);
        echo ("<script LANGUAGE='JavaScript'>
                        window.alert('$msg');
                        window.location.href='$url';
                        </script>");
    }
}

/* End of file Pengguna.php */
/* Location: ./application/controllers/Pengguna.php */
/* Generated by Mohamad Wahyu Dewantoro 2017-04-29 07:13:04 */
