<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

require_once APPPATH . '/third_party/spout/src/Spout/Autoloader/autoload.php';

use Box\Spout\Writer\WriterFactory;
use Box\Spout\Common\Type;
use Box\Spout\Writer\Style\StyleBuilder;

class Data extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        if ($this->session->userdata('pbb') <> 1) {
            $this->session->set_flashdata('notif', '<div class="badge">
                    Silahkan login dengan username dan password anda.</p>
                    </div>');
            redirect('auth');
        }
        $this->load->model('Mmaster');
        $this->load->library('form_validation');
        $this->load->library('datatables');
    }

    function getkelurahan()
    {
        $KD_KECAMATAN = $this->input->post('kd_kec');
        $res = $this->db->query("select kd_kelurahan,nm_kelurahan 
                                from ref_kelurahan
                                where kd_kecamatan='$KD_KECAMATAN' order by kd_kelurahan asc")->result();
        $option = "<option value=''>Pilih</option>";
        $option .= "<option value=''>000 Semua Kelurahan</option>";
        foreach ($res as $res) {
            $option .= "<option value='" . $res->KD_KELURAHAN . "'>" . $res->KD_KELURAHAN . ' ' . $res->NM_KELURAHAN . "</option>";
        }

        echo $option;
    }


    function dhr()
    {
        if (isset($_POST['tahun'])) {
            $tahun       = $_POST['tahun'];
        } else {
            $tahun = date('Y');
        }



        $kec = $this->db->query("SELECT KD_KECAMATAN,NM_KECAMATAN FROM REF_KECAMATAN ORDER BY KD_KECAMATAN ASC")->result();
        $KD_KECAMATAN = $this->input->post('KD_KECAMATAN', true);
        $KD_KELURAHAN = $this->input->post('KD_KELURAHAN', true);
        $kode_cari = $this->input->post('cari', true);

        $where = "";
        if ($KD_KECAMATAN <> '') {
            $where .= " AND KD_KECAMATAN='" . $KD_KECAMATAN . "'";
        }

        if ($KD_KELURAHAN <> '') {
            $where .= " AND KD_KELURAHAN='" . $KD_KELURAHAN . "'";
        }

        $jns1 = "";
        $jns2 = "";
        $jns3 = "";
        $jns4 = "";
        $jns5 = "";
        if (count($_POST['jns_bumi']) == 0) { } else {
            $w_jns = "";
            for ($i = 0; $i < count($_POST['jns_bumi']); $i++) {
                $w_jns .= "'" . $_POST['jns_bumi'][$i] . "',";
                if ($_POST['jns_bumi'][$i] == '1') {
                    $jns1 =  $_POST['jns_bumi'][$i];
                } else if ($_POST['jns_bumi'][$i] == '2') {
                    $jns2 =  $_POST['jns_bumi'][$i];
                } else if ($_POST['jns_bumi'][$i] == '3') {
                    $jns3 =  $_POST['jns_bumi'][$i];
                } else if ($_POST['jns_bumi'][$i] == '4') {
                    $jns4 =  $_POST['jns_bumi'][$i];
                } else {
                    $jns5 =  $_POST['jns_bumi'][$i];
                }
            }

            $r_jns = rtrim($w_jns, ',');
            $where .= " AND JNS_BUMI IN(" . $r_jns . ")";
        }

        if ($KD_KECAMATAN <> '') {
            $kel = $this->db->query("select kd_kelurahan,nm_kelurahan 
                                    from ref_kelurahan
                                    where kd_kecamatan='$KD_KECAMATAN' order by kd_kelurahan asc")->result();
        } else {
            $kel = null;
        }

        if (isset($kode_cari)) {
            $query = $this->db->query("SELECT * FROM MV_DHR where THN_PAJAK_SPPT = '$tahun' $where  order by KD_PROPINSI ASC, KD_DATI2 ASC, KD_KECAMATAN ASC, KD_KELURAHAN ASC, KD_BLOK ASC, NO_URUT ASC, KD_JNS_OP ASC ")->result();
            $data = array(
                'rk'           => $query,
                'tahun'        => $tahun,
                'kec'          => $kec,
                'kel'          => $kel,
                'KD_KELURAHAN' => $KD_KELURAHAN,
                'KD_KECAMATAN' => $KD_KECAMATAN,
                'jns1'         => $jns1,
                'jns2'         => $jns2,
                'jns3'         => $jns3,
                'jns4'         => $jns4,
                'jns5'         => $jns5,
            );
        } else {

            $data = array(
                'tahun'        => $tahun,
                'kec'          => $kec,
                'kel'          => $kel,
                'KD_KELURAHAN' => $KD_KELURAHAN,
                'KD_KECAMATAN' => $KD_KECAMATAN
            );
        }


        $this->template->load('template', 'datahasilrekaman/dhr', $data);
    }

    function dhrLengkap()
    {
        if (isset($_POST['tahun'])) {
            $tahun       = $_POST['tahun'];
        } else {
            $tahun = date('Y');
        }


        $kec = $this->db->query("SELECT KD_KECAMATAN,NM_KECAMATAN FROM REF_KECAMATAN ORDER BY KD_KECAMATAN ASC")->result();
        $KD_KECAMATAN = $this->input->post('KD_KECAMATAN', true);
        $KD_KELURAHAN = $this->input->post('KD_KELURAHAN', true);
        $kode_cari = $this->input->post('cari', true);

        $where = "";
        if ($KD_KECAMATAN <> '') {
            $where .= " AND KD_KECAMATAN='" . $KD_KECAMATAN . "'";
        }

        if ($KD_KELURAHAN <> '') {
            $where .= " AND KD_KELURAHAN='" . $KD_KELURAHAN . "'";
        }

        if (count($_POST['jns_bumi']) == 0) { } else {
            for ($i = 0; $i < count($_POST['jns_bumi']); $i++) {
                $where .= " AND JNS_BUMI='" . $_POST['jns_bumi'][$i] . "'";
            }
        }

        if ($KD_KECAMATAN <> '') {
            $kel = $this->db->query("select kd_kelurahan,nm_kelurahan 
                                    from ref_kelurahan
                                    where kd_kecamatan='$KD_KECAMATAN' order by kd_kelurahan asc")->result();
        } else {
            $kel = null;
        }

        if (isset($kode_cari)) {
            $query = $this->db->query("SELECT * FROM MV_DHR where THN_PAJAK_SPPT = '$tahun' $where order by KD_PROPINSI ASC, KD_DATI2 ASC, KD_KECAMATAN ASC, KD_KELURAHAN ASC, KD_BLOK ASC, NO_URUT ASC, KD_JNS_OP ASC")->result();
            $data = array(
                'rk'           => $query,
                'tahun'        => $tahun,
                'kec'          => $kec,
                'kel'          => $kel,
                'KD_KELURAHAN' => $KD_KELURAHAN,
                'KD_KECAMATAN' => $KD_KECAMATAN
            );
        } else {

            $data = array(
                'tahun'        => $tahun,
                'kec'          => $kec,
                'kel'          => $kel,
                'KD_KELURAHAN' => $KD_KELURAHAN,
                'KD_KECAMATAN' => $KD_KECAMATAN
            );
        }


        $this->template->load('template', 'datahasilrekaman/dhrlengkap', $data);
    }

    function dhrexcel()
    {
        ini_set('max_execution_time', 300);
        $KD_KECAMATAN = urldecode($this->input->get('kec', true));
        $KD_KELURAHAN = urldecode($this->input->get('kel', true));
        $tahun = urldecode($this->input->get('tahun'));

        $where = "";
        if ($KD_KECAMATAN <> '') {
            $where .= " AND a.kd_kecamatan='" . $KD_KECAMATAN . "'";
        }
        if ($KD_KELURAHAN <> '') {
            $where .= " AND a.kd_kelurahan='" . $KD_KELURAHAN . "'";
        }

        $kecamatan = $this->db->query("select  distinct kode_kec, nm_kecamatan, kode_kel,nm_kelurahan from (
                                    SELECT a.kd_kecamatan as kode_kec, b.nm_kecamatan,a.kd_kelurahan as kode_kel,c.kd_kelurahan, c.nm_kelurahan
                                        FROM MV_DHR a
                                    LEFT JOIN  REF_KECAMATAN b
                                    ON a.kd_kecamatan = b.kd_kecamatan
                                    JOIN REF_KELURAHAN c
                                    ON a.kd_kecamatan = c.kd_kecamatan
                                    AND a.kd_kelurahan = c.kd_kelurahan
                                    where THN_PAJAK_SPPT = '$tahun' $where
                                    
                                ) ORDER BY nm_kelurahan asc")->result();

        $header = ['NO', 'NOP', 'FORMULIR SPOP', 'JUMLAH BNG', 'ALAMAT OP', 'RT/RW', 'NAMA WP', 'LUAS BUMI', 'ZNT', 'NILAI BUMI', 'KTP', 'NPWP', 'STATUS WP', 'PEKERJAAN WP', 'PERSIL'];

        $no = 1;
        foreach ($kecamatan as $key) {
            echo $key->KODE_KEC . '/' . $key->KODE_KEL . ' : ' . $key->NM_KECAMATAN . '/' . $key->NM_KELURAHAN;
            echo '<br><hr>';
            $no++;
        }

        //                 echo 'JUMLAH DATA : '.$no;
        // exit();
        //setup Spout Excel Writer, set tipenya xlsx
        $writer = WriterFactory::create(Type::XLSX);
        // download to browser
        $writer->openToBrowser('datahasilrekaman' . date('YmdHis') . '.xlsx');
        // set style untuk header
        $headerStyle = (new StyleBuilder())
            ->setFontBold()
            ->build();

        $first = 1;
        foreach ($kecamatan as $res) {

            $sql = $this->db->query("SELECT * FROM MV_DHR where THN_PAJAK_SPPT = '$tahun' AND KD_KECAMATAN=" . $res->KODE_KEC . " AND KD_KELURAHAN =" . $res->KODE_KEL . " order by KD_PROPINSI ASC, KD_DATI2 ASC, KD_KECAMATAN ASC, KD_KELURAHAN ASC, KD_BLOK ASC, NO_URUT ASC, KD_JNS_OP ASC")->result();

            $arrisi = array();
            $no = 1;
            foreach ($sql as $rk) {
                $nop = $rk->KD_PROPINSI . '.' . $rk->KD_DATI2 . '.' . $rk->KD_KECAMATAN . '.' . $rk->KD_KELURAHAN . '.' . $rk->KD_BLOK . '.' . $rk->NO_URUT . '.' . $rk->KD_JNS_OP;
                $rt_rw = $rk->RT_OP . '/' . $rk->RW_OP;
                $ktp = "'" . $rk->KD_PROPINSI . $rk->KD_DATI2 . $rk->KD_KECAMATAN . $rk->KD_KELURAHAN . $rk->KD_BLOK . $rk->NO_URUT . $rk->KD_JNS_OP;
                $sts_wp = $rk->KD_STATUS_WP . ' - PEMILIK';
                $alamat = $rk->ALAMAT_OP . ', ' . $rk->BLOK_KAV_NO_OP;

                $cc  = $no . '+' . $nop . '+' . $rk->NO_FORMULIR_SPOP . '+' . '' . '+' . $alamat . '+' . $rt_rw . '+' . $rk->NM_WP_SPPT . '+' . $rk->LUAS_BUMI . '+' . $rk->KD_ZNT . '+' . $rk->NILAI_SISTEM_BUMI . '+' . $ktp . '+' . '' . '+' . $sts_wp . '+' . '' . '+' . $rk->NO_PERSIL;

                $ff  = explode('+', $cc);
                array_push($arrisi, $ff);
                $no++;

                // echo json_encode(array('result' => $arrisi ));
                //echo $cc."<br><hr>";
            }

            if ($first == 1) {
                // write ke Sheet pertama
                $nama = $res->NM_KELURAHAN . ' - Kec.' . $res->NM_KECAMATAN;
                $writer->getCurrentSheet()->setName($nama);
                // header Sheet pertama
                $writer->addRowWithStyle($header, $headerStyle);
                // data Sheet pertama
                $writer->addRows($arrisi);
            } else {
                $nama = $res->NM_KELURAHAN . ' - Kec.' . $res->NM_KECAMATAN;
                // write ke Sheet kedua
                $writer->addNewSheetAndMakeItCurrent()->setName($nama);
                // header Sheet kedua
                $writer->addRowWithStyle($header, $headerStyle);
                // data Sheet pertama
                $writer->addRows($arrisi);
            }
            $first++;
        }
        $writer->close();
    }

    function dhrexcelLengkap()
    {
        ini_set('max_execution_time', 300);
        $KD_KECAMATAN = urldecode($this->input->get('kec', true));
        $KD_KELURAHAN = urldecode($this->input->get('kel', true));
        $tahun = urldecode($this->input->get('tahun'));

        $where = "";
        if ($KD_KECAMATAN <> '') {
            $where .= " AND a.kd_kecamatan='" . $KD_KECAMATAN . "'";
        }
        if ($KD_KELURAHAN <> '') {
            $where .= " AND a.kd_kelurahan='" . $KD_KELURAHAN . "'";
        }

        $kecamatan = $this->db->query("select  distinct kode_kec, nm_kecamatan, kode_kel,nm_kelurahan from (
                                    SELECT a.kd_kecamatan as kode_kec, b.nm_kecamatan,a.kd_kelurahan as kode_kel,c.kd_kelurahan, c.nm_kelurahan
                                        FROM MV_DHR a
                                    LEFT JOIN  REF_KECAMATAN b
                                    ON a.kd_kecamatan = b.kd_kecamatan
                                    JOIN REF_KELURAHAN c
                                    ON a.kd_kecamatan = c.kd_kecamatan
                                    AND a.kd_kelurahan = c.kd_kelurahan
                                    where THN_PAJAK_SPPT = '$tahun' $where
                                    
                                ) ORDER BY nm_kelurahan asc")->result();

        $header = ['NO', 'NOP', 'FORMULIR SPOP', 'JUMLAH BNG', 'ALAMAT OP', 'RT/RW', 'NAMA WP', 'LUAS BUMI', 'ZNT', 'NILAI BUMI', 'KTP', 'NPWP', 'STATUS WP', 'PEKERJAAN WP', 'PERSIL', 'FORMULIR LSPOP', 'NO. BANGUNAN', 'LUAS BANGUNAN', 'JUMLAH LANTAI', 'TAHUN DIBANGUN', 'TAHUN RENOVASI', 'KONDISI UMUM BANGUNAN', 'JENIS KONSTRUKSI', 'JENIS ATAP', 'JENIS DINDING', 'JENIS LANTAI', 'JENIS LANGIT-LANGIT'];

        $no = 1;
        foreach ($kecamatan as $key) {
            echo $key->KODE_KEC . '/' . $key->KODE_KEL . ' : ' . $key->NM_KECAMATAN . '/' . $key->NM_KELURAHAN;
            echo '<br><hr>';
            $no++;
        }

        //                 echo 'JUMLAH DATA : '.$no;
        // exit();
        //setup Spout Excel Writer, set tipenya xlsx
        $writer = WriterFactory::create(Type::XLSX);
        // download to browser
        $writer->openToBrowser('datahasilrekamanlengkap' . date('YmdHis') . '.xlsx');
        // set style untuk header
        $headerStyle = (new StyleBuilder())
            ->setFontBold()
            ->build();

        $first = 1;
        foreach ($kecamatan as $res) {

            $sql = $this->db->query("SELECT * FROM MV_DHR where THN_PAJAK_SPPT = '$tahun' AND KD_KECAMATAN=" . $res->KODE_KEC . " AND KD_KELURAHAN =" . $res->KODE_KEL)->result();

            $arrisi = array();
            $no = 1;
            foreach ($sql as $rk) {
                $nop = $rk->KD_PROPINSI . '.' . $rk->KD_DATI2 . '.' . $rk->KD_KECAMATAN . '.' . $rk->KD_KELURAHAN . '.' . $rk->KD_BLOK . '.' . $rk->NO_URUT . '.' . $rk->KD_JNS_OP;
                $rt_rw = $rk->RT_OP . '/' . $rk->RW_OP;
                $ktp = "'" . $rk->KD_PROPINSI . $rk->KD_DATI2 . $rk->KD_KECAMATAN . $rk->KD_KELURAHAN . $rk->KD_BLOK . $rk->NO_URUT . $rk->KD_JNS_OP;
                $sts_wp = $rk->KD_STATUS_WP . ' - PEMILIK';

                $cc  = $no . '+' . $nop . '+' . $rk->NO_FORMULIR_SPOP . '+' . '' . '+' . $rk->ALAMAT_OP . '+' . $rt_rw . '+' . $rk->NM_WP_SPPT . '+' . $rk->LUAS_BUMI . '+' . $rk->KD_ZNT . '+' . $rk->NILAI_SISTEM_BUMI . '+' . $ktp . '+' . '' . '+' . $sts_wp . '+' . '' . '+' . $rk->NO_PERSIL . '+' . $rk->NO_FORMULIR_SPOP . '+' . $rk->NO_BNG . '+' . $rk->LUAS_BNG . '+' . $rk->JML_LANTAI_BNG . '+' . $rk->THN_DIBANGUN_BNG . '+' . $rk->THN_RENOVASI_BNG . '+' . $rk->KONDISI_BNG . '+' . $rk->JNS_KONSTRUKSI_BNG . '+' . $rk->JNS_ATAP_BNG . '+' . $rk->KD_DINDING . '+' . $rk->KD_LANTAI . '+' . $rk->KD_LANGIT_LANGIT;

                $ff  = explode('+', $cc);
                array_push($arrisi, $ff);
                $no++;

                // echo json_encode(array('result' => $arrisi ));
                //echo $cc."<br><hr>";
            }

            if ($first == 1) {
                // write ke Sheet pertama
                $nama = $res->NM_KELURAHAN . ' - Kec.' . $res->NM_KECAMATAN;
                $writer->getCurrentSheet()->setName($nama);
                // header Sheet pertama
                $writer->addRowWithStyle($header, $headerStyle);
                // data Sheet pertama
                $writer->addRows($arrisi);
            } else {
                $nama = $res->NM_KELURAHAN . ' - Kec.' . $res->NM_KECAMATAN;
                // write ke Sheet kedua
                $writer->addNewSheetAndMakeItCurrent()->setName($nama);
                // header Sheet kedua
                $writer->addRowWithStyle($header, $headerStyle);
                // data Sheet pertama
                $writer->addRows($arrisi);
            }
            $first++;
        }
        $writer->close();
    }
}
