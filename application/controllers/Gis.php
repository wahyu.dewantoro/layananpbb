<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

require_once APPPATH . '/third_party/spout/src/Spout/Autoloader/autoload.php';

use Box\Spout\Writer\WriterFactory;
use Box\Spout\Common\Type;
use Box\Spout\Writer\Style\StyleBuilder;

class Gis extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        if ($this->session->userdata('pbb') <> 1) {
            $this->session->set_flashdata('notif', '<div class="badge">
                    Silahkan login dengan username dan password anda.</p>
                    </div>');
            redirect('auth');
        }
        $this->load->model('Mmaster');
        $this->load->library('form_validation');
        $this->load->library('datatables');
    }

    function getkelurahan()
    {
        $KD_KECAMATAN = $this->input->post('kd_kec');
        $res = $this->db->query("select kd_kelurahan,nm_kelurahan 
                                from ref_kelurahan@to17
                                where kd_kecamatan='$KD_KECAMATAN' order by kd_kelurahan asc")->result();
        $option = "<option value=''>Pilih</option>";
        $option .= "<option value=''>000 Semua Kelurahan</option>";
        foreach ($res as $res) {
            $option .= "<option value='" . $res->KD_KELURAHAN . "'>" . $res->KD_KELURAHAN . ' ' . $res->NM_KELURAHAN . "</option>";
        }

        echo $option;
    }

    function getblok()
    {
        $KD_KECAMATAN = $this->input->post('kd_kec');
        $KD_KELURAHAN = $this->input->post('kd_kel');
        //echo $KD_KECAMATAN.'-'.$KD_KELURAHAN;
        $res = $this->db->query("select kd_kecamatan, kd_kelurahan, kd_blok from sppt where kd_kecamatan='$KD_KECAMATAN' and kd_kelurahan = '$KD_KELURAHAN' group by kd_kecamatan, kd_kelurahan, kd_blok order by kd_blok asc")->result();
        $option = "<option value=''>Pilih</option>";
        $option .= "<option value=''>Semua Blok</option>";
        foreach ($res as $res) {
            $option .= "<option value='" . $res->KD_BLOK . "'>" . $res->KD_BLOK . "</option>";
        }

        echo $option;
    }

    function mappingNop()
    {
        // if(isset($_POST['tgl'])){
        //   $tgl       =$_POST['tgl'];
        // }else{ 
        //   $tgl=date('d-m-Y');
        // }

        $kec = $this->db->query("SELECT KD_KECAMATAN,NM_KECAMATAN FROM REF_KECAMATAN@TO17 ORDER BY KD_KECAMATAN ASC")->result();
        $kec2 = $this->db->query("SELECT KD_KECAMATAN,NM_KECAMATAN FROM REF_KECAMATAN@TO17 ORDER BY KD_KECAMATAN ASC")->result();
        $KD_KECAMATAN = $this->input->post('KD_KECAMATAN', true);
        $KD_KECAMATAN2 = $this->input->post('KD_KECAMATAN2', true);
        $KD_KELURAHAN = $this->input->post('KD_KELURAHAN', true);
        $KD_KELURAHAN2 = $this->input->post('KD_KELURAHAN2', true);
        $KD_BLOK = $this->input->post('KD_BLOK', true);
        $kode_cari = $this->input->post('cari', true);
        $kode_cari2 = $this->input->post('cari2', true);

        $where = "";
        $query_loc = "";
        if ($KD_KECAMATAN <> '') {
            $where .= " WHERE KD_KECAMATAN='" . $KD_KECAMATAN . "'";
        }

        if ($KD_KECAMATAN2 <> '') {
            $query_loc = $this->db->query(" SELECT * FROM REF_KECAMATAN@to17 WHERE KD_KECAMATAN='" . $KD_KECAMATAN2 . "'")->row();
        }

        if ($KD_KELURAHAN <> '') {
            $where .= " AND KD_KELURAHAN='" . $KD_KELURAHAN . "'";
        }

        if ($KD_KELURAHAN2 <> '') {
            $query_loc = $this->db->query(" SELECT * FROM REF_KELURAHAN@to17 WHERE KD_KECAMATAN='" . $KD_KECAMATAN2 . "' AND KD_KELURAHAN='" . $KD_KELURAHAN2 . "'")->row();
        }

        if ($KD_BLOK <> '') {
            $where .= " AND KD_BLOK='" . $KD_BLOK . "'";
        }

        if ($KD_KECAMATAN <> '') {
            $kel = $this->db->query("select kd_kelurahan,nm_kelurahan 
                                    from ref_kelurahan
                                    where kd_kecamatan='$KD_KECAMATAN' order by kd_kelurahan asc")->result();
        } else {
            $kel = null;
        }

        if ($KD_KECAMATAN2 <> '') {
            $kel2 = $this->db->query("select kd_kelurahan,nm_kelurahan 
                                    from ref_kelurahan@to17
                                    where kd_kecamatan='$KD_KECAMATAN2' order by kd_kelurahan asc")->result();
        } else {
            $kel2 = null;
        }

        if ($KD_KELURAHAN <> '') {
            $blok = $this->db->query("select kd_kecamatan, kd_kelurahan, kd_blok from sppt where kd_kecamatan='$KD_KECAMATAN' and kd_kelurahan = '$KD_KELURAHAN' group by kd_kecamatan, kd_kelurahan, kd_blok order by kd_blok asc")->result();
        } else {
            $blok = null;
        }

        $query = $this->db->query("SELECT KD_PROPINSI||KD_DATI2||KD_KECAMATAN||KD_KELURAHAN||KD_BLOK||NO_URUT||KD_JNS_OP NOP, LAT, LNG FROM MAPPING_NOP $where ORDER BY ID ASC")->result();

        if (isset($kode_cari)) {
            $data = array(
                'button'       => 'Form Permohonan',
                'data'         => $query,
                'kec'          => $kec,
                'kec2'         => $kec2,
                'kel'          => $kel,
                'kel2'         => $kel2,
                'blok'         => $blok,
                'KD_KELURAHAN' => $KD_KELURAHAN,
                'KD_KECAMATAN' => $KD_KECAMATAN,
                'KD_BLOK'      => $KD_BLOK,
                'active'       => 'active',
            );

            // echo $where;
        } else if (isset($kode_cari2)) {
            $data = array(
                'button'       => 'Form Permohonan',
                'data2'        => $query_loc,
                'kec'          => $kec,
                'kec2'         => $kec2,
                'kel'          => $kel,
                'kel2'         => $kel2,
                'blok'         => $blok,
                'KD_KELURAHAN2' => $KD_KELURAHAN2,
                'KD_KECAMATAN2' => $KD_KECAMATAN2,
                'active2'       => 'active',
            );

            // echo $query_loc->LATI;
        } else {
            $data = array(
                'button'       => 'Form Permohonan',
                'kec'          => $kec,
                'kec2'         => $kec2,
                'kel'          => $kel,
                'kel2'         => $kel2,
                'blok'         => $blok,
                'KD_KELURAHAN2' => $KD_KELURAHAN2,
                'KD_KECAMATAN2' => $KD_KECAMATAN2,
                'active2'       => 'active',
            );
        }

        //exit();


        $this->template->load('template', 'gis/mappingNop', $data);
    }

    function tambahlokasiNop()
    {
        $lat = $this->input->post('lati', true);
        $lng = $this->input->post('longi', true);
        $nop = $this->input->post('NOP', true);
        $data = explode('.', $nop);
        $kd_propinsi  = $data[0];
        $kd_dati2     = $data[1];
        $kd_kecamatan = $data[2];
        $kd_kelurahan = $data[3];
        $kd_blok      = $data[4];
        $no_urut      = $data[5];
        $kd_jns_op    = $data[6];
        $tgl_layanan = date('Y-m-d H:i:s');
        $c_nop = str_replace('.', '', $nop);

        $cek = $this->db->query("SELECT COUNT(*) AS JML FROM MAPPING_NOP WHERE KD_PROPINSI||KD_DATI2||KD_KECAMATAN||KD_KELURAHAN||KD_BLOK||NO_URUT||KD_JNS_OP='$c_nop'")->row();
        if ($cek->JML > 0) {
            echo "<script> alert('NOP sudah di mapping !'); window.location='" . base_url('gis/mappingNop') . "'</script>";
        } else {

            $cari = $this->db->query("SELECT COUNT(*) JML FROM SPPT WHERE KD_PROPINSI||KD_DATI2||KD_KECAMATAN||KD_KELURAHAN||KD_BLOK||NO_URUT||KD_JNS_OP='$c_nop'")->row();
            if ($cari->JML == 0) {
                echo "<script> alert('NOP belum terdaftar !'); window.location='" . base_url('gis/mappingNop') . "'</script>";
            } else {

                $query = $this->db->query("INSERT INTO MAPPING_NOP (KD_PROPINSI,KD_DATI2,KD_KECAMATAN,KD_KELURAHAN,KD_BLOK,NO_URUT,KD_JNS_OP, LAT, LNG, CREATE_AT, STATUS) VALUES ('$kd_propinsi','$kd_dati2','$kd_kecamatan','$kd_kelurahan','$kd_blok','$no_urut','$kd_jns_op', '$lat', '$lng', TO_DATE('$tgl_layanan','yyyy/mm/dd hh24:mi:ss'), 0)");

                if ($query) {
                    echo "<script> alert('NOP berhasil di mapping !'); window.location='" . base_url('gis/mappingNop') . "'</script>";
                } else {
                    echo "<script> alert('Gagal mapping !'); window.location='" . base_url('gis/mappingNop') . "'</script>";
                }
            }
        }
    }

    function prosesimport()
    {
        error_reporting(E_ALL ^ E_NOTICE);

        require_once 'excel_reader2.php';
        $data = new Spreadsheet_Excel_Reader($_FILES['data_excel']['tmp_name']);

        $baris = $data->rowcount();

        $jum = 0;
        for ($i = 2; $i <= $baris; $i++) {

            $nop          = $data->val($i, 1);
            $lat          = $data->val($i, 2);
            $lng          = $data->val($i, 3);
            $kd_propinsi  = substr($nop, 0, 2);
            $kd_dati2     = substr($nop, 2, 2);
            $kd_kecamatan = substr($nop, 4, 3);
            $kd_kelurahan = substr($nop, 7, 3);
            $kd_blok      = substr($nop, 10, 3);
            $no_urut      = substr($nop, 13, 4);
            $kd_jns_op    = substr($nop, 17, 1);

            $cari = $this->db->query("SELECT * FROM MAPPING_NOP WHERE KD_PROPINSI='$kd_propinsi' AND KD_DATI2='$kd_dati2' AND KD_KECAMATAN='$kd_kecamatan' AND KD_KELURAHAN='$kd_kelurahan' AND KD_BLOK='$kd_blok' AND NO_URUT='$no_urut' AND KD_JNS_OP='$kd_jns_op' ")->result();

            $tgl_layanan = date('Y-m-d H:i:s');

            if (count($cari) > 0) { } else {
                $query = $this->db->query("INSERT INTO MAPPING_NOP (KD_PROPINSI,KD_DATI2,KD_KECAMATAN,KD_KELURAHAN,KD_BLOK,NO_URUT,KD_JNS_OP, LAT, LNG, CREATE_AT, STATUS) VALUES ('$kd_propinsi','$kd_dati2','$kd_kecamatan','$kd_kelurahan','$kd_blok','$no_urut','$kd_jns_op', '$lat', '$lng', TO_DATE('$tgl_layanan','yyyy/mm/dd hh24:mi:ss'), 0)");
            }



            if ($query) {
                $jum += 1;
            }
        }


        // echo "<script>alert('data berhasil di proses :'".$jum.");</script>";
        // echo "<script>window.alert('data berhasil di proses :'".$jum."); window.location='".base_url()."replikasi/sudahverlap';</script>";
        $this->session->set_flashdata('notif', '<div class="badge badge-success"><p> Data berhasil di proses : ' . $jum . '</p></div>');
        // $this->session->set_flashdata('jml_data', $jum);
        // $this->session->set_flashdata('modal', '1');
        redirect('gis/mappingNop');
    }

    function coba()
    {
        $this->load->view('gis/coba');
    }

    function monitoringVerlap()
    {
        if (isset($_GET['username'])) {
            $data['rk'] = $this->db->query("SELECT * FROM MONITORING_VERLAP WHERE USERNAME='" . $_GET['username'] . "'")->result();
            $data['nama_petugas'] = $_GET['username'];
        } else {
            $data['rk'] = $this->db->query("SELECT * FROM MONITORING_VERLAP WHERE STATUS=1")->result();
            $data['nama_petugas'] = $_GET['username'];
        }

        $data['data'] = $this->db->query("SELECT * FROM MONITORING_VERLAP")->result();
        $this->template->load('template', 'gis/monitoringverlap', $data);
    }
}
