<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

require_once APPPATH . '/third_party/spout/src/Spout/Autoloader/autoload.php';

use Box\Spout\Writer\WriterFactory;
use Box\Spout\Common\Type;
use Box\Spout\Writer\Style\StyleBuilder;

class Realisasi extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        if ($this->session->userdata('pbb') <> 1) {
            $this->session->set_flashdata('notif', '<div class="badge">
                    Silahkan login dengan username dan password anda.</p>
                    </div>');
            redirect('auth');
        }
        $this->load->model('Mrealisasi');
        $this->load->library('form_validation');
        $this->load->library('datatables');
    }

    public function normatif()
    {

        $tahun = $this->input->post('tahun') <> '' ? $this->input->post('tahun') : date('Y');
        $kd_kecamatan = $this->input->post('kd_kecamatan') <> '' ? $this->input->post('kd_kecamatan') : null;
        $kd_kelurahan = $this->input->post('kd_kelurahan') <> '' ? $this->input->post('kd_kelurahan') : null;
        $jb = $this->input->post('buku') <> '' ? $this->input->post('buku') : null;

        if ($kd_kecamatan != null) {
            $data['ref_kel'] = $this->db->query("select kd_kelurahan, nm_kelurahan
                               from ref_kelurahan 
                               where kd_kecamatan='$kd_kecamatan'
                               order by kd_kelurahan asc")->result();
        }

        $data['tahun']        = $tahun;
        $data['kd_kecamatan'] = $kd_kecamatan;
        $data['kd_kelurahan'] = $kd_kelurahan;
        $data['buku'] = array('Buku I', 'Buku II', 'Buku III', 'Buku IV', 'Buku V');
        $data['jns_buku'] = $jb;
        $data['ref_kec'] = $this->db->query("select kd_kecamatan,nm_kecamatan from ref_kecamatan order by kd_kecamatan asc")->result();
        $this->template->load('template', 'realisasi/realisasi_list', $data);
    }

    public function jsonnominatif()
    {
        ini_set('memory_limit', '-1');
        ini_set('memory_limit', '2048M');
        header('Content-Type: application/json');

        $tahun        = $this->input->post('tahun', true);
        $kd_kecamatan = $this->input->post('kd_kecamatan', true);
        $kd_kelurahan = $this->input->post('kd_kelurahan', true);
        $buku = $this->input->post('buku', true);

        echo $this->getjsonnominatif($tahun, $kd_kecamatan, $kd_kelurahan, $buku);
    }

    public function getjsonnominatif($tahun, $kd_kecamatan, $kd_kelurahan, $buku = null)
    {
        ini_set('memory_limit', '-1');
        ini_set('memory_limit', '2048M');
        header('Content-Type: application/json');

        return  $this->Mrealisasi->jsonRealisasiNominatif($tahun, $kd_kecamatan, $kd_kelurahan, $buku);
    }

    function getkelurahan()
    {
        $kd_kecamatan = $this->input->post('kd_kecamatan');
        $res = $this->db->query("select kd_kelurahan, nm_kelurahan
                               from ref_kelurahan 
                               where kd_kecamatan='$kd_kecamatan'
                               order by kd_kelurahan asc")->result();
        $html = "<option value=''>Pilih kelurahan</option>";
        foreach ($res as $res) {
            $html .= "<option value='" . $res->KD_KELURAHAN . "'>" . $res->KD_KELURAHAN . ' ' . $res->NM_KELURAHAN . "</option>";
        }

        echo $html;
    }

    function cetakNominatif()
    {
        $tahun        = urldecode($this->input->get('tahun'));
        $kd_kecamatan = urldecode($this->input->get('kd_kecamatan'));
        $kd_kelurahan = urldecode($this->input->get('kd_kelurahan'));
        $buku = urldecode($this->input->get('buku'));
        $arrphp = $this->Mrealisasi->getDataNominatif($tahun, $kd_kecamatan, $kd_kelurahan, $buku);

        /*$header=['NOP', 'Alamat OP', 'RW / RT OP','Kecamatan', 'Luas Bumi', 'Luas Bangunan', 'Nama WP', 'Alamat WP', 'RW / RT WP', 'Kelurahan WP', 'Kota WP', 'PBB', 'Terbit SPPT', 'Lunas','Pembayaran SPPT', ]; */

        $header = array('No', 'NOP', 'Alamat OP', 'RT/RW OP', 'Luas Tanah', 'Luas Bangunan', 'Nama WP', 'Alamat WP', 'RT/RW WP', 'Kelurahan WP', 'Kota WP', 'PBB', 'Buku');

        $writer = WriterFactory::create(Type::XLSX);
        // download to browser
        $writer->openToBrowser('data_nominatif.xlsx');
        // set style untuk header
        $headerStyle = (new StyleBuilder())
            ->setFontBold()
            ->build();

        $first = 1;

        /*$query=$this->db->query("select a.*,get_kecamatan(substr(nop,7,3)) nm_kec from dhkp_buku_345 a where thn_pajak_sppt=$tahun $and order by nop asc")->result();*/
        $arrisi = array();
        foreach ($arrphp as $rk) {
            /*$lunas=$rk->TGL_PEMBAYARAN_SPPT<>''?'Lunas':'Belum';
                            $ff= array($rk->NOP, $rk->ALAMAT_OP, $rk->RWRT_OP,$rk->NM_KEC,$rk->LUAS_BUMI_SPPT,$rk->LUAS_BNG_SPPT, $rk->NM_WP_SPPT, $rk->ALAMAT_WP, $rk->RWRT_WP,$rk->KELURAHAN_WP_SPPT,$rk->KOTA_WP_SPPT,(int)$rk->PBB, $rk->TGL_TERBIT_SPPT, $lunas,$rk->TGL_PEMBAYARAN_SPPT);
                            // $ff  =explode('#',$cc);*/

            $ff = array(
                $first,
                $rk->NOP,
                $rk->ALAMAT_OP,
                $rk->RTRW_OP,
                $rk->LUAS_BUMI_SPPT,
                $rk->LUAS_BNG_SPPT,
                $rk->NM_WP_SPPT,
                $rk->ALAMAT_WP,
                $rk->RTRW_WP,
                $rk->KELURAHAN_WP_SPPT,
                $rk->KOTA_WP_SPPT,
                (int) $rk->PBB,
                $rk->BUKU,
            );
            array_push($arrisi, $ff);
            $first++;
        }


        // write ke Sheet pertama

        $writer->getCurrentSheet()->setName("data_nominatif");
        // header Sheet pertama
        $writer->addRowWithStyle($header, $headerStyle);
        // data Sheet pertama
        $writer->addRows($arrisi);


        // close writter
        $writer->close();

        // print_r($data);
        /*echo $this->db->last_query();
        die();*/
        /*
       header("Content-Type: application/force-download");
       header("Cache-Control: no-cache, must-revalidate");
       header("Expires: Sat, 26 Jul 2010 05:00:00 GMT"); 
       header("content-disposition: attachment;filename=datanominatif".$tahun."_".$kd_kecamatan."_".$kd_kelurahan.".xls");
        $html="
        
        <table border='1'>
                    <thead>
                        <tr>
                            <th >No</th>
                                <th>NOP</th>
                                <th>Alamat OP</th>
                                <th>RT/RW OP</th>
                                <th>Luas Tanah</th>
                                <th>Luas Bangunan</th>
                                <th>Nama WP</th>
                                <th>Alamat WP</th>
                                <th>RT/RW WP</th>
                                <th>Kelurahan WP</th>
                                <th>Kota WP</th>
                                <th>PBB</th>
                        </tr>
                    </thead>
                    <tbody>";
                    foreach($arrphp as $rk){
                        $html.="<tr>
                            <td>". $rk->NOP. "</td>
                            <td>". $rk->ALAMAT_OP. "</td>
                            <td>". $rk->RTRW_OP. "</td>
                            <td>". $rk->LUAS_BUMI_SPPT. "</td>
                            <td>". $rk->LUAS_BNG_SPPT. "</td>
                            <td>". $rk->NM_WP_SPPT. "</td>
                            <td>". $rk->ALAMAT_WP. "</td>
                            <td>". $rk->RTRW_WP. "</td>
                            <td>". $rk->KELURAHAN_WP_SPPT. "</td>
                            <td>". $rk->KOTA_WP_SPPT. "</td>
                            <td>". (int)$rk->PBB. "</td>
                            <td>". $rk->BUKU. "</td>
                        </tr>";
                    }
                    $html.="</tbody>
                </table>";
                 // $html;
                echo $html;*/
    }

    public function desaLunas()
    {
        $jum = $this->db->query("select sum(baku) baku from desa_lunas")->row();
        $data = array(
            'jumlah' => $jum->BAKU
        );

        $this->template->load('template', 'realisasi/desalunas_list', $data);
    }

    public function desa_create()
    {
        $data = array(
            'button'       => 'Tambah Desa Lunas',
            'action'       => site_url('realisasi/tambahdesa_action'),
            'ID'           => set_value('ID'),
            'KD_KELURAHAN' => set_value('KD_KELURAHAN'),
            'KD_KECAMATAN' => set_value('KD_KECAMATAN'),
            'TGL_LUNAS'    => set_value('TGL_LUNAS'),
            'BAKU'         => set_value('BAKU'),
            'TAHUN_PAJAK'  => set_value('TAHUN_PAJAK'),
            'ref_kec'      => $this->db->query("select kd_kecamatan,nm_kecamatan from ref_kecamatan order by kd_kecamatan asc")->result()
        );

        $this->template->load('template', 'realisasi/desa_form', $data);
    }


    function tambahdesa_action()
    {
        /*echo "<pre>";
        print_r($this->input->post());*/

        $KD_KECAMATAN = $this->input->post('KD_KECAMATAN', true);
        $KD_KELURAHAN = $this->input->post('KD_KELURAHAN', true);
        $TGL_LUNAS    = $this->input->post('TGL_LUNAS', true);
        $BAKU         = str_replace('.', '', $this->input->post('BAKU', true));
        $TAHUN_PAJAK  = $this->input->post('TAHUN_PAJAK', true);

        // cek
        $cek = $this->db->query("SELECT count(*) inc FROM DESA_LUNAS WHERE 
KD_KECAMATAN='$KD_KECAMATAN' AND KD_KELURAHAN='$KD_KELURAHAN' AND TAHUN_PAJAK='$TAHUN_PAJAK'")->row();

        if ($cek->INC == 0) {
            // insert
            $this->db->set('KD_KECAMATAN', $KD_KECAMATAN);
            $this->db->set('KD_KELURAHAN', $KD_KELURAHAN);
            $this->db->set('TGL_LUNAS', "to_date('" . $TGL_LUNAS . "','dd-mm-yyyy')", false);
            $this->db->set('BAKU', $BAKU);
            $this->db->set('TAHUN_PAJAK', $TAHUN_PAJAK);
            $this->db->insert('DESA_LUNAS');
            $notif = "Data telah di simpan.";
        } else {
            // ga di insert
            $notif = "Data tidak di simpan karena sudah ada data.";
        }

        $alert = "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button> " . $notif . "</div>";
        $this->session->set_flashdata('notif_desa', $alert);
        redirect($_SERVER['HTTP_REFERER']);
    }

    function editdesa()
    {
        $id = $this->input->get('id');

        if ($id) {
            $this->db->where('ID', $id);
            $this->db->select("DESA_LUNAS.*,to_char(tgl_lunas,'dd-mm-yyyy') tgl_lns");
            $res = $this->db->get('DESA_LUNAS')->row();
            if ($res) {
                $kd_kecamatan = $res->KD_KECAMATAN;
                $data = array(
                    'button'       => 'Edit Desa Lunas',
                    'action'       => site_url('realisasi/editdesa_action'),
                    'ID'           => set_value('ID', $res->ID),
                    'KD_KELURAHAN' => set_value('KD_KELURAHAN', $res->KD_KELURAHAN),
                    'KD_KECAMATAN' => set_value('KD_KECAMATAN', $res->KD_KECAMATAN),
                    'TGL_LUNAS'    => set_value('TGL_LUNAS', $res->TGL_LNS),
                    'BAKU'         => set_value('BAKU', number_format($res->BAKU, '0', '', '.')),
                    'TAHUN_PAJAK'  => set_value('TAHUN_PAJAK', $res->TAHUN_PAJAK),
                    'ref_kec'      => $this->db->query("select kd_kecamatan,nm_kecamatan from ref_kecamatan order by kd_kecamatan asc")->result(),
                    'ref_kel' => $this->db->query("select kd_kelurahan, nm_kelurahan
                               from ref_kelurahan 
                               where kd_kecamatan='$kd_kecamatan'
                               order by kd_kelurahan asc")->result()
                );

                $this->template->load('template', 'realisasi/desa_form', $data);
            } else {
                redirect($_SERVER['HTTP_REFERER']);
            }
        } else {
            redirect($_SERVER['HTTP_REFERER']);
        }
    }

    function editdesa_action()
    {
        $KD_KECAMATAN = $this->input->post('KD_KECAMATAN', true);
        $KD_KELURAHAN = $this->input->post('KD_KELURAHAN', true);
        $TGL_LUNAS    = $this->input->post('TGL_LUNAS', true);
        $BAKU         = str_replace('.', '', $this->input->post('BAKU', true));
        $TAHUN_PAJAK  = $this->input->post('TAHUN_PAJAK', true);
        $ID           = $this->input->post('ID', true);
        $this->db->set('KD_KECAMATAN', $KD_KECAMATAN);
        $this->db->set('KD_KELURAHAN', $KD_KELURAHAN);
        $this->db->set('TGL_LUNAS', "to_date('" . $TGL_LUNAS . "','dd-mm-yyyy')", false);
        $this->db->set('BAKU', $BAKU);
        $this->db->set('TAHUN_PAJAK', $TAHUN_PAJAK);
        $this->db->where('ID', $ID);
        $this->db->update('DESA_LUNAS');

        // echo $this->db->last_query();

        $notif = "Data telah di update.";
        $alert = "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button> " . $notif . "</div>";
        $this->session->set_flashdata('notif_desa', $alert);
        redirect('realisasi/desaLunas');
    }

    function hapusdesa()
    {
        $id = $this->input->get('id');
        $this->db->where('ID', $id);
        $this->db->delete('DESA_LUNAS');
        $notif = "Data telah di hapus.";
        $alert = "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button> " . $notif . "</div>";
        $this->session->set_flashdata('notif_desa', $alert);
        redirect('realisasi/desaLunas');
    }

    function jsondesalunas()
    {
        /*ini_set('memory_limit', '-1');
        ini_set('memory_limit', '2048M');*/
        header('Content-Type: application/json');
        echo  $this->Mrealisasi->jsondesalunas();
    }

    function pembayaranPokok()
    {
        $query = $this->db->query("select BUKU,BAKU,POKOK_ML,DENDA_ML,JUMLAH_ML,POKOK_MI,DENDA_MI,JUMLAH_MI,POKOK_SMI,DENDA_SMI,JUMLAH_SMI,round(persen) PERSEN,SISA from MV_REALISASI")->result();
        $data = array(
            'data' => $query
        );
        $this->template->load('template', 'realisasi/pembayaranPokok', $data);
    }

    function cetakpembayaranPokok()
    {
        $data = $this->db->query("select BUKU,BAKU,POKOK_ML,DENDA_ML,JUMLAH_ML,POKOK_MI,DENDA_MI,JUMLAH_MI,POKOK_SMI,DENDA_SMI,JUMLAH_SMI,round(persen) PERSEN,SISA from MV_REALISASI")->result();
        header("Content-Type: application/force-download");
        header("Cache-Control: no-cache, must-revalidate");
        header("Expires: Sat, 26 Jul 2010 05:00:00 GMT");
        header("content-disposition: attachment;filename=realisasi_pembayaranpokok.xls");
        $html = ' <table  width="100%" border="1" class="table table-bordered table-hover" id="example2">
                    <thead>
                         <tr>
                           <th rowspan="3">Uraian</th>
                           <th rowspan="3">Baku</th>
                           
                           <th colspan="9">Realisasi</th>
                           <th rowspan="3">(%)</th>
                           <th rowspan="3">Sisa</th>
                        </tr>
                        <tr>
                           <th colspan="3">Minggu Lalu</th>
                           <th colspan="3">Minggu Ini</th>
                            <th colspan="3">S/D Minggu Ini</th>
                        </tr>
                        <tr>
                           <th>Pokok</th>
                           <th>Denda</th>
                           <th>Jumlah</th>
                           <th>Pokok</th>
                           <th>Denda</th>
                           <th>Jumlah</th>
                           <th>Pokok</th>
                           <th>Denda</th>
                           <th>Jumlah</th>
                          
                        </tr>
                    </thead><tbody>';
        foreach ($data as $rk) {
            $html .= '<tr>

                                <td>' . $rk->BUKU . '</td>
                                <td align="right">' . number_format($rk->BAKU, 0, '', '.') . '</td>
                                <td align="right">' . number_format($rk->POKOK_ML, 0, '', '.') . '</td>
                                <td align="right">' . number_format($rk->DENDA_ML, 0, '', '.') . '</td>
                                <td align="right">' . number_format($rk->JUMLAH_ML, 0, '', '.') . '</td>
                                <td align="right">' . number_format($rk->POKOK_MI, 0, '', '.') . '</td>
                                <td align="right">' . number_format($rk->DENDA_MI, 0, '', '.') . '</td>
                                <td align="right">' . number_format($rk->JUMLAH_MI, 0, '', '.') . '</td>
                                <td align="right">' . number_format($rk->POKOK_SMI, 0, '', '.') . '</td>
                                <td align="right">' . number_format($rk->DENDA_SMI, 0, '', '.') . '</td>
                                <td align="right">' . number_format($rk->JUMLAH_SMI, 0, '', '.') . '</td>
                                <td align="center">' . number_format($rk->PERSEN, 2, ',', '.') . '</td>
                                <td align="right">' . number_format($rk->SISA, 0, '', '.') . '</td>
                            </tr>';
        }

        $html .= '</tbody></table>';
        echo $html;
    }

    function jsonrealisasiPokok()
    {
        header('Content-Type: application/json');
        echo  $this->Mrealisasi->jsonrealisasiPokok();
    }

    function ketetapantigaUp()
    {
        $data['data'] = $this->db->query("SELECT *  FROM MV_REALISASI_TIGA_EMPAT_LIMA")->result();
        $this->template->load('template', 'realisasi/ketetapantigaUp', $data);
    }

    function ketetapansatuUp()
    {
        $data['data'] = $this->db->query("SELECT *  FROM MV_REALISASI_SATU_DUA")->result();
        $this->template->load('template', 'realisasi/ketetapansatuUp', $data);
    }



    function cetaketetapantigaUp()
    {
        $data = $this->db->query("SELECT *  FROM MV_REALISASI_TIGA_EMPAT_LIMA")->result();
        header("Content-Type: application/force-download");
        header("Cache-Control: no-cache, must-revalidate");
        header("Expires: Sat, 26 Jul 2010 05:00:00 GMT");
        header("content-disposition: attachment;filename=laporan_realisasi.xls");
        $html = ' <table width="100%" border="1">
                    <thead>
                        <tr>
                            <th rowspan="2">Kecamatan</th>
                            <th rowspan="2">Jumalah SPPT</th>
                            <th  rowspan="2">Baku</th>
                            <th colspan="4">Realisasi</th>
                            <th rowspan="2">%</th>
                            <th colspan="2">Sisa</th>
                        </tr>
                        <tr>
                            
                            <th>Sppt</th>
                            <th>Pokok</th>
                            <th>Denda</th>
                            <th>Jumlah</th>
                            <th>SPPT</th>
                            <th>Pokok</th>
                            
                        </tr>
                    </thead>
                    <tbody>';
        foreach ($data as $rk) {
            $html .= '<tr>
                                 <td>' . $rk->NM_KECAMATAN . '</td>
                                 <td align="center">' . $rk->JUM_SPPT . '</td>
                                 <td align="right">' . number_format($rk->BAKU, '0', '', '.') . '</td>
                                 <td align="center">' . $rk->SPPT_REALISASI . '</td>
                                 <td align="right">' . number_format($rk->POKOK_REALISASI, '0', '', '.') . '</td>
                                 <td align="right">' . number_format($rk->DENDA_REALISASI, '0', '', '.') . '</td>
                                 <td align="right">' . number_format($rk->JUMLAH_REALISASI, '0', '', '.') . '</td>
                                 <td>' . number_format($rk->PERSEN_REALISASI, '2', ',', '.') . '</td>
                                 <td align="center">' . $rk->SPPT_SISA . '</td>
                                 <td align="right">' . number_format($rk->POKOK_SISA, '0', '', '.') . '</td>
                            </tr>';
        }
        $html .= '</tbody>
                </table>';
        echo $html;
    }

    function cetaketetapansatuUp()
    {
        $data = $this->db->query("SELECT *  FROM MV_REALISASI_SATU_DUA")->result();
        header("Content-Type: application/force-download");
        header("Cache-Control: no-cache, must-revalidate");
        header("Expires: Sat, 26 Jul 2010 05:00:00 GMT");
        header("content-disposition: attachment;filename=laporan_realisasi_satu_dua.xls");
        $html = ' <table width="100%" border="1">
                    <thead>
                        <tr>
                            <th rowspan="2">Kecamatan</th>
                            <th rowspan="2">Jumalah SPPT</th>
                            <th  rowspan="2">Baku</th>
                            <th colspan="4">Realisasi</th>
                            <th rowspan="2">%</th>
                            <th colspan="2">Sisa</th>
                        </tr>
                        <tr>
                            
                            <th>Sppt</th>
                            <th>Pokok</th>
                            <th>Denda</th>
                            <th>Jumlah</th>
                            <th>SPPT</th>
                            <th>Pokok</th>
                            
                        </tr>
                    </thead>
                    <tbody>';
        foreach ($data as $rk) {
            $html .= '<tr>
                                 <td>' . $rk->NM_KECAMATAN . '</td>
                                 <td align="center">' . $rk->JUM_SPPT . '</td>
                                 <td align="right">' . number_format($rk->BAKU, '0', '', '.') . '</td>
                                 <td align="center">' . $rk->SPPT_REALISASI . '</td>
                                 <td align="right">' . number_format($rk->POKOK_REALISASI, '0', '', '.') . '</td>
                                 <td align="right">' . number_format($rk->DENDA_REALISASI, '0', '', '.') . '</td>
                                 <td align="right">' . number_format($rk->JUMLAH_REALISASI, '0', '', '.') . '</td>
                                 <td>' . number_format($rk->PERSEN_REALISASI, '2', ',', '.') . '</td>
                                 <td align="center">' . $rk->SPPT_SISA . '</td>
                                 <td align="right">' . number_format($rk->POKOK_SISA, '0', '', '.') . '</td>
                            </tr>';
        }
        $html .= '</tbody>
                </table>';
        echo $html;
    }

    function apbd()
    {
        $data['rk'] = $this->db->query("select * from TARGET_APBD_TES")->result();
        $this->template->load('template', 'realisasi/targetApbd', $data);
    }

    function targetapbd_create()
    {
        $data = array(
            'button'     => 'Tambah Target APBD',
            'action'     => site_url('realisasi/action_target_apbd'),
            'THN_PAJAK'  => set_value('THN_PAJAK'),
            'TARGET'     => set_value('TARGET'),
            'TARGET_PAK' => set_value('TARGET_PAK'),
            'kat'        => '0',
            'edit' => false
        );
        $this->template->load('template', 'realisasi/targetApbd_form', $data);
    }

    function action_target_apbd()
    {
        $THN_PAJAK  = $this->input->post('THN_PAJAK', true);
        $KATEGORI   = $this->input->post('KATEGORI', true);
        $TARGET     = str_replace('.', '', $this->input->post('TARGET', true));
        $TARGET_PAK = str_replace('.', '', $this->input->post('TARGET_PAK', true));

        // cek
        $cc = $this->db->query("select count(*) jum from TARGET_APBD_TES
where thn_pajak='$THN_PAJAK' AND KATEGORI='$KATEGORI'")->row();
        if ($cc->JUM == 0) {
            $this->db->set('THN_PAJAK', $THN_PAJAK);
            $this->db->set('TARGET', $TARGET);
            $this->db->set('TARGET_PAK', $TARGET_PAK);
            $this->db->set('KATEGORI', $KATEGORI);
            $this->db->insert('TARGET_APBD_TES');
            $notif = "Data telah di simpan";
        } else {
            $notif = "Data tidak di simpan karena sudah ada data.";
        }


        $alert = "<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button> " . $notif . "</div>";
        $this->session->set_flashdata('notif_desa', $alert);
        redirect($_SERVER['HTTP_REFERER']);
    }



    function hapusapbd($id)
    {
        // $this->db->where('THN_PAJAK',$thn);
        // $this->db->where('BUKU','Buku I dan II');
        $this->db->where('ID', $id);
        $this->db->delete('TARGET_APBD_TES');
        redirect($_SERVER['HTTP_REFERER']);
    }


    function editapbd($id)
    {
        // $this->db->where('THN_PAJAK',$tahun);
        // $this->db->where('BUKU','Buku I dan II');
        $this->db->where('ID', $id);
        $rk = $this->db->get('TARGET_APBD_TES')->row();
        $data = array(
            'button'     => 'Edit Target APBD',
            'action'     => site_url('realisasi/update_target_apbd'),
            'THN_PAJAK'  => set_value('THN_PAJAK', $rk->THN_PAJAK),
            'TARGET'     => set_value('TARGET', number_format($rk->TARGET, '0', '', '.')),
            'TARGET_PAK' => set_value('TARGET_PAK', number_format($rk->TARGET_PAK, '0', '', '.')),
            'kat'        => $rk->KATEGORI,
            'ID'         => $rk->ID,
            'edit' => true
        );
        $this->template->load('template', 'realisasi/targetApbd_form', $data);
    }

    function update_target_apbd()
    {
        $THN_PAJAK  = $this->input->post('THN_PAJAK', true);
        $KATEGORI   = $this->input->post('KATEGORI', true);
        $ID         = $this->input->post('ID', true);
        $TARGET     = str_replace('.', '', $this->input->post('TARGET', true));
        $TARGET_PAK = str_replace('.', '', $this->input->post('TARGET_PAK', true));

        $this->db->set('TARGET', $TARGET);
        $this->db->set('TARGET_PAK', $TARGET_PAK);
        $this->db->set('KATEGORI', $KATEGORI);
        $this->db->where('ID', $ID);
        $this->db->update('TARGET_APBD_TES');
        redirect('realisasi/apbd');
    }

    function realisasiAPBD()
    {
        /*$tahun = $this->input->post('TAHUN',true);
        if($tahun==''){
            $thn = date('Y');
        }else{
            $thn = $tahun;
        }

        $data['tahun'] = $thn;*/
        $data['rk'] = $this->db->query("SELECT * FROM MV_REALISASI_TARGET_SPBD")->result();
        $this->template->load('template', 'realisasi/realisasiAPBD', $data);
    }

    function realisasiAPBDTigaup()
    {
        $tahun = $this->input->post('TAHUN', true);
        if ($tahun == '') {
            $thn = date('Y');
        } else {
            $thn = $tahun;
        }

        $data['tahun'] = $thn;
        $data['rk'] = $this->db->query("SELECT  B.KATEGORI, B.THN_PAJAK, B.TARGET, B.TARGET_PAK, A.* FROM MV_REALISASI A LEFT JOIN (SELECT CASE WHEN  TA.KATEGORI = 1 THEN 'Buku I, II' ELSE 'Buku III,IV,V' END BUKU, TA.* FROM TARGET_APBD_TES TA) B ON A.BUKU = B.BUKU WHERE B.THN_PAJAK='$thn' AND B.KATEGORI=2")->result();

        $this->template->load('template', 'realisasi/realisasiAPBDTigaup', $data);
    }

    //     function realisasiAPBDsatuup(){
    //         $data['rk']=$this->db->query("SELECT * FROM TARGET_APBD WHERE BUKU='Buku I, II'")->result();
    //         $this->template->load('template','realisasi/realisasiAPBDsatuup',$data);          
    //     }

    //     public function rtabpdsatuup_create() 
    //     {
    //         $data = array(
    //              'button'     => 'Tambah Realisasi Target APBD Buku I, II',
    //              'action'     => site_url('realisasi/action_rtabpdsatuup'),
    //              'THN_PAJAK'  => set_value('THN_PAJAK'),
    //              'TARGET'     => set_value('TARGET'),
    //              'TARGET_PAK' => set_value('TARGET_PAK'),
    //              'edit'=>false
    //             );
    //         $this->template->load('template','realisasi/rtApbdsatuup_form',$data);  
    //     }

    //     function action_rtabpdsatuup(){
    //         $THN_PAJAK=$this->input->post('THN_PAJAK',true);
    //         $TARGET=str_replace('.','',$this->input->post('TARGET',true));
    //         $TARGET_PAK=str_replace('.','',$this->input->post('TARGET_PAK',true));

    //         // cek
    //         $cc=$this->db->query("select count(*) jum from TARGET_APBD
    // where thn_pajak='$THN_PAJAK' and buku='Buku I, II'")->row();
    //         if($cc->JUM==0){
    //             $this->db->set('THN_PAJAK',$THN_PAJAK);
    //             $this->db->set('TARGET',$TARGET);
    //             $this->db->set('TARGET_PAK',$TARGET_PAK);
    //             $this->db->set('BUKU','Buku I, II');
    //             $this->db->insert('TARGET_APBD');
    //             $notif="Data telah di simpan";
    //         }else{
    //             $notif="Data tidak di simpan karena sudah ada data.";
    //         }


    //          $alert="<div class='alert alert-success'><button type='button' class='close' data-dismiss='alert' aria-hidden='true'>&times;</button> ".$notif."</div>";
    //          $this->session->set_flashdata('notif_desa',$alert);
    //         redirect($_SERVER['HTTP_REFERER']);

    //     }

    //     function hapusrtabpdsatuup($thn){
    //         $this->db->where('THN_PAJAK',$thn);
    //         $this->db->where('BUKU','Buku I, II');
    //         $this->db->delete('TARGET_APBD');
    //         redirect($_SERVER['HTTP_REFERER']);        
    //     }

    //     function editrtabpdsatuup($tahun){
    //         $this->db->where('THN_PAJAK',$tahun);
    //         $this->db->where('BUKU','Buku I, II');
    //         $rk=$this->db->get('TARGET_APBD')->row();
    //           $data = array(
    //              'button'     => 'Edit Realisasi Target APBD Buku I, II',
    //              'action'     => site_url('realisasi/update_rtabpdsatuup'),
    //              'THN_PAJAK'  => set_value('THN_PAJAK',$rk->THN_PAJAK),
    //              'TARGET'     => set_value('TARGET',number_format($rk->TARGET,'0','','.')),
    //              'TARGET_PAK' => set_value('TARGET_PAK',number_format($rk->TARGET_PAK,'0','','.')),
    //              'edit'=>true
    //             );
    //         $this->template->load('template','realisasi/rtApbdsatuup_form',$data);  

    //     }

    //     function update_rtabpdsatuup(){
    //         $THN_PAJAK  =$this->input->post('THN_PAJAK',true);
    //         $TARGET     =str_replace('.','',$this->input->post('TARGET',true));
    //         $TARGET_PAK =str_replace('.','',$this->input->post('TARGET_PAK',true));

    //         $this->db->set('TARGET',$TARGET);
    //         $this->db->set('TARGET_PAK',$TARGET_PAK);
    //         $this->db->where('THN_PAJAK',$THN_PAJAK);
    //         $this->db->where('BUKU','Buku I, II');
    //         $this->db->update('TARGET_APBD');
    //         redirect('realisasi/realisasiAPBDsatuup');
    //     }

    //     function realisasiAPBDtigaup(){
    //         $data['rk']=$this->db->query("SELECT * FROM MV_TARGET_APBD")->result();
    //         $this->template->load('template','realisasi/realisasiAPBDtigaup',$data);          
    //     }

    function cetaksatuUp()
    {
        $tahun = $this->input->get('tahun', true);
        if ($tahun == '') {
            $thn = date('Y');
        } else {
            $thn = $tahun;
        }

        $data['tahun'] = $thn;

        $rk = $this->db->query("SELECT * FROM MV_REALISASI_TARGET_SPBD")->result();
        header("Content-Type: application/force-download");
        header("Cache-Control: no-cache, must-revalidate");
        header("Expires: Sat, 26 Jul 2010 05:00:00 GMT");
        header("content-disposition: attachment;filename=realisasi_pajak.xls");
        $html = ' <table class="table table-bordered table-hover" border="1" >
                    <thead>
                        <tr>
                            <th width="5%">No</th>
                            <th>Buku</th>
                            <th>Target</th>
                            <th>Pokok</th>
                            <th>Denda</th>
                            <th>Jumlah</th>
                            <th>%</th>
                            <th>Kurang / Lebih</th>
                        </tr>
                    </thead>
                    <tbody>';
        $no = 1;
        foreach ($rk as $rk) {
            $html .= '<tr>
                            <td align="center">' . $no . '</td>
                            <td>' . $rk->BUKU . '</td>
                            <td align="right">' . number_format($rk->TARGET, 0, '', '.') . '</td>
                            <td align="right">' . number_format($rk->POKOK, 0, '', '.') . '</td>
                            <td align="right">' . number_format($rk->DENDA, 0, '', '.') . '</td>
                            <td align="right">' . number_format($rk->JUMLAH, 0, '', '.') . '</td>
                            <td align="center">' . number_format($rk->PERSEN, 2, ',', '.') . '</td>
                            <td align="right">' . number_format($rk->KURANG_ATAU_LEBIH, 0, '', '.') . '</td>
                        </tr>';
            $no++;
        }
        $html .= '</tbody>
                </table>';
        echo $html;
    }

    function cetaktigaUp()
    {
        $tahun = $this->input->get('tahun', true);
        if ($tahun == '') {
            $thn = date('Y');
        } else {
            $thn = $tahun;
        }

        $data['tahun'] = $thn;

        $rk = $this->db->query("SELECT  B.KATEGORI, B.THN_PAJAK, B.TARGET, B.TARGET_PAK, A.* FROM MV_REALISASI A LEFT JOIN (SELECT CASE WHEN  TA.KATEGORI = 1 THEN 'Buku I, II' ELSE 'Buku III,IV,V' END BUKU, TA.* FROM TARGET_APBD_TES TA) B ON A.BUKU = B.BUKU WHERE B.THN_PAJAK='$thn' AND B.KATEGORI=2")->result();
        header("Content-Type: application/force-download");
        header("Cache-Control: no-cache, must-revalidate");
        header("Expires: Sat, 26 Jul 2010 05:00:00 GMT");
        header("content-disposition: attachment;filename=target_apbd_tigaup.xls");
        $html = ' <table class="table table-bordered table-hover" border="1" >
                    <thead>
                        <tr>
                            <th width="5%">No</th>
                            <th>Tahun</th>
                            <th>Buku</th>
                            <th>Target</th>
                            <th>Pokok</th>
                            <th>Denda</th>
                            <th>Jumlah</th>
                            <th>%</th>
                            <th>Kurang / Lebih</th>
                        </tr>
                    </thead>
                    <tbody>';
        $no = 1;
        foreach ($rk as $rk) {
            $html .= '<tr>
                            <td align="center">' . $no . '</td>
                            <td>' . $rk->THN_PAJAK . '</td>
                            <td>' . $rk->BUKU . '</td>
                            <td align="right">' . number_format($rk->TARGET, 0, '', '.') . '</td>
                            <td align="right">' . number_format($rk->POKOK, 0, '', '.') . '</td>
                            <td align="right">' . number_format($rk->DENDA_ML, 0, '', '.') . '</td>
                            <td align="right">' . number_format($rk->JUMLAH_ML, 0, '', '.') . '</td>
                            <td align="center">' . number_format($rk->PERSEN, 2, ',', '.') . '</td>
                            <td align="right">' . number_format($rk->SISA, 0, '', '.') . '</td>
                        </tr>';
            $no++;
        }
        $html .= '</tbody>
                </table>';
        echo $html;
    }



    public function _rules()
    {
        $this->form_validation->set_rules('tahun', 'tahun', 'trim|required');
        $this->form_validation->set_rules('target', 'target', 'trim|required');
        $this->form_validation->set_error_delimiters('<span class="label label-danger ">', '</span>');
    }

    /*

    public function create() 
    {
        $data = array(
            'button'     => 'Tambah Bank',
            'action'     => site_url('bank/create_action'),
            'ID' => set_value('ID'),
            'NAMA_BANK' => set_value('NAMA_BANK'),
            'KODE_BANK' => set_value('KODE_BANK'),
	);
        $this->template->load('template','bank/bank_form', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'NAMA_BANK' => $this->input->post('NAMA_BANK',TRUE),
        'KODE_BANK' => $this->input->post('KODE_BANK',TRUE),
	    );

            $this->Mbank->insert($data);
            // $this->db->query("commit");
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('bank'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Mbank->get_by_id($id);

        if ($row) {
            $data = array(
                'button'     => 'Update Group ',
                'action'     => site_url('bank/update_action'),
                'ID' => set_value('ID',$row->ID),
                'NAMA_BANK' => set_value('NAMA_BANK',$row->NAMA_BANK),
                'KODE_BANK' => set_value('KODE_BANK',$row->KODE_BANK),
	    );
            $this->template->load('template','bank/bank_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('bank'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('kode_group', TRUE));
        } else {
            $data = array(
		'NAMA_BANK' => $this->input->post('NAMA_BANK',TRUE),
        'KODE_BANK' => $this->input->post('KODE_BANK',TRUE),
	    );

            $this->Mbank->update($this->input->post('ID', TRUE), $data);
            $this->db->query("commit");
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('bank'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Mbank->get_by_id($id);

        if ($row) {
            $this->Mbank->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('bank'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('bank'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('NAMA_BANK', 'nama bank', 'trim|required');
    $this->form_validation->set_rules('KODE_BANK', 'kode bank', 'trim|required');
	$this->form_validation->set_rules('ID', 'ID', 'trim');
	$this->form_validation->set_error_delimiters('<span class="label label-danger ">', '</span>');
    }

    function test(){
        $rk=$this->db->query("select * from p_group")->result();
        echo "<pre>";
        print_r($rk);
        echo "<pre>";
    }
*/
}

/* End of file Group.php */
/* Location: ./application/controllers/Group.php */
/* Generated by Mohamad Wahyu Dewantoro 2017-04-28 01:25:45 */
