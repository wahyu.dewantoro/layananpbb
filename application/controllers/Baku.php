<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

require_once APPPATH . '/third_party/spout/src/Spout/Autoloader/autoload.php';

use Box\Spout\Writer\WriterFactory;
use Box\Spout\Common\Type;
use Box\Spout\Writer\Style\StyleBuilder;


class Baku extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        if ($this->session->userdata('pbb') <> 1) {
            $this->session->set_flashdata('notif', '<div class="badge">
                    Silahkan login dengan username dan password anda.</p>
                    </div>');
            redirect('auth');
        }

 
    }

    function ringkasan()
    {
        $th = date('Y');

        if (isset($_GET['TAHUN'])) {
            $tahun = $_GET['TAHUN'];
        } else {
            $tahun = date('Y');
        }

        $kec = $this->db->query("SELECT KD_KECAMATAN,NM_KECAMATAN FROM REF_KECAMATAN order by kd_kecamatan asc")->result();
        $KD_KECAMATAN = $this->input->get('KD_KECAMATAN', true);
        $KD_KELURAHAN = $this->input->get('KD_KELURAHAN', true);

        $where = "";
        if ($KD_KECAMATAN <> '') {
            $where .= " where A.kd_kecamatan='" . $KD_KECAMATAN . "'";
        }

        if ($KD_KELURAHAN <> '') {
            $where .= " and A.KD_KELURAHAN='" . $KD_KELURAHAN . "'";
        }

        if ($KD_KECAMATAN <> '') {
            $kel = $this->db->query("SELECT kd_kelurahan,nm_kelurahan
                                    from ref_kelurahan
                                    where kd_kecamatan='$KD_KECAMATAN'  order by kd_kecamatan asc,kd_kelurahan asc")->result();
        } else {
            $kel = null;
        }
        // $query = $this->db->query("SELECT A.KD_KECAMATAN,GET_KECAMATAN(A.KD_KECAMATAN) NAMA_KEC,A.KD_KELURAHAN , GET_KELURAHAN(A.KD_KELURAHAN,A.KD_KECAMATAN) NM_KEL,A.BUKU,SPPT_BAKU,JUMLAH_BAKU,SPPT_REALISASI,JUMLAH_REALISASI,SPPT_PIUTANG,JUMLAH_PIUTANG
        //                      FROM MASTER_BUKU A
        //                      LEFT JOIN   (SELECT * FROM  MV_BAKU_REALISASI WHERE  THN_PAJAK_SPPT=$tahun) B ON (  A.KD_KECAMATAN=B.KD_KECAMATAN AND A.KD_KELURAHAN=B.KD_KELURAHAN AND A.BUKU=B.BUKU)
        //                      $where
        //                      ORDER BY A.KD_KECAMATAN ASC,A.KD_KELURAHAN ASC,A.BUKU ASC")->result();

        $query = [];
        if ($KD_KECAMATAN <> '' && $KD_KELURAHAN <> '') {
            $query = $this->db->query("SELECT A.*,
							         B.SPPT_BAKU SPPT_REALISASI,
							         B.JUMLAH_BAKU JUMLAH_REALISASI,
							         C.SPPT_BAKU SPPT_PIUTANG,
							         B.JUMLAH_BAKU JUMLAH_PIUTANG
							    FROM (  SELECT A.KD_KECAMATAN,
							                   GET_KECAMATAN (A.KD_KECAMATAN) NAMA_KEC,
							                   A.KD_KELURAHAN,
							                   GET_KELURAHAN (A.KD_KELURAHAN, A.KD_KECAMATAN) NM_KEL,
							                   A.BUKU,
							                   COUNT (PBB) SPPT_BAKU,
							                   SUM (PBB) JUMLAH_BAKU
							              FROM MV_DHKP_DESA_TAHUN A
							             WHERE     A.THN_PAJAK_SPPT = $tahun
							                   AND KD_KECAMATAN = '$KD_KECAMATAN'
							                   AND KD_KELURAHAN = '$KD_KELURAHAN'
							          GROUP BY A.KD_KECAMATAN,
							                   GET_KECAMATAN (A.KD_KECAMATAN),
							                   A.KD_KELURAHAN,
							                   GET_KELURAHAN (A.KD_KELURAHAN, A.KD_KECAMATAN),
							                   A.BUKU) A
							         LEFT JOIN (  SELECT A.KD_KECAMATAN,
							                             GET_KECAMATAN (A.KD_KECAMATAN) NAMA_KEC,
							                             A.KD_KELURAHAN,
							                             GET_KELURAHAN (A.KD_KELURAHAN, A.KD_KECAMATAN)
							                                NM_KEL,
							                             A.BUKU,
							                             COUNT (PBB) SPPT_BAKU,
							                             SUM (PBB) JUMLAH_BAKU
							                        FROM MV_DHKP_DESA_TAHUN A
							                       WHERE     A.THN_PAJAK_SPPT = $tahun
							                             AND KD_KECAMATAN = '$KD_KECAMATAN'
							                             AND KD_KELURAHAN = '$KD_KELURAHAN'
							                             AND TGL_PEMBAYARAN_SPPT IS NOT NULL
							                    GROUP BY A.KD_KECAMATAN,
							                             GET_KECAMATAN (A.KD_KECAMATAN),
							                             A.KD_KELURAHAN,
							                             GET_KELURAHAN (A.KD_KELURAHAN, A.KD_KECAMATAN),
							                             A.BUKU) B
							            ON     A.KD_KECAMATAN = B.KD_KECAMATAN
							               AND A.KD_KELURAHAN = B.KD_KELURAHAN
							               AND A.BUKU = B.BUKU
							         LEFT JOIN (  SELECT A.KD_KECAMATAN,
							                             GET_KECAMATAN (A.KD_KECAMATAN) NAMA_KEC,
							                             A.KD_KELURAHAN,
							                             GET_KELURAHAN (A.KD_KELURAHAN, A.KD_KECAMATAN)
							                                NM_KEL,
							                             A.BUKU,
							                             COUNT (PBB) SPPT_BAKU,
							                             SUM (PBB) JUMLAH_BAKU
							                        FROM MV_DHKP_DESA_TAHUN A
							                       WHERE     A.THN_PAJAK_SPPT = $tahun
							                             AND KD_KECAMATAN = '$KD_KECAMATAN'
							                             AND KD_KELURAHAN = '$KD_KELURAHAN'
							                             AND TGL_PEMBAYARAN_SPPT IS NOT NULL
							                    GROUP BY A.KD_KECAMATAN,
							                             GET_KECAMATAN (A.KD_KECAMATAN),
							                             A.KD_KELURAHAN,
							                             GET_KELURAHAN (A.KD_KELURAHAN, A.KD_KECAMATAN),
							                             A.BUKU) C
							            ON     A.KD_KECAMATAN = C.KD_KECAMATAN
							               AND A.KD_KELURAHAN = C.KD_KELURAHAN
							               AND A.BUKU = C.BUKU
							ORDER BY A.BUKU ASC")->result();
        }

        $data = array(
            'judul'        => 'Ringkasan Baku Tahun ' . $tahun,
            'data'         => $query,
            'kec'          => $kec,
            'kel'          => $kel,
            'KD_KELURAHAN' => $KD_KELURAHAN,
            'KD_KECAMATAN' => $KD_KECAMATAN,
            'tahun'        => $tahun,
        );

        $this->template->load('template', 'baku/ringkasanbaku', $data);
    }

    function dataDHKP()
    {
        $th = date('Y');
        if (isset($_GET['TAHUN'])) {
            $tahun = $_GET['TAHUN'];
        } else {
            $tahun = date('Y');
        }
        $kec = $this->db->query("SELECT KD_KECAMATAN,NM_KECAMATAN FROM REF_KECAMATAN order by kd_kecamatan asc")->result();
        $KD_KECAMATAN = $this->input->get('KD_KECAMATAN', true);
        $KD_KELURAHAN = $this->input->get('KD_KELURAHAN', true);

        $and = "";
        if ($KD_KECAMATAN <> '') {
            $and .= " and  substr(nop,7,3)='" . $KD_KECAMATAN . "'";
        }

        if ($KD_KELURAHAN <> '') {
            $and .= " and substr(nop,11,3)='" . $KD_KELURAHAN . "'";
        }

        if ($KD_KECAMATAN <> '') {
            $kel = $this->db->query("select kd_kelurahan,nm_kelurahan
                                    from ref_kelurahan
                                    where kd_kecamatan='$KD_KECAMATAN' order by kd_kecamatan asc,kd_kelurahan asc")->result();
        } else {
            $kel = null;
        }

        // if($KD_KECAMATAN<>'' || $KD_KELURAHAN<>''){

        $query = $this->db->query("select * from dhkp_buku_345 where thn_pajak_sppt=$tahun $and order by nop asc")->result();
        /*}else{
            $query=[];
        }*/


        $data = array(
            'judul'        => 'DHKP buku 3,4,5 Tahun ' . $tahun,
            'data'         => $query,
            'kec'          => $kec,
            'kel'          => $kel,
            'KD_KELURAHAN' => $KD_KELURAHAN,
            'KD_KECAMATAN' => $KD_KECAMATAN,
            'tahun'        => $tahun,
        );
        $this->template->load('template', 'baku/viewdataDHKP', $data);
    }

    function DownloadDhkpDesa()
    {
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', 3000);

        if (isset($_GET['tgl1'])) {
            $tgl1       = $_GET['tgl1'];
        } else {
            $tgl1 = date('d-m-Y');
        }

        // $this->db->where("TO_DATE(TO_CHAR(TGL_PERFORASI,'dd/mm/yyyy'),'dd/mm/yyyy') = TO_DATE('$q','DD-MM-YYYY')");
        // $exp=explode('-',$tgl1);
        /*$kec=$this->db->query("SELECT A.*, B.NM_KECAMATAN FROM P_DHKP_KEC_FILE A  JOIN REF_KECAMATAN B ON A.KD_KECAMATAN=B.KD_KECAMATAN WHERE TO_DATE(TO_CHAR(TANGGAL,'dd/mm/yyyy'),'dd/mm/yyyy') = TO_DATE('$tgl1','DD-MM-YYYY')")->result();*/
        $exp = explode('-', $tgl1);


        $kec = $this->db->query("SELECT A.*, B.NM_KECAMATAN FROM P_DHKP_KEC_FILE A  JOIN REF_KECAMATAN B ON A.KD_KECAMATAN=B.KD_KECAMATAN WHERE TO_DATE(TO_CHAR(TANGGAL,'dd/mm/yyyy'),'dd/mm/yyyy') = TO_DATE('$tgl1','DD-MM-YYYY')
            and file_dhkp like '%" . $exp[2] . $exp[1] . $exp[0] . "%'
            ")->result();


        /*
  echo $this->db->last_query();
  die();*/
        $data = array(
            'judul'        => 'Download DHKP Desa',
            'data'         => $kec,
            'tgl1'         => $tgl1
        );
        $this->template->load('template', 'baku/viewdownloaddhkpdesa', $data);
    }

    function tes()
    {
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', 30000);
        $out = shell_exec('cd C:\xampp\htdocs\layanan200\PythonDHKP && myenv\Scripts\activate.bat && python export.py');
        echo $out;
    }
    function dhkpDesa()
    {
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', 3000);

        $th = date('Y');
        if (isset($_GET['TAHUN'])) {
            $tahun = $_GET['TAHUN'];
        } else {
            $tahun = date('Y');
        }
        $kec = $this->db->query("SELECT KD_KECAMATAN,NM_KECAMATAN FROM REF_KECAMATAN  order by kd_kecamatan asc")->result();
        $KD_KECAMATAN = $this->input->get('KD_KECAMATAN', true);
        $KD_KELURAHAN = $this->input->get('KD_KELURAHAN', true);
        $BUKU = $this->input->get('BUKU', true);
        $and = '';
        if ($KD_KECAMATAN <> '') {
            $and = "and kd_kecamatan='$KD_KECAMATAN'";
            $kel = $this->db->query("select kd_kelurahan,nm_kelurahan
                                    from ref_kelurahan
                                    where kd_kecamatan='$KD_KECAMATAN' order by kd_kecamatan asc,kd_kelurahan asc")->result();
        } else {
            $kel = null;
        }


        if ($KD_KELURAHAN <> '') {
            $and .= " and kd_kelurahan='$KD_KELURAHAN'";
        }

        $b1 = "";
        $b2 = "";
        $b3 = "";
        $b4 = "";
        $b5 = "";
        $filter_buku = "";

        if ($BUKU <> '') {
            $w_buku = "";
            if (count($BUKU) == 0) {
            } else {

                for ($i = 0; $i < count($BUKU); $i++) {
                    $w_buku .= "'" . $BUKU[$i] . "',";
                    if ($BUKU[$i] == 'Buku I') {
                        $b1 = $BUKU[$i];
                    } else if ($BUKU[$i] == 'Buku II') {
                        $b2 = $BUKU[$i];
                    } else if ($BUKU[$i] == 'Buku III') {
                        $b3 = $BUKU[$i];
                    } else if ($BUKU[$i] == 'Buku IV') {
                        $b4 = $BUKU[$i];
                    } else {
                        $b5 = $BUKU[$i];
                    }
                }


                $r_buku = rtrim($w_buku, ',');
                $and .= " and buku in(" . $r_buku . ")";
                $filter_buku = $r_buku;
            }
        }





        $sql = "select * from mv_dhkp_desa where thn_pajak_sppt=$tahun $and";
        // echo $sql;
        /*echo $sql;
        exit();*/
        /*


            die(); */
        if ($KD_KECAMATAN <> '' || $KD_KELURAHAN <> '') {
            $dd = $this->db->query($sql)->result();
        } else {
            $dd = [];
        }

        $data = array(
            'judul'        => 'DHKP atau Update BAKU Tahun ' . $tahun,
            'kec'          => $kec,
            'kel'          => $kel,
            'KD_KECAMATAN' => $KD_KECAMATAN,
            'KD_KELURAHAN' => $KD_KELURAHAN,
            'data'         => $dd,
            'tahun'        => $tahun,
            'buku'         => array('Buku I', 'Buku II', 'Buku III', 'Buku IV', 'Buku V'),
            'b1'           => $b1,
            'b2'           => $b2,
            'b3'           => $b3,
            'b4'           => $b4,
            'b5'           => $b5,
            'filter_buku'  => $filter_buku,
        );
        $this->template->load('template', 'baku/viewdhkpdesa', $data);
    }

    function getkelurahan()
    {
        $KD_KECAMATAN = $this->input->post('kd_kec');
        $res = $this->db->query("select kd_kelurahan,nm_kelurahan
                                from ref_kelurahan
                                where kd_kecamatan='$KD_KECAMATAN' order by kd_kelurahan asc")->result();
        $option = "<option value=''>Pilih</option>";
        foreach ($res as $res) {
            $option .= "<option value='" . $res->KD_KELURAHAN . "'>" . $res->KD_KELURAHAN . ' ' . $res->NM_KELURAHAN . "</option>";
        }

        echo $option;
    }


    function cetakRingkasanbaku()
    {
        $th = date('Y');
        if (isset($_GET['TAHUN'])) {
            $tahun = $_GET['TAHUN'];
        } else {
            $tahun = date('Y');
        }
        $KD_KECAMATAN = $this->input->get('KD_KECAMATAN', true);
        $KD_KELURAHAN = $this->input->get('KD_KELURAHAN', true);


        if ($KD_KELURAHAN == '' || $KD_KECAMATAN == '') {
            redirect($_SERVER['HTTP_REFERER']);
        }


        $where = "";

        if ($KD_KECAMATAN <> '') {
            $where = "where a.KD_KECAMATAN='" . $KD_KECAMATAN . "'";
        }

        if ($KD_KELURAHAN <> '') {
            $where .= " and a.KD_KELURAHAN='" . $KD_KELURAHAN . "'";
        }

        /*$kecamatan=$this->db->query("select  distinct kd_kecamatan,nama_kec from (
                                    SELECT A.KD_KECAMATAN,GET_KECAMATAN(A.KD_KECAMATAN) NAMA_KEC,A.KD_KELURAHAN , GET_KELURAHAN(A.KD_KELURAHAN,A.KD_KECAMATAN) NM_KEL,A.BUKU,SPPT_BAKU,JUMLAH_BAKU,SPPT_REALISASI,JUMLAH_REALISASI,SPPT_PIUTANG,JUMLAH_PIUTANG
                                        FROM MASTER_BUKU A
                                    LEFT JOIN   (SELECT * FROM  MV_BAKU_REALISASI WHERE  THN_PAJAK_SPPT=$tahun ) B ON (  A.KD_KECAMATAN=B.KD_KECAMATAN AND A.KD_KELURAHAN=B.KD_KELURAHAN AND A.BUKU=B.BUKU)
                                    $where
                                    ORDER BY A.KD_KECAMATAN ASC,A.KD_KELURAHAN ASC,A.BUKU ASC
                                )")->result();*/
        $header = ['Kecamatan', 'Kelurahan', 'Buku', 'Jumlah Baku', 'TotalBaku', 'Jumlah realisasi', 'Total Realisasi', 'Jumlah Piutang', 'Total Piutang'];

        // setup Spout Excel Writer, set tipenya xlsx
        $writer = WriterFactory::create(Type::XLSX);
        // download to browser
        $writer->openToBrowser('ringkasan-baku.xlsx');
        // set style untuk header
        $headerStyle = (new StyleBuilder())
            ->setFontBold()
            ->build();

        // $first=1;
        // foreach($kecamatan as $kec){


        $sql = $this->db->query("SELECT A.*,
							         B.SPPT_BAKU SPPT_REALISASI,
							         B.JUMLAH_BAKU JUMLAH_REALISASI,
							         C.SPPT_BAKU SPPT_PIUTANG,
							         B.JUMLAH_BAKU JUMLAH_PIUTANG
							    FROM (  SELECT A.KD_KECAMATAN,
							                   GET_KECAMATAN (A.KD_KECAMATAN) NAMA_KEC,
							                   A.KD_KELURAHAN,
							                   GET_KELURAHAN (A.KD_KELURAHAN, A.KD_KECAMATAN) NM_KEL,
							                   A.BUKU,
							                   COUNT (PBB) SPPT_BAKU,
							                   SUM (PBB) JUMLAH_BAKU
							              FROM MV_DHKP_DESA_TAHUN A
							             WHERE     A.THN_PAJAK_SPPT = $tahun
							                   AND KD_KECAMATAN = '$KD_KECAMATAN'
							                   AND KD_KELURAHAN = '$KD_KELURAHAN'
							          GROUP BY A.KD_KECAMATAN,
							                   GET_KECAMATAN (A.KD_KECAMATAN),
							                   A.KD_KELURAHAN,
							                   GET_KELURAHAN (A.KD_KELURAHAN, A.KD_KECAMATAN),
							                   A.BUKU) A
							         LEFT JOIN (  SELECT A.KD_KECAMATAN,
							                             GET_KECAMATAN (A.KD_KECAMATAN) NAMA_KEC,
							                             A.KD_KELURAHAN,
							                             GET_KELURAHAN (A.KD_KELURAHAN, A.KD_KECAMATAN)
							                                NM_KEL,
							                             A.BUKU,
							                             COUNT (PBB) SPPT_BAKU,
							                             SUM (PBB) JUMLAH_BAKU
							                        FROM MV_DHKP_DESA_TAHUN A
							                       WHERE     A.THN_PAJAK_SPPT = $tahun
							                             AND KD_KECAMATAN = '$KD_KECAMATAN'
							                             AND KD_KELURAHAN = '$KD_KELURAHAN'
							                             AND TGL_PEMBAYARAN_SPPT IS NOT NULL
							                    GROUP BY A.KD_KECAMATAN,
							                             GET_KECAMATAN (A.KD_KECAMATAN),
							                             A.KD_KELURAHAN,
							                             GET_KELURAHAN (A.KD_KELURAHAN, A.KD_KECAMATAN),
							                             A.BUKU) B
							            ON     A.KD_KECAMATAN = B.KD_KECAMATAN
							               AND A.KD_KELURAHAN = B.KD_KELURAHAN
							               AND A.BUKU = B.BUKU
							         LEFT JOIN (  SELECT A.KD_KECAMATAN,
							                             GET_KECAMATAN (A.KD_KECAMATAN) NAMA_KEC,
							                             A.KD_KELURAHAN,
							                             GET_KELURAHAN (A.KD_KELURAHAN, A.KD_KECAMATAN)
							                                NM_KEL,
							                             A.BUKU,
							                             COUNT (PBB) SPPT_BAKU,
							                             SUM (PBB) JUMLAH_BAKU
							                        FROM MV_DHKP_DESA_TAHUN A
							                       WHERE     A.THN_PAJAK_SPPT = $tahun
							                             AND KD_KECAMATAN = '$KD_KECAMATAN'
							                             AND KD_KELURAHAN = '$KD_KELURAHAN'
							                             AND TGL_PEMBAYARAN_SPPT IS NOT NULL
							                    GROUP BY A.KD_KECAMATAN,
							                             GET_KECAMATAN (A.KD_KECAMATAN),
							                             A.KD_KELURAHAN,
							                             GET_KELURAHAN (A.KD_KELURAHAN, A.KD_KECAMATAN),
							                             A.BUKU) C
							            ON     A.KD_KECAMATAN = C.KD_KECAMATAN
							               AND A.KD_KELURAHAN = C.KD_KELURAHAN
							               AND A.BUKU = C.BUKU
							ORDER BY A.BUKU ASC")->result();
        $arrisi = array();


        $SPPT_BAKU        = 0;
        $JUMLAH_BAKU      = 0;
        $SPPT_REALISASI   = 0;
        $JUMLAH_REALISASI = 0;
        $SPPT_PIUTANG     = 0;
        $JUMLAH_PIUTANG   = 0;
        foreach ($sql as $rk) {
            $SPPT_BAKU += $rk->SPPT_BAKU;
            $JUMLAH_BAKU += $rk->JUMLAH_BAKU;
            $SPPT_REALISASI += $rk->SPPT_REALISASI;
            $JUMLAH_REALISASI += $rk->JUMLAH_REALISASI;
            $SPPT_PIUTANG += $rk->SPPT_PIUTANG;
            $JUMLAH_PIUTANG += $rk->JUMLAH_PIUTANG;
            $ff = array(
                $rk->NAMA_KEC, $rk->NM_KEL, $rk->BUKU,
                (int) $rk->SPPT_BAKU,
                (int) $rk->JUMLAH_BAKU,
                (int) $rk->SPPT_REALISASI,
                (int) $rk->JUMLAH_REALISASI,
                (int) $rk->SPPT_PIUTANG,
                (int) $rk->JUMLAH_PIUTANG
            );
            array_push($arrisi, $ff);
        }
        $ffa = array(
            '', '', '',
            number_format($SPPT_BAKU, 0, '', '.'),
            number_format($JUMLAH_BAKU, 0, '', '.'),
            number_format($SPPT_REALISASI, 0, '', '.'),
            number_format($JUMLAH_REALISASI, 0, '', '.'),
            number_format($SPPT_PIUTANG, 0, '', '.'),
            number_format($JUMLAH_PIUTANG, 0, '', '.')
        );
        array_push($arrisi, $ffa);


        /* echo "<pre>";
                            print_r($arrisi);*/

        $writer->getCurrentSheet()->setName('Ringkasan Baku');
        $writer->addRowWithStyle($header, $headerStyle);
        $writer->addRows($arrisi); // tambahkan row untuk data anggota

        // if($first==1){
        // write ke Sheet pertama
        /*                $writer->getCurrentSheet()->setName('Ringkasan Baku');
                // header Sheet pertama
                $writer->addRowWithStyle($header, $headerStyle);
                // data Sheet pertama
                $writer->addRows($arrisi);*/
        /*}else{
                // write ke Sheet kedua
                $writer->addNewSheetAndMakeItCurrent()->setName($kec->KD_KECAMATAN.' '.$kec->NAMA_KEC);
                // header Sheet kedua
                $writer->addRowWithStyle($header, $headerStyle);
                // data Sheet pertama
                $writer->addRows($arrisi);
            }*/
        // $first++;
        // }

        // close writter
        $writer->close();
    }

    function cetakBakuDHKP()
    {
        $th = date('Y');
        if (isset($_GET['TAHUN'])) {
            $tahun = $_GET['TAHUN'];
        } else {
            $tahun = date('Y');
        }
        $KD_KECAMATAN = $this->input->get('KD_KECAMATAN', true);
        $KD_KELURAHAN = $this->input->get('KD_KELURAHAN', true);

        $and = "";
        if ($KD_KECAMATAN <> '') {
            $and .= " and  substr(nop,7,3)='" . $KD_KECAMATAN . "'";
        }

        if ($KD_KELURAHAN <> '') {
            $and .= " and substr(nop,11,3)='" . $KD_KELURAHAN . "'";
        }


        $header = ['NOP', 'Alamat OP', 'RW / RT OP', 'Kecamatan', 'Luas Bumi', 'Luas Bangunan', 'Nama WP', 'Alamat WP', 'RW / RT WP', 'Kelurahan WP', 'Kota WP', 'PBB', 'Terbit SPPT', 'Lunas', 'Pembayaran SPPT',];

        // setup Spout Excel Writer, set tipenya xlsx
        $writer = WriterFactory::create(Type::XLSX);
        // download to browser
        $writer->openToBrowser('DHKPbuku-345.xlsx');
        // set style untuk header
        $headerStyle = (new StyleBuilder())
            ->setFontBold()
            ->build();

        $first = 1;

        $query = $this->db->query("select a.*,get_kecamatan(substr(nop,7,3)) nm_kec from dhkp_buku_345 a where thn_pajak_sppt=$tahun $and order by nop asc")->result();
        $arrisi = array();

        $PBB = 0;

        foreach ($query as $rk) {
            $PBB += $rk->PBB;
            $lunas = $rk->TGL_PEMBAYARAN_SPPT <> '' ? 'Lunas' : 'Belum';
            $ff = array($rk->NOP, $rk->ALAMAT_OP, $rk->RWRT_OP, $rk->NM_KEC, $rk->LUAS_BUMI_SPPT, $rk->LUAS_BNG_SPPT, $rk->NM_WP_SPPT, $rk->ALAMAT_WP, $rk->RWRT_WP, $rk->KELURAHAN_WP_SPPT, $rk->KOTA_WP_SPPT, (int) $rk->PBB, $rk->TGL_TERBIT_SPPT, $lunas, $rk->TGL_PEMBAYARAN_SPPT);
            array_push($arrisi, $ff);
        }

        $ffa = array('', '', '', '', '', '', '', '', '', '', '', number_format($PBB, 0, '', '.'), '', '', '');
        array_push($arrisi, $ffa);

        // write ke Sheet pertama
        $writer->getCurrentSheet()->setName("Buku 3,4,5");
        // header Sheet pertama
        $writer->addRowWithStyle($header, $headerStyle);
        // data Sheet pertama
        $writer->addRows($arrisi);


        // close writter
        $writer->close();
    }

    function cetakBakuDHKPdesa()
    {
        ini_set('memory_limit', '-1');
        $th = date('Y');
        if (isset($_GET['TAHUN'])) {
            $tahun = $_GET['TAHUN'];
        } else {
            $tahun = date('Y');
        }

        $KD_KECAMATAN = $this->input->get('KD_KECAMATAN', true);
        $KD_KELURAHAN = $this->input->get('KD_KELURAHAN', true);
        $BUKU         = $this->input->get('BUKU', true);



        $and = '';
        // if($KD_KECAMATAN<>''){
        $and .= " and kd_kecamatan='$KD_KECAMATAN'";
        // }

        if ($KD_KELURAHAN <> '') {
            $and .= "and kd_kelurahan='$KD_KELURAHAN'";
        }


        $filter_buku = "";
        if ($BUKU <> 'null') {
            $str_buku = explode(",", $BUKU);
            $w_buku = "";
            if (count($str_buku) == 0) {
            } else {
                if (isset($_GET['CARI'])) {
                    for ($i = 0; $i < count($str_buku); $i++) {
                        $w_buku .= "" . $str_buku[$i] . ",";
                    }
                } else {
                    for ($i = 0; $i < count($str_buku); $i++) {
                        $w_buku .= "'" . $str_buku[$i] . "',";
                    }
                }

                $r_buku = rtrim($w_buku, ',');
                $and .= " and buku in(" . $r_buku . ")";
                $filter_buku = " and buku in(" . $r_buku . ")";
            }
        }



        $header = ['NOP', 'Alamat OP', 'RW / RT OP', 'Luas Bumi', 'Luas Bangunan', 'Nama', 'Alamat WP', 'RW / RT WP', 'Kelurahan WP', 'Kota WP', 'PBB Awal', 'PBB Akhir', 'Tahun', 'Terbit SPPT', 'Lunas', 'Bayar SPPT', 'Keterangan'];

        $desa = "select nm_kelurahan nama_kel,kd_kelurahan,kd_kecamatan
          from ref_kelurahan
          where kd_kecamatan=$KD_KECAMATAN and kd_kelurahan in (select kd_kelurahan from mv_dhkp_desa where thn_pajak_sppt=$tahun $and ) order by to_number(kd_kelurahan) asc";

        $des = $this->db->query($desa)->result();
        /*echo $this->db->last_query();
          die();*/

        // nama desa/kecamatan
        $cbv = $this->db->query("select get_kecamatan('$KD_KECAMATAN') kec from dual")->row();
        $nm_kec = $cbv->KEC;

        $cba = $this->db->query("select get_kelurahan('$KD_KELURAHAN','$KD_KECAMATAN') kel from dual")->row();
        $nm_kel = $cba->KEL;




        // setup Spout Excel Writer, set tipenya xlsx
        $writer = WriterFactory::create(Type::XLSX);
        // download to browser

        // set style untuk header
        $headerStyle = (new StyleBuilder())
            ->setFontBold()
            ->build();



        $writer->setTempFolder('tmp/'); //define temporary folder yg akan digunakan untuk menampung hasil file yg ditulis sementara
        $namaFile = date('YmdHis') . 'DHKP-' . $KD_KECAMATAN . ' ' . $nm_kec . '_' . $KD_KELURAHAN . ' ' . $nm_kel . '.xlsx'; //nama filenya
        $filePath = 'tmp/' . $namaFile;

        $defaultStyle = (new StyleBuilder())
            ->setFontName('Arial')
            ->setFontSize(10)
            ->setShouldWrapText(false)
            ->build();
        $writer->setDefaultRowStyle($defaultStyle)
            ->openToFile($filePath);

        $first = 1;
        foreach ($des as $kec) {
            $anddua = " and KD_KECAMATAN='" . $kec->KD_KECAMATAN . "' and kd_kelurahan='" . $kec->KD_KELURAHAN . "' " . $filter_buku;

            $sql = "select *   from mv_dhkp_desa where thn_pajak_sppt=$tahun $anddua ";

            $query  = $this->db->query($sql)->result();
            /*echo $this->db->last_query();
             echo "<hr>";*/
            $arrisi = array();

            $pbb = 0;
            foreach ($query as $rk) {
                $pbb += $rk->PBB;
                $lunas = $rk->TGL_PEMBAYARAN_SPPT <> '' ? 'Lunas' : 'Belum';
                $ff = array($rk->NOP, $rk->ALAMAT_OP, $rk->RWRT_OP, $rk->LUAS_BUMI_SPPT, $rk->LUAS_BNG_SPPT, $rk->NM_WP_SPPT, $rk->ALAMAT_WP, $rk->RWRT_WP, $rk->KELURAHAN_WP_SPPT, $rk->KOTA_WP_SPPT, null, (int) $rk->PBB, $rk->THN_PAJAK_SPPT, $rk->TGL_TERBIT_SPPT, $lunas, $rk->TGL_PEMBAYARAN_SPPT, $rk->KET);
                array_push($arrisi, $ff);
            }
            $ffa = array('', '', '', '', '', '', '', '', '', '', null, number_format((int) $pbb, 0, '', '.'), '', '', '', '');
            array_push($arrisi, $ffa);

            /*echo "<pre>";
                print_r($arrisi);
                echo "</pre>";*/

            if ($first == 1) {
                // write ke Sheet pertama
                $writer->getCurrentSheet()->setName($kec->KD_KELURAHAN . ' ' . $kec->NAMA_KEL);
                // header Sheet pertama
                $writer->addRowWithStyle($header, $headerStyle);
                // data Sheet pertama
                $writer->addRows($arrisi);
            } else {
                // write ke Sheet kedua
                $writer->addNewSheetAndMakeItCurrent()->setName($kec->KD_KELURAHAN . ' ' . $kec->NAMA_KEL);
                // header Sheet kedua
                $writer->addRowWithStyle($header, $headerStyle);
                // data Sheet pertama
                $writer->addRows($arrisi);
            }


            $first++;
        }


        // close writter
        $writer->close();
        $this->load->helper('download');
        force_download($filePath, null);
    }
}
