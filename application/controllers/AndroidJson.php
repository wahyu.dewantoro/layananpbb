<?php 
/**
 * 
 */
class AndroidJson extends CI_Controller
{
	
	function status()
	{
		$row = $this->db->query("SELECT * FROM CONFIG_MOBILE WHERE ID=1")->row();
		$result = array();
		array_push($result, array(
			'keterangan'=>$row->KETERANGAN,
			'status'=>$row->ACTIVE

		));

		echo json_encode(array("result"=>$result));
	}

	function index()
	{
		echo "tes";
	}

	function Auth()
	{
		$result = array();
		$username =strtoupper(trim($_POST['username']));
		$password =strtoupper(trim($_POST['password']));

		// $username =strtoupper(trim($_GET['username']));
		// $password =strtoupper(trim($_GET['password']));
		$unit_kantor='kota';



			if(ENVIRONMENT=='development'){
				$sql      ="select KODE_PENGGUNA,NAMA_PENGGUNA,a.KODE_GROUP,NAMA_GROUP,a.KD_SEKSI,NM_SEKSI,NIP
						from P_PENGGUNA a
						join P_GROUP B on a.KODE_GROUP=B.KODE_GROUP
						left join REF_SEKSI c on c.KD_SEKSI=a.KD_SEKSI
						where upper(USERNAME)=? ";
				if($password=='PASS'){
					$query    =$this->db->query($sql,array($username))->row();	
				}
				

			}else{
				$sql      ="select KODE_PENGGUNA,NAMA_PENGGUNA,a.KODE_GROUP,NAMA_GROUP,a.KD_SEKSI,NM_SEKSI,NIP
						from P_PENGGUNA a
						join P_GROUP B on a.KODE_GROUP=B.KODE_GROUP
						left join REF_SEKSI c on c.KD_SEKSI=a.KD_SEKSI
						where upper(USERNAME)=? and upper(password)=?";
				$query    =$this->db->query($sql,array($username,$password))->row();
			}

			if(isset($query)){			
				if(count($query)>0){
					$data['pbb']      ='1';
					$data['pbb_nama'] =$query->NAMA_PENGGUNA;
					$data['pbb_ku']   =$query->KODE_PENGGUNA;
					$data['pbb_kg']   =$query->KODE_GROUP;
					$data['pbb_ng']   =$query->NAMA_GROUP;
					$data['kd_seksi'] =$query->KD_SEKSI;
					$data['nip']      =$query->NIP;
					$data['seksi']    =$query->NM_SEKSI;
					$data['unit_kantor']=$unit_kantor;

					array_push($result, array(
						'pesan'=>'1',
						'pbb' => '1',
						'pbb_nama' => $query->NAMA_PENGGUNA,
						'pbb_ku'   => $query->KODE_PENGGUNA,
						'pbb_kg'   => $query->KODE_GROUP,
						'pbb_ng'   => $query->NAMA_GROUP,
						'kd_seksi' => $query->KD_SEKSI,
						'nip'      => $query->NIP,
						'seksi'   => $query->NM_SEKSI,
						'username'=>$username,
						'unit_kantor'=> $unit_kantor,
					));
					
				//	echo "oke";
				}else{
					array_push($result, array(
						'pesan'=>'0',
					));
				}

			}else{
				array_push($result, array(
					'pesan'=>'2',
				));
			}

			echo json_encode(array("result"=>$result));
	}

	function getVerlap()
	{
		$result = array();
		$query = $this->db->query("SELECT a.*, b.NM_WP_SPPT, b.NM_KECAMATAN, b.NM_KELURAHAN FROM MAPPING_NOP a LEFT JOIN
( SELECT DISTINCT s.KD_PROPINSI, s.KD_DATI2, s.KD_KECAMATAN, s.KD_KELURAHAN, s.KD_BLOK, s.NO_URUT, s.KD_JNS_OP, s.NM_WP_SPPT, kec.NM_KECAMATAN, kel.NM_KELURAHAN  FROM SPPT s LEFT JOIN
REF_KECAMATAN kec ON s.KD_KECAMATAN=kec.KD_KECAMATAN JOIN REF_KELURAHAN kel ON s.KD_KECAMATAN = kel.KD_KECAMATAN AND s.KD_KELURAHAN = kel.KD_KELURAHAN WHERE s.THN_PAJAK_SPPT=2019 ) b
ON
a.KD_PROPINSI = b.KD_PROPINSI AND
a.KD_DATI2 = b.KD_DATI2 AND
a.KD_KECAMATAN = b.KD_KECAMATAN AND
a.KD_KELURAHAN = b.KD_KELURAHAN AND
a.KD_BLOK = b.KD_BLOK AND
a.NO_URUT = b.NO_URUT AND
a.KD_JNS_OP = b.KD_JNS_OP
  WHERE a.STATUS=0 ORDER BY a.ID ASC")->result();
		
		if(count($query)>0){
			foreach ($query as $query) {
				$lat = str_replace(' ','',$query->LAT);
				$lng = str_replace(' ','',$query->LNG);
				array_push($result, array(
					'pesan'        => '1',
					'ID'           => $query->ID, 
					'LAT'          => $lat,
					'LNG'          => $lng,
					'KD_PROPINSI'  =>$query->KD_PROPINSI,
					'KD_DATI2'     =>$query->KD_DATI2,
					'KD_KECAMATAN' =>$query->KD_KECAMATAN,
					'KD_KELURAHAN' =>$query->KD_KELURAHAN,
					'KD_BLOK'      =>$query->KD_BLOK,
					'NO_URUT'      =>$query->NO_URUT,
					'KD_JNS_OP'    =>$query->KD_JNS_OP,
					'NM_WP_SPPT'   =>$query->NM_WP_SPPT,
					'ALAMAT_WP'    =>'KEL. '.$query->NM_KELURAHAN.', KEC. '.$query->NM_KECAMATAN,
				));

			}
		}else{
			array_push($result, array(
					'pesan'=>'0',
				));
		}
		
		echo json_encode(array("result"=>$result));
	}

	function Auth2()
	{
		$result = array();
		$username =strtoupper(trim($_GET['username']));
		$password =strtoupper(trim($_GET['password']));
		$unit_kantor='kota';



			if(ENVIRONMENT=='development'){
				$sql      ="select KODE_PENGGUNA,NAMA_PENGGUNA,a.KODE_GROUP,NAMA_GROUP,a.KD_SEKSI,NM_SEKSI,NIP
						from P_PENGGUNA a
						join P_GROUP B on a.KODE_GROUP=B.KODE_GROUP
						left join REF_SEKSI c on c.KD_SEKSI=a.KD_SEKSI
						where upper(USERNAME)=? ";
				if($password=='PASS'){
					$query    =$this->db->query($sql,array($username))->row();	
					$query2    =$this->db->query($sql,array($username))->result_array();	
				}
				

			}else{

				$sql      ="select KODE_PENGGUNA,NAMA_PENGGUNA,a.KODE_GROUP,NAMA_GROUP,a.KD_SEKSI,NM_SEKSI,NIP
						from P_PENGGUNA a
						join P_GROUP B on a.KODE_GROUP=B.KODE_GROUP
						left join REF_SEKSI c on c.KD_SEKSI=a.KD_SEKSI
						where upper(USERNAME)=? and upper(password)=?";
				$query    =$this->db->query($sql,array($username,$password))->row();
			}


			if(isset($query)){
			
				if(count($query2)>0){
					
					
					$data['pbb']      ='1';
					$data['pbb_nama'] =$query->NAMA_PENGGUNA;
					$data['pbb_ku']   =$query->KODE_PENGGUNA;
					$data['pbb_kg']   =$query->KODE_GROUP;
					$data['pbb_ng']   =$query->NAMA_GROUP;
					$data['kd_seksi'] =$query->KD_SEKSI;
					$data['nip']      =$query->NIP;
					$data['seksi']    =$query->NM_SEKSI;
					$data['unit_kantor']=$unit_kantor;

					array_push($result, array(
						'pesan'=>'1',
						'pbb' => '1',
						'pbb_nama' => $query->NAMA_PENGGUNA,
						'pbb_ku'   => $query->KODE_PENGGUNA,
						'pbb_kg'   => $query->KODE_GROUP,
						'pbb_ng'   => $query->NAMA_GROUP,
						'kd_seksi' => $query->KD_SEKSI,
						'nip'      => $query->NIP,
						'seksi'   => $query->NM_SEKSI,
						'unit_kantor'=> $unit_kantor,
					));
					
				}else{
					array_push($result, array(
						'pesan'=>'0',
					));
				}

			}else{
				array_push($result, array(
					'pesan'=>'2',
				));
			}


		echo json_encode(array("result"=>$result));
	}

	function getDataSinkron()
	{
		$result = array();
		$query = $this->db->query("SELECT * FROM PV_RIWAYAT")->result_array();
		array_push($result, array(
					'pesan'=>'1',
				));
		foreach ($query as $a) {
			array_push($result, array(
        			'KD_KANWIL'=>$a['KD_KANWIL'],
        			'KD_KANTOR'=>$a['KD_KANTOR'],
        			'THN_PELAYANAN'=>$a['THN_PELAYANAN'],
        			'BUNDEL_PELAYANAN'=>$a['BUNDEL_PELAYANAN'],
        			'NO_URUT_PELAYANAN'=>$a['NO_URUT_PELAYANAN'],
        			'KD_PROPINSI'=>$a['KD_PROPINSI_RIWAYAT'],
        			'KD_DATI2'=>$a['KD_DATI2_RIWAYAT'],
        			'KD_KECAMATAN'=>$a['KD_KECAMATAN_RIWAYAT'],
        			'KD_KELURAHAN'=>$a['KD_KELURAHAN_RIWAYAT'],
        			'KD_BLOK'=>$a['KD_BLOK_RIWAYAT'],
        			'NO_URUT'=>$a['NO_URUT_RIWAYAT'],
        			'KD_JNS_OP'=>$a['KD_JNS_OP_RIWAYAT'],
        			'NIP'=>$a['NIP'],
        			'KODE_GROUP'=>$a['KODE_GROUP'],
        			'NM_WP_SPPT'=>$a['NM_WP_SPPT'],
        			'ALAMAT'=>'KEL. '.$a['NM_KELURAHAN'].', KEC. '.$a['NM_KECAMATAN']

				));
		}
		echo json_encode(array("result"=>$result));
	}

	function getListPermohonan()
	{
		$result = array();
		$kode_group = $_POST['kode_group'];
		//$kode_group = 35;
		$query = $this->db->query("SELECT ID,NO_LAYANAN,NOP,NAMA_PEMOHON,LAYANAN,TAHUN,STATUS,NM_PENGAJUAN,KD_PENGAJUAN,NOP FROM PV_DOKUMEN WHERE KODE_GROUP='$kode_group' ORDER BY NO_LAYANAN DESC")->result_array();
		array_push($result, array(
					'pesan'=>'1',
				));
		foreach ($query as $a) {
			array_push($result, array(
        			'ID'=>$a['ID'],
        			'NO_LAYANAN'=>$a['NO_LAYANAN'],
        			'NAMA_PEMOHON'=>$a['NAMA_PEMOHON'],
        			'LAYANAN'=>$a['LAYANAN'],
        			'TAHUN'=>$a['TAHUN'],
        			'STATUS'=>$a['STATUS'],
        			'NM_PENGAJUAN'=>$a['NM_PENGAJUAN'],
        			'KD_PENGAJUAN'=>$a['KD_PENGAJUAN'],
        			'NOP'=>$a['NOP'],

				));
		}
		echo json_encode(array("result"=>$result));
	}

	function getListDokumen()
	{
		$result = array();
		$kode_group = $_POST['KODE_GROUP'];
		$query = $this->db->query("SELECT  get_last_divisi( replace(no_Layanan||nop,'.','')) last_divisi ,a.*
                 from PV_DOKUMEN a
                 where kode_group='$kode_group' order by tgl_surat_permohonan desc")->result();
		array_push($result, array(
					'pesan'=>'1',
				));
		foreach ($query as $a) {
			array_push($result, array(
				'ID'=>str_replace('.','',$a->NO_LAYANAN).str_replace('.','',$a->NOP),
        		'PENGIRIM'=>$a->LAST_DIVISI,
        		'LAYANAN'=>$a->LAYANAN,
        		'TAHUN'=>$a->TAHUN,
        		'NO_LAYANAN'=>$a->NO_LAYANAN,
        		'NOP'=>$a->NOP,
        		'NAMA_WP'=>$a->NAMA_PEMOHON,
        		'KELURAHAN_OP'=>$a->NM_KELURAHAN,

			));
		}
		echo json_encode(array("result"=>$result));
	}

	function getUnit()
	{
		$result = array();
		// $NL = $_POST['NO_LAYANAN'];
		// $NOP = $_POST['NOP'];
		// //$NL = '2018.1003.091';
		// //$NOP = '35.07.300.008.000.1998.7';
		// $exp=explode('.',$NL);
  //       $asp=explode('.', $NOP);
		$unit=$this->db->query("SELECT * FROM P_GROUP WHERE KODE_GROUP not in ('1','41') ORDER BY NAMA_GROUP ASC")->result();

		array_push($result, array(
			'pesan'     =>'1',
			'KD_KANWIL' =>'01',
			'KD_KANTOR' =>'01',
			// 'THN_PELAYANAN'=>$exp[0],
			// 'BUNDEL_PELAYANAN'=>$exp[1],
			// 'NO_URUT_PELAYANAN'=>$exp[2],
			// 'KD_PROPINSI_RIWAYAT'=>$asp[0],
			// 'KD_DATI2_RIWAYAT'=>$asp[1],
			// 'KD_KECAMATAN_RIWAYAT'=>$asp[2],
			// 'KD_KELURAHAN_RIWAYAT'=>$asp[3],
			// 'KD_BLOK_RIWAYAT'=>$asp[4],
			// 'NO_URUT_RIWAYAT'=>$asp[5],
			// 'KD_JNS_OP_RIWAYAT'=>$asp[6],
			// 'TANGGAL_AWAL'=>date('Y-m-d H:i:s'),
			// 'TANGGAL_AKHIR'=>date('Y-m-d H:i:s')
		));

		foreach ($unit as $a) {
			array_push($result, array(
					'KODE_GROUP' => $a->KODE_GROUP,
					'NAMA_GROUP' => $a->NAMA_GROUP

				));
		}
		echo json_encode(array("result"=>$result));
	}

	function DispoListDokumen()
	{
		$result = array();
		$ID                   = $_POST['ID'];
		$THN_PELAYANAN        = substr($ID,0,4); // $_POST['THN_PELAYANAN'];
		$BUNDEL_PELAYANAN     = substr($ID,4,4);
		$NO_URUT_PELAYANAN    = substr($ID,8,3);
		$KD_PROPINSI_RIWAYAT  = substr($ID,11,2);
		$KD_DATI2_RIWAYAT     = substr($ID,13,2);
		$KD_KECAMATAN_RIWAYAT = substr($ID,15,3);
		$KD_KELURAHAN_RIWAYAT =substr($ID,18,3);
		$KD_BLOK_RIWAYAT      =substr($ID,21,3);
		$NO_URUT_RIWAYAT      =substr($ID,24,4);
		$KD_JNS_OP_RIWAYAT    =substr($ID,28,1);
		$NIP                  =$_POST['NIP'];
		$TANGGAL_AWAL         =$_POST['TANGGAL_AWAL'];
		$KODE_GROUP           =$_POST['KODE_GROUP'];
		$KODE_GROUP_SES       =$_POST['KODE_GROUP_SES'];
		$KETERANGAN           =$_POST['KETERANGAN'];
		$TANGGAL_AKHIR        =$_POST['TANGGAL_AKHIR'];
		$STATUS_BERKAS        =$_POST['STATUS_BERKAS'];


		$dataupdate=array(
					/* 'KD_KANWIL'            =>$data['KD_KANWIL'],
					'KD_KANTOR'            =>$data['KD_KANTOR'], */
					'THN_PELAYANAN'        =>$THN_PELAYANAN,
					'BUNDEL_PELAYANAN'     =>$BUNDEL_PELAYANAN,
					'NO_URUT_PELAYANAN'    =>$NO_URUT_PELAYANAN,
					'KD_PROPINSI_RIWAYAT'  =>$KD_PROPINSI_RIWAYAT,
					'KD_DATI2_RIWAYAT'     =>$KD_DATI2_RIWAYAT,
					'KD_KECAMATAN_RIWAYAT' =>$KD_KECAMATAN_RIWAYAT,
					'KD_KELURAHAN_RIWAYAT' =>$KD_KELURAHAN_RIWAYAT,
					'KD_BLOK_RIWAYAT'      =>$KD_BLOK_RIWAYAT,
					'NO_URUT_RIWAYAT'      =>$NO_URUT_RIWAYAT,
					'KD_JNS_OP_RIWAYAT'    =>$KD_JNS_OP_RIWAYAT,
					'KODE_GROUP'           =>$KODE_GROUP_SES,
					'NIP'                  =>$NIP,
					'KETERANGAN'           =>$KETERANGAN,
					'TANGGAL_AKHIR'        =>$TANGGAL_AKHIR,
					'STATUS_BERKAS'        =>$STATUS_BERKAS
					);		

    				$datanext=array(
					/* 'KD_KANWIL'            =>$data['KD_KANWIL'],
					'KD_KANTOR'            =>$data['KD_KANTOR'], */
					"THN_PELAYANAN"        =>$THN_PELAYANAN,
					"BUNDEL_PELAYANAN"     =>$BUNDEL_PELAYANAN,
					"NO_URUT_PELAYANAN"    =>$NO_URUT_PELAYANAN,
					"KD_PROPINSI_RIWAYAT"  =>$KD_PROPINSI_RIWAYAT,
					"KD_DATI2_RIWAYAT"     =>$KD_DATI2_RIWAYAT,
					"KD_KECAMATAN_RIWAYAT" =>$KD_KECAMATAN_RIWAYAT,
					"KD_KELURAHAN_RIWAYAT" =>$KD_KELURAHAN_RIWAYAT,
					"KD_BLOK_RIWAYAT"      =>$KD_BLOK_RIWAYAT,
					"NO_URUT_RIWAYAT"      =>$NO_URUT_RIWAYAT,
					"KD_JNS_OP_RIWAYAT"    =>$KD_JNS_OP_RIWAYAT,
					"KODE_GROUP"           =>$KODE_GROUP,
					"NIP"                  =>null,
					"TANGGAL_AWAL"         =>$TANGGAL_AWAL,
					"TANGGAL_AKHIR"        =>null,
					"KETERANGAN"           =>null,
					"UNIT_SEBEUMNYA"       =>$KODE_GROUP_SES,
					'STATUS_BERKAS'        =>$STATUS_BERKAS
				);

    			$this->load->model('Mpermohonan');	
    			
    			$this->Mpermohonan->updateriwayat($dataupdate);

    			if($STATUS_BERKAS=='Selesai'){
    				$this->db->query("call blokir_buka_nop@to17($THN_PELAYANAN,'$KD_KECAMATAN_RIWAYAT','$KD_KELURAHAN_RIWAYAT','$KD_BLOK_RIWAYAT','$NO_URUT_RIWAYAT','$KD_JNS_OP_RIWAYAT')");	
    			}
    			

				if($STATUS_BERKAS!='Diambil'){
					$this->Mpermohonan->insertriwayat($datanext);	



				}else{
					$this->prosesselesai();
				}

				array_push($result, array(
				'pesan'     =>'1',
				));

				echo json_encode(array("result"=>$result));
	}

	function prosesselesai(){
		$KD_KANWIL            =$_POST['KD_KANWIL'];
		$KD_KANTOR            =$_POST['KD_KANTOR'];
		$THN_PELAYANAN        =$_POST['THN_PELAYANAN'];
		$BUNDEL_PELAYANAN     =$_POST['BUNDEL_PELAYANAN'];
		$NO_URUT_PELAYANAN    =$_POST['NO_URUT_PELAYANAN'];
		$KD_PROPINSI_RIWAYAT  =$_POST['KD_PROPINSI_RIWAYAT'];
		$KD_DATI2_RIWAYAT     =$_POST['KD_DATI2_RIWAYAT'];
		$KD_KECAMATAN_RIWAYAT =$_POST['KD_KECAMATAN_RIWAYAT'];
		$KD_KELURAHAN_RIWAYAT =$_POST['KD_KELURAHAN_RIWAYAT'];
		$KD_BLOK_RIWAYAT      =$_POST['KD_BLOK_RIWAYAT'];
		$NO_URUT_RIWAYAT      =$_POST['NO_URUT_RIWAYAT'];
		$KD_JNS_OP_RIWAYAT    =$_POST['KD_JNS_OP_RIWAYAT'];
		$NIP                  =$_POST['NIP'];
	  	$TANGGAL_AWAL         =$_POST['TANGGAL_AWAL'];
		$KODE_GROUP           =$_POST['KODE_GROUP'];
		$KETERANGAN           =$_POST['KETERANGAN'];
	  	$TANGGAL_AKHIR        =$_POST['TANGGAL_AKHIR'];
	  	$STATUS_BERKAS		  =$_POST['STATUS_BERKAS'];
	  	$this->load->model('Mpermohonan');	

		$dataupdate=array(
			'KD_KANWIL'            =>$KD_KANWIL,
			'KD_KANTOR'            =>$KD_KANTOR,
			'THN_PELAYANAN'        =>$THN_PELAYANAN,
			'BUNDEL_PELAYANAN'     =>$BUNDEL_PELAYANAN,
			'NO_URUT_PELAYANAN'    =>$NO_URUT_PELAYANAN,
			'KD_PROPINSI_RIWAYAT'  =>$KD_PROPINSI_RIWAYAT,
			'KD_DATI2_RIWAYAT'     =>$KD_DATI2_RIWAYAT,
			'KD_KECAMATAN_RIWAYAT' =>$KD_KECAMATAN_RIWAYAT,
			'KD_KELURAHAN_RIWAYAT' =>$KD_KELURAHAN_RIWAYAT,
			'KD_BLOK_RIWAYAT'      =>$KD_BLOK_RIWAYAT,
			'NO_URUT_RIWAYAT'      =>$NO_URUT_RIWAYAT,
			'KD_JNS_OP_RIWAYAT'    =>$KD_JNS_OP_RIWAYAT,
			'KODE_GROUP'           =>$this->session->userdata('pbb_kg'),
			'NIP'                  =>$NIP,
			'KETERANGAN'           =>$KETERANGAN,
			'TANGGAL_AKHIR'        =>$TANGGAL_AKHIR,
			'STATUS_BERKAS'		   =>$STATUS_BERKAS
			);

		$this->Mpermohonan->updateriwayat($dataupdate);
		$this->db->query("UPDATE pst_detail set tgl_selesai=TO_DATE('$TANGGAL_AKHIR', 'yyyy-mm-dd hh24:mi:ss') , status_selesai='1'
							where KD_KANWIL ='$KD_KANWIL'
							and KD_KANTOR ='$KD_KANTOR'
							and THN_PELAYANAN ='$THN_PELAYANAN'
							and BUNDEL_PELAYANAN ='$BUNDEL_PELAYANAN'
							and NO_URUT_PELAYANAN ='$NO_URUT_PELAYANAN'
							and KD_PROPINSI_PEMOHON ='$KD_PROPINSI_RIWAYAT'
							and KD_DATI2_PEMOHON ='$KD_DATI2_RIWAYAT'
							and KD_KECAMATAN_PEMOHON ='$KD_KECAMATAN_RIWAYAT'
							and KD_KELURAHAN_PEMOHON ='$KD_KELURAHAN_RIWAYAT'
							and KD_BLOK_PEMOHON ='$KD_BLOK_RIWAYAT'
							and NO_URUT_PEMOHON ='$NO_URUT_RIWAYAT'
							and KD_JNS_OP_PEMOHON ='$KD_JNS_OP_RIWAYAT'");




		// redirect('permohonan/detailmonitoring/'.$THN_PELAYANAN.$BUNDEL_PELAYANAN.$NO_URUT_PELAYANAN.$KD_PROPINSI_RIWAYAT.$KD_DATI2_RIWAYAT.$KD_KECAMATAN_RIWAYAT.$KD_KELURAHAN_RIWAYAT.$KD_BLOK_RIWAYAT.$NO_URUT_RIWAYAT.$KD_JNS_OP_RIWAYAT);    	
    }

	function getInternalPedanil()
	{
		$result = array();
		$kode_group = $_POST['KODE_GROUP'];
		$nip = $_POST['NIP'];
		//$kode_group = 35;
		$where = "";
		if($kode_group=='41'){
            $where = " AND NIP_PEDANIL='".$nip."'"; 
         }
		$query = $this->db->query("SELECT ID,NO_LAYANAN,NOP,LAYANAN,TAHUN,STATUS,NIP_PEDANIL FROM PV_DOKUMEN WHERE KODE_GROUP='$kode_group' AND UNIT_SEBELUMNYA!='41' $where ORDER BY tgl_surat_permohonan DESC")->result_array();

		array_push($result, array(
					'pesan'=>'1',
				));

		foreach ($query as $a) {
			array_push($result, array(
        			'ID'=>$a['ID'],
        			'NO_LAYANAN'=>$a['NO_LAYANAN'],
        			'LAYANAN'=>$a['LAYANAN'],
        			'TAHUN'=>$a['TAHUN'],
        			'STATUS'=>$a['STATUS'],
        			'NIP_PEDANIL'=>$a['NIP_PEDANIL'],
        			'NOP'=>$a['NOP'],

				));
		}
		echo json_encode(array("result"=>$result));
	}

	function DetailInternalPedanil(){
		$kode_group = $_POST['KODE_GROUP'];
		$id = $_POST['ID'];
        if($kode_group=='35'){
            $this->shareKaPedanil($id,$kode_group);
        }else{  
            $this->pedanilSelesai($id,$kode_group);
        }
        
    }

    function shareKaPedanil($id,$kg){

    	$result = array();
        $rk=$this->db->query("SELECT A. THN_PELAYANAN||A.BUNDEL_PELAYANAN||A.NO_URUT_PELAYANAN||A.KD_PROPINSI_PEMOHON||A.KD_DATI2_PEMOHON||A.KD_KECAMATAN_PEMOHON||A.KD_KELURAHAN_PEMOHON||A.KD_BLOK_PEMOHON||A.NO_URUT_PEMOHON||A.KD_JNS_OP_PEMOHON ID,
                                        A.THN_PELAYANAN||'.'|| A.BUNDEL_PELAYANAN||'.'|| A.NO_URUT_PELAYANAN NO_LAYANAN, 
                                        A.KD_PROPINSI_PEMOHON||'.'||A.KD_DATI2_PEMOHON||'.'||A.KD_KECAMATAN_PEMOHON||'.'||A.KD_KELURAHAN_PEMOHON||'.'||A.KD_BLOK_PEMOHON||'.'||A.NO_URUT_PEMOHON||'.'||A.KD_JNS_OP_PEMOHON NOP,
                                        B.NM_JENIS_PELAYANAN LAYANAN,A.THN_PELAYANAN TAHUN,CATATAN_PST
                                        FROM PST_DETAIL A
                                        JOIN PST_PERMOHONAN D    ON     D.KD_KANWIL = A.KD_KANWIL
                                        AND D.KD_KANTOR = A.KD_KANTOR
                                        AND D.THN_PELAYANAN = A.THN_PELAYANAN
                                        AND D.BUNDEL_PELAYANAN = A.BUNDEL_PELAYANAN
                                        AND D.NO_URUT_PELAYANAN = A.NO_URUT_PELAYANAN
                                        JOIN REF_JNS_PELAYANAN B ON A.KD_JNS_PELAYANAN=B.KD_JNS_PELAYANAN
                                        WHERE A.THN_PELAYANAN||A.BUNDEL_PELAYANAN||A.NO_URUT_PELAYANAN||A.KD_PROPINSI_PEMOHON||A.KD_DATI2_PEMOHON||A.KD_KECAMATAN_PEMOHON||A.KD_KELURAHAN_PEMOHON||A.KD_BLOK_PEMOHON||NO_URUT_PEMOHON||A.KD_JNS_OP_PEMOHON='$id'")->row();
        $hd=$this->db->query("select count(*) res
                            from p_riwayat
                            where THN_PELAYANAN||BUNDEL_PELAYANAN||NO_URUT_PELAYANAN||KD_PROPINSI_RIWAYAT||KD_DATI2_RIWAYAT||KD_KECAMATAN_RIWAYAT||KD_KELURAHAN_RIWAYAT||KD_BLOK_RIWAYAT||NO_URUT_RIWAYAT||KD_JNS_OP_RIWAYAT='$id' and tanggal_akhir is null and kode_group='$kg'")->row();
        $this->load->model('Mpermohonan');
        $NAMA_WP = $this->Mpermohonan->getNamaWP($rk->NOP);
        $tgl_dispo=$this->db->query("SELECT to_char(tanggal_awal,'d Mon YYYY' ) awal,to_char(tanggal_akhir,'d Mon YYYY' ) akhir,keterangan,nip, nama_group posisi,status_berkas
                                    from p_riwayat a
                                    join p_group b on a.kode_group=b.kode_group
                                    where THN_PELAYANAN||BUNDEL_PELAYANAN||NO_URUT_PELAYANAN||KD_PROPINSI_RIWAYAT||KD_DATI2_RIWAYAT||KD_KECAMATAN_RIWAYAT||KD_KELURAHAN_RIWAYAT||KD_BLOK_RIWAYAT||NO_URUT_RIWAYAT||KD_JNS_OP_RIWAYAT='$id' order by masuk desc,a.kode_group desc")->row();
        array_push($result, array(
					'pesan'=>'1',
					'STATUS'=>'1',
					'ID'=>$id,
					'KD_KANWIL'=>'01',
					'KD_KANTOR'=>'01',
					'NO_LAYANAN'=>$rk->NO_LAYANAN,
					'NOP'=>$rk->NOP,
					'NAMA_WP'=>$NAMA_WP,
					'LAYANAN'=>$rk->LAYANAN,
					'TAHUN'=>$rk->TAHUN,
					'CATATAN_PST'=>$rk->CATATAN_PST,
					'button'=>$hd->RES,
					'TGL_DISPO'=>$tgl_dispo->AWAL
				));

        $rn=$this->db->query("SELECT to_char(tanggal_awal,'d Mon YYYY' ) awal,to_char(tanggal_akhir,'d Mon YYYY' ) akhir,keterangan,nip, nama_group posisi,status_berkas
                                    from p_riwayat a
                                    join p_group b on a.kode_group=b.kode_group
                                    where THN_PELAYANAN||BUNDEL_PELAYANAN||NO_URUT_PELAYANAN||KD_PROPINSI_RIWAYAT||KD_DATI2_RIWAYAT||KD_KECAMATAN_RIWAYAT||KD_KELURAHAN_RIWAYAT||KD_BLOK_RIWAYAT||NO_URUT_RIWAYAT||KD_JNS_OP_RIWAYAT='$id' order by masuk desc,a.kode_group desc")->result();

        foreach ($rn as $rn) {
        	array_push($result, array(
        			'TANGGAL'=>$rn->AWAL.'-'.$rn->AKHIR,
        			'KETERANGAN'=>$rn->KETERANGAN,
        			'NIP'=>$rn->NIP,
        			'POSISI'=>$rn->POSISI,
        			'STATUS_BERKAS'=>$rn->STATUS_BERKAS,

			));
        }
        
        echo json_encode(array("result"=>$result));
        
    }

    function getUnitInternalPedanil()
	{
		$result = array();
		$unit=$this->db->query("SELECT NIP,NAMA_PENGGUNA  FROM P_PENGGUNA WHERE KODE_GROUP=41")->result();

		array_push($result, array(
			'pesan'                 =>'1',
			'KD_KANWIL'             =>'01',
			'KD_KANTOR'             =>'01',
			'TGL_AWAL'              =>date('d-m-Y'),
			'TGL_PERKIRAAN_SELESAI' =>date('d-m-Y', strtotime('+3 days', strtotime(date('Y-m-d')))),
			'KODE_GROUP'            =>'41',
			'STATUS_BERKAS'         =>'Proses',
		));

		foreach ($unit as $a) {
			array_push($result, array(
					'NIP'           => $a->NIP,
					'NAMA_PENGGUNA' => $a->NAMA_PENGGUNA

				));
		}
		echo json_encode(array("result"=>$result));
	}

	function ProsesKirimDokumen()
	{
		$result = array();
		$ID = $_POST['ID'];
		$KD_KANWIL            =$_POST['KD_KANWIL'];
        $KD_KANTOR            =$_POST['KD_KANTOR'];
        $THN_PELAYANAN        = substr($ID,0,4); // $_POST['THN_PELAYANAN'];
		$BUNDEL_PELAYANAN     = substr($ID,4,4);
		$NO_URUT_PELAYANAN    = substr($ID,8,3);
		$KD_PROPINSI_RIWAYAT  = substr($ID,11,2);
		$KD_DATI2_RIWAYAT     = substr($ID,13,2);
		$KD_KECAMATAN_RIWAYAT = substr($ID,15,3);
		$KD_KELURAHAN_RIWAYAT =substr($ID,18,3);
		$KD_BLOK_RIWAYAT      =substr($ID,21,3);
		$NO_URUT_RIWAYAT      =substr($ID,24,4);
		$KD_JNS_OP_RIWAYAT    =substr($ID,28,1);
        $NIP                  =$_POST['NIP'];
        
        $TANGGAL_AWAL         =$_POST['TANGGAL_AWAL'];
        $KODE_GROUP           =$_POST['KODE_GROUP'];
        $KODE_GROUP_SES           =$_POST['KODE_GROUP_SES'];
        $KETERANGAN           =NULL;
        $TANGGAL_AKHIR        =$_POST['TANGGAL_AKHIR'];
        $STATUS_BERKAS        =$_POST['STATUS_BERKAS'];



        $dataupdate=array(
            'KD_KANWIL'            =>$KD_KANWIL,
            'KD_KANTOR'            =>$KD_KANTOR,
            'THN_PELAYANAN'        =>$THN_PELAYANAN,
            'BUNDEL_PELAYANAN'     =>$BUNDEL_PELAYANAN,
            'NO_URUT_PELAYANAN'    =>$NO_URUT_PELAYANAN,
            'KD_PROPINSI_RIWAYAT'  =>$KD_PROPINSI_RIWAYAT,
            'KD_DATI2_RIWAYAT'     =>$KD_DATI2_RIWAYAT,
            'KD_KECAMATAN_RIWAYAT' =>$KD_KECAMATAN_RIWAYAT,
            'KD_KELURAHAN_RIWAYAT' =>$KD_KELURAHAN_RIWAYAT,
            'KD_BLOK_RIWAYAT'      =>$KD_BLOK_RIWAYAT,
            'NO_URUT_RIWAYAT'      =>$NO_URUT_RIWAYAT,
            'KD_JNS_OP_RIWAYAT'    =>$KD_JNS_OP_RIWAYAT,
            'KODE_GROUP'           =>$KODE_GROUP_SES,
            'NIP'                  =>$NIP,
            'KETERANGAN'           =>$KETERANGAN,
            'TANGGAL_AKHIR'        =>$TANGGAL_AKHIR,
            'STATUS_BERKAS'        =>$STATUS_BERKAS
            );

        $datanext=array(
                "KD_KANWIL"            =>$KD_KANWIL,
                "KD_KANTOR"            =>$KD_KANTOR,
                "THN_PELAYANAN"        =>$THN_PELAYANAN,
                "BUNDEL_PELAYANAN"     =>$BUNDEL_PELAYANAN,
                "NO_URUT_PELAYANAN"    =>$NO_URUT_PELAYANAN,
                "KD_PROPINSI_RIWAYAT"  =>$KD_PROPINSI_RIWAYAT,
                "KD_DATI2_RIWAYAT"     =>$KD_DATI2_RIWAYAT,
                "KD_KECAMATAN_RIWAYAT" =>$KD_KECAMATAN_RIWAYAT,
                "KD_KELURAHAN_RIWAYAT" =>$KD_KELURAHAN_RIWAYAT,
                "KD_BLOK_RIWAYAT"      =>$KD_BLOK_RIWAYAT,
                "NO_URUT_RIWAYAT"      =>$NO_URUT_RIWAYAT,
                "KD_JNS_OP_RIWAYAT"    =>$KD_JNS_OP_RIWAYAT,
                "KODE_GROUP"           =>$KODE_GROUP,
                "NIP"                  =>null,
                "TANGGAL_AWAL"         =>$TANGGAL_AWAL,
                "TANGGAL_AKHIR"        =>null,
                "KETERANGAN"           =>null,
                'UNIT_SEBEUMNYA'       =>$KODE_GROUP_SES
            );


            $disposisi=array(
                'KD_KANWIL'             =>$KD_KANWIL,
                'KD_KANTOR'             =>$KD_KANTOR,
                'THN_PELAYANAN'         =>$THN_PELAYANAN,
                'BUNDEL_PELAYANAN'      =>$BUNDEL_PELAYANAN,
                'NO_URUT_PELAYANAN'     =>$NO_URUT_PELAYANAN,
                'KD_PROPINSI'           =>$KD_PROPINSI_RIWAYAT,
                'KD_DATI2'              =>$KD_DATI2_RIWAYAT,
                'KD_KECAMATAN'          =>$KD_KECAMATAN_RIWAYAT,
                'KD_KELURAHAN'          =>$KD_KELURAHAN_RIWAYAT,
                'KD_BLOK'               =>$KD_BLOK_RIWAYAT,
                'NO_URUT'               =>$NO_URUT_RIWAYAT,
                'KD_JNS_OP'             =>$KD_JNS_OP_RIWAYAT,
                'NIP'                   =>$_POST['NIP_DISPOSISI'],
                'TGL_AWAL'              =>date('Y-m-d',  strtotime($_POST['TGL_AWAL'])),
                'TGL_PERKIRAAN_SELESAI' => date('Y-m-d',strtotime($_POST['TGL_PERKIRAAN_SELESAI'])),
                'KETERANGAN'            =>NULL
                );
        
        $this->load->model('Mdispo');
        $this->load->model('Mpermohonan');
        $this->Mdispo->insertDisposisi($disposisi);
        $this->Mpermohonan->updateriwayat($dataupdate);
        $this->Mpermohonan->insertriwayat($datanext);   
        
        array_push($result, array(
			'pesan'=>'1',
		));
        
        echo json_encode(array("result"=>$result));
	}

    function pedanilSelesai($id,$kg){
    	$result = array();
        $rk=$this->db->query("SELECT A. THN_PELAYANAN||A.BUNDEL_PELAYANAN||A.NO_URUT_PELAYANAN||A.KD_PROPINSI_PEMOHON||A.KD_DATI2_PEMOHON||A.KD_KECAMATAN_PEMOHON||A.KD_KELURAHAN_PEMOHON||A.KD_BLOK_PEMOHON||A.NO_URUT_PEMOHON||A.KD_JNS_OP_PEMOHON ID,
                                        A.THN_PELAYANAN||'.'|| A.BUNDEL_PELAYANAN||'.'|| A.NO_URUT_PELAYANAN NO_LAYANAN, 
                                        A.KD_PROPINSI_PEMOHON||'.'||A.KD_DATI2_PEMOHON||'.'||A.KD_KECAMATAN_PEMOHON||'.'||A.KD_KELURAHAN_PEMOHON||'.'||A.KD_BLOK_PEMOHON||'.'||A.NO_URUT_PEMOHON||'.'||A.KD_JNS_OP_PEMOHON NOP,
                                        B.NM_JENIS_PELAYANAN LAYANAN,A.THN_PELAYANAN TAHUN,CATATAN_PST
                                        FROM PST_DETAIL A
                                        JOIN PST_PERMOHONAN D    ON     D.KD_KANWIL = A.KD_KANWIL
                                        AND D.KD_KANTOR = A.KD_KANTOR
                                        AND D.THN_PELAYANAN = A.THN_PELAYANAN
                                        AND D.BUNDEL_PELAYANAN = A.BUNDEL_PELAYANAN
                                        AND D.NO_URUT_PELAYANAN = A.NO_URUT_PELAYANAN
                                        JOIN REF_JNS_PELAYANAN B ON A.KD_JNS_PELAYANAN=B.KD_JNS_PELAYANAN
                                        WHERE A.THN_PELAYANAN||A.BUNDEL_PELAYANAN||A.NO_URUT_PELAYANAN||A.KD_PROPINSI_PEMOHON||A.KD_DATI2_PEMOHON||A.KD_KECAMATAN_PEMOHON||A.KD_KELURAHAN_PEMOHON||A.KD_BLOK_PEMOHON||NO_URUT_PEMOHON||A.KD_JNS_OP_PEMOHON='$id'")->row();
        $rn=$this->db->query("SELECT to_char(tanggal_awal,'d Mon YYYY' ) awal,to_char(tanggal_akhir,'d Mon YYYY' ) akhir,keterangan,nip, nama_group posisi,status_berkas
                                    from p_riwayat a
                                    join p_group b on a.kode_group=b.kode_group
                                    where THN_PELAYANAN||BUNDEL_PELAYANAN||NO_URUT_PELAYANAN||KD_PROPINSI_RIWAYAT||KD_DATI2_RIWAYAT||KD_KECAMATAN_RIWAYAT||KD_KELURAHAN_RIWAYAT||KD_BLOK_RIWAYAT||NO_URUT_RIWAYAT||KD_JNS_OP_RIWAYAT='$id' order by masuk desc,a.kode_group desc")->result();
        
        $kg=$_POST['KODE_GROUP'];
        $hd=$this->db->query("SELECT count(*) res
                            from p_riwayat
                            where THN_PELAYANAN||BUNDEL_PELAYANAN||NO_URUT_PELAYANAN||KD_PROPINSI_RIWAYAT||KD_DATI2_RIWAYAT||KD_KECAMATAN_RIWAYAT||KD_KELURAHAN_RIWAYAT||KD_BLOK_RIWAYAT||NO_URUT_RIWAYAT||KD_JNS_OP_RIWAYAT='$id' and tanggal_akhir is null and kode_group='$kg'")->row();
        // $shd=$this->db->query("SELECT count(*) res
        //                     from p_riwayat
        //                     where THN_PELAYANAN||BUNDEL_PELAYANAN||NO_URUT_PELAYANAN||KD_PROPINSI_RIWAYAT||KD_DATI2_RIWAYAT||KD_KECAMATAN_RIWAYAT||KD_KELURAHAN_RIWAYAT||KD_BLOK_RIWAYAT||NO_URUT_RIWAYAT||KD_JNS_OP_RIWAYAT='$id'  and kode_group='$kg'")->row();
        $button =$hd->RES;
        // $data['btn']    =$shd->RES;
        
        $dispo  =$this->db->query("SELECT a.nip,nama_pengguna pegawai,to_char(tgl_awal,'dd/mm/yyyy') ta  ,to_char(tgl_perkiraan_selesai,'dd/mm/yyyy') tps
                                            from P_DISPOSISIPEDANIL a
                                            join p_pengguna b on a.nip=b.nip
                                            where THN_PELAYANAN||BUNDEL_PELAYANAN||NO_URUT_PELAYANAN||KD_PROPINSI||KD_DATI2||KD_KECAMATAN||KD_KELURAHAN||KD_BLOK||NO_URUT||KD_JNS_OP='$id'")->row();
        $this->load->model('Mpermohonan');

        array_push($result, array(
			'pesan'       =>'1',
			'STATUS'      =>'2',
			'ID'          =>$id,
			'KD_KANWIL'=>'01',
			'KD_KANTOR'=>'01',
			'button'      =>$button,
			'NIP'         =>$dispo->NIP,
			'PEGAWAI'     =>$dispo->PEGAWAI,
			'TA'          =>$dispo->TA,
			'TPS'         =>$dispo->TPS,
			'NO_LAYANAN'  =>$rk->NO_LAYANAN,
			'NOP'         =>$rk->NOP,
			'NAMA_WP'     =>$this->Mpermohonan->getNamaWP($rk->NOP),
			'LAYANAN'     =>$rk->LAYANAN,
			'TAHUN'       =>$rk->TAHUN,
			'CATATAN_PST' =>$rk->CATATAN_PST

		));

		foreach ($rn as $rn) {
			array_push($result, array(
				'TANGGAL'       =>$rn->AWAL.' - '.$rn->AKHIR,
				'POSISI'        => $rn->POSISI,
				'STATUS_BERKAS' =>$rn->STATUS_BERKAS,
				'KETERANGAN'    =>$rn->KETERANGAN,
			));
		}

		echo json_encode(array("result"=>$result));
    }

    function getPedanilSelesai()
    {

    	$result = array();
    	array_push($result, array(
			'pesan'                 =>'1',
			'KD_KANWIL'             =>'01',
			'KD_KANTOR'             =>'01',
			'TANGGAL_AWAL'          =>date('Y-m-d H:i:s'),
			'TANGGAL_AKHIR'         =>date('Y-m-d H:i:s'),
			'KODE_GROUP'            =>'35',
			'STATUS_BERKAS'         =>'Proses',
			'TGL_AWAL'              =>date('Y-m-d'),
			'TGL_PERKIRAAN_SELESAI' =>date('Y-m-d', strtotime('+3 days', strtotime(date('Y-m-d')))),
			'TGL_SELESAI'           =>date('Y-m-d'),
		));
    	echo json_encode(array("result"=>$result));
    }

    function ProsesSelesaiDispo(){
		$result               = array();
		$ID                   = $_POST['ID'];
		$KD_KANWIL            =$_POST['KD_KANWIL'];
		$KD_KANTOR            =$_POST['KD_KANTOR'];
		$THN_PELAYANAN        = substr($ID,0,4); // $_POST['THN_PELAYANAN'];
		$BUNDEL_PELAYANAN     = substr($ID,4,4);
		$NO_URUT_PELAYANAN    = substr($ID,8,3);
		$KD_PROPINSI_RIWAYAT  = substr($ID,11,2);
		$KD_DATI2_RIWAYAT     = substr($ID,13,2);
		$KD_KECAMATAN_RIWAYAT = substr($ID,15,3);
		$KD_KELURAHAN_RIWAYAT =substr($ID,18,3);
		$KD_BLOK_RIWAYAT      =substr($ID,21,3);
		$NO_URUT_RIWAYAT      =substr($ID,24,4);
		$KD_JNS_OP_RIWAYAT    =substr($ID,28,1);
		$NIP                  =$_POST['NIP'];
		$TANGGAL_AWAL         =$_POST['TANGGAL_AWAL'];
		$KODE_GROUP           =$_POST['KODE_GROUP'];
		$KODE_GROUP_SES       =$_POST['KODE_GROUP_SES'];
		$KETERANGAN           =NULL;
		$TANGGAL_AKHIR        =$_POST['TANGGAL_AKHIR'];
		$STATUS_BERKAS        =$_POST['STATUS_BERKAS'];

        $dataupdate=array(
            'KD_KANWIL'            =>$KD_KANWIL,
            'KD_KANTOR'            =>$KD_KANTOR,
            'THN_PELAYANAN'        =>$THN_PELAYANAN,
            'BUNDEL_PELAYANAN'     =>$BUNDEL_PELAYANAN,
            'NO_URUT_PELAYANAN'    =>$NO_URUT_PELAYANAN,
            'KD_PROPINSI_RIWAYAT'  =>$KD_PROPINSI_RIWAYAT,
            'KD_DATI2_RIWAYAT'     =>$KD_DATI2_RIWAYAT,
            'KD_KECAMATAN_RIWAYAT' =>$KD_KECAMATAN_RIWAYAT,
            'KD_KELURAHAN_RIWAYAT' =>$KD_KELURAHAN_RIWAYAT,
            'KD_BLOK_RIWAYAT'      =>$KD_BLOK_RIWAYAT,
            'NO_URUT_RIWAYAT'      =>$NO_URUT_RIWAYAT,
            'KD_JNS_OP_RIWAYAT'    =>$KD_JNS_OP_RIWAYAT,
            'KODE_GROUP'           =>$KODE_GROUP_SES,
            'NIP'                  =>$NIP,
            'KETERANGAN'           =>$KETERANGAN,
            'TANGGAL_AKHIR'        =>$TANGGAL_AKHIR,
            'STATUS_BERKAS'        =>$STATUS_BERKAS
            );

          $disposisi=array(
                'KD_KANWIL'         =>$KD_KANWIL,
                'KD_KANTOR'         =>$KD_KANTOR,
                'THN_PELAYANAN'     =>$THN_PELAYANAN,
                'BUNDEL_PELAYANAN'  =>$BUNDEL_PELAYANAN,
                'NO_URUT_PELAYANAN' =>$NO_URUT_PELAYANAN,
                'KD_PROPINSI'       =>$KD_PROPINSI_RIWAYAT,
                'KD_DATI2'          =>$KD_DATI2_RIWAYAT,
                'KD_KECAMATAN'      =>$KD_KECAMATAN_RIWAYAT,
                'KD_KELURAHAN'      =>$KD_KELURAHAN_RIWAYAT,
                'KD_BLOK'           =>$KD_BLOK_RIWAYAT,
                'NO_URUT'           =>$NO_URUT_RIWAYAT,
                'KD_JNS_OP'         =>$KD_JNS_OP_RIWAYAT,
                'TGL_SELESAI'       =>$_POST['TGL_SELESAI'],
                'STATUS'            =>'1',
                
                );

            $datanext=array(
                "KD_KANWIL"           =>$KD_KANWIL,
                "KD_KANTOR"           =>$KD_KANTOR,
                "THN_PELAYANAN"       =>$THN_PELAYANAN,
                "BUNDEL_PELAYANAN"    =>$BUNDEL_PELAYANAN,
                "NO_URUT_PELAYANAN"   =>$NO_URUT_PELAYANAN,
                "KD_PROPINSI_RIWAYAT" =>$KD_PROPINSI_RIWAYAT,
                "KD_DATI2_RIWAYAT"    =>$KD_DATI2_RIWAYAT,
                "KD_KECAMATAN_RIWAYAT"=>$KD_KECAMATAN_RIWAYAT,
                "KD_KELURAHAN_RIWAYAT"=>$KD_KELURAHAN_RIWAYAT,
                "KD_BLOK_RIWAYAT"     =>$KD_BLOK_RIWAYAT,
                "NO_URUT_RIWAYAT"     =>$NO_URUT_RIWAYAT,
                "KD_JNS_OP_RIWAYAT"   =>$KD_JNS_OP_RIWAYAT,
                "KODE_GROUP"          =>$KODE_GROUP,
                "NIP"                 =>null,
                "TANGGAL_AWAL"        =>$TANGGAL_AWAL,
                "TANGGAL_AKHIR"       =>null,
                "KETERANGAN"          =>null,
                "UNIT_SEBEUMNYA"=>$KODE_GROUP_SES
            );


		$this->load->model('Mdispo');
        $this->load->model('Mpermohonan');
        $this->Mpermohonan->updateriwayat($dataupdate);
        $this->Mpermohonan->insertriwayat($datanext);
        $this->Mdispo->updatedisposisi($disposisi);

        array_push($result, array(
			'pesan'=>'1',
		));
        
        echo json_encode(array("result"=>$result));

    }

	function getMapping()
	{
		$result = array();
		$NOP = $this->input->get('nop', true);
		$query = $this->db->query("SELECT * FROM MAPPING_NOP WHERE NOP='$NOP'")->result();
		
		if(count($query)>0){
			$lat = str_replace(' ','',$query->LATITUDE);
			$lng = str_replace(' ','',$query->LONGITUDE);
			array_push($result, array(
					'pesan'        =>'1',
					'NM_WP_SPPT'   =>'',
					'NM_KELURAHAN' =>'',
					'NM_KECAMATAN' =>'',
					'LATI'         => $lat,
					'LONGI'        => $lng
				));
		}else{
			array_push($result, array(
					'pesan'=>'0',
				));
		}
		
		echo json_encode(array("result"=>$result));
	}


	function MonitoringAktivitas()
  	{
  	  	$result = array();
      	$username = $this->input->post('username');
      	$status = $this->input->post('status');

      	$cek = $this->db->query("SELECT COUNT(*) AS JML FROM MONITORING_VERLAP WHERE USERNAME='$username'")->row();
      	if($cek->JML > 0){
      		$query = $this->db->query("UPDATE MONITORING_VERLAP SET STATUS='1' WHERE USERNAME='$username'");
        	array_push($result, array(
					'pesan'=>'1',
				));
      	}else{
      		$query = $this->db->query("INSERT INTO MONITORING_VERLAP VALUES('','$username', '', '', '$status')");
      		if($query){
      			array_push($result, array(
					'pesan'=>'1',
				));
      		}else{
      			array_push($result, array(
					'pesan'=>'0',
				));
      		}
      	}

      	echo json_encode(array("result"=>$result));
  	}

  	function UpdateMonitoring()
  	{
  		$result = array();
      	$username = $this->input->post('username');
      	$status = $this->input->post('status');
      	$lati = $this->input->post('lati');
      	$longi = $this->input->post('longi');

      	$query = $this->db->query("UPDATE MONITORING_VERLAP SET LATI='$lati', LONGI='$longi', STATUS='$status' WHERE USERNAME='$username'");

      	if($query){
      		array_push($result, array(
					'pesan'=>'1',
				));
      	}else{
      		array_push($result, array(
					'pesan'=>'0',
				));
      	}

      	echo json_encode(array("result"=>$result));
  	}

  	function getMonitoring(){
  		$result = array();
    	if(isset($_POST['filter'])){
				$filter               =$_POST['filter'];
			switch ($filter) {
				case 'No Layanan':
					# code...
					 
					$nl =$_POST['no_layanan'];
					$wh ="where no_layanan='$nl'";
					break;
				case 'NOP':
					# code...
					$KD_PROPINSI_PEMOHON  =$_POST['KD_PROPINSI_PEMOHON'];
					$KD_DATI2_PEMOHON     =$_POST['KD_DATI2_PEMOHON'];
					$KD_KECAMATAN_PEMOHON =$_POST['KD_KECAMATAN_PEMOHON'];
					$KD_KELURAHAN_PEMOHON =$_POST['KD_KELURAHAN_PEMOHON'];
					$KD_BLOK_PEMOHON      =$_POST['KD_BLOK_PEMOHON'];
					$NO_URUT_PEMOHON      =$_POST['NO_URUT_PEMOHON'];
					$KD_JNS_OP_PEMOHON    =$_POST['KD_JNS_OP_PEMOHON'];
					$nop                          =$KD_PROPINSI_PEMOHON.'.'.$KD_DATI2_PEMOHON.'.'.$KD_KECAMATAN_PEMOHON.'.'.$KD_KELURAHAN_PEMOHON.'.'.$KD_BLOK_PEMOHON.'.'.$NO_URUT_PEMOHON.'.'.$KD_JNS_OP_PEMOHON;
					$wh                           ="where nop='$nop'";
 
					break;
				 
			}
			
			$res   =$this->db->query("select * from PV_MONITOR $wh")->result();
			
			array_push($result, array(
					'pesan'=>'1',
				));

			foreach ($res as $a) {

				$b = $this->db->query("SELECT to_char(tanggal_awal,'dd Mon YYYY' ) awal,to_char(tanggal_akhir,'dd Mon YYYY' ) akhir,keterangan,nip, nama_group posisi,status_berkas from p_riwayat a join p_group b on a.kode_group=b.kode_group where  THN_PELAYANAN||'.'||BUNDEL_PELAYANAN||'.'||NO_URUT_PELAYANAN='".$a->NO_LAYANAN."' and KD_PROPINSI_RIWAYAT||'.'||KD_DATI2_RIWAYAT||'.'||KD_KECAMATAN_RIWAYAT||'.'||KD_KELURAHAN_RIWAYAT||'.'||KD_BLOK_RIWAYAT||'.'||NO_URUT_RIWAYAT||'.'||KD_JNS_OP_RIWAYAT='".$a->NOP."' order by masuk desc,a.kode_group desc")->row();

				if($b->AKHIR==''){
					$TANGGAL = $b->AWAL;
				}else{
					$TANGGAL = $b->AWAL.'-'.$b->AKHIR;
				}

				array_push($result, array(
					'NO_LAYANAN'=>$a->NO_LAYANAN,
					'NOP'=>$a->NOP,
					'LAYANAN'=>$a->LAYANAN,
					'STATUS'=>$a->STATUS,
					'TANGGAL'=>$TANGGAL,
					'POSISI'=>$b->POSISI
				));

			}
			  
    	}else{
				array_push($result, array(
					'pesan'=>'0',
				));
    	}

    	 
    	 echo json_encode(array("result"=>$result));
    }

	function getDetailPermohonan()
	{
		$result = array();
		$ID = $_POST['ID'];
		$KG = $_POST['kode_group'];
		//$ID = '20181003091350730000800019987';
		//$KG = '35';

		$rk=$this->db->query("SELECT A. THN_PELAYANAN||A.BUNDEL_PELAYANAN||A.NO_URUT_PELAYANAN||A.KD_PROPINSI_PEMOHON||A.KD_DATI2_PEMOHON||A.KD_KECAMATAN_PEMOHON||A.KD_KELURAHAN_PEMOHON||A.KD_BLOK_PEMOHON||A.NO_URUT_PEMOHON||A.KD_JNS_OP_PEMOHON ID,
										A.THN_PELAYANAN||'.'|| A.BUNDEL_PELAYANAN||'.'|| A.NO_URUT_PELAYANAN NO_LAYANAN, 
										A.KD_PROPINSI_PEMOHON||'.'||A.KD_DATI2_PEMOHON||'.'||A.KD_KECAMATAN_PEMOHON||'.'||A.KD_KELURAHAN_PEMOHON||'.'||A.KD_BLOK_PEMOHON||'.'||A.NO_URUT_PEMOHON||'.'||A.KD_JNS_OP_PEMOHON NOP,
										B.NM_JENIS_PELAYANAN LAYANAN,A.THN_PELAYANAN TAHUN,CATATAN_PST,nm_pengajuan
										FROM PST_DETAIL A
										JOIN PST_PERMOHONAN D    ON     D.KD_KANWIL = A.KD_KANWIL
										AND D.KD_KANTOR = A.KD_KANTOR
										AND D.THN_PELAYANAN = A.THN_PELAYANAN
										AND D.BUNDEL_PELAYANAN = A.BUNDEL_PELAYANAN
										AND D.NO_URUT_PELAYANAN = A.NO_URUT_PELAYANAN
										JOIN REF_JNS_PELAYANAN B ON A.KD_JNS_PELAYANAN=B.KD_JNS_PELAYANAN
										WHERE A.THN_PELAYANAN||A.BUNDEL_PELAYANAN||A.NO_URUT_PELAYANAN||A.KD_PROPINSI_PEMOHON||A.KD_DATI2_PEMOHON||A.KD_KECAMATAN_PEMOHON||A.KD_KELURAHAN_PEMOHON||A.KD_BLOK_PEMOHON||NO_URUT_PEMOHON||A.KD_JNS_OP_PEMOHON='$ID'")->row();

		$nop = $rk->NOP;
		
		$rk2=$this->db->query("select nm_wp from dat_objek_pajak a
                                join dat_subjek_pajak b on a.subjek_pajak_id=b.subjek_pajak_id
                                where KD_PROPINSI||'.'||KD_DATI2||'.'||KD_KECAMATAN||'.'||KD_KELURAHAN||'.'||KD_BLOK||'.'||NO_URUT||'.'||KD_JNS_OP='$nop'")->row();
		$rk3 =$this->db->query("select nm_wp from dat_objek_pajak a
                                join dat_subjek_pajak b on a.subjek_pajak_id=b.subjek_pajak_id
                                where KD_PROPINSI||'.'||KD_DATI2||'.'||KD_KECAMATAN||'.'||KD_KELURAHAN||'.'||KD_BLOK||'.'||NO_URUT||'.'||KD_JNS_OP='$nop'")->result_array();
        if(count($rk3)>0){
            $res=$rk2->NM_WP;
        }else{
            $res='-';
        }

		$rn=$this->db->query("SELECT  to_char(tanggal_awal,'dd/mm/YYYY' ) awal,to_char(tanggal_akhir,'dd/mm/YYYY' ) akhir,keterangan,a.nip, nama_group posisi,status_berkas,nm_pegawai
									from p_riwayat a
									join p_group b on a.kode_group=b.kode_group
									left join pegawai c on c.nip=a.nip
									where THN_PELAYANAN||BUNDEL_PELAYANAN||NO_URUT_PELAYANAN||KD_PROPINSI_RIWAYAT||KD_DATI2_RIWAYAT||KD_KECAMATAN_RIWAYAT||KD_KELURAHAN_RIWAYAT||KD_BLOK_RIWAYAT||NO_URUT_RIWAYAT||KD_JNS_OP_RIWAYAT='$ID' order by masuk desc,a.kode_group desc")->result();

		$hd=$this->db->query("select count(*) res
							from p_riwayat
							where THN_PELAYANAN||BUNDEL_PELAYANAN||NO_URUT_PELAYANAN||KD_PROPINSI_RIWAYAT||KD_DATI2_RIWAYAT||KD_KECAMATAN_RIWAYAT||KD_KELURAHAN_RIWAYAT||KD_BLOK_RIWAYAT||NO_URUT_RIWAYAT||KD_JNS_OP_RIWAYAT='$ID' and tanggal_akhir is null and kode_group='$KG'")->row();

		array_push($result, array(
					'pesan'       =>'1',
					'button'      =>$hd->RES,
					'NO_LAYANAN'  =>$rk->NO_LAYANAN,
					'NOP'         =>$nop,
					'NAMA_WP'     =>$res,
					'LAYANAN'     =>$rk->LAYANAN,
					'TAHUN_PAJAK' =>$rk->TAHUN,
					'CATATAN'     =>$rk->CATATAN_PST
				));

		foreach ($rn as $a) {
			$tanggal = "";
			if($a->AWAL==$a->AKHIR)
				{ $tanggal = $a->AWAL; }
			else{ 
				if($a->AKHIR!=''){ $tanggal = $a->AWAL.' - '.$a->AKHIR; }
				else{ $tanggal = $a->AWAL; } 
			}
			array_push($result, array(
					'TANGGAL'    => $tanggal,
					'POSISI'     =>$a->POSISI,
					'PEGAWAI'    =>$a->NM_PEGAWAI,
					'STATUS'     =>$a->STATUS_BERKAS,
					'KETERANGAN' =>$a->KETERANGAN

				));
		}
		echo json_encode(array("result"=>$result));
	}

	public function TambahDataVerlap()
	{
  //       $kd_kanwil = $_POST['kd_kanwil'];
  //       $kd_kantor = $_POST['kd_kantor'];
		// $thn_pel = $_POST['thn_pel'];
		// $bundel = $_POST['bundel'];
		// $no_urut_pel = $_POST['no_urut_pel'];
		$kd_propinsi = $_POST['kd_propinsi'];
		$kd_dati2 = $_POST['kd_dati2'];
		$kd_kecamatan = $_POST['kd_kecamatan'];
		$kd_kelurahan = $_POST['kd_kelurahan'];
		$kd_blok = $_POST['kd_blok'];
		$no_urut = $_POST['no_urut'];
		$kd_jns_op = $_POST['kd_jns_op'];
		$username = $_POST['username'];
		$l_tanah = $_POST['luas_tanah'];
		$l_bng = $_POST['luas_bangunan'];
		$tgl_penelitian = date('m/d/Y', strtotime($_POST['tgl_penelitian']));
		// $latitude = $_POST['latitude'];
		// $longitude = $_POST['longitude'];
		$catatan = $_POST['catatan'];


		$datax = array(
		 	// 'KD_KANWIL' => $kd_kanwil,
		 	// 'KD_KANTOR' => $kd_kantor,
		 	// 'THN_PELAYANAN' => $thn_pel,
		 	// 'BUNDEL_PELAYANAN' => $bundel,
		 	// 'NO_URUT_PELAYANAN' => $no_urut_pel,
		 	'KD_PROPINSI' => $kd_propinsi,
		 	'KD_DATI2' => $kd_dati2,
		 	'KD_KECAMATAN' => $kd_kecamatan,
		 	'KD_KELURAHAN' => $kd_kelurahan,
		 	'KD_BLOK' => $kd_blok,
		 	'NO_URUT' => $no_urut,
		 	'KD_JNS_OP' => $kd_jns_op,
		 	'USERNAME'=>$username,
		 	'LUAS_TANAH'=>$l_tanah,
			'LUAS_BANGUNAN'=>$l_bng,
			// 'LATITUDE'=>$latitude,
			// 'LONGITUDE'=>$longitude,
			'CATATAN'=>$catatan
			 );



		
		// $lxnjoptanah = $l_bumi*$njop_bumi;
		// $lxnjopbangunan = $l_bangunan*$njop_bangunan;
		// $njop_pbb = $lxnjoptanah+$lxnjopbangunan;
		// 
		
    	

        $config = array();
    	$config['upload_path'] = './assets/mapping/';
    	$config['allowed_types'] = 'jpg|png';

		$config['encrypt_name'] = TRUE;

		$this->load->library('upload');

    	$files = $_FILES;
     	$ImageCount = count($_FILES['foto']['name']);
     	//echo $_FILES['foto']['name'][0].$_FILES['foto']['type'][0];
  		//exit();
  		//
  		
  		$this->db->set('TANGGAL_PENELITIAN',"to_date('$tgl_penelitian','mm/dd/yyyy')",false);
    	$query = $this->db->insert('VERLAP_M', $datax);
		
    	for($i=0; $i< $ImageCount; $i++)
    	{           
    		$this->upload->initialize($config);
	        $_FILES['foto']['name']= $files['foto']['name'][$i];
	        $_FILES['foto']['type']= $files['foto']['type'][$i];
	        $_FILES['foto']['tmp_name']= $files['foto']['tmp_name'][$i];
	        $_FILES['foto']['error']= $files['foto']['error'][$i];
	        $_FILES['foto']['size']= $files['foto']['size'][$i];    

	        
	        //$this->upload->do_upload('foto');
	       

	        if ( ! $this->upload->do_upload('foto'))
	        {
	            $error = array($this->upload->display_errors());
	            print_r($error);
	        }
	        else
	        {
	        		$cari = $this->db->query("SELECT MAX(ID) AS ID FROM VERLAP_M")->row();
	                $data=$this->upload->data();
	                $this->db->set('ID_VERLAP', $cari->ID);
	                // $this->db->set('REF_BERKAS_ID', $this->input->post('REF_BERKAS2'));
	                // $this->db->set('CREATED_AT','sysdate',false);
	                $this->db->set('FOTO',$config['upload_path'].$data['file_name']);
	                $this->db->insert('VERLAP_FOTO');	                
	        }
    	}

		 
		 if($query){
		 	echo "sukses";
		 	// $update_data = array(
		 	// 	'STATUS_VALIDASI_DISPENDA' => '7',
		 	// 	'USERNAME' => $username, );
		 	// $this->db->where('ID', $id_sspd);
		 	// $this->db->update('BPHTB_SSPD', $update_data);

		 	// sspd_log($id_sspd,'7',$username);
		 }else{
		 	echo "gagal";
		 }

	}

	function BelumVerlap()
	{
		$result = array();
		$rk= $this->db->query("SELECT DISTINCT a.*, c.NM_KELURAHAN, b.NM_WP FROM P_RIWAYAT a LEFT JOIN REF_KELURAHAN c ON a.KD_KECAMATAN_RIWAYAT=c.KD_KECAMATAN AND a.KD_KELURAHAN_RIWAYAT=c.KD_KELURAHAN JOIN DAT_SUBJEK_PAJAK b ON a.KD_PROPINSI_RIWAYAT||a.KD_DATI2_RIWAYAT||a.KD_KECAMATAN_RIWAYAT||a.KD_KELURAHAN_RIWAYAT||a.KD_BLOK_RIWAYAT||a.NO_URUT_RIWAYAT||
a.KD_JNS_OP_RIWAYAT = b.SUBJEK_PAJAK_ID
WHERE KODE_GROUP=41 ORDER BY a.KD_PROPINSI_RIWAYAT ASC,a.KD_DATI2_RIWAYAT ASC,a.KD_KECAMATAN_RIWAYAT ASC,a.KD_KELURAHAN_RIWAYAT ASC,a.KD_BLOK_RIWAYAT ASC,a.NO_URUT_RIWAYAT ASC,a.KD_JNS_OP_RIWAYAT ASC")->result();

		array_push($result, array(
			'pesan' => '1',
		));

		foreach ($rk as $rk) {
			$NOP = $rk->KD_PROPINSI_RIWAYAT.'.'.$rk->KD_DATI2_RIWAYAT.'.'.$rk->KD_KECAMATAN_RIWAYAT.'.'.$rk->KD_KELURAHAN_RIWAYAT.'.'.$rk->KD_BLOK_RIWAYAT.'.'.$rk->NO_URUT_RIWAYAT.'.'.$rk->KD_JNS_OP_RIWAYAT;
			array_push($result, array(
				'NOP'          => $NOP,
				'NM_WP'        =>$rk->NM_WP,
				'NO_LAYANAN'   => $rk->THN_PELAYANAN.'.'.$rk->BUNDEL_PELAYANAN.'.'.$rk->NO_URUT_PELAYANAN,
				'NM_KELURAHAN' =>$rk->NM_KELURAHAN,
			));
		}

		echo json_encode(array("result"=>$result));
	}

	function DetailBelumVerlap()
	{
		$result = array();
		$nop = explode('.',$_POST['NOP']);
        $kd_propinsi = $nop[0];
        $kd_dati2 = $nop[1];
        $kd_kecamatan = $nop[2];
        $kd_kelurahan = $nop[3];
        $kd_blok = $nop[4];
        $no_urut = $nop[5];
        $kd_jns_op = $nop[6];

        $rk = $this->db->query("SELECT DISTINCT a.*, c.NM_KELURAHAN, b.NM_WP FROM P_RIWAYAT a LEFT JOIN REF_KELURAHAN c ON a.KD_KECAMATAN_RIWAYAT=c.KD_KECAMATAN AND a.KD_KELURAHAN_RIWAYAT=c.KD_KELURAHAN JOIN DAT_SUBJEK_PAJAK b ON a.KD_PROPINSI_RIWAYAT||a.KD_DATI2_RIWAYAT||a.KD_KECAMATAN_RIWAYAT||a.KD_KELURAHAN_RIWAYAT||a.KD_BLOK_RIWAYAT||a.NO_URUT_RIWAYAT||
a.KD_JNS_OP_RIWAYAT = b.SUBJEK_PAJAK_ID WHERE KODE_GROUP=41 AND a.KD_PROPINSI_RIWAYAT='$kd_propinsi' AND a.KD_DATI2_RIWAYAT='$kd_dati2' AND a.KD_KECAMATAN_RIWAYAT='$kd_kecamatan' AND a.KD_KELURAHAN_RIWAYAT='$kd_kelurahan' AND a.KD_BLOK_RIWAYAT='$kd_blok' AND a.NO_URUT_RIWAYAT='$no_urut' AND a.KD_JNS_OP_RIWAYAT='$kd_jns_op' ORDER BY a.KD_PROPINSI_RIWAYAT ASC,a.KD_DATI2_RIWAYAT ASC,a.KD_KECAMATAN_RIWAYAT ASC,a.KD_KELURAHAN_RIWAYAT ASC,a.KD_BLOK_RIWAYAT ASC,a.NO_URUT_RIWAYAT ASC,a.KD_JNS_OP_RIWAYAT ASC")->row();

        $NOP = $rk->KD_PROPINSI_RIWAYAT.'.'.$rk->KD_DATI2_RIWAYAT.'.'.$rk->KD_KECAMATAN_RIWAYAT.'.'.$rk->KD_KELURAHAN_RIWAYAT.'.'.$rk->KD_BLOK_RIWAYAT.'.'.$rk->NO_URUT_RIWAYAT.'.'.$rk->KD_JNS_OP_RIWAYAT;

        array_push($result, array(
			'pesan'        => '1',
			'NOP'          => $NOP,
			'NO_LAYANAN'   => $rk->THN_PELAYANAN.'.'.$rk->BUNDEL_PELAYANAN.'.'.$rk->NO_URUT_PELAYANAN,
			'NM_WP'        => $rk->NM_WP,
			'NM_KELURAHAN' => $rk->NM_KELURAHAN
		));

		echo json_encode(array("result"=>$result));
	}

	function createverlap()
    {
        $this->load->model('Mdispo');
        $this->load->model('Mpermohonan');

        $tgl_penelitian = date('m/d/Y');
        $nop = explode('.',$_POST['NOP']);
        $nopel = explode('.',$_POST['NO_LAYANAN']);
        $kode_group = $_POST['KODE_GROUP'];

        $var=str_replace('.','', $_POST['NO_LAYANAN'].$_POST['NOP']);
        $ddf= $this->Mdispo->getDataShareSelesai($var);

        $this->db->trans_start();
        $data = array(
            'LUAS_TANAH'        => $_POST['LUAS_BUMI'],
            'LUAS_BANGUNAN'     => $_POST['LUAS_BNG'],
            'CATATAN'           => $_POST['CATATAN'],
            'KD_PROPINSI'       => $nop[0],
            'KD_DATI2'          => $nop[1],
            'KD_KECAMATAN'      => $nop[2],
            'KD_KELURAHAN'      => $nop[3],
            'KD_BLOK'           => $nop[4],
            'NO_URUT'           => $nop[5],
            'KD_JNS_OP'         => $nop[6],
            'THN_PELAYANAN'     => $nopel[0],
            'BUNDEL_PELAYANAN'  => $nopel[1],
            'NO_URUT_PELAYANAN' => $nopel[2],
            'KODE_GROUP'        => $kode_group,
            'USERNAME'=>$_POST['USERNAME']
        );

        $this->db->set('TANGGAL_PENELITIAN',"to_date('$tgl_penelitian','mm/dd/yyyy')",false);
        $query = $this->db->insert('VERLAP_M', $data);

        $KD_KANWIL            ='01';
        $KD_KANTOR            ='01';
        $THN_PELAYANAN        =substr($ddf['id'], 0,4);
        $BUNDEL_PELAYANAN     =substr($ddf['id'], 4,4);
        $NO_URUT_PELAYANAN    =substr($ddf['id'], 8,3);
        $KD_PROPINSI_RIWAYAT  =substr($ddf['id'], 11,2);
        $KD_DATI2_RIWAYAT     =substr($ddf['id'], 13,2);
        $KD_KECAMATAN_RIWAYAT =substr($ddf['id'], 15,3);
        $KD_KELURAHAN_RIWAYAT =substr($ddf['id'], 18,3);
        $KD_BLOK_RIWAYAT      =substr($ddf['id'], 21,3);
        $NO_URUT_RIWAYAT      =substr($ddf['id'], 24,4);
        $KD_JNS_OP_RIWAYAT    =substr($ddf['id'], 28,1);
        $NIP                  =$_POST['NIP'];
        $TANGGAL_AWAL         =date('Y-m-d H:i:s');
        $KODE_GROUP           ='35';
        $KETERANGAN           =NULL;
        $TANGGAL_AKHIR        =date('Y-m-d H:i:s');
        // $STATUS_BERKAS        ='Proses';

        $dataupdate=array(
            'KD_KANWIL'            =>$KD_KANWIL,
            'KD_KANTOR'            =>$KD_KANTOR,
            'THN_PELAYANAN'        =>$THN_PELAYANAN,
            'BUNDEL_PELAYANAN'     =>$BUNDEL_PELAYANAN,
            'NO_URUT_PELAYANAN'    =>$NO_URUT_PELAYANAN,
            'KD_PROPINSI_RIWAYAT'  =>$KD_PROPINSI_RIWAYAT,
            'KD_DATI2_RIWAYAT'     =>$KD_DATI2_RIWAYAT,
            'KD_KECAMATAN_RIWAYAT' =>$KD_KECAMATAN_RIWAYAT,
            'KD_KELURAHAN_RIWAYAT' =>$KD_KELURAHAN_RIWAYAT,
            'KD_BLOK_RIWAYAT'      =>$KD_BLOK_RIWAYAT,
            'NO_URUT_RIWAYAT'      =>$NO_URUT_RIWAYAT,
            'KD_JNS_OP_RIWAYAT'    =>$KD_JNS_OP_RIWAYAT,
            'KODE_GROUP'           =>$_POST['KODE_GROUP'],
            'NIP'                  =>$NIP,
            'KETERANGAN'           =>$KETERANGAN,
            'TANGGAL_AKHIR'        =>$TANGGAL_AKHIR,
            'STATUS_BERKAS'        =>'Proses'
            );

          $disposisi=array(
                'KD_KANWIL'         =>$KD_KANWIL,
                'KD_KANTOR'         =>$KD_KANTOR,
                'THN_PELAYANAN'     =>$THN_PELAYANAN,
                'BUNDEL_PELAYANAN'  =>$BUNDEL_PELAYANAN,
                'NO_URUT_PELAYANAN' =>$NO_URUT_PELAYANAN,
                'KD_PROPINSI'       =>$KD_PROPINSI_RIWAYAT,
                'KD_DATI2'          =>$KD_DATI2_RIWAYAT,
                'KD_KECAMATAN'      =>$KD_KECAMATAN_RIWAYAT,
                'KD_KELURAHAN'      =>$KD_KELURAHAN_RIWAYAT,
                'KD_BLOK'           =>$KD_BLOK_RIWAYAT,
                'NO_URUT'           =>$NO_URUT_RIWAYAT,
                'KD_JNS_OP'         =>$KD_JNS_OP_RIWAYAT,
                'TGL_SELESAI'       =>date('Y-m-d'),
                'STATUS'            =>'1',
                
                );

            $datanext=array(
                "KD_KANWIL"           =>$KD_KANWIL,
                "KD_KANTOR"           =>$KD_KANTOR,
                "THN_PELAYANAN"       =>$THN_PELAYANAN,
                "BUNDEL_PELAYANAN"    =>$BUNDEL_PELAYANAN,
                "NO_URUT_PELAYANAN"   =>$NO_URUT_PELAYANAN,
                "KD_PROPINSI_RIWAYAT" =>$KD_PROPINSI_RIWAYAT,
                "KD_DATI2_RIWAYAT"    =>$KD_DATI2_RIWAYAT,
                "KD_KECAMATAN_RIWAYAT"=>$KD_KECAMATAN_RIWAYAT,
                "KD_KELURAHAN_RIWAYAT"=>$KD_KELURAHAN_RIWAYAT,
                "KD_BLOK_RIWAYAT"     =>$KD_BLOK_RIWAYAT,
                "NO_URUT_RIWAYAT"     =>$NO_URUT_RIWAYAT,
                "KD_JNS_OP_RIWAYAT"   =>$KD_JNS_OP_RIWAYAT,
                "KODE_GROUP"          =>$KODE_GROUP,
                "NIP"                 =>null,
                "TANGGAL_AWAL"        =>$TANGGAL_AWAL,
                "TANGGAL_AKHIR"       =>null,
                "KETERANGAN"          =>null,
                "UNIT_SEBEUMNYA"=>$kode_group
            );



          $this->Mpermohonan->updateriwayat($dataupdate);
          $this->Mpermohonan->insertriwayat($datanext);
          $this->Mdispo->updatedisposisi($disposisi);

        $this->db->trans_complete();
		$result = array();
        if ($this->db->trans_status() === TRUE)
        {
            array_push($result, array(
				'pesan' => '1',
			));
		
        }else{
            array_push($result, array(
				'pesan' => '0',
			));

        }

        echo json_encode(array("result"=>$result));
    }

	// function sendDispo(){
	// 	$result = array();
	// 	$KD_KANWIL            =$_POST['KD_KANWIL'];
	// 	$KD_KANTOR            =$_POST['KD_KANTOR'];
	// 	$THN_PELAYANAN        =$_POST['THN_PELAYANAN'];
	// 	$BUNDEL_PELAYANAN     =$_POST['BUNDEL_PELAYANAN'];
	// 	$NO_URUT_PELAYANAN    =$_POST['NO_URUT_PELAYANAN'];
	// 	$KD_PROPINSI_RIWAYAT  =$_POST['KD_PROPINSI_RIWAYAT'];
	// 	$KD_DATI2_RIWAYAT     =$_POST['KD_DATI2_RIWAYAT'];
	// 	$KD_KECAMATAN_RIWAYAT =$_POST['KD_KECAMATAN_RIWAYAT'];
	// 	$KD_KELURAHAN_RIWAYAT =$_POST['KD_KELURAHAN_RIWAYAT'];
	// 	$KD_BLOK_RIWAYAT      =$_POST['KD_BLOK_RIWAYAT'];
	// 	$NO_URUT_RIWAYAT      =$_POST['NO_URUT_RIWAYAT'];
	// 	$KD_JNS_OP_RIWAYAT    =$_POST['KD_JNS_OP_RIWAYAT'];
	// 	$NIP                  =$_POST['NIP'];
	//   	$TANGGAL_AWAL         =$_POST['TANGGAL_AWAL'];
	// 	$KODE_GROUP           =$_POST['KODE_GROUP'];
	// 	$KETERANGAN           =$_POST['KETERANGAN'];
	//   	$TANGGAL_AKHIR        =$_POST['TANGGAL_AKHIR'];
	//   	$STATUS_BERKAS		  =$_POST['STATUS_BERKAS'];

	//   	$SESSION_KG = $_POST['KG'];

	// 	$dataupdate=array(
	// 		'KD_KANWIL'            =>$KD_KANWIL,
	// 		'KD_KANTOR'            =>$KD_KANTOR,
	// 		'THN_PELAYANAN'        =>$THN_PELAYANAN,
	// 		'BUNDEL_PELAYANAN'     =>$BUNDEL_PELAYANAN,
	// 		'NO_URUT_PELAYANAN'    =>$NO_URUT_PELAYANAN,
	// 		'KD_PROPINSI_RIWAYAT'  =>$KD_PROPINSI_RIWAYAT,
	// 		'KD_DATI2_RIWAYAT'     =>$KD_DATI2_RIWAYAT,
	// 		'KD_KECAMATAN_RIWAYAT' =>$KD_KECAMATAN_RIWAYAT,
	// 		'KD_KELURAHAN_RIWAYAT' =>$KD_KELURAHAN_RIWAYAT,
	// 		'KD_BLOK_RIWAYAT'      =>$KD_BLOK_RIWAYAT,
	// 		'NO_URUT_RIWAYAT'      =>$NO_URUT_RIWAYAT,
	// 		'KD_JNS_OP_RIWAYAT'    =>$KD_JNS_OP_RIWAYAT,
	// 		'KODE_GROUP'           =>$SESSION_KG,
	// 		'NIP'                  =>$NIP,
	// 		'KETERANGAN'           =>$KETERANGAN,
	// 		'TANGGAL_AKHIR'        =>$TANGGAL_AKHIR,
	// 		'STATUS_BERKAS'        =>$STATUS_BERKAS
	// 		);

	// 	$datanext=array(
	// 			"KD_KANWIL"            =>$KD_KANWIL,
	// 			"KD_KANTOR"            =>$KD_KANTOR,
	// 			"THN_PELAYANAN"        =>$THN_PELAYANAN,
	// 			"BUNDEL_PELAYANAN"     =>$BUNDEL_PELAYANAN,
	// 			"NO_URUT_PELAYANAN"    =>$NO_URUT_PELAYANAN,
	// 			"KD_PROPINSI_RIWAYAT"  =>$KD_PROPINSI_RIWAYAT,
	// 			"KD_DATI2_RIWAYAT"     =>$KD_DATI2_RIWAYAT,
	// 			"KD_KECAMATAN_RIWAYAT" =>$KD_KECAMATAN_RIWAYAT,
	// 			"KD_KELURAHAN_RIWAYAT" =>$KD_KELURAHAN_RIWAYAT,
	// 			"KD_BLOK_RIWAYAT"      =>$KD_BLOK_RIWAYAT,
	// 			"NO_URUT_RIWAYAT"      =>$NO_URUT_RIWAYAT,
	// 			"KD_JNS_OP_RIWAYAT"    =>$KD_JNS_OP_RIWAYAT,
	// 			"KODE_GROUP"           =>$KODE_GROUP,
	// 			"NIP"                  =>null,
	// 			"TANGGAL_AWAL"         =>$TANGGAL_AWAL,
	// 			"TANGGAL_AKHIR"        =>null,
	// 			"KETERANGAN"           =>null,
	// 			"UNIT_SEBEUMNYA"       =>$SESSION_KG
	// 		);

	// 	$this->db->query("UPDATE p_riwayat set status_berkas='$STATUS_BERKAS',keterangan='$KETERANGAN',tanggal_akhir=TO_DATE('$TANGGAL_AKHIR', 'yyyy-mm-dd hh24:mi:ss'),nip='$NIP' 
 //                            where KD_KANWIL            ='$KD_KANWIL'
 //                            and KD_KANTOR            ='$KD_KANTOR'
 //                            and THN_PELAYANAN        ='$THN_PELAYANAN'
 //                            and BUNDEL_PELAYANAN     ='$BUNDEL_PELAYANAN'
 //                            and NO_URUT_PELAYANAN    ='$NO_URUT_PELAYANAN'
 //                            and KD_PROPINSI_RIWAYAT  ='$KD_PROPINSI_RIWAYAT'
 //                            and KD_DATI2_RIWAYAT     ='$KD_DATI2_RIWAYAT'
 //                            and KD_KECAMATAN_RIWAYAT ='$KD_KECAMATAN_RIWAYAT'
 //                            and KD_KELURAHAN_RIWAYAT ='$KD_KELURAHAN_RIWAYAT'
 //                            and KD_BLOK_RIWAYAT      ='$KD_BLOK_RIWAYAT'
 //                            and NO_URUT_RIWAYAT      ='$NO_URUT_RIWAYAT'
 //                            and KD_JNS_OP_RIWAYAT    ='$KD_JNS_OP_RIWAYAT'
 //                            and KODE_GROUP          ='$KODE_GROUP'
 //                            and TANGGAL_AKHIR is null");

	// 	if($STATUS_BERKAS!='Diambil'){
	// 		$NIPX = null;
	// 		$TGL_AKHIR = null;
	// 		$KET = null;
	// 		$this->db->query("INSERT INTO P_RIWAYAT (KD_KANWIL, KD_KANTOR, THN_PELAYANAN, BUNDEL_PELAYANAN, NO_URUT_PELAYANAN, KD_PROPINSI_RIWAYAT, KD_DATI2_RIWAYAT, KD_KECAMATAN_RIWAYAT, KD_KELURAHAN_RIWAYAT, KD_BLOK_RIWAYAT, NO_URUT_RIWAYAT, KD_JNS_OP_RIWAYAT, KODE_GROUP, NIP, TANGGAL_AWAL, TANGGAL_AKHIR, KETERANGAN,STATUS_BERKAS,UNIT_SEBEUMNYA) VALUES ('$KD_KANWIL', '$KD_KANTOR', '$THN_PELAYANAN', '$BUNDEL_PELAYANAN', '$NO_URUT_PELAYANAN', '$KD_PROPINSI_RIWAYAT', '$KD_DATI2_RIWAYAT', '$KD_KECAMATAN_RIWAYAT', '$KD_KELURAHAN_RIWAYAT', '$KD_BLOK_RIWAYAT', '$NO_URUT_RIWAYAT', '$KD_JNS_OP_RIWAYAT', '$KODE_GROUP', '$NIPX',TO_DATE('$TANGGAL_AWAL', 'yyyy-mm-dd hh24:mi:ss') , TO_DATE('$TGL_AKHIR', 'yyyy-mm-dd hh24:mi:ss') , '$KET','$STATUS_BERKAS','$SESSION_KG')");
	// 		//$this->Mpermohonan->insertriwayat($datanext);	
	// 	}else{

	// 		$this->db->query("UPDATE p_riwayat set status_berkas='$STATUS_BERKAS',keterangan='$KETERANGAN',tanggal_akhir=TO_DATE('$TANGGAL_AKHIR', 'yyyy-mm-dd hh24:mi:ss'),nip='$NIP' 
 //                            where KD_KANWIL            ='$KD_KANWIL'
 //                            and KD_KANTOR            ='$KD_KANTOR'
 //                            and THN_PELAYANAN        ='$THN_PELAYANAN'
 //                            and BUNDEL_PELAYANAN     ='$BUNDEL_PELAYANAN'
 //                            and NO_URUT_PELAYANAN    ='$NO_URUT_PELAYANAN'
 //                            and KD_PROPINSI_RIWAYAT  ='$KD_PROPINSI_RIWAYAT'
 //                            and KD_DATI2_RIWAYAT     ='$KD_DATI2_RIWAYAT'
 //                            and KD_KECAMATAN_RIWAYAT ='$KD_KECAMATAN_RIWAYAT'
 //                            and KD_KELURAHAN_RIWAYAT ='$KD_KELURAHAN_RIWAYAT'
 //                            and KD_BLOK_RIWAYAT      ='$KD_BLOK_RIWAYAT'
 //                            and NO_URUT_RIWAYAT      ='$NO_URUT_RIWAYAT'
 //                            and KD_JNS_OP_RIWAYAT    ='$KD_JNS_OP_RIWAYAT'
 //                            and KODE_GROUP          ='$KODE_GROUP'
 //                            and TANGGAL_AKHIR is null");

	// 		$this->db->query("UPDATE pst_detail set tgl_selesai=TO_DATE('$TANGGAL_AKHIR', 'yyyy-mm-dd hh24:mi:ss') , status_selesai='1'
	// 						where KD_KANWIL ='$KD_KANWIL'
	// 						and KD_KANTOR ='$KD_KANTOR'
	// 						and THN_PELAYANAN ='$THN_PELAYANAN'
	// 						and BUNDEL_PELAYANAN ='$BUNDEL_PELAYANAN'
	// 						and NO_URUT_PELAYANAN ='$NO_URUT_PELAYANAN'
	// 						and KD_PROPINSI_PEMOHON ='$KD_PROPINSI_RIWAYAT'
	// 						and KD_DATI2_PEMOHON ='$KD_DATI2_RIWAYAT'
	// 						and KD_KECAMATAN_PEMOHON ='$KD_KECAMATAN_RIWAYAT'
	// 						and KD_KELURAHAN_PEMOHON ='$KD_KELURAHAN_RIWAYAT'
	// 						and KD_BLOK_PEMOHON ='$KD_BLOK_RIWAYAT'
	// 						and NO_URUT_PEMOHON ='$NO_URUT_RIWAYAT'
	// 						and KD_JNS_OP_PEMOHON ='$KD_JNS_OP_RIWAYAT'");
	// 	}


	// 	array_push($result, array(
	// 		'pesan' => '1',
	// 	));

	// 	echo json_encode(array("result"=>$result));
		
	// 	// redirect('permohonan/detailmonitoring/'.$THN_PELAYANAN.$BUNDEL_PELAYANAN.$NO_URUT_PELAYANAN.$KD_PROPINSI_RIWAYAT.$KD_DATI2_RIWAYAT.$KD_KECAMATAN_RIWAYAT.$KD_KELURAHAN_RIWAYAT.$KD_BLOK_RIWAYAT.$NO_URUT_RIWAYAT.$KD_JNS_OP_RIWAYAT);
 //    }
}
