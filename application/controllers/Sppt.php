<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Sppt extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Msppt');
        $this->load->library('datatables');
        /*$this->load->model('Mspop');
		$this->load->model('Mpermohonan');*/
        if ($this->session->userdata('pbb') <> 1) {
            $this->session->set_flashdata('notif', '<div class="badge">
                    Silahkan login dengan username dan password anda.</p>
                    </div>');
            redirect('auth');
        }
    }

    function riwayat()
    {
        $this->template->load('template', 'sppt/formriwayat');
    }

    function riwayatnonlunas()
    {
        $this->template->load('template', 'sppt/formriwayatbelumlunas');
    }


    function sistep()
    {
        $this->template->load('template', 'sppt/formsistep');
    }

    function ceknop()
    {
        $res = str_replace('.', '', $this->input->post('nop'));
        $var = $this->input->post('var');

        if ($var == 'scan') {
            $dd = $this->db->query("select substr( dec_sppt('$res'),1,18) nop from dual")->row();
            $res = $dd->NOP;
        }


        // cek pembatalan
        $bb = $this->db->query("select cek_pembatalan('$res') batal from dual")->row();

        $data['KD_PROPINSI']  = substr($res, 0, 2);
        $data['KD_DATI2']     = substr($res, 2, 2);
        $data['KD_KECAMATAN'] = substr($res, 4, 3);
        $data['KD_KELURAHAN'] = substr($res, 7, 3);
        $data['KD_BLOK']      = substr($res, 10, 3);
        $data['NO_URUT']      = substr($res, 13, 4);
        $data['KD_JNS_OP']    = substr($res, 17, 1);
        $data['cetak'] = false;
        $hasil['res'] = $this->Msppt->getRIwayatbyNop($data, $bb->BATAL);

        $hasil['nop'] = $this->input->post('nop');
        $hasil['pembatalan'] = $bb->BATAL;
        $this->load->view('sppt/datasppt', $hasil);
    }

    function ceknopbelumlunas()
    {
        $res = str_replace('.', '', $this->input->post('nop'));
        $var = $this->input->post('var');

        if ($var == 'scan') {
            $dd = $this->db->query("select substr( dec_sppt('$res'),1,18) nop from dual")->row();
            $res = $dd->NOP;
        }
        // cek pembatalan
        $bb = $this->db->query("select cek_pembatalan('$res') batal from dual")->row();

        $data['KD_PROPINSI']  = substr($res, 0, 2);
        $data['KD_DATI2']     = substr($res, 2, 2);
        $data['KD_KECAMATAN'] = substr($res, 4, 3);
        $data['KD_KELURAHAN'] = substr($res, 7, 3);
        $data['KD_BLOK']      = substr($res, 10, 3);
        $data['NO_URUT']      = substr($res, 13, 4);
        $data['KD_JNS_OP']    = substr($res, 17, 1);
        $data['cetak'] = false;
        $hasil['res'] = $this->Msppt->getRIwayatbyNopBelumLunas($data, $bb->BATAL);
        $hasil['nop'] = $this->input->post('nop');
        $hasil['pembatalan'] = $bb->BATAL;
        $this->load->view('sppt/dataspptdua', $hasil);
    }


    function cetakspptdua($nop)
    {
        // echo $nop;

        $res                  = str_replace('.', '', $nop);

        $data['KD_PROPINSI']  = substr($res, 0, 2);
        $data['KD_DATI2']     = substr($res, 2, 2);
        $data['KD_KECAMATAN'] = substr($res, 4, 3);
        $data['KD_KELURAHAN'] = substr($res, 7, 3);
        $data['KD_BLOK']      = substr($res, 10, 3);
        $data['NO_URUT']      = substr($res, 13, 4);
        $data['KD_JNS_OP']    = substr($res, 17, 1);
        $data['NOP']          = $nop;
        $data['cetak'] = true;
        $bb                   = $this->db->query("select cek_pembatalan('$res') batal from dual")->row();
        $rk = $this->Msppt->getRIwayatbyNopBelumLunas($data, $bb->BATAL);
        if (count($rk) > 0) {

            $data['res'] = $rk;
            $data['pembatalan'] = $bb->BATAL;
            $this->load->view('sppt/cetakbayarsppt', $data);
        }
    }

    function cetaksppt($nop)
    {
        // echo $nop;

        $res                  = str_replace('.', '', $nop);
        // echo $nop;
        $data['KD_PROPINSI']  = substr($res, 0, 2);
        $data['KD_DATI2']     = substr($res, 2, 2);
        $data['KD_KECAMATAN'] = substr($res, 4, 3);
        $data['KD_KELURAHAN'] = substr($res, 7, 3);
        $data['KD_BLOK']      = substr($res, 10, 3);
        $data['NO_URUT']      = substr($res, 13, 4);
        $data['KD_JNS_OP']    = substr($res, 17, 1);
        $data['NOP']          = $nop;
        $data['cetak'] = true;
        $bb                   = $this->db->query("select cek_pembatalan('$res') batal from dual")->row();
        $rk = $this->Msppt->getRIwayatbyNop($data, $bb->BATAL);
        if (count($rk) > 0) {

            $data['res'] = $rk;
            $data['pembatalan'] = $bb->BATAL;
            $this->load->view('sppt/cetakbayarsppt', $data);
        }
    }

    function pencarian()
    {
        $data['kec'] = $this->db->query("select * from REF_KECAMATAN order by nm_kecamatan asc")->result();
        $this->template->load('template', 'sppt/form_pencarian', $data);
    }

    function getKelurahan()
    {
        $kode_kec = $this->input->post('kode_kec');
        $kel = $this->db->query("select * from ref_kelurahan where kd_kecamatan='$kode_kec' order by nm_kelurahan asc")->result();
        $option = "<option value=''>Pilih</option>";
        foreach ($kel as $kk) {
            $option .= "<option value='" . $kk->KD_KELURAHAN . "'>" . $kk->KD_KELURAHAN . ' ' . $kk->NM_KELURAHAN . "</option>";
        }
        echo $option;
    }

    function listpencarian()
    {
        $data = $this->input->post();
        $this->load->view('sppt/listpencarian', $data);
    }

    function jsonspptpencarian()
    {
        $kode_kec = $this->input->post('KODE_KEC');
        $kode_kel = $this->input->post('KODE_KEL');
        $nama = $this->input->post('NAMA');
        header('Content-Type: application/json');
        echo $this->Msppt->jsonpencarian($kode_kec, $kode_kel, $nama);
    }

    function cekdetail()
    {
        $this->template->load('template', 'sppt/detail_sppt');
    }

    function getDataNop()
    {
        $var = $this->input->get('nop');

        $sql = "SELECT a.*,get_alamat_op('ALAMAT',SUBSTR ('$var', 1, 18)) alamat_op, get_alamat_op('RT',SUBSTR ('$var', 1, 18)) RT_op, get_alamat_op('RW',SUBSTR ('$var', 1, 18)) RW_op
          FROM sppt a
         WHERE      thn_pajak_sppt = SUBSTR ('$var', 19, 4)
               AND kd_propinsi = SUBSTR ('$var', 1, 2)
               AND kd_dati2 = SUBSTR ('$var', 3, 2)
               AND kd_kecamatan = SUBSTR ('$var', 5, 3)
               AND kd_kelurahan = SUBSTR ('$var', 8, 3)
               AND kd_blok = SUBSTR ('$var', 11, 3)
               AND no_urut = SUBSTR ('$var', 14, 4)
               AND kd_jns_op = SUBSTR ('$var', 18, 1)";
        $res = $this->db->query($sql)->row();
        // echo "<pre>";
        // print_r($this->input->get());
        // echo "</pre>";
        if ($res) {
            $KD_PROPINSI               = $res->KD_PROPINSI <> '' ? $res->KD_PROPINSI : '';
            $KD_DATI2                  = $res->KD_DATI2 <> '' ? $res->KD_DATI2 : '';
            $KD_KECAMATAN              = $res->KD_KECAMATAN <> '' ? $res->KD_KECAMATAN : '';
            $KD_KELURAHAN              = $res->KD_KELURAHAN <> '' ? $res->KD_KELURAHAN : '';
            $KD_BLOK                   = $res->KD_BLOK <> '' ? $res->KD_BLOK : '';
            $NO_URUT                   = $res->NO_URUT <> '' ? $res->NO_URUT : '';
            $KD_JNS_OP                 = $res->KD_JNS_OP <> '' ? $res->KD_JNS_OP : '';
            $THN_PAJAK_SPPT            = $res->THN_PAJAK_SPPT <> '' ? $res->THN_PAJAK_SPPT : '';
            $SIKLUS_SPPT               = $res->SIKLUS_SPPT <> '' ? $res->SIKLUS_SPPT : '';
            $KD_KANWIL                 = $res->KD_KANWIL <> '' ? $res->KD_KANWIL : '';
            $KD_KANTOR                 = $res->KD_KANTOR <> '' ? $res->KD_KANTOR : '';
            $KD_TP                     = $res->KD_TP <> '' ? $res->KD_TP : '';
            $NM_WP_SPPT                = $res->NM_WP_SPPT <> '' ? $res->NM_WP_SPPT : '';
            $JLN_WP_SPPT               = $res->JLN_WP_SPPT <> '' ? $res->JLN_WP_SPPT : '';
            $BLOK_KAV_NO_WP_SPPT       = $res->BLOK_KAV_NO_WP_SPPT <> '' ? $res->BLOK_KAV_NO_WP_SPPT : '';
            $RW_WP_SPPT                = $res->RW_WP_SPPT <> '' ? $res->RW_WP_SPPT : '';
            $RT_WP_SPPT                = $res->RT_WP_SPPT <> '' ? $res->RT_WP_SPPT : '';
            $KELURAHAN_WP_SPPT         = $res->KELURAHAN_WP_SPPT <> '' ? $res->KELURAHAN_WP_SPPT : '';
            $KOTA_WP_SPPT              = $res->KOTA_WP_SPPT <> '' ? $res->KOTA_WP_SPPT : '';
            $KD_POS_WP_SPPT            = $res->KD_POS_WP_SPPT <> '' ? $res->KD_POS_WP_SPPT : '';
            $NPWP_SPPT                 = $res->NPWP_SPPT <> '' ? $res->NPWP_SPPT : '';
            $NO_PERSIL_SPPT            = $res->NO_PERSIL_SPPT <> '' ? $res->NO_PERSIL_SPPT : '';
            $KD_KLS_TANAH              = $res->KD_KLS_TANAH <> '' ? $res->KD_KLS_TANAH : '';
            $THN_AWAL_KLS_TANAH        = $res->THN_AWAL_KLS_TANAH <> '' ? $res->THN_AWAL_KLS_TANAH : '';
            $KD_KLS_BNG                = $res->KD_KLS_BNG <> '' ? $res->KD_KLS_BNG : '';
            $THN_AWAL_KLS_BNG          = $res->THN_AWAL_KLS_BNG <> '' ? $res->THN_AWAL_KLS_BNG : '';
            $TGL_JATUH_TEMPO_SPPT      = $res->TGL_JATUH_TEMPO_SPPT <> '' ? $res->TGL_JATUH_TEMPO_SPPT : '';
            $LUAS_BUMI_SPPT            = $res->LUAS_BUMI_SPPT <> '' ? $res->LUAS_BUMI_SPPT : '';
            $LUAS_BNG_SPPT             = $res->LUAS_BNG_SPPT <> '' ? $res->LUAS_BNG_SPPT : '';
            $NJOP_BUMI_SPPT            = $res->NJOP_BUMI_SPPT <> '' ? $res->NJOP_BUMI_SPPT : '';
            $NJOP_BNG_SPPT             = $res->NJOP_BNG_SPPT <> '' ? $res->NJOP_BNG_SPPT : '';
            $NJOP_SPPT                 = $res->NJOP_SPPT <> '' ? $res->NJOP_SPPT : '';
            $NJOPTKP_SPPT              = $res->NJOPTKP_SPPT <> '' ? $res->NJOPTKP_SPPT : '';
            $PBB_TERHUTANG_SPPT        = $res->PBB_TERHUTANG_SPPT <> '' ? $res->PBB_TERHUTANG_SPPT : '';
            $FAKTOR_PENGURANG_SPPT     = $res->FAKTOR_PENGURANG_SPPT <> '' ? $res->FAKTOR_PENGURANG_SPPT : '';
            $PBB_YG_HARUS_DIBAYAR_SPPT = $res->PBB_YG_HARUS_DIBAYAR_SPPT <> '' ? $res->PBB_YG_HARUS_DIBAYAR_SPPT : '';
            $STATUS_PEMBAYARAN_SPPT    = $res->STATUS_PEMBAYARAN_SPPT <> '' ? $res->STATUS_PEMBAYARAN_SPPT : '';
            $STATUS_TAGIHAN_SPPT       = $res->STATUS_TAGIHAN_SPPT <> '' ? $res->STATUS_TAGIHAN_SPPT : '';
            $STATUS_CETAK_SPPT         = $res->STATUS_CETAK_SPPT <> '' ? $res->STATUS_CETAK_SPPT : '';
            $TGL_TERBIT_SPPT           = $res->TGL_TERBIT_SPPT <> '' ? $res->TGL_TERBIT_SPPT : '';
            $TGL_CETAK_SPPT            = $res->TGL_CETAK_SPPT <> '' ? $res->TGL_CETAK_SPPT : '';
            $NIP_PENCETAK_SPPT         = $res->NIP_PENCETAK_SPPT <> '' ? $res->NIP_PENCETAK_SPPT : '';
            $ALAMAT_OP = $res->ALAMAT_OP <> '' ? $res->ALAMAT_OP : '';
            $RT_OP = $res->RT_OP <> '' ? $res->RT_OP : '';
            $RW_OP = $res->RW_OP <> '' ? $res->RW_OP : '';
        } else {
            $KD_PROPINSI               = '';
            $KD_DATI2                  = '';
            $KD_KECAMATAN              = '';
            $KD_KELURAHAN              = '';
            $KD_BLOK                   = '';
            $NO_URUT                   = '';
            $KD_JNS_OP                 = '';
            $THN_PAJAK_SPPT            = '';
            $SIKLUS_SPPT               = '';
            $KD_KANWIL                 = '';
            $KD_KANTOR                 = '';
            $KD_TP                     = '';
            $NM_WP_SPPT                = '';
            $JLN_WP_SPPT               = '';
            $BLOK_KAV_NO_WP_SPPT       = '';
            $RW_WP_SPPT                = '';
            $RT_WP_SPPT                = '';
            $KELURAHAN_WP_SPPT         = '';
            $KOTA_WP_SPPT              = '';
            $KD_POS_WP_SPPT            = '';
            $NPWP_SPPT                 = '';
            $NO_PERSIL_SPPT            = '';
            $KD_KLS_TANAH              = '';
            $THN_AWAL_KLS_TANAH        = '';
            $KD_KLS_BNG                = '';
            $THN_AWAL_KLS_BNG          = '';
            $TGL_JATUH_TEMPO_SPPT      = '';
            $LUAS_BUMI_SPPT            = '';
            $LUAS_BNG_SPPT             = '';
            $NJOP_BUMI_SPPT            = '';
            $NJOP_BNG_SPPT             = '';
            $NJOP_SPPT                 = '';
            $NJOPTKP_SPPT              = '';
            $PBB_TERHUTANG_SPPT        = '';
            $FAKTOR_PENGURANG_SPPT     = '';
            $PBB_YG_HARUS_DIBAYAR_SPPT = '';
            $STATUS_PEMBAYARAN_SPPT    = '';
            $STATUS_TAGIHAN_SPPT       = '';
            $STATUS_CETAK_SPPT         = '';
            $TGL_TERBIT_SPPT           = '';
            $TGL_CETAK_SPPT            = '';
            $NIP_PENCETAK_SPPT         = '';
            $ALAMAT_OP = '';
            $RT_OP     = '';
            $RW_OP     = '';
        }

        $row['KD_PROPINSI']               = $KD_PROPINSI;
        $row['KD_DATI2']                  = $KD_DATI2;
        $row['KD_KECAMATAN']              = $KD_KECAMATAN;
        $row['KD_KELURAHAN']              = $KD_KELURAHAN;
        $row['KD_BLOK']                   = $KD_BLOK;
        $row['NO_URUT']                   = $NO_URUT;
        $row['KD_JNS_OP']                 = $KD_JNS_OP;
        $row['THN_PAJAK_SPPT']            = $THN_PAJAK_SPPT;
        $row['SIKLUS_SPPT']               = $SIKLUS_SPPT;
        $row['KD_KANWIL']                 = $KD_KANWIL;
        $row['KD_KANTOR']                 = $KD_KANTOR;
        $row['KD_TP']                     = $KD_TP;
        $row['NM_WP_SPPT']                = $NM_WP_SPPT;
        $row['JLN_WP_SPPT']               = $JLN_WP_SPPT;
        $row['BLOK_KAV_NO_WP_SPPT']       = $BLOK_KAV_NO_WP_SPPT;
        $row['RW_WP_SPPT']                = $RW_WP_SPPT;
        $row['RT_WP_SPPT']                = $RT_WP_SPPT;
        $row['KELURAHAN_WP_SPPT']         = $KELURAHAN_WP_SPPT;
        $row['KOTA_WP_SPPT']              = $KOTA_WP_SPPT;
        $row['KD_POS_WP_SPPT']            = $KD_POS_WP_SPPT;
        $row['NPWP_SPPT']                 = $NPWP_SPPT;
        $row['NO_PERSIL_SPPT']            = $NO_PERSIL_SPPT;
        $row['KD_KLS_TANAH']              = $KD_KLS_TANAH;
        $row['THN_AWAL_KLS_TANAH']        = $THN_AWAL_KLS_TANAH;
        $row['KD_KLS_BNG']                = $KD_KLS_BNG;
        $row['THN_AWAL_KLS_BNG']          = $THN_AWAL_KLS_BNG;
        $row['TGL_JATUH_TEMPO_SPPT']      = $TGL_JATUH_TEMPO_SPPT;
        $row['LUAS_BUMI_SPPT']            = $LUAS_BUMI_SPPT;
        $row['LUAS_BNG_SPPT']             = $LUAS_BNG_SPPT;
        $row['NJOP_BUMI_SPPT']            = $NJOP_BUMI_SPPT;
        $row['NJOP_BNG_SPPT']             = $NJOP_BNG_SPPT;
        $row['NJOP_SPPT']                 = $NJOP_SPPT;
        $row['NJOPTKP_SPPT']              = $NJOPTKP_SPPT;
        $row['PBB_TERHUTANG_SPPT']        = $PBB_TERHUTANG_SPPT;
        $row['FAKTOR_PENGURANG_SPPT']     = $FAKTOR_PENGURANG_SPPT;
        $row['PBB_YG_HARUS_DIBAYAR_SPPT'] = $PBB_YG_HARUS_DIBAYAR_SPPT;
        $row['STATUS_PEMBAYARAN_SPPT']    = $STATUS_PEMBAYARAN_SPPT;
        $row['STATUS_TAGIHAN_SPPT']       = $STATUS_TAGIHAN_SPPT;
        $row['STATUS_CETAK_SPPT']         = $STATUS_CETAK_SPPT;
        $row['TGL_TERBIT_SPPT']           = $TGL_TERBIT_SPPT;
        $row['TGL_CETAK_SPPT']            = $TGL_CETAK_SPPT;
        $row['NIP_PENCETAK_SPPT']         = $NIP_PENCETAK_SPPT;
        $row['ALAMAT_OP'] = $ALAMAT_OP;
        $row['RT_OP'] = $RT_OP;
        $row['RW_OP'] = $RW_OP;
        $row_set[]                 = $row;
        echo $return = json_encode($row_set);
    }
}
