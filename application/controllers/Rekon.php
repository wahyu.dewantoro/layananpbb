<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

require_once APPPATH . '/third_party/spout/src/Spout/Autoloader/autoload.php';

use Box\Spout\Writer\WriterFactory;
use Box\Spout\Common\Type;
use Box\Spout\Writer\Style\StyleBuilder;

class Rekon extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        if ($this->session->userdata('pbb') <> 1) {
            $this->session->set_flashdata('notif', '<div class="badge">
                    Silahkan login dengan username dan password anda.</p>
                    </div>');
            redirect('auth');
        }
        $this->load->model('Mmaster');
        $this->load->library('form_validation');
        $this->load->library('datatables');
    }

    function getkelurahan()
    {
        $KD_KECAMATAN = $this->input->post('kd_kec');
        $res = $this->db->query("select kd_kelurahan,nm_kelurahan 
                                from ref_kelurahan
                                where kd_kecamatan='$KD_KECAMATAN' order by kd_kelurahan asc")->result();
        $option = "<option value=''>Pilih</option>";
        $option .= "<option value=''>000 Semua Kelurahan</option>";
        foreach ($res as $res) {
            $option .= "<option value='" . $res->KD_KELURAHAN . "'>" . $res->KD_KELURAHAN . ' ' . $res->NM_KELURAHAN . "</option>";
        }

        echo $option;
    }

    function rekonpembayaranbni()
    {
        if (isset($_POST['tgl1'])) {
            $tgl1       = $_POST['tgl1'];
        } else {
            $tgl1 = date('d-m-Y');
        }

        if (isset($_POST['cari'])) {
            // $data['rk'] = $this->db->query("SELECT a.kd_propinsi||a.kd_dati2||a.kd_kecamatan||a.kd_kelurahan||a.kd_blok||a.no_urut||a.kd_jns_op NOP, a.thn_pajak_sppt, a.jml_sppt_yg_dibayar, to_char(a.tgl_pembayaran_sppt, 'dd-mm-yyyy') as TGL_BAPENDA, b.jumlah, to_char(b.tanggal, 'dd-mm-yyyy') as TGL_BNI FROM pembayaran_sppt@tospo14 a LEFT JOIN sppt_pembayaran_bni@to17 b ON a.kd_propinsi||a.kd_dati2||a.kd_kecamatan||a.kd_kelurahan||a.kd_blok||a.no_urut||a.kd_jns_op = b.nop AND a.thn_pajak_sppt = b.thn_pajak WHERE a.kode_bank_bayar='009' AND to_char(a.tgl_pembayaran_sppt,'dd-mm-yyyy') = '$tgl1'")->result();

            $data['rk'] = $this->db->query("SELECT nop_bpd, thn_pajak_bpd,tgl_bpd,jumlah_bpd, nop_bni, thn_pajak_bni, tanggal_bni, jumlah_bni from (
SELECT a.kd_propinsi||a.kd_dati2||a.kd_kecamatan||a.kd_kelurahan||a.kd_blok||a.no_urut||a.kd_jns_op nop_bpd ,a.thn_pajak_sppt as thn_pajak_bpd, a.jml_sppt_yg_dibayar as jumlah_bpd, 
to_char(a.tgl_pembayaran_sppt, 'dd-mm-yyyy') as tgl_bpd
FROM pembayaran_sppt@tospo14 a
WHERE a.kode_bank_bayar='009' AND to_char(a.tgl_pembayaran_sppt,'dd-mm-yyyy') = '$tgl1'
)a, 
(
SELECT NOP as nop_bni,thn_pajak as thn_pajak_bni, jumlah as jumlah_bni, tanggal as tanggal_bni FROM  sppt_pembayaran_bni@to17
WHERE to_char(tanggal,'dd-mm-yyyy') = '$tgl1'
) b
where b.nop_bni(+) = a.nop_bpd
and b.thn_pajak_bni(+) = a.thn_pajak_bpd
union
select nop_bpd, thn_pajak_bpd,tgl_bpd,jumlah_bpd, nop_bni, thn_pajak_bni, tanggal_bni, jumlah_bni from (
SELECT c.kd_propinsi||c.kd_dati2||c.kd_kecamatan||c.kd_kelurahan||c.kd_blok||c.no_urut||c.kd_jns_op nop_bpd ,c.thn_pajak_sppt as thn_pajak_bpd, c.jml_sppt_yg_dibayar as jumlah_bpd, 
to_char(c.tgl_pembayaran_sppt, 'dd-mm-yyyy') as tgl_bpd
FROM pembayaran_sppt@tospo14 c
WHERE c.kode_bank_bayar='009' AND to_char(c.tgl_pembayaran_sppt,'dd-mm-yyyy') = '$tgl1'
)c, 
(
SELECT NOP as nop_bni,thn_pajak as thn_pajak_bni, jumlah as jumlah_bni, tanggal as tanggal_bni FROM  sppt_pembayaran_bni@to17
WHERE to_char(tanggal,'dd-mm-yyyy') = '$tgl1'
) d
where d.nop_bni= c.nop_bpd(+)
and d.thn_pajak_bni = c.thn_pajak_bpd(+)")->result();

            $hitung = 0;
            foreach ($data['rk'] as $row) {

                if (($row->JUMLAH_BPD != $row->JUMLAH_BNI) && $row->JUMLAH_BPD != '' && $row->JUMLAH_BNI != '') {
                    $hitung++;
                } else if ($row->NOP_BPD == '' || $row->THN_PAJAK_BPD == '' || $row->TGL_BPD == '' || $row->JUMLAH_BPD == '' || $row->NOP_BNI == '' || $row->THN_PAJAK_BNI == '' || $row->TANGGAL_BNI == '' || $row->JUMLAH_BNI == '') {
                    $hitung++;
                }
            }

            $data['hitung'] = $hitung;
        }
        $data['tgl1'] = $tgl1;


        $this->template->load('template', 'rekon/rekonpembayaranbni', $data);
    }

    function laprekonpembayaranbni()
    {

        $bln = $this->input->post('bulan', true);
        $thn = $this->input->post('tahun', true);

        $bulan = "";
        $tahun = "";

        if (isset($bln)) {
            $bulan = $bln;
        } else {
            $bulan = date('m');
        }

        if (isset($thn)) {
            $tahun = $thn;
        } else {
            $tahun = date('Y');
        }

        if (isset($_GET['t'])) {
            $tanggal = $_GET['t'];
            $bulan = substr($tanggal, 3, 2);
            $tahun = substr($tanggal, 6, 4);
            $data['drk'] = $this->db->query("SELECT a.*, to_char(TANGGAL,'DD-MM-YYYY HH:MI:SS') as TGL_PEMBAYARAN FROM SPPT_PEMBAYARAN_BNI@to17 a WHERE to_char(a.TANGGAL,'dd-mm-yyyy') = '$tanggal' ORDER BY TO_CHAR(TANGGAL, 'dd-mm-yyyy hh:mi:ss') ASC ")->result();
        }

        $data['bln'] = $bulan;
        $data['thn'] = $tahun;


        $data['rk'] = $this->db->query("SELECT COUNT(NOP) AS JML_NOP, TO_CHAR(TANGGAL, 'dd-mm-yyyy') AS TGL_PEMBAYARAN FROM SPPT_PEMBAYARAN_BNI@to17 WHERE extract(YEAR from TANGGAL) = '$tahun' AND extract(MONTH from TANGGAL) = '$bulan' GROUP BY TO_CHAR(TANGGAL, 'dd-mm-yyyy') ORDER BY TO_CHAR(TANGGAL, 'dd-mm-yyyy') ASC")->result();

        $this->template->load('template', 'rekon/laprekonpembayaran', $data);
    }

    function dlaprekonpembayaranbni()
    {
        if (isset($_POST['tgl1'])) {
            $tgl1       = $_POST['tgl1'];
        } else {
            $tgl1 = date('d-m-Y');
        }

        if (isset($_POST['cari'])) {
            $data['rk'] = $this->db->query("SELECT a.*, to_char(TANGGAL,'DD-MM-YYYY HH:MI:SS') as TGL_PEMBAYARAN FROM SPPT_PEMBAYARAN_BNI@to17 a WHERE to_char(a.TANGGAL,'dd-mm-yyyy') = '$tgl1'")->result();
        }
        $data['tgl1'] = $tgl1;


        $this->template->load('template', 'rekon/laprekonpembayaran', $data);
    }

    function prosesimport()
    {
        error_reporting(E_ALL ^ E_NOTICE);

        require_once 'excel_reader2.php';
        $data = new Spreadsheet_Excel_Reader($_FILES['data_excel']['tmp_name']);

        $baris = $data->rowcount();

        $jum = 0;
        for ($i = 2; $i < $baris; $i++) {

            $nop     = $data->val($i, 1);
            $nama    = $data->val($i, 2);
            $tahun   = $data->val($i, 3);
            $pokok   = $data->val($i, 4);
            $denda   = $data->val($i, 5);
            $jumlah  = $data->val($i, 6);
            $tanggal = substr($data->val($i, 7), 0, 4) . '/' . substr($data->val($i, 7), 4, 2) . '/' . substr($data->val($i, 7), 6, 2);
            //cek jumlah karakter pada kolom jam
            $jam = "";
            $jml_karakter = strlen($data->val($i, 8));
            if ($jml_karakter == 1) {
                $jam = '00:' . '00:' . '0' . substr($data->val($i, 8), 0, 1);
            } else if ($jml_karakter == 2) {
                $jam = '00:' . '00:' . substr($data->val($i, 8), 0, 2);
            } else if ($jml_karakter == 3) {
                $jam = '00:' . '0' . substr($data->val($i, 8), 0, 1) . ':' . substr($data->val($i, 8), 1, 2);
            } else if ($jml_karakter == 4) {
                $jam = '00:' . substr($data->val($i, 8), 0, 2) . ':' . substr($data->val($i, 8), 2, 2);
            } else if ($jml_karakter == 5) {
                $jam = '0' . substr($data->val($i, 8), 0, 1) . ':' . substr($data->val($i, 8), 1, 2) . ':' . substr($data->val($i, 8), 3, 2);
            } else {
                $jam     = substr($data->val($i, 8), 0, 2) . ':' . substr($data->val($i, 8), 2, 2) . ':' . substr($data->val($i, 8), 4, 2);
            }

            $date    = $tanggal . ' ' . $jam;

            $cari = $this->db->query("SELECT * FROM SPPT_PEMBAYARAN_BNI@to17 WHERE NOP=$nop AND THN_PAJAK=$tahun AND TANGGAL=TO_DATE('$date', 'yyyy/mm/dd hh24:mi:ss')")->result();

            if (count($cari) > 0) {
                foreach ($cari as $rk) {
                    $delete = $this->db->query("DELETE FROM SPPT_PEMBAYARAN_BNI@to17 WHERE ID=$rk->ID");
                }
                $query = $this->db->query("INSERT INTO SPPT_PEMBAYARAN_BNI@to17 VALUES (0,$nop,'$nama',$tahun,$pokok,$denda,$jumlah,to_date('$date','yyyy/mm/dd hh24:mi:ss'))");
            } else {
                $query = $this->db->query("INSERT INTO SPPT_PEMBAYARAN_BNI@to17 VALUES (0,$nop,'$nama',$tahun,$pokok,$denda,$jumlah,to_date('$date','yyyy/mm/dd hh24:mi:ss'))");
            }



            if ($query) {
                $jum += 1;
            }
        }


        // echo "<script>alert('data berhasil di proses :'".$jum.");</script>";
        // echo "<script>window.alert('data berhasil di proses :'".$jum."); window.location='".base_url()."replikasi/sudahverlap';</script>";
        // $this->session->set_flashdata('notif', '<div class="badge badge-success"><p> Data berhasil di proses : '.$jum.'</p></div>');
        $this->session->set_flashdata('jml_data', $jum);
        $this->session->set_flashdata('modal', '1');
        redirect('rekon/rekonpembayaranbni');
    }

    function dhr()
    {
        if (isset($_POST['tahun'])) {
            $tahun       = $_POST['tahun'];
        } else {
            $tahun = date('Y');
        }



        $kec = $this->db->query("SELECT KD_KECAMATAN,NM_KECAMATAN FROM REF_KECAMATAN ORDER BY KD_KECAMATAN ASC")->result();
        $KD_KECAMATAN = $this->input->post('KD_KECAMATAN', true);
        $KD_KELURAHAN = $this->input->post('KD_KELURAHAN', true);
        $kode_cari = $this->input->post('cari', true);

        $where = "";
        if ($KD_KECAMATAN <> '') {
            $where .= " AND KD_KECAMATAN='" . $KD_KECAMATAN . "'";
        }

        if ($KD_KELURAHAN <> '') {
            $where .= " AND KD_KELURAHAN='" . $KD_KELURAHAN . "'";
        }

        if (count($_POST['jns_bumi']) == 0) { } else {
            for ($i = 0; $i < count($_POST['jns_bumi']); $i++) {
                $where .= " AND JNS_BUMI='" . $_POST['jns_bumi'][$i] . "'";
            }
        }

        if ($KD_KECAMATAN <> '') {
            $kel = $this->db->query("select kd_kelurahan,nm_kelurahan 
                                    from ref_kelurahan
                                    where kd_kecamatan='$KD_KECAMATAN' order by kd_kelurahan asc")->result();
        } else {
            $kel = null;
        }

        if (isset($kode_cari)) {
            $query = $this->db->query("SELECT * FROM MV_DHR where extract(YEAR from TGL_PENDATAAN_OP) = '$tahun' $where")->result();
            $data = array(
                'rk'           => $query,
                'tahun'        => $tahun,
                'kec'          => $kec,
                'kel'          => $kel,
                'KD_KELURAHAN' => $KD_KELURAHAN,
                'KD_KECAMATAN' => $KD_KECAMATAN
            );
        } else {

            $data = array(
                'tahun'        => $tahun,
                'kec'          => $kec,
                'kel'          => $kel,
                'KD_KELURAHAN' => $KD_KELURAHAN,
                'KD_KECAMATAN' => $KD_KECAMATAN
            );
        }


        $this->template->load('template', 'datahasilrekaman/dhr', $data);
    }

    function dhrLengkap()
    {
        if (isset($_POST['tahun'])) {
            $tahun       = $_POST['tahun'];
        } else {
            $tahun = date('Y');
        }

        $kec = $this->db->query("SELECT KD_KECAMATAN,NM_KECAMATAN FROM REF_KECAMATAN ORDER BY KD_KECAMATAN ASC")->result();
        $KD_KECAMATAN = $this->input->post('KD_KECAMATAN', true);
        $KD_KELURAHAN = $this->input->post('KD_KELURAHAN', true);
        //$kode_cari=$this->input->post('cari',true);

        if ($KD_KECAMATAN <> '') {
            $kel = $this->db->query("select kd_kelurahan,nm_kelurahan 
                                    from ref_kelurahan
                                    where kd_kecamatan='$KD_KECAMATAN' order by kd_kelurahan asc")->result();
        } else {
            $kel = null;
        }

        $data = array(
            'tahun'        => $tahun,
            'kec'          => $kec,
            'kel'          => $kel,
            'KD_KELURAHAN' => $KD_KELURAHAN,
            'KD_KECAMATAN' => $KD_KECAMATAN
        );


        $this->template->load('template', 'datahasilrekaman/dhrlengkap', $data);
    }

    function dhrexcel()
    {
        ini_set('max_execution_time', 300);
        $KD_KECAMATAN = urldecode($this->input->get('kec', true));
        $KD_KELURAHAN = urldecode($this->input->get('kel', true));
        $tahun = urldecode($this->input->get('tahun'));

        $where = "";
        if ($KD_KECAMATAN <> '') {
            $where .= " AND a.kd_kecamatan='" . $KD_KECAMATAN . "'";
        }
        if ($KD_KELURAHAN <> '') {
            $where .= " AND a.kd_kelurahan='" . $KD_KELURAHAN . "'";
        }

        $kecamatan = $this->db->query("select  distinct kode_kec, nm_kecamatan, kode_kel,nm_kelurahan from (
                                    SELECT a.kd_kecamatan as kode_kec, b.nm_kecamatan,a.kd_kelurahan as kode_kel,c.kd_kelurahan, c.nm_kelurahan
                                        FROM MV_DHR a
                                    LEFT JOIN  REF_KECAMATAN b
                                    ON a.kd_kecamatan = b.kd_kecamatan
                                    JOIN REF_KELURAHAN c
                                    ON a.kd_kecamatan = c.kd_kecamatan
                                    AND a.kd_kelurahan = c.kd_kelurahan
                                    where extract(YEAR from a.TGL_PENDATAAN_OP) = '$tahun' $where
                                    
                                ) ORDER BY nm_kelurahan asc")->result();

        $header = ['NO', 'NOP', 'FORMULIR SPOP', 'JUMLAH BNG', 'ALAMAT OP', 'RT/RW', 'NAMA WP', 'LUAS BUMI', 'ZNT', 'NILAI BUMI', 'KTP', 'NPWP', 'STATUS WP', 'PEKERJAAN WP', 'PERSIL'];

        $no = 1;
        foreach ($kecamatan as $key) {
            echo $key->KODE_KEC . '/' . $key->KODE_KEL . ' : ' . $key->NM_KECAMATAN . '/' . $key->NM_KELURAHAN;
            echo '<br><hr>';
            $no++;
        }

        echo 'JUMLAH DATA : ' . $no;
        exit();
        //setup Spout Excel Writer, set tipenya xlsx
        $writer = WriterFactory::create(Type::XLSX);
        // download to browser
        $writer->openToBrowser('datahasilrekaman' . date('YmdHis') . '.xlsx');
        // set style untuk header
        $headerStyle = (new StyleBuilder())
            ->setFontBold()
            ->build();

        $first = 1;
        foreach ($kecamatan as $res) {

            $sql = $this->db->query("SELECT * FROM MV_DHR where extract(YEAR from TGL_PENDATAAN_OP) = '$tahun' AND KD_KECAMATAN=" . $res->KODE_KEC . " AND KD_KELURAHAN =" . $res->KODE_KEL)->result();

            $arrisi = array();
            $no = 1;
            foreach ($sql as $rk) {
                $nop = $rk->KD_PROPINSI . '.' . $rk->KD_DATI2 . '.' . $rk->KD_KECAMATAN . '.' . $rk->KD_KELURAHAN . '.' . $rk->KD_BLOK . '.' . $rk->NO_URUT . '.' . $rk->KD_JNS_OP;
                $rt_rw = $rk->RT_OP . '/' . $rk->RW_OP;
                $ktp = "'" . $rk->KD_PROPINSI . $rk->KD_DATI2 . $rk->KD_KECAMATAN . $rk->KD_KELURAHAN . $rk->KD_BLOK . $rk->NO_URUT . $rk->KD_JNS_OP;
                $sts_wp = $rk->KD_STATUS_WP . ' - PEMILIK';

                $cc  = $no . '+' . $nop . '+' . $rk->NO_FORMULIR_SPOP . '+' . '' . '+' . $rk->ALAMAT_OP . '+' . $rt_rw . '+' . $rk->NM_WP_SPPT . '+' . $rk->LUAS_BUMI . '+' . $rk->KD_ZNT . '+' . $rk->NILAI_SISTEM_BUMI . '+' . $ktp . '+' . '' . '+' . $sts_wp . '+' . '' . '+' . $rk->NO_PERSIL;

                $ff  = explode('+', $cc);
                array_push($arrisi, $ff);
                $no++;

                // echo json_encode(array('result' => $arrisi ));
                //echo $cc."<br><hr>";
            }

            if ($first == 1) {
                // write ke Sheet pertama
                $nama = $res->NM_KELURAHAN . ' - Kec.' . $res->NM_KECAMATAN;
                $writer->getCurrentSheet()->setName($nama);
                // header Sheet pertama
                $writer->addRowWithStyle($header, $headerStyle);
                // data Sheet pertama
                $writer->addRows($arrisi);
            } else {
                $nama = $res->NM_KELURAHAN . ' - Kec.' . $res->NM_KECAMATAN;
                // write ke Sheet kedua
                $writer->addNewSheetAndMakeItCurrent()->setName($nama);
                // header Sheet kedua
                $writer->addRowWithStyle($header, $headerStyle);
                // data Sheet pertama
                $writer->addRows($arrisi);
            }
            $first++;
        }
        $writer->close();
    }

    //     function dhrLengkapexcel(){
    //         //ini_set('max_execution_time', 300);
    //         $KD_KECAMATAN=urldecode($this->input->get('KD_KECAMATAN',true));
    //         $KD_KELURAHAN=urldecode($this->input->get('KD_KELURAHAN',true));
    //         $tahun =urldecode($this->input->get('tahun')); 
    // //exit();
    //         $where="";
    //         if($KD_KECAMATAN<>''){
    //             $where .=" AND a.kd_kecamatan='". $KD_KECAMATAN."'";
    //         }

    //         if($KD_KELURAHAN<>''){
    //             $where .=" AND a.kd_kelurahan='". $KD_KELURAHAN."'";
    //         }

    //         $kecamatan=$this->db->query("select  distinct kode_kec, nm_kecamatan, kode_kel,nm_kelurahan from (
    //                                     SELECT a.kd_kecamatan as kode_kec, b.nm_kecamatan,a.kd_kelurahan as kode_kel,c.kd_kelurahan, c.nm_kelurahan
    //                                         FROM MV_DHR a
    //                                     LEFT JOIN  REF_KECAMATAN b
    //                                     ON a.kd_kecamatan = b.kd_kecamatan
    //                                     JOIN REF_KELURAHAN c
    //                                     ON a.kd_kecamatan = c.kd_kecamatan
    //                                     AND a.kd_kelurahan = c.kd_kelurahan
    //                                     where extract(YEAR from a.TGL_PENDATAAN_OP) = '$tahun' $where                                    
    //                                 ) ORDER BY nm_kelurahan asc")->result();

    //                 $header=['NO','NOP','FORMULIR SPOP','JUMLAH BNG','ALAMAT OP','RT/RW','NAMA WP', 'LUAS BUMI','ZNT','NILAI BUMI','KTP','NPWP','STATUS WP','PEKERJAAN WP','PERSIL','FORMULIR LSPOP','NO. BANGUNAN','LUAS BANGUNAN','JUMLAH LANTAI','TAHUN DIBANGUN','TAHUN RENOVASI','KONDISI UMUM BANGUNAN','JENIS KONSTRUKSI','JENIS ATAP','JENIS DINDING','JENIS LANTAI','JENIS LANGIT - LANGIT'];

    // //                 $no=1;
    // //                 foreach ($kecamatan as $key) {
    // //                      echo $key->KODE_KEC.'/'.$key->KODE_KEL.' : '.$key->NM_KECAMATAN.'/'.$key->NM_KELURAHAN;
    // //                      echo '<br><hr>';
    // //                      $no++;
    // //                 }

    // //                 echo 'JUMLAH DATA : '.$no;
    // // exit();
    //           //setup Spout Excel Writer, set tipenya xlsx
    //         $writer = WriterFactory::create(Type::XLSX);
    //         // download to browser
    //         $writer->openToBrowser('datahasilrekamanlengkap'.date('YmdHis').'.xlsx');
    //         // set style untuk header
    //         $headerStyle = (new StyleBuilder())
    //                ->setFontBold()
    //                ->build();

    //          $first=1;
    // foreach($kecamatan as $res){

    //             // echo $res->KODE_KEC.'/'.$res->KODE_KEL;
    //             // echo '<br><hr>';
    //             // exit();

    //             $sql=$this->db->query("SELECT * FROM MV_DHR where extract(YEAR from TGL_PENDATAAN_OP) = '$tahun' AND ROWNUM <=10 AND KD_KECAMATAN=".$res->KODE_KEC." AND KD_KELURAHAN =".$res->KODE_KEL.)->result();

    //                 $arrisi=array();
    //                 $no=1;
    //                 foreach($sql as $rk){
    //                     $nop = $rk->KD_PROPINSI.'.'.$rk->KD_DATI2.'.'.$rk->KD_KECAMATAN.'.'.$rk->KD_KELURAHAN.'.'.$rk->KD_BLOK.'.'.$rk->NO_URUT.'.'.$rk->KD_JNS_OP;
    //                     $rt_rw = $rk->RT_OP.'/'.$rk->RW_OP;
    //                     $ktp = "'".$rk->KD_PROPINSI.$rk->KD_DATI2.$rk->KD_KECAMATAN.$rk->KD_KELURAHAN.$rk->KD_BLOK.$rk->NO_URUT.$rk->KD_JNS_OP;
    //                     $sts_wp = $rk->KD_STATUS_WP.' - PEMILIK';

    //                     $cc  =$no .'+'. $nop .'+'. $rk->NO_FORMULIR_SPOP .'+'. '' .'+'. $rk->ALAMAT_OP.'+'. $rt_rw.'+'.$rk->NM_WP_SPPT.'+'. $rk->LUAS_BUMI.'+'. $rk->KD_ZNT.'+'. $rk->NILAI_SISTEM_BUMI.'+'.$ktp.'+'.''.'+'.$sts_wp.'+'.''.'+'.$rk->NO_PERSIL.'+'.$rk->NO_FORMULIR_LSPOP.'+'.$rk->NO_BNG.'+'.$rk->LUAS_BNG.'+'.$rk->JML_LANTAI_BNG.'+'.$rk->THN_DIBANGUN_BNG.'+'.$rk->THN_RENOVASI_BNG.'+'.$rk->KONDISI_BNG.'+'.$rk->JNS_KONSTRUKSI_BNG.'+'.$rk->JNS_ATAP_BNG.'+'.$rk->KD_DINDING.'+'.$rk->KD_LANTAI.'+'.$rk->KD_LANGIT_LANGIT;

    //                     $ff  =explode('+',$cc);
    //                     array_push($arrisi,$ff);
    //                     $no++;

    //                     //echo json_encode(array('result' => $arrisi ));
    //                     //echo $cc."<br><hr>";
    //                 }

    //         //     exit();
    //         if($first==1){
    //               // write ke Sheet pertama
    //                 $nama = $res->NM_KELURAHAN.' - Kec.'.$res->NM_KECAMATAN;
    //                 $writer->getCurrentSheet()->setName($nama);
    //                 // header Sheet pertama
    //                 $writer->addRowWithStyle($header, $headerStyle);
    //                 // data Sheet pertama
    //                 $writer->addRows($arrisi);
    //             }else{
    //                 $nama = $res->NM_KELURAHAN.' - Kec.'.$res->NM_KECAMATAN;
    //                 // write ke Sheet kedua
    //                 $writer->addNewSheetAndMakeItCurrent()->setName($nama);
    //                 // header Sheet kedua
    //                 $writer->addRowWithStyle($header, $headerStyle);
    //                 // data Sheet pertama
    //                 $writer->addRows($arrisi);
    //             }
    //         $first++;
    //         }
    //         $writer->close();

    //     }

    //     function dhrexcel2(){
    //         $KD_KECAMATAN=urldecode($this->input->get('kec',true));
    //         $KD_KELURAHAN=urldecode($this->input->get('kel',true));
    //         $tahun =urldecode($this->input->get('tahun')); 

    //         $where="";
    //         if($KD_KECAMATAN<>''){
    //             $where .=" AND KD_KECAMATAN='". $KD_KECAMATAN."'";
    //         }

    //         if($KD_KELURAHAN<>''){
    //             $where .=" AND KD_KELURAHAN='". $KD_KELURAHAN."'";
    //         }

    //         $data['rk'] = $this->db->query("SELECT * FROM MV_DHR where extract(YEAR from TGL_PENDATAAN_OP) = '$tahun' $where")->result();

    //          //print_r($data);
    //         $this->load->view('datahasilrekaman/dhrexcel',$data);
    //     }

}
