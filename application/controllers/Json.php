<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Json extends CI_Controller
{
	public function __construct()
	{
		parent::__construct();
	}

	function riwayatpembayaran($nop = null)
	{


		if (!empty($nop)) {
			$nopfil = preg_replace("/[^0-9]/", "", $nop);

			$rk = $this->db->query("select * from ( 
								 select   kd_propinsi||kd_dati2||kd_kecamatan||kd_kelurahan||kd_blok||no_urut||kd_jns_op nop,thn_pajak_sppt tahun_pajak,to_char(tgl_pembayaran_sppt,'dd-mm-yyyy') tanggal_bayar,case when tgl_pembayaran_sppt is not null then 'Lunas' else 'Belum lunas' end status
								 from pembayaran_sppt 
								 where kd_propinsi||kd_dati2||kd_kecamatan||kd_kelurahan||kd_blok||no_urut||kd_jns_op='$nopfil'
								 order by  thn_pajak_sppt desc
								) where rownum <=5")->result_array();
			$data['rowcount'] = count($rk);
			$data['data'] = $rk;
			echo json_encode($data);
		} else {
			echo base_url() . 'Json/riwayatpembayaran/{NOP}';
		}
	}

	function spptbynoptahunpajak($nop = null, $tahun = null)
	{
		if (!empty($nop) && !empty($tahun)) {

			$rk = $this->db->query("select kd_propinsi||kd_dati2||kd_kecamatan||kd_kelurahan||kd_blok||no_urut||kd_jns_op nop, NM_WP_SPPT nama_wp,jln_wp_sppt jalan,rw_wp_sppt rw,rt_wp_sppt rt,kelurahan_wp_sppt kelurahan,kota_wp_sppt kota,luas_bumi_sppt luas_tanah,luas_bng_sppt luas_bangunan,njop_bumi_sppt njop_tanah,njop_bng_sppt njop_bangunan, pbb_yg_harus_dibayar_sppt harus_bayar_sppt 
									from sppt
									 where kd_propinsi||kd_dati2||kd_kecamatan||kd_kelurahan||kd_blok||no_urut||kd_jns_op='$nop' and THN_PAJAK_SPPT='$tahun'")->result_array();
			$data['rowcount'] = count($rk);
			$data['data'] = $rk;
			echo json_encode($data);
		} else {
			echo base_url() . 'Json/spptbynoptahunpajak/{NOP}/{tahun}';
		}
	}
}
