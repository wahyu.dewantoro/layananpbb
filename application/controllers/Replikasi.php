<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

require_once APPPATH . '/third_party/spout/src/Spout/Autoloader/autoload.php';

use Box\Spout\Writer\WriterFactory;
use Box\Spout\Common\Type;
use Box\Spout\Writer\Style\StyleBuilder;

class Replikasi extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        if ($this->session->userdata('pbb') <> 1) {
            $this->session->set_flashdata('notif', '<div class="badge">
                    Silahkan login dengan username dan password anda.</p>
                    </div>');
            redirect('auth');
        }
        $this->load->model('Mmaster');
        $this->load->library('form_validation');
        $this->load->library('datatables');
        $this->kg = $this->session->userdata('pbb_kg');
    }

    function getkelurahan()
    {
        $KD_KECAMATAN = $this->input->post('kd_kec');
        $res = $this->db->query("select kd_kelurahan,nm_kelurahan 
                                from ref_kelurahan
                                where kd_kecamatan='$KD_KECAMATAN'")->result();
        $option = "<option value=''>Pilih</option>";
        foreach ($res as $res) {
            $option .= "<option value='" . $res->KD_KELURAHAN . "'>" . $res->KD_KELURAHAN . ' ' . $res->NM_KELURAHAN . "</option>";
        }

        echo $option;
    }


    function belumverlap()
    {

        $data['rk'] = $this->db->query("SELECT DISTINCT a.*, c.NM_KELURAHAN, b.NM_WP FROM P_RIWAYAT a LEFT JOIN REF_KELURAHAN c ON a.KD_KECAMATAN_RIWAYAT=c.KD_KECAMATAN AND a.KD_KELURAHAN_RIWAYAT=c.KD_KELURAHAN JOIN DAT_SUBJEK_PAJAK b ON a.KD_PROPINSI_RIWAYAT||a.KD_DATI2_RIWAYAT||a.KD_KECAMATAN_RIWAYAT||a.KD_KELURAHAN_RIWAYAT||a.KD_BLOK_RIWAYAT||a.NO_URUT_RIWAYAT||
a.KD_JNS_OP_RIWAYAT = b.SUBJEK_PAJAK_ID
WHERE KODE_GROUP=41 ORDER BY a.KD_PROPINSI_RIWAYAT ASC,a.KD_DATI2_RIWAYAT ASC,a.KD_KECAMATAN_RIWAYAT ASC,a.KD_KELURAHAN_RIWAYAT ASC,a.KD_BLOK_RIWAYAT ASC,a.NO_URUT_RIWAYAT ASC,a.KD_JNS_OP_RIWAYAT ASC")->result();

        // $data['rk']=$this->db->query("SELECT DISTINCT a.*, b.NM_WP_SPPT, c.NM_KELURAHAN FROM MAPPING_NOP a LEFT JOIN SPPT b ON a.KD_PROPINSI=b.KD_PROPINSI AND a.KD_DATI2=b.KD_DATI2 AND a.KD_KECAMATAN=b.KD_KECAMATAN AND a.KD_KELURAHAN=b.KD_KELURAHAN AND a.KD_BLOK=b.KD_BLOK AND a.NO_URUT=b.NO_URUT AND a.KD_JNS_OP=b.KD_JNS_OP JOIN REF_KELURAHAN c ON a.KD_KECAMATAN=c.KD_KECAMATAN AND a.KD_KELURAHAN=c.KD_KELURAHAN WHERE a.STATUS=0")->result();

        $this->template->load('template', 'replikasi/belumverlap', $data);
    }

    function sudahverlap()
    {
        $data['rk'] = $this->db->query("SELECT DISTINCT a.*, b.NM_WP AS NM_WP_SPPT, c.NM_KELURAHAN FROM VERLAP_M a LEFT JOIN DAT_SUBJEK_PAJAK b ON a.KD_PROPINSI||a.KD_DATI2||a.KD_KECAMATAN||a.KD_KELURAHAN||a.KD_BLOK||a.NO_URUT||a.KD_JNS_OP = b.SUBJEK_PAJAK_ID JOIN REF_KELURAHAN c ON a.KD_KECAMATAN=c.KD_KECAMATAN AND a.KD_KELURAHAN=c.KD_KELURAHAN ORDER BY a.THN_PELAYANAN DESC,a.BUNDEL_PELAYANAN DESC,a.NO_URUT_PELAYANAN DESC")->result();

        $this->template->load('template', 'replikasi/sudahverlap', $data);
    }

    function verlap()
    {
        $nop = explode('.', $_GET['id']);
        $kd_propinsi = $nop[0];
        $kd_dati2 = $nop[1];
        $kd_kecamatan = $nop[2];
        $kd_kelurahan = $nop[3];
        $kd_blok = $nop[4];
        $no_urut = $nop[5];
        $kd_jns_op = $nop[6];

        $data['rk'] = $this->db->query("SELECT DISTINCT a.*, b.NM_WP_SPPT, c.NM_KELURAHAN FROM P_RIWAYAT a LEFT JOIN SPPT b ON a.KD_PROPINSI_RIWAYAT=b.KD_PROPINSI AND a.KD_DATI2_RIWAYAT=b.KD_DATI2 AND a.KD_KECAMATAN_RIWAYAT=b.KD_KECAMATAN AND a.KD_KELURAHAN_RIWAYAT=b.KD_KELURAHAN AND a.KD_BLOK_RIWAYAT=b.KD_BLOK AND a.NO_URUT_RIWAYAT=b.NO_URUT AND a.KD_JNS_OP_RIWAYAT=b.KD_JNS_OP JOIN REF_KELURAHAN c ON a.KD_KECAMATAN_RIWAYAT=c.KD_KECAMATAN AND a.KD_KELURAHAN_RIWAYAT=c.KD_KELURAHAN  WHERE KODE_GROUP=41 AND a.KD_PROPINSI_RIWAYAT='$kd_propinsi' AND a.KD_DATI2_RIWAYAT='$kd_dati2' AND a.KD_KECAMATAN_RIWAYAT='$kd_kecamatan' AND a.KD_KELURAHAN_RIWAYAT='$kd_kelurahan' AND a.KD_BLOK_RIWAYAT='$kd_blok' AND a.NO_URUT_RIWAYAT='$no_urut' AND a.KD_JNS_OP_RIWAYAT='$kd_jns_op' ORDER BY a.THN_PELAYANAN DESC,a.BUNDEL_PELAYANAN DESC,a.NO_URUT_PELAYANAN DESC")->row();

        $this->template->load('template', 'replikasi/verlap', $data);
    }

    function editverlap()
    {
        $ID = $_GET['id'];
        $data['rk'] = $this->db->query("SELECT DISTINCT a.*, b.NM_WP_SPPT, c.NM_KELURAHAN FROM VERLAP_M a LEFT JOIN SPPT b ON a.KD_PROPINSI=b.KD_PROPINSI AND a.KD_DATI2=b.KD_DATI2 AND a.KD_KECAMATAN=b.KD_KECAMATAN AND a.KD_KELURAHAN=b.KD_KELURAHAN AND a.KD_BLOK=b.KD_BLOK AND a.NO_URUT=b.NO_URUT AND a.KD_JNS_OP=b.KD_JNS_OP JOIN REF_KELURAHAN c ON a.KD_KECAMATAN=c.KD_KECAMATAN AND a.KD_KELURAHAN=c.KD_KELURAHAN WHERE a.ID='$ID' ORDER BY a.THN_PELAYANAN DESC,a.BUNDEL_PELAYANAN DESC,a.NO_URUT_PELAYANAN DESC")->row();

        $this->template->load('template', 'replikasi/editverlap', $data);
    }

    function updateverlap()
    {
        $ID = $_POST['ID'];
        $data = array(
            'LUAS_TANAH'        => $_POST['luas_bumi'],
            'LUAS_BANGUNAN'     => $_POST['luas_bng'],
            'CATATAN'           => $_POST['catatan'],
        );
        $this->db->where('ID', $ID);
        $query = $this->db->update('VERLAP_M', $data);

        if ($query) {
            echo "<script>window.alert('Berhasil update data verlap '); window.location='" . base_url() . "replikasi/sudahverlap';</script>";
        } else {
            echo "<script>window.alert('Gagal update '); window.location='" . base_url() . "replikasi/sudahverlap';</script>";
        }
    }

    function createverlap()
    {
        $this->load->model('Mdispo');
        $this->load->model('Mpermohonan');

        $tgl_penelitian = date('m/d/Y');
        $nop = explode('.', $_POST['nop']);
        $nopel = explode('.', $_POST['nopel']);
        $kode_group = $_SESSION['pbb_kg'];

        $var = str_replace('.', '', $_POST['nopel'] . $_POST['nop']);
        $ddf = $this->Mdispo->getDataShareSelesai($var);
        /*echo "<pre>";
        print_r($ddf['id']);
        echo "<br>";
        echo substr($ddf['id'], 0,4);
        echo "<br>";
        echo substr($ddf['id'], 4,4);
        echo "<br>";
        echo substr($ddf['id'], 8,3);
        echo "<br>";
        echo substr($ddf['id'], 11,2);
        echo "<br>";
        echo substr($ddf['id'], 13,2);
        echo "<br>";
        echo substr($ddf['id'], 15,3);
        echo "<br>";
        echo substr($ddf['id'], 18,3);
        echo "<br>";
        echo substr($ddf['id'], 21,3);
        echo "<br>";
        echo substr($ddf['id'], 24,4);
        echo "<br>";
        echo substr($ddf['id'], 28,1);*/
        // die();

        // $this->Mdispo->getDataShareSelesai($id);
        $this->db->trans_start();
        $data = array(
            'LUAS_TANAH'        => $_POST['luas_bumi'],
            'LUAS_BANGUNAN'     => $_POST['luas_bng'],
            'CATATAN'           => $_POST['catatan'],
            'KD_PROPINSI'       => $nop[0],
            'KD_DATI2'          => $nop[1],
            'KD_KECAMATAN'      => $nop[2],
            'KD_KELURAHAN'      => $nop[3],
            'KD_BLOK'           => $nop[4],
            'NO_URUT'           => $nop[5],
            'KD_JNS_OP'         => $nop[6],
            'THN_PELAYANAN'     => $nopel[0],
            'BUNDEL_PELAYANAN'  => $nopel[1],
            'NO_URUT_PELAYANAN' => $nopel[2],
            'KODE_GROUP'        => $kode_group,
            'USERNAME' => $this->session->userdata('username')
        );

        $this->db->set('TANGGAL_PENELITIAN', "to_date('$tgl_penelitian','mm/dd/yyyy')", false);
        $query = $this->db->insert('VERLAP_M', $data);
        /*$ddf= $this->Mdispo->getDataShareSelesai($var);

                $expa=explode('.',$rk->NO_LAYANAN);
                $aspa=explode('.', $rk->NOP);*/

        $KD_KANWIL            = '01';
        $KD_KANTOR            = '01';
        $THN_PELAYANAN        = substr($ddf['id'], 0, 4);
        $BUNDEL_PELAYANAN     = substr($ddf['id'], 4, 4);
        $NO_URUT_PELAYANAN    = substr($ddf['id'], 8, 3);
        $KD_PROPINSI_RIWAYAT  = substr($ddf['id'], 11, 2);
        $KD_DATI2_RIWAYAT     = substr($ddf['id'], 13, 2);
        $KD_KECAMATAN_RIWAYAT = substr($ddf['id'], 15, 3);
        $KD_KELURAHAN_RIWAYAT = substr($ddf['id'], 18, 3);
        $KD_BLOK_RIWAYAT      = substr($ddf['id'], 21, 3);
        $NO_URUT_RIWAYAT      = substr($ddf['id'], 24, 4);
        $KD_JNS_OP_RIWAYAT    = substr($ddf['id'], 28, 1);
        $NIP                  = $this->session->userdata('nip');
        $TANGGAL_AWAL         = date('Y-m-d H:i:s');
        $KODE_GROUP           = '35';
        $KETERANGAN           = NULL;
        $TANGGAL_AKHIR        = date('Y-m-d H:i:s');
        // $STATUS_BERKAS        ='Proses';

        $dataupdate = array(
            'KD_KANWIL'            => $KD_KANWIL,
            'KD_KANTOR'            => $KD_KANTOR,
            'THN_PELAYANAN'        => $THN_PELAYANAN,
            'BUNDEL_PELAYANAN'     => $BUNDEL_PELAYANAN,
            'NO_URUT_PELAYANAN'    => $NO_URUT_PELAYANAN,
            'KD_PROPINSI_RIWAYAT'  => $KD_PROPINSI_RIWAYAT,
            'KD_DATI2_RIWAYAT'     => $KD_DATI2_RIWAYAT,
            'KD_KECAMATAN_RIWAYAT' => $KD_KECAMATAN_RIWAYAT,
            'KD_KELURAHAN_RIWAYAT' => $KD_KELURAHAN_RIWAYAT,
            'KD_BLOK_RIWAYAT'      => $KD_BLOK_RIWAYAT,
            'NO_URUT_RIWAYAT'      => $NO_URUT_RIWAYAT,
            'KD_JNS_OP_RIWAYAT'    => $KD_JNS_OP_RIWAYAT,
            'KODE_GROUP'           => $this->session->userdata('pbb_kg'),
            'NIP'                  => $NIP,
            'KETERANGAN'           => $KETERANGAN,
            'TANGGAL_AKHIR'        => $TANGGAL_AKHIR,
            'STATUS_BERKAS'        => 'Proses'
        );

        $disposisi = array(
            'KD_KANWIL'         => $KD_KANWIL,
            'KD_KANTOR'         => $KD_KANTOR,
            'THN_PELAYANAN'     => $THN_PELAYANAN,
            'BUNDEL_PELAYANAN'  => $BUNDEL_PELAYANAN,
            'NO_URUT_PELAYANAN' => $NO_URUT_PELAYANAN,
            'KD_PROPINSI'       => $KD_PROPINSI_RIWAYAT,
            'KD_DATI2'          => $KD_DATI2_RIWAYAT,
            'KD_KECAMATAN'      => $KD_KECAMATAN_RIWAYAT,
            'KD_KELURAHAN'      => $KD_KELURAHAN_RIWAYAT,
            'KD_BLOK'           => $KD_BLOK_RIWAYAT,
            'NO_URUT'           => $NO_URUT_RIWAYAT,
            'KD_JNS_OP'         => $KD_JNS_OP_RIWAYAT,
            'TGL_SELESAI'       => date('Y-m-d'),
            'STATUS'            => '1',

        );

        $datanext = array(
            "KD_KANWIL"           => $KD_KANWIL,
            "KD_KANTOR"           => $KD_KANTOR,
            "THN_PELAYANAN"       => $THN_PELAYANAN,
            "BUNDEL_PELAYANAN"    => $BUNDEL_PELAYANAN,
            "NO_URUT_PELAYANAN"   => $NO_URUT_PELAYANAN,
            "KD_PROPINSI_RIWAYAT" => $KD_PROPINSI_RIWAYAT,
            "KD_DATI2_RIWAYAT"    => $KD_DATI2_RIWAYAT,
            "KD_KECAMATAN_RIWAYAT" => $KD_KECAMATAN_RIWAYAT,
            "KD_KELURAHAN_RIWAYAT" => $KD_KELURAHAN_RIWAYAT,
            "KD_BLOK_RIWAYAT"     => $KD_BLOK_RIWAYAT,
            "NO_URUT_RIWAYAT"     => $NO_URUT_RIWAYAT,
            "KD_JNS_OP_RIWAYAT"   => $KD_JNS_OP_RIWAYAT,
            "KODE_GROUP"          => $KODE_GROUP,
            "NIP"                 => null,
            "TANGGAL_AWAL"        => $TANGGAL_AWAL,
            "TANGGAL_AKHIR"       => null,
            "KETERANGAN"          => null,
            "UNIT_SEBEUMNYA" => $this->kg
        );



        $this->Mpermohonan->updateriwayat($dataupdate);
        $this->Mpermohonan->insertriwayat($datanext);
        $this->Mdispo->updatedisposisi($disposisi);

        $this->db->trans_complete();

        if ($this->db->trans_status() === TRUE) {
            echo "<script>window.alert('Berhasil entri data verlap '); window.location='" . base_url() . "replikasi/sudahverlap';</script>";
        } else {
            echo "<script>window.alert('Gagal entri '); window.location='" . base_url() . "replikasi/sudahverlap';</script>";
        }
    }

    function dhrLengkap()
    {
        if (isset($_POST['tahun'])) {
            $tahun       = $_POST['tahun'];
        } else {
            $tahun = date('Y');
        }

        $kec = $this->db->query("SELECT KD_KECAMATAN,NM_KECAMATAN FROM REF_KECAMATAN")->result();
        $KD_KECAMATAN = $this->input->post('KD_KECAMATAN', true);
        $KD_KELURAHAN = $this->input->post('KD_KELURAHAN', true);
        //$kode_cari=$this->input->post('cari',true);

        if ($KD_KECAMATAN <> '') {
            $kel = $this->db->query("select kd_kelurahan,nm_kelurahan 
                                    from ref_kelurahan
                                    where kd_kecamatan='$KD_KECAMATAN'")->result();
        } else {
            $kel = null;
        }

        $data = array(
            'tahun'        => $tahun,
            'kec'          => $kec,
            'kel'          => $kel,
            'KD_KELURAHAN' => $KD_KELURAHAN,
            'KD_KECAMATAN' => $KD_KECAMATAN
        );


        $this->template->load('template', 'datahasilrekaman/dhrlengkap', $data);
    }

    function dhrexcel()
    {
        ini_set('max_execution_time', 300);
        $KD_KECAMATAN = urldecode($this->input->get('kec', true));
        $KD_KELURAHAN = urldecode($this->input->get('kel', true));
        $tahun = urldecode($this->input->get('tahun'));

        $where = "";
        if ($KD_KECAMATAN <> '') {
            $where .= " AND a.kd_kecamatan='" . $KD_KECAMATAN . "'";
        }
        if ($KD_KELURAHAN <> '') {
            $where .= " AND a.kd_kelurahan='" . $KD_KELURAHAN . "'";
        }

        $kecamatan = $this->db->query("select  distinct kode_kec, nm_kecamatan, kode_kel,nm_kelurahan from (
                                    SELECT a.kd_kecamatan as kode_kec, b.nm_kecamatan,a.kd_kelurahan as kode_kel,c.kd_kelurahan, c.nm_kelurahan
                                        FROM MV_DHR a
                                    LEFT JOIN  REF_KECAMATAN b
                                    ON a.kd_kecamatan = b.kd_kecamatan
                                    JOIN REF_KELURAHAN c
                                    ON a.kd_kecamatan = c.kd_kecamatan
                                    AND a.kd_kelurahan = c.kd_kelurahan
                                    where extract(YEAR from a.TGL_PENDATAAN_OP) = '$tahun' $where
                                    
                                ) ORDER BY nm_kelurahan asc")->result();

        $header = ['NO', 'NOP', 'FORMULIR SPOP', 'JUMLAH BNG', 'ALAMAT OP', 'RT/RW', 'NAMA WP', 'LUAS BUMI', 'ZNT', 'NILAI BUMI', 'KTP', 'NPWP', 'STATUS WP', 'PEKERJAAN WP', 'PERSIL'];

        $no = 1;
        foreach ($kecamatan as $key) {
            echo $key->KODE_KEC . '/' . $key->KODE_KEL . ' : ' . $key->NM_KECAMATAN . '/' . $key->NM_KELURAHAN;
            echo '<br><hr>';
            $no++;
        }

        echo 'JUMLAH DATA : ' . $no;
        exit();
        //setup Spout Excel Writer, set tipenya xlsx
        $writer = WriterFactory::create(Type::XLSX);
        // download to browser
        $writer->openToBrowser('datahasilrekaman' . date('YmdHis') . '.xlsx');
        // set style untuk header
        $headerStyle = (new StyleBuilder())
            ->setFontBold()
            ->build();

        $first = 1;
        foreach ($kecamatan as $res) {

            $sql = $this->db->query("SELECT * FROM MV_DHR where extract(YEAR from TGL_PENDATAAN_OP) = '$tahun' AND KD_KECAMATAN=" . $res->KODE_KEC . " AND KD_KELURAHAN =" . $res->KODE_KEL)->result();

            $arrisi = array();
            $no = 1;
            foreach ($sql as $rk) {
                $nop = $rk->KD_PROPINSI . '.' . $rk->KD_DATI2 . '.' . $rk->KD_KECAMATAN . '.' . $rk->KD_KELURAHAN . '.' . $rk->KD_BLOK . '.' . $rk->NO_URUT . '.' . $rk->KD_JNS_OP;
                $rt_rw = $rk->RT_OP . '/' . $rk->RW_OP;
                $ktp = "'" . $rk->KD_PROPINSI . $rk->KD_DATI2 . $rk->KD_KECAMATAN . $rk->KD_KELURAHAN . $rk->KD_BLOK . $rk->NO_URUT . $rk->KD_JNS_OP;
                $sts_wp = $rk->KD_STATUS_WP . ' - PEMILIK';

                $cc  = $no . '+' . $nop . '+' . $rk->NO_FORMULIR_SPOP . '+' . '' . '+' . $rk->ALAMAT_OP . '+' . $rt_rw . '+' . $rk->NM_WP_SPPT . '+' . $rk->LUAS_BUMI . '+' . $rk->KD_ZNT . '+' . $rk->NILAI_SISTEM_BUMI . '+' . $ktp . '+' . '' . '+' . $sts_wp . '+' . '' . '+' . $rk->NO_PERSIL;

                $ff  = explode('+', $cc);
                array_push($arrisi, $ff);
                $no++;

                // echo json_encode(array('result' => $arrisi ));
                //echo $cc."<br><hr>";
            }

            if ($first == 1) {
                // write ke Sheet pertama
                $nama = $res->NM_KELURAHAN . ' - Kec.' . $res->NM_KECAMATAN;
                $writer->getCurrentSheet()->setName($nama);
                // header Sheet pertama
                $writer->addRowWithStyle($header, $headerStyle);
                // data Sheet pertama
                $writer->addRows($arrisi);
            } else {
                $nama = $res->NM_KELURAHAN . ' - Kec.' . $res->NM_KECAMATAN;
                // write ke Sheet kedua
                $writer->addNewSheetAndMakeItCurrent()->setName($nama);
                // header Sheet kedua
                $writer->addRowWithStyle($header, $headerStyle);
                // data Sheet pertama
                $writer->addRows($arrisi);
            }
            $first++;
        }
        $writer->close();
    }

    //     function dhrLengkapexcel(){
    //         //ini_set('max_execution_time', 300);
    //         $KD_KECAMATAN=urldecode($this->input->get('KD_KECAMATAN',true));
    //         $KD_KELURAHAN=urldecode($this->input->get('KD_KELURAHAN',true));
    //         $tahun =urldecode($this->input->get('tahun')); 
    // //exit();
    //         $where="";
    //         if($KD_KECAMATAN<>''){
    //             $where .=" AND a.kd_kecamatan='". $KD_KECAMATAN."'";
    //         }

    //         if($KD_KELURAHAN<>''){
    //             $where .=" AND a.kd_kelurahan='". $KD_KELURAHAN."'";
    //         }

    //         $kecamatan=$this->db->query("select  distinct kode_kec, nm_kecamatan, kode_kel,nm_kelurahan from (
    //                                     SELECT a.kd_kecamatan as kode_kec, b.nm_kecamatan,a.kd_kelurahan as kode_kel,c.kd_kelurahan, c.nm_kelurahan
    //                                         FROM MV_DHR a
    //                                     LEFT JOIN  REF_KECAMATAN b
    //                                     ON a.kd_kecamatan = b.kd_kecamatan
    //                                     JOIN REF_KELURAHAN c
    //                                     ON a.kd_kecamatan = c.kd_kecamatan
    //                                     AND a.kd_kelurahan = c.kd_kelurahan
    //                                     where extract(YEAR from a.TGL_PENDATAAN_OP) = '$tahun' $where                                    
    //                                 ) ORDER BY nm_kelurahan asc")->result();

    //                 $header=['NO','NOP','FORMULIR SPOP','JUMLAH BNG','ALAMAT OP','RT/RW','NAMA WP', 'LUAS BUMI','ZNT','NILAI BUMI','KTP','NPWP','STATUS WP','PEKERJAAN WP','PERSIL','FORMULIR LSPOP','NO. BANGUNAN','LUAS BANGUNAN','JUMLAH LANTAI','TAHUN DIBANGUN','TAHUN RENOVASI','KONDISI UMUM BANGUNAN','JENIS KONSTRUKSI','JENIS ATAP','JENIS DINDING','JENIS LANTAI','JENIS LANGIT - LANGIT'];

    // //                 $no=1;
    // //                 foreach ($kecamatan as $key) {
    // //                      echo $key->KODE_KEC.'/'.$key->KODE_KEL.' : '.$key->NM_KECAMATAN.'/'.$key->NM_KELURAHAN;
    // //                      echo '<br><hr>';
    // //                      $no++;
    // //                 }

    // //                 echo 'JUMLAH DATA : '.$no;
    // // exit();
    //           //setup Spout Excel Writer, set tipenya xlsx
    //         $writer = WriterFactory::create(Type::XLSX);
    //         // download to browser
    //         $writer->openToBrowser('datahasilrekamanlengkap'.date('YmdHis').'.xlsx');
    //         // set style untuk header
    //         $headerStyle = (new StyleBuilder())
    //                ->setFontBold()
    //                ->build();

    //          $first=1;
    // foreach($kecamatan as $res){

    //             // echo $res->KODE_KEC.'/'.$res->KODE_KEL;
    //             // echo '<br><hr>';
    //             // exit();

    //             $sql=$this->db->query("SELECT * FROM MV_DHR where extract(YEAR from TGL_PENDATAAN_OP) = '$tahun' AND ROWNUM <=10 AND KD_KECAMATAN=".$res->KODE_KEC." AND KD_KELURAHAN =".$res->KODE_KEL.)->result();

    //                 $arrisi=array();
    //                 $no=1;
    //                 foreach($sql as $rk){
    //                     $nop = $rk->KD_PROPINSI.'.'.$rk->KD_DATI2.'.'.$rk->KD_KECAMATAN.'.'.$rk->KD_KELURAHAN.'.'.$rk->KD_BLOK.'.'.$rk->NO_URUT.'.'.$rk->KD_JNS_OP;
    //                     $rt_rw = $rk->RT_OP.'/'.$rk->RW_OP;
    //                     $ktp = "'".$rk->KD_PROPINSI.$rk->KD_DATI2.$rk->KD_KECAMATAN.$rk->KD_KELURAHAN.$rk->KD_BLOK.$rk->NO_URUT.$rk->KD_JNS_OP;
    //                     $sts_wp = $rk->KD_STATUS_WP.' - PEMILIK';

    //                     $cc  =$no .'+'. $nop .'+'. $rk->NO_FORMULIR_SPOP .'+'. '' .'+'. $rk->ALAMAT_OP.'+'. $rt_rw.'+'.$rk->NM_WP_SPPT.'+'. $rk->LUAS_BUMI.'+'. $rk->KD_ZNT.'+'. $rk->NILAI_SISTEM_BUMI.'+'.$ktp.'+'.''.'+'.$sts_wp.'+'.''.'+'.$rk->NO_PERSIL.'+'.$rk->NO_FORMULIR_LSPOP.'+'.$rk->NO_BNG.'+'.$rk->LUAS_BNG.'+'.$rk->JML_LANTAI_BNG.'+'.$rk->THN_DIBANGUN_BNG.'+'.$rk->THN_RENOVASI_BNG.'+'.$rk->KONDISI_BNG.'+'.$rk->JNS_KONSTRUKSI_BNG.'+'.$rk->JNS_ATAP_BNG.'+'.$rk->KD_DINDING.'+'.$rk->KD_LANTAI.'+'.$rk->KD_LANGIT_LANGIT;

    //                     $ff  =explode('+',$cc);
    //                     array_push($arrisi,$ff);
    //                     $no++;

    //                     //echo json_encode(array('result' => $arrisi ));
    //                     //echo $cc."<br><hr>";
    //                 }

    //         //     exit();
    //         if($first==1){
    //               // write ke Sheet pertama
    //                 $nama = $res->NM_KELURAHAN.' - Kec.'.$res->NM_KECAMATAN;
    //                 $writer->getCurrentSheet()->setName($nama);
    //                 // header Sheet pertama
    //                 $writer->addRowWithStyle($header, $headerStyle);
    //                 // data Sheet pertama
    //                 $writer->addRows($arrisi);
    //             }else{
    //                 $nama = $res->NM_KELURAHAN.' - Kec.'.$res->NM_KECAMATAN;
    //                 // write ke Sheet kedua
    //                 $writer->addNewSheetAndMakeItCurrent()->setName($nama);
    //                 // header Sheet kedua
    //                 $writer->addRowWithStyle($header, $headerStyle);
    //                 // data Sheet pertama
    //                 $writer->addRows($arrisi);
    //             }
    //         $first++;
    //         }
    //         $writer->close();

    //     }

    //     function dhrexcel2(){
    //         $KD_KECAMATAN=urldecode($this->input->get('kec',true));
    //         $KD_KELURAHAN=urldecode($this->input->get('kel',true));
    //         $tahun =urldecode($this->input->get('tahun')); 

    //         $where="";
    //         if($KD_KECAMATAN<>''){
    //             $where .=" AND KD_KECAMATAN='". $KD_KECAMATAN."'";
    //         }

    //         if($KD_KELURAHAN<>''){
    //             $where .=" AND KD_KELURAHAN='". $KD_KELURAHAN."'";
    //         }

    //         $data['rk'] = $this->db->query("SELECT * FROM MV_DHR where extract(YEAR from TGL_PENDATAAN_OP) = '$tahun' $where")->result();

    //          //print_r($data);
    //         $this->load->view('datahasilrekaman/dhrexcel',$data);
    //     }

}
