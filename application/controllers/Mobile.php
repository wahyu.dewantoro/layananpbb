<?php
defined('BASEPATH') or exit('No direct script access allowed');
/**
 * 
 */
class Mobile extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        if ($this->session->userdata('pbb') <> 1) {
            $this->session->set_flashdata('notif', '<div class="badge">
                    Silahkan login dengan username dan password anda.</p>
                    </div>');
            redirect('auth');
        }
        $this->load->library('form_validation');
    }

    function index()
    {
        $row = $this->db->query("SELECT * FROM CONFIG_MOBILE WHERE ID=1")->row();

        if ($row) {
            $data = array(
                'button'      => 'Update Status',
                'action'      => site_url('mobile/update_action'),
                'kode_config'   => set_value('kode_config', $row->ID),
                'status'   => set_value('active', $row->ACTIVE),
                'keterangan'   => set_value('keterangan', $row->KETERANGAN),
            );
            $this->template->load('template', 'mobile/menu_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('mobile'));
        }
    }

    public function update_action()
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('kode_config', TRUE));
        } else {
            $data = array(
                'KETERANGAN' => $this->input->post('keterangan', TRUE),
                'ACTIVE' => $this->input->post('active', TRUE),
            );

            $this->db->where("ID", $this->input->post('kode_config', TRUE));
            $query = $this->db->update("CONFIG_MOBILE", $data);
            $this->db->query("commit");
            $this->session->set_flashdata('message', 'Update Record Success');

            $base = base_url();
            if ($query) {
                echo "<script>alert('Berhasil Update Status !'); document.location= '" . "$base" . "mobile'</script>";
            } else {
                echo "<script>alert('Gagal Update Status !'); document.location= '" . "$base" . "mobile'</script>";
            }
        }
    }

    public function _rules()
    {
        $this->form_validation->set_rules('keterangan', 'keterangan', 'trim|required');
        $this->form_validation->set_rules('active', 'active', 'trim|required');

        $this->form_validation->set_rules('kode_config', 'kode_config', 'trim');
        $this->form_validation->set_error_delimiters('<span class="label label-danger ">', '</span>');
    }
}
