<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class View extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('Mview');
		$this->load->model('Mspop');
		$this->load->model('Mpermohonan');
		if ($this->session->userdata('pbb') <> 1) {
			$this->session->set_flashdata('notif', '<div class="badge">
                    Silahkan login dengan username dan password anda.</p>
                    </div>');
			redirect('auth');
		}
	}
	function detail_op()
	{
		if (isset($_POST['KD_PROPINSI'])) {
			$KD_PROPINSI  = $_POST['KD_PROPINSI'];
			$KD_DATI2     = $_POST['KD_DATI2'];
			$KD_KECAMATAN = $_POST['KD_KECAMATAN'];
			$KD_KELURAHAN = $_POST['KD_KELURAHAN'];
			$KD_BLOK      = $_POST['KD_BLOK'];
			$NO_URUT      = $_POST['NO_URUT'];
			$KD_JNS_OP    = $_POST['KD_JNS_OP'];
			$nop          = $KD_PROPINSI . '.' . $KD_DATI2 . '.' . $KD_KECAMATAN . '.' . $KD_KELURAHAN . '.' . $KD_BLOK . '.' . $NO_URUT . '.' . $KD_JNS_OP;
			$_SESSION['KD_PROPINSI']  = $KD_PROPINSI;
			$_SESSION['KD_DATI2']     = $KD_DATI2;
			$_SESSION['KD_KECAMATAN'] = $KD_KECAMATAN;
			$_SESSION['KD_KELURAHAN'] = $KD_KELURAHAN;
			$_SESSION['KD_BLOK']      = $KD_BLOK;
			$_SESSION['NO_URUT']      = $NO_URUT;
			$_SESSION['KD_JNS_OP']    = $KD_JNS_OP;

			$data['objek_pajak']  = $this->Mview->getObjekPajak($nop);
			$data['subjek_pajak'] = $this->Mview->getSubjekPajak($nop);
			$data['DATA_BANGUNAN'] = $this->Mview->getDataBangunan($nop);
			$data['SPPT']		  = $this->Mview->getSPPT($nop);
		} else {
			//$data['filter'] ='No Layanan';
			$data['objek_pajak']  = null;
			$data['subjek_pajak'] = null;
			$data['DATA_BANGUNAN'] = null;
			$data['SPPT']		  = null;
			$_SESSION['KD_PROPINSI']  = null;
			$_SESSION['KD_DATI2']     = null;
			$_SESSION['KD_KECAMATAN'] = null;
			$_SESSION['KD_KELURAHAN'] = null;
			$_SESSION['KD_BLOK']      = null;
			$_SESSION['NO_URUT']      = null;
			$_SESSION['KD_JNS_OP']    = null;
		}
		$this->template->load('template', 'view/detail_op', $data);
	}
	function detail_wp()
	{
		if (isset($_POST['SUBJEK_PAJAK_ID'])) {
			$SUBJEK_PAJAK_ID  	  		= $_POST['SUBJEK_PAJAK_ID'];
			$_SESSION['SUBJEK_PAJAK_ID'] = $SUBJEK_PAJAK_ID;

			$data['informasi']    		= $this->Mview->getSubjekPajak($SUBJEK_PAJAK_ID);
			$data['objek_pajak']  		= $this->Mview->wp_getObjekPajak($SUBJEK_PAJAK_ID);
			//$data['piutang']	  		=$this->Mview->wp_piutang($SUBJEK_PAJAK_ID);*/
		} else {
			//$data['filter'] ='No Layanan';
			$data['objek_pajak']  		= null;
			$data['subjek_pajak'] 		= null;
			$data['DATA_BANGUNAN']		= null;
			$data['SPPT']		  		= null;
			$_SESSION['SUBJEK_PAJAK_ID'] = null;
		}
		$this->template->load('template', 'view/detail_wp', $data);
	}
}
