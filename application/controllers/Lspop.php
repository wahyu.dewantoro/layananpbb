<?php
if (!defined('BASEPATH')) exit('No direct script access allowed');

class Lspop extends CI_Controller
{
	function __construct()
	{
		parent::__construct();
		$this->load->model('Mlspop');
		$this->load->model('Mpermohonan');
		if ($this->session->userdata('pbb') <> 1) {
			$this->session->set_flashdata('notif', '<div class="badge">
                    Silahkan login dengan username dan password anda.</p>
                    </div>');
			redirect('auth');
		}
	}


	function form_lspop()
	{
		if (isset($_GET['KD_JNS_OP'])) {
			$JNS_PELAYANAN     = $_GET['KD_JNS_PELAYANAN'];
			//$JNS_FORMULIR      =$_GET['KD_JNS_FORMULIR'];
			$THN_PELAYANAN     = $_GET['THN_PELAYANAN'];
			$BUNDEL_PELAYANAN  = $_GET['BUNDEL_PELAYANAN'];
			$NO_URUT_DALAM     = $_GET['NO_URUT_DALAM'];
			$NO_URUT  		   = $_GET['NO_URUT'];

			$KD_PROPINSI  = $_GET['KD_PROPINSI'];
			$KD_DATI2     = $_GET['KD_DATI2'];
			$KD_KECAMATAN = $_GET['KD_KECAMATAN'];
			$KD_KELURAHAN = $_GET['KD_KELURAHAN'];
			$KD_BLOK      = $_GET['KD_BLOK'];
			$NO_URUT_NOP  = $_GET['NO_URUT_NOP'];
			$KD_JNS_OP    = $_GET['KD_JNS_OP'];
			$NOP 		  = $KD_PROPINSI . $KD_DATI2 . $KD_KECAMATAN . $KD_KELURAHAN . $KD_BLOK . $NO_URUT_NOP . $KD_JNS_OP;

			$KD_PROPINSI_ASAL  = $_GET['KD_PROPINSI_ASAL'];
			$KD_DATI2_ASAL     = $_GET['KD_DATI2_ASAL'];
			$KD_KECAMATAN_ASAL = $_GET['KD_KECAMATAN_ASAL'];
			$KD_KELURAHAN_ASAL = $_GET['KD_KELURAHAN_ASAL'];
			$KD_BLOK_ASAL      = $_GET['KD_BLOK_ASAL'];
			$NO_URUT_ASAL  	   = $_GET['NO_URUT_ASAL'];
			$KD_JNS_OP_ASAL    = $_GET['KD_JNS_OP_ASAL'];
			$NOP_ASAL		   = $KD_PROPINSI_ASAL . $KD_DATI2_ASAL . $KD_KECAMATAN_ASAL . $KD_KELURAHAN_ASAL . $KD_BLOK_ASAL . $NO_URUT_ASAL . $KD_JNS_OP_ASAL;
			$NO_BNG    		   = $_GET['NO_BNG'];

			$_SESSION['JNS_PELAYANAN']   = $JNS_PELAYANAN;
			//$_SESSION['JNS_FORMULIR']    =$JNS_FORMULIR;
			$_SESSION['THN_PELAYANAN']   = $THN_PELAYANAN;
			$_SESSION['BUNDEL_PELAYANAN'] = $BUNDEL_PELAYANAN;
			$_SESSION['NO_URUT_DALAM']   = $NO_URUT_DALAM;
			$_SESSION['NO_URUT']	     = $NO_URUT;
			$_SESSION['KD_PROPINSI']  	 = $KD_PROPINSI;
			$_SESSION['KD_DATI2']     	 = $KD_DATI2;
			$_SESSION['KD_KECAMATAN'] 	 = $KD_KECAMATAN;
			$_SESSION['KD_KELURAHAN'] 	 = $KD_KELURAHAN;
			$_SESSION['KD_BLOK']      	 = $KD_BLOK;
			$_SESSION['NO_URUT_NOP']  	 = $NO_URUT_NOP;
			$_SESSION['KD_JNS_OP']    	 = $KD_JNS_OP;

			$_SESSION['KD_PROPINSI_ASAL']  	 = $KD_PROPINSI_ASAL;
			$_SESSION['KD_DATI2_ASAL']     	 = $KD_DATI2_ASAL;
			$_SESSION['KD_KECAMATAN_ASAL'] 	 = $KD_KECAMATAN_ASAL;
			$_SESSION['KD_KELURAHAN_ASAL'] 	 = $KD_KELURAHAN_ASAL;
			$_SESSION['KD_BLOK_ASAL']      	 = $KD_BLOK_ASAL;
			$_SESSION['NO_URUT_ASAL']  	 	 = $NO_URUT_ASAL;
			$_SESSION['KD_JNS_OP_ASAL']    	 = $KD_JNS_OP_ASAL;
			$_SESSION['NO_BNG']		    	 = $NO_BNG;

			$data['NO_FORMULIR_SPOP']	 = $THN_PELAYANAN . $BUNDEL_PELAYANAN . $NO_URUT_DALAM . $NO_URUT;
			$data['nop_asal']			 = $NOP_ASAL;
			$data['isi'] = $this->db->query("select * from dat_objek_pajak where subjek_pajak_id='$NOP'")->row();
			$this->template->load('template', 'lspop/form_lspop', $data);
		} else {
			$_SESSION['JNS_PELAYANAN']	 = NULL;
			$_SESSION['JNS_FORMULIR'] 	 = NULL;
			$_SESSION['THN_PELAYANAN']   = NULL;
			$_SESSION['BUNDEL_PELAYANAN'] = NULL;
			$_SESSION['NO_URUT_DALAM']   = NULL;
			$_SESSION['NO_URUT']	     = NULL;
			$_SESSION['KD_PROPINSI']  	 = NULL;
			$_SESSION['KD_DATI2']     	 = NULL;
			$_SESSION['KD_KECAMATAN'] 	 = NULL;
			$_SESSION['KD_KELURAHAN'] 	 = NULL;
			$_SESSION['KD_BLOK']      	 = NULL;
			$_SESSION['NO_URUT_NOP']  	 = NULL;
			$_SESSION['KD_JNS_OP']    	 = NULL;
			$_SESSION['NO_BNG']			 = NULL;

			$_SESSION['KD_PROPINSI_ASAL']  	 = NULL;
			$_SESSION['KD_DATI2_ASAL']     	 = NULL;
			$_SESSION['KD_KECAMATAN_ASAL'] 	 = NULL;
			$_SESSION['KD_KELURAHAN_ASAL'] 	 = NULL;
			$_SESSION['KD_BLOK_ASAL']      	 = NULL;
			$_SESSION['NO_URUT_ASAL']  	 	 = NULL;
			$_SESSION['KD_JNS_OP_ASAL']    	 = NULL;

			$data['nop_asal']			 = '';
			$data['JNS_PELAYANAN']		 = '';
			$this->template->load('template', 'lspop/form_lspop', $data);
		}
	}
	function create_lspop()
	{
		error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
		$NIP 						= $this->session->userdata('nip');
		$KD_JNS_PELAYANAN			= $_POST['KD_JNS_PELAYANAN'];
		$KD_PROPINSI  				= $_POST['KD_PROPINSI'];
		$KD_DATI2     				= $_POST['KD_DATI2'];
		$KD_KECAMATAN 				= $_POST['KD_KECAMATAN'];
		$KD_KELURAHAN 				= $_POST['KD_KELURAHAN'];
		$KD_BLOK      				= $_POST['KD_BLOK'];
		$NO_URUT_NOP     			= $_POST['NO_URUT_NOP'];
		$KD_JNS_OP    				= $_POST['KD_JNS_OP'];
		$NOP 			            = $KD_PROPINSI . $KD_DATI2 . $KD_KECAMATAN . $KD_KELURAHAN . $KD_BLOK . $NO_URUT_NOP . $KD_JNS_OP;
		$NO_FORMULIR    			= $_POST['NO_FORMULIR_SPOP'];
		$NOP_ASAL 					= $_POST['NOP_ASAL'];
		$NO_BNG  					= $_POST['NO_BNG'];
		////////////////////\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
		$KD_JPB  					= $_POST['KD_JPB'];
		$LUAS_BNG     				= $_POST['LUAS_BNG'];
		$JML_LANTAI_BNG  			= $_POST['JML_LANTAI_BNG'];
		$THN_DIBANGUN_BNG  			= $_POST['THN_DIBANGUN_BNG'];
		$THN_RENOVASI_BNG  			= $_POST['THN_RENOVASI_BNG'];
		$KONDISI_BNG  				= $_POST['KONDISI_BNG'];
		$JNS_KONSTRUKSI_BNG 		= $_POST['JNS_KONSTRUKSI_BNG'];
		$JNS_ATAP_BNG  				= $_POST['JNS_ATAP_BNG'];
		$KD_DINDING  				= $_POST['KD_DINDING'];
		$KD_LANTAI  				= $_POST['KD_LANTAI'];
		$KD_LANGIT_LANGIT  			= $_POST['KD_LANGIT_LANGIT'];
		$DAYA_LISTRIK  				= $_POST['DAYA_LISTRIK'];
		$ACSPLIT  					= $_POST['ACSPLIT'];
		$ACWINDOW  					= $_POST['ACWINDOW'];
		$ACSENTRAL  				= $_POST['ACSENTRAL'];
		$LUAS_KOLAM  				= $_POST['LUAS_KOLAM'];
		$FINISHING_KOLAM  			= $_POST['FINISHING_KOLAM'];
		//////////////////////////////\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
		$TGL_PENDATAAN  			= date('Y-m-d', strtotime($this->input->post('TGL_PENDATAAN', TRUE)));
		$NIP_PENDATA  				= $_POST['NIP_PENDATA'];
		$TGL_PEMERIKSAAN  			= date('Y-m-d', strtotime($this->input->post('TGL_PEMERIKSAAN', TRUE)));
		$NIP_PEMERIKSA  			= $_POST['NIP_PEMERIKSA'];

		$LIFT_PENUMPANG  			= $_POST['LIFT_PENUMPANG'];
		$LIFT_KAPSUL  				= $_POST['LIFT_KAPSUL'];
		$LIFT_BARANG  				= $_POST['LIFT_BARANG'];
		////////////////////////////////\\\\\\\\\\\\\\\\\\\\\\\\\\\\\			
		$TGG_BERJALAN_A  			= $_POST['TGG_BERJALAN_A'];
		$TGG_BERJALAN_B  			= $_POST['TGG_BERJALAN_B'];
		$PJG_PAGAR  				= $_POST['PJG_PAGAR'];
		$BHN_PAGAR  				= $_POST['BHN_PAGAR'];
		$HYDRANT  					= $_POST['HYDRANT'];
		$SPRINKLER  				= $_POST['SPRINKLER'];
		$FIRE_ALARM  				= $_POST['FIRE_ALARM'];
		$JML_PABX  					= $_POST['JML_PABX'];
		$SUMUR_ARTESIS  			= $_POST['SUMUR_ARTESIS'];
		$LUAS_PERKERASAN_RINGAN  	= $_POST['LUAS_PERKERASAN_RINGAN'];
		$LUAS_PERKERASAN_SEDANG  	= $_POST['LUAS_PERKERASAN_SEDANG'];
		$LUAS_PERKERASAN_BERAT  	= $_POST['LUAS_PERKERASAN_BERAT'];
		$LUAS_PERKERASAN_DG_TUTUP  	= $_POST['LUAS_PERKERASAN_DG_TUTUP'];

		$LAP_TENIS_LAMPU_BETON  	= $_POST['LAP_TENIS_LAMPU_BETON'];
		$LAP_TENIS_LAMPU_ASPAL  	= $_POST['LAP_TENIS_LAMPU_ASPAL'];
		$LAP_TENIS_LAMPU_RUMPUT  	= $_POST['LAP_TENIS_LAMPU_RUMPUT'];
		$LAP_TENIS_BETON  			= $_POST['LAP_TENIS_BETON'];
		$LAP_TENIS_ASPAL  			= $_POST['LAP_TENIS_ASPAL'];
		$LAP_TENIS_RUMPUT  			= $_POST['LAP_TENIS_RUMPUT'];

		$this->db->trans_begin();
		$this->db->query("INSERT INTO TBL_LSPOP (NO_FORMULIR,JNS_TRANSAKSI,NOP,NO_BNG,
											KD_JPB,THN_DIBANGUN_BNG,THN_RENOVASI_BNG,LUAS_BNG,
											JML_LANTAI_BNG,KONDISI_BNG,JNS_KONSTRUKSI_BNG,JNS_ATAP_BNG,KD_DINDING,KD_LANTAI,KD_LANGIT_LANGIT,
											DAYA_LISTRIK,ACSPLIT,ACWINDOW,ACSENTRAL,
											LUAS_KOLAM,FINISHING_KOLAM,
											LUAS_PERKERASAN_RINGAN,LUAS_PERKERASAN_SEDANG,LUAS_PERKERASAN_BERAT,LUAS_PERKERASAN_DG_TUTUP,
											LAP_TENIS_LAMPU_BETON,LAP_TENIS_LAMPU_ASPAL,LAP_TENIS_LAMPU_RUMPUT,LAP_TENIS_BETON,LAP_TENIS_ASPAL,LAP_TENIS_RUMPUT,
											LIFT_PENUMPANG,LIFT_KAPSUL,LIFT_BARANG,TGG_BERJALAN_A,TGG_BERJALAN_B,PJG_PAGAR,BHN_PAGAR,
											HYDRANT,SPRINKLER,FIRE_ALARM,JML_PABX,SUMUR_ARTESIS,NILAI_INDIVIDU,
											JPB3_8_TINGGI_KOLOM,JPB3_8_LEBAR_BENTANG,JPB3_8_DD_LANTAI,JPB3_8_KEL_DINDING,JPB3_8_MEZZANINE,
											JPB5_KLS_BNG,JPB5_LUAS_KAMAR,JPB5_LUAS_RNG_LAIN,
											JPB7_JNS_HOTEL,JPB7_BINTANG,JPB7_JML_KAMAR,JPB7_LUAS_KAMAR,JPB7_LUAS_RNG_LAIN,
											JPB13_KLS_BNG,JPB13_JML,JPB13_LUAS_KAMAR,JPB13_LUAS_RNG_LAIN,
											JPB15_LETAK_TANGKI,JPB15_KAPASITAS_TANGKI,JPB_LAIN_KLS_BNG,
											TGL_PENDATAAN_BNG,NIP_PENDATA_BNG,TGL_PEMERIKSAAN_BNG,NIP_PEMERIKSA_BNG,TGL_PEREKAMAN_BNG,NIP_PEREKAM_BNG) 
											VALUES ('$NO_FORMULIR','$KD_JNS_PELAYANAN','$NOP','$NO_BNG',
											'$KD_JPB','$THN_DIBANGUN_BNG','$THN_RENOVASI_BNG','$LUAS_BNG',
											'$JML_LANTAI_BNG','$KONDISI_BNG','$JNS_KONSTRUKSI_BNG','$JNS_ATAP_BNG','$KD_DINDING','$KD_LANTAI','$KD_LANGIT_LANGIT',
											'$DAYA_LISTRIK','$ACSPLIT','$ACWINDOW','$ACSENTRAL',
											'$LUAS_KOLAM','$FINISHING_KOLAM',
											'$LUAS_PERKERASAN_RINGAN','$LUAS_PERKERASAN_SEDANG','$LUAS_PERKERASAN_BERAT','$LUAS_PERKERASAN_DG_TUTUP',
											'$LAP_TENIS_LAMPU_BETON','$LAP_TENIS_LAMPU_ASPAL','$LAP_TENIS_LAMPU_RUMPUT','$LAP_TENIS_BETON','$LAP_TENIS_ASPAL','$LAP_TENIS_RUMPUT',
											'$LIFT_PENUMPANG','$LIFT_KAPSUL','$LIFT_BARANG','$TGG_BERJALAN_A','$TGG_BERJALAN_B','$PJG_PAGAR','$BHN_PAGAR',
											'$HYDRANT','$SPRINKLER','$FIRE_ALARM','$JML_PABX','$SUMUR_ARTESIS','0',
											'','','','','',
											'','','',
											'','','','','',
											'','','','',
											'','','',
											to_date('$TGL_PENDATAAN','YYYY-MM-DD'),'$NIP_PENDATA',to_date('$TGL_PEMERIKSAAN','YYYY-MM-DD'),'$NIP_PEMERIKSA',to_date('$TGL_PENDATAAN','YYYY-MM-DD'),'$NIP')");
		$this->db->query("INSERT INTO DAT_OP_BANGUNAN 
												(KD_PROPINSI,KD_DATI2,KD_KECAMATAN,
												KD_KELURAHAN,KD_BLOK,NO_URUT,
												KD_JNS_OP,NO_BNG,KD_JPB,
												NO_FORMULIR_LSPOP,THN_DIBANGUN_BNG,THN_RENOVASI_BNG,
												LUAS_BNG,JML_LANTAI_BNG,KONDISI_BNG,JNS_KONSTRUKSI_BNG,JNS_ATAP_BNG,KD_DINDING,KD_LANTAI,KD_LANGIT_LANGIT,
												NILAI_SISTEM_BNG,JNS_TRANSAKSI_BNG,

												TGL_PENDATAAN_BNG,NIP_PENDATA_BNG,
												TGL_PEMERIKSAAN_BNG,NIP_PEMERIKSA_BNG,
												TGL_PEREKAMAN_BNG,NIP_PEREKAM_BNG) 
												VALUES 
												('$KD_PROPINSI','$KD_DATI2','$KD_KECAMATAN',
												'$KD_KELURAHAN','$KD_BLOK','$NO_URUT_NOP',
												'$KD_JNS_OP','$NO_BNG','$KD_JPB',
												'$NO_FORMULIR','$THN_DIBANGUN_BNG','$THN_RENOVASI_BNG',
												'$LUAS_BNG','$JML_LANTAI_BNG','$KONDISI_BNG','$JNS_KONSTRUKSI_BNG','$JNS_ATAP_BNG','$KD_DINDING','$KD_LANTAI','$KD_LANGIT_LANGIT',
												'0','$KD_JNS_PELAYANAN',

												to_date('$TGL_PENDATAAN','YYYY-MM-DD'),'$NIP_PENDATA',
												to_date('$TGL_PEMERIKSAAN','YYYY-MM-DD'),'$NIP_PEMERIKSA',
												to_date('$TGL_PENDATAAN','YYYY-MM-DD'),'$NIP')");
		$this->db->query("UPDATE P_NOMOR SET STATUS = '1' WHERE  NO_FORMULIR= '$NO_FORMULIR' AND NOP = '$NOP'");
		$rk = $this->db->query("select NO_FORMULIR from p_nomor where nop='$NOP' and keterangan_nomor='LSPOP' AND STATUS='0' order by no_bangunan asc")->row();

		$this->db->trans_complete();

		if ($this->db->trans_status() === TRUE) {
			// if (count($rk)<=0) {
			redirect('lspop/form_lspop/');
		} else {
			redirect('spop/form_lspop/');
			/*echo "<script>window.alert('Entry Data Bangunan Berikutnya '); window.location='".base_url().'lspop/form_lspop_lanjutan/'.$rk->NO_FORMULIR."';</script>";*/
			//echo	$cek=$this->db->query("select no_formulir from p_nomor where nop='$NOP' and keterangan_nomor='LSPOP' AND STATUS='0' order by no_bangunan asc")->row;		
			//redirect('lspop/form_lspop/');

		}
	}
	function form_lspop_lanjutan($formulir = "")
	{
		$data['formulir'] = $formulir;
		$data['rk'] = $this->db->query("select tahun,jenis_pelayanan,bundel,no_urut,NO_BANGUNAN,nop from p_nomor where no_formulir='$formulir' AND status='0'")->row();
		$this->template->load('template', 'lspop/form_lspop_next', $data);
	}
	function tambah_bangunan($nop = "")
	{
		$SUB_KD_PROPINSI  	  = substr($nop, 0, 2);
		$SUB_KD_DATI2     	  = substr($nop, 2, 2);
		$SUB_KD_KECAMATAN 	  = substr($nop, 4, 3);
		$SUB_KD_KELURAHAN 	  = substr($nop, 7, 3);
		$SUB_KD_BLOK      	  = substr($nop, 10, 3);
		$SUB_NO_URUT_NOP      = substr($nop, 13, 4);
		$SUB_KD_JNS_OP    	  = substr($nop, 17, 1);
		$nip 				  = $this->session->userdata('nip');
		$data['KD_PROPINSI']  = $SUB_KD_PROPINSI;
		$data['KD_DATI2']	  = $SUB_KD_DATI2;
		$data['KD_KECAMATAN'] = $SUB_KD_KECAMATAN;
		$data['KD_KELURAHAN'] = $SUB_KD_KELURAHAN;
		$data['KD_BLOK']	  = $SUB_KD_BLOK;
		$data['NO_URUT_NOP']  = $SUB_NO_URUT_NOP;
		$data['KD_JNS_OP']	  = $SUB_KD_JNS_OP;
		$data['nop']		  = $nop;
		$data['data_bangunan'] = $this->db->query("select max(no_bng)jumlah_bng from P_TEMP_LSPOP where nop='$nop' and nip_perekam_bng='$nip'")->row();
		$this->template->load('template', 'lspop/form_tambah_data_bng', $data);
	}

	function getformulir()
	{
		if (isset($_POST['nop'])) {
			$NOP = $_POST['nop'];
			$exp = explode('=', $NOP);
			$id = str_replace('|', '', $exp[0]);
			$rk = $this->db->query("select tahun,jenis_pelayanan,bundel,no_urut,NO_BANGUNAN from p_nomor where nop='$id' and keterangan_nomor='LSPOP' AND STATUS='0' order by no_bangunan asc")->row();
			error_reporting(E_ALL ^ (E_NOTICE | E_WARNING));
			/*echo $this->db->last_query();
	    		echo "<br>";*/

			echo $rk->TAHUN . '|' . $rk->JENIS_PELAYANAN . '|' . $rk->BUNDEL . '|' . $rk->NO_URUT . '|' . $rk->NO_BANGUNAN;
		}
	}
	function getNop()
	{
		if (isset($_POST['nop'])) {
			$nop = $_POST['nop'];
			$exp = explode('=', $nop);
			$id = str_replace('|', '', $exp[0]);
			$jns = $exp[1];
			$rk = $this->db->query("select * from tbl_spop
									where subjek_pajak_id='$id'")->row();
			if ($rk) {
				$row['value']          = $id;
				$row['nomor_formulir'] = $rk->no_formulir;
				$row_set[]       	   = $row;
			}

			echo $return = json_encode($row_set);
		}
	}

	function getform()
	{
		if (isset($_POST['nop'])) {

			$this->template->load('template', 'spop/ajax_perekaman_wp');
		}
	}

	function dataLspop()
	{
		$data['data'] = $this->db->query("select * from tbl_lspop")->result();
		$this->template->load('template', 'lspop/datalSpop', $data);
	}
}
