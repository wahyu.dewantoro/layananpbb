<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Mbank extends CI_Model
{

    public $table = 'REF_BANK';
    public $id = 'ID';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    }

    // datatables
    function json()
    {
        $this->datatables->select('ID,NAMA_BANK,KODE_BANK');
        $this->datatables->from('REF_BANK');
        $this->datatables->add_column('action', '<div class="btn-group">' . anchor(site_url('bank/update/$1'), '<i class="fa fa-edit"></i>', 'class="btn btn-xs btn-success"') . anchor(site_url('bank/delete/$1'), '<i class="fa fa-trash"></i>', 'class="btn btn-xs btn-danger" onclick="javasciprt: return confirm(\'Apakah anda yakin?\')"') . '</div>', 'ID');
        return $this->datatables->generate();
    }

    // get all
    function get_all()
    {
        $this->db->order_by($this->id, $this->order);
        return $this->db->get($this->table)->result();
    }

    // get data by id
    function get_by_id($id)
    {
        $this->db->where($this->id, $id);
        return $this->db->get($this->table)->row();
    }

    // get total rows
    function total_rows($q = NULL)
    {
        $this->db->like('kode_group', $q);
        $this->db->or_like('nama_group', $q);
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    // get data with limit and search
    function get_limit_data($limit, $start = 0, $q = NULL)
    {
        $this->db->order_by($this->id, $this->order);
        $this->db->like('kode_group', $q);
        $this->db->or_like('nama_group', $q);
        $this->db->limit($limit, $start);
        return $this->db->get($this->table)->result();
    }

    // insert data
    function insert($data)
    {
        $this->db->insert($this->table, $data);
    }

    // update data
    function update($id, $data)
    {
        $this->db->where($this->id, $id);
        $this->db->update($this->table, $data);
    }

    // delete data
    function delete($id)
    {
        $this->db->where($this->id, $id);
        $this->db->delete($this->table);
    }

    function asd()
    { }
}

/* End of file Mgroup.php */
/* Location: ./application/models/Mgroup.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2017-04-28 01:25:45 */
/* http://harviacode.com */
