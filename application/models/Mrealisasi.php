<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Mrealisasi extends CI_Model
{

    public $table = 'REF_BANK';
    public $id = 'ID';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    }

    // datatables
    function json()
    {
        $this->datatables->select('ID,NAMA_BANK,KODE_BANK');
        $this->datatables->from('REF_BANK');
        $this->datatables->add_column('action', '<div class="btn-group">' . anchor(site_url('bank/update/$1'), '<i class="fa fa-edit"></i>', 'class="btn btn-xs btn-success"') . anchor(site_url('bank/delete/$1'), '<i class="fa fa-trash"></i>', 'class="btn btn-xs btn-danger" onclick="javasciprt: return confirm(\'Apakah anda yakin?\')"') . '</div>', 'ID');
        return $this->datatables->generate();
    }


    function jsonRealisasiNominatif($tahun, $kd_kecamatan, $kd_kelurahan, $buku)
    {
        $tb = $this->queryNominatif($tahun, $kd_kecamatan, $kd_kelurahan, $buku);
        $this->datatables->select("NOP,ALAMAT_OP,RTRW_OP,LUAS_BUMI_SPPT,LUAS_BNG_SPPT,NM_WP_SPPT,ALAMAT_WP,RTRW_WP,KELURAHAN_WP_SPPT,KOTA_WP_SPPT,PBB,BUKU");
        $this->datatables->from("( $tb )");

        if ($buku != null) {
            $this->db->where('BUKU', $buku);
        }

        return $this->datatables->generate();
    }

    function queryNominatif($tahun, $kd_kecamatan, $kd_kelurahan, $buku = null)
    {


        return $tb = "SELECT    A.KD_PROPINSI || '.'|| A.KD_DATI2 || '.'|| A.KD_KECAMATAN || '.'|| A.KD_KELURAHAN || '.'|| A.KD_BLOK || '-'|| A.NO_URUT || '.'|| A.KD_JNS_OP NOP, GET_KELURAHAN (A.KD_KELURAHAN, A.KD_KECAMATAN) ALAMAT_OP, RT_OP || '/' || RW_OP RTRW_OP, LUAS_BUMI_SPPT, LUAS_BNG_SPPT, NM_WP_SPPT, JLN_WP_SPPT || ' ' || BLOK_KAV_NO_WP_SPPT ALAMAT_WP, RT_WP_SPPT || '/' || RW_WP_SPPT RTRW_WP, KELURAHAN_WP_SPPT, KOTA_WP_SPPT, PBB_YG_HARUS_DIBAYAR_SPPT PBB, CASE WHEN A.PBB_YG_HARUS_DIBAYAR_SPPT <= 100000 THEN 'Buku I'WHEN A.PBB_YG_HARUS_DIBAYAR_SPPT > 100000 AND A.PBB_YG_HARUS_DIBAYAR_SPPT <= 500000 THEN 'Buku II'WHEN A.PBB_YG_HARUS_DIBAYAR_SPPT > 500000 AND A.PBB_YG_HARUS_DIBAYAR_SPPT <= 2000000 THEN 'Buku III'WHEN A.PBB_YG_HARUS_DIBAYAR_SPPT > 2000000 AND A.PBB_YG_HARUS_DIBAYAR_SPPT <= 5000000 THEN 'Buku IV'ELSE 'Buku V'END BUKU, THN_PAJAK_SPPT 
        FROM    SPPT A
          JOIN DAT_OBJEK_PAJAK B ON     A.KD_PROPINSI = B.KD_PROPINSI AND A.KD_DATI2 = B.KD_DATI2 AND A.KD_KECAMATAN = B.KD_KECAMATAN AND A.KD_KELURAHAN = B.KD_KELURAHAN AND A.KD_BLOK = B.KD_BLOK AND A.NO_URUT = B.NO_URUT AND A.KD_JNS_OP = B.KD_JNS_OP where thn_pajak_sppt ='$tahun' and a.kd_kecamatan='$kd_kecamatan' and a.kd_kelurahan ='$kd_kelurahan' ";
    }


    function getDataNominatif($tahun, $kd_kecamatan, $kd_kelurahan, $buku)
    {
        $tb = $this->queryNominatif($tahun, $kd_kecamatan, $kd_kelurahan);
        if ($buku != null) {
            $this->db->where('BUKU', $buku);
        }
        return $this->db->get("(" . $tb . ")")->result();
    }

    function jsondesalunas()
    {

        $tb = "SELECT  ID,NM_KECAMATAN,NM_KELURAHAN,TAHUN_PAJAK,BAKU,to_char(tgl_lunas,'dd-mm-yyyy') TGL_LUNAS
FROM DESA_LUNAS A
JOIN REF_KECAMATAN B ON B.KD_KECAMATAN=A.KD_KECAMATAN
JOIN REF_KELURAHAN C ON C.KD_KELURAHAN=A.KD_KELURAHAN AND C.KD_KECAMATAN=A.KD_KECAMATAN
ORDER BY TGL_LUNAS ASC";

        $this->datatables->select("ID,NM_KECAMATAN,NM_KELURAHAN,TAHUN_PAJAK,BAKU,TGL_LUNAS");
        $this->datatables->from("( $tb )");

        return $this->datatables->generate();
    }


    function jsonrealisasiPokok()
    {
        $this->datatables->select("BUKU,BAKU,POKOK_ML,DENDA_ML,JUMLAH_ML,POKOK_MI,DENDA_MI,JUMLAH_MI,POKOK_SMI,DENDA_SMI,JUMLAH_SMI,PERSEN,SISA");
        $this->datatables->from('(select BUKU,BAKU,POKOK_ML,DENDA_ML,JUMLAH_ML,POKOK_MI,DENDA_MI,JUMLAH_MI,POKOK_SMI,DENDA_SMI,JUMLAH_SMI,round(persen) PERSEN,SISA from MV_REALISASI)');
        return  $this->datatables->generate();
    }


    /*function jsonrealisasiPokoktigaup(){
      $tb="SELECT NM_KECAMATAN, JUM_SPPT, BAKU, SPPT_REALISASI, POKOK_REALISASI, DENDA_REALISASI, JUMLAH_REALISASI, SPPT_REALISASI, SPPT_SISA, POKOK_SISA, PERSEN_REALISASI
FROM MV_REALISASI_TIGA_EMPAT_LIMA";

        $this->datatables->select("NM_KECAMATAN,JUM_SPPT,BAKU,SPPT_REALISASI,POKOK_REALISASI,DENDA_REALISASI,JUMLAH_REALISASI,SPPT_REALISASI,SPPT_SISA,POKOK_SISA,PERSEN_REALISASI",false);
        $this->datatables->from("($tb)");
        return  $this->datatables->generate();
     }*/

    /* // get all
    function get_all()
    {
        $this->db->order_by($this->id, $this->order);
        return $this->db->get($this->table)->result();
    }

    // get data by id
    function get_by_id($id)
    {
        $this->db->where($this->id, $id);
        return $this->db->get($this->table)->row();
    }
    
    // get total rows
    function total_rows($q = NULL) {
        $this->db->like('kode_group', $q);
	$this->db->or_like('nama_group', $q);
	$this->db->from($this->table);
        return $this->db->count_all_results();
    }

    // get data with limit and search
    function get_limit_data($limit, $start = 0, $q = NULL) {
        $this->db->order_by($this->id, $this->order);
        $this->db->like('kode_group', $q);
	$this->db->or_like('nama_group', $q);
	$this->db->limit($limit, $start);
        return $this->db->get($this->table)->result();
    }

    // insert data
    function insert($data)
    {
        $this->db->insert($this->table, $data);
    }

    // update data
    function update($id, $data)
    {
        $this->db->where($this->id, $id);
        $this->db->update($this->table, $data);
    }

    // delete data
    function delete($id)
    {
        $this->db->where($this->id, $id);
        $this->db->delete($this->table);
    }*/
}

/* End of file Mgroup.php */
/* Location: ./application/models/Mgroup.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2017-04-28 01:25:45 */
/* http://harviacode.com */
