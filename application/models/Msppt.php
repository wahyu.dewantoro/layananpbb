<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Msppt extends CI_Model
{



    function __construct()
    {
        parent::__construct();
    }

    function getRIwayatbyNopBelumLunas($nop = array(), $batal = null)
    {
        $KD_PROPINSI  = $nop['KD_PROPINSI'];
        $KD_DATI2     = $nop['KD_DATI2'];
        $KD_KECAMATAN = $nop['KD_KECAMATAN'];
        $KD_KELURAHAN = $nop['KD_KELURAHAN'];
        $KD_BLOK      = $nop['KD_BLOK'];
        $NO_URUT      = $nop['NO_URUT'];
        $KD_JNS_OP    = $nop['KD_JNS_OP'];
        $cetak        = $nop['cetak'];
        $and = ' ';
        // if ($cetak == true) {
            $and .= "AND STATUS_PEMBAYARAN_SPPT=0 AND THN_PAJAK_SPPT >=2003";
        // }
        $res = $this->db->query("SELECT SPT.KD_PROPINSI,SPT.KD_DATI2, SPT.KD_KECAMATAN, SPT.KD_KELURAHAN, SPT.KD_BLOK, SPT.NO_URUT, SPT.KD_JNS_OP,SPT.KD_PROPINSI||'.'||SPT.KD_DATI2||'.'||SPT.KD_KECAMATAN||'.'|| SPT.KD_KELURAHAN||'.'|| SPT.KD_BLOK||'.'||SPT.NO_URUT||'.'||SPT.KD_JNS_OP NOP,NM_WP_SPPT, 'RT '|| RT_WP_SPPT ||'RW '||RW_WP_SPPT ||KELURAHAN_WP_SPPT ALAMAT, NJOP_BUMI_SPPT, NJOP_BNG_SPPT,PBB_YG_HARUS_DIBAYAR_SPPT ,get_denda(spt.kd_dati2, spt.kd_kecamatan, spt.kd_kelurahan, spt.kd_blok, spt.no_urut, spt.kd_jns_op,spt.thn_pajak_sppt,pbb_yg_harus_dibayar_sppt,tgl_jatuh_tempo_sppt, case when STATUS_PEMBAYARAN_SPPT= 0 then sysdate else TGL_PEMBAYARAN_SPPT end ) denda, STATUS_PEMBAYARAN_SPPT, to_char(TGL_PEMBAYARAN_SPPT ,'dd/mm/yyyy') TGL_BAYAR,SPT.THN_PAJAK_SPPT
                            FROM (
                                SELECT * FROM SPPT
                                WHERE KD_PROPINSI = '$KD_PROPINSI'
                                AND KD_DATI2      = '$KD_DATI2'
                                AND KD_KECAMATAN  = '$KD_KECAMATAN'
                                AND KD_KELURAHAN  = '$KD_KELURAHAN'
                                AND KD_BLOK       = '$KD_BLOK'
                                AND NO_URUT       = '$NO_URUT'
                                AND KD_JNS_OP     ='$KD_JNS_OP'   
                                $and
                                ) SPT
                                left join  PEMBAYARAN_SPPT PS
                                on  SPT.KD_KECAMATAN = PS.KD_KECAMATAN
                                AND SPT.KD_KELURAHAN = PS.KD_KELURAHAN
                                AND SPT.KD_BLOK =  PS.KD_BLOK
                                AND SPT.NO_URUT =  PS.NO_URUT
                                AND SPT.KD_JNS_OP = PS.KD_JNS_OP
                                AND SPT.THN_PAJAK_SPPT = PS.THN_PAJAK_SPPT order by SPT.THN_PAJAK_SPPT asc")->result();
        return $res;
    }

    function getRIwayatbyNop($nop = array(), $batal = null)
    {
        $KD_PROPINSI  = $nop['KD_PROPINSI'];
        $KD_DATI2     = $nop['KD_DATI2'];
        $KD_KECAMATAN = $nop['KD_KECAMATAN'];
        $KD_KELURAHAN = $nop['KD_KELURAHAN'];
        $KD_BLOK      = $nop['KD_BLOK'];
        $NO_URUT      = $nop['NO_URUT'];
        $KD_JNS_OP    = $nop['KD_JNS_OP'];
        $cetak        = $nop['cetak'];
        $and = ' ';
        if ($cetak == true) {
            $and .= "AND STATUS_PEMBAYARAN_SPPT!=3 AND THN_PAJAK_SPPT >=2003";
        }
        $res = $this->db->query("SELECT SPT.KD_PROPINSI,SPT.KD_DATI2, SPT.KD_KECAMATAN, SPT.KD_KELURAHAN, SPT.KD_BLOK, SPT.NO_URUT, SPT.KD_JNS_OP,SPT.KD_PROPINSI||'.'||SPT.KD_DATI2||'.'||SPT.KD_KECAMATAN||'.'|| SPT.KD_KELURAHAN||'.'|| SPT.KD_BLOK||'.'||SPT.NO_URUT||'.'||SPT.KD_JNS_OP NOP,NM_WP_SPPT, 'RT '|| RT_WP_SPPT ||'RW '||RW_WP_SPPT ||KELURAHAN_WP_SPPT ALAMAT, NJOP_BUMI_SPPT, NJOP_BNG_SPPT,PBB_YG_HARUS_DIBAYAR_SPPT ,get_denda(spt.kd_dati2, spt.kd_kecamatan, spt.kd_kelurahan, spt.kd_blok, spt.no_urut, spt.kd_jns_op,spt.thn_pajak_sppt,pbb_yg_harus_dibayar_sppt,tgl_jatuh_tempo_sppt, case when STATUS_PEMBAYARAN_SPPT= 0 then sysdate else TGL_PEMBAYARAN_SPPT end ) denda, STATUS_PEMBAYARAN_SPPT, to_char(TGL_PEMBAYARAN_SPPT ,'dd/mm/yyyy') TGL_BAYAR,SPT.THN_PAJAK_SPPT
                            FROM (
                                SELECT * FROM SPPT
                                WHERE KD_PROPINSI = '$KD_PROPINSI'
                                AND KD_DATI2      = '$KD_DATI2'
                                AND KD_KECAMATAN  = '$KD_KECAMATAN'
                                AND KD_KELURAHAN  = '$KD_KELURAHAN'
                                AND KD_BLOK       = '$KD_BLOK'
                                AND NO_URUT       = '$NO_URUT'
                                AND KD_JNS_OP     ='$KD_JNS_OP'   
                                $and
                                ) SPT
                                left join  PEMBAYARAN_SPPT PS
                                on  SPT.KD_KECAMATAN = PS.KD_KECAMATAN
                                AND SPT.KD_KELURAHAN = PS.KD_KELURAHAN
                                AND SPT.KD_BLOK =  PS.KD_BLOK
                                AND SPT.NO_URUT =  PS.NO_URUT
                                AND SPT.KD_JNS_OP = PS.KD_JNS_OP
                                AND SPT.THN_PAJAK_SPPT = PS.THN_PAJAK_SPPT order by SPT.THN_PAJAK_SPPT asc")->result();
        return $res;
    }


    function jsonpencarian($kode_kec, $kode_kel, $nama = null)
    {
        $nama = strtolower($nama);
        $this->datatables->select("THN_PAJAK_SPPT,
                                NOP,
                                NM_WP_SPPT,
                                ALAMAT,
                                LUAS_BUMI_SPPT,
                                LUAS_BNG_SPPT,
                                NJOP_BUMI_SPPT,
                                NJOP_BNG_SPPT,
                                PBB_YG_HARUS_DIBAYAR_SPPT");
        $this->datatables->from("( SELECT THN_PAJAK_SPPT,KD_PROPINSI||'.'||KD_DATI2||'.'||KD_KECAMATAN||'.'|| KD_KELURAHAN||'.'||KD_BLOK||'-'||NO_URUT||'.'||KD_JNS_OP NOP,NM_WP_SPPT,JLN_WP_SPPT||' '||BLOK_KAV_NO_WP_SPPT||' RW '||RW_WP_SPPT||' RT '||RT_WP_SPPT||' '||KELURAHAN_WP_SPPT||' '||KOTA_WP_SPPT ALAMAT,LUAS_BUMI_SPPT,LUAS_BNG_SPPT,NJOP_BUMI_SPPT,NJOP_BNG_SPPT,PBB_YG_HARUS_DIBAYAR_SPPT  
                                FROM SPPT  WHERE kd_kecamatan='$kode_kec' and kd_kelurahan='$kode_kel' and lower(nm_wp_sppt) like '%$nama%')");
        return $this->datatables->generate();
    }
}
