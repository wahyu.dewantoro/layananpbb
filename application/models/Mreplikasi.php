<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Mreplikasi extends CI_Model
{

    public $table = 'PST_PERMOHONAN';
    public $id = '';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    }

    function getNamaWP($nop)
    {
        $rk = $this->db->query("select nm_wp from dat_subjek_pajak b 
                                where subjek_pajak_id='$nop'")->row();
        if (count($rk) > 0) {
            $res = $rk->NM_WP;
        } else {
            $res = '-';
        }
        return $res;
    }
}

/* End of file Mpermohonan.php */
/* Location: ./application/models/Mpermohonan.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2017-04-29 08:09:04 */
/* http://harviacode.com */
