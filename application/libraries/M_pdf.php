<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

include_once APPPATH . '/third_party/mpdf/mpdf.php';

class M_pdf
{

    public $param;
    public $pdf;
    public function __construct($param = "'utf-8', [216, 279]")
    {
        $this->param = $param;
        // 21,6 CM X 27,9 
        $this->pdf = new mPDF('utf-8'/*,[216,279]*/);
    }
}
